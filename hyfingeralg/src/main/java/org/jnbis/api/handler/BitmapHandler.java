package org.jnbis.api.handler;

import org.jnbis.api.model.Bitmap;


import java.io.ByteArrayOutputStream;
import java.io.IOException;

/**
 * A handler for bitmap images.
 */
public final class BitmapHandler {
    private static final int[] MASKS = {0x000000ff, 0x000000ff, 0x000000ff};

    private final Bitmap bitmap;

    /**
     * Creates an instance of <code>BitmapHandler</code> with given <code>Bitmap</code>.
     *
     * @param bitmap the given bitmap to handle, not null
     * @see Bitmap
     */
    public BitmapHandler(Bitmap bitmap) {
        this.bitmap = bitmap;
    }



    /**
     * Returns the enclosed <code>Bitmap</code> data.
     *
     * @return a FileHandler, not null
     * @see Bitmap
     */
    public Bitmap asBitmap() {
        return bitmap;
    }


}
