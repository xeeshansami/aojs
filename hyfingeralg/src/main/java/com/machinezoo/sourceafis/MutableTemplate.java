// Part of SourceAFIS for Java: https://sourceafis.machinezoo.com/java
package com.machinezoo.sourceafis;

import java.util.*;

public class MutableTemplate {
	public IntPoint size;
	public List<MutableMinutia> minutiae;
}
