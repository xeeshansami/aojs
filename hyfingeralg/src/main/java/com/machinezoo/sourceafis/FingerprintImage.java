// Part of SourceAFIS for Java: https://sourceafis.machinezoo.com/java
package com.machinezoo.sourceafis;

import java.util.*;



public class FingerprintImage {
	/*
	 * API roadmap:
	 * + position(FingerprintPosition)
	 * + other fingerprint properties
	 */
	static {
		PlatformCheck.run();
	}

	public FingerprintImage() {
	}
	double dpi = 500;

	public FingerprintImage dpi(double dpi) {
		if (dpi < 20 || dpi > 20_000)
			throw new IllegalArgumentException();
		this.dpi = dpi;
		return this;
	}
	public DoubleMatrix matrix;

	public FingerprintImage decode(byte[] image) {
		Objects.requireNonNull(image);
		ImageDecoder.DecodedImage decoded = ImageDecoder.decodeAny(image);
		matrix = new DoubleMatrix(decoded.width, decoded.height);
		for (int y = 0; y < decoded.height; ++y) {
			for (int x = 0; x < decoded.width; ++x) {
				int pixel = decoded.pixels[y * decoded.width + x];
				int color = (pixel & 0xff) + ((pixel >> 8) & 0xff) + ((pixel >> 16) & 0xff);
				matrix.set(x, y, 1 - color * (1.0 / (3.0 * 255.0)));
			}
		}
		return this;
	}

}
