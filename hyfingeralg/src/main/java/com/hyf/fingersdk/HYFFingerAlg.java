package com.hyf.fingersdk;

import com.machinezoo.noexception.Exceptions;
import com.machinezoo.sourceafis.DoubleMatrix;
import com.machinezoo.sourceafis.FeatureExtractor;
import com.machinezoo.sourceafis.FingerprintImage;
import com.machinezoo.sourceafis.ImageDecoder;
import com.seamfix.calculatenfiq.NFIQUtil;

/*import org.jnbis.api.Jnbis;
import org.jnbis.api.model.Bitmap;*/

public class HYFFingerAlg {

    public static int getRawMinutiaeCount(byte[] img, int width, int height) {
        return getRawMinutiaeCount(img, width, height, 500.0f);
    }

    public static int getRawNFIQ(byte[] img, int width, int height) {
        return NFIQUtil.calculateNFIQUsingRawBytes(img, width, height);

    }

    public static int getRawMinutiaeCount(byte[] img, int width, int height, double dpi) {
       /*
        DoubleMatrix matrix = new DoubleMatrix(width, height);
        int v = 0;
        int color = 0 ;
        int i = 0 ;
        for (int y = 0; y < height; ++y) {
            for (int x = 0; x < width; ++x) {
                v = img[i++]&0xFF;
                color = Color.argb(0,v, v, v);
                matrix.set(x, y, 1 - color * (1.0 / (3.0 * 255.0)));
            }
        }



        MutableTemplate immutable = FeatureExtractor.extract(matrix,dpi);
        return immutable.minutiae.size();
        */
        ImageDecoder.DecodedImage decoded = decode(img, width, height);
        DoubleMatrix matrix = new DoubleMatrix(width, height);
        for (int y = 0; y < decoded.height; ++y) {
            for (int x = 0; x < decoded.width; ++x) {
                int pixel = decoded.pixels[y * decoded.width + x];
                int color = (pixel & 0xff) + ((pixel >> 8) & 0xff) + ((pixel >> 16) & 0xff);
                matrix.set(x, y, 1 - color * (1.0 / (3.0 * 255.0)));
            }
        }


        com.machinezoo.sourceafis.MutableTemplate immutable = FeatureExtractor.extract(matrix, dpi);
        return immutable.minutiae.size();

    }

    public static com.machinezoo.sourceafis.ImageDecoder.DecodedImage decode(byte[] img, int width, int height) {
        return Exceptions.sneak().get(() -> {

            int[] pixels = new int[width * height];
            for (int y = 0; y < height; ++y) {
                for (int x = 0; x < width; ++x) {
                    int gray = img[y * width + x] & 0xff;
                    pixels[y * width + x] = 0xff00_0000 | (gray << 16) | (gray << 8) | gray;
                }
            }
            return new com.machinezoo.sourceafis.ImageDecoder.DecodedImage(width, height, pixels);
        });
    }


    public static int getWsqMinutiaeCount(byte[] wsq) {
        return getWsqMinutiaeCount(wsq, 500.0f);
    }

    public static int getWsqMinutiaeCount(byte[] image, double dpi) {
        /*
        if (image.length < 2 || image[0] != (byte)0xff || image[1] != (byte)0xa0)
            return -1;

        Bitmap bitmap = Jnbis.wsq().decode(image).asBitmap();
        int width = bitmap.getWidth();
        int height = bitmap.getHeight();
        byte[] buffer = bitmap.getPixels();

        return getRawMinutiaeCount(buffer,width,height,dpi);
        */


        com.machinezoo.sourceafis.MutableTemplate immutable = FeatureExtractor.extract(new FingerprintImage().decode(image).matrix, dpi);

        return immutable.minutiae.size();


    }


    public static int getNFIQVale(int data_quality) {
        int nfiq = 0;
        if (data_quality >= 100) {
            nfiq = 1;
        } else if (data_quality >= 75 && data_quality <= 99) {
            nfiq = 2;
        } else if (data_quality >= 50 && data_quality <= 74) {
            nfiq = 3;
        } else if (data_quality >= 25 && data_quality <= 49) {
            nfiq = 4;
        } else if (data_quality >= 0 && data_quality <= 24) {
            nfiq = 5;
        }
        return nfiq;
    }


}
