package com.hyf.fingersdk;

import java.util.*;



public class HFingerprintImage {


	public HFingerprintImage() {
	}
	double dpi = 500;

	public HFingerprintImage dpi(double dpi) {
		if (dpi < 20 || dpi > 20_000)
			throw new IllegalArgumentException();
		this.dpi = dpi;
		return this;
	}
	public DoubleMatrix matrix;

	public HFingerprintImage decode(byte[] image) {
		Objects.requireNonNull(image);
		ImageDecoder.DecodedImage decoded = ImageDecoder.decodeAny(image);
		matrix = new DoubleMatrix(decoded.width, decoded.height);
		for (int y = 0; y < decoded.height; ++y) {
			for (int x = 0; x < decoded.width; ++x) {
				int pixel = decoded.pixels[y * decoded.width + x];
				int color = (pixel & 0xff) + ((pixel >> 8) & 0xff) + ((pixel >> 16) & 0xff);
				matrix.set(x, y, 1 - color * (1.0 / (3.0 * 255.0)));
			}
		}
		return this;
	}

}
