// Part of SourceAFIS for Java: https://sourceafis.machinezoo.com/java
package com.hyf.fingersdk;

enum MinutiaType {
	ENDING,
	BIFURCATION
}
