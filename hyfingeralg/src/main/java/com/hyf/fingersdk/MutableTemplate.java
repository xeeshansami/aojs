// Part of SourceAFIS for Java: https://sourceafis.machinezoo.com/java
package com.hyf.fingersdk;

import java.util.List;

public class MutableTemplate {
	public IntPoint size;
	public List<MutableMinutia> minutiae;
}
