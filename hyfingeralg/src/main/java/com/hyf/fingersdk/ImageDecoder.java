
package com.hyf.fingersdk;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import static java.util.stream.Collectors.*;

import java.lang.reflect.*;
import java.util.*;


abstract class ImageDecoder {
	static class DecodedImage {
		int width;
		int height;
		/*
		 * Format identical to BufferedImage.TYPE_INT_ARGB, i.e. 8 bits for alpha (FF is opaque, 00 is transparent)
		 * followed by 8-bit values for red, green, and blue in this order from highest bits to lowest.
		 */
		int[] pixels;
		DecodedImage(int width, int height, int[] pixels) {
			this.width = width;
			this.height = height;
			this.pixels = pixels;
		}
	}
	/*
	 * This is used to check whether the image decoder implementation exists.
	 * If it does not, we can produce understandable error message instead of ClassNotFoundException.
	 */
	abstract boolean available();
	abstract String name();
	/*
	 * Decoding method never returns null. It throws if it fails to decode the template,
	 * including cases when the decoder simply doesn't support the image format.
	 */
	abstract DecodedImage decode(byte[] image);
	/*
	 * Order is significant. If multiple decoders support the format, the first one wins.
	 * This list is ordered to favor more common image formats and more common image decoders.
	 * This makes sure that SourceAFIS performs equally well for common formats and common decoders
	 * regardless of how many special-purpose decoders are added to this list.
	 */
	private static final List<ImageDecoder> all = Arrays.asList(
		new AndroidDecoder());
	static DecodedImage decodeAny(byte[] image) {
		Map<ImageDecoder, Throwable> exceptions = new HashMap<>();
		for (ImageDecoder decoder : all) {
			try {
				if (!decoder.available())
					throw new UnsupportedOperationException("Image decoder is not available.");
				return decoder.decode(image);
			} catch (Throwable ex) {
				exceptions.put(decoder, ex);
			}
		}
		/*
		 * We should create an exception type that contains a lists of exceptions from all decoders.
		 * But for now we don't want to complicate SourceAFIS API.
		 * It will wait until this code gets moved to a separate image decoding library.
		 * For now, we just summarize all the exceptions in a long message.
		 */
		throw new IllegalArgumentException(String.format("Unsupported image format [%s].", all.stream()
			.map(d -> String.format("%s = '%s'", d.name(), formatError(exceptions.get(d))))
			.collect(joining(", "))));
	}
	private static String formatError(Throwable exception) {
		List<Throwable> ancestors = new ArrayList<>();
		for (Throwable ancestor = exception; ancestor != null; ancestor = ancestor.getCause())
			ancestors.add(ancestor);
		return ancestors.stream()
			.map(ex -> ex.toString())
			.collect(joining(" -> "));
	}



	/*
	 * This decoder uses Android's Bitmap class to decode templates.
	 * Note that Bitmap class will not work in unit tests. It only works inside a full-blown emulator.
	 * 
	 * Since direct references of Android libraries would not compile,
	 * we will reference BitmapFactory and Bitmap via reflection.
	 */
	private static class AndroidDecoder extends ImageDecoder {
		@Override
		boolean available() {
			return true;
		}
		@Override
		String name() {
			return "Android";
		}
		@Override
		DecodedImage decode(byte[] image) {
			BitmapFactory.decodeByteArray(image,0,image.length);
			Bitmap bitmap = BitmapFactory.decodeByteArray(image,0,image.length);
			int width = bitmap.getWidth();
			int height = bitmap.getHeight();
			int[] pixels = new int[width * height];
			bitmap.getPixels(pixels, 0, width, 0, 0, width, height);
			return new DecodedImage(width, height, pixels);
		}
	}
}
