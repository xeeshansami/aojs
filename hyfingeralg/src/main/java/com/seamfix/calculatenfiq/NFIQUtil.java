package com.seamfix.calculatenfiq;

public class NFIQUtil {

   // private static NFIQUtil m_instance = null;
    static {
        try {
            System.loadLibrary("log");
            System.loadLibrary("nativelib");
        } catch (UnsatisfiedLinkError e) {

        }
    }

  /*  public static NFIQUtil getInstance()
    {
        if (m_instance == null)
        {
            m_instance = new NFIQUtil();
        }
        return m_instance;
    }*/

    private static native int calculateNFIQ(byte[] imageData, int imageWidth, int imageHeight);

    public static int calculateNFIQUsingRawBytes(byte[] rawBytes, int imageWidth, int imageHeight) {
        return calculateNFIQ(rawBytes,imageWidth,imageHeight);
    }
}
