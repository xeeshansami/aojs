package com.hbl.bot.model

import com.hbl.bot.ui.adapter.CIF14Model


enum class DocumentCategoryState {
    Mandatory, MandatoryCompleted, Optional, OptionalCompleted
}

class ModelDocumentUpload(
    val category: String,
    var state: DocumentCategoryState,
    val images: ArrayList<CIF14Model>
)