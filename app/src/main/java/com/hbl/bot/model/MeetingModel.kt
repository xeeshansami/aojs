package com.hbl.bot.model

class MeetingModel(
    val Name: String,
    val ContactNo: String,
    val TrackingNo: String,
    val Date: String,
    val Time: String,
    val Address: String
) {

}