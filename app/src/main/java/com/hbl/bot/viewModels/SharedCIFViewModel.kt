package com.hbl.bot.viewModels

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hbl.bot.R
import com.hbl.bot.model.DocumentCategoryState
import com.hbl.bot.model.DrawerItem
import com.hbl.bot.model.DrawerModel
import com.hbl.bot.model.ModelDocumentUpload
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.POMStatus

class SharedCIFViewModel : ViewModel() {
    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val types = arrayOf(
        ModelDocumentUpload("ID Document", DocumentCategoryState.Optional, ArrayList()),
        ModelDocumentUpload("Nadra Verification", DocumentCategoryState.Mandatory, ArrayList()),
        ModelDocumentUpload("Proof of Address", DocumentCategoryState.Optional, ArrayList()),
        ModelDocumentUpload("UNSC Validation Print", DocumentCategoryState.Mandatory, ArrayList()),
        ModelDocumentUpload("Customer Signed ACOIF", DocumentCategoryState.Mandatory, ArrayList()),
        ModelDocumentUpload(
            "Customer & Bank Signed T&Cs",
            DocumentCategoryState.Optional,
            ArrayList()
        ),
        ModelDocumentUpload("Signature Specimen", DocumentCategoryState.Optional, ArrayList()),
        ModelDocumentUpload(
            "Proof of Income/ Source of Funds",
            DocumentCategoryState.Mandatory,
            ArrayList()
        ),
        ModelDocumentUpload("Key Fact Sheet", DocumentCategoryState.Optional, ArrayList()),
        ModelDocumentUpload(
            "Signature Specimen (SS) Card",
            DocumentCategoryState.Optional,
            ArrayList()
        )
    )
    val isPhotoTaken = MutableLiveData<Boolean>()
    val showAvatar = MutableLiveData<Boolean>()
    val mList = mutableListOf<DrawerModel>()
    val indicator = MutableLiveData<Boolean>(true)
    var currentFragmentIndex = MutableLiveData<Int>()
    var totalSteps = MutableLiveData<Int>()
    var pOMStatus: POMStatus? = null

    //:TODO initiateDrawer
    fun initiateDrawer(context: Context) {
        pOMStatus = POMStatus(context as CIFRootActivity)
        mList.clear()
        // All Models
        /*if count == x all fields are null so status will be lineup*/
        /*if count  < x some fields is null or not so status will be pending*/
        /*if count == 0 all fields are filled so status will be done*/
        //:TODO customer ID information
        if (pOMStatus?.customerIDInfoPage1()!! <= 7 && pOMStatus?.customerIDInfoPage1() != 0) {
            Log.i("customerIDInfoPage1", "Lineup")
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_id_info),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerIDInfoPage1() == 0) {
            Log.i("customerIDInfoPage1", "done")
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_id_info),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customer Biometric
        if (pOMStatus?.customerBiometricPage1()!! <= 4 && pOMStatus?.customerBiometricPage1() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.bio_metric_Information),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerBiometricPage1() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.bio_metric_Information),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customer ID Details
        if (pOMStatus?.customerIdDetailsPage1()!! <= 3 && pOMStatus?.customerIdDetailsPage1() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_id_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerIdDetailsPage1() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_id_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customer personal information
        if (pOMStatus?.customerPersonalInfoPage1And2()!! <= 12 && pOMStatus?.customerPersonalInfoPage1And2() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_personal_information),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerPersonalInfoPage1And2() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_personal_information),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customer PEP
        if (/*pOMStatus?.customerInfo?.PEP.equals("1") &&*/ pOMStatus?.customerPepPage1_2_3() == 0) {
            Log.i("PepTestDrawer", " = " + pOMStatus?.customerInfo?.PEP)
            mList.add(
                DrawerModel(
                    context.getString(R.string.pep_details),
                    DrawerItem.ITEM_DONE
                )
            )
        } else {
            mList.add(
                DrawerModel(
                    context.getString(R.string.pep_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        }
        //:TODO customer Contact Info
        if (pOMStatus?.customerContactInfoPage1()!! <= 5 && pOMStatus?.customerContactInfoPage1() != 0) {
            // Customer Contact Details
            mList.add(
                DrawerModel(
                    context.getString(R.string.contact_information),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerContactInfoPage1() == 0) {
            // Customer Contact Details
            mList.add(
                DrawerModel(
                    context.getString(R.string.contact_information),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customerAddress1 Current
        if (pOMStatus?.customerAddressPage1()!! <= 6 && pOMStatus?.customerAddressPage1() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.current_address),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerAddressPage1() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.current_address),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customerAddress2 Permanent
        if (pOMStatus?.customerAddressPage2()!! <= 5 && pOMStatus?.customerAddressPage2() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.permanent_address),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerAddressPage2() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.permanent_address),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customerAddress3 Office
        if (pOMStatus?.customerAddressPage3()!! <= 5 && pOMStatus?.customerAddressPage3() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.office_address),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerAddressPage3() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.office_address),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO  FATCA Information
        if (pOMStatus?.customerFATCAPage1And2()!! <= 4 && pOMStatus?.customerFATCAPage1And2() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.fata_for_us_citizens_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerFATCAPage1And2() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.fata_for_us_citizens_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO Customer CRS
        if (pOMStatus?.customerCRSPage1And2()!! <= 3 && pOMStatus?.customerCRSPage1And2() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.other_country_tax_payer),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerCRSPage1And2() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.other_country_tax_payer),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Demographic (CDD)
        if (pOMStatus?.customerDemographicsPage1_2_3()!! <= 9 && pOMStatus?.customerDemographicsPage1_2_3() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_demographic),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerDemographicsPage1_2_3() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_demographic),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Financial Supporter Page
        if (pOMStatus?.customerFinancialSupporterPage1()!! <= 8 && pOMStatus?.customerFinancialSupporterPage1() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.financial_supporter_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerFinancialSupporterPage1() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.financial_supporter_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO CDD Details
        if (pOMStatus?.customerCDDDetails()!! <= 3 && pOMStatus?.customerCDDDetails() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.cdd_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerCDDDetails() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.cdd_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Foreign Inward/Onward Remittance Details
        if (pOMStatus?.customerInwardOutWardPage()!! <= 6 && pOMStatus?.customerInwardOutWardPage() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.foreign_inward_outward_remittance_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerInwardOutWardPage() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.foreign_inward_outward_remittance_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Other Bank Infor
        if (pOMStatus?.customerOtherBankInfo()!! <= 2 && pOMStatus?.customerOtherBankInfo() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.other_bank_information_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerOtherBankInfo() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.other_bank_information_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Verification Customer Due Diligence
        if (pOMStatus?.customerVerifyCDD()!! <= 4 && pOMStatus?.customerVerifyCDD() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.Verification_customer_due_dillgence),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerVerifyCDD() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.Verification_customer_due_dillgence),
                    DrawerItem.ITEM_DONE
                )
            )
        }


        //:TODO Customer Enhance Due Diligence
        //:TODO EDD Details
        if (pOMStatus?.customerEDDDetails()!! <= 2 && pOMStatus?.customerEDDDetails() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerEDDDetails() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO EDD-Additional Source of Income
        if (pOMStatus?.customerEddAdditionalSource()!! == 1) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_additional_source_of_income),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerEddAdditionalSource() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_additional_source_of_income),
                    DrawerItem.ITEM_DONE
                )
            )
        }


        //:TODO EDD - Additional Queries
        if (pOMStatus?.customerEddAdditionalQueries()!! <= 6 && pOMStatus?.customerEddAdditionalQueries() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_additional_quries),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerEddAdditionalQueries() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_additional_quries),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO EDD Financial Supporter of income
        if (pOMStatus?.customerEddFinancialSupporter()!! <= 2 && pOMStatus?.customerEddFinancialSupporter() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_financial_supporter_source_of_income),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerEddFinancialSupporter() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_financial_supporter_source_of_income),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO EDD Verification
        if (pOMStatus?.customerEddVerification()!! <= 4 && pOMStatus?.customerEddVerification() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_verification),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.customerEddVerification() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_verification),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO Account
        if (pOMStatus?.Account_Page_1_2_3_4()!! <= 10 && pOMStatus?.Account_Page_1_2_3_4() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.account),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.Account_Page_1_2_3_4() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.account),
                    DrawerItem.ITEM_DONE
                )
            )
        }


        //:TODO NOK
        if (pOMStatus?.NextOfKin_Page_1_2_3()!! <= 9 && pOMStatus?.NextOfKin_Page_1_2_3() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.next_of_kin),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (pOMStatus?.NextOfKin_Page_1_2_3() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.next_of_kin),
                    DrawerItem.ITEM_DONE
                )
            )
        }

    }
}
