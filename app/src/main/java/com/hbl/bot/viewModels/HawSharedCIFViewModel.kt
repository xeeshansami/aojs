package com.hbl.bot.viewModels

import android.content.Context
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hbl.bot.R
import com.hbl.bot.model.DocumentCategoryState
import com.hbl.bot.model.DrawerItem
import com.hbl.bot.model.DrawerModel
import com.hbl.bot.model.ModelDocumentUpload
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.utils.HawPOMStatus

class HawSharedCIFViewModel : ViewModel() {
    private val _text = MutableLiveData<String>().apply {
        value = "This is home Fragment"
    }
    val types = arrayOf(
        ModelDocumentUpload("ID Document", DocumentCategoryState.Optional, ArrayList()),
        ModelDocumentUpload("Nadra Verification", DocumentCategoryState.Mandatory, ArrayList()),
        ModelDocumentUpload("Proof of Address", DocumentCategoryState.Optional, ArrayList()),
        ModelDocumentUpload("UNSC Validation Print", DocumentCategoryState.Mandatory, ArrayList()),
        ModelDocumentUpload("Customer Signed ACOIF", DocumentCategoryState.Mandatory, ArrayList()),
        ModelDocumentUpload(
            "Customer & Bank Signed T&Cs",
            DocumentCategoryState.Optional,
            ArrayList()
        ),
        ModelDocumentUpload("Signature Specimen", DocumentCategoryState.Optional, ArrayList()),
        ModelDocumentUpload(
            "Proof of Income/ Source of Funds",
            DocumentCategoryState.Mandatory,
            ArrayList()
        ),
        ModelDocumentUpload("Key Fact Sheet", DocumentCategoryState.Optional, ArrayList()),
        ModelDocumentUpload(
            "Signature Specimen (SS) Card",
            DocumentCategoryState.Optional,
            ArrayList()
        )
    )
    val isPhotoTaken = MutableLiveData<Boolean>()
    val showAvatar = MutableLiveData<Boolean>()
    val mList = mutableListOf<DrawerModel>()
    val indicator = MutableLiveData<Boolean>(true)
    var currentFragmentIndex = MutableLiveData<Int>()
    var totalSteps = MutableLiveData<Int>()
    var HawPOMStatus: HawPOMStatus? = null

    //:TODO initiateDrawer
    fun initiateDrawer(context: Context) {
        HawPOMStatus = HawPOMStatus(context as HawActivity)
        mList.clear()
        // All Models
        /*if count == x all fields are null so status will be lineup*/
        /*if count  < x some fields is null or not so status will be pending*/
        /*if count == 0 all fields are filled so status will be done*/
        //:TODO customer ID information
        if (HawPOMStatus?.customerIDInfoPage1()!! <= 7 && HawPOMStatus?.customerIDInfoPage1() != 0) {
            Log.i("customerIDInfoPage1", "Lineup")
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_id_info),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerIDInfoPage1() == 0) {
            Log.i("customerIDInfoPage1", "done")
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_id_info),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customer Biometric
        if (HawPOMStatus?.customerBiometricPage1()!! <= 4 && HawPOMStatus?.customerBiometricPage1() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.bio_metric_Information),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerBiometricPage1() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.bio_metric_Information),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customer ID Details
        if (HawPOMStatus?.customerIdDetailsPage1()!! <= 3 && HawPOMStatus?.customerIdDetailsPage1() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_id_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerIdDetailsPage1() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_id_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customer personal information
        if (HawPOMStatus?.customerPersonalInfoPage1And2()!! <= 12 && HawPOMStatus?.customerPersonalInfoPage1And2() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_personal_information),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerPersonalInfoPage1And2() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_personal_information),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customer PEP
        if (/*HawPOMStatus?.customerInfo?.PEP.equals("1") &&*/ HawPOMStatus?.customerPepPage1_2_3() == 0) {
            Log.i("PepTestDrawer", " = " + HawPOMStatus?.customerInfo?.PEP)
            mList.add(
                DrawerModel(
                    context.getString(R.string.pep_details),
                    DrawerItem.ITEM_DONE
                )
            )
        } else {
            mList.add(
                DrawerModel(
                    context.getString(R.string.pep_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        }
        //:TODO customer Contact Info
        if (HawPOMStatus?.customerContactInfoPage1()!! <= 5 && HawPOMStatus?.customerContactInfoPage1() != 0) {
            // Customer Contact Details
            mList.add(
                DrawerModel(
                    context.getString(R.string.contact_information),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerContactInfoPage1() == 0) {
            // Customer Contact Details
            mList.add(
                DrawerModel(
                    context.getString(R.string.contact_information),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customerAddress1 Current
        if (HawPOMStatus?.customerAddressPage1()!! <= 6 && HawPOMStatus?.customerAddressPage1() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.current_address),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerAddressPage1() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.current_address),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customerAddress2 Permanent
        if (HawPOMStatus?.customerAddressPage2()!! <= 5 && HawPOMStatus?.customerAddressPage2() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.permanent_address),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerAddressPage2() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.permanent_address),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO customerAddress3 Office
        if (HawPOMStatus?.customerAddressPage3()!! <= 5 && HawPOMStatus?.customerAddressPage3() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.office_address),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerAddressPage3() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.office_address),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO  FATCA Information
        if (HawPOMStatus?.customerFATCAPage1And2()!! <= 4 && HawPOMStatus?.customerFATCAPage1And2() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.fata_for_us_citizens_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerFATCAPage1And2() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.fata_for_us_citizens_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO Customer CRS
        if (HawPOMStatus?.customerCRSPage1And2()!! <= 3 && HawPOMStatus?.customerCRSPage1And2() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.other_country_tax_payer),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerCRSPage1And2() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.other_country_tax_payer),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Demographic (CDD)
        if (HawPOMStatus?.customerDemographicsPage1_2_3()!! <= 9 && HawPOMStatus?.customerDemographicsPage1_2_3() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_demographic),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerDemographicsPage1_2_3() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.customer_demographic),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Financial Supporter Page
        if (HawPOMStatus?.customerFinancialSupporterPage1()!! <= 8 && HawPOMStatus?.customerFinancialSupporterPage1() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.financial_supporter_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerFinancialSupporterPage1() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.financial_supporter_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO CDD Details
        if (HawPOMStatus?.customerCDDDetails()!! <= 3 && HawPOMStatus?.customerCDDDetails() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.cdd_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerCDDDetails() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.cdd_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Foreign Inward/Onward Remittance Details
        if (HawPOMStatus?.customerInwardOutWardPage()!! <= 6 && HawPOMStatus?.customerInwardOutWardPage() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.foreign_inward_outward_remittance_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerInwardOutWardPage() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.foreign_inward_outward_remittance_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Other Bank Infor
        if (HawPOMStatus?.customerOtherBankInfo()!! <= 2 && HawPOMStatus?.customerOtherBankInfo() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.other_bank_information_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerOtherBankInfo() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.other_bank_information_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO Verification Customer Due Diligence
        if (HawPOMStatus?.customerVerifyCDD()!! <= 4 && HawPOMStatus?.customerVerifyCDD() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.Verification_customer_due_dillgence),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerVerifyCDD() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.Verification_customer_due_dillgence),
                    DrawerItem.ITEM_DONE
                )
            )
        }


        //:TODO Customer Enhance Due Diligence
        //:TODO EDD Details
        if (HawPOMStatus?.customerEDDDetails()!! <= 2 && HawPOMStatus?.customerEDDDetails() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_details),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerEDDDetails() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_details),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO EDD-Additional Source of Income
        if (HawPOMStatus?.customerEddAdditionalSource()!! == 1) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_additional_source_of_income),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerEddAdditionalSource() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_additional_source_of_income),
                    DrawerItem.ITEM_DONE
                )
            )
        }


        //:TODO EDD - Additional Queries
        if (HawPOMStatus?.customerEddAdditionalQueries()!! <= 6 && HawPOMStatus?.customerEddAdditionalQueries() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_additional_quries),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerEddAdditionalQueries() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_additional_quries),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO EDD Financial Supporter of income
        if (HawPOMStatus?.customerEddFinancialSupporter()!! <= 2 && HawPOMStatus?.customerEddFinancialSupporter() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_financial_supporter_source_of_income),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerEddFinancialSupporter() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_financial_supporter_source_of_income),
                    DrawerItem.ITEM_DONE
                )
            )
        }

        //:TODO EDD Verification
        if (HawPOMStatus?.customerEddVerification()!! <= 4 && HawPOMStatus?.customerEddVerification() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_verification),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.customerEddVerification() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.edd_verification),
                    DrawerItem.ITEM_DONE
                )
            )
        }
        //:TODO Account
        if (HawPOMStatus?.Account_Page_1_2_3_4()!! <= 10 && HawPOMStatus?.Account_Page_1_2_3_4() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.account),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.Account_Page_1_2_3_4() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.account),
                    DrawerItem.ITEM_DONE
                )
            )
        }


        //:TODO NOK
        if (HawPOMStatus?.NextOfKin_Page_1_2_3()!! <= 9 && HawPOMStatus?.NextOfKin_Page_1_2_3() != 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.next_of_kin),
                    DrawerItem.ITEM_LINEUP
                )
            )
        } else if (HawPOMStatus?.NextOfKin_Page_1_2_3() == 0) {
            mList.add(
                DrawerModel(
                    context.getString(R.string.next_of_kin),
                    DrawerItem.ITEM_DONE
                )
            )
        }

    }
}
