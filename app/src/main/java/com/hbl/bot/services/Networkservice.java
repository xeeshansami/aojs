package com.hbl.bot.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.TrafficStats;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.SystemClock;

import com.hbl.bot.R;
import com.hbl.bot.utils.Constants;

import java.util.Locale;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

public class Networkservice extends Service {
    private long mTotalSpeed = 0;
    private long mDownSpeed = 0;
    private long mUpSpeed = 0;
    private long mLastRxBytes = 0;
    private long mLastTxBytes = 0;
    private long mLastTime = 0;
    private boolean mIsSpeedUnitBits = false;
    private HumanSpeed mSpeed;
    @Override
    public void onCreate() {
        super.onCreate();
        mSpeed = new HumanSpeed();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        handler.post(perioud);
        return START_STICKY;
    }

    public static boolean isOnline(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo nf = cm.getActiveNetworkInfo();
        if (nf != null && nf.isConnectedOrConnecting())
            return true;
        else
            return false;
    }

    Handler handler = new Handler();
    private Runnable perioud = new Runnable() {
        @RequiresApi(api = Build.VERSION_CODES.Q)
        @Override
        public void run() {
            handler.postDelayed(perioud, 1 * 1000 - SystemClock.elapsedRealtime() % 1000);
            getNetworkSpeed();
        }
    };


    @RequiresApi(api = Build.VERSION_CODES.Q)
    public void getNetworkSpeed() {
        long currentRxBytes = TrafficStats.getTotalRxBytes();
        long currentTxBytes = TrafficStats.getTotalTxBytes();
        long usedRxBytes = currentRxBytes - mLastRxBytes;
        long usedTxBytes = currentTxBytes - mLastTxBytes;
        long currentTime = System.currentTimeMillis();
        long usedTime = currentTime - mLastTime;

        mLastRxBytes = currentRxBytes;
        mLastTxBytes = currentTxBytes;
        mLastTime = currentTime;


        calcSpeed(usedTime, usedRxBytes, usedTxBytes);
    }

    void calcSpeed(long timeTaken, long downBytes, long upBytes) {
        long totalSpeed = 0;
        long downSpeed = 0;
        long upSpeed = 0;

        long totalBytes = downBytes + upBytes;

        if (timeTaken > 0) {
            totalSpeed = totalBytes * 1000 / timeTaken;
            downSpeed = downBytes * 1000 / timeTaken;
            upSpeed = upBytes * 1000 / timeTaken;
        }

        mTotalSpeed = totalSpeed;
        mDownSpeed = downSpeed;
        mUpSpeed = upSpeed;

        mSpeed.setSpeed(mTotalSpeed);
    }
    public class HumanSpeed {
        String speedValue;
        String speedUnit;

        private void setSpeed(long speed) {
            if (this == null) return;

            if (mIsSpeedUnitBits) {
                speed *= 8;
            }

            if (speed < 1000000) {
                this.speedUnit = getString(mIsSpeedUnitBits ? R.string.kbps : R.string.kBps);
                this.speedValue = String.valueOf(speed / 1000);
            } else if (speed >= 1000000) {
                this.speedUnit = getString(mIsSpeedUnitBits ? R.string.Mbps : R.string.MBps);

                if (speed < 10000000) {
                    this.speedValue = String.format(Locale.ENGLISH, "%.1f", speed / 1000000.0);
                } else if (speed < 100000000) {
                    this.speedValue = String.valueOf(speed / 1000000);
                } else {
                    this.speedValue = getString(R.string.plus99);
                }
            } else {
                this.speedValue = getString(R.string.dash);
                this.speedUnit = getString(R.string.dash);
            }

            Intent intent = new Intent();
            intent.setAction(Constants.BROADCAST);
            intent.putExtra("online_status", "" + isOnline(Networkservice.this));
            intent.putExtra("networkSpeed",   this.speedValue);
//            Log.i("networkSpeed",this.speedValue);
            sendBroadcast(intent);
        }
    }


}