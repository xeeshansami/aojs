package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface KonnectBranchCityCallBack {
    fun konnectBranchCitySuccess(response: KonnectBranchCityResponse)
    fun konnectBranchCityFailure(response: BaseResponse)
}