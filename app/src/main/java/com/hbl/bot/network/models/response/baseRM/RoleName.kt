import com.google.gson.annotations.SerializedName

data class RoleName(
    @SerializedName("ROLE_NAME") val rOLE_NAME: String
)