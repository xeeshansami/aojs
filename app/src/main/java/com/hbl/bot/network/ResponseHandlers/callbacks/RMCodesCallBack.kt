package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface RMCodesCallBack {
    fun RMCodesSuccess(response: RMCodesResponse)
    fun RMCodesFailure(response: BaseResponse)
}