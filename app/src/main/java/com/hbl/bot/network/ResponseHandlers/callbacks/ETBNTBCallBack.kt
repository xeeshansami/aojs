package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ETBNTBCallBack {
    fun ETBNTBSuccess(response: ETBNTBResponse)
    fun ETBNTBFailure(response: BaseResponse)
}