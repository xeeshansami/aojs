package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.CustomerSegmentCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.CustomerSegmentResponse

class CustomerSegmentHR(callBack: CustomerSegmentCallBack) : BaseRH<CustomerSegmentResponse>() {
    var callback: CustomerSegmentCallBack = callBack
    override fun onSuccess(response: CustomerSegmentResponse?) {
        response?.let { callback.CustomerSegmentSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CustomerSegmentFailure(it) }
    }
}