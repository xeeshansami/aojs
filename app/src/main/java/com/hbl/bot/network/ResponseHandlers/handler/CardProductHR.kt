package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class CardProductHR(callBack: CardProductCallBack) : BaseRH<CardProductResponse>() {
    var callback: CardProductCallBack = callBack
    override fun onSuccess(response: CardProductResponse?) {
        response?.let { callback.CardProductSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CardProductFailure(it) }
    }
}