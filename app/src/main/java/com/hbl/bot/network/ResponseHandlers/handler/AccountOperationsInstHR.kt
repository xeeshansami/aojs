package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class AccountOperationsInstHR(callBack: AccountOperationsInstCallBack) :
    BaseRH<AccountOperationsInstResponse>() {
    var callback: AccountOperationsInstCallBack = callBack
    override fun onSuccess(response: AccountOperationsInstResponse?) {
        response?.let { callback.AccountOperationsInstSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.AccountOperationsInstFailure(it) }
    }
}