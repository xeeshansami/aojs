package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class BusinessAreaHR(callBack: BusinessAreaCallBack) : BaseRH<BusinessAreaResponse>() {
    var callback: BusinessAreaCallBack = callBack
    override fun onSuccess(response: BusinessAreaResponse?) {
        response?.let { callback.BusinessAreaSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.BusinessAreaFailure(it) }
    }
}