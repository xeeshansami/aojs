package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.CityCallBack
import com.hbl.bot.network.models.response.base.*

class CityHR(callBack: CityCallBack) : BaseRH<CityResponse>() {
    var callback: CityCallBack = callBack
    override fun onSuccess(response: CityResponse?) {
        response?.let { callback.CitySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CityFailure(it) }
    }
}