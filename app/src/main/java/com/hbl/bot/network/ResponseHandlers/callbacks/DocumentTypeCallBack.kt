package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface DocumentTypeCallBack {
    fun DocumentTypeSuccess(response: DocTypeResponse)
    fun DocumentTypeFailure(response: BaseResponse)
}