package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ProvinceCallBack {
    fun ProvinceSuccess(response: ProvinceResponse)
    fun ProvinceFailure(response: BaseResponse)
}