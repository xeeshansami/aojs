package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.PepCheckCallBack
import com.hbl.bot.network.models.response.base.*

class PepCheckHR(callBack: PepCheckCallBack) : BaseRH<PepCheckResponse>() {
    var callback: PepCheckCallBack = callBack
    override fun onSuccess(response: PepCheckResponse?) {
        response?.let { callback.PepCheckSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PepCheckFailure(it) }
    }
}