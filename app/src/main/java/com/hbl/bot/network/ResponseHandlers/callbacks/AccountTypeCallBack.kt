package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface AccountTypeCallBack {
    fun AccountTypeSuccess(response: AccountTypeResponse)
    fun AccountTypeFailure(response: BaseResponse)
}