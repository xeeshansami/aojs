package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CountryCityCode() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("COUNTRY_CODE")
    @Expose
    var COUNTRY_CODE: String? = null

    @SerializedName("COUNTRY_DIAL_CODE")
    @Expose
    var COUNTRY_DIAL_CODE: String? = null

    @SerializedName("CITY")
    @Expose
    var CITY: String? = null

    @SerializedName("CITY_DIAL_CODE")
    @Expose
    var CITY_DIAL_CODE: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<CountryCityCode> {
        override fun createFromParcel(parcel: Parcel): CountryCityCode {
            return CountryCityCode(parcel)
        }

        override fun newArray(size: Int): Array<CountryCityCode?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return COUNTRY_CODE.toString() + CITY.toString()
    }
}