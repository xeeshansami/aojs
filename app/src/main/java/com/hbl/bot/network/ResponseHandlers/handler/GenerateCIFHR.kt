package com.hbl.bot.network.ResponseHandlers.handler

import android.util.Log
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class GenerateCIFHR(callBack: GenerateCIFCallBack) : BaseRH<GenerateCIFResponse>() {
    var callback: GenerateCIFCallBack = callBack
    override fun onSuccess(response: GenerateCIFResponse?) {
        try {
            response?.let { callback.GenerateCIFSuccess(it) }
        } catch (ex: Exception) {
            Log.i("SubmitExceptions", ": " + ex.message)
        }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.GenerateCIFFailure(it) }
    }
}