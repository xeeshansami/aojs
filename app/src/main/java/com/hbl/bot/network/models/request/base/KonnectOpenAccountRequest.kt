package com.hbl.bot.network.models.request.base

class KonnectOpenAccountRequest() {
    var identifier = "openAccount"
    var tracking_id = ""
    var channel = "2"
    var beneficial_owner = ""
    var k_tran_id = ""
    var doc_no = ""
    var mobile_number = ""
    var city = ""
    var bm_device_serial = ""
    var bm_device = ""
    var bm_format = "WSQ"
    //var bm_format = "ISO_19794_2"
    var bm_finger_index = ""
    var passcode = ""
    var biometric_data = ""
    var purpose_of_account_id = ""
    var is_pep = "0"
    var fatca = "0"
    //dd-mm-yyyy
    var issuance_date=""
    var accept_terms_and_cond = "1"
    var kfs = "1"
    var email = ""
    var branch_id= ""

}