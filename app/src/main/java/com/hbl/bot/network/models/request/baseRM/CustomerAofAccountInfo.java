
package com.hbl.bot.network.models.request.baseRM;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerAofAccountInfo {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("TRACKING_ID")
    @Expose
    private String tRACKINGID;
    @SerializedName("MYSIS_REF")
    @Expose
    private String mYSISREF;
    @SerializedName("MYSIS_OLD_REF")
    @Expose
    private String mYSISOLDREF;
    @SerializedName("ID_DOC_NO")
    @Expose
    private String iDDOCNO;
    @SerializedName("ID_DOCUMENT_TYPE")
    @Expose
    private String iDDOCUMENTTYPE;
    @SerializedName("NAME")
    @Expose
    private String nAME;
    @SerializedName("WORK_FLOW_CODE")
    @Expose
    private String wORKFLOWCODE;
    @SerializedName("WORK_FLOW_CODE_DESC")
    @Expose
    private String wORKFLOWCODEDESC;
    @SerializedName("PROCESS_CODE")
    @Expose
    private String pROCESSCODE;
    @SerializedName("STATUS")
    @Expose
    private String sTATUS;
    @SerializedName("STATUSDESC")
    @Expose
    private String sTATUSDESC;
    @SerializedName("PICKEDBY")
    @Expose
    private String pICKEDBY;
    @SerializedName("PICKEDBY_ROLE")
    @Expose
    private String pICKEDBYROLE;
    @SerializedName("LAST_ACTION_DATE")
    @Expose
    private String lASTACTIONDATE;
    @SerializedName("LAST_ACTION_USER")
    @Expose
    private String lASTACTIONUSER;
    @SerializedName("LAST_ACTION_USER_ID")
    @Expose
    private String lASTACTIONUSERID;
    @SerializedName("PRIORITY")
    @Expose
    private String pRIORITY;
    @SerializedName("RISK_RATING")
    @Expose
    private String rISKRATING;
    @SerializedName("RISK_RATING_TOTAL")
    @Expose
    private Integer rISKRATINGTOTAL;
    @SerializedName("CHANNEL")
    @Expose
    private String cHANNEL;
    @SerializedName("PURPOSE_OF_FORM")
    @Expose
    private String pURPOSEOFFORM;
    @SerializedName("PURPOSEOFFORM_DESC")
    @Expose
    private String pURPOSEOFFORMDESC;
    @SerializedName("USER_BRANCH")
    @Expose
    private String uSERBRANCH;
    @SerializedName("USER_REGION")
    @Expose
    private String uSERREGION;
    @SerializedName("INITIATED_RM_ID")
    @Expose
    private String iNITIATEDRMID;
    @SerializedName("INITIATED_USER_ID")
    @Expose
    private String iNITIATEDUSERID;
    @SerializedName("APPROPRIATE_RELATION")
    @Expose
    private String aPPROPRIATERELATION;
    @SerializedName("APPRRELATIONDESC")
    @Expose
    private String aPPRRELATIONDESC;
    @SerializedName("AREA")
    @Expose
    private String aREA;
    @SerializedName("DISCREPENT_APPROVAL")
    @Expose
    private String dISCREPENTAPPROVAL;
    @SerializedName("ADMIN_TRIBAL_AREA")
    @Expose
    private String aDMINTRIBALAREA;
    @SerializedName("EMAIL_NOT_ALLOWED")
    @Expose
    private Integer eMAILNOTALLOWED;
    @SerializedName("FILE_UPLOADED")
    @Expose
    private String fILEUPLOADED;
    @SerializedName("ETBNTBFLAG")
    @Expose
    private String eTBNTBFLAG;
    @SerializedName("HIGH_RISK_BRANCH")
    @Expose
    private String hIGHRISKBRANCH;
    @SerializedName("PARTIAL_DOC_ENABLE")
    @Expose
    private String pARTIALDOCENABLE;
    @SerializedName("AOF_APPROVALS")
    @Expose
    private List<AOFAPPROVAL> aOFAPPROVALS = null;
    @SerializedName("CUST_BIOMETRIC")
    @Expose
    private List<CUSTBIOMETRIC> cUSTBIOMETRIC = null;
    @SerializedName("CUST_INFO")
    @Expose
    private List<CUSTINFO> cUSTINFO = null;
    @SerializedName("CUST_PEP")
    @Expose
    private List<CUSTPEP> cUSTPEP = null;
    @SerializedName("CUST_FIN")
    @Expose
    private List<Object> cUSTFIN = null;
    @SerializedName("CUST_SOLE_SELF")
    @Expose
    private List<CUSTSOLESELF> cUSTSOLESELF = null;
    @SerializedName("CUST_EDD")
    @Expose
    private List<CUSTEDD> cUSTEDD = null;
    @SerializedName("CUST_CDD")
    @Expose
    private List<CUSTCDD> cUSTCDD = null;
    @SerializedName("CUST_CONTACTS")
    @Expose
    private List<CUSTCONTACT> cUSTCONTACTS = null;
    @SerializedName("CUST_ADDR")
    @Expose
    private List<CUSTADDRRESS> cUSTADDR = null;
    @SerializedName("CUST_ACCOUNTS")
    @Expose
    private List<Object> cUSTACCOUNTS = null;
    @SerializedName("CUST_DEMOGRAPHICS")
    @Expose
    private List<CUSTDEMOGRAPHIC> cUSTDEMOGRAPHICS = null;
    @SerializedName("CUST_NEXTOFKIN")
    @Expose
    private List<Object> cUSTNEXTOFKIN = null;
    @SerializedName("CUST_DISCREPANCIES")
    @Expose
    private List<Object> cUSTDISCREPANCIES = null;
    @SerializedName("CUST_DOCUMENT_DISCREPANCIES")
    @Expose
    private List<Object> cUSTDOCUMENTDISCREPANCIES = null;
    @SerializedName("ACTION_EVENT_INFO")
    @Expose
    private List<Object> aCTIONEVENTINFO = null;
    @SerializedName("CUST_JOINT")
    @Expose
    private List<Object> cUSTJOINT = null;
    @SerializedName("CUST_STATUS")
    @Expose
    private List<CUSTStatus> cUSTSTATUS = null;
    @SerializedName("USER_INFO")
    @Expose
    private List<USERINFO> uSERINFO = null;
    @SerializedName("DOC_CHECKLIST")
    @Expose
    private List<DOCCHECKLIST> dOCCHECKLIST = null;
    @SerializedName("CUST_DOCUMENTS")
    @Expose
    private List<CUSTDOCUMENT> cUSTDOCUMENTS = null;
    @SerializedName("NON_IN_ACCOUNTS")
    @Expose
    private List<Object> nONINACCOUNTS = null;
    @SerializedName("GUARDIAN_INFO")
    @Expose
    private List<Object> gUARDIANINFO = null;
    @SerializedName("NON_IN_DEMOGRAPHICS")
    @Expose
    private List<Object> nONINDEMOGRAPHICS = null;
    @SerializedName("NON_IN_SIGNATORY")
    @Expose
    private List<Object> nONINSIGNATORY = null;
    @SerializedName("NON_IN_ADDR")
    @Expose
    private List<Object> nONINADDR = null;
    @SerializedName("NON_IN_CDD")
    @Expose
    private List<Object> nONINCDD = null;
    @SerializedName("PROCESSINGDATE")
    @Expose
    private String pROCESSINGDATE;
    @SerializedName("PEP_UPDATE")
    @Expose
    private Integer pEPUPDATE;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTRACKINGID() {
        return tRACKINGID;
    }

    public void setTRACKINGID(String tRACKINGID) {
        this.tRACKINGID = tRACKINGID;
    }

    public String getMYSISREF() {
        return mYSISREF;
    }

    public void setMYSISREF(String mYSISREF) {
        this.mYSISREF = mYSISREF;
    }

    public String getMYSISOLDREF() {
        return mYSISOLDREF;
    }

    public void setMYSISOLDREF(String mYSISOLDREF) {
        this.mYSISOLDREF = mYSISOLDREF;
    }

    public String getIDDOCNO() {
        return iDDOCNO;
    }

    public void setIDDOCNO(String iDDOCNO) {
        this.iDDOCNO = iDDOCNO;
    }

    public String getIDDOCUMENTTYPE() {
        return iDDOCUMENTTYPE;
    }

    public void setIDDOCUMENTTYPE(String iDDOCUMENTTYPE) {
        this.iDDOCUMENTTYPE = iDDOCUMENTTYPE;
    }

    public String getNAME() {
        return nAME;
    }

    public void setNAME(String nAME) {
        this.nAME = nAME;
    }

    public String getWORKFLOWCODE() {
        return wORKFLOWCODE;
    }

    public void setWORKFLOWCODE(String wORKFLOWCODE) {
        this.wORKFLOWCODE = wORKFLOWCODE;
    }

    public String getWORKFLOWCODEDESC() {
        return wORKFLOWCODEDESC;
    }

    public void setWORKFLOWCODEDESC(String wORKFLOWCODEDESC) {
        this.wORKFLOWCODEDESC = wORKFLOWCODEDESC;
    }

    public String getPROCESSCODE() {
        return pROCESSCODE;
    }

    public void setPROCESSCODE(String pROCESSCODE) {
        this.pROCESSCODE = pROCESSCODE;
    }

    public String getSTATUS() {
        return sTATUS;
    }

    public void setSTATUS(String sTATUS) {
        this.sTATUS = sTATUS;
    }

    public String getSTATUSDESC() {
        return sTATUSDESC;
    }

    public void setSTATUSDESC(String sTATUSDESC) {
        this.sTATUSDESC = sTATUSDESC;
    }

    public String getPICKEDBY() {
        return pICKEDBY;
    }

    public void setPICKEDBY(String pICKEDBY) {
        this.pICKEDBY = pICKEDBY;
    }

    public String getPICKEDBYROLE() {
        return pICKEDBYROLE;
    }

    public void setPICKEDBYROLE(String pICKEDBYROLE) {
        this.pICKEDBYROLE = pICKEDBYROLE;
    }

    public String getLASTACTIONDATE() {
        return lASTACTIONDATE;
    }

    public void setLASTACTIONDATE(String lASTACTIONDATE) {
        this.lASTACTIONDATE = lASTACTIONDATE;
    }

    public String getLASTACTIONUSER() {
        return lASTACTIONUSER;
    }

    public void setLASTACTIONUSER(String lASTACTIONUSER) {
        this.lASTACTIONUSER = lASTACTIONUSER;
    }

    public String getLASTACTIONUSERID() {
        return lASTACTIONUSERID;
    }

    public void setLASTACTIONUSERID(String lASTACTIONUSERID) {
        this.lASTACTIONUSERID = lASTACTIONUSERID;
    }

    public String getPRIORITY() {
        return pRIORITY;
    }

    public void setPRIORITY(String pRIORITY) {
        this.pRIORITY = pRIORITY;
    }

    public String getRISKRATING() {
        return rISKRATING;
    }

    public void setRISKRATING(String rISKRATING) {
        this.rISKRATING = rISKRATING;
    }

    public Integer getRISKRATINGTOTAL() {
        return rISKRATINGTOTAL;
    }

    public void setRISKRATINGTOTAL(Integer rISKRATINGTOTAL) {
        this.rISKRATINGTOTAL = rISKRATINGTOTAL;
    }

    public String getCHANNEL() {
        return cHANNEL;
    }

    public void setCHANNEL(String cHANNEL) {
        this.cHANNEL = cHANNEL;
    }

    public String getPURPOSEOFFORM() {
        return pURPOSEOFFORM;
    }

    public void setPURPOSEOFFORM(String pURPOSEOFFORM) {
        this.pURPOSEOFFORM = pURPOSEOFFORM;
    }

    public String getPURPOSEOFFORMDESC() {
        return pURPOSEOFFORMDESC;
    }

    public void setPURPOSEOFFORMDESC(String pURPOSEOFFORMDESC) {
        this.pURPOSEOFFORMDESC = pURPOSEOFFORMDESC;
    }

    public String getUSERBRANCH() {
        return uSERBRANCH;
    }

    public void setUSERBRANCH(String uSERBRANCH) {
        this.uSERBRANCH = uSERBRANCH;
    }

    public String getUSERREGION() {
        return uSERREGION;
    }

    public void setUSERREGION(String uSERREGION) {
        this.uSERREGION = uSERREGION;
    }

    public String getINITIATEDRMID() {
        return iNITIATEDRMID;
    }

    public void setINITIATEDRMID(String iNITIATEDRMID) {
        this.iNITIATEDRMID = iNITIATEDRMID;
    }

    public String getINITIATEDUSERID() {
        return iNITIATEDUSERID;
    }

    public void setINITIATEDUSERID(String iNITIATEDUSERID) {
        this.iNITIATEDUSERID = iNITIATEDUSERID;
    }

    public String getAPPROPRIATERELATION() {
        return aPPROPRIATERELATION;
    }

    public void setAPPROPRIATERELATION(String aPPROPRIATERELATION) {
        this.aPPROPRIATERELATION = aPPROPRIATERELATION;
    }

    public String getAPPRRELATIONDESC() {
        return aPPRRELATIONDESC;
    }

    public void setAPPRRELATIONDESC(String aPPRRELATIONDESC) {
        this.aPPRRELATIONDESC = aPPRRELATIONDESC;
    }

    public String getAREA() {
        return aREA;
    }

    public void setAREA(String aREA) {
        this.aREA = aREA;
    }

    public String getDISCREPENTAPPROVAL() {
        return dISCREPENTAPPROVAL;
    }

    public void setDISCREPENTAPPROVAL(String dISCREPENTAPPROVAL) {
        this.dISCREPENTAPPROVAL = dISCREPENTAPPROVAL;
    }

    public String getADMINTRIBALAREA() {
        return aDMINTRIBALAREA;
    }

    public void setADMINTRIBALAREA(String aDMINTRIBALAREA) {
        this.aDMINTRIBALAREA = aDMINTRIBALAREA;
    }

    public Integer getEMAILNOTALLOWED() {
        return eMAILNOTALLOWED;
    }

    public void setEMAILNOTALLOWED(Integer eMAILNOTALLOWED) {
        this.eMAILNOTALLOWED = eMAILNOTALLOWED;
    }

    public String getFILEUPLOADED() {
        return fILEUPLOADED;
    }

    public void setFILEUPLOADED(String fILEUPLOADED) {
        this.fILEUPLOADED = fILEUPLOADED;
    }

    public String getETBNTBFLAG() {
        return eTBNTBFLAG;
    }

    public void setETBNTBFLAG(String eTBNTBFLAG) {
        this.eTBNTBFLAG = eTBNTBFLAG;
    }

    public String getHIGHRISKBRANCH() {
        return hIGHRISKBRANCH;
    }

    public void setHIGHRISKBRANCH(String hIGHRISKBRANCH) {
        this.hIGHRISKBRANCH = hIGHRISKBRANCH;
    }

    public String getPARTIALDOCENABLE() {
        return pARTIALDOCENABLE;
    }

    public void setPARTIALDOCENABLE(String pARTIALDOCENABLE) {
        this.pARTIALDOCENABLE = pARTIALDOCENABLE;
    }

    public List<AOFAPPROVAL> getAOFAPPROVALS() {
        return aOFAPPROVALS;
    }

    public void setAOFAPPROVALS(List<AOFAPPROVAL> aOFAPPROVALS) {
        this.aOFAPPROVALS = aOFAPPROVALS;
    }

    public List<CUSTBIOMETRIC> getCUSTBIOMETRIC() {
        return cUSTBIOMETRIC;
    }

    public void setCUSTBIOMETRIC(List<CUSTBIOMETRIC> cUSTBIOMETRIC) {
        this.cUSTBIOMETRIC = cUSTBIOMETRIC;
    }

    public List<CUSTINFO> getCUSTINFO() {
        return cUSTINFO;
    }

    public void setCUSTINFO(List<CUSTINFO> cUSTINFO) {
        this.cUSTINFO = cUSTINFO;
    }

    public List<CUSTPEP> getCUSTPEP() {
        return cUSTPEP;
    }

    public void setCUSTPEP(List<CUSTPEP> cUSTPEP) {
        this.cUSTPEP = cUSTPEP;
    }

    public List<Object> getCUSTFIN() {
        return cUSTFIN;
    }

    public void setCUSTFIN(List<Object> cUSTFIN) {
        this.cUSTFIN = cUSTFIN;
    }

    public List<CUSTSOLESELF> getCUSTSOLESELF() {
        return cUSTSOLESELF;
    }

    public void setCUSTSOLESELF(List<CUSTSOLESELF> cUSTSOLESELF) {
        this.cUSTSOLESELF = cUSTSOLESELF;
    }

    public List<CUSTEDD> getCUSTEDD() {
        return cUSTEDD;
    }

    public void setCUSTEDD(List<CUSTEDD> cUSTEDD) {
        this.cUSTEDD = cUSTEDD;
    }

    public List<CUSTCDD> getCUSTCDD() {
        return cUSTCDD;
    }

    public void setCUSTCDD(List<CUSTCDD> cUSTCDD) {
        this.cUSTCDD = cUSTCDD;
    }

    public List<CUSTCONTACT> getCUSTCONTACTS() {
        return cUSTCONTACTS;
    }

    public void setCUSTCONTACTS(List<CUSTCONTACT> cUSTCONTACTS) {
        this.cUSTCONTACTS = cUSTCONTACTS;
    }

    public List<CUSTADDRRESS> getCUSTADDR() {
        return cUSTADDR;
    }

    public void setCUSTADDR(List<CUSTADDRRESS> cUSTADDR) {
        this.cUSTADDR = cUSTADDR;
    }

    public List<Object> getCUSTACCOUNTS() {
        return cUSTACCOUNTS;
    }

    public void setCUSTACCOUNTS(List<Object> cUSTACCOUNTS) {
        this.cUSTACCOUNTS = cUSTACCOUNTS;
    }

    public List<CUSTDEMOGRAPHIC> getCUSTDEMOGRAPHICS() {
        return cUSTDEMOGRAPHICS;
    }

    public void setCUSTDEMOGRAPHICS(List<CUSTDEMOGRAPHIC> cUSTDEMOGRAPHICS) {
        this.cUSTDEMOGRAPHICS = cUSTDEMOGRAPHICS;
    }

    public List<Object> getCUSTNEXTOFKIN() {
        return cUSTNEXTOFKIN;
    }

    public void setCUSTNEXTOFKIN(List<Object> cUSTNEXTOFKIN) {
        this.cUSTNEXTOFKIN = cUSTNEXTOFKIN;
    }

    public List<Object> getCUSTDISCREPANCIES() {
        return cUSTDISCREPANCIES;
    }

    public void setCUSTDISCREPANCIES(List<Object> cUSTDISCREPANCIES) {
        this.cUSTDISCREPANCIES = cUSTDISCREPANCIES;
    }

    public List<Object> getCUSTDOCUMENTDISCREPANCIES() {
        return cUSTDOCUMENTDISCREPANCIES;
    }

    public void setCUSTDOCUMENTDISCREPANCIES(List<Object> cUSTDOCUMENTDISCREPANCIES) {
        this.cUSTDOCUMENTDISCREPANCIES = cUSTDOCUMENTDISCREPANCIES;
    }

    public List<Object> getACTIONEVENTINFO() {
        return aCTIONEVENTINFO;
    }

    public void setACTIONEVENTINFO(List<Object> aCTIONEVENTINFO) {
        this.aCTIONEVENTINFO = aCTIONEVENTINFO;
    }

    public List<Object> getCUSTJOINT() {
        return cUSTJOINT;
    }

    public void setCUSTJOINT(List<Object> cUSTJOINT) {
        this.cUSTJOINT = cUSTJOINT;
    }

    public List<CUSTStatus> getCUSTSTATUS() {
        return cUSTSTATUS;
    }

    public void setCUSTSTATUS(List<CUSTStatus> cUSTSTATUS) {
        this.cUSTSTATUS = cUSTSTATUS;
    }

    public List<USERINFO> getUSERINFO() {
        return uSERINFO;
    }

    public void setUSERINFO(List<USERINFO> uSERINFO) {
        this.uSERINFO = uSERINFO;
    }

    public List<DOCCHECKLIST> getDOCCHECKLIST() {
        return dOCCHECKLIST;
    }

    public void setDOCCHECKLIST(List<DOCCHECKLIST> dOCCHECKLIST) {
        this.dOCCHECKLIST = dOCCHECKLIST;
    }

    public List<CUSTDOCUMENT> getCUSTDOCUMENTS() {
        return cUSTDOCUMENTS;
    }

    public void setCUSTDOCUMENTS(List<CUSTDOCUMENT> cUSTDOCUMENTS) {
        this.cUSTDOCUMENTS = cUSTDOCUMENTS;
    }

    public List<Object> getNONINACCOUNTS() {
        return nONINACCOUNTS;
    }

    public void setNONINACCOUNTS(List<Object> nONINACCOUNTS) {
        this.nONINACCOUNTS = nONINACCOUNTS;
    }

    public List<Object> getGUARDIANINFO() {
        return gUARDIANINFO;
    }

    public void setGUARDIANINFO(List<Object> gUARDIANINFO) {
        this.gUARDIANINFO = gUARDIANINFO;
    }

    public List<Object> getNONINDEMOGRAPHICS() {
        return nONINDEMOGRAPHICS;
    }

    public void setNONINDEMOGRAPHICS(List<Object> nONINDEMOGRAPHICS) {
        this.nONINDEMOGRAPHICS = nONINDEMOGRAPHICS;
    }

    public List<Object> getNONINSIGNATORY() {
        return nONINSIGNATORY;
    }

    public void setNONINSIGNATORY(List<Object> nONINSIGNATORY) {
        this.nONINSIGNATORY = nONINSIGNATORY;
    }

    public List<Object> getNONINADDR() {
        return nONINADDR;
    }

    public void setNONINADDR(List<Object> nONINADDR) {
        this.nONINADDR = nONINADDR;
    }

    public List<Object> getNONINCDD() {
        return nONINCDD;
    }

    public void setNONINCDD(List<Object> nONINCDD) {
        this.nONINCDD = nONINCDD;
    }

    public String getPROCESSINGDATE() {
        return pROCESSINGDATE;
    }

    public void setPROCESSINGDATE(String pROCESSINGDATE) {
        this.pROCESSINGDATE = pROCESSINGDATE;
    }

    public Integer getPEPUPDATE() {
        return pEPUPDATE;
    }

    public void setPEPUPDATE(Integer pEPUPDATE) {
        this.pEPUPDATE = pEPUPDATE;
    }

}
