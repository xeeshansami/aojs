package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface SundryCallBack {
    fun SundrySuccess(response: SundryResponse)
    fun SundryFailure(response: BaseResponse)
}