package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.BoolCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.BoolResponse

class BoolBaseHR(callBack: BoolCallBack) : BaseRH<BoolResponse>() {
    var callback: BoolCallBack = callBack
    override fun onSuccess(response: BoolResponse?) {
        response?.let { callback.BoolSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.BoolFailure(it) }
    }
}