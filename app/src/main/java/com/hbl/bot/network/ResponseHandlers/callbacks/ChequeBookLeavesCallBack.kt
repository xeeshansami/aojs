package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ChequeBookLeavesCallBack {
    fun ChequeBookLeavesSuccess(response: ChequeBookLeavesResponse)
    fun ChequeBookLeavesFailure(response: BaseResponse)
}