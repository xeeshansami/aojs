package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PrintDocCallBack {
    fun PrintDocSuccess(response: PrintDocsResponse)
    fun PrintDocFailure(response: BaseResponse)
}