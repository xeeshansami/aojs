package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class RM_INFO() : Serializable, Parcelable {
    @SerializedName("RM_CODE")
    @Expose
    val RM_CODE: String? = null

    @SerializedName("RM_NAME")
    @Expose
    val RM_NAME: String? = null

    @SerializedName("RM_BRANCH")
    @Expose
    val RM_BRANCH: String? = null

    @SerializedName("DATE_TIME")
    @Expose
    val DATE_TIME: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<RM_INFO> {
        override fun createFromParcel(parcel: Parcel): RM_INFO {
            return RM_INFO(parcel)
        }

        override fun newArray(size: Int): Array<RM_INFO?> {
            return arrayOfNulls(size)
        }
    }


}