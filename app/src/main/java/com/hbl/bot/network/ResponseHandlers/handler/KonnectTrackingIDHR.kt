package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class KonnectTrackingIDHR(callBack: KonnectTrackingIDCallBack) : BaseRH<KonnectTrackingIDResponse>() {
    var callback: KonnectTrackingIDCallBack = callBack
    override fun onSuccess(response: KonnectTrackingIDResponse?) {
        response?.let { callback.konnectTrackingIDSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.konnectTrackingIDFailure(it) }
    }
}