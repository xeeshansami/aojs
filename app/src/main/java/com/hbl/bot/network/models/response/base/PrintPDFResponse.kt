package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class PrintPDFResponse(
) : Parcelable {
    var data: ArrayList<PrintPDFData> =ArrayList<PrintPDFData>()
    var message: String=""
    var status: String=""

    constructor(parcel: Parcel) : this() {
        message = parcel.readString().toString()
        status = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(message)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PrintPDFResponse> {
        override fun createFromParcel(parcel: Parcel): PrintPDFResponse {
            return PrintPDFResponse(parcel)
        }

        override fun newArray(size: Int): Array<PrintPDFResponse?> {
            return arrayOfNulls(size)
        }
    }
}