package com.hbl.bot.network.models.response.base


import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SignatureResponse {
    @SerializedName("data")
    @Expose
    var data: ArrayList<String>? = null
    @SerializedName("message")
    var message: String = ""
    @SerializedName("status")
    var status: String = ""
}