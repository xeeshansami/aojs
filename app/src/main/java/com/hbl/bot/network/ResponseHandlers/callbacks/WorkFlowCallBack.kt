package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface WorkFlowCallBack {
    fun WorkFlowSuccess(response: WorkFlowResponse)
    fun WorkFlowFailure(response: BaseResponse)
}