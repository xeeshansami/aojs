package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class RiskRatingResponse() : Parcelable {
    var data: ArrayList<RiskRating>? = null
    var message: String? = null
    var status: String? = null

    constructor(parcel: Parcel) : this() {
        message = parcel.readString()
        status = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(message)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RiskRatingResponse> {
        override fun createFromParcel(parcel: Parcel): RiskRatingResponse {
            return RiskRatingResponse(parcel)
        }

        override fun newArray(size: Int): Array<RiskRatingResponse?> {
            return arrayOfNulls(size)
        }
    }
}