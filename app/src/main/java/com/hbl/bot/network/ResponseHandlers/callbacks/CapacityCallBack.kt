package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface CapacityCallBack {
    fun CapacitySuccess(response: CapacityResponse)
    fun CapacityFailure(response: BaseResponse)
}