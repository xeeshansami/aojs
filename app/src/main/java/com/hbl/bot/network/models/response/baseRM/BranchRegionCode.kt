package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class BranchRegionCode() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("Branch_Code")
    @Expose
    var Branch_Code: String? = null

    @SerializedName("Branch_Name")
    @Expose
    var Branch_Name: String? = null

    @SerializedName("Region_Code")
    @Expose
    val Region_Code: String? = null

    @SerializedName("Region_Name")
    @Expose
    val Region_Name: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<BranchRegionCode> {
        override fun createFromParcel(parcel: Parcel): BranchRegionCode {
            return BranchRegionCode(parcel)
        }

        override fun newArray(size: Int): Array<BranchRegionCode?> {
            return arrayOfNulls(size)
        }
    }
    override fun toString(): String {
        return "$Branch_Code - $Branch_Name"
    }
}