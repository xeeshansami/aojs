package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface SubmitToSuperCallBack {
    fun Success(response: SubmitToSuperResponse)
    fun Failure(response: BaseResponse)
}