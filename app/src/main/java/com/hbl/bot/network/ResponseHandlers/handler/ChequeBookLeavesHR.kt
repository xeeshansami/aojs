package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.ChequeBookLeavesCallBack
import com.hbl.bot.network.models.response.base.*

class ChequeBookLeavesHR(callBack: ChequeBookLeavesCallBack) : BaseRH<ChequeBookLeavesResponse>() {
    var callback: ChequeBookLeavesCallBack = callBack
    override fun onSuccess(response: ChequeBookLeavesResponse?) {
        response?.let { callback.ChequeBookLeavesSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ChequeBookLeavesFailure(it) }
    }
}