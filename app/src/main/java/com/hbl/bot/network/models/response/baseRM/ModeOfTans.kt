package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ModeOfTans() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("TRAN_CODE")
    @Expose
    var TRAN_CODE: String? = null

    @SerializedName("TRAN_DESC")
    @Expose
    var TRAN_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<ModeOfTans> {
        override fun createFromParcel(parcel: Parcel): ModeOfTans {
            return ModeOfTans(parcel)
        }

        override fun newArray(size: Int): Array<ModeOfTans?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return TRAN_DESC.toString()
    }
}