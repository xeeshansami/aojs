
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KINCUSTDOCUMENT {

    @SerializedName("DOC_ID")
    @Expose
    private String dOCID = "";
    @SerializedName("DOC_TYPE")
    @Expose
    private String dOCTYPE = "";
    @SerializedName("DOC_NAME")
    @Expose
    private String dOCNAME = "";
    @SerializedName("DOC_PATH")
    @Expose
    private String dOCPATH = "";
    @SerializedName("DOC_STATUS")
    @Expose
    private String dOCSTATUS = "";
    @SerializedName("DOC_MAP")
    @Expose
    private String dOCMAP = "";
    @SerializedName("ADDITIONAL_DOC_MAP")
    @Expose
    private String aDDITIONALDOCMAP = "";

    public String getDOCID() {
        return dOCID;
    }

    public void setDOCID(String dOCID) {
        this.dOCID = dOCID;
    }

    public String getDOCTYPE() {
        return dOCTYPE;
    }

    public void setDOCTYPE(String dOCTYPE) {
        this.dOCTYPE = dOCTYPE;
    }

    public String getDOCNAME() {
        return dOCNAME;
    }

    public void setDOCNAME(String dOCNAME) {
        this.dOCNAME = dOCNAME;
    }

    public String getDOCPATH() {
        return dOCPATH;
    }

    public void setDOCPATH(String dOCPATH) {
        this.dOCPATH = dOCPATH;
    }

    public String getDOCSTATUS() {
        return dOCSTATUS;
    }

    public void setDOCSTATUS(String dOCSTATUS) {
        this.dOCSTATUS = dOCSTATUS;
    }

    public String getDOCMAP() {
        return dOCMAP;
    }

    public void setDOCMAP(String dOCMAP) {
        this.dOCMAP = dOCMAP;
    }

    public String getADDITIONALDOCMAP() {
        return aDDITIONALDOCMAP;
    }

    public void setADDITIONALDOCMAP(String aDDITIONALDOCMAP) {
        this.aDDITIONALDOCMAP = aDDITIONALDOCMAP;
    }

}
