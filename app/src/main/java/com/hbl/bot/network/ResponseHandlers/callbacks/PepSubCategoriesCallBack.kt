package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PepSubCategoriesCallBack {
    fun PepSubCategoriesSuccess(response: PepSubCategoriesResponse)
    fun PepSubCategoriesFailure(response: BaseResponse)
}