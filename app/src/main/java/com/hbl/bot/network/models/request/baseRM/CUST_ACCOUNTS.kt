package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class CUST_ACCOUNTS() : Parcelable {
    var IS_DEBIT_CARD_TYPE_CHANGED = ""
    var IS_DEBIT_CARD = ""
    var NAME_PROSPECTIVE_REMITTER = ""
    var RELATIONSHIP_CODE = ""
    var RELATIONSHIP_DESC = ""
    var IS_PRIORITY = ""
    var PRITY_DESC = ""
    var PRITY_CODE = ""
    var USER_BRANCH_PRIORITY = ""
    var ACCTTYPEDESC = ""
    var ACCT_OPER_TYPE_CODE = ""
    var ACCT_OPER_TYPE_DESC = ""
    var CHEQUEBOOK_LEAVES = ""
    var CHEQUEBOOK_LEAVES_DESC = ""
    var MODE = ""
    var MODE_DESC = ""
    var ACCT_CCY = ""
    var ACCT_NO = ""
    var ACCT_SNG_JNT = ""
    var ACCT_SNG_JNT_DESC = ""
    var ACCT_TYPE = ""
    var BRANCHNAME = ""
    var BRANCH_CODE = ""
    var IBAN = ""

    //    var CURR = ""
    var CIF_NO = ""
    var CONVENTIONAL_ISLAMIC = ""
    var CURRENCY = ""
    var CURRENCY_DESC = ""
    var DEBITCARDTYPEDESC = ""
    var DEBIT_CARD_REQ = ""
    var DEBIT_CARD_TYPE = ""
    var ESTATEMENT = ""
    var ESTATEMENTFREQDESC = ""
    var ESTATEMENT_FREQ = ""
    var FREE_INSURANCE = ""
    var HBL_NISA = ""
    var ID_DOC_NO = ""
    var INITDEPSOURCEDESC = ""
    var INIT_DEPOSIT = ""
    var INIT_DEP_SOURCE = ""
    var INTERNET_BANK = ""
    var NAME_ON_CARD = ""
    var OPERINSTDESC = ""
    var OPER_INSTRUCTION = ""
    var OTHER_PURPOSE_OF_ACCT_DESC = ""
    var PHONE_BANK = ""
    var PREFERREDCOMMUMODEDESC = ""
    var PREFERRED_COMMU_MODE = ""
    var PURPOSEOFACCTDESC = ""
    var PURPOSE_OF_ACCT = ""
    var SUNDRY_ANALYSIS = ""
    var SUNDRY_DESC = ""
    var RM_CODE = ""
    var RM_DESC = ""
    var SMS_ALERT = ""
    var STAFF_ACCT_NO = ""
    var TITLE_OF_ACCT = ""
    var TITLE_SHORT_OF_ACCT = ""
    var ZAKAT_EXEMPT = ""

    constructor(parcel: Parcel) : this() {
        IS_DEBIT_CARD = parcel.readString().toString()
        IS_DEBIT_CARD_TYPE_CHANGED = parcel.readString().toString()
        SUNDRY_ANALYSIS = parcel.readString().toString()
        SUNDRY_DESC = parcel.readString().toString()
        NAME_PROSPECTIVE_REMITTER = parcel.readString().toString()
        RELATIONSHIP_CODE = parcel.readString().toString()
        RELATIONSHIP_DESC = parcel.readString().toString()
        IBAN = parcel.readString().toString()
        MODE = parcel.readString().toString()
        CHEQUEBOOK_LEAVES = parcel.readString().toString()
        CHEQUEBOOK_LEAVES_DESC = parcel.readString().toString()
        PRITY_DESC = parcel.readString().toString()
        PRITY_CODE = parcel.readString().toString()
        IS_PRIORITY = parcel.readString().toString()
        USER_BRANCH_PRIORITY = parcel.readString().toString()
        ACCT_OPER_TYPE_CODE = parcel.readString().toString()
//        CURR /= parcel.readString().toString()
        ACCTTYPEDESC = parcel.readString().toString()
        ACCT_CCY = parcel.readString().toString()
        ACCT_NO = parcel.readString().toString()
        ACCT_SNG_JNT = parcel.readString().toString()
        ACCT_SNG_JNT_DESC = parcel.readString().toString()
        ACCT_TYPE = parcel.readString().toString()
        BRANCHNAME = parcel.readString().toString()
        BRANCH_CODE = parcel.readString().toString()
        CIF_NO = parcel.readString().toString()
        CONVENTIONAL_ISLAMIC = parcel.readString().toString()
        CURRENCY = parcel.readString().toString()
        CURRENCY_DESC = parcel.readString().toString()
        DEBITCARDTYPEDESC = parcel.readString().toString()
        DEBIT_CARD_REQ = parcel.readString().toString()
        DEBIT_CARD_TYPE = parcel.readString().toString()
        ESTATEMENT = parcel.readString().toString()
        ESTATEMENTFREQDESC = parcel.readString().toString()
        ESTATEMENT_FREQ = parcel.readString().toString()
        FREE_INSURANCE = parcel.readString().toString()
        HBL_NISA = parcel.readString().toString()
        ID_DOC_NO = parcel.readString().toString()
        INITDEPSOURCEDESC = parcel.readString().toString()
        INIT_DEPOSIT = parcel.readString().toString()
        INIT_DEP_SOURCE = parcel.readString().toString()
        INTERNET_BANK = parcel.readString().toString()
        OPERINSTDESC = parcel.readString().toString()
        OPER_INSTRUCTION = parcel.readString().toString()
        PHONE_BANK = parcel.readString().toString()
        PREFERREDCOMMUMODEDESC = parcel.readString().toString()
        PREFERRED_COMMU_MODE = parcel.readString().toString()
        PURPOSEOFACCTDESC = parcel.readString().toString()
        PURPOSE_OF_ACCT = parcel.readString().toString()
        RM_CODE = parcel.readString().toString()
        RM_DESC = parcel.readString().toString()
        SMS_ALERT = parcel.readString().toString()
        STAFF_ACCT_NO = parcel.readString().toString()
        TITLE_OF_ACCT = parcel.readString().toString()
        TITLE_SHORT_OF_ACCT = parcel.readString().toString()
        ZAKAT_EXEMPT = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(IS_DEBIT_CARD_TYPE_CHANGED)
        parcel.writeString(IS_DEBIT_CARD)
        parcel.writeString(SUNDRY_DESC)
        parcel.writeString(SUNDRY_ANALYSIS)
        parcel.writeString(NAME_PROSPECTIVE_REMITTER)
        parcel.writeString(RELATIONSHIP_CODE)
        parcel.writeString(RELATIONSHIP_DESC)
        parcel.writeString(IBAN)
        parcel.writeString(MODE)
        parcel.writeString(MODE_DESC)
        parcel.writeString(CHEQUEBOOK_LEAVES)
        parcel.writeString(CHEQUEBOOK_LEAVES_DESC)
        parcel.writeString(IS_PRIORITY)
        parcel.writeString(PRITY_CODE)
        parcel.writeString(PRITY_DESC)
        parcel.writeString(USER_BRANCH_PRIORITY)
        parcel.writeString(ACCT_OPER_TYPE_CODE)
        parcel.writeString(ACCTTYPEDESC)
        parcel.writeString(ACCT_CCY)
//        parcel.writeString(CURR)
        parcel.writeString(ACCT_NO)
        parcel.writeString(ACCT_SNG_JNT)
        parcel.writeString(ACCT_SNG_JNT_DESC)
        parcel.writeString(ACCT_TYPE)
        parcel.writeString(BRANCHNAME)
        parcel.writeString(BRANCH_CODE)
        parcel.writeString(CIF_NO)
        parcel.writeString(CONVENTIONAL_ISLAMIC)
        parcel.writeString(CURRENCY)
        parcel.writeString(CURRENCY_DESC)
        parcel.writeString(DEBITCARDTYPEDESC)
        parcel.writeString(DEBIT_CARD_REQ)
        parcel.writeString(DEBIT_CARD_TYPE)
        parcel.writeString(ESTATEMENT)
        parcel.writeString(ESTATEMENTFREQDESC)
        parcel.writeString(ESTATEMENT_FREQ)
        parcel.writeString(FREE_INSURANCE)
        parcel.writeString(HBL_NISA)
        parcel.writeString(ID_DOC_NO)
        parcel.writeString(INITDEPSOURCEDESC)
        parcel.writeString(INIT_DEPOSIT)
        parcel.writeString(INIT_DEP_SOURCE)
        parcel.writeString(INTERNET_BANK)
        parcel.writeString(OPERINSTDESC)
        parcel.writeString(OPER_INSTRUCTION)
        parcel.writeString(PHONE_BANK)
        parcel.writeString(PREFERREDCOMMUMODEDESC)
        parcel.writeString(PREFERRED_COMMU_MODE)
        parcel.writeString(PURPOSEOFACCTDESC)
        parcel.writeString(PURPOSE_OF_ACCT)
        parcel.writeString(RM_CODE)
        parcel.writeString(RM_DESC)
        parcel.writeString(SMS_ALERT)
        parcel.writeString(STAFF_ACCT_NO)
        parcel.writeString(TITLE_OF_ACCT)
        parcel.writeString(TITLE_SHORT_OF_ACCT)
        parcel.writeString(ZAKAT_EXEMPT)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CUST_ACCOUNTS> {
        override fun createFromParcel(parcel: Parcel): CUST_ACCOUNTS {
            return CUST_ACCOUNTS(parcel)
        }

        override fun newArray(size: Int): Array<CUST_ACCOUNTS?> {
            return arrayOfNulls(size)
        }
    }
}