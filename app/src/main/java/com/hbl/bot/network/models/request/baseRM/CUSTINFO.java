
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CUSTINFO {

    public String getVISA_VALIDITY_DATE() {
        return VISA_VALIDITY_DATE;
    }

    public void setVISA_VALIDITY_DATE(String VISA_VALIDITY_DATE) {
        this.VISA_VALIDITY_DATE = VISA_VALIDITY_DATE;
    }

    @SerializedName("OfficeAddress")
    @Expose
    public String OfficeAddress = "";
    @SerializedName("IndustryMainCategory")
    @Expose
    public String IndustryMainCategory = "";
    @SerializedName("IndustrySubCategory")
    @Expose
    public String IndustrySubCategory = "";
    @SerializedName("OrgCode")
    @Expose
    public String OrgCode = "";
    @SerializedName("NameOfCompanyOrEmployer")
    @Expose
    public String NameOfCompanyOrEmployer = "";
    @SerializedName("VISA_VALIDITY_DATE")
    @Expose
    public String VISA_VALIDITY_DATE = "";
    @SerializedName("CIF_NO")
    @Expose
    public String CIF_NO = "";
    @SerializedName("ETBNTB_CIF_NO")
    @Expose
    public String ETBNTB_CIF_NO = "";
    @SerializedName("CUSTOMER_VISUALLY_IMPAIRED")
    @Expose
    public String CUSTOMER_VISUALLY_IMPAIRED = "";
    @SerializedName("PEP")
    @Expose
    public String PEP = "";
    @SerializedName("TITLE_DESC")
    @Expose
    public String TITLE_DESC = "";
    @SerializedName("TITLE_CODE")
    @Expose
    public String TITLE_CODE = "";
    @SerializedName("FIRST_NAME")
    @Expose
    public String FIRST_NAME = "";
    @SerializedName("MIDDLE_NAME")
    @Expose
    public String MIDDLE_NAME = "";
    @SerializedName("LAST_NAME")
    @Expose
    public String LAST_NAME = "";
    @SerializedName("FULL_NAME")
    @Expose
    public String FULL_NAME = "";
    @SerializedName("FATHER_NAME")
    @Expose
    public String FATHER_NAME = "";
    @SerializedName("HUSBAND_NAME")
    @Expose
    public String HUSBAND_NAME = "";
    @SerializedName("DATE_OF_BIRTH")
    @Expose
    public String DATE_OF_BIRTH = "";
    @SerializedName("MARITAL_STATUS")
    @Expose
    public String MARITAL_STATUS = "";
    @SerializedName("MARITALDESC")
    @Expose
    public String MARITALDESC = "";
    @SerializedName("GENDER")
    @Expose
    public String GENDER = "";
    @SerializedName("GENDERDESC")
    @Expose
    public String GENDERDESC = "";
    @SerializedName("MOTHER_NAME")
    @Expose
    public String MOTHER_NAME = "";
    @SerializedName("SBP_CODE")
    @Expose
    public String SBP_CODE = "";
    @SerializedName("SBP_DESC")
    @Expose
    public String SBP_DESC = "";
    @SerializedName("RM_CODE")
    @Expose
    public String RM_CODE = "";
    @SerializedName("RM_DESC")
    @Expose
    public String RM_DESC = "";
    @SerializedName("SUNDRY_ANALYSIS")
    @Expose
    public String SUNDRY_ANALYSIS = "";
    @SerializedName("SUNDRYDESC")
    @Expose
    public String SUNDRYDESC = "";
    @SerializedName("ID_DOCUMENT_TYPE")
    @Expose
    public String ID_DOCUMENT_TYPE = "";
    @SerializedName("ID_DOCUMENT_TYPE_DESC")
    @Expose
    public String ID_DOCUMENT_TYPE_DESC = "";
    @SerializedName("ID_DOCUMENT_NO")
    @Expose
    public String ID_DOCUMENT_NO = "";
    @SerializedName("ISSUE_DATE")
    @Expose
    public String ISSUE_DATE = "";
    @SerializedName("EXPIRY_DATE")
    @Expose
    public String EXPIRY_DATE = "";
    @SerializedName("NTN")
    @Expose
    public String NTN = "";
    @SerializedName("NATIONALITY")
    @Expose
    public String NATIONALITY = "";
    @SerializedName("NATIONALITYDESC")
    @Expose
    public String NATIONALITYDESC = "";
    @SerializedName("DUAL_NATIONALITY")
    @Expose
    public String DUAL_NATIONALITY = "";
    @SerializedName("SECOND_NATIONALITY")
    @Expose
    public String SECOND_NATIONALITY = "";
    @SerializedName("SECONDNATIONALITYDESC")
    @Expose
    public String SECONDNATIONALITYDESC = "";
    @SerializedName("COUNTRY_OF_BIRTH")
    @Expose
    public String COUNTRY_OF_BIRTH = "";
    @SerializedName("COUNTRYOFBIRTHDESC")
    @Expose
    public String COUNTRYOFBIRTHDESC = "";
    @SerializedName("CITY_OF_BIRTH")
    @Expose
    public String CITY_OF_BIRTH = "";
    @SerializedName("CITYOFBIRTHDESC")
    @Expose
    public String CITYOFBIRTHDESC = "";
    @SerializedName("COUNTRY_OF_RES")
    @Expose
    public String COUNTRY_OF_RES = "";
    @SerializedName("COUNTRYOFRESDESC")
    @Expose
    public String COUNTRYOFRESDESC = "";
    @SerializedName("INTERNET_MOB_BNK_REQ")
    @Expose
    public String INTERNET_MOB_BNK_REQ = "";
    @SerializedName("TAX_PAYER_OTHER_COUNTRY")
    @Expose
    public String TAX_PAYER_OTHER_COUNTRY = "";
    @SerializedName("TAX_PAYER_BOOl")
    @Expose
    public String TAX_PAYER_BOOl = "";
    @SerializedName("FATCA")
    @Expose
    public Integer FATCA = 0;
    @SerializedName("US_ADDRESS")
    @Expose
    public String US_ADDRESS = "";
    @SerializedName("US_ADDRESS_2")
    @Expose
    public String US_ADDRESS_2 = "";
    @SerializedName("US_TELEPHONE")
    @Expose
    public String US_TELEPHONE = "";
    @SerializedName("EXPIRY_DATE_OF_RES_CARD")
    @Expose
    public String EXPIRY_DATE_OF_RES_CARD = "";
    @SerializedName("IRS_FORM_W8_SUBMITTED")
    @Expose
    public String IRS_FORM_W8_SUBMITTED = "";
    @SerializedName("IRS_FORM_W9_SUBMITTED")
    @Expose
    public String IRS_FORM_W9_SUBMITTED = "";
    @SerializedName("TIN")
    @Expose
    public String TIN = "";
    @SerializedName("SPEND_DAYS")
    @Expose
    public String SPEND_DAYS = "";
    @SerializedName("RENOUNCED_US_CITIZENSHIP")
    @Expose
    public String RENOUNCED_US_CITIZENSHIP = "";
    @SerializedName("FATCA_COUNTRY_CODE")
    @Expose
    public String FATCA_COUNTRY_CODE = "";
    @SerializedName("FATCA_COUNTRY_DESC")
    @Expose
    public String FATCA_COUNTRY_DESC = "";
    @SerializedName("TAX_RES_COUNTRY")
    @Expose
    public String TAX_RES_COUNTRY = "";
    @SerializedName("TAX_RES_COUNTRY_NAME")
    @Expose
    public String TAX_RES_COUNTRY_NAME = "";
    @SerializedName("NAME_OF_COUNTRY1")
    @Expose
    public String NAME_OF_COUNTRY1 = "";
    @SerializedName("NAMEOFCOUNTRYDESC1")
    @Expose
    public String NAMEOFCOUNTRYDESC1 = "";
    @SerializedName("TIN1")
    @Expose
    public String TIN1 = "";
    @SerializedName("REASON_IN_CASE_OF_NO_TIN1")
    @Expose
    public String REASON_IN_CASE_OF_NO_TIN1 = "";
    @SerializedName("COMMENT_IN_CASE_OF_NO_TIN1")
    @Expose
    public String COMMENT_IN_CASE_OF_NO_TIN1 = "";
    @SerializedName("NAME_OF_COUNTRY2")
    @Expose
    public String NAME_OF_COUNTRY2 = "";
    @SerializedName("NAMEOFCOUNTRYDESC2")
    @Expose
    public String NAMEOFCOUNTRYDESC2 = "";
    @SerializedName("TIN2")
    @Expose
    public String TIN2 = "";
    @SerializedName("REASON_IN_CASE_OF_NO_TIN2")
    @Expose
    public String REASON_IN_CASE_OF_NO_TIN2 = "";
    @SerializedName("COMMENT_IN_CASE_OF_NO_TIN2")
    @Expose
    public String COMMENT_IN_CASE_OF_NO_TIN2 = "";
    @SerializedName("NAME_OF_COUNTRY3")
    @Expose
    public String NAME_OF_COUNTRY3 = "";
    @SerializedName("NAMEOFCOUNTRYDESC3")
    @Expose
    public String NAMEOFCOUNTRYDESC3 = "";
    @SerializedName("TIN3")
    @Expose
    public String TIN3 = "";
    @SerializedName("REASON_IN_CASE_OF_NO_TIN3")
    @Expose
    public String REASON_IN_CASE_OF_NO_TIN3 = "";
    @SerializedName("COMMENT_IN_CASE_OF_NO_TIN3")
    @Expose
    public String COMMENT_IN_CASE_OF_NO_TIN3 = "";
    @SerializedName("OVERSEAS_CARD_NUMBER")
    @Expose
    public String OVERSEAS_CARD_NUMBER = "";
    @SerializedName("EXPIRY_DATE_OF_OVERSEAS_CARD")
    @Expose
    public String EXPIRY_DATE_OF_OVERSEAS_CARD = "";
    @SerializedName("DATE_OF_SIGNED")
    @Expose
    public String DATE_OF_SIGNED = "";
    @SerializedName("CAPACITY_CODE")
    @Expose
    public String CAPACITY_CODE = "";
    @SerializedName("CAPACITY_DESC")
    @Expose
    public String CAPACITY_DESC = "";
    @SerializedName("SECOND_NATIONALITY_ID")
    @Expose
    public String SECOND_NATIONALITY_ID = "";
    @SerializedName("PASSPORT_EXPIRATION_DATE")
    @Expose
    public String PASSPORT_EXPIRATION_DATE = "";
    @SerializedName("TAX_PAYER_BIRTH_CITY_CODE")
    @Expose
    public String TAX_PAYER_BIRTH_CITY_CODE = "";
    @SerializedName("TAX_PAYER_BIRTH_CITY_NAME")
    @Expose
    public String TAX_PAYER_BIRTH_CITY_NAME = "";
    @SerializedName("DATE_CRS_FORM_SIGNED")
    @Expose
    public String DATE_CRS_FORM_SIGNED = "";
    @SerializedName("GOVERMENT_DOCUMENT_INDICATOR")
    @Expose
    public String GOVERMENT_DOCUMENT_INDICATOR = "";
    @SerializedName("CONSUMER_PRODUCT")
    @Expose
    public String CONSUMER_PRODUCT = "";
    @SerializedName("CONSUMER_CODE")
    @Expose
    public String CONSUMER_CODE = "";
    @SerializedName("CONSUMERDESC")
    @Expose
    public String CONSUMERDESC = "";
    @SerializedName("IS_FORCE_CRS")
    @Expose
    public String IS_FORCE_CRS = "";
    @SerializedName("US_RES_CARD_NO")
    @Expose
    public String US_RES_CARD_NO = "";

    public String getCIFNO() {
        return CIF_NO;
    }

    public void setCIFNO(String cIFNO) {
        this.CIF_NO = cIFNO;
    }

    public String getETBNTBCIFNO() {
        return ETBNTB_CIF_NO;
    }

    public void setETBNTBCIFNO(String eTBNTBCIFNO) {
        this.ETBNTB_CIF_NO = eTBNTBCIFNO;
    }

    public String getCUSTOMERVISUALLYIMPAIRED() {
        return CUSTOMER_VISUALLY_IMPAIRED;
    }

    public void setCUSTOMERVISUALLYIMPAIRED(String cUSTOMERVISUALLYIMPAIRED) {
        this.CUSTOMER_VISUALLY_IMPAIRED = cUSTOMERVISUALLYIMPAIRED;
    }

    public String getPEP() {
        return PEP;
    }

    public void setPEP(String pEP) {
        this.PEP = pEP;
    }

    public String getTITLEDESC() {
        return TITLE_DESC;
    }

    public void setTITLEDESC(String tITLEDESC) {
        this.TITLE_DESC = tITLEDESC;
    }

    public String getTITLECODE() {
        return TITLE_CODE;
    }

    public void setTITLECODE(String tITLECODE) {
        this.TITLE_CODE = tITLECODE;
    }

    public String getFIRSTNAME() {
        return FIRST_NAME;
    }

    public void setFIRSTNAME(String fIRSTNAME) {
        this.FIRST_NAME = fIRSTNAME;
    }

    public String getMIDDLENAME() {
        return MIDDLE_NAME;
    }

    public void setMIDDLENAME(String mIDDLENAME) {
        this.MIDDLE_NAME = mIDDLENAME;
    }

    public String getLASTNAME() {
        return LAST_NAME;
    }

    public void setLASTNAME(String lASTNAME) {
        this.LAST_NAME = lASTNAME;
    }

    public String getFATHERNAME() {
        return FATHER_NAME;
    }

    public void setFATHERNAME(String fATHERNAME) {
        this.FATHER_NAME = fATHERNAME;
    }

    public String getHUSBANDNAME() {
        return HUSBAND_NAME;
    }

    public void setHUSBANDNAME(String hUSBANDNAME) {
        this.HUSBAND_NAME = hUSBANDNAME;
    }

    public String getDATEOFBIRTH() {
        return DATE_OF_BIRTH;
    }

    public void setDATEOFBIRTH(String dATEOFBIRTH) {
        this.DATE_OF_BIRTH = dATEOFBIRTH;
    }

    public String getMARITALSTATUS() {
        return MARITAL_STATUS;
    }

    public void setMARITALSTATUS(String mARITALSTATUS) {
        this.MARITAL_STATUS = mARITALSTATUS;
    }

    public String getMARITALDESC() {
        return MARITALDESC;
    }

    public void setMARITALDESC(String mARITALDESC) {
        this.MARITALDESC = mARITALDESC;
    }

    public String getGENDER() {
        return GENDER;
    }

    public void setGENDER(String gENDER) {
        this.GENDER = gENDER;
    }

    public String getGENDERDESC() {
        return GENDERDESC;
    }

    public void setGENDERDESC(String gENDERDESC) {
        this.GENDERDESC = gENDERDESC;
    }

    public String getMOTHERNAME() {
        return MOTHER_NAME;
    }

    public void setMOTHERNAME(String mOTHERNAME) {
        this.MOTHER_NAME = mOTHERNAME;
    }

    public String getSBPCODE() {
        return SBP_CODE;
    }

    public void setSBPCODE(String sBPCODE) {
        this.SBP_CODE = sBPCODE;
    }

    public String getSBPDESC() {
        return SBP_DESC;
    }

    public void setSBPDESC(String sBPDESC) {
        this.SBP_DESC = sBPDESC;
    }

    public String getRMCODE() {
        return RM_CODE;
    }

    public void setRMCODE(String rMCODE) {
        this.RM_CODE = rMCODE;
    }

    public String getRMDESC() {
        return RM_DESC;
    }

    public void setRMDESC(String rMDESC) {
        this.RM_DESC = rMDESC;
    }

    public String getSUNDRYANALYSIS() {
        return SUNDRY_ANALYSIS;
    }

    public void setSUNDRYANALYSIS(String sUNDRYANALYSIS) {
        this.SUNDRY_ANALYSIS = sUNDRYANALYSIS;
    }

    public String getSUNDRYDESC() {
        return SUNDRYDESC;
    }

    public void setSUNDRYDESC(String sUNDRYDESC) {
        this.SUNDRYDESC = sUNDRYDESC;
    }

    public String getIDDOCUMENTTYPE() {
        return ID_DOCUMENT_TYPE;
    }

    public void setIDDOCUMENTTYPE(String iDDOCUMENTTYPE) {
        this.ID_DOCUMENT_TYPE = iDDOCUMENTTYPE;
    }

    public String getIDDOCUMENTTYPEDESC() {
        return ID_DOCUMENT_TYPE_DESC;
    }

    public void setIDDOCUMENTTYPEDESC(String iDDOCUMENTTYPEDESC) {
        this.ID_DOCUMENT_TYPE_DESC = iDDOCUMENTTYPEDESC;
    }

    public String getIDDOCUMENTNO() {
        return ID_DOCUMENT_NO;
    }

    public void setIDDOCUMENTNO(String iDDOCUMENTNO) {
        this.ID_DOCUMENT_NO = iDDOCUMENTNO;
    }

    public String getISSUEDATE() {
        return ISSUE_DATE;
    }

    public void setISSUEDATE(String iSSUEDATE) {
        this.ISSUE_DATE = iSSUEDATE;
    }

    public String getEXPIRYDATE() {
        return EXPIRY_DATE;
    }

    public void setEXPIRYDATE(String eXPIRYDATE) {
        this.EXPIRY_DATE = eXPIRYDATE;
    }

    public String getNTN() {
        return NTN;
    }

    public void setNTN(String nTN) {
        this.NTN = nTN;
    }

    public String getNATIONALITY() {
        return NATIONALITY;
    }

    public void setNATIONALITY(String nATIONALITY) {
        this.NATIONALITY = nATIONALITY;
    }

    public String getNATIONALITYDESC() {
        return NATIONALITYDESC;
    }

    public void setNATIONALITYDESC(String nATIONALITYDESC) {
        this.NATIONALITYDESC = nATIONALITYDESC;
    }

    public String getDUALNATIONALITY() {
        return DUAL_NATIONALITY;
    }

    public void setDUALNATIONALITY(String dUALNATIONALITY) {
        this.DUAL_NATIONALITY = dUALNATIONALITY;
    }

    public String getSECONDNATIONALITY() {
        return SECOND_NATIONALITY;
    }

    public void setSECONDNATIONALITY(String sECONDNATIONALITY) {
        this.SECOND_NATIONALITY = sECONDNATIONALITY;
    }

    public String getSECONDNATIONALITYDESC() {
        return SECONDNATIONALITYDESC;
    }

    public void setSECONDNATIONALITYDESC(String sECONDNATIONALITYDESC) {
        this.SECONDNATIONALITYDESC = sECONDNATIONALITYDESC;
    }

    public String getCOUNTRYOFBIRTH() {
        return COUNTRY_OF_BIRTH;
    }

    public void setCOUNTRYOFBIRTH(String cOUNTRYOFBIRTH) {
        this.COUNTRY_OF_BIRTH = cOUNTRYOFBIRTH;
    }

    public String getCOUNTRYOFBIRTHDESC() {
        return COUNTRYOFBIRTHDESC;
    }

    public void setCOUNTRYOFBIRTHDESC(String cOUNTRYOFBIRTHDESC) {
        this.COUNTRYOFBIRTHDESC = cOUNTRYOFBIRTHDESC;
    }

    public String getCITYOFBIRTH() {
        return CITY_OF_BIRTH;
    }

    public void setCITYOFBIRTH(String cITYOFBIRTH) {
        this.CITY_OF_BIRTH = cITYOFBIRTH;
    }

    public String getCITYOFBIRTHDESC() {
        return CITYOFBIRTHDESC;
    }

    public void setCITYOFBIRTHDESC(String cITYOFBIRTHDESC) {
        this.CITYOFBIRTHDESC = cITYOFBIRTHDESC;
    }

    public String getCOUNTRYOFRES() {
        return COUNTRY_OF_RES;
    }

    public void setCOUNTRYOFRES(String cOUNTRYOFRES) {
        this.COUNTRY_OF_RES = cOUNTRYOFRES;
    }

    public String getCOUNTRYOFRESDESC() {
        return COUNTRYOFRESDESC;
    }

    public void setCOUNTRYOFRESDESC(String cOUNTRYOFRESDESC) {
        this.COUNTRYOFRESDESC = cOUNTRYOFRESDESC;
    }

    public String getINTERNETMOBBNKREQ() {
        return INTERNET_MOB_BNK_REQ;
    }

    public void setINTERNETMOBBNKREQ(String iNTERNETMOBBNKREQ) {
        this.INTERNET_MOB_BNK_REQ = iNTERNETMOBBNKREQ;
    }

    public String getTAXPAYEROTHERCOUNTRY() {
        return TAX_PAYER_OTHER_COUNTRY;
    }

    public void setTAXPAYEROTHERCOUNTRY(String tAXPAYEROTHERCOUNTRY) {
        this.TAX_PAYER_OTHER_COUNTRY = tAXPAYEROTHERCOUNTRY;
    }

    public String getTAXPAYERBOOl() {
        return TAX_PAYER_BOOl;
    }

    public void setTAXPAYERBOOl(String tAXPAYERBOOl) {
        this.TAX_PAYER_BOOl = tAXPAYERBOOl;
    }

    public Integer getFATCA() {
        return FATCA;
    }

    public void setFATCA(Integer fATCA) {
        this.FATCA = fATCA;
    }

    public String getUSADDRESS() {
        return US_ADDRESS;
    }

    public void setUSADDRESS(String uSADDRESS) {
        this.US_ADDRESS = uSADDRESS;
    }

    public String getUSADDRESS2() {
        return US_ADDRESS_2;
    }

    public void setUSADDRESS2(String uSADDRESS2) {
        this.US_ADDRESS_2 = uSADDRESS2;
    }

    public String getUSTELEPHONE() {
        return US_TELEPHONE;
    }

    public void setUSTELEPHONE(String uSTELEPHONE) {
        this.US_TELEPHONE = uSTELEPHONE;
    }

    public String getEXPIRYDATEOFRESCARD() {
        return EXPIRY_DATE_OF_RES_CARD;
    }

    public void setEXPIRYDATEOFRESCARD(String eXPIRYDATEOFRESCARD) {
        this.EXPIRY_DATE_OF_RES_CARD = eXPIRYDATEOFRESCARD;
    }

    public String getIRSFORMW8SUBMITTED() {
        return IRS_FORM_W8_SUBMITTED;
    }

    public void setIRSFORMW8SUBMITTED(String iRSFORMW8SUBMITTED) {
        this.IRS_FORM_W8_SUBMITTED = iRSFORMW8SUBMITTED;
    }

    public String getIRSFORMW9SUBMITTED() {
        return IRS_FORM_W9_SUBMITTED;
    }

    public void setIRSFORMW9SUBMITTED(String iRSFORMW9SUBMITTED) {
        this.IRS_FORM_W9_SUBMITTED = iRSFORMW9SUBMITTED;
    }

    public String getTIN() {
        return TIN;
    }

    public void setTIN(String tIN) {
        this.TIN = tIN;
    }

    public String getSPENDDAYS() {
        return SPEND_DAYS;
    }

    public void setSPENDDAYS(String sPENDDAYS) {
        this.SPEND_DAYS = sPENDDAYS;
    }

    public String getRENOUNCEDUSCITIZENSHIP() {
        return RENOUNCED_US_CITIZENSHIP;
    }

    public void setRENOUNCEDUSCITIZENSHIP(String rENOUNCEDUSCITIZENSHIP) {
        this.RENOUNCED_US_CITIZENSHIP = rENOUNCEDUSCITIZENSHIP;
    }

    public String getFATCACOUNTRYCODE() {
        return FATCA_COUNTRY_CODE;
    }

    public void setFATCACOUNTRYCODE(String fATCACOUNTRYCODE) {
        this.FATCA_COUNTRY_CODE = fATCACOUNTRYCODE;
    }

    public String getFATCACOUNTRYDESC() {
        return FATCA_COUNTRY_DESC;
    }

    public void setFATCACOUNTRYDESC(String fATCACOUNTRYDESC) {
        this.FATCA_COUNTRY_DESC = fATCACOUNTRYDESC;
    }

    public String getTAXRESCOUNTRY() {
        return TAX_RES_COUNTRY;
    }

    public void setTAXRESCOUNTRY(String tAXRESCOUNTRY) {
        this.TAX_RES_COUNTRY = tAXRESCOUNTRY;
    }

    public String getTAXRESCOUNTRYNAME() {
        return TAX_RES_COUNTRY_NAME;
    }

    public void setTAXRESCOUNTRYNAME(String tAXRESCOUNTRYNAME) {
        this.TAX_RES_COUNTRY_NAME = tAXRESCOUNTRYNAME;
    }

    public String getNAMEOFCOUNTRY1() {
        return NAME_OF_COUNTRY1;
    }

    public void setNAMEOFCOUNTRY1(String nAMEOFCOUNTRY1) {
        this.NAME_OF_COUNTRY1 = nAMEOFCOUNTRY1;
    }

    public String getNAMEOFCOUNTRYDESC1() {
        return NAMEOFCOUNTRYDESC1;
    }

    public void setNAMEOFCOUNTRYDESC1(String nAMEOFCOUNTRYDESC1) {
        this.NAMEOFCOUNTRYDESC1 = nAMEOFCOUNTRYDESC1;
    }

    public String getTIN1() {
        return TIN1;
    }

    public void setTIN1(String tIN1) {
        this.TIN1 = tIN1;
    }

    public String getREASONINCASEOFNOTIN1() {
        return REASON_IN_CASE_OF_NO_TIN1;
    }

    public void setREASONINCASEOFNOTIN1(String rEASONINCASEOFNOTIN1) {
        this.REASON_IN_CASE_OF_NO_TIN1 = rEASONINCASEOFNOTIN1;
    }

    public String getCOMMENTINCASEOFNOTIN1() {
        return COMMENT_IN_CASE_OF_NO_TIN1;
    }

    public void setCOMMENTINCASEOFNOTIN1(String cOMMENTINCASEOFNOTIN1) {
        this.COMMENT_IN_CASE_OF_NO_TIN1 = cOMMENTINCASEOFNOTIN1;
    }

    public String getNAMEOFCOUNTRY2() {
        return NAME_OF_COUNTRY2;
    }

    public void setNAMEOFCOUNTRY2(String nAMEOFCOUNTRY2) {
        this.NAME_OF_COUNTRY2 = nAMEOFCOUNTRY2;
    }

    public String getNAMEOFCOUNTRYDESC2() {
        return NAMEOFCOUNTRYDESC2;
    }

    public void setNAMEOFCOUNTRYDESC2(String nAMEOFCOUNTRYDESC2) {
        this.NAMEOFCOUNTRYDESC2 = nAMEOFCOUNTRYDESC2;
    }

    public String getTIN2() {
        return TIN2;
    }

    public void setTIN2(String tIN2) {
        this.TIN2 = tIN2;
    }

    public String getREASONINCASEOFNOTIN2() {
        return REASON_IN_CASE_OF_NO_TIN2;
    }

    public void setREASONINCASEOFNOTIN2(String rEASONINCASEOFNOTIN2) {
        this.REASON_IN_CASE_OF_NO_TIN2 = rEASONINCASEOFNOTIN2;
    }

    public String getCOMMENTINCASEOFNOTIN2() {
        return COMMENT_IN_CASE_OF_NO_TIN2;
    }

    public void setCOMMENTINCASEOFNOTIN2(String cOMMENTINCASEOFNOTIN2) {
        this.COMMENT_IN_CASE_OF_NO_TIN2 = cOMMENTINCASEOFNOTIN2;
    }

    public String getNAMEOFCOUNTRY3() {
        return NAME_OF_COUNTRY3;
    }

    public void setNAMEOFCOUNTRY3(String nAMEOFCOUNTRY3) {
        this.NAME_OF_COUNTRY3 = nAMEOFCOUNTRY3;
    }

    public String getNAMEOFCOUNTRYDESC3() {
        return NAMEOFCOUNTRYDESC3;
    }

    public void setNAMEOFCOUNTRYDESC3(String nAMEOFCOUNTRYDESC3) {
        this.NAMEOFCOUNTRYDESC3 = nAMEOFCOUNTRYDESC3;
    }

    public String getTIN3() {
        return TIN3;
    }

    public void setTIN3(String tIN3) {
        this.TIN3 = tIN3;
    }

    public String getREASONINCASEOFNOTIN3() {
        return REASON_IN_CASE_OF_NO_TIN3;
    }

    public void setREASONINCASEOFNOTIN3(String rEASONINCASEOFNOTIN3) {
        this.REASON_IN_CASE_OF_NO_TIN3 = rEASONINCASEOFNOTIN3;
    }

    public String getCOMMENTINCASEOFNOTIN3() {
        return COMMENT_IN_CASE_OF_NO_TIN3;
    }

    public void setCOMMENTINCASEOFNOTIN3(String cOMMENTINCASEOFNOTIN3) {
        this.COMMENT_IN_CASE_OF_NO_TIN3 = cOMMENTINCASEOFNOTIN3;
    }

    public String getOVERSEASCARDNUMBER() {
        return OVERSEAS_CARD_NUMBER;
    }

    public void setOVERSEASCARDNUMBER(String oVERSEASCARDNUMBER) {
        this.OVERSEAS_CARD_NUMBER = oVERSEASCARDNUMBER;
    }

    public String getEXPIRYDATEOFOVERSEASCARD() {
        return EXPIRY_DATE_OF_OVERSEAS_CARD;
    }

    public void setEXPIRYDATEOFOVERSEASCARD(String eXPIRYDATEOFOVERSEASCARD) {
        this.EXPIRY_DATE_OF_OVERSEAS_CARD = eXPIRYDATEOFOVERSEASCARD;
    }

    public String getDATEOFSIGNED() {
        return DATE_OF_SIGNED;
    }

    public void setDATEOFSIGNED(String dATEOFSIGNED) {
        this.DATE_OF_SIGNED = dATEOFSIGNED;
    }

    public String getCAPACITYCODE() {
        return CAPACITY_CODE;
    }

    public void setCAPACITYCODE(String cAPACITYCODE) {
        this.CAPACITY_CODE = cAPACITYCODE;
    }

    public String getCAPACITYDESC() {
        return CAPACITY_DESC;
    }

    public void setCAPACITYDESC(String cAPACITYDESC) {
        this.CAPACITY_DESC = cAPACITYDESC;
    }

    public String getSECONDNATIONALITYID() {
        return SECOND_NATIONALITY_ID;
    }

    public void setSECONDNATIONALITYID(String sECONDNATIONALITYID) {
        this.SECOND_NATIONALITY_ID = sECONDNATIONALITYID;
    }

    public String getPASSPORTEXPIRATIONDATE() {
        return PASSPORT_EXPIRATION_DATE;
    }

    public void setPASSPORTEXPIRATIONDATE(String pASSPORTEXPIRATIONDATE) {
        this.PASSPORT_EXPIRATION_DATE = pASSPORTEXPIRATIONDATE;
    }

    public String getTAXPAYERBIRTHCITYCODE() {
        return TAX_PAYER_BIRTH_CITY_CODE;
    }

    public void setTAXPAYERBIRTHCITYCODE(String tAXPAYERBIRTHCITYCODE) {
        this.TAX_PAYER_BIRTH_CITY_CODE = tAXPAYERBIRTHCITYCODE;
    }

    public String getTAXPAYERBIRTHCITYNAME() {
        return TAX_PAYER_BIRTH_CITY_NAME;
    }

    public void setTAXPAYERBIRTHCITYNAME(String tAXPAYERBIRTHCITYNAME) {
        this.TAX_PAYER_BIRTH_CITY_NAME = tAXPAYERBIRTHCITYNAME;
    }

    public String getDATECRSFORMSIGNED() {
        return DATE_CRS_FORM_SIGNED;
    }

    public void setDATECRSFORMSIGNED(String dATECRSFORMSIGNED) {
        this.DATE_CRS_FORM_SIGNED = dATECRSFORMSIGNED;
    }

    public String getGOVERMENTDOCUMENTINDICATOR() {
        return GOVERMENT_DOCUMENT_INDICATOR;
    }

    public void setGOVERMENTDOCUMENTINDICATOR(String gOVERMENTDOCUMENTINDICATOR) {
        this.GOVERMENT_DOCUMENT_INDICATOR = gOVERMENTDOCUMENTINDICATOR;
    }

    public String getCONSUMERPRODUCT() {
        return CONSUMER_PRODUCT;
    }

    public void setCONSUMERPRODUCT(String cONSUMERPRODUCT) {
        this.CONSUMER_PRODUCT = cONSUMERPRODUCT;
    }

    public String getCONSUMERCODE() {
        return CONSUMER_CODE;
    }

    public void setCONSUMERCODE(String cONSUMERCODE) {
        this.CONSUMER_CODE = cONSUMERCODE;
    }

    public String getCONSUMERDESC() {
        return CONSUMERDESC;
    }

    public void setCONSUMERDESC(String cONSUMERDESC) {
        this.CONSUMERDESC = cONSUMERDESC;
    }

    public String getISFORCECRS() {
        return IS_FORCE_CRS;
    }

    public void setISFORCECRS(String iSFORCECRS) {
        this.IS_FORCE_CRS = iSFORCECRS;
    }

    public String getUSRESCARDNO() {
        return US_RES_CARD_NO;
    }

    public void setUSRESCARDNO(String uSRESCARDNO) {
        this.US_RES_CARD_NO = uSRESCARDNO;
    }

}
