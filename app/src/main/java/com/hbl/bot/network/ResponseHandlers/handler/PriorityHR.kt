package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.PriorityCallBack
import com.hbl.bot.network.models.response.base.*

class PriorityHR(callBack: PriorityCallBack) : BaseRH<PriorityResponse>() {
    var callback: PriorityCallBack = callBack
    override fun onSuccess(response: PriorityResponse?) {
        response?.let { callback.PrioritySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PriorityFailure(it) }
    }
}