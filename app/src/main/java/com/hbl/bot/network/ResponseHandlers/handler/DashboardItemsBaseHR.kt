package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.BoolCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.DashboardItemsCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.BoolResponse
import com.hbl.bot.network.models.response.base.DashboardResponse

class DashboardItemsBaseHR(callBack: DashboardItemsCallBack) : BaseRH<DashboardResponse>() {
    var callback: DashboardItemsCallBack = callBack
    override fun onSuccess(response: DashboardResponse?) {
        response?.let { callback.DashboardItemsSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.DashboardItemsFailure(it) }
    }
}