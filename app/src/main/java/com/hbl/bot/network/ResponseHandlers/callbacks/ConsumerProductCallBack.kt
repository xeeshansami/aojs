package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ConsumerProductCallBack {
    fun ConsumerProductSuccess(response: ConsumerProductResponse)
    fun ConsumerProductFailure(response: BaseResponse)
}