package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class PrintPDFData(
) : Parcelable {
    var PDF_BASE64: String=""

    constructor(parcel: Parcel) : this() {
        PDF_BASE64 = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PDF_BASE64)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PrintPDFData> {
        override fun createFromParcel(parcel: Parcel): PrintPDFData {
            return PrintPDFData(parcel)
        }

        override fun newArray(size: Int): Array<PrintPDFData?> {
            return arrayOfNulls(size)
        }
    }
}