/*
Copyright (c) 2020 Kotlin CustomerAofAccountInfo Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */
package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CustomerAccounts() : Serializable, Parcelable {
    @SerializedName("BRANCH_CODE")
    var BRANCH_CODE: String? = null

    @SerializedName("BRANCHNAME")
    var BRANCHNAME: String? = null

    @SerializedName("ACCT_NO")
    var ACCT_NO: String? = null

    @SerializedName("STAFF_ACCT_NO")
    var STAFF_ACCT_NO: String? = null

    @SerializedName("TITLE_OF_ACCT")
    var TITLE_OF_ACCT: String? = null

    @SerializedName("TITLE_SHORT_OF_ACCT")
    var TITLE_SHORT_OF_ACCT: String? = null

    @SerializedName("ACCT_TYPE")
    var ACCT_TYPE: String? = null

    @SerializedName("ACCTTYPEDESC")
    var ACCTTYPEDESC: String? = null

    @SerializedName("ACCT_CCY")
    var ACCT_CCY: String? = null

    @SerializedName("CURRENCY")
    var CURRENCY: String? = null

    @SerializedName("CURRENCY_DESC")
    var CURRENCY_DESC: String? = null

    @SerializedName("INIT_DEP_SOURCE")
    var INIT_DEP_SOURCE: String? = null

    @SerializedName("INITDEPSOURCEDESC")
    var INITDEPSOURCEDESC: String? = null

    @SerializedName("INIT_DEPOSIT")
    var INIT_DEPOSIT: String? = null

    @SerializedName("SMS_ALERT")
    var SMS_ALERT: String? = null

    @SerializedName("INTERNET_BANK")
    var INTERNET_BANK: String? = null

    @SerializedName("PHONE_BANK")
    var PHONE_BANK: String? = null

    @SerializedName("ESTATEMENT")
    var ESTATEMENT: String? = null

    @SerializedName("ESTATEMENT_FREQ")
    var ESTATEMENT_FREQ: String? = null

    @SerializedName("ESTATEMENTFREQDESC")
    var ESTATEMENTFREQDESC: String? = null

    @SerializedName("DEBIT_CARD_REQ")
    var DEBIT_CARD_REQ: String? = null

    @SerializedName("DEBIT_CARD_TYPE")
    var DEBIT_CARD_TYPE: String? = null

    @SerializedName("DEBITCARDTYPEDESC")
    var DEBITCARDTYPEDESC: String? = null

    @SerializedName("NAME_ON_CARD")
    var NAME_ON_CARD: String? = null

    @SerializedName("ZAKAT_EXEMPT")
    var ZAKAT_EXEMPT: String? = null

    @SerializedName("FREE_INSURANCE")
    var FREE_INSURANCE: String? = null

    @SerializedName("PURPOSE_OF_ACCT")
    var PURPOSE_OF_ACCT: String? = null

    @SerializedName("PURPOSEOFACCTDESC")
    var PURPOSEOFACCTDESC: String? = null

    @SerializedName("OTHER_PURPOSE_OF_ACCT_DESC")
    var OTHER_PURPOSE_OF_ACCT_DESC: String? = null

    @SerializedName("OPER_INSTRUCTION")
    var OPER_INSTRUCTION: String? = null

    @SerializedName("OPERINSTDESC")
    var OPERINSTDESC: String? = null

    @SerializedName("ID_DOC_NO")
    var ID_DOC_NO: String? = null

    @SerializedName("CIF_NO")
    var CIF_NO: String? = null

    @SerializedName("ACCT_SNG_JNT")
    var ACCT_SNG_JNT: String? = null

    @SerializedName("ACCT_SNG_JNT_DESC")
    var ACCT_SNG_JNT_DESC: String? = null

    @SerializedName("RM_CODE")
    var RM_CODE: String? = null

    @SerializedName("RM_DESC")
    var RM_DESC: String? = null

    @SerializedName("PREFERRED_COMMU_MODE")
    var PREFERRED_COMMU_MODE: String? = null

    constructor(source: Parcel) : this(
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<CustomerAccounts> =
            object : Parcelable.Creator<CustomerAccounts> {
                override fun createFromParcel(source: Parcel): CustomerAccounts =
                    CustomerAccounts(
                        source
                    )

                override fun newArray(size: Int): Array<CustomerAccounts?> =
                    arrayOfNulls(size)
            }
    }
}
