package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class MaritalHR(callBack: MaritalCallBack) : BaseRH<MaritalResponse>() {
    var callback: MaritalCallBack = callBack
    override fun onSuccess(response: MaritalResponse?) {
        response?.let { callback.MaritalSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.MaritalFailure(it) }
    }
}