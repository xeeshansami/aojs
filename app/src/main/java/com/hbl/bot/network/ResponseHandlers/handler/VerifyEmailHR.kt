package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.VerifyEmailCallBack
import com.hbl.bot.network.models.response.base.*

class VerifyEmailHR(callBack: VerifyEmailCallBack) : BaseRH<VerifyEmailResponse>() {
    var callback: VerifyEmailCallBack = callBack
    override fun onSuccess(response: VerifyEmailResponse?) {
        response?.let { callback.VerifyEmailSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.VerifyEmailFailure(it) }
    }
}