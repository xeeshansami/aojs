package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.NadraVerifyResponse

interface NadraVerifyCallBack {
    fun NadraVerifySuccess(response: NadraVerifyResponse)
    fun NadraVerifyFailure(response: BaseResponse)
}