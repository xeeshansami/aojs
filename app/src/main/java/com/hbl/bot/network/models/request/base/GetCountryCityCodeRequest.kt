package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable

class GetCountryCityCodeRequest(
) : Parcelable {
    var find: FindCountryCityCode = FindCountryCityCode()
    var identifier: String? = null

    constructor(parcel: Parcel) : this() {
        identifier = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(identifier)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GetCountryCityCodeRequest> {
        override fun createFromParcel(parcel: Parcel): GetCountryCityCodeRequest {
            return GetCountryCityCodeRequest(parcel)
        }

        override fun newArray(size: Int): Array<GetCountryCityCodeRequest?> {
            return arrayOfNulls(size)
        }
    }
}