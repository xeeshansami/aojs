package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class RMCodesHR(callBack: RMCodesCallBack) : BaseRH<RMCodesResponse>() {
    var callback: RMCodesCallBack = callBack
    override fun onSuccess(response: RMCodesResponse?) {
        response?.let { callback.RMCodesSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.RMCodesFailure(it) }
    }
}