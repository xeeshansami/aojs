package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class BranchRegionHR(callBack: BranchRegionCallBack) : BaseRH<BranchRegionResponse>() {
    var callback: BranchRegionCallBack = callBack
    override fun onSuccess(response: BranchRegionResponse?) {
        response?.let { callback.Success(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.Failure(it) }
    }
}