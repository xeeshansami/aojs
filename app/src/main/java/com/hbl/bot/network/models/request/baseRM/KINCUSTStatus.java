
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KINCUSTStatus extends Object {

    @SerializedName("USER_ROLE")
    @Expose
    private String uSERROLE = "";
    @SerializedName("USER_ID")
    @Expose
    private String uSERID = "";
    @SerializedName("USER_NAME")
    @Expose
    private String uSERNAME = "";
    @SerializedName("NEW_STATUS")
    @Expose
    private String nEWSTATUS = "";
    @SerializedName("OLD_STATUS")
    @Expose
    private String oLDSTATUS = "";
    @SerializedName("ACTION")
    @Expose
    private String aCTION = "";
    @SerializedName("COMMENTS")
    @Expose
    private String cOMMENTS = "";
    @SerializedName("LOG_DATE")
    @Expose
    private String lOGDATE = "";
    @SerializedName("PROCESSINGDATE")
    @Expose
    private String pROCESSINGDATE = "";
    @SerializedName("BRANCH")
    @Expose
    private String bRANCH = "";
    @SerializedName("REGION")
    @Expose
    private String rEGION = "";

    public String getUSERROLE() {
        return uSERROLE;
    }

    public void setUSERROLE(String uSERROLE) {
        this.uSERROLE = uSERROLE;
    }

    public String getUSERID() {
        return uSERID;
    }

    public void setUSERID(String uSERID) {
        this.uSERID = uSERID;
    }

    public String getUSERNAME() {
        return uSERNAME;
    }

    public void setUSERNAME(String uSERNAME) {
        this.uSERNAME = uSERNAME;
    }

    public String getNEWSTATUS() {
        return nEWSTATUS;
    }

    public void setNEWSTATUS(String nEWSTATUS) {
        this.nEWSTATUS = nEWSTATUS;
    }

    public String getOLDSTATUS() {
        return oLDSTATUS;
    }

    public void setOLDSTATUS(String oLDSTATUS) {
        this.oLDSTATUS = oLDSTATUS;
    }

    public String getACTION() {
        return aCTION;
    }

    public void setACTION(String aCTION) {
        this.aCTION = aCTION;
    }

    public String getCOMMENTS() {
        return cOMMENTS;
    }

    public void setCOMMENTS(String cOMMENTS) {
        this.cOMMENTS = cOMMENTS;
    }

    public String getLOGDATE() {
        return lOGDATE;
    }

    public void setLOGDATE(String lOGDATE) {
        this.lOGDATE = lOGDATE;
    }

    public String getPROCESSINGDATE() {
        return pROCESSINGDATE;
    }

    public void setPROCESSINGDATE(String pROCESSINGDATE) {
        this.pROCESSINGDATE = pROCESSINGDATE;
    }

    public String getBRANCH() {
        return bRANCH;
    }

    public void setBRANCH(String bRANCH) {
        this.bRANCH = bRANCH;
    }

    public String getREGION() {
        return rEGION;
    }

    public void setREGION(String rEGION) {
        this.rEGION = rEGION;
    }

}
