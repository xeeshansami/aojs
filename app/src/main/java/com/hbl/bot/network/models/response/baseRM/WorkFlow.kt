package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class WorkFlow(
) : Parcelable {
    var PUR_DESC = ""
    var WORK_FLOW_CODE = ""
    var _id = ""

    constructor(parcel: Parcel) : this() {
        PUR_DESC = parcel.readString().toString()
        WORK_FLOW_CODE = parcel.readString().toString()
        _id = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PUR_DESC)
        parcel.writeString(WORK_FLOW_CODE)
        parcel.writeString(_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<WorkFlow> {
        override fun createFromParcel(parcel: Parcel): WorkFlow {
            return WorkFlow(parcel)
        }

        override fun newArray(size: Int): Array<WorkFlow?> {
            return arrayOfNulls(size)
        }
    }
}