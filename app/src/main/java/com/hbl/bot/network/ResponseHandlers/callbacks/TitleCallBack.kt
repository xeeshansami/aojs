package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface TitleCallBack {
    fun TitleSuccess(response: TitleResponse)
    fun TitleFailure(response: BaseResponse)
}