package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface SourceOfIncomeCallBack {
    fun SourceOfIncomSuccess(response: SourceOfIncomeResponse)
    fun SourceOfIncomFailure(response: BaseResponse)
}