package com.hbl.bot.network.ViewModels

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.ResponseHandlers.handler.*
import com.hbl.bot.network.apiInterface.APIInterface
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.retrofitBuilder.RetrofitBuilder
import com.hbl.bot.utils.Config
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.ToastUtils

class LovViewModel() : ViewModel() {
    lateinit var purpOfCIFLiveData: MutableLiveData<PurposeCIFResponse>
    lateinit var docsLiveData: MutableLiveData<DocsResponse>
    lateinit var citiesLiveData: MutableLiveData<CityResponse>
    lateinit var boolLiveData: MutableLiveData<BoolResponse>
    lateinit var cusomterSegmentLiveData: MutableLiveData<CustomerSegmentResponse>
    lateinit var allCountriesLiveData: MutableLiveData<CountryResponse>
    lateinit var allSpecificLiveData: MutableLiveData<CountryResponse>
    val globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
    var apiInterface: APIInterface? = null

    init {
        apiInterface =
            GlobalClass.applicationContext?.let {
                RetrofitBuilder.getRetrofitInstance(
                    it,
                    RetrofitEnums.URL_HBL,
                    Config.API_CONNECT_TIMEOUT
                )
            }
        docsLiveData = MutableLiveData()
        boolLiveData = MutableLiveData()
        cusomterSegmentLiveData = MutableLiveData()
        allSpecificLiveData = MutableLiveData()
        allCountriesLiveData = MutableLiveData()
        purpOfCIFLiveData = MutableLiveData()
        citiesLiveData = MutableLiveData()
    }

    fun getSpecificCountries(): MutableLiveData<CountryResponse> {
        return allSpecificLiveData
    }

    fun getAllCountries(): MutableLiveData<CountryResponse> {
        return allCountriesLiveData
    }

    fun getDocs(): MutableLiveData<DocsResponse> {
        return docsLiveData
    }

    fun getPurposeCIF(): MutableLiveData<PurposeCIFResponse> {
        return purpOfCIFLiveData
    }

    fun getCities(): MutableLiveData<CityResponse> {
        return citiesLiveData
    }

    fun getBools(): MutableLiveData<BoolResponse> {
        return boolLiveData
    }

    fun getCustomerSegment(): MutableLiveData<CustomerSegmentResponse> {
        return cusomterSegmentLiveData
    }

    /*call postCity Live Data*/
    suspend fun postCities(
        request: LovRequest,
        activity: FragmentActivity?
    ) {
        globalClass?.showDialog(activity)
//        citiesLiveData.postValue(apiInterface?.getCities(request)?.body())
        /*   apiInterface?.getCities(request)?.enqueue(CityHR(object : CityCallBack {
               override fun CitySuccess(response: CityResponse) {
                   if (!response.data.isNullOrEmpty())
                       citiesLiveData.postValue(response)
               }
   
               override fun CityFailure(response: BaseResponse) {
                   citiesLiveData.postValue(null)
                   ToastUtils.normalShowToast(activity, response.message)
                   globalClass?.hideLoader()
               }
           }))*/
    }

    /*call postPurpOfCIf Live Data*/
    suspend fun postPurpOfCIf(
        request: LovRequest,
        activity: FragmentActivity?
    ) {
//        purpOfCIFLiveData.postValue(apiInterface?.getPurpOfCif(request)?.body())
        globalClass?.hideLoader()

        /*       apiInterface?.getPurpOfCif(request)?.enqueue(PurposeCIFHR(object : PurposeCIFCallBack {
                   override fun PurposeCIFSuccess(response: PurposeCIFResponse) {
       
                   }
       
                   override fun PurposeCIFFailure(response: BaseResponse) {
                       purpOfCIFLiveData.postValue(null)
                       ToastUtils.normalShowToast(activity, response.message)
                       globalClass?.hideLoader()
                   }
               }))*/
    }

    /*call getDocs Live Data*/
    fun postDocs(
        request: LovRequest,
        activity: FragmentActivity?
    ) {
        globalClass?.showDialog(activity)
        apiInterface?.getDocs(request)?.enqueue(DocsBaseHR(object : DocsCallBack {
            override fun DocsSuccess(response: DocsResponse) {
                if (!response.data.isNullOrEmpty())
                    docsLiveData.postValue(response)
            }

            override fun DocsFailure(response: BaseResponse) {
                docsLiveData.postValue(null)
                ToastUtils.normalShowToast(activity, response.message, 1)
                globalClass?.hideLoader()
            }
        }))
    }

    /*call getBools Live Data*/
    fun postBools(
        request: LovRequest,
        activity: FragmentActivity?
    ) {
        apiInterface?.getBools(request)?.enqueue(BoolBaseHR(object : BoolCallBack {
            override fun BoolSuccess(response: BoolResponse) {
                if (!response.data.isNullOrEmpty())
                    boolLiveData.postValue(response)
            }

            override fun BoolFailure(response: BaseResponse) {
                boolLiveData.postValue(null)
                ToastUtils.normalShowToast(activity, response.message, 1)
                globalClass?.hideLoader()
            }

        }))
    }

    /*call getAllCountries Live Data*/
    fun postAllCountries(
        request: LovRequest,
        activity: FragmentActivity?
    ) {
        apiInterface?.getCountries(request)?.enqueue(CountryHR(object : CountryCallBack {
            override fun CountrySuccess(response: CountryResponse) {
                if (!response.data.isNullOrEmpty())
                    allCountriesLiveData.postValue(response)
                globalClass?.hideLoader()
            }

            override fun CountryFailure(response: BaseResponse) {
                allCountriesLiveData.postValue(null)
                ToastUtils.normalShowToast(activity, response.message, 1)
                globalClass?.hideLoader()
            }

        }))
    }

    /*call getCustomerSegment Live Data*/
    fun postCustomerSegment(
        request: LovRequest,
        activity: FragmentActivity?
    ) {
        apiInterface?.getCustomerSegments(request)
            ?.enqueue(CustomerSegmentHR(object : CustomerSegmentCallBack {
                override fun CustomerSegmentSuccess(response: CustomerSegmentResponse) {
                    if (!response.data.isNullOrEmpty())
                        cusomterSegmentLiveData.postValue(response)
                    globalClass?.hideLoader()
                }

                override fun CustomerSegmentFailure(response: BaseResponse) {
                    cusomterSegmentLiveData.postValue(null)
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    globalClass?.hideLoader()
                }

            }))
    }

    /*call getSpecificCountry Live Data*/
    fun postSpecificCountry(
        request: LovRequest,
        activity: FragmentActivity?
    ) {
        apiInterface?.getCountries(request)?.enqueue(CountryHR(object : CountryCallBack {
            override fun CountrySuccess(response: CountryResponse) {
                if (!response.data.isNullOrEmpty())
                    allSpecificLiveData.postValue(response)
                globalClass?.hideLoader()
            }

            override fun CountryFailure(response: BaseResponse) {
                allSpecificLiveData.postValue(null)
                ToastUtils.normalShowToast(activity, response.message, 1)
                globalClass?.hideLoader()
            }
        }))
    }
}