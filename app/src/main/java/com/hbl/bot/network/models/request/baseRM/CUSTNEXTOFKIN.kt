package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class CUSTNEXTOFKIN(
) : Parcelable {
    var ADDRESS_LINE1 = ""
    var ADDRESS_LINE2 = ""
    var ADDRESS_LINE3 = ""
    var ADDRESS_LINE4 = ""
    var CIF_NO = ""
    var CITY = ""
    var CITY_NAME = ""
    var COUNTRY = ""
    var COUNTRY_NAME = ""
    var EXISTING_RELATIONSHIP_HBL = ""
    var ID_DOCUMENT_NO = ""
    var ID_DOCUMENT_TYPE = ""
    var ID_DOCUMENT_TYPE_DESC = ""
    var KIN_NO = "1"
    var NAME = ""
    var NAME_OF_MANDATE_HOLDER = ""
    var POWDER_OF_ATTORNEY = ""
    var POWDER_OF_ATTORNEY_REASON = ""
    var PRIM_PHONE = ""
    var RELATIONSHIP_CODE = ""
    var RELATIONSHIP_DESC = ""

    constructor(parcel: Parcel) : this() {
        ADDRESS_LINE1 = parcel.readString().toString()
        ADDRESS_LINE2 = parcel.readString().toString()
        ADDRESS_LINE3 = parcel.readString().toString()
        ADDRESS_LINE4 = parcel.readString().toString()
        CITY = parcel.readString().toString()
        CITY_NAME = parcel.readString().toString()
        COUNTRY = parcel.readString().toString()
        COUNTRY_NAME = parcel.readString().toString()
        ID_DOCUMENT_TYPE_DESC = parcel.readString().toString()
        KIN_NO = parcel.readString().toString()
        NAME = parcel.readString().toString()
        POWDER_OF_ATTORNEY = parcel.readString().toString()
        PRIM_PHONE = parcel.readString().toString()
        RELATIONSHIP_CODE = parcel.readString().toString()
        RELATIONSHIP_DESC = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ADDRESS_LINE1)
        parcel.writeString(ADDRESS_LINE2)
        parcel.writeString(ADDRESS_LINE3)
        parcel.writeString(ADDRESS_LINE4)
        parcel.writeString(CITY)
        parcel.writeString(CITY_NAME)
        parcel.writeString(COUNTRY)
        parcel.writeString(COUNTRY_NAME)
        parcel.writeString(ID_DOCUMENT_TYPE_DESC)
        parcel.writeString(KIN_NO)
        parcel.writeString(NAME)
        parcel.writeString(POWDER_OF_ATTORNEY)
        parcel.writeString(PRIM_PHONE)
        parcel.writeString(RELATIONSHIP_CODE)
        parcel.writeString(RELATIONSHIP_DESC)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CUSTNEXTOFKIN> {
        override fun createFromParcel(parcel: Parcel): CUSTNEXTOFKIN {
            return CUSTNEXTOFKIN(parcel)
        }

        override fun newArray(size: Int): Array<CUSTNEXTOFKIN?> {
            return arrayOfNulls(size)
        }
    }
}