package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class EstateFrequency() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("E_STATE_FRQ")
    @Expose
    var E_STATE_FRQ: String? = null

    @SerializedName("E_STATE_FRQ_CODE")
    @Expose
    var E_STATE_FRQ_CODE: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<EstateFrequency> {
        override fun createFromParcel(parcel: Parcel): EstateFrequency {
            return EstateFrequency(parcel)
        }

        override fun newArray(size: Int): Array<EstateFrequency?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return E_STATE_FRQ.toString()
    }
}