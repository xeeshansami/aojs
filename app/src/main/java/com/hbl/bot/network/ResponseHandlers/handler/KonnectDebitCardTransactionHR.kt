package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class KonnectDebitCardTransactionHR(callBack: KonnectDebitCardTransactionCallBack) :
    BaseRH<KonnectDebitCardTransactionResponse>() {
    var callback: KonnectDebitCardTransactionCallBack = callBack
    override fun onSuccess(response: KonnectDebitCardTransactionResponse?) {
        response?.let { callback.konnectDebitCardTransactionSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.konnectDebitCardTransactionFailure(it) }
    }
}