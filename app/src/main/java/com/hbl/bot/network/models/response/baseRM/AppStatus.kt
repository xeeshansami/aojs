package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class AppStatus() : Parcelable {
    var Acct: String? = null
    var AcctEdit: String? = null
    var AcctMaint: String? = null
    var AcctMaintEdit: String? = null
    var CIF: String? = null
    var CIFAcct: String? = null
    var CIFAcctEdit: String? = null
    var CIFEdit: String? = null
    var CIFMaint: String? = null
    var CIFMaintEdit: String? = null
    var _id: String? = null

    constructor(parcel: Parcel) : this() {
        Acct = parcel.readString()
        AcctEdit = parcel.readString()
        AcctMaint = parcel.readString()
        AcctMaintEdit = parcel.readString()
        CIF = parcel.readString()
        CIFAcct = parcel.readString()
        CIFAcctEdit = parcel.readString()
        CIFEdit = parcel.readString()
        CIFMaint = parcel.readString()
        CIFMaintEdit = parcel.readString()
        _id = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(Acct)
        parcel.writeString(AcctEdit)
        parcel.writeString(AcctMaint)
        parcel.writeString(AcctMaintEdit)
        parcel.writeString(CIF)
        parcel.writeString(CIFAcct)
        parcel.writeString(CIFAcctEdit)
        parcel.writeString(CIFEdit)
        parcel.writeString(CIFMaint)
        parcel.writeString(CIFMaintEdit)
        parcel.writeString(_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AppStatus> {
        override fun createFromParcel(parcel: Parcel): AppStatus {
            return AppStatus(parcel)
        }

        override fun newArray(size: Int): Array<AppStatus?> {
            return arrayOfNulls(size)
        }
    }
}