package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface DraftCallBack {
    fun DraftSuccess(response: AofAccountInfoResponse)
    fun DraftFailure(response: BaseResponse)
}