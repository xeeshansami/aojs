package com.hbl.bot.network.models.request.baseRM

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class CUSTCDD {
    @SerializedName("IS_OTHER_BANK_ACCOUNT")
    @Expose
    var iS_OTHER_BANK_ACCOUNT = ""

    @SerializedName("IS_FOREGIN_FUND_OW")
    @Expose
    var iS_FOREGIN_FUND_OW = ""

    @SerializedName("IS_FOREIGN_FUND_IW")
    @Expose
    var iS_FOREIGN_FUND_IW = ""

    @SerializedName("EXPECTED_INWARD_MTHLY_INCOME_PKR_1")
    @Expose
    var eXPECTED_INWARD_MTHLY_INCOME_PKR_1 = ""

    @SerializedName("COUNTRY_INWARD_REMITT_1")
    @Expose
    var cOUNTRY_INWARD_REMITT_1 = ""

    @SerializedName("DESC_COUNTRY_INWARD_REMITT_1")
    @Expose
    var dESC_COUNTRY_INWARD_REMITT_1 = ""

    @SerializedName("PURPOSE_INWARD_1")
    @Expose
    var pURPOSE_INWARD_1 = ""

    @SerializedName("EXPECTED_INWARD_MTHLY_INCOME_PKR_2")
    @Expose
    var eXPECTED_INWARD_MTHLY_INCOME_PKR_2 = ""

    @SerializedName("COUNTRY_INWARD_REMITT_2")
    @Expose
    var cOUNTRY_INWARD_REMITT_2 = ""

    @SerializedName("DESC_COUNTRY_INWARD_REMITT_2")
    @Expose
    var dESC_COUNTRY_INWARD_REMITT_2 = ""

    @SerializedName("PURPOSE_INWARD_2")
    @Expose
    var pURPOSE_INWARD_2 = ""

    @SerializedName("EXPECTED_INWARD_MTHLY_INCOME_PKR_3")
    @Expose
    var eXPECTED_INWARD_MTHLY_INCOME_PKR_3 = ""

    @SerializedName("COUNTRY_INWARD_REMITT_3")
    @Expose
    var cOUNTRY_INWARD_REMITT_3 = ""

    @SerializedName("DESC_COUNTRY_INWARD_REMITT_3")
    @Expose
    var dESC_COUNTRY_INWARD_REMITT_3 = ""

    @SerializedName("PURPOSE_INWARD_3")
    @Expose
    var pURPOSE_INWARD_3 = ""

    @SerializedName("EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1")
    @Expose
    var eXPECTED_OUTWARD_MTHLY_INCOME_PKR_1 = ""

    @SerializedName("COUNTRY_OUTWARD_REMITT_1")
    @Expose
    var cOUNTRY_OUTWARD_REMITT_1 = ""

    @SerializedName("DESC_COUNTRY_OUTWARD_REMITT_1")
    @Expose
    var dESC_COUNTRY_OUTWARD_REMITT_1 = ""

    @SerializedName("PURPOSE_OUTWARD_1")
    @Expose
    var pURPOSE_OUTWARD_1 = ""

    @SerializedName("EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2")
    @Expose
    var eXPECTED_OUTWARD_MTHLY_INCOME_PKR_2 = ""

    @SerializedName("COUNTRY_OUTWARD_REMITT_2")
    @Expose
    var cOUNTRY_OUTWARD_REMITT_2 = ""

    @SerializedName("DESC_COUNTRY_OUTWARD_REMITT_2")
    @Expose
    var dESC_COUNTRY_OUTWARD_REMITT_2 = ""

    @SerializedName("PURPOSE_OUTWARD_2")
    @Expose
    var pURPOSE_OUTWARD_2 = ""

    @SerializedName("EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3")
    @Expose
    var eXPECTED_OUTWARD_MTHLY_INCOME_PKR_3 = ""

    @SerializedName("COUNTRY_OUTWARD_REMITT_3")
    @Expose
    var cOUNTRY_OUTWARD_REMITT_3 = ""

    @SerializedName("DESC_COUNTRY_OUTWARD_REMITT_3")
    @Expose
    var dESC_COUNTRY_OUTWARD_REMITT_3 = ""

    @SerializedName("PURPOSE_OUTWARD_3")
    @Expose
    var pURPOSE_OUTWARD_3 = ""

    @SerializedName("NAME_OF_BANK_1")
    @Expose
    var nAME_OF_BANK_1 = ""

    @SerializedName("NAME_OF_BRANCH_1")
    @Expose
    var nAME_OF_BRANCH_1 = ""

    @SerializedName("NAME_OF_BANK_2")
    @Expose
    var nAME_OF_BANK_2 = ""

    @SerializedName("NAME_OF_BRANCH_2")
    @Expose
    var nAME_OF_BRANCH_2 = ""

    @SerializedName("NAME_OF_BANK_3")
    @Expose
    var nAME_OF_BANK_3 = ""

    @SerializedName("NAME_OF_BRANCH_3")
    @Expose
    var nAME_OF_BRANCH_3 = ""

    @SerializedName("SOURCE_OF_INCOME_LANDLORD")
    @Expose
    var sOURCE_OF_INCOME_LANDLORD = ""

    @SerializedName("DESC_SOURCE_OF_INCOME_LANDLORD")
    @Expose
    var dESC_SOURCE_OF_INCOME_LANDLORD = ""

    @SerializedName("OTH_SOURCE_OF_INCOME")
    @Expose
    var oTH_SOURCE_OF_INCOME = ""

    @SerializedName("SEASONAL_ACITIVITY_IN_ACCT")
    @Expose
    var sEASONAL_ACITIVITY_IN_ACCT = ""

    @SerializedName("NATR_EXP_MTH_HIGH_TURN_OVER")
    @Expose
    var nATR_EXP_MTH_HIGH_TURN_OVER = ""

    @SerializedName("AGRI_LAND_PASSBOOK_COPY_OBTN")
    @Expose
    var aGRI_LAND_PASSBOOK_COPY_OBTN = ""

    @SerializedName("AGRI_SOURCE_OF_INCOME")
    @Expose
    var aGRI_SOURCE_OF_INCOME = ""

    @SerializedName("AGRI_REASON")
    @Expose
    var aGRI_REASON = ""

    @SerializedName("DATE_INCP_BUSS_PROF")
    @Expose
    var dATE_INCP_BUSS_PROF = ""

    @SerializedName("DESC_BUSS_ACTY_PRD_SERV")
    @Expose
    var dESC_BUSS_ACTY_PRD_SERV = ""

    @SerializedName("DESC_BUSS_GEO_AREA_OPR_SUPP")
    @Expose
    var dESC_BUSS_GEO_AREA_OPR_SUPP = ""

    @SerializedName("CUST_SRC_OF_INCOME_INDUS")
    @Expose
    var cUST_SRC_OF_INCOME_INDUS = ""

    @SerializedName("DESC_CUST_SRC_OF_INCOME_INDUS")
    @Expose
    var dESC_CUST_SRC_OF_INCOME_INDUS = ""

    @SerializedName("CUST_SRC_OF_INDUS_TYPE")
    @Expose
    var cUST_SRC_OF_INDUS_TYPE = ""

    @SerializedName("DESC_CUST_SRC_OF_INDUS_TYPE")
    @Expose
    var dESC_CUST_SRC_OF_INDUS_TYPE = ""

    @SerializedName("HOLD_MAIL_INSTR")
    @Expose
    var hOLD_MAIL_INSTR = ""

    @SerializedName("FOREIGN_ACCT_NO")
    @Expose
    var fOREIGN_ACCT_NO = ""

    @SerializedName("NAME_COUNTER_PARTY_1")
    @Expose
    var nAME_COUNTER_PARTY_1 = ""

    @SerializedName("PRD_SERV_PRVD_BUSS_TRAN_1")
    @Expose
    var pRD_SERV_PRVD_BUSS_TRAN_1 = ""

    @SerializedName("NAME_COUNTER_PARTY_2")
    @Expose
    var nAME_COUNTER_PARTY_2 = ""

    @SerializedName("PRD_SERV_PRVD_BUSS_TRAN_2")
    @Expose
    var pRD_SERV_PRVD_BUSS_TRAN_2 = ""

    @SerializedName("NAME_COUNTER_PARTY_3")
    @Expose
    var nAME_COUNTER_PARTY_3 = ""

    @SerializedName("PRD_SERV_PRVD_BUSS_TRAN_3")
    @Expose
    var pRD_SERV_PRVD_BUSS_TRAN_3 = ""

    @SerializedName("FILLED_BY")
    @Expose
    var fILLED_BY = ""

    @SerializedName("APPROVED_BY_AOF")
    @Expose
    var aPPROVED_BY_AOF = ""

    @SerializedName("APPROVED_BY_BR_MGR")
    @Expose
    var aPPROVED_BY_BR_MGR = ""

    @SerializedName("DESC_BREIF_REMARKS")
    @Expose
    var dESC_BREIF_REMARKS = ""

    @SerializedName("AREA_OF_OPERATION_SUPP")
    @Expose
    var aREA_OF_OPERATION_SUPP = ""
}