package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface MaritalCallBack {
    fun MaritalSuccess(response: MaritalResponse)
    fun MaritalFailure(response: BaseResponse)
}