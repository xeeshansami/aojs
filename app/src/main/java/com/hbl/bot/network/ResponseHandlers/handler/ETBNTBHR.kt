package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class ETBNTBHR(callBack: ETBNTBCallBack) : BaseRH<ETBNTBResponse>() {
    var callback: ETBNTBCallBack = callBack
    override fun onSuccess(response: ETBNTBResponse?) {
        response?.let { callback.ETBNTBSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ETBNTBFailure(it) }
    }
}