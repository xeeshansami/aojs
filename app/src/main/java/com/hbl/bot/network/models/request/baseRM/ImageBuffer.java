
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageBuffer {

    @SerializedName("id")
    @Expose
    public int id;


    @SerializedName("istagged")
    @Expose
    public boolean istagged;


    @SerializedName("taggedBy")
    @Expose
    public String taggedBy = "";


    @SerializedName("seq")
    @Expose
    public int seq;


}
