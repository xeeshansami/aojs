package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface DashboardItemsCallBack {
    fun DashboardItemsSuccess(response: DashboardResponse)
    fun DashboardItemsFailure(response: BaseResponse)
}