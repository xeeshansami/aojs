package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.GetCountryCityCodeCallBack
import com.hbl.bot.network.models.response.base.*

class GetCountryCityCodeHR(callBack: GetCountryCityCodeCallBack) :
    BaseRH<CountryCityCodeResponse>() {
    var callback: GetCountryCityCodeCallBack = callBack
    override fun onSuccess(response: CountryCityCodeResponse?) {
        response?.let { callback.GetCountryCityCodeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.GetCountryCityCodeFailure(it) }
    }
}