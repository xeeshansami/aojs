package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class ProvinceHR(callBack: ProvinceCallBack) : BaseRH<ProvinceResponse>() {
    var callback: ProvinceCallBack = callBack
    override fun onSuccess(response: ProvinceResponse?) {
        response?.let { callback.ProvinceSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ProvinceFailure(it) }
    }
}