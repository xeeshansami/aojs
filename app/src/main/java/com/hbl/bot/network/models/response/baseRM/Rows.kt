package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Rows() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    var _id: String? = null

    @SerializedName("WORK_FLOW_CODE")
    var WORK_FLOW_CODE: String = ""

    @SerializedName("WORK_FLOW_CODE_DESC")
    var WORK_FLOW_CODE_DESC: String = ""

    @SerializedName("TRACKING_ID")
    @Expose
    var TRACKING_ID: String? = null

    @SerializedName("ID_DOC_NO")
    @Expose
    var ID_DOC_NO: String? = null

    @SerializedName("NAME")
    @Expose
    var NAME: String? = null

    @SerializedName("STATUSDESC")
    @Expose
    var STATUSDESC: String? = null


    @SerializedName("USER_BRANCH")
    @Expose
    var USER_BRANCH: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<Title> {
        override fun createFromParcel(parcel: Parcel): Title {
            return Title(parcel)
        }

        override fun newArray(size: Int): Array<Title?> {
            return arrayOfNulls(size)
        }
    }

}