package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class RiskRating() : Parcelable {
    var RISK_RATING: String? = null
    var RISK_RATING_TOTAL: Int? = null
    var mbresult: String? = null
    var modelBasedScore: Int? = null
    var rbresult: String? = null
    var requestId: Int? = null
    var ruleBasedScore: Int? = null
    var watchListResponse: WatchListResponse? = null

    constructor(parcel: Parcel) : this() {
        RISK_RATING = parcel.readString()
        RISK_RATING_TOTAL = parcel.readValue(Int::class.java.classLoader) as? Int
        mbresult = parcel.readString()
        modelBasedScore = parcel.readValue(Int::class.java.classLoader) as? Int
        rbresult = parcel.readString()
        requestId = parcel.readValue(Int::class.java.classLoader) as? Int
        ruleBasedScore = parcel.readValue(Int::class.java.classLoader) as? Int
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(RISK_RATING)
        parcel.writeValue(RISK_RATING_TOTAL)
        parcel.writeString(mbresult)
        parcel.writeValue(modelBasedScore)
        parcel.writeString(rbresult)
        parcel.writeValue(requestId)
        parcel.writeValue(ruleBasedScore)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<RiskRating> {
        override fun createFromParcel(parcel: Parcel): RiskRating {
            return RiskRating(parcel)
        }

        override fun newArray(size: Int): Array<RiskRating?> {
            return arrayOfNulls(size)
        }
    }
}