package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface AccountOperationsTypeCallBack {
    fun AccountOperationsTypeSuccess(response: AccountOperationsTypeResponse)
    fun AccountOperationsTypeFailure(response: BaseResponse)
}