package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable

class ManualVersysisRequest(
) : Parcelable {
    var BIOTRACKING_ID: String=""
    var BRANCH_ADDRESS: String=""
    var BRANCH_CODE: String=""
    var BRANCH_NAME: String=""
    var BRANCH_TYPE: String=""
    var CHANNEL_ID: String=""
    var CONTACT_NUMBER: String=""
    var DATETIME: String=""
    var ID_DOCUMENT_NO: String=""
    var ISSUE_DATE: String=""
    var MOBILE_NO: String=""
    var NADRA_VERISYS_CHECK: String=""
    var OLDER_DAYS: String=""
    var PROVINCE_CODE: String=""
    var PROVINCE_DESC: String=""
    var ROLE: Int=0
    var SERVICE_TYPE: String=""
    var SESSION_ID: String=""
    var TRACKING_ID: String=""
    var URLTYPE: Any=""
    var USER_ID: String=""

    constructor(parcel: Parcel) : this() {
        BIOTRACKING_ID = parcel.readString().toString()
        BRANCH_ADDRESS = parcel.readString().toString()
        BRANCH_CODE = parcel.readString().toString()
        BRANCH_NAME = parcel.readString().toString()
        BRANCH_TYPE = parcel.readString().toString()
        CHANNEL_ID = parcel.readString().toString()
        CONTACT_NUMBER = parcel.readString().toString()
        DATETIME = parcel.readString().toString()
        ID_DOCUMENT_NO = parcel.readString().toString()
        ISSUE_DATE = parcel.readString().toString()
        MOBILE_NO = parcel.readString().toString()
        NADRA_VERISYS_CHECK = parcel.readString().toString()
        OLDER_DAYS = parcel.readString().toString()
        PROVINCE_CODE = parcel.readString().toString()
        PROVINCE_DESC = parcel.readString().toString()
        ROLE = parcel.readInt()
        SERVICE_TYPE = parcel.readString().toString()
        SESSION_ID = parcel.readString().toString()
        TRACKING_ID = parcel.readString().toString()
        USER_ID = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(BIOTRACKING_ID)
        parcel.writeString(BRANCH_ADDRESS)
        parcel.writeString(BRANCH_CODE)
        parcel.writeString(BRANCH_NAME)
        parcel.writeString(BRANCH_TYPE)
        parcel.writeString(CHANNEL_ID)
        parcel.writeString(CONTACT_NUMBER)
        parcel.writeString(DATETIME)
        parcel.writeString(ID_DOCUMENT_NO)
        parcel.writeString(ISSUE_DATE)
        parcel.writeString(MOBILE_NO)
        parcel.writeString(NADRA_VERISYS_CHECK)
        parcel.writeString(OLDER_DAYS)
        parcel.writeString(PROVINCE_CODE)
        parcel.writeString(PROVINCE_DESC)
        parcel.writeInt(ROLE)
        parcel.writeString(SERVICE_TYPE)
        parcel.writeString(SESSION_ID)
        parcel.writeString(TRACKING_ID)
        parcel.writeString(USER_ID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ManualVersysisRequest> {
        override fun createFromParcel(parcel: Parcel): ManualVersysisRequest {
            return ManualVersysisRequest(parcel)
        }

        override fun newArray(size: Int): Array<ManualVersysisRequest?> {
            return arrayOfNulls(size)
        }
    }
}