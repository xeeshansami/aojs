package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class BranchType() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
     val _id: String? = null

    @SerializedName("BR_CODE")
    @Expose
     var BR_CODE: String? = null

    @SerializedName("BR_TYPE")
    @Expose
     val BR_TYPE: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<BranchType> {
        override fun createFromParcel(parcel: Parcel): BranchType {
            return BranchType(parcel)
        }

        override fun newArray(size: Int): Array<BranchType?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return BR_TYPE.toString()
    }
}