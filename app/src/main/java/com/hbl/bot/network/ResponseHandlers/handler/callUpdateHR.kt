package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class callUpdateHR(callBack: callUpdateCallBack) : BaseRH<UpdateCallResponse>() {
    var callback: callUpdateCallBack = callBack
    override fun onSuccess(response: UpdateCallResponse?) {
        response?.let { callback.callUpdateSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.callUpdateFailure(it) }
    }
}