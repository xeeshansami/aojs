package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface TrackingIDCallBack {
    fun TrackingIDSuccess(response: TrackingIDResponse)
    fun TrackingIDFailure(response: BaseResponse)
}