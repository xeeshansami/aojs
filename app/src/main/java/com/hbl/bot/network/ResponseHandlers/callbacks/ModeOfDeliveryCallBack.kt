package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ModeOfDeliveryCallBack {
    fun ModeOfDeliverySuccess(response: MODResponse)
    fun ModeOfDeliveryFailure(response: BaseResponse)
}