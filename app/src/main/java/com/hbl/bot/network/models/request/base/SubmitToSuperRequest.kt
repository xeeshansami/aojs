/* 
Copyright (c) 2020 Kotlin SubmitData Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */
package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.request.baseRM.*
import java.io.Serializable


class SubmitToSuperRequest() : Serializable, Parcelable {

    @SerializedName("tracking_id")
    var tracking_id: String? = null

    @SerializedName("cust_documents")
    var cust_documents: List<CUSTDOCUMENT>? = null

    @SerializedName("doc_checklist")
    var doc_checklist: List<DOCCHECKLIST>? = null

    @SerializedName("identifier")
    var identifier: String? = null

    @SerializedName("_TYPE")
    var _TYPE: String? = null

    @SerializedName("p_s_doc_list")
    var P_S_DOC_LIST: PDCODSNEW = PDCODSNEW()

    @SerializedName("M_COM_UPLOAD_DOC")
    var COM_UPLOAD_DOC: String? = null

    constructor(parcel: Parcel) : this() {
        tracking_id = parcel.readString()
        identifier = parcel.readString()
        _TYPE = parcel.readString()
        COM_UPLOAD_DOC = parcel.readString()
    }

    override fun writeToParcel(dest: Parcel?, flags: Int) {
    }

    override fun describeContents() = 0

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<SubmitToSuperRequest> =
            object : Parcelable.Creator<SubmitToSuperRequest> {
                override fun createFromParcel(source: Parcel): SubmitToSuperRequest =
                    SubmitToSuperRequest(source)

                override fun newArray(size: Int): Array<SubmitToSuperRequest?> = arrayOfNulls(size)
            }
    }


}