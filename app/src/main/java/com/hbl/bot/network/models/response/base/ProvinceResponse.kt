package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.Province
import java.io.Serializable


class ProvinceResponse() : Serializable, Parcelable {
    @SerializedName("status")
    @Expose
    private val status: String? = null

    @SerializedName("message")
    @Expose
    private val message: String? = null

    @SerializedName("data")
    @Expose
    val data: ArrayList<Province>? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ProvinceResponse> {
        override fun createFromParcel(parcel: Parcel): ProvinceResponse {
            return ProvinceResponse(parcel)
        }

        override fun newArray(size: Int): Array<ProvinceResponse?> {
            return arrayOfNulls(size)
        }
    }
}