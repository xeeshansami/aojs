package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.CountryCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.CountryResponse

class CountryHR(callBack: CountryCallBack) : BaseRH<CountryResponse>() {
    var callback: CountryCallBack = callBack
    override fun onSuccess(response: CountryResponse?) {
        response?.let { callback.CountrySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CountryFailure(it) }
    }
}