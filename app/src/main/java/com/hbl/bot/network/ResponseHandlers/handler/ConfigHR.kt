package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class ConfigHR(callBack: ConfigCallBack) : BaseRH<ConfigResponse>() {
    var callback: ConfigCallBack = callBack
    override fun onSuccess(response: ConfigResponse?) {
        response?.let { callback.ConfigSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ConfigFailure(it) }
    }
}