package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PurposeOfAccountCallBack {
    fun PurposeOfAccountSuccess(response: PurposeOfAccountResponse)
    fun PurposeOfAccountFailure(response: BaseResponse)
}