package com.hbl.bot.network.models.response.base


import com.google.gson.annotations.SerializedName

class PrintDocumentData {
    @SerializedName("DOC_SETUP")
    var dOCSETUP: ArrayList<DOCSETUP> =  ArrayList<DOCSETUP>()
    @SerializedName("PAGE_SETUP")
    var pAGESETUP: ArrayList<PAGESETUP> = ArrayList<PAGESETUP>()
}