package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class KINCUST_ACCOUNTS() : Parcelable {
    var ACCTTYPEDESC = ""
    var ACCT_OPER_TYPE_CODE = ""
    var ACCT_CCY = ""
    var ACCT_NO = ""
    var ACCT_SNG_JNT = ""
    var ACCT_SNG_JNT_DESC = ""
    var ACCT_TYPE = ""
    var BRANCHNAME = ""
    var BRANCH_CODE = ""
    var CIF_NO = ""
    var CONVENTIONAL_ISLAMIC = ""
    var CURRENCY = ""
    var CURRENCY_DESC = ""
    var DEBITCARDTYPEDESC = ""
    var DEBIT_CARD_REQ = ""
    var DEBIT_CARD_TYPE = ""
    var ESTATEMENT = ""
    var ESTATEMENTFREQDESC = ""
    var ESTATEMENT_FREQ = ""
    var FREE_INSURANCE = ""
    var HBL_NISA = ""
    var ID_DOC_NO = ""
    var INITDEPSOURCEDESC = ""
    var INIT_DEPOSIT = ""
    var INIT_DEP_SOURCE = ""
    var INTERNET_BANK = ""
    var NAME_ON_CARD: Any? = ""
    var OPERINSTDESC = ""
    var OPER_INSTRUCTION = ""
    var OTHER_PURPOSE_OF_ACCT_DESC = ""
    var PHONE_BANK = ""
    var PREFERREDCOMMUMODEDESC = ""
    var PREFERRED_COMMU_MODE = ""
    var PURPOSEOFACCTDESC = ""
    var PURPOSE_OF_ACCT = ""
    var RM_CODE = ""
    var RM_DESC = ""
    var SMS_ALERT = ""
    var STAFF_ACCT_NO = ""
    var TITLE_OF_ACCT = ""
    var TITLE_SHORT_OF_ACCT = ""
    var ZAKAT_EXEMPT = ""

    constructor(parcel: Parcel) : this() {
        ACCT_OPER_TYPE_CODE = parcel.readString().toString()
        ACCTTYPEDESC = parcel.readString().toString()
        ACCT_CCY = parcel.readString().toString()
        ACCT_NO = parcel.readString().toString()
        ACCT_SNG_JNT = parcel.readString().toString()
        ACCT_SNG_JNT_DESC = parcel.readString().toString()
        ACCT_TYPE = parcel.readString().toString()
        BRANCHNAME = parcel.readString().toString()
        BRANCH_CODE = parcel.readString().toString()
        CIF_NO = parcel.readString().toString()
        CONVENTIONAL_ISLAMIC = parcel.readString().toString()
        CURRENCY = parcel.readString().toString()
        CURRENCY_DESC = parcel.readString().toString()
        DEBITCARDTYPEDESC = parcel.readString().toString()
        DEBIT_CARD_REQ = parcel.readString().toString()
        DEBIT_CARD_TYPE = parcel.readString().toString()
        ESTATEMENT = parcel.readString().toString()
        ESTATEMENTFREQDESC = parcel.readString().toString()
        ESTATEMENT_FREQ = parcel.readString().toString()
        FREE_INSURANCE = parcel.readString().toString()
        HBL_NISA = parcel.readString().toString()
        ID_DOC_NO = parcel.readString().toString()
        INITDEPSOURCEDESC = parcel.readString().toString()
        INIT_DEPOSIT = parcel.readString().toString()
        INIT_DEP_SOURCE = parcel.readString().toString()
        INTERNET_BANK = parcel.readString().toString()
        OPERINSTDESC = parcel.readString().toString()
        OPER_INSTRUCTION = parcel.readString().toString()
        PHONE_BANK = parcel.readString().toString()
        PREFERREDCOMMUMODEDESC = parcel.readString().toString()
        PREFERRED_COMMU_MODE = parcel.readString().toString()
        PURPOSEOFACCTDESC = parcel.readString().toString()
        PURPOSE_OF_ACCT = parcel.readString().toString()
        RM_CODE = parcel.readString().toString()
        RM_DESC = parcel.readString().toString()
        SMS_ALERT = parcel.readString().toString()
        STAFF_ACCT_NO = parcel.readString().toString()
        TITLE_OF_ACCT = parcel.readString().toString()
        TITLE_SHORT_OF_ACCT = parcel.readString().toString()
        ZAKAT_EXEMPT = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ACCT_OPER_TYPE_CODE)
        parcel.writeString(ACCTTYPEDESC)
        parcel.writeString(ACCT_CCY)
        parcel.writeString(ACCT_NO)
        parcel.writeString(ACCT_SNG_JNT)
        parcel.writeString(ACCT_SNG_JNT_DESC)
        parcel.writeString(ACCT_TYPE)
        parcel.writeString(BRANCHNAME)
        parcel.writeString(BRANCH_CODE)
        parcel.writeString(CIF_NO)
        parcel.writeString(CONVENTIONAL_ISLAMIC)
        parcel.writeString(CURRENCY)
        parcel.writeString(CURRENCY_DESC)
        parcel.writeString(DEBITCARDTYPEDESC)
        parcel.writeString(DEBIT_CARD_REQ)
        parcel.writeString(DEBIT_CARD_TYPE)
        parcel.writeString(ESTATEMENT)
        parcel.writeString(ESTATEMENTFREQDESC)
        parcel.writeString(ESTATEMENT_FREQ)
        parcel.writeString(FREE_INSURANCE)
        parcel.writeString(HBL_NISA)
        parcel.writeString(ID_DOC_NO)
        parcel.writeString(INITDEPSOURCEDESC)
        parcel.writeString(INIT_DEPOSIT)
        parcel.writeString(INIT_DEP_SOURCE)
        parcel.writeString(INTERNET_BANK)
        parcel.writeString(OPERINSTDESC)
        parcel.writeString(OPER_INSTRUCTION)
        parcel.writeString(PHONE_BANK)
        parcel.writeString(PREFERREDCOMMUMODEDESC)
        parcel.writeString(PREFERRED_COMMU_MODE)
        parcel.writeString(PURPOSEOFACCTDESC)
        parcel.writeString(PURPOSE_OF_ACCT)
        parcel.writeString(RM_CODE)
        parcel.writeString(RM_DESC)
        parcel.writeString(SMS_ALERT)
        parcel.writeString(STAFF_ACCT_NO)
        parcel.writeString(TITLE_OF_ACCT)
        parcel.writeString(TITLE_SHORT_OF_ACCT)
        parcel.writeString(ZAKAT_EXEMPT)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KINCUST_ACCOUNTS> {
        override fun createFromParcel(parcel: Parcel): KINCUST_ACCOUNTS {
            return KINCUST_ACCOUNTS(parcel)
        }

        override fun newArray(size: Int): Array<KINCUST_ACCOUNTS?> {
            return arrayOfNulls(size)
        }
    }
}