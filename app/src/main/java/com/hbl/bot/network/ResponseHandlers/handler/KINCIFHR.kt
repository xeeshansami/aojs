package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.KINCIFCallBack
import com.hbl.bot.network.models.response.base.*

class KINCIFHR(callBack: KINCIFCallBack) : BaseRH<KINAccountInfoResponse>() {
    var callback: KINCIFCallBack = callBack
    override fun onSuccess(response: KINAccountInfoResponse?) {
        response?.let { callback.KINCIFSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.KINCIFFailure(it) }
    }
}