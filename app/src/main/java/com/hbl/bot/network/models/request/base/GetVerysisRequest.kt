package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class GetVerysisRequest(
) : Parcelable {
    @SerializedName("WORK_FLOW_CODE")
    var WORK_FLOW_CODE: String = ""

    @SerializedName("ETBNTBFLAG")
    var ETBNTBFLAG: String = ""

    @SerializedName("CITIZEN_NUMBER")
    var CITIZEN_NUMBER: String = ""

    @SerializedName("USER_ID")
    var USER_ID: String = ""

    @SerializedName("connection._peername.address")
    var address: String = ""

    @SerializedName("connection._peername.port")
    var port: String = ""

    @SerializedName("method")
    var method: String = ""

    constructor(parcel: Parcel) : this() {
        CITIZEN_NUMBER = parcel.readString().toString()
        USER_ID = parcel.readString().toString()
        address = parcel.readString().toString()
        port = parcel.readString().toString()
        method = parcel.readString().toString()
        ETBNTBFLAG = parcel.readString().toString()
        WORK_FLOW_CODE = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(WORK_FLOW_CODE)
        parcel.writeString(ETBNTBFLAG)
        parcel.writeString(CITIZEN_NUMBER)
        parcel.writeString(USER_ID)
        parcel.writeString(address)
        parcel.writeString(port)
        parcel.writeString(method)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GetVerysisRequest> {
        override fun createFromParcel(parcel: Parcel): GetVerysisRequest {
            return GetVerysisRequest(parcel)
        }

        override fun newArray(size: Int): Array<GetVerysisRequest?> {
            return arrayOfNulls(size)
        }
    }
}