
package com.hbl.bot.network.models.response.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CustomerAofAccountInfo {

    @SerializedName("Tracking_id")
    @Expose
    private String trackingId;

    public String getTrackingId() {
        return trackingId;
    }

    public void setTrackingId(String trackingId) {
        this.trackingId = trackingId;
    }

}
