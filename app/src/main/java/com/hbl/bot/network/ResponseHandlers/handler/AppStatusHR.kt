package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.AppStatusCallBack
import com.hbl.bot.network.models.response.base.*

class AppStatusHR(callBack: AppStatusCallBack) : BaseRH<AppStatusResponse>() {
    var callback: AppStatusCallBack = callBack
    override fun onSuccess(response: AppStatusResponse?) {
        response?.let { callback.AppStatusSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.AppStatusFailure(it) }
    }
}