package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Onboarding() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("TRACKING_ID")
    @Expose
    val TRACKING_ID: String? = null

    @SerializedName("NAME")
    @Expose
    val NAME: String? = null

    @SerializedName("CONTACT_NO")
    @Expose
    val CONTACT_NO: String? = null

    @SerializedName("BRANCH_CODE")
    @Expose
    val BRANCH_CODE: String? = null

    @SerializedName("BRANCH_NAME")
    @Expose
    val BRANCH_NAME: String? = null

    @SerializedName("EMAIL")
    @Expose
    val EMAIL: String? = null

    @SerializedName("ADDRESS")
    @Expose
    val ADDRESS: String? = null

    @SerializedName("CALL_DATE_TIME")
    @Expose
    val CALL_DATE_TIME: String? = null

    @SerializedName("MEET_DATE_TIME")
    @Expose
    val MEET_DATE_TIME: String? = null

    @SerializedName("CHANNEL")
    @Expose
    val CHANNEL: String? = null

    @SerializedName("STATUS")
    @Expose
    val STATUS: String? = null

    @SerializedName("DATE_TIME")
    @Expose
    val DATE_TIME: String? = null

    @SerializedName("ID_DOC_NO")
    @Expose
    val ID_DOC_NO: String? = null

    @SerializedName("MEET_DATE")
    @Expose
    val MEET_DATE: String? = null

    @SerializedName("MEET_TIME")
    @Expose
    val MEET_TIME: String? = null

    @SerializedName("APPLIED_FOR")
    @Expose
    val APPLIED_FOR: ArrayList<APPLIED_FOR>? = null

    @SerializedName("RM_INFO")
    @Expose
    val RM_INFO: ArrayList<RM_INFO>? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<Onboarding> {
        override fun createFromParcel(parcel: Parcel): Onboarding {
            return Onboarding(parcel)
        }

        override fun newArray(size: Int): Array<Onboarding?> {
            return arrayOfNulls(size)
        }
    }

}