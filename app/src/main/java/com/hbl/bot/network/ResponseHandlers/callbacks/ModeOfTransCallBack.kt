package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ModeOfTransCallBack {
    fun ModeOfTransSuccess(response: ModeOfTransResponse)
    fun ModeOfTransFailure(response: BaseResponse)
}