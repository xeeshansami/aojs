package com.hbl.bot.network.models.response.base

import com.hbl.bot.network.models.response.baseRM.PepFlag

data class PepCheckResponse(
    val `data`: List<PepFlag>,
    val message: String,
    val status: String
)