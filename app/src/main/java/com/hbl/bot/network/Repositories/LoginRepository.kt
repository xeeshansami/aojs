package com.hbl.bot.network.Repositories

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import com.hbl.bot.network.ResponseHandlers.callbacks.BranchCodeCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.BranchTypeCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.LoginCallBack
import com.hbl.bot.network.ResponseHandlers.handler.BranchCodeHR
import com.hbl.bot.network.ResponseHandlers.handler.BranchTypeHR
import com.hbl.bot.network.ResponseHandlers.handler.LoginBaseHR
import com.hbl.bot.network.apiInterface.APIInterface
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.BranchCodeRequest
import com.hbl.bot.network.models.request.base.LoginRequest
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.BranchCodeResponse
import com.hbl.bot.network.models.response.base.BranchTypeResponse
import com.hbl.bot.network.models.response.base.LoginResponse
import com.hbl.bot.network.retrofitBuilder.RetrofitBuilder
import com.hbl.bot.utils.Config
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.ToastUtils

class LoginRepository() {
    val globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
    var apiInterface: APIInterface? = null

    init {
        apiInterface =
            GlobalClass.applicationContext?.let {
                RetrofitBuilder.getRetrofitInstance(
                    it,
                    RetrofitEnums.URL_HBL
                            , Config.API_CONNECT_TIMEOUT
                )
            }
    }

    fun getLogin(
        loginRequest: LoginRequest,
        activity: FragmentActivity?
    ): MutableLiveData<LoginResponse> {
        globalClass?.showDialog(activity)
        val data: MutableLiveData<LoginResponse> = MutableLiveData<LoginResponse>()
        apiInterface?.getLogin(loginRequest)?.enqueue(LoginBaseHR(object : LoginCallBack {
            override fun LoginSuccess(response: LoginResponse) {
                if (!response.data.isNullOrEmpty())
                    data.value = response
            }

            override fun LoginFailure(response: BaseResponse) {
                data.value = null
                ToastUtils.normalShowToast(activity, response.message, 1)
                globalClass?.hideLoader()
            }

        }))
        return data
    }

    fun getBranchCode(
        request: BranchCodeRequest,
        activity: FragmentActivity?
    ): MutableLiveData<BranchCodeResponse> {
        val data: MutableLiveData<BranchCodeResponse> = MutableLiveData<BranchCodeResponse>()
        apiInterface?.getBranchCode(request)?.enqueue(
            BranchCodeHR(
                object : BranchCodeCallBack {
                    override fun BranchCodeSuccess(response: BranchCodeResponse) {
                        GlobalClass.sharedPreferenceManager!!.setBranchCode(response?.data?.get(0))
                        if (!response.data.isNullOrEmpty())
                            data.value = response
                    }

                    override fun BranchCodeFailure(response: BaseResponse) {
                        ToastUtils.normalShowToast(activity, response.message, 1)
                        globalClass?.hideLoader()
                    }
                })
        )
        return data
    }

    fun getBranchType(
        request: BranchCodeRequest,
        activity: FragmentActivity?
    ): MutableLiveData<BranchTypeResponse> {
        val data: MutableLiveData<BranchTypeResponse> = MutableLiveData<BranchTypeResponse>()
        apiInterface?.getBranchType(request)?.enqueue(
            BranchTypeHR(
                object : BranchTypeCallBack {
                    override fun BranchTypeSuccess(response: BranchTypeResponse) {
//                        GlobalClass.sharedPreferenceManager!!.setBranchCode(response?.data?.get(0))
                        if (!response.data.isNullOrEmpty())
                            data.value = response
                        globalClass?.hideLoader()
                    }

                    override fun BranchTypeFailure(response: BaseResponse) {
                        ToastUtils.normalShowToast(activity, response.message, 1)
                        globalClass?.hideLoader()
                    }
                })
        )
        return data
    }
}