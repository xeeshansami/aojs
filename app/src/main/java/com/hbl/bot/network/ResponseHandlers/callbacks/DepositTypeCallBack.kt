package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface DepositTypeCallBack {
    fun DepositTypeSuccess(response: DepositTypeResponse)
    fun DepositTypeFailure(response: BaseResponse)
}