package com.hbl.bot.network.ViewModels

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.hbl.bot.network.models.request.base.LoginRequest
import com.hbl.bot.network.models.response.base.BranchCodeResponse
import com.hbl.bot.network.models.response.base.BranchTypeResponse
import com.hbl.bot.network.models.response.base.LoginResponse
import com.hbl.bot.network.Repositories.LoginRepository
import com.hbl.bot.network.models.request.base.BranchCodeRequest

class LoginViewModel() : ViewModel() {
    var loginRepository = LoginRepository()
    var loginResponseLiveData: LiveData<LoginResponse>? = null
    var branchCodeResponseLiveData: LiveData<BranchCodeResponse>? = null
    var branchTypeResponseLiveData: LiveData<BranchTypeResponse>? = null

    /*call Login Live Data*/
    fun getLoginLiveData(
        loginRequest: LoginRequest,
        activity: FragmentActivity?
    ): LiveData<LoginResponse>? {
        if (loginResponseLiveData == null) {
            return loginRepository.getLogin(loginRequest, activity)
        } else {
            return loginResponseLiveData
        }
    }

    /*call Branch Live Data*/
    fun getBranchLiveData(
        branchCodeRequest: BranchCodeRequest,
        activity: FragmentActivity?
    ): LiveData<BranchCodeResponse>? {
        if (branchCodeResponseLiveData == null) {
            return loginRepository.getBranchCode(branchCodeRequest, activity)
        } else {
            return branchCodeResponseLiveData
        }
    }


    /*call Branch Live Data*/
    fun getBranchTypeLiveData(
        request: BranchCodeRequest,
        activity: FragmentActivity?
    ): LiveData<BranchTypeResponse>? {
        if (branchTypeResponseLiveData == null) {
            return loginRepository.getBranchType(request, activity)
        } else {
            return branchTypeResponseLiveData
        }
    }
}