package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.PreferencesMailingCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.PreferencesMailingResponse

class PreferencesMailingBaseHR(callBack: PreferencesMailingCallBack) :
    BaseRH<PreferencesMailingResponse>() {
    var callback: PreferencesMailingCallBack = callBack
    override fun onSuccess(response: PreferencesMailingResponse?) {
        response?.let { callback.PreferencesMailingSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PreferencesMailingFailure(it) }
    }
}