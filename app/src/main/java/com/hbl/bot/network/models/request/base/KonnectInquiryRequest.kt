package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable

class KonnectInquiryRequest() : Parcelable {
    var tracking_id: String? = null
    var doc_id: String? = null
    var mobile_number: String? = null
    var city: String? = null
    var flag: String? = null
    var branch_id: String? = null
    var beneficial_owner_id: String? = null
    var email: String? = null

    constructor(parcel: Parcel) : this() {
        tracking_id = parcel.readString()
        doc_id = parcel.readString()
        mobile_number = parcel.readString()
        city = parcel.readString()
        flag = parcel.readString()
        branch_id = parcel.readString()
        beneficial_owner_id = parcel.readString()
        email = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(tracking_id)
        parcel.writeString(doc_id)
        parcel.writeString(mobile_number)
        parcel.writeString(city)
        parcel.writeString(flag)
        parcel.writeString(branch_id)
        parcel.writeString(beneficial_owner_id)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KonnectInquiryRequest> {
        override fun createFromParcel(parcel: Parcel): KonnectInquiryRequest {
            return KonnectInquiryRequest(parcel)
        }

        override fun newArray(size: Int): Array<KonnectInquiryRequest?> {
            return arrayOfNulls(size)
        }
    }
}