
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KINUSERINFO {

    @SerializedName("USER_ID")
    @Expose
    private String uSERID = "";
    @SerializedName("USER_ROLE")
    @Expose
    private Integer uSERROLE = 0;
    @SerializedName("USER_NAME")
    @Expose
    private String uSERNAME = "";
    @SerializedName("BRANCH_CODE")
    @Expose
    private String bRANCHCODE = "";
    @SerializedName("ACTION_ID")
    @Expose
    private Integer aCTIONID = 0;
    @SerializedName("ACTION_NAME")
    @Expose
    private String aCTIONNAME = "";
    @SerializedName("ACTION_EVENT")
    @Expose
    private String aCTIONEVENT = "";
    @SerializedName("ACTION_DATE")
    @Expose
    private String aCTIONDATE = "";

    public String getUSERID() {
        return uSERID;
    }

    public void setUSERID(String uSERID) {
        this.uSERID = uSERID;
    }

    public Integer getUSERROLE() {
        return uSERROLE;
    }

    public void setUSERROLE(Integer uSERROLE) {
        this.uSERROLE = uSERROLE;
    }

    public String getUSERNAME() {
        return uSERNAME;
    }

    public void setUSERNAME(String uSERNAME) {
        this.uSERNAME = uSERNAME;
    }

    public String getBRANCHCODE() {
        return bRANCHCODE;
    }

    public void setBRANCHCODE(String bRANCHCODE) {
        this.bRANCHCODE = bRANCHCODE;
    }

    public Integer getACTIONID() {
        return aCTIONID;
    }

    public void setACTIONID(Integer aCTIONID) {
        this.aCTIONID = aCTIONID;
    }

    public String getACTIONNAME() {
        return aCTIONNAME;
    }

    public void setACTIONNAME(String aCTIONNAME) {
        this.aCTIONNAME = aCTIONNAME;
    }

    public String getACTIONEVENT() {
        return aCTIONEVENT;
    }

    public void setACTIONEVENT(String aCTIONEVENT) {
        this.aCTIONEVENT = aCTIONEVENT;
    }

    public String getACTIONDATE() {
        return aCTIONDATE;
    }

    public void setACTIONDATE(String aCTIONDATE) {
        this.aCTIONDATE = aCTIONDATE;
    }

}
