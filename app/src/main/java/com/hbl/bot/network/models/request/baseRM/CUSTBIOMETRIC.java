
package com.hbl.bot.network.models.request.baseRM;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hbl.bot.network.models.response.baseRM.RESPONSEFINGERINDEX;

public class CUSTBIOMETRIC {

    @SerializedName("BIOTRACKING_ID")
    @Expose
    private String bIOTRACKINGID;
    @SerializedName("TRACKING_ID")
    @Expose
    private String tRACKINGID;
    @SerializedName("CHANNEL_ID")
    @Expose
    private String cHANNELID;
    @SerializedName("DATETIME")
    @Expose
    private String dATETIME;
    @SerializedName("TRANSACTION_ID")
    @Expose
    private String tRANSACTIONID;
    @SerializedName("USER_ID")
    @Expose
    private String uSERID;
    @SerializedName("BRANCH_CODE")
    @Expose
    private String bRANCHCODE;
    @SerializedName("BRANCH_NAME")
    @Expose
    private String bRANCHNAME;
    @SerializedName("BRANCH_ADDRESS")
    @Expose
    private String bRANCHADDRESS;
    @SerializedName("BRANCH_TYPE")
    @Expose
    private String bRANCHTYPE;
    @SerializedName("ROLE")
    @Expose
    private Integer rOLE;
    @SerializedName("URLTYPE")
    @Expose
    private Object uRLTYPE;
    @SerializedName("SERVICE_TYPE")
    @Expose
    private String sERVICETYPE;
    @SerializedName("SESSION_ID")
    @Expose
    private String sESSIONID;
    @SerializedName("CITIZEN_NUMBER")
    @Expose
    private String cITIZENNUMBER;
    @SerializedName("MOBILE_NO")
    @Expose
    private String mOBILENO;
    @SerializedName("CONTACT_NUMBER")
    @Expose
    private String cONTACTNUMBER;
    @SerializedName("BIOPURPOSE")
    @Expose
    private List<Object> bIOPURPOSE = null;
    @SerializedName("FINGER_INDEX")
    @Expose
    private String fINGERINDEX;
    @SerializedName("TEMPLATE_TYPE")
    @Expose
    private String tEMPLATETYPE;
    @SerializedName("PROVINCE_CODE")
    @Expose
    private String pROVINCECODE;
    @SerializedName("PROVINCE_DESC")
    @Expose
    private String pROVINCEDESC;
    @SerializedName("ACCOUNT_BAT_CODE")
    @Expose
    private String aCCOUNTBATCODE;
    @SerializedName("ACCOUNT_BAT_NAME")
    @Expose
    private String aCCOUNTBATNAME;
    @SerializedName("OLDER_DAYS")
    @Expose
    private String oLDERDAYS;
    @SerializedName("NADRA_VERISYS_CHECK")
    @Expose
    private String nADRAVERISYSCHECK;
    @SerializedName("RESPONSE_CODE")
    @Expose
    private String rESPONSECODE;
    @SerializedName("RESPONSE_MESSAGE")
    @Expose
    private String rESPONSEMESSAGE;
    @SerializedName("RESPONSE_SESSION_ID")
    @Expose
    private String rESPONSESESSIONID;
    @SerializedName("RESPONSE_CITIZEN_NUMBER")
    @Expose
    private String rESPONSECITIZENNUMBER;
    @SerializedName("RESPONSE_NAME")
    @Expose
    private String rESPONSENAME;
    @SerializedName("RESPONSE_FATHER_HUSBAND_NAME")
    @Expose
    private String rESPONSEFATHERHUSBANDNAME;
    @SerializedName("RESPONSE_PRESENT_ADDRESS")
    @Expose
    private String rESPONSEPRESENTADDRESS;
    @SerializedName("RESPONSE_PERMANENT_ADDRESS")
    @Expose
    private String rESPONSEPERMANENTADDRESS;
    @SerializedName("RESPONSE_DATE_OF_BIRTH")
    @Expose
    private String rESPONSEDATEOFBIRTH;
    @SerializedName("RESPONSE_BIRTH_PLACE")
    @Expose
    private String rESPONSEBIRTHPLACE;
    @SerializedName("RESPONSE_PHOTOGRAPH")
    @Expose
    private String rESPONSEPHOTOGRAPH;
    @SerializedName("RESPONSE_EXPIRY_DATE")
    @Expose
    private String rESPONSEEXPIRYDATE;
    @SerializedName("RESPONSE_FINGER_INDEX")
    @Expose
    private RESPONSEFINGERINDEX rESPONSEFINGERINDEX;
    @SerializedName("RESPONSE_CITIZEN_STATUS")
    @Expose
    private String rESPONSECITIZENSTATUS;
    @SerializedName("RESPONSE_CARD_TYPE")
    @Expose
    private String rESPONSECARDTYPE;
    @SerializedName("VERISYS_LOCATION")
    @Expose
    private String vERISYSLOCATION;

    public String getBIOTRACKINGID() {
        return bIOTRACKINGID;
    }

    public void setBIOTRACKINGID(String bIOTRACKINGID) {
        this.bIOTRACKINGID = bIOTRACKINGID;
    }

    public String getTRACKINGID() {
        return tRACKINGID;
    }

    public void setTRACKINGID(String tRACKINGID) {
        this.tRACKINGID = tRACKINGID;
    }

    public String getCHANNELID() {
        return cHANNELID;
    }

    public void setCHANNELID(String cHANNELID) {
        this.cHANNELID = cHANNELID;
    }

    public String getDATETIME() {
        return dATETIME;
    }

    public void setDATETIME(String dATETIME) {
        this.dATETIME = dATETIME;
    }

    public String getTRANSACTIONID() {
        return tRANSACTIONID;
    }

    public void setTRANSACTIONID(String tRANSACTIONID) {
        this.tRANSACTIONID = tRANSACTIONID;
    }

    public String getUSERID() {
        return uSERID;
    }

    public void setUSERID(String uSERID) {
        this.uSERID = uSERID;
    }

    public String getBRANCHCODE() {
        return bRANCHCODE;
    }

    public void setBRANCHCODE(String bRANCHCODE) {
        this.bRANCHCODE = bRANCHCODE;
    }

    public String getBRANCHNAME() {
        return bRANCHNAME;
    }

    public void setBRANCHNAME(String bRANCHNAME) {
        this.bRANCHNAME = bRANCHNAME;
    }

    public String getBRANCHADDRESS() {
        return bRANCHADDRESS;
    }

    public void setBRANCHADDRESS(String bRANCHADDRESS) {
        this.bRANCHADDRESS = bRANCHADDRESS;
    }

    public String getBRANCHTYPE() {
        return bRANCHTYPE;
    }

    public void setBRANCHTYPE(String bRANCHTYPE) {
        this.bRANCHTYPE = bRANCHTYPE;
    }

    public Integer getROLE() {
        return rOLE;
    }

    public void setROLE(Integer rOLE) {
        this.rOLE = rOLE;
    }

    public Object getURLTYPE() {
        return uRLTYPE;
    }

    public void setURLTYPE(Object uRLTYPE) {
        this.uRLTYPE = uRLTYPE;
    }

    public String getSERVICETYPE() {
        return sERVICETYPE;
    }

    public void setSERVICETYPE(String sERVICETYPE) {
        this.sERVICETYPE = sERVICETYPE;
    }

    public String getSESSIONID() {
        return sESSIONID;
    }

    public void setSESSIONID(String sESSIONID) {
        this.sESSIONID = sESSIONID;
    }

    public String getCITIZENNUMBER() {
        return cITIZENNUMBER;
    }

    public void setCITIZENNUMBER(String cITIZENNUMBER) {
        this.cITIZENNUMBER = cITIZENNUMBER;
    }

    public String getMOBILENO() {
        return mOBILENO;
    }

    public void setMOBILENO(String mOBILENO) {
        this.mOBILENO = mOBILENO;
    }

    public String getCONTACTNUMBER() {
        return cONTACTNUMBER;
    }

    public void setCONTACTNUMBER(String cONTACTNUMBER) {
        this.cONTACTNUMBER = cONTACTNUMBER;
    }

    public List<Object> getBIOPURPOSE() {
        return bIOPURPOSE;
    }

    public void setBIOPURPOSE(List<Object> bIOPURPOSE) {
        this.bIOPURPOSE = bIOPURPOSE;
    }

    public String getFINGERINDEX() {
        return fINGERINDEX;
    }

    public void setFINGERINDEX(String fINGERINDEX) {
        this.fINGERINDEX = fINGERINDEX;
    }

    public String getTEMPLATETYPE() {
        return tEMPLATETYPE;
    }

    public void setTEMPLATETYPE(String tEMPLATETYPE) {
        this.tEMPLATETYPE = tEMPLATETYPE;
    }

    public String getPROVINCECODE() {
        return pROVINCECODE;
    }

    public void setPROVINCECODE(String pROVINCECODE) {
        this.pROVINCECODE = pROVINCECODE;
    }

    public String getPROVINCEDESC() {
        return pROVINCEDESC;
    }

    public void setPROVINCEDESC(String pROVINCEDESC) {
        this.pROVINCEDESC = pROVINCEDESC;
    }

    public String getACCOUNTBATCODE() {
        return aCCOUNTBATCODE;
    }

    public void setACCOUNTBATCODE(String aCCOUNTBATCODE) {
        this.aCCOUNTBATCODE = aCCOUNTBATCODE;
    }

    public String getACCOUNTBATNAME() {
        return aCCOUNTBATNAME;
    }

    public void setACCOUNTBATNAME(String aCCOUNTBATNAME) {
        this.aCCOUNTBATNAME = aCCOUNTBATNAME;
    }

    public String getOLDERDAYS() {
        return oLDERDAYS;
    }

    public void setOLDERDAYS(String oLDERDAYS) {
        this.oLDERDAYS = oLDERDAYS;
    }

    public String getNADRAVERISYSCHECK() {
        return nADRAVERISYSCHECK;
    }

    public void setNADRAVERISYSCHECK(String nADRAVERISYSCHECK) {
        this.nADRAVERISYSCHECK = nADRAVERISYSCHECK;
    }

    public String getRESPONSECODE() {
        return rESPONSECODE;
    }

    public void setRESPONSECODE(String rESPONSECODE) {
        this.rESPONSECODE = rESPONSECODE;
    }

    public String getRESPONSEMESSAGE() {
        return rESPONSEMESSAGE;
    }

    public void setRESPONSEMESSAGE(String rESPONSEMESSAGE) {
        this.rESPONSEMESSAGE = rESPONSEMESSAGE;
    }

    public String getRESPONSESESSIONID() {
        return rESPONSESESSIONID;
    }

    public void setRESPONSESESSIONID(String rESPONSESESSIONID) {
        this.rESPONSESESSIONID = rESPONSESESSIONID;
    }

    public String getRESPONSECITIZENNUMBER() {
        return rESPONSECITIZENNUMBER;
    }

    public void setRESPONSECITIZENNUMBER(String rESPONSECITIZENNUMBER) {
        this.rESPONSECITIZENNUMBER = rESPONSECITIZENNUMBER;
    }

    public String getRESPONSENAME() {
        return rESPONSENAME;
    }

    public void setRESPONSENAME(String rESPONSENAME) {
        this.rESPONSENAME = rESPONSENAME;
    }

    public String getRESPONSEFATHERHUSBANDNAME() {
        return rESPONSEFATHERHUSBANDNAME;
    }

    public void setRESPONSEFATHERHUSBANDNAME(String rESPONSEFATHERHUSBANDNAME) {
        this.rESPONSEFATHERHUSBANDNAME = rESPONSEFATHERHUSBANDNAME;
    }

    public String getRESPONSEPRESENTADDRESS() {
        return rESPONSEPRESENTADDRESS;
    }

    public void setRESPONSEPRESENTADDRESS(String rESPONSEPRESENTADDRESS) {
        this.rESPONSEPRESENTADDRESS = rESPONSEPRESENTADDRESS;
    }

    public String getRESPONSEPERMANENTADDRESS() {
        return rESPONSEPERMANENTADDRESS;
    }

    public void setRESPONSEPERMANENTADDRESS(String rESPONSEPERMANENTADDRESS) {
        this.rESPONSEPERMANENTADDRESS = rESPONSEPERMANENTADDRESS;
    }

    public String getRESPONSEDATEOFBIRTH() {
        return rESPONSEDATEOFBIRTH;
    }

    public void setRESPONSEDATEOFBIRTH(String rESPONSEDATEOFBIRTH) {
        this.rESPONSEDATEOFBIRTH = rESPONSEDATEOFBIRTH;
    }

    public String getRESPONSEBIRTHPLACE() {
        return rESPONSEBIRTHPLACE;
    }

    public void setRESPONSEBIRTHPLACE(String rESPONSEBIRTHPLACE) {
        this.rESPONSEBIRTHPLACE = rESPONSEBIRTHPLACE;
    }

    public String getRESPONSEPHOTOGRAPH() {
        return rESPONSEPHOTOGRAPH;
    }

    public void setRESPONSEPHOTOGRAPH(String rESPONSEPHOTOGRAPH) {
        this.rESPONSEPHOTOGRAPH = rESPONSEPHOTOGRAPH;
    }

    public String getRESPONSEEXPIRYDATE() {
        return rESPONSEEXPIRYDATE;
    }

    public void setRESPONSEEXPIRYDATE(String rESPONSEEXPIRYDATE) {
        this.rESPONSEEXPIRYDATE = rESPONSEEXPIRYDATE;
    }

    public RESPONSEFINGERINDEX getRESPONSEFINGERINDEX() {
        return rESPONSEFINGERINDEX;
    }

    public void setRESPONSEFINGERINDEX(RESPONSEFINGERINDEX rESPONSEFINGERINDEX) {
        this.rESPONSEFINGERINDEX = rESPONSEFINGERINDEX;
    }

    public String getRESPONSECITIZENSTATUS() {
        return rESPONSECITIZENSTATUS;
    }

    public void setRESPONSECITIZENSTATUS(String rESPONSECITIZENSTATUS) {
        this.rESPONSECITIZENSTATUS = rESPONSECITIZENSTATUS;
    }

    public String getRESPONSECARDTYPE() {
        return rESPONSECARDTYPE;
    }

    public void setRESPONSECARDTYPE(String rESPONSECARDTYPE) {
        this.rESPONSECARDTYPE = rESPONSECARDTYPE;
    }

    public String getVERISYSLOCATION() {
        return vERISYSLOCATION;
    }

    public void setVERISYSLOCATION(String vERISYSLOCATION) {
        this.vERISYSLOCATION = vERISYSLOCATION;
    }

}
