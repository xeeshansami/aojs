package com.hbl.bot.network.ResponseHandlers.handler
import com.hbl.bot.network.ResponseHandlers.callbacks.PrintPDFCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.PrintPDFResponse

class PrintPDFBaseHR(callBack: PrintPDFCallBack) : BaseRH<PrintPDFResponse>() {
    var callback: PrintPDFCallBack=callBack
    override fun onSuccess(response: PrintPDFResponse?) {
        response?.let { callback.PrintPDFSuccess(it) }
    }
    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PrintPDFFailure(it) }
    }
}