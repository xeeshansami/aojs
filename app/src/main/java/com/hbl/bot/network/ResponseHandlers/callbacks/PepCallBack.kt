package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PepCallBack {
    fun PepSuccess(response: PepResponse)
    fun PepFailure(response: BaseResponse)
}