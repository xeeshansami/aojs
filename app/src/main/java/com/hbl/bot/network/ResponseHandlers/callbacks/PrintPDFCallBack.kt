package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PrintPDFCallBack  {
    fun PrintPDFSuccess(response: PrintPDFResponse)
    fun PrintPDFFailure(response: BaseResponse)
}