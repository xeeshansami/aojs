package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class KINCUSTADDRRESS() : Parcelable {
    var ADDRESS_LINE1 = ""
    var ADDRESS_LINE2 = ""
    var ADDRESS_LINE3 = ""
    var ADDRESS_TYPE = ""
    var CITY = ""
    var CITY_NAME = ""
    var COUNTRY = ""
    var COUNTRY_NAME = ""
    var NEAREST_LANDMARK = ""
    var POSTCODE = ""
    var TYPE_OF_ADDRESS_CODE = ""
    var TYPE_OF_ADDRESS_DESC = ""

    constructor(parcel: Parcel) : this() {
        ADDRESS_LINE1 = parcel.readString().toString()
        ADDRESS_LINE2 = parcel.readString().toString()
        ADDRESS_LINE3 = parcel.readString().toString()
        ADDRESS_TYPE = parcel.readString().toString()
        CITY = parcel.readString().toString()
        CITY_NAME = parcel.readString().toString()
        COUNTRY = parcel.readString().toString()
        COUNTRY_NAME = parcel.readString().toString()
        NEAREST_LANDMARK = parcel.readString().toString()
        POSTCODE = parcel.readString().toString()
        TYPE_OF_ADDRESS_CODE = parcel.readString().toString()
        TYPE_OF_ADDRESS_DESC = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ADDRESS_LINE1)
        parcel.writeString(ADDRESS_LINE2)
        parcel.writeString(ADDRESS_LINE3)
        parcel.writeString(ADDRESS_TYPE)
        parcel.writeString(CITY)
        parcel.writeString(CITY_NAME)
        parcel.writeString(COUNTRY)
        parcel.writeString(COUNTRY_NAME)
        parcel.writeString(NEAREST_LANDMARK)
        parcel.writeString(POSTCODE)
        parcel.writeString(TYPE_OF_ADDRESS_CODE)
        parcel.writeString(TYPE_OF_ADDRESS_DESC)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KINCUSTADDRRESS> {
        override fun createFromParcel(parcel: Parcel): KINCUSTADDRRESS {
            return KINCUSTADDRRESS(parcel)
        }

        override fun newArray(size: Int): Array<KINCUSTADDRRESS?> {
            return arrayOfNulls(size)
        }
    }
}