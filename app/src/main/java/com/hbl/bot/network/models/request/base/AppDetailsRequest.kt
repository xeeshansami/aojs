/* 
Copyright (c) 2020 Kotlin CustomerAofAccountInfo Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */
package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class AppDetailsRequest() : Parcelable {
    @SerializedName("haveGoogleServices")
    @Expose
    var HAVE_GOOGLE_SERVICES: String? = null

    @SerializedName("IMEI")
    var IMEI: String? = null

    @SerializedName("identifier")
    var identifier: String? = null

    @SerializedName("androidVersion")
    var androidVersion: String? = null

    @SerializedName("applicationName")
    var applicationName: String? = null

    @SerializedName("versionName")
    var versionName: String? = null

    @SerializedName("versionCode")
    var versionCode: String? = null

    @SerializedName("userID")
    var userID: String? = null

    @SerializedName("packageName")
    var packageName: String? = null

    @SerializedName("applicationID")
    var applicationID: String? = null

    @SerializedName("apkSize")
    var apkSize: String? = null

    @SerializedName("createdApkDate")
    var createdApkDate: String? = null

    @SerializedName("updateApkDate")
    var updateApkDate: String? = null

    @SerializedName("androidApiVersion")
    var androidApiVersion: String? = null

    constructor(parcel: Parcel) : this() {
        HAVE_GOOGLE_SERVICES = parcel.readString()
        IMEI = parcel.readString()
        androidApiVersion = parcel.readString()
        identifier = parcel.readString()
        applicationName = parcel.readString()
        versionName = parcel.readString()
        versionCode = parcel.readString()
        userID = parcel.readString()
        applicationID = parcel.readString()
        apkSize = parcel.readString()
        createdApkDate = parcel.readString()
        updateApkDate = parcel.readString()
        androidVersion = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(HAVE_GOOGLE_SERVICES)
        parcel.writeString(IMEI)
        parcel.writeString(androidVersion)
        parcel.writeString(identifier)
        parcel.writeString(applicationName)
        parcel.writeString(versionName)
        parcel.writeString(versionCode)
        parcel.writeString(userID)
        parcel.writeString(applicationID)
        parcel.writeString(apkSize)
        parcel.writeString(createdApkDate)
        parcel.writeString(updateApkDate)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AppDetailsRequest> {
        override fun createFromParcel(parcel: Parcel): AppDetailsRequest {
            return AppDetailsRequest(parcel)
        }

        override fun newArray(size: Int): Array<AppDetailsRequest?> {
            return arrayOfNulls(size)
        }
    }
}