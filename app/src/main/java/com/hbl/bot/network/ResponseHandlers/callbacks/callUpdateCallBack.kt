package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface callUpdateCallBack {
    fun callUpdateSuccess(response: UpdateCallResponse)
    fun callUpdateFailure(response: BaseResponse)
}