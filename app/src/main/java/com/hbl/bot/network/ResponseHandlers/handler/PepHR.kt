package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class PepHR(callBack: PepCallBack) : BaseRH<PepResponse>() {
    var callback: PepCallBack = callBack
    override fun onSuccess(response: PepResponse?) {
        response?.let { callback.PepSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PepFailure(it) }
    }
}