
package com.hbl.bot.network.models.response.baseRM;

import java.io.Serializable;
import java.util.ArrayList;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NadraVerify implements Serializable, Parcelable {


    public BiometricCustInfo getCustInfo() {
        return custInfo;
    }

    public void setCustInfo(BiometricCustInfo custInfo) {
        this.custInfo = custInfo;
    }

    @SerializedName("custInfo")
    @Expose
    BiometricCustInfo custInfo = new BiometricCustInfo();

    public BiometricAddrResidential getCustAddrResidential() {
        return custAddrResidential;
    }

    public void setCustAddrResidential(BiometricAddrResidential custAddrResidential) {
        this.custAddrResidential = custAddrResidential;
    }

    public BiometricAddrPermanent getCustAddrPermanent() {
        return custAddrPermanent;
    }

    public void setCustAddrPermanent(BiometricAddrPermanent custAddrPermanent) {
        this.custAddrPermanent = custAddrPermanent;
    }

    @SerializedName("custAddrResidential")
    @Expose
    BiometricAddrResidential custAddrResidential = new BiometricAddrResidential();
    @SerializedName("custAddrPermanent")
    @Expose
    BiometricAddrPermanent custAddrPermanent = new BiometricAddrPermanent();
    @SerializedName("RESPONSE_PERMANENT_ADDRESS_ENG")
    @Expose
    private String RESPONSE_PERMANENT_ADDRESS_ENG = "";
    @SerializedName("RESPONSE_PRESENT_ADDRESS_ENG")
    @Expose
    private String RESPONSE_PRESENT_ADDRESS_ENG = "";
    @SerializedName("RESPONSE_BIRTH_PLACE_ENG")
    @Expose
    private String RESPONSE_BIRTH_PLACE_ENG = "";

    public String getRESPONSE_PERMANENT_ADDRESS_ENG() {
        return RESPONSE_PERMANENT_ADDRESS_ENG;
    }

    public void setRESPONSE_PERMANENT_ADDRESS_ENG(String RESPONSE_PERMANENT_ADDRESS_ENG) {
        this.RESPONSE_PERMANENT_ADDRESS_ENG = RESPONSE_PERMANENT_ADDRESS_ENG;
    }

    public String getRESPONSE_PRESENT_ADDRESS_ENG() {
        return RESPONSE_PRESENT_ADDRESS_ENG;
    }

    public void setRESPONSE_PRESENT_ADDRESS_ENG(String RESPONSE_PRESENT_ADDRESS_ENG) {
        this.RESPONSE_PRESENT_ADDRESS_ENG = RESPONSE_PRESENT_ADDRESS_ENG;
    }

    public String getRESPONSE_BIRTH_PLACE_ENG() {
        return RESPONSE_BIRTH_PLACE_ENG;
    }

    public void setRESPONSE_BIRTH_PLACE_ENG(String RESPONSE_BIRTH_PLACE_ENG) {
        this.RESPONSE_BIRTH_PLACE_ENG = RESPONSE_BIRTH_PLACE_ENG;
    }

    public String getRESPONSE_NAME_ENG() {
        return RESPONSE_NAME_ENG;
    }

    public void setRESPONSE_NAME_ENG(String RESPONSE_NAME_ENG) {
        this.RESPONSE_NAME_ENG = RESPONSE_NAME_ENG;
    }

    public String getRESPONSE_FATHER_HUSBAND_NAME_ENG() {
        return RESPONSE_FATHER_HUSBAND_NAME_ENG;
    }

    public void setRESPONSE_FATHER_HUSBAND_NAME_ENG(String RESPONSE_FATHER_HUSBAND_NAME_ENG) {
        this.RESPONSE_FATHER_HUSBAND_NAME_ENG = RESPONSE_FATHER_HUSBAND_NAME_ENG;
    }

    @SerializedName("user_login")
    @Expose
    private String user_login = "";
    @SerializedName("RESPONSE_NAME_ENG")
    @Expose
    private String RESPONSE_NAME_ENG = "";
    @SerializedName("RESPONSE_FATHER_HUSBAND_NAME_ENG")
    @Expose
    private String RESPONSE_FATHER_HUSBAND_NAME_ENG = "";
    @SerializedName("ID_DOCUMENT_NO")
    @Expose
    private String ID_DOCUMENT_NO = "";
    @SerializedName("BIOTRACKING_ID")
    @Expose
    private String BIOTRACKING_ID = "";
    @SerializedName("TRACKING_ID")
    @Expose
    private String TRACKING_ID = "";
    @SerializedName("CHANNEL_ID")
    @Expose
    private String CHANNEL_ID = "";

    public String getID_DOCUMENT_NO() {
        return ID_DOCUMENT_NO;
    }

    public void setID_DOCUMENT_NO(String ID_DOCUMENT_NO) {
        this.ID_DOCUMENT_NO = ID_DOCUMENT_NO;
    }

    @SerializedName("DATETIME")
    @Expose
    private String DATETIME = "";
    @SerializedName("TRANSACTION_ID")
    @Expose
    private String TRANSACTION_ID = "";
    @SerializedName("USER_ID")
    @Expose
    private String USER_ID = "";
    @SerializedName("BRANCH_CODE")
    @Expose
    private String BRANCH_CODE = "";
    @SerializedName("BRANCH_NAME")
    @Expose
    private String BRANCH_NAME = "";
    @SerializedName("BRANCH_ADDRESS")
    @Expose
    private String BRANCH_ADDRESS = "";
    @SerializedName("BRANCH_TYPE")
    @Expose
    private String BRANCH_TYPE = "";
    @SerializedName("ROLE")
    @Expose
    private String ROLE = "";
    @SerializedName("URLTYPE")
    @Expose
    private String URLTYPE = "";
    @SerializedName("SERVICE_TYPE")
    @Expose
    private String SERVICE_TYPE = "";
    @SerializedName("SESSION_ID")
    @Expose
    private String SESSION_ID = "";
    @SerializedName("CITIZEN_NUMBER")
    @Expose
    private String CITIZEN_NUMBER = "";
    @SerializedName("MOBILE_OPERATOR_CODE")
    @Expose
    private String MOBILE_OPERATOR_CODE = "";
    @SerializedName("MOBILE_NO")
    @Expose
    public String MOBILE_NO = "";
    @SerializedName("CONTACT_NUMBER")
    @Expose
    private String CONTACT_NUMBER = "";
    @SerializedName("BIOPURPOSE")
    @Expose
    private ArrayList<String> BIOPURPOSE = null;
    @SerializedName("FINGER_INDEX")
    @Expose
    private String FINGER_INDEX = "";
    @SerializedName("TEMPLATE_TYPE")
    @Expose
    private String TEMPLATE_TYPE = "";
    @SerializedName("FINGER_TEMPLATE")
    @Expose
    private String FINGER_TEMPLATE = "";

    public String getFINGER_TEMPLATE() {
        return FINGER_TEMPLATE;
    }

    public void setFINGER_TEMPLATE(String FINGER_TEMPLATE) {
        this.FINGER_TEMPLATE = FINGER_TEMPLATE;
    }

    @SerializedName("PROVINCE_CODE")
    @Expose
    private String PROVINCE_CODE = "";
    @SerializedName("PROVINCE_DESC")
    @Expose
    private String PROVINCE_DESC = "";
    @SerializedName("ACCOUNT_BAT_CODE")
    @Expose
    private String ACCOUNT_BAT_CODE = "";
    @SerializedName("ACCOUNT_BAT_NAME")
    @Expose
    private String ACCOUNT_BAT_NAME = "";
    @SerializedName("OLDER_DAYS")
    @Expose
    private String OLDER_DAYS = "";
    @SerializedName("NADRA_VERISYS_CHECK")
    @Expose
    private String NADRA_VERISYS_CHECK = "";
    @SerializedName("RESPONSE_TRANSACTION_ID")
    @Expose
    private String RESPONSE_TRANSACTION_ID = "";
    @SerializedName("RESPONSE_CODE")
    @Expose
    private String RESPONSE_CODE = "";
    @SerializedName("RESPONSE_MESSAGE")
    @Expose
    private String RESPONSE_MESSAGE = "";
    @SerializedName("RESPONSE_SESSION_ID")
    @Expose
    private String RESPONSE_SESSION_ID = "";
    @SerializedName("RESPONSE_CITIZEN_NUMBER")
    @Expose
    private String RESPONSE_CITIZEN_NUMBER = "";
    @SerializedName("RESPONSE_NAME")
    @Expose
    private String RESPONSE_NAME = "";
    @SerializedName("RESPONSE_FATHER_HUSBAND_NAME")
    @Expose
    private String RESPONSE_FATHER_HUSBAND_NAME = "";
    @SerializedName("RESPONSE_PRESENT_ADDRESS")
    @Expose
    private String RESPONSE_PRESENT_ADDRESS = "";
    @SerializedName("RESPONSE_PERMANENT_ADDRESS")
    @Expose
    private String RESPONSE_PERMANENT_ADDRESS = "";
    @SerializedName("RESPONSE_DATE_OF_BIRTH")
    @Expose
    private String RESPONSE_DATE_OF_BIRTH = "";
    @SerializedName("RESPONSE_BIRTH_PLACE")
    @Expose
    private String RESPONSE_BIRTH_PLACE = "";
    @SerializedName("RESPONSE_PHOTOGRAPH")
    @Expose
    public String RESPONSE_PHOTOGRAPH = "";
    @SerializedName("RESPONSE_EXPIRY_DATE")
    @Expose
    private String RESPONSE_EXPIRY_DATE = "";
    @SerializedName("RESPONSE_FINGER_INDEX")
    @Expose
    private RESPONSEFINGERINDEX RESPONSE_FINGER_INDEX;
    @SerializedName("RESPONSE_CARD_TYPE")
    @Expose
    private String RESPONSE_CARD_TYPE = "";
    @SerializedName("VERISYS_LOCATION")
    @Expose
    private String VERISYS_LOCATION = "";
    @SerializedName("_id")
    @Expose
    private String _id = "";
    public final static Creator<NadraVerify> CREATOR = new Creator<NadraVerify>() {


        @SuppressWarnings({
                "unchecked"
        })
        public NadraVerify createFromParcel(Parcel in) {
            return new NadraVerify(in);
        }

        public NadraVerify[] newArray(int size) {
            return (new NadraVerify[size]);
        }

    };
    private final static long serialVersionUID = 7097308964367234492L;

    protected NadraVerify(Parcel in) {
        this.user_login = ((String) in.readValue((String.class.getClassLoader())));
        this.BIOTRACKING_ID = ((String) in.readValue((String.class.getClassLoader())));
        this.TRACKING_ID = ((String) in.readValue((String.class.getClassLoader())));
        this.CHANNEL_ID = ((String) in.readValue((String.class.getClassLoader())));
        this.DATETIME = ((String) in.readValue((String.class.getClassLoader())));
        this.TRANSACTION_ID = ((String) in.readValue((String.class.getClassLoader())));
        this.USER_ID = ((String) in.readValue((String.class.getClassLoader())));
        this.BRANCH_CODE = ((String) in.readValue((String.class.getClassLoader())));
        this.BRANCH_NAME = ((String) in.readValue((String.class.getClassLoader())));
        this.BRANCH_ADDRESS = ((String) in.readValue((String.class.getClassLoader())));
        this.BRANCH_TYPE = ((String) in.readValue((String.class.getClassLoader())));
        this.ROLE = ((String) in.readValue((String.class.getClassLoader())));
        this.URLTYPE = ((String) in.readValue((String.class.getClassLoader())));
        this.SERVICE_TYPE = ((String) in.readValue((String.class.getClassLoader())));
        this.SESSION_ID = ((String) in.readValue((String.class.getClassLoader())));
        this.CITIZEN_NUMBER = ((String) in.readValue((String.class.getClassLoader())));
        this.MOBILE_OPERATOR_CODE = ((String) in.readValue((String.class.getClassLoader())));
        this.MOBILE_NO = ((String) in.readValue((String.class.getClassLoader())));
        this.CONTACT_NUMBER = ((String) in.readValue((String.class.getClassLoader())));
        this.BIOPURPOSE = ((ArrayList<String>) in.readValue((String.class.getClassLoader())));
        this.FINGER_INDEX = ((String) in.readValue((String.class.getClassLoader())));
        this.TEMPLATE_TYPE = ((String) in.readValue((String.class.getClassLoader())));
        this.PROVINCE_CODE = ((String) in.readValue((String.class.getClassLoader())));
        this.PROVINCE_DESC = ((String) in.readValue((String.class.getClassLoader())));
        this.ACCOUNT_BAT_CODE = ((String) in.readValue((String.class.getClassLoader())));
        this.ACCOUNT_BAT_NAME = ((String) in.readValue((String.class.getClassLoader())));
        this.OLDER_DAYS = ((String) in.readValue((String.class.getClassLoader())));
        this.NADRA_VERISYS_CHECK = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_TRANSACTION_ID = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_CODE = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_MESSAGE = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_SESSION_ID = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_CITIZEN_NUMBER = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_NAME = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_FATHER_HUSBAND_NAME = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_PRESENT_ADDRESS = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_PERMANENT_ADDRESS = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_DATE_OF_BIRTH = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_BIRTH_PLACE = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_PHOTOGRAPH = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_EXPIRY_DATE = ((String) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_FINGER_INDEX = ((RESPONSEFINGERINDEX) in.readValue((String.class.getClassLoader())));
        this.RESPONSE_CARD_TYPE = ((String) in.readValue((String.class.getClassLoader())));
        this.VERISYS_LOCATION = ((String) in.readValue((String.class.getClassLoader())));
        this._id = ((String) in.readValue((String.class.getClassLoader())));
    }

    public NadraVerify() {
    }

    public String getBIOTRACKINGID() {
        return BIOTRACKING_ID;
    }

    public void setBIOTRACKINGID(String bIOTRACKINGID) {
        this.BIOTRACKING_ID = bIOTRACKINGID;
    }

    public String getTRACKINGID() {
        return TRACKING_ID;
    }

    public void setTRACKINGID(String tRACKINGID) {
        this.TRACKING_ID = tRACKINGID;
    }

    public String getCHANNELID() {
        return CHANNEL_ID;
    }

    public void setCHANNELID(String cHANNELID) {
        this.CHANNEL_ID = cHANNELID;
    }

    public String getDATETIME() {
        return DATETIME;
    }

    public void setDATETIME(String dATETIME) {
        this.DATETIME = dATETIME;
    }

    public String getTRANSACTIONID() {
        return TRANSACTION_ID;
    }

    public void setTRANSACTIONID(String tRANSACTIONID) {
        this.TRANSACTION_ID = tRANSACTIONID;
    }

    public String getUSERID() {
        return USER_ID;
    }

    public void setUSERID(String uSERID) {
        this.USER_ID = uSERID;
    }

    public String getBRANCHCODE() {
        return BRANCH_CODE;
    }

    public void setBRANCHCODE(String bRANCHCODE) {
        this.BRANCH_CODE = bRANCHCODE;
    }

    public String getBRANCHNAME() {
        return BRANCH_NAME;
    }

    public void setBRANCHNAME(String bRANCHNAME) {
        this.BRANCH_NAME = bRANCHNAME;
    }

    public String getBRANCHADDRESS() {
        return BRANCH_ADDRESS;
    }

    public void setBRANCHADDRESS(String bRANCHADDRESS) {
        this.BRANCH_ADDRESS = bRANCHADDRESS;
    }

    public String getBRANCHTYPE() {
        return BRANCH_TYPE;
    }

    public void setBRANCHTYPE(String bRANCHTYPE) {
        this.BRANCH_TYPE = bRANCHTYPE;
    }

    public String getROLE() {
        return ROLE;
    }

    public void setROLE(String rOLE) {
        this.ROLE = rOLE;
    }

    public String getURLTYPE() {
        return URLTYPE;
    }

    public void setURLTYPE(String uRLTYPE) {
        this.URLTYPE = uRLTYPE;
    }

    public String getSERVICETYPE() {
        return SERVICE_TYPE;
    }

    public void setSERVICETYPE(String sERVICETYPE) {
        this.SERVICE_TYPE = sERVICETYPE;
    }

    public String getSESSIONID() {
        return SESSION_ID;
    }

    public void setSESSIONID(String sESSIONID) {
        this.SESSION_ID = sESSIONID;
    }

    public String getCITIZENNUMBER() {
        return CITIZEN_NUMBER;
    }

    public void setCITIZENNUMBER(String cITIZENNUMBER) {
        this.CITIZEN_NUMBER = cITIZENNUMBER;
    }

    public String getMOBILEOPERATORCODE() {
        return MOBILE_OPERATOR_CODE;
    }

    public void setMOBILEOPERATORCODE(String mOBILEOPERATORCODE) {
        this.MOBILE_OPERATOR_CODE = mOBILEOPERATORCODE;
    }

    public String getMOBILENO() {
        return MOBILE_NO;
    }

    public void setMOBILENO(String mOBILENO) {
        this.MOBILE_NO = mOBILENO;
    }

    public String getUser_login() {
        return user_login;
    }

    public void setUser_login(String user_login) {
        this.user_login = user_login;
    }

    public String getCONTACTNUMBER() {
        return CONTACT_NUMBER;
    }

    public void setCONTACTNUMBER(String cONTACTNUMBER) {
        this.CONTACT_NUMBER = cONTACTNUMBER;
    }

    public ArrayList<String> getBIOPURPOSE() {
        return BIOPURPOSE;
    }

    public void setBIOPURPOSE(ArrayList<String> bIOPURPOSE) {
        this.BIOPURPOSE = bIOPURPOSE;
    }

    public String getFINGERINDEX() {
        return FINGER_INDEX;
    }

    public void setFINGERINDEX(String fINGERINDEX) {
        this.FINGER_INDEX = fINGERINDEX;
    }

    public String getTEMPLATETYPE() {
        return TEMPLATE_TYPE;
    }

    public void setTEMPLATETYPE(String tEMPLATETYPE) {
        this.TEMPLATE_TYPE = tEMPLATETYPE;
    }

    public String getPROVINCECODE() {
        return PROVINCE_CODE;
    }

    public void setPROVINCECODE(String pROVINCECODE) {
        this.PROVINCE_CODE = pROVINCECODE;
    }

    public String getPROVINCEDESC() {
        return PROVINCE_DESC;
    }

    public void setPROVINCEDESC(String pROVINCEDESC) {
        this.PROVINCE_DESC = pROVINCEDESC;
    }

    public String getACCOUNTBATCODE() {
        return ACCOUNT_BAT_CODE;
    }

    public void setACCOUNTBATCODE(String aCCOUNTBATCODE) {
        this.ACCOUNT_BAT_CODE = aCCOUNTBATCODE;
    }

    public String getACCOUNTBATNAME() {
        return ACCOUNT_BAT_NAME;
    }

    public void setACCOUNTBATNAME(String aCCOUNTBATNAME) {
        this.ACCOUNT_BAT_NAME = aCCOUNTBATNAME;
    }

    public String getOLDERDAYS() {
        return OLDER_DAYS;
    }

    public void setOLDERDAYS(String oLDERDAYS) {
        this.OLDER_DAYS = oLDERDAYS;
    }

    public String getNADRAVERISYSCHECK() {
        return NADRA_VERISYS_CHECK;
    }

    public void setNADRAVERISYSCHECK(String nADRAVERISYSCHECK) {
        this.NADRA_VERISYS_CHECK = nADRAVERISYSCHECK;
    }

    public String getRESPONSETRANSACTIONID() {
        return RESPONSE_TRANSACTION_ID;
    }

    public void setRESPONSETRANSACTIONID(String rESPONSETRANSACTIONID) {
        this.RESPONSE_TRANSACTION_ID = rESPONSETRANSACTIONID;
    }

    public String getRESPONSECODE() {
        return RESPONSE_CODE;
    }

    public void setRESPONSECODE(String rESPONSECODE) {
        this.RESPONSE_CODE = rESPONSECODE;
    }

    public String getRESPONSEMESSAGE() {
        return RESPONSE_MESSAGE;
    }

    public void setRESPONSEMESSAGE(String rESPONSEMESSAGE) {
        this.RESPONSE_MESSAGE = rESPONSEMESSAGE;
    }

    public String getRESPONSESESSIONID() {
        return RESPONSE_SESSION_ID;
    }

    public void setRESPONSESESSIONID(String rESPONSESESSIONID) {
        this.RESPONSE_SESSION_ID = rESPONSESESSIONID;
    }

    public String getRESPONSECITIZENNUMBER() {
        return RESPONSE_CITIZEN_NUMBER;
    }

    public void setRESPONSECITIZENNUMBER(String rESPONSECITIZENNUMBER) {
        this.RESPONSE_CITIZEN_NUMBER = rESPONSECITIZENNUMBER;
    }

    public String getRESPONSENAME() {
        return RESPONSE_NAME;
    }

    public void setRESPONSENAME(String rESPONSENAME) {
        this.RESPONSE_NAME = rESPONSENAME;
    }

    public String getRESPONSEFATHERHUSBANDNAME() {
        return RESPONSE_FATHER_HUSBAND_NAME;
    }

    public void setRESPONSEFATHERHUSBANDNAME(String rESPONSEFATHERHUSBANDNAME) {
        this.RESPONSE_FATHER_HUSBAND_NAME = rESPONSEFATHERHUSBANDNAME;
    }

    public String getRESPONSEPRESENTADDRESS() {
        return RESPONSE_PRESENT_ADDRESS;
    }

    public void setRESPONSEPRESENTADDRESS(String rESPONSEPRESENTADDRESS) {
        this.RESPONSE_PRESENT_ADDRESS = rESPONSEPRESENTADDRESS;
    }

    public String getRESPONSEPERMANENTADDRESS() {
        return RESPONSE_PERMANENT_ADDRESS;
    }

    public void setRESPONSEPERMANENTADDRESS(String rESPONSEPERMANENTADDRESS) {
        this.RESPONSE_PERMANENT_ADDRESS = rESPONSEPERMANENTADDRESS;
    }

    public String getRESPONSEDATEOFBIRTH() {
        return RESPONSE_DATE_OF_BIRTH;
    }

    public void setRESPONSEDATEOFBIRTH(String rESPONSEDATEOFBIRTH) {
        this.RESPONSE_DATE_OF_BIRTH = rESPONSEDATEOFBIRTH;
    }

    public String getRESPONSEBIRTHPLACE() {
        return RESPONSE_BIRTH_PLACE;
    }

    public void setRESPONSEBIRTHPLACE(String rESPONSEBIRTHPLACE) {
        this.RESPONSE_BIRTH_PLACE = rESPONSEBIRTHPLACE;
    }

    public String getRESPONSEPHOTOGRAPH() {
        return RESPONSE_PHOTOGRAPH;
    }

    public void setRESPONSEPHOTOGRAPH(String rESPONSEPHOTOGRAPH) {
        this.RESPONSE_PHOTOGRAPH = rESPONSEPHOTOGRAPH;
    }

    public String getRESPONSEEXPIRYDATE() {
        return RESPONSE_EXPIRY_DATE;
    }

    public void setRESPONSEEXPIRYDATE(String rESPONSEEXPIRYDATE) {
        this.RESPONSE_EXPIRY_DATE = rESPONSEEXPIRYDATE;
    }

    public RESPONSEFINGERINDEX getRESPONSEFINGERINDEX() {
        return RESPONSE_FINGER_INDEX;
    }

    public void setRESPONSEFINGERINDEX(RESPONSEFINGERINDEX rESPONSEFINGERINDEX) {
        this.RESPONSE_FINGER_INDEX = rESPONSEFINGERINDEX;
    }

    public String getRESPONSECARDTYPE() {
        return RESPONSE_CARD_TYPE;
    }

    public void setRESPONSECARDTYPE(String rESPONSECARDTYPE) {
        this.RESPONSE_CARD_TYPE = rESPONSECARDTYPE;
    }

    public String getVERISYSLOCATION() {
        return VERISYS_LOCATION;
    }

    public void setVERISYSLOCATION(String vERISYSLOCATION) {
        this.VERISYS_LOCATION = vERISYSLOCATION;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(user_login);
        dest.writeValue(BIOTRACKING_ID);
        dest.writeValue(TRACKING_ID);
        dest.writeValue(CHANNEL_ID);
        dest.writeValue(DATETIME);
        dest.writeValue(TRANSACTION_ID);
        dest.writeValue(USER_ID);
        dest.writeValue(BRANCH_CODE);
        dest.writeValue(BRANCH_NAME);
        dest.writeValue(BRANCH_ADDRESS);
        dest.writeValue(BRANCH_TYPE);
        dest.writeValue(ROLE);
        dest.writeValue(URLTYPE);
        dest.writeValue(SERVICE_TYPE);
        dest.writeValue(SESSION_ID);
        dest.writeValue(CITIZEN_NUMBER);
        dest.writeValue(MOBILE_OPERATOR_CODE);
        dest.writeValue(MOBILE_NO);
        dest.writeValue(CONTACT_NUMBER);
        dest.writeValue(BIOPURPOSE);
        dest.writeValue(FINGER_INDEX);
        dest.writeValue(TEMPLATE_TYPE);
        dest.writeValue(PROVINCE_CODE);
        dest.writeValue(PROVINCE_DESC);
        dest.writeValue(ACCOUNT_BAT_CODE);
        dest.writeValue(ACCOUNT_BAT_NAME);
        dest.writeValue(OLDER_DAYS);
        dest.writeValue(NADRA_VERISYS_CHECK);
        dest.writeValue(RESPONSE_TRANSACTION_ID);
        dest.writeValue(RESPONSE_CODE);
        dest.writeValue(RESPONSE_MESSAGE);
        dest.writeValue(RESPONSE_SESSION_ID);
        dest.writeValue(RESPONSE_CITIZEN_NUMBER);
        dest.writeValue(RESPONSE_NAME);
        dest.writeValue(RESPONSE_FATHER_HUSBAND_NAME);
        dest.writeValue(RESPONSE_PRESENT_ADDRESS);
        dest.writeValue(RESPONSE_PERMANENT_ADDRESS);
        dest.writeValue(RESPONSE_DATE_OF_BIRTH);
        dest.writeValue(RESPONSE_BIRTH_PLACE);
        dest.writeValue(RESPONSE_PHOTOGRAPH);
        dest.writeValue(RESPONSE_EXPIRY_DATE);
        dest.writeValue(RESPONSE_FINGER_INDEX);
        dest.writeValue(RESPONSE_CARD_TYPE);
        dest.writeValue(VERISYS_LOCATION);
        dest.writeValue(_id);
    }

    public int describeContents() {
        return 0;
    }

}
