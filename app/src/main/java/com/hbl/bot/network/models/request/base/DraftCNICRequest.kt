package com.hbl.bot.network.models.request.base

import com.google.gson.annotations.SerializedName

class DraftCNICRequest() {
    @SerializedName("DOC_ID")
    var DOC_ID : String = ""

    @SerializedName("identifier")
    var identifier: String = ""
}