package com.hbl.bot.network.models.response.baseRM

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName


class Batch {
    @SerializedName("itemDescription")
    @Expose
    var itemDescription = ""

}