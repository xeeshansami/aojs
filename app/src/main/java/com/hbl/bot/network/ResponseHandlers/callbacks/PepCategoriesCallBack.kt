package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PepCategoriesCallBack {
    fun PepCategoriesuccess(response: PepCategoriesResponse)
    fun PepCategoriesFailure(response: BaseResponse)
}