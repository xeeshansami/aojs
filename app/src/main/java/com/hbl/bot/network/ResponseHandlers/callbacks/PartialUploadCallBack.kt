package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PartialUploadCallBack {
    fun PartialUploadSuccess(response: PartialUploadResponse)
    fun PartialUploadFailure(response: BaseResponse)
}