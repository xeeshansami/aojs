package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ConfigCallBack {
    fun ConfigSuccess(response: ConfigResponse)
    fun ConfigFailure(response: BaseResponse)
}