
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PathFile {

    @SerializedName("id")
    @Expose
    private int id;


    @SerializedName("istagged")
    @Expose
    private boolean istagged;


    @SerializedName("taggedBy")
    @Expose
    private String taggedBy = "";


    @SerializedName("seq")
    @Expose
    private int seq;


}
