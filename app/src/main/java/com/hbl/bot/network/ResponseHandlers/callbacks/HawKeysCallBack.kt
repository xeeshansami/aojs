package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface HawKeysCallBack {
    fun Success(response: HawKeysResponse)
    fun Failure(response: BaseResponse)
}