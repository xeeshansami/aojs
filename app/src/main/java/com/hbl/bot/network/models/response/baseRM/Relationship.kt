package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Relationship() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("RELATION_CODE")
    @Expose
    var RELATION_CODE: String? = null

    @SerializedName("RELATION_DESC")
    @Expose
    var RELATION_DESC: String? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<Relationship> {
        override fun createFromParcel(parcel: Parcel): Relationship {
            return Relationship(parcel)
        }

        override fun newArray(size: Int): Array<Relationship?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return RELATION_DESC.toString()
    }
}