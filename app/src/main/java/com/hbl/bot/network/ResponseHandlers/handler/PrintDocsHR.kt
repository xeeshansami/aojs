package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class PrintDocsHR(callBack: PrintDocCallBack) : BaseRH<PrintDocsResponse>() {
    var callback: PrintDocCallBack = callBack
    override fun onSuccess(response: PrintDocsResponse?) {
        response?.let { callback.PrintDocSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PrintDocFailure(it) }
    }
}