package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class HawKeys() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("NAME")
    @Expose
    var NAME: String? = null

    @SerializedName("LOV_CODE")
    @Expose
    public var LOV_CODE: String? = null

    @SerializedName("LOV_DESC")
    @Expose
    public var LOV_DESC : String? = null

    @SerializedName("IDENTIFIER")
    @Expose
    public var IDENTIFIER : String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<HawKeys> {
        override fun createFromParcel(parcel: Parcel): HawKeys {
            return HawKeys(parcel)
        }

        override fun newArray(size: Int): Array<HawKeys?> {
            return arrayOfNulls(size)
        }
    }


}