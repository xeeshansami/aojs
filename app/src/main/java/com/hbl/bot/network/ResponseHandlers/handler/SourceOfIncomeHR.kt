package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class SourceOfIncomeHR(callBack: SourceOfIncomeCallBack) : BaseRH<SourceOfIncomeResponse>() {
    var callback: SourceOfIncomeCallBack = callBack
    override fun onSuccess(response: SourceOfIncomeResponse?) {
        response?.let { callback.SourceOfIncomSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.SourceOfIncomFailure(it) }
    }
}