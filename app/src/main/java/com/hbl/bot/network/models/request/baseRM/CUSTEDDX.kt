package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class CUSTEDDX(
) : Parcelable {
    var ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK: String? = null
    var AGRI_INCOME: String? = null
    var AGRI_INCOME_EXP_AMT: String? = null
    var AGRI_RENTAL_INCOME: String? = null
    var AGRI_RENTAL_INCOME_EXP_AMT: String? = null
    var AMOUNT_OF_FOREIGN_TRANSACTION: String? = null
    var AMOUNT_OF_LOCAL_TRANSACTION: String? = null
    var APPROVED_BY_DATE: String? = null
    var APPROVED_BY_NAME: String? = null
    var APPROVED_COMPL_AUTH_DATE: String? = null
    var APPROVED_COMPL_AUTH_NAME: String? = null
    var AVG_MTHLY_BAL_IN_THOSE_ACCTS: String? = null
    var BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY: String? = null
    var BUSINESS_INCOME: String? = null
    var BUSINESS_INCOME_EXP_AMT: String? = null
    var BUSS_INCOME_FIN_SUPPORTER: String? = null
    var BUSS_INCOME_FIN_SUPPORTER_EXP_AMT: String? = null
    var CONDUCT_FOREIGN_BANK_TRANSFER: String? = null
    var CONDUCT_LOCAL_BANK_TRANSFER: String? = null
    var COUNTRY_CODE_INWARD_FOREIGN1: String? = null
    var COUNTRY_CODE_INWARD_FOREIGN2: String? = null
    var COUNTRY_CODE_INWARD_FOREIGN3: String? = null
    var COUNTRY_CODE_INWARD_LOCAL1: String? = null
    var COUNTRY_CODE_INWARD_LOCAL2: String? = null
    var COUNTRY_CODE_INWARD_LOCAL3: String? = null
    var COUNTRY_CODE_OUTWARD_FOREIGN1: String? = null
    var COUNTRY_CODE_OUTWARD_FOREIGN2: String? = null
    var COUNTRY_CODE_OUTWARD_FOREIGN3: String? = null
    var COUNTRY_CODE_OUTWARD_LOCAL1: String? = null
    var COUNTRY_CODE_OUTWARD_LOCAL2: String? = null
    var COUNTRY_CODE_OUTWARD_LOCAL3: String? = null
    var COUNTRY_DESC_INWARD_FOREIGN1: String? = null
    var COUNTRY_DESC_INWARD_FOREIGN2: String? = null
    var COUNTRY_DESC_INWARD_FOREIGN3: String? = null
    var COUNTRY_DESC_INWARD_LOCAL1: String? = null
    var COUNTRY_DESC_INWARD_LOCAL2: String? = null
    var COUNTRY_DESC_INWARD_LOCAL3: String? = null
    var COUNTRY_DESC_OUTWARD_FOREIGN1: String? = null
    var COUNTRY_DESC_OUTWARD_FOREIGN2: String? = null
    var COUNTRY_DESC_OUTWARD_FOREIGN3: String? = null
    var COUNTRY_DESC_OUTWARD_LOCAL1: String? = null
    var COUNTRY_DESC_OUTWARD_LOCAL2: String? = null
    var COUNTRY_DESC_OUTWARD_LOCAL3: String? = null
    var DESC_NATIONALITY: String? = null
    var DESC_REASON_EDD: String? = null
    var DESC_RELATIONSHIP_WITH_FIN: String? = null
    var EDD_CONDUCT_BY_DATE: String? = null
    var EDD_CONDUCT_BY_NAME: String? = null
    var FIN_SUPP_GUR_FMLY_PEP: String? = null
    var FOREIGN_CONTACT_NO: String? = null
    var FOREIGN_CONTACT_NO_HIGH_RISK_NRP: String? = null
    var FOREIGN_OFFICE_ADDRESS_1: String? = null
    var FOREIGN_OFFICE_ADDRESS_2: String? = null
    var FOREIGN_RESIDENT_ADDRESS_1: String? = null
    var FOREIGN_RESIDENT_ADDRESS_2: String? = null
    var ID_NO: String? = null
    var INHERITANCE: String? = null
    var INHERITANCE_EXP_AMT: String? = null
    var IS_HIGH_RISK_SEGMENT: String? = null
    var IS_NON_RESIDENT_INFORMATION: String? = null
    var IS_THE_FINANCIAL_SUPPORTER: String? = null
    var NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1: String? = null
    var NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2: String? = null
    var NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3: String? = null
    var NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1: String? = null
    var NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2: String? = null
    var NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3: String? = null
    var NAME_OF_INSTITUTION_1: String? = null
    var NAME_OF_INSTITUTION_2: String? = null
    var NAME_OF_INSTITUTION_3: String? = null
    var NATIONALITY: String? = null
    var OFFICE_ADDRESS_FOREIGN_COUNTRY: String? = null
    var OTHER: String? = null
    var OTHER_EXP_AMT: String? = null
    var OTHER_SOURCE: String? = null
    var OTHER_SOURCE_2: String? = null
    var OTHER_SOURCE_NRP: String? = null
    var PENSION: String? = null
    var PENSION_EXP_AMT: String? = null
    var PURPOSE_OF_FOREIGN_TRANSACTION: String? = null
    var PURPOSE_OF_LOCAL_TRANSACTION: String? = null
    var REASON_EDD: String? = null
    var RELATIONSHIP_FOREIGN_BENEFICIARY_1: String? = null
    var RELATIONSHIP_FOREIGN_BENEFICIARY_2: String? = null
    var RELATIONSHIP_FOREIGN_BENEFICIARY_3: String? = null
    var RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1: String? = null
    var RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2: String? = null
    var RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3: String? = null
    var RELATIONSHIP_LOCAL_BENEFICIARY_1: String? = null
    var RELATIONSHIP_LOCAL_BENEFICIARY_2: String? = null
    var RELATIONSHIP_LOCAL_BENEFICIARY_3: String? = null
    var RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1: String? = null
    var RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2: String? = null
    var RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3: String? = null
    var RELATIONSHIP_WITH_FIN: String? = null
    var RENTAL_INCOME: String? = null
    var RENTAL_INCOME_EXP_AMT: String? = null
    var RENTAL_INCOME_FIN_SUPPORTER: String? = null
    var RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT: String? = null
    var RENTAL_INCOME_RES_PROJ: String? = null
    var RENTAL_INCOME_RES_PROJ_EXP_AMT: String? = null
    var RESIDENTIAL_ADDRESS: String? = null
    var RETIREMENT_FUND: String? = null
    var RETIREMENT_FUND_EXP_AMT: String? = null
    var SAL_OF_FIN_SUPPORTER: String? = null
    var SAL_OF_FIN_SUPPORTER_EXP_AMT: String? = null

    constructor(parcel: Parcel) : this() {
        ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK = parcel.readString()
        AGRI_INCOME = parcel.readString()
        AGRI_INCOME_EXP_AMT = parcel.readString()
        AGRI_RENTAL_INCOME = parcel.readString()
        AGRI_RENTAL_INCOME_EXP_AMT = parcel.readString()
        AMOUNT_OF_FOREIGN_TRANSACTION = parcel.readString()
        AMOUNT_OF_LOCAL_TRANSACTION = parcel.readString()
        APPROVED_BY_DATE = parcel.readString()
        APPROVED_BY_NAME = parcel.readString()
        APPROVED_COMPL_AUTH_DATE = parcel.readString()
        APPROVED_COMPL_AUTH_NAME = parcel.readString()
        AVG_MTHLY_BAL_IN_THOSE_ACCTS = parcel.readString()
        BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY = parcel.readString()
        BUSINESS_INCOME = parcel.readString()
        BUSINESS_INCOME_EXP_AMT = parcel.readString()
        BUSS_INCOME_FIN_SUPPORTER = parcel.readString()
        BUSS_INCOME_FIN_SUPPORTER_EXP_AMT = parcel.readString()
        CONDUCT_FOREIGN_BANK_TRANSFER = parcel.readString()
        CONDUCT_LOCAL_BANK_TRANSFER = parcel.readString()
        COUNTRY_CODE_INWARD_FOREIGN1 = parcel.readString()
        COUNTRY_CODE_INWARD_FOREIGN2 = parcel.readString()
        COUNTRY_CODE_INWARD_FOREIGN3 = parcel.readString()
        COUNTRY_CODE_INWARD_LOCAL1 = parcel.readString()
        COUNTRY_CODE_INWARD_LOCAL2 = parcel.readString()
        COUNTRY_CODE_INWARD_LOCAL3 = parcel.readString()
        COUNTRY_CODE_OUTWARD_FOREIGN1 = parcel.readString()
        COUNTRY_CODE_OUTWARD_FOREIGN2 = parcel.readString()
        COUNTRY_CODE_OUTWARD_FOREIGN3 = parcel.readString()
        COUNTRY_CODE_OUTWARD_LOCAL1 = parcel.readString()
        COUNTRY_CODE_OUTWARD_LOCAL2 = parcel.readString()
        COUNTRY_CODE_OUTWARD_LOCAL3 = parcel.readString()
        COUNTRY_DESC_INWARD_FOREIGN1 = parcel.readString()
        COUNTRY_DESC_INWARD_FOREIGN2 = parcel.readString()
        COUNTRY_DESC_INWARD_FOREIGN3 = parcel.readString()
        COUNTRY_DESC_INWARD_LOCAL1 = parcel.readString()
        COUNTRY_DESC_INWARD_LOCAL2 = parcel.readString()
        COUNTRY_DESC_INWARD_LOCAL3 = parcel.readString()
        COUNTRY_DESC_OUTWARD_FOREIGN1 = parcel.readString()
        COUNTRY_DESC_OUTWARD_FOREIGN2 = parcel.readString()
        COUNTRY_DESC_OUTWARD_FOREIGN3 = parcel.readString()
        COUNTRY_DESC_OUTWARD_LOCAL1 = parcel.readString()
        COUNTRY_DESC_OUTWARD_LOCAL2 = parcel.readString()
        COUNTRY_DESC_OUTWARD_LOCAL3 = parcel.readString()
        DESC_NATIONALITY = parcel.readString()
        DESC_REASON_EDD = parcel.readString()
        DESC_RELATIONSHIP_WITH_FIN = parcel.readString()
        EDD_CONDUCT_BY_DATE = parcel.readString()
        EDD_CONDUCT_BY_NAME = parcel.readString()
        FIN_SUPP_GUR_FMLY_PEP = parcel.readString()
        FOREIGN_CONTACT_NO = parcel.readString()
        FOREIGN_CONTACT_NO_HIGH_RISK_NRP = parcel.readString()
        FOREIGN_OFFICE_ADDRESS_1 = parcel.readString()
        FOREIGN_OFFICE_ADDRESS_2 = parcel.readString()
        FOREIGN_RESIDENT_ADDRESS_1 = parcel.readString()
        FOREIGN_RESIDENT_ADDRESS_2 = parcel.readString()
        ID_NO = parcel.readString()
        INHERITANCE = parcel.readString()
        INHERITANCE_EXP_AMT = parcel.readString()
        IS_HIGH_RISK_SEGMENT = parcel.readString()
        IS_NON_RESIDENT_INFORMATION = parcel.readString()
        IS_THE_FINANCIAL_SUPPORTER = parcel.readString()
        NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1 = parcel.readString()
        NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2 = parcel.readString()
        NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3 = parcel.readString()
        NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1 = parcel.readString()
        NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2 = parcel.readString()
        NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3 = parcel.readString()
        NAME_OF_INSTITUTION_1 = parcel.readString()
        NAME_OF_INSTITUTION_2 = parcel.readString()
        NAME_OF_INSTITUTION_3 = parcel.readString()
        NATIONALITY = parcel.readString()
        OFFICE_ADDRESS_FOREIGN_COUNTRY = parcel.readString()
        OTHER = parcel.readString()
        OTHER_EXP_AMT = parcel.readString()
        OTHER_SOURCE = parcel.readString()
        OTHER_SOURCE_2 = parcel.readString()
        OTHER_SOURCE_NRP = parcel.readString()
        PENSION = parcel.readString()
        PENSION_EXP_AMT = parcel.readString()
        PURPOSE_OF_FOREIGN_TRANSACTION = parcel.readString()
        PURPOSE_OF_LOCAL_TRANSACTION = parcel.readString()
        REASON_EDD = parcel.readString()
        RELATIONSHIP_FOREIGN_BENEFICIARY_1 = parcel.readString()
        RELATIONSHIP_FOREIGN_BENEFICIARY_2 = parcel.readString()
        RELATIONSHIP_FOREIGN_BENEFICIARY_3 = parcel.readString()
        RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1 = parcel.readString()
        RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2 = parcel.readString()
        RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3 = parcel.readString()
        RELATIONSHIP_LOCAL_BENEFICIARY_1 = parcel.readString()
        RELATIONSHIP_LOCAL_BENEFICIARY_2 = parcel.readString()
        RELATIONSHIP_LOCAL_BENEFICIARY_3 = parcel.readString()
        RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1 = parcel.readString()
        RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2 = parcel.readString()
        RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3 = parcel.readString()
        RELATIONSHIP_WITH_FIN = parcel.readString()
        RENTAL_INCOME = parcel.readString()
        RENTAL_INCOME_EXP_AMT = parcel.readString()
        RENTAL_INCOME_FIN_SUPPORTER = parcel.readString()
        RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT = parcel.readString()
        RENTAL_INCOME_RES_PROJ = parcel.readString()
        RENTAL_INCOME_RES_PROJ_EXP_AMT = parcel.readString()
        RESIDENTIAL_ADDRESS = parcel.readString()
        RETIREMENT_FUND = parcel.readString()
        RETIREMENT_FUND_EXP_AMT = parcel.readString()
        SAL_OF_FIN_SUPPORTER = parcel.readString()
        SAL_OF_FIN_SUPPORTER_EXP_AMT = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK)
        parcel.writeString(AGRI_INCOME)
        parcel.writeString(AGRI_INCOME_EXP_AMT)
        parcel.writeString(AGRI_RENTAL_INCOME)
        parcel.writeString(AGRI_RENTAL_INCOME_EXP_AMT)
        parcel.writeString(AMOUNT_OF_FOREIGN_TRANSACTION)
        parcel.writeString(AMOUNT_OF_LOCAL_TRANSACTION)
        parcel.writeString(APPROVED_BY_DATE)
        parcel.writeString(APPROVED_BY_NAME)
        parcel.writeString(APPROVED_COMPL_AUTH_DATE)
        parcel.writeString(APPROVED_COMPL_AUTH_NAME)
        parcel.writeString(AVG_MTHLY_BAL_IN_THOSE_ACCTS)
        parcel.writeString(BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY)
        parcel.writeString(BUSINESS_INCOME)
        parcel.writeString(BUSINESS_INCOME_EXP_AMT)
        parcel.writeString(BUSS_INCOME_FIN_SUPPORTER)
        parcel.writeString(BUSS_INCOME_FIN_SUPPORTER_EXP_AMT)
        parcel.writeString(CONDUCT_FOREIGN_BANK_TRANSFER)
        parcel.writeString(CONDUCT_LOCAL_BANK_TRANSFER)
        parcel.writeString(COUNTRY_CODE_INWARD_FOREIGN1)
        parcel.writeString(COUNTRY_CODE_INWARD_FOREIGN2)
        parcel.writeString(COUNTRY_CODE_INWARD_FOREIGN3)
        parcel.writeString(COUNTRY_CODE_INWARD_LOCAL1)
        parcel.writeString(COUNTRY_CODE_INWARD_LOCAL2)
        parcel.writeString(COUNTRY_CODE_INWARD_LOCAL3)
        parcel.writeString(COUNTRY_CODE_OUTWARD_FOREIGN1)
        parcel.writeString(COUNTRY_CODE_OUTWARD_FOREIGN2)
        parcel.writeString(COUNTRY_CODE_OUTWARD_FOREIGN3)
        parcel.writeString(COUNTRY_CODE_OUTWARD_LOCAL1)
        parcel.writeString(COUNTRY_CODE_OUTWARD_LOCAL2)
        parcel.writeString(COUNTRY_CODE_OUTWARD_LOCAL3)
        parcel.writeString(COUNTRY_DESC_INWARD_FOREIGN1)
        parcel.writeString(COUNTRY_DESC_INWARD_FOREIGN2)
        parcel.writeString(COUNTRY_DESC_INWARD_FOREIGN3)
        parcel.writeString(COUNTRY_DESC_INWARD_LOCAL1)
        parcel.writeString(COUNTRY_DESC_INWARD_LOCAL2)
        parcel.writeString(COUNTRY_DESC_INWARD_LOCAL3)
        parcel.writeString(COUNTRY_DESC_OUTWARD_FOREIGN1)
        parcel.writeString(COUNTRY_DESC_OUTWARD_FOREIGN2)
        parcel.writeString(COUNTRY_DESC_OUTWARD_FOREIGN3)
        parcel.writeString(COUNTRY_DESC_OUTWARD_LOCAL1)
        parcel.writeString(COUNTRY_DESC_OUTWARD_LOCAL2)
        parcel.writeString(COUNTRY_DESC_OUTWARD_LOCAL3)
        parcel.writeString(DESC_NATIONALITY)
        parcel.writeString(DESC_REASON_EDD)
        parcel.writeString(DESC_RELATIONSHIP_WITH_FIN)
        parcel.writeString(EDD_CONDUCT_BY_DATE)
        parcel.writeString(EDD_CONDUCT_BY_NAME)
        parcel.writeString(FIN_SUPP_GUR_FMLY_PEP)
        parcel.writeString(FOREIGN_CONTACT_NO)
        parcel.writeString(FOREIGN_CONTACT_NO_HIGH_RISK_NRP)
        parcel.writeString(FOREIGN_OFFICE_ADDRESS_1)
        parcel.writeString(FOREIGN_OFFICE_ADDRESS_2)
        parcel.writeString(FOREIGN_RESIDENT_ADDRESS_1)
        parcel.writeString(FOREIGN_RESIDENT_ADDRESS_2)
        parcel.writeString(ID_NO)
        parcel.writeString(INHERITANCE)
        parcel.writeString(INHERITANCE_EXP_AMT)
        parcel.writeString(IS_HIGH_RISK_SEGMENT)
        parcel.writeString(IS_NON_RESIDENT_INFORMATION)
        parcel.writeString(IS_THE_FINANCIAL_SUPPORTER)
        parcel.writeString(NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1)
        parcel.writeString(NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2)
        parcel.writeString(NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3)
        parcel.writeString(NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1)
        parcel.writeString(NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2)
        parcel.writeString(NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3)
        parcel.writeString(NAME_OF_INSTITUTION_1)
        parcel.writeString(NAME_OF_INSTITUTION_2)
        parcel.writeString(NAME_OF_INSTITUTION_3)
        parcel.writeString(NATIONALITY)
        parcel.writeString(OFFICE_ADDRESS_FOREIGN_COUNTRY)
        parcel.writeString(OTHER)
        parcel.writeString(OTHER_EXP_AMT)
        parcel.writeString(OTHER_SOURCE)
        parcel.writeString(OTHER_SOURCE_2)
        parcel.writeString(OTHER_SOURCE_NRP)
        parcel.writeString(PENSION)
        parcel.writeString(PENSION_EXP_AMT)
        parcel.writeString(PURPOSE_OF_FOREIGN_TRANSACTION)
        parcel.writeString(PURPOSE_OF_LOCAL_TRANSACTION)
        parcel.writeString(REASON_EDD)
        parcel.writeString(RELATIONSHIP_FOREIGN_BENEFICIARY_1)
        parcel.writeString(RELATIONSHIP_FOREIGN_BENEFICIARY_2)
        parcel.writeString(RELATIONSHIP_FOREIGN_BENEFICIARY_3)
        parcel.writeString(RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1)
        parcel.writeString(RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2)
        parcel.writeString(RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3)
        parcel.writeString(RELATIONSHIP_LOCAL_BENEFICIARY_1)
        parcel.writeString(RELATIONSHIP_LOCAL_BENEFICIARY_2)
        parcel.writeString(RELATIONSHIP_LOCAL_BENEFICIARY_3)
        parcel.writeString(RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1)
        parcel.writeString(RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2)
        parcel.writeString(RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3)
        parcel.writeString(RELATIONSHIP_WITH_FIN)
        parcel.writeString(RENTAL_INCOME)
        parcel.writeString(RENTAL_INCOME_EXP_AMT)
        parcel.writeString(RENTAL_INCOME_FIN_SUPPORTER)
        parcel.writeString(RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT)
        parcel.writeString(RENTAL_INCOME_RES_PROJ)
        parcel.writeString(RENTAL_INCOME_RES_PROJ_EXP_AMT)
        parcel.writeString(RESIDENTIAL_ADDRESS)
        parcel.writeString(RETIREMENT_FUND)
        parcel.writeString(RETIREMENT_FUND_EXP_AMT)
        parcel.writeString(SAL_OF_FIN_SUPPORTER)
        parcel.writeString(SAL_OF_FIN_SUPPORTER_EXP_AMT)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CUSTEDDX> {
        override fun createFromParcel(parcel: Parcel): CUSTEDDX {
            return CUSTEDDX(parcel)
        }

        override fun newArray(size: Int): Array<CUSTEDDX?> {
            return arrayOfNulls(size)
        }
    }
}