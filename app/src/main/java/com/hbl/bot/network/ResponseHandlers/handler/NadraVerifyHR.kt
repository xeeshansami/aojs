package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class NadraVerifyHR(callBack: NadraVerifyCallBack) : BaseRH<NadraVerifyResponse>() {
    var callback: NadraVerifyCallBack = callBack
    override fun onSuccess(response: NadraVerifyResponse?) {
        response?.let { callback.NadraVerifySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.NadraVerifyFailure(it) }
    }
}