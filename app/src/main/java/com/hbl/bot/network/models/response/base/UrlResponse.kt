package com.hbl.bot.network.models.response.base


import com.google.gson.annotations.SerializedName

class UrlResponse {
    @SerializedName("data")
    var `data`: List<Data> = listOf()
    @SerializedName("message")
    var message: String = ""
    @SerializedName("status")
    var status: String = ""
}