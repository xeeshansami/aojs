package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable

class PrintDocumentRequest(
) : Parcelable {
     var FATCA: String=""
     var PEP: String=""
     var RISK_RATING: String=""
     var WORK_FLOW_CODE: String=""
     var identifier: String=""
     var tracking_id: String=""
     var PDF_COUNT: String=""

    constructor(parcel: Parcel) : this() {
        FATCA = parcel.readString().toString()
        PEP = parcel.readString().toString()
        RISK_RATING = parcel.readString().toString()
        WORK_FLOW_CODE = parcel.readString().toString()
        identifier = parcel.readString().toString()
        tracking_id = parcel.readString().toString()
        PDF_COUNT = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PDF_COUNT)
        parcel.writeString(FATCA)
        parcel.writeString(PEP)
        parcel.writeString(RISK_RATING)
        parcel.writeString(WORK_FLOW_CODE)
        parcel.writeString(identifier)
        parcel.writeString(tracking_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PrintDocumentRequest> {
        override fun createFromParcel(parcel: Parcel): PrintDocumentRequest {
            return PrintDocumentRequest(parcel)
        }

        override fun newArray(size: Int): Array<PrintDocumentRequest?> {
            return arrayOfNulls(size)
        }
    }
}