package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface APKCheckCallBack {
    fun APKCheckSuccess(response: APKCheckResponse)
    fun APKCheckFailure(response: BaseResponse)
}