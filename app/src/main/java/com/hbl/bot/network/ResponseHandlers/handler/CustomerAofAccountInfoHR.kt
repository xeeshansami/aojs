package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class CustomerAofAccountInfoHR(callBack: CustomerAofAccountInfoCallBack) :
    BaseRH<AllSubmitResponse>() {
    var callback: CustomerAofAccountInfoCallBack = callBack
    override fun onSuccess(response: AllSubmitResponse?) {
        response?.let { callback.CustomerAofAccountInfoSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CustomerAofAccountInfoFailure(it) }
    }
}