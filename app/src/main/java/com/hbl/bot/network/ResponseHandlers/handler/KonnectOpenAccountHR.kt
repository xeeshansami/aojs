package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class KonnectOpenAccountHR(callBack: KonnectOpenAccountCallBack) :
    BaseRH<KonnectOpenAccountResponse>() {
    var callback: KonnectOpenAccountCallBack = callBack
    override fun onSuccess(response: KonnectOpenAccountResponse?) {
        response?.let { callback.konnectOpenAccountSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.konnectOpenAccountFailure(it) }
    }
}