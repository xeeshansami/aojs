package com.hbl.bot.network.models.response.baseRM

data class GenerateCIFAccountData(
    val ACCOUNTNO: String,
    val RESPCODE: String,
    val RESPDESC: String
)