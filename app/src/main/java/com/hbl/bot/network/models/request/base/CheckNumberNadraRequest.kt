package com.hbl.bot.network.models.request.base


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class CheckNumberNadraRequest() : Parcelable {
    @SerializedName("CHANNEL_ID")
    var cHANNELID: String = ""
    @SerializedName("CONTACT_NUMBER")
    var cONTACTNUMBER: String = ""
    @SerializedName("ID_DOCUMENT_NO")
    var iDDOCUMENTNO: String = ""
    @SerializedName("TRACKING_ID")
    var tRACKINGID: String = ""
    @SerializedName("USER_ID")
    var uSERID: String = ""

    constructor(parcel: Parcel) : this() {
        cHANNELID = parcel.readString().toString()
        cONTACTNUMBER = parcel.readString().toString()
        iDDOCUMENTNO = parcel.readString().toString()
        tRACKINGID = parcel.readString().toString()
        uSERID = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(cHANNELID)
        parcel.writeString(cONTACTNUMBER)
        parcel.writeString(iDDOCUMENTNO)
        parcel.writeString(tRACKINGID)
        parcel.writeString(uSERID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CheckNumberNadraRequest> {
        override fun createFromParcel(parcel: Parcel): CheckNumberNadraRequest {
            return CheckNumberNadraRequest(parcel)
        }

        override fun newArray(size: Int): Array<CheckNumberNadraRequest?> {
            return arrayOfNulls(size)
        }
    }
}