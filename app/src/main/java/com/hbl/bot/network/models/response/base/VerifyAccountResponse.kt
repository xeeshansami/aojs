package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.*
import java.io.Serializable


class VerifyAccountResponse() : Serializable, Parcelable {
    @SerializedName("status")
    @Expose
    public val status: String? = null

    @SerializedName("message")
    @Expose
    public val message: String? = null

    @SerializedName("data")
    @Expose
    val data: ArrayList<AccountVerifcation>? = null

    constructor(source: Parcel) : this(
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<VerifyAccountResponse> =
            object : Parcelable.Creator<VerifyAccountResponse> {
                override fun createFromParcel(source: Parcel): VerifyAccountResponse =
                    VerifyAccountResponse(source)

                override fun newArray(size: Int): Array<VerifyAccountResponse?> = arrayOfNulls(size)
            }
    }
}