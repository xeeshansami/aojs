
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AOFAPPROVAL {

    @SerializedName("MAKER")
    @Expose
    private String MAKER;
    @SerializedName("CHECKER")
    @Expose
    private String CHECKER;
    @SerializedName("COPC1")
    @Expose
    private String COPC1;
    @SerializedName("COPC2")
    @Expose
    private String COPC2;

    public String getMAKER() {
        return MAKER;
    }

    public void setMAKER(String mAKER) {
        this.MAKER = mAKER;
    }

    public String getCHECKER() {
        return CHECKER;
    }

    public void setCHECKER(String cHECKER) {
        this.CHECKER = cHECKER;
    }

    public String getCOPC1() {
        return COPC1;
    }

    public void setCOPC1(String cOPC1) {
        this.COPC1 = cOPC1;
    }

    public String getCOPC2() {
        return COPC2;
    }

    public void setCOPC2(String cOPC2) {
        this.COPC2 = cOPC2;
    }

}
