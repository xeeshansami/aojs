package com.hbl.bot.network.apiInterface


import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.request.baseRM.DataRequest
import com.hbl.bot.network.models.request.baseRM.ETBNTBRequest
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.NadraVerify
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


interface APIInterface {
    @POST("m/login_new")
    fun getLogin(@Body loginRequest: LoginRequest?): Call<LoginResponse>

    @POST("m/lov")
    fun getPriority(@Body lovRequest: LovRequest?): Call<PriorityResponse>

    @POST("m/lov")
    fun getChequeBookLeaves(@Body lovRequest: LovRequest?): Call<ChequeBookLeavesResponse>

    @POST("m/lov")
    fun getDocs(@Body lovRequest: LovRequest?): Call<DocsResponse>

    @POST("m/lov")
    fun getHawCompanies(@Body lovRequest: LovRequest?): Call<HawCompanyResponse>

    @POST("m/lov")
    fun getMOD(@Body lovRequest: LovRequest?): Call<MODResponse>

    @POST("m/lov")
    fun getCustomerSegments(@Body lovRequest: LovRequest?): Call<CustomerSegmentResponse>

    @POST("m/lov")
    fun getCountries(@Body lovRequest: LovRequest?): Call<CountryResponse>

    @POST("m/lov")
    fun getBools(@Body lovRequest: LovRequest?): Call<BoolResponse>

    @POST("m/lov")
    fun getCities(@Body lovRequest: LovRequest?): Call<CityResponse>

    @POST("m/lov")
    fun getSourceOfFundLandLoard(@Body lovRequest: LovRequest?): Call<SourceOffundIncomeResponse>

    @POST("m/lov")
    fun getTitle(@Body lovRequest: LovRequest?): Call<TitleResponse>

    @POST("m/lov")
    fun getMarital(@Body lovRequest: LovRequest?): Call<MaritalResponse>

    @POST("m/lov")
    fun getGender(@Body lovRequest: LovRequest?): Call<GenderResponse>

    @POST("m/lov")
    fun getHawKeys(@Body lovRequest: LovRequest?): Call<HawKeysResponse>

    @POST("m/formsignature")
    fun postSignature(@Body lovRequest: SignatureRequest?): Call<SignatureResponse>

    @POST("m/lov")
    fun getSBPCodes(@Body lovRequest: LovRequest?): Call<SBPCodesResponse>

    @POST("m/trackingid")
    fun getTrackingID(@Body trackingIDRequest: TrackingIDRequest?): Call<TrackingIDResponse>

    @POST("m/lov")
    fun getBranchType(@Body branchRequest: BranchCodeRequest?): Call<BranchTypeResponse>

    @POST("m/lov")
    fun getBranchCode(@Body branchRequest: BranchCodeRequest?): Call<BranchCodeResponse>

    @POST("m/lov")
    fun getCardProducts(@Body lovRequest: DebitCardLovRequest?): Call<CardProductResponse>

    @POST("m/lov")
    fun getRMCodes(@Body lovRequest: LovRequest?): Call<RMCodesResponse>

    @POST("m/lov")
    fun getSundry(@Body lovRequest: LovRequest?): Call<SundryResponse>

    @POST("m/lov")
    fun getConsumerProduct(@Body lovRequest: LovRequest?): Call<ConsumerProductResponse>

    @POST("m/lov")
    fun getRelationship(@Body lovRequest: LovRequest?): Call<RelationshipResponse>

    @POST("m/lov")
    fun getProvince(@Body lovRequest: LovRequest?): Call<ProvinceResponse>

    @POST("m/lov")
    fun getBioAccountType(@Body lovRequest: LovRequest?): Call<BioAccountTypeResponse>

    @POST("m/lov")
    fun getPrefComMode(@Body lovRequest: LovRequest?): Call<PrefComModeResponse>

    @POST("m/lov")
    fun getEstateFrequency(@Body lovRequest: LovRequest?): Call<EstateFrequencyResponse>

    @POST("m/lov")
    fun getPurposeOfAccount(@Body lovRequest: LovRequest?): Call<PurposeOfAccountResponse>

    @POST("m/lov")
    fun getDepositType(@Body lovRequest: LovRequest?): Call<DepositTypeResponse>

    @POST("m/lov")
    fun getCurrency(@Body lovRequest: CurrenyLovRequest?): Call<CurrencyResponse>

    @POST("m/lov")
    fun getAccountType(@Body lovRequest: LovRequest?): Call<AccountTypeResponse>

    @POST("m/lov")
    fun getIndustryMainCategory(@Body lovRequest: LovRequest?): Call<IndustryMainCategoryResponse>

    @POST("m/lov")
    fun getIndustrySubCategory(@Body lovRequest: LovRequest?): Call<IndustrySubCategoryResponse>

    @POST("m/lov")
    fun getModeOfTrans(@Body lovRequest: LovRequest?): Call<ModeOfTransResponse>

    @POST("m/lov")
    fun getNatureOfBusiness(@Body lovRequest: LovRequest?): Call<NatureOfBusinessResponse>

    @POST("m/lov")
    fun getProfessions(@Body lovRequest: LovRequest?): Call<ProfessionResponse>

    @POST("m/lov")
    fun getBranchRegion(@Body lovRequest: LovRequest?): Call<BranchRegionResponse>

    @POST("m/documentPrint")
    fun getUrl(@Body request: ConfigRequest?): Call<UrlResponse>

    @POST("m/lov")
    fun getPurpOfCif(@Body lovRequest: LovRequest?): Call<PurposeCIFResponse>

    @POST("m/lov")
    fun getPreferenceMailing(@Body lovRequest: LovRequest?): Call<PreferencesMailingResponse>

    @POST("m/lov")
    fun getSourceOffund(@Body lovRequest: LovRequest?): Call<SourceOffundResponse>

    @POST("m/lov")
    fun getCapacity(@Body lovRequest: LovRequest?): Call<CapacityResponse>

    @POST("m/lov")
    fun getAccountOperationsType(@Body lovRequest: LovRequest?): Call<AccountOperationsTypeResponse>

    @POST("m/lov")
    fun getAccountOperationsInst(@Body lovRequest: LovRequest?): Call<AccountOperationsInstResponse>

    @POST("m/lov")
    fun getSourceOfIncome(@Body lovRequest: LovRequest?): Call<SourceOfIncomeResponse>

    @POST("m/lov")
    fun getReasonePrefEDD(@Body lovRequest: LovRequest?): Call<ReasonPrefEDDResponse>

    @POST("m/lov")
    fun getPurposeOfTin(@Body lovRequest: LovRequest?): Call<PurposeOfTinResponse>

    @POST("m/lov")
    fun getAddressType(@Body lovRequest: LovRequest?): Call<AddressTypeResponse>

    @POST("m/lov")
    fun getSourceOfWealth(@Body lovRequest: LovRequest?): Call<SourceOfWealthResponse>

    @POST("m/lov")
    fun getPep(@Body lovRequest: LovRequest?): Call<PepResponse>

    @POST("m/lov")
    fun getPepCategories(@Body lovRequest: LovRequest?): Call<PepCategoriesResponse>

    @POST("m/lov")
    fun getPepSubCategories(@Body request: PepSubCategoriesRequest?): Call<PepSubCategoriesResponse>

    @POST("m/lov")
    fun getCountryCityCode(@Body request: GetCountryCityCodeRequest?): Call<CountryCityCodeResponse>

    @POST("m/nadra_account_verify")
    fun postNadraVerfication(@Body nardaVerifyRequest: NadraVerify?): Call<NadraVerifyResponse>

    @POST("m/nadra_get_citizen")
    fun postManualVerysis(@Body request: ManualVersysisRequest?): Call<NadraVerifyResponse>

    @POST("m/getbioverisys")
    fun getVerysis(@Body nardaVerifyRequest: GetVerysisRequest?): Call<NadraVerifyResponse>

    @POST("m/submit")
    fun postAofaccountInfo(@Body customerAofAccountInfoRequest: AofAccountInfoRequest?): Call<AllSubmitResponse>

    @POST("m/riskrating")
    fun postRiskRating(@Body customerAofAccountInfoRequest: AofAccountInfoRequest?): Call<RiskRatingResponse>

    @POST("m/documentScan")
    fun getDocumentType(@Body lovRequest: DocLovRequest?): Call<DocTypeResponse>

    @POST("m/documentScan")
    fun PartialUpload(@Body lovRequest: PartialUploadRequest?): Call<PartialUploadResponse>

    @POST("m/documentScan")
    fun getConfig(@Body lovRequest: ConfigRequest?): Call<ConfigResponse>

    @POST("m/documentScan")
    fun SubmitToSuper(@Body lovRequest: SubmitToSuperRequest?): Call<SubmitToSuperResponse>

    @POST("m/etbntb")
    fun submitETBNTB(@Body etbntbRequest: ETBNTBRequest?): Call<ETBNTBResponse>

    @POST("m/onboarding")
    fun onboarding(@Body onBoardingRequest: OnBoardingRequest?): Call<OnBoardingResponse>

    @POST("m/mysisref")
    fun getMySisRef(@Body lovRequest: LovRequest?): Call<MYSISREFResponse>

    @POST("m/lov")
    fun getBusinessArea(@Body lovRequest: BusinessAreaRequest?): Call<BusinessAreaResponse>



    @POST("m/lov")
    fun getAppStatus(@Body lovRequest: LovRequest?): Call<AppStatusResponse>

    @POST("m/submit")
    fun postSubmitGenerateCIF(@Body generateRequest: GenerateCIFRequest?): Call<GenerateCIFResponse>

    @POST("m/submit")
    fun postSubmitGenerateCIFAccount(@Body generateRequest: GenerateCIFRequest?): Call<GenerateCIFAccountResponse>

    @POST("m/documentPrint")
    fun gettemplate(@Body htmlRequest: HTMLRequest?): Call<HTMLResponse>


//    @POST("m/generatecifaccount")
//    fun genCIFAndAccount(@Body dataRequest: DataRequest?): Call<CIFResponse>


    @POST("m/notification1")
    fun genCIFAndAccount(@Body dataRequest: DataRequest?): Call<CIFResponse>

    @POST("m/generatecif")
    fun genCIF(@Body dataRequest: DataRequest?): Call<CIFResponse>

    @POST("m/lov")
    fun getWorkFlow(@Body lovRequest: LovRequest?): Call<WorkFlowResponse>


    @POST("m/onboarding")
    fun callUpdate(@Body request: UpdateCallRequest): Call<UpdateCallResponse>


    @POST("m/onboarding")
    fun meetUpdate(@Body request: UpdateMeetRequest): Call<UpdateMeetResponse>

    @POST("m/pepidentify")
    fun pepCheck(@Body request: PepCheckRequest): Call<PepCheckResponse>


    @POST("m/dashboard")
    fun showRecords(@Body request: showRecordsRequest): Call<showRecordsResponse>

    @POST("m/dashboard")
    fun postDashboardItems(@Body lovRequest: DashboardItemsRequest?): Call<DashboardResponse>

    @POST("m/dashboard")
    fun showFilterRecords(@Body request: showFilterRecordsRequest): Call<showRecordsResponse>


    @POST("m/dashboard")
    fun getCount(@Body request: CountRequest): Call<CountResponse>

/*
    @POST("m/documentPrint")
    fun printDocs(@Body request: PrintDocsRequest): Call<PrintDocsResponse>
    */
    /* @Multipart
     @POST("m/documentPrint")
     fun printDocs(@Part  request: RequestBody): Call<PrintDocsResponse>*/

    @Multipart
    @POST("m/documentPrint")
    fun printDocs(
        @Part("printdoc\"; filename=\"hbl.pdf\" ") file: RequestBody?,
        @Part("trackingid") trackingid: RequestBody?,
        @Part("cnic") cnic: RequestBody?,
        @Part("mysisno") mysisno: RequestBody?,
        @Part("cifno") cifno: RequestBody?,
        @Part("identifier") identifier: RequestBody?
    ): Call<PrintDocsResponse>


    @POST("m/maintenance")
    fun callDraftData(@Body request: DraftRequest): Call<AofAccountInfoResponse>



    @POST("m/maintenance")
    fun callDraftMaintanance(@Body request: DraftMaintainanceRequest): Call<AofAccountInfoResponse>

    @POST("ciffetch")
    fun getCIF(@Body request: CIFRequest): Call<AofAccountInfoResponse>

    @POST("ciffetch")
    fun getKINCIF(@Body request: CIFRequest): Call<KINAccountInfoResponse>

    @POST("m/documentPrint")
    fun getPDF(@Body request: PrintPDFRequest): Call<PrintPDFResponse>

    @POST("m/documentPrint")
    fun getPrintDocs(@Body request: PrintDocumentRequest): Call<PrintDocumentResponse>

    @POST("m/get_pmd")
    fun checkNumberNadra(@Body request: CheckNumberNadraRequest): Call<CheckNumberNadraResponse>

    @POST("m/accountvalidation")
    fun VerifyAccount(@Body request: VerifyAccountRequest): Call<VerifyAccountResponse>

    @POST("m/device-registration")
    fun VerifyEmail(@Body request: VerifyEmailRequest): Call<VerifyEmailResponse>

    @POST("m/device-registration")
    fun VerifyOTP(@Body request: VerifyOTPRequest): Call<VerifyOTPResponse>

    @POST("m/app-version")
    fun sendAppDetails(@Body request: AppDetailsRequest): Call<AppStatusResponse>

    @POST("m/files")
    fun APKCheck(@Body request: APKCheckRequest): Call<APKCheckResponse>

    @POST("m/manual-verisys")
    fun manualVerysis(@Body request: ManualVerysisRequest): Call<APKCheckResponse>

    /*######################################
    ############   KONNECT  ################
    ######################################*/

    @GET("tracking/{branchCode}")
    fun getKonnectTrackingID(@Path("branchCode") branchCode: String?): Call<KonnectTrackingIDResponse>

    @GET("branch/{branchCode}/city")
    fun getKonnectBranchCity(@Path("branchCode") branchCode: String?): Call<KonnectBranchCityResponse>

    @GET("lov/purpofacct")
    fun getKonnectPurposeOfAccount(/*@Body lovRequest: LovRequest?*/): Call<KonnectPurposeOfAccountResponse>

    @POST("account/inquiry")
    fun getKonnectInquiry(@Body konnectInquiryRequest: KonnectInquiryRequest?): Call<KonnectInquiryResponse>

    @POST("etbntb")
    fun getKonnectETBNTB(@Body etbntbRequest: ETBNTBRequest?): Call<ETBNTBResponse>

    @POST("ciffetch")
    fun getKonnectCIF(@Body request: CIFRequest): Call<AofAccountInfoResponse>

    @POST("openaccount")
    fun getKonnectOpenAccount(@Body request: KonnectOpenAccountRequest): Call<KonnectOpenAccountResponse>

    @POST("debit/inquiry")
    fun getKonnectDebitCardInquiry(@Body konnectDebitCardInquiryRequest: KonnectDebitCardInquiryRequest?): Call<KonnectDebitCardInquiryResponse>

    @POST("debit/create")
    fun getKonnectDebitCardTransaction(@Body konnectDebitCardTransactionRequest: KonnectDebitCardTransactionRequest?): Call<KonnectDebitCardTransactionResponse>



    companion object {
        const val HEADER_TAG = "@"
        const val HEADER_POSTFIX = ": "
        const val HEADER_TAG_PUBLIC = "public"
    }
}