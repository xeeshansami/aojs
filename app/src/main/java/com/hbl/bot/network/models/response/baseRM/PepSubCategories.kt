package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class PepSubCategories(
) : Parcelable {
    var PEP_CATG_CODE: String? = null
    var PEP_CATG_DESC: String? = null
    var PEP_SUB_CATG_CODE: String? = null
    var PEP_SUB_CATG_DESC: String? = null
    var _id: String? = null

    constructor(parcel: Parcel) : this() {
        PEP_CATG_CODE = parcel.readString()
        PEP_CATG_DESC = parcel.readString()
        PEP_SUB_CATG_CODE = parcel.readString()
        PEP_SUB_CATG_DESC = parcel.readString()
        _id = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PEP_CATG_CODE)
        parcel.writeString(PEP_CATG_DESC)
        parcel.writeString(PEP_SUB_CATG_CODE)
        parcel.writeString(PEP_SUB_CATG_DESC)
        parcel.writeString(_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PepSubCategories> {
        override fun createFromParcel(parcel: Parcel): PepSubCategories {
            return PepSubCategories(parcel)
        }

        override fun newArray(size: Int): Array<PepSubCategories?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return PEP_SUB_CATG_DESC.toString()
    }
}