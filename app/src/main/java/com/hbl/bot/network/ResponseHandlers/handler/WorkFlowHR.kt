package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class WorkFlowHR(callBack: WorkFlowCallBack) : BaseRH<WorkFlowResponse>() {
    var callback: WorkFlowCallBack = callBack
    override fun onSuccess(response: WorkFlowResponse?) {
        response?.let { callback.WorkFlowSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.WorkFlowFailure(it) }
    }
}