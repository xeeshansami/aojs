
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KINCUSTEDD {

    @SerializedName("REASON_EDD")
    @Expose
    public String REASON_EDD = "";
    @SerializedName("DESC_REASON_EDD")
    @Expose
    public String DESC_REASON_EDD = "";
    @SerializedName("FOREIGN_CONTACT_NO")
    @Expose
    public String FOREIGN_CONTACT_NO = "";
    @SerializedName("FOREIGN_RESIDENT_ADDRESS_1")
    @Expose
    public String FOREIGN_RESIDENT_ADDRESS_1 = "";
    @SerializedName("FOREIGN_RESIDENT_ADDRESS_2")
    @Expose
    public String FOREIGN_RESIDENT_ADDRESS_2 = "";
    @SerializedName("FOREIGN_OFFICE_ADDRESS_1")
    @Expose
    public String FOREIGN_OFFICE_ADDRESS_1 = "";
    @SerializedName("FOREIGN_OFFICE_ADDRESS_2")
    @Expose
    public String FOREIGN_OFFICE_ADDRESS_2 = "";
    @SerializedName("AGRI_INCOME")
    @Expose
    public String AGRI_INCOME = "";
    @SerializedName("AGRI_INCOME_EXP_AMT")
    @Expose
    public String AGRI_INCOME_EXP_AMT = "";
    @SerializedName("AGRI_RENTAL_INCOME")
    @Expose
    public String AGRI_RENTAL_INCOME = "";
    @SerializedName("AGRI_RENTAL_INCOME_EXP_AMT")
    @Expose
    public String AGRI_RENTAL_INCOME_EXP_AMT = "";
    @SerializedName("BUSINESS_INCOME")
    @Expose
    public String BUSINESS_INCOME = "";
    @SerializedName("BUSINESS_INCOME_EXP_AMT")
    @Expose
    public String BUSINESS_INCOME_EXP_AMT = "";
    @SerializedName("RENTAL_INCOME")
    @Expose
    public String RENTAL_INCOME = "";
    @SerializedName("RENTAL_INCOME_EXP_AMT")
    @Expose
    public String RENTAL_INCOME_EXP_AMT = "";
    @SerializedName("RENTAL_INCOME_RES_PROJ")
    @Expose
    public String RENTAL_INCOME_RES_PROJ = "";
    @SerializedName("RENTAL_INCOME_RES_PROJ_EXP_AMT")
    @Expose
    public String RENTAL_INCOME_RES_PROJ_EXP_AMT = "";
    @SerializedName("OTHER_SOURCE")
    @Expose
    public String OTHER_SOURCE = "";
    @SerializedName("OTHER_SOURCE_2")
    @Expose
    public String OTHER_SOURCE_2 = "";
    @SerializedName("CONDUCT_FOREIGN_BANK_TRANSFER")
    @Expose
    public String CONDUCT_FOREIGN_BANK_TRANSFER = "";
    @SerializedName("COUNTRY_CODE_INWARD_FOREIGN1")
    @Expose
    public String COUNTRY_CODE_INWARD_FOREIGN1 = "";
    @SerializedName("COUNTRY_DESC_INWARD_FOREIGN1")
    @Expose
    public String COUNTRY_DESC_INWARD_FOREIGN1 = "";
    @SerializedName("COUNTRY_CODE_OUTWARD_FOREIGN1")
    @Expose
    public String COUNTRY_CODE_OUTWARD_FOREIGN1 = "";
    @SerializedName("COUNTRY_DESC_OUTWARD_FOREIGN1")
    @Expose
    public String COUNTRY_DESC_OUTWARD_FOREIGN1 = "";
    @SerializedName("NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1")
    @Expose
    public String NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1 = "";
    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_1")
    @Expose
    public String RELATIONSHIP_FOREIGN_BENEFICIARY_1 = "";
    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1")
    @Expose
    public String RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1 = "";
    @SerializedName("COUNTRY_CODE_INWARD_FOREIGN2")
    @Expose
    public String COUNTRY_CODE_INWARD_FOREIGN2 = "";
    @SerializedName("COUNTRY_DESC_INWARD_FOREIGN2")
    @Expose
    public String COUNTRY_DESC_INWARD_FOREIGN2 = "";
    @SerializedName("COUNTRY_CODE_OUTWARD_FOREIGN2")
    @Expose
    public String COUNTRY_CODE_OUTWARD_FOREIGN2 = "";
    @SerializedName("COUNTRY_DESC_OUTWARD_FOREIGN2")
    @Expose
    public String COUNTRY_DESC_OUTWARD_FOREIGN2 = "";
    @SerializedName("NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2")
    @Expose
    public String NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2 = "";
    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_2")
    @Expose
    public String RELATIONSHIP_FOREIGN_BENEFICIARY_2 = "";
    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2")
    @Expose
    public String RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2 = "";
    @SerializedName("COUNTRY_CODE_INWARD_FOREIGN3")
    @Expose
    public String COUNTRY_CODE_INWARD_FOREIGN3 = "";
    @SerializedName("COUNTRY_DESC_INWARD_FOREIGN3")
    @Expose
    public String COUNTRY_DESC_INWARD_FOREIGN3 = "";
    @SerializedName("COUNTRY_CODE_OUTWARD_FOREIGN3")
    @Expose
    public String COUNTRY_CODE_OUTWARD_FOREIGN3 = "";
    @SerializedName("COUNTRY_DESC_OUTWARD_FOREIGN3")
    @Expose
    public String COUNTRY_DESC_OUTWARD_FOREIGN3 = "";
    @SerializedName("NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3")
    @Expose
    public String NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3 = "";
    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_3")
    @Expose
    public String RELATIONSHIP_FOREIGN_BENEFICIARY_3 = "";
    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3")
    @Expose
    public String RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3 = "";
    @SerializedName("ID_NO")
    @Expose
    public String ID_NO = "";
    @SerializedName("NATIONALITY")
    @Expose
    public String NATIONALITY = "";
    @SerializedName("DESC_NATIONALITY")
    @Expose
    public String DESC_NATIONALITY = "";
    @SerializedName("BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY")
    @Expose
    public String BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY = "";
    @SerializedName("ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK")
    @Expose
    public String ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK = "";
    @SerializedName("NAME_OF_INSTITUTION_1")
    @Expose
    public String NAME_OF_INSTITUTION_1 = "";
    @SerializedName("NAME_OF_INSTITUTION_2")
    @Expose
    public String NAME_OF_INSTITUTION_2 = "";
    @SerializedName("NAME_OF_INSTITUTION_3")
    @Expose
    public String NAME_OF_INSTITUTION_3 = "";
    @SerializedName("AVG_MTHLY_BAL_IN_THOSE_ACCTS")
    @Expose
    public String AVG_MTHLY_BAL_IN_THOSE_ACCTS = "";
    @SerializedName("FOREIGN_CONTACT_NO_HIGH_RISK_NRP")
    @Expose
    public String FOREIGN_CONTACT_NO_HIGH_RISK_NRP = "";
    @SerializedName("RESIDENTIAL_ADDRESS")
    @Expose
    public String RESIDENTIAL_ADDRESS = "";
    @SerializedName("OFFICE_ADDRESS_FOREIGN_COUNTRY")
    @Expose
    public String OFFICE_ADDRESS_FOREIGN_COUNTRY = "";
    @SerializedName("SAL_OF_FIN_SUPPORTER")
    @Expose
    public String SAL_OF_FIN_SUPPORTER = "";
    @SerializedName("SAL_OF_FIN_SUPPORTER_EXP_AMT")
    @Expose
    public String SAL_OF_FIN_SUPPORTER_EXP_AMT = "";
    @SerializedName("RENTAL_INCOME_FIN_SUPPORTER")
    @Expose
    public String RENTAL_INCOME_FIN_SUPPORTER = "";
    @SerializedName("RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT")
    @Expose
    public String RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT = "";
    @SerializedName("BUSS_INCOME_FIN_SUPPORTER")
    @Expose
    public String BUSS_INCOME_FIN_SUPPORTER = "";
    @SerializedName("BUSS_INCOME_FIN_SUPPORTER_EXP_AMT")
    @Expose
    public String BUSS_INCOME_FIN_SUPPORTER_EXP_AMT = "";
    @SerializedName("RETIREMENT_FUND")
    @Expose
    public String RETIREMENT_FUND = "";
    @SerializedName("RETIREMENT_FUND_EXP_AMT")
    @Expose
    public String RETIREMENT_FUND_EXP_AMT = "";
    @SerializedName("PENSION")
    @Expose
    public String PENSION = "";
    @SerializedName("PENSION_EXP_AMT")
    @Expose
    public String PENSION_EXP_AMT = "";
    @SerializedName("INHERITANCE")
    @Expose
    public String INHERITANCE = "";
    @SerializedName("INHERITANCE_EXP_AMT")
    @Expose
    public String INHERITANCE_EXP_AMT = "";
    @SerializedName("OTHER")
    @Expose
    public String OTHER = "";
    @SerializedName("OTHER_EXP_AMT")
    @Expose
    public String OTHER_EXP_AMT = "";
    @SerializedName("OTHER_SOURCE_NRP")
    @Expose
    public String OTHER_SOURCE_NRP = "";
    @SerializedName("RELATIONSHIP_WITH_FIN")
    @Expose
    public String RELATIONSHIP_WITH_FIN = "";
    @SerializedName("DESC_RELATIONSHIP_WITH_FIN")
    @Expose
    public String DESC_RELATIONSHIP_WITH_FIN = "";
    @SerializedName("FIN_SUPP_GUR_FMLY_PEP")
    @Expose
    public String FIN_SUPP_GUR_FMLY_PEP = "";
    @SerializedName("EDD_CONDUCT_BY_NAME")
    @Expose
    public String EDD_CONDUCT_BY_NAME = "";
    @SerializedName("EDD_CONDUCT_BY_DATE")
    @Expose
    public String EDD_CONDUCT_BY_DATE = "";
    @SerializedName("APPROVED_BY_NAME")
    @Expose
    public String APPROVED_BY_NAME = "";
    @SerializedName("APPROVED_BY_DATE")
    @Expose
    public String APPROVED_BY_DATE = "";
    @SerializedName("APPROVED_COMPL_AUTH_NAME")
    @Expose
    public String APPROVED_COMPL_AUTH_NAME = "";
    @SerializedName("APPROVED_COMPL_AUTH_DATE")
    @Expose
    public String APPROVED_COMPL_AUTH_DATE = "";
    @SerializedName("CONDUCT_LOCAL_BANK_TRANSFER")
    @Expose
    public String CONDUCT_LOCAL_BANK_TRANSFER = "";
    @SerializedName("COUNTRY_CODE_INWARD_LOCAL1")
    @Expose
    public String COUNTRY_CODE_INWARD_LOCAL1 = "";
    @SerializedName("COUNTRY_DESC_INWARD_LOCAL1")
    @Expose
    public String COUNTRY_DESC_INWARD_LOCAL1 = "";
    @SerializedName("COUNTRY_CODE_OUTWARD_LOCAL1")
    @Expose
    public String COUNTRY_CODE_OUTWARD_LOCAL1 = "";
    @SerializedName("COUNTRY_DESC_OUTWARD_LOCAL1")
    @Expose
    public String COUNTRY_DESC_OUTWARD_LOCAL1 = "";
    @SerializedName("NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1")
    @Expose
    public String NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1 = "";
    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_1")
    @Expose
    public String RELATIONSHIP_LOCAL_BENEFICIARY_1 = "";
    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1")
    @Expose
    public String RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1 = "";
    @SerializedName("COUNTRY_CODE_INWARD_LOCAL2")
    @Expose
    public String COUNTRY_CODE_INWARD_LOCAL2 = "";
    @SerializedName("COUNTRY_DESC_INWARD_LOCAL2")
    @Expose
    public String COUNTRY_DESC_INWARD_LOCAL2 = "";
    @SerializedName("COUNTRY_CODE_OUTWARD_LOCAL2")
    @Expose
    public String COUNTRY_CODE_OUTWARD_LOCAL2 = "";
    @SerializedName("COUNTRY_DESC_OUTWARD_LOCAL2")
    @Expose
    public String COUNTRY_DESC_OUTWARD_LOCAL2 = "";
    @SerializedName("NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2")
    @Expose
    public String NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2 = "";
    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_2")
    @Expose
    public String RELATIONSHIP_LOCAL_BENEFICIARY_2 = "";
    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2")
    @Expose
    public String RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2 = "";
    @SerializedName("COUNTRY_CODE_INWARD_LOCAL3")
    @Expose
    public String COUNTRY_CODE_INWARD_LOCAL3 = "";
    @SerializedName("COUNTRY_DESC_INWARD_LOCAL3")
    @Expose
    public String COUNTRY_DESC_INWARD_LOCAL3 = "";
    @SerializedName("COUNTRY_CODE_OUTWARD_LOCAL3")
    @Expose
    public String COUNTRY_CODE_OUTWARD_LOCAL3 = "";
    @SerializedName("COUNTRY_DESC_OUTWARD_LOCAL3")
    @Expose
    public String COUNTRY_DESC_OUTWARD_LOCAL3 = "";
    @SerializedName("NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3")
    @Expose
    public String NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3 = "";
    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3")
    @Expose
    public String RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3 = "";
    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_3")
    @Expose
    public String RELATIONSHIP_LOCAL_BENEFICIARY_3 = "";
    @SerializedName("PURPOSE_OF_FOREIGN_TRANSACTION")
    @Expose
    public String PURPOSE_OF_FOREIGN_TRANSACTION = "";
    @SerializedName("PURPOSE_OF_LOCAL_TRANSACTION")
    @Expose
    public String PURPOSE_OF_LOCAL_TRANSACTION = "";
    @SerializedName("AMOUNT_OF_FOREIGN_TRANSACTION")
    @Expose
    public String AMOUNT_OF_FOREIGN_TRANSACTION = "";
    @SerializedName("AMOUNT_OF_LOCAL_TRANSACTION")
    @Expose
    public String AMOUNT_OF_LOCAL_TRANSACTION = "";
    @SerializedName("IS_NON_RESIDENT_INFORMATION")
    @Expose
    public String IS_NON_RESIDENT_INFORMATION = "N";
    @SerializedName("IS_HIGH_RISK_SEGMENT")
    @Expose
    public String IS_HIGH_RISK_SEGMENT = "N";
    @SerializedName("IS_THE_FINANCIAL_SUPPORTER")
    @Expose
    public String IS_THE_FINANCIAL_SUPPORTER = "N";

    public String getREASON_EDD() {
        return REASON_EDD;
    }

    public void setREASON_EDD(String REASON_EDD) {
        this.REASON_EDD = REASON_EDD;
    }

    public String getFOREIGN_CONTACT_NO() {
        return FOREIGN_CONTACT_NO;
    }

    public void setFOREIGN_CONTACT_NO(String FOREIGN_CONTACT_NO) {
        this.FOREIGN_CONTACT_NO = FOREIGN_CONTACT_NO;
    }

    public String getFOREIGN_RESIDENT_ADDRESS_1() {
        return FOREIGN_RESIDENT_ADDRESS_1;
    }

    public void setFOREIGN_RESIDENT_ADDRESS_1(String FOREIGN_RESIDENT_ADDRESS_1) {
        this.FOREIGN_RESIDENT_ADDRESS_1 = FOREIGN_RESIDENT_ADDRESS_1;
    }

    public String getFOREIGN_RESIDENT_ADDRESS_2() {
        return FOREIGN_RESIDENT_ADDRESS_2;
    }

    public void setFOREIGN_RESIDENT_ADDRESS_2(String FOREIGN_RESIDENT_ADDRESS_2) {
        this.FOREIGN_RESIDENT_ADDRESS_2 = FOREIGN_RESIDENT_ADDRESS_2;
    }

    public String getFOREIGN_OFFICE_ADDRESS_1() {
        return FOREIGN_OFFICE_ADDRESS_1;
    }

    public void setFOREIGN_OFFICE_ADDRESS_1(String FOREIGN_OFFICE_ADDRESS_1) {
        this.FOREIGN_OFFICE_ADDRESS_1 = FOREIGN_OFFICE_ADDRESS_1;
    }

    public String getFOREIGN_OFFICE_ADDRESS_2() {
        return FOREIGN_OFFICE_ADDRESS_2;
    }

    public void setFOREIGN_OFFICE_ADDRESS_2(String FOREIGN_OFFICE_ADDRESS_2) {
        this.FOREIGN_OFFICE_ADDRESS_2 = FOREIGN_OFFICE_ADDRESS_2;
    }

    public String getAGRI_INCOME() {
        return AGRI_INCOME;
    }

    public void setAGRI_INCOME(String AGRI_INCOME) {
        this.AGRI_INCOME = AGRI_INCOME;
    }

    public String getAGRI_INCOME_EXP_AMT() {
        return AGRI_INCOME_EXP_AMT;
    }

    public void setAGRI_INCOME_EXP_AMT(String AGRI_INCOME_EXP_AMT) {
        this.AGRI_INCOME_EXP_AMT = AGRI_INCOME_EXP_AMT;
    }

    public String getAGRI_RENTAL_INCOME() {
        return AGRI_RENTAL_INCOME;
    }

    public void setAGRI_RENTAL_INCOME(String AGRI_RENTAL_INCOME) {
        this.AGRI_RENTAL_INCOME = AGRI_RENTAL_INCOME;
    }

    public String getAGRI_RENTAL_INCOME_EXP_AMT() {
        return AGRI_RENTAL_INCOME_EXP_AMT;
    }

    public void setAGRI_RENTAL_INCOME_EXP_AMT(String AGRI_RENTAL_INCOME_EXP_AMT) {
        this.AGRI_RENTAL_INCOME_EXP_AMT = AGRI_RENTAL_INCOME_EXP_AMT;
    }

    public String getBUSINESS_INCOME() {
        return BUSINESS_INCOME;
    }

    public void setBUSINESS_INCOME(String BUSINESS_INCOME) {
        this.BUSINESS_INCOME = BUSINESS_INCOME;
    }

    public String getBUSINESS_INCOME_EXP_AMT() {
        return BUSINESS_INCOME_EXP_AMT;
    }

    public void setBUSINESS_INCOME_EXP_AMT(String BUSINESS_INCOME_EXP_AMT) {
        this.BUSINESS_INCOME_EXP_AMT = BUSINESS_INCOME_EXP_AMT;
    }

    public String getRENTAL_INCOME() {
        return RENTAL_INCOME;
    }

    public void setRENTAL_INCOME(String RENTAL_INCOME) {
        this.RENTAL_INCOME = RENTAL_INCOME;
    }

    public String getRENTAL_INCOME_EXP_AMT() {
        return RENTAL_INCOME_EXP_AMT;
    }

    public void setRENTAL_INCOME_EXP_AMT(String RENTAL_INCOME_EXP_AMT) {
        this.RENTAL_INCOME_EXP_AMT = RENTAL_INCOME_EXP_AMT;
    }

    public String getRENTAL_INCOME_RES_PROJ() {
        return RENTAL_INCOME_RES_PROJ;
    }

    public void setRENTAL_INCOME_RES_PROJ(String RENTAL_INCOME_RES_PROJ) {
        this.RENTAL_INCOME_RES_PROJ = RENTAL_INCOME_RES_PROJ;
    }

    public String getRENTAL_INCOME_RES_PROJ_EXP_AMT() {
        return RENTAL_INCOME_RES_PROJ_EXP_AMT;
    }

    public void setRENTAL_INCOME_RES_PROJ_EXP_AMT(String RENTAL_INCOME_RES_PROJ_EXP_AMT) {
        this.RENTAL_INCOME_RES_PROJ_EXP_AMT = RENTAL_INCOME_RES_PROJ_EXP_AMT;
    }

    public String getOTHER_SOURCE() {
        return OTHER_SOURCE;
    }

    public void setOTHER_SOURCE(String OTHER_SOURCE) {
        this.OTHER_SOURCE = OTHER_SOURCE;
    }

    public String getOTHER_SOURCE_2() {
        return OTHER_SOURCE_2;
    }

    public void setOTHER_SOURCE_2(String OTHER_SOURCE_2) {
        this.OTHER_SOURCE_2 = OTHER_SOURCE_2;
    }

    public String getCONDUCT_FOREIGN_BANK_TRANSFER() {
        return CONDUCT_FOREIGN_BANK_TRANSFER;
    }

    public void setCONDUCT_FOREIGN_BANK_TRANSFER(String CONDUCT_FOREIGN_BANK_TRANSFER) {
        this.CONDUCT_FOREIGN_BANK_TRANSFER = CONDUCT_FOREIGN_BANK_TRANSFER;
    }

    public String getCOUNTRY_CODE_INWARD_FOREIGN1() {
        return COUNTRY_CODE_INWARD_FOREIGN1;
    }

    public void setCOUNTRY_CODE_INWARD_FOREIGN1(String COUNTRY_CODE_INWARD_FOREIGN1) {
        this.COUNTRY_CODE_INWARD_FOREIGN1 = COUNTRY_CODE_INWARD_FOREIGN1;
    }

    public String getCOUNTRY_DESC_INWARD_FOREIGN1() {
        return COUNTRY_DESC_INWARD_FOREIGN1;
    }

    public void setCOUNTRY_DESC_INWARD_FOREIGN1(String COUNTRY_DESC_INWARD_FOREIGN1) {
        this.COUNTRY_DESC_INWARD_FOREIGN1 = COUNTRY_DESC_INWARD_FOREIGN1;
    }

    public String getCOUNTRY_CODE_OUTWARD_FOREIGN1() {
        return COUNTRY_CODE_OUTWARD_FOREIGN1;
    }

    public void setCOUNTRY_CODE_OUTWARD_FOREIGN1(String COUNTRY_CODE_OUTWARD_FOREIGN1) {
        this.COUNTRY_CODE_OUTWARD_FOREIGN1 = COUNTRY_CODE_OUTWARD_FOREIGN1;
    }

    public String getCOUNTRY_DESC_OUTWARD_FOREIGN1() {
        return COUNTRY_DESC_OUTWARD_FOREIGN1;
    }

    public void setCOUNTRY_DESC_OUTWARD_FOREIGN1(String COUNTRY_DESC_OUTWARD_FOREIGN1) {
        this.COUNTRY_DESC_OUTWARD_FOREIGN1 = COUNTRY_DESC_OUTWARD_FOREIGN1;
    }

    public String getNAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1() {
        return NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1;
    }

    public void setNAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1(String NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1) {
        this.NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1 = NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1;
    }

    public String getRELATIONSHIP_FOREIGN_BENEFICIARY_1() {
        return RELATIONSHIP_FOREIGN_BENEFICIARY_1;
    }

    public void setRELATIONSHIP_FOREIGN_BENEFICIARY_1(String RELATIONSHIP_FOREIGN_BENEFICIARY_1) {
        this.RELATIONSHIP_FOREIGN_BENEFICIARY_1 = RELATIONSHIP_FOREIGN_BENEFICIARY_1;
    }

    public String getRELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1() {
        return RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1;
    }

    public void setRELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1(String RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1) {
        this.RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1 = RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1;
    }

    public String getCOUNTRY_CODE_INWARD_FOREIGN2() {
        return COUNTRY_CODE_INWARD_FOREIGN2;
    }

    public void setCOUNTRY_CODE_INWARD_FOREIGN2(String COUNTRY_CODE_INWARD_FOREIGN2) {
        this.COUNTRY_CODE_INWARD_FOREIGN2 = COUNTRY_CODE_INWARD_FOREIGN2;
    }

    public String getCOUNTRY_DESC_INWARD_FOREIGN2() {
        return COUNTRY_DESC_INWARD_FOREIGN2;
    }

    public void setCOUNTRY_DESC_INWARD_FOREIGN2(String COUNTRY_DESC_INWARD_FOREIGN2) {
        this.COUNTRY_DESC_INWARD_FOREIGN2 = COUNTRY_DESC_INWARD_FOREIGN2;
    }

    public String getCOUNTRY_CODE_OUTWARD_FOREIGN2() {
        return COUNTRY_CODE_OUTWARD_FOREIGN2;
    }

    public void setCOUNTRY_CODE_OUTWARD_FOREIGN2(String COUNTRY_CODE_OUTWARD_FOREIGN2) {
        this.COUNTRY_CODE_OUTWARD_FOREIGN2 = COUNTRY_CODE_OUTWARD_FOREIGN2;
    }

    public String getCOUNTRY_DESC_OUTWARD_FOREIGN2() {
        return COUNTRY_DESC_OUTWARD_FOREIGN2;
    }

    public void setCOUNTRY_DESC_OUTWARD_FOREIGN2(String COUNTRY_DESC_OUTWARD_FOREIGN2) {
        this.COUNTRY_DESC_OUTWARD_FOREIGN2 = COUNTRY_DESC_OUTWARD_FOREIGN2;
    }

    public String getNAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2() {
        return NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2;
    }

    public void setNAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2(String NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2) {
        this.NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2 = NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2;
    }

    public String getRELATIONSHIP_FOREIGN_BENEFICIARY_2() {
        return RELATIONSHIP_FOREIGN_BENEFICIARY_2;
    }

    public void setRELATIONSHIP_FOREIGN_BENEFICIARY_2(String RELATIONSHIP_FOREIGN_BENEFICIARY_2) {
        this.RELATIONSHIP_FOREIGN_BENEFICIARY_2 = RELATIONSHIP_FOREIGN_BENEFICIARY_2;
    }

    public String getRELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2() {
        return RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2;
    }

    public void setRELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2(String RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2) {
        this.RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2 = RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2;
    }

    public String getCOUNTRY_CODE_INWARD_FOREIGN3() {
        return COUNTRY_CODE_INWARD_FOREIGN3;
    }

    public void setCOUNTRY_CODE_INWARD_FOREIGN3(String COUNTRY_CODE_INWARD_FOREIGN3) {
        this.COUNTRY_CODE_INWARD_FOREIGN3 = COUNTRY_CODE_INWARD_FOREIGN3;
    }

    public String getCOUNTRY_DESC_INWARD_FOREIGN3() {
        return COUNTRY_DESC_INWARD_FOREIGN3;
    }

    public void setCOUNTRY_DESC_INWARD_FOREIGN3(String COUNTRY_DESC_INWARD_FOREIGN3) {
        this.COUNTRY_DESC_INWARD_FOREIGN3 = COUNTRY_DESC_INWARD_FOREIGN3;
    }

    public String getCOUNTRY_CODE_OUTWARD_FOREIGN3() {
        return COUNTRY_CODE_OUTWARD_FOREIGN3;
    }

    public void setCOUNTRY_CODE_OUTWARD_FOREIGN3(String COUNTRY_CODE_OUTWARD_FOREIGN3) {
        this.COUNTRY_CODE_OUTWARD_FOREIGN3 = COUNTRY_CODE_OUTWARD_FOREIGN3;
    }

    public String getCOUNTRY_DESC_OUTWARD_FOREIGN3() {
        return COUNTRY_DESC_OUTWARD_FOREIGN3;
    }

    public void setCOUNTRY_DESC_OUTWARD_FOREIGN3(String COUNTRY_DESC_OUTWARD_FOREIGN3) {
        this.COUNTRY_DESC_OUTWARD_FOREIGN3 = COUNTRY_DESC_OUTWARD_FOREIGN3;
    }

    public String getNAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3() {
        return NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3;
    }

    public void setNAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3(String NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3) {
        this.NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3 = NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3;
    }

    public String getRELATIONSHIP_FOREIGN_BENEFICIARY_3() {
        return RELATIONSHIP_FOREIGN_BENEFICIARY_3;
    }

    public void setRELATIONSHIP_FOREIGN_BENEFICIARY_3(String RELATIONSHIP_FOREIGN_BENEFICIARY_3) {
        this.RELATIONSHIP_FOREIGN_BENEFICIARY_3 = RELATIONSHIP_FOREIGN_BENEFICIARY_3;
    }

    public String getRELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3() {
        return RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3;
    }

    public void setRELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3(String RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3) {
        this.RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3 = RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3;
    }

    public String getID_NO() {
        return ID_NO;
    }

    public void setID_NO(String ID_NO) {
        this.ID_NO = ID_NO;
    }

    public String getNATIONALITY() {
        return NATIONALITY;
    }

    public void setNATIONALITY(String NATIONALITY) {
        this.NATIONALITY = NATIONALITY;
    }

    public String getDESC_NATIONALITY() {
        return DESC_NATIONALITY;
    }

    public void setDESC_NATIONALITY(String DESC_NATIONALITY) {
        this.DESC_NATIONALITY = DESC_NATIONALITY;
    }

    public String getBNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY() {
        return BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY;
    }

    public void setBNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY(String BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY) {
        this.BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY = BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY;
    }

    public String getACCT_HOLDR_MNT_OTH_ACCT_IN_PAK() {
        return ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK;
    }

    public void setACCT_HOLDR_MNT_OTH_ACCT_IN_PAK(String ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK) {
        this.ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK = ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK;
    }

    public String getNAME_OF_INSTITUTION_1() {
        return NAME_OF_INSTITUTION_1;
    }

    public void setNAME_OF_INSTITUTION_1(String NAME_OF_INSTITUTION_1) {
        this.NAME_OF_INSTITUTION_1 = NAME_OF_INSTITUTION_1;
    }

    public String getNAME_OF_INSTITUTION_2() {
        return NAME_OF_INSTITUTION_2;
    }

    public void setNAME_OF_INSTITUTION_2(String NAME_OF_INSTITUTION_2) {
        this.NAME_OF_INSTITUTION_2 = NAME_OF_INSTITUTION_2;
    }

    public String getNAME_OF_INSTITUTION_3() {
        return NAME_OF_INSTITUTION_3;
    }

    public void setNAME_OF_INSTITUTION_3(String NAME_OF_INSTITUTION_3) {
        this.NAME_OF_INSTITUTION_3 = NAME_OF_INSTITUTION_3;
    }

    public String getAVG_MTHLY_BAL_IN_THOSE_ACCTS() {
        return AVG_MTHLY_BAL_IN_THOSE_ACCTS;
    }

    public void setAVG_MTHLY_BAL_IN_THOSE_ACCTS(String AVG_MTHLY_BAL_IN_THOSE_ACCTS) {
        this.AVG_MTHLY_BAL_IN_THOSE_ACCTS = AVG_MTHLY_BAL_IN_THOSE_ACCTS;
    }

    public String getFOREIGN_CONTACT_NO_HIGH_RISK_NRP() {
        return FOREIGN_CONTACT_NO_HIGH_RISK_NRP;
    }

    public void setFOREIGN_CONTACT_NO_HIGH_RISK_NRP(String FOREIGN_CONTACT_NO_HIGH_RISK_NRP) {
        this.FOREIGN_CONTACT_NO_HIGH_RISK_NRP = FOREIGN_CONTACT_NO_HIGH_RISK_NRP;
    }

    public String getRESIDENTIAL_ADDRESS() {
        return RESIDENTIAL_ADDRESS;
    }

    public void setRESIDENTIAL_ADDRESS(String RESIDENTIAL_ADDRESS) {
        this.RESIDENTIAL_ADDRESS = RESIDENTIAL_ADDRESS;
    }

    public String getOFFICE_ADDRESS_FOREIGN_COUNTRY() {
        return OFFICE_ADDRESS_FOREIGN_COUNTRY;
    }

    public void setOFFICE_ADDRESS_FOREIGN_COUNTRY(String OFFICE_ADDRESS_FOREIGN_COUNTRY) {
        this.OFFICE_ADDRESS_FOREIGN_COUNTRY = OFFICE_ADDRESS_FOREIGN_COUNTRY;
    }

    public String getSAL_OF_FIN_SUPPORTER() {
        return SAL_OF_FIN_SUPPORTER;
    }

    public void setSAL_OF_FIN_SUPPORTER(String SAL_OF_FIN_SUPPORTER) {
        this.SAL_OF_FIN_SUPPORTER = SAL_OF_FIN_SUPPORTER;
    }

    public String getSAL_OF_FIN_SUPPORTER_EXP_AMT() {
        return SAL_OF_FIN_SUPPORTER_EXP_AMT;
    }

    public void setSAL_OF_FIN_SUPPORTER_EXP_AMT(String SAL_OF_FIN_SUPPORTER_EXP_AMT) {
        this.SAL_OF_FIN_SUPPORTER_EXP_AMT = SAL_OF_FIN_SUPPORTER_EXP_AMT;
    }

    public String getRENTAL_INCOME_FIN_SUPPORTER() {
        return RENTAL_INCOME_FIN_SUPPORTER;
    }

    public void setRENTAL_INCOME_FIN_SUPPORTER(String RENTAL_INCOME_FIN_SUPPORTER) {
        this.RENTAL_INCOME_FIN_SUPPORTER = RENTAL_INCOME_FIN_SUPPORTER;
    }

    public String getRENTAL_INCOME_FIN_SUPPORTER_EXP_AMT() {
        return RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT;
    }

    public void setRENTAL_INCOME_FIN_SUPPORTER_EXP_AMT(String RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT) {
        this.RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT = RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT;
    }

    public String getBUSS_INCOME_FIN_SUPPORTER() {
        return BUSS_INCOME_FIN_SUPPORTER;
    }

    public void setBUSS_INCOME_FIN_SUPPORTER(String BUSS_INCOME_FIN_SUPPORTER) {
        this.BUSS_INCOME_FIN_SUPPORTER = BUSS_INCOME_FIN_SUPPORTER;
    }

    public String getBUSS_INCOME_FIN_SUPPORTER_EXP_AMT() {
        return BUSS_INCOME_FIN_SUPPORTER_EXP_AMT;
    }

    public void setBUSS_INCOME_FIN_SUPPORTER_EXP_AMT(String BUSS_INCOME_FIN_SUPPORTER_EXP_AMT) {
        this.BUSS_INCOME_FIN_SUPPORTER_EXP_AMT = BUSS_INCOME_FIN_SUPPORTER_EXP_AMT;
    }

    public String getRETIREMENT_FUND() {
        return RETIREMENT_FUND;
    }

    public void setRETIREMENT_FUND(String RETIREMENT_FUND) {
        this.RETIREMENT_FUND = RETIREMENT_FUND;
    }

    public String getRETIREMENT_FUND_EXP_AMT() {
        return RETIREMENT_FUND_EXP_AMT;
    }

    public void setRETIREMENT_FUND_EXP_AMT(String RETIREMENT_FUND_EXP_AMT) {
        this.RETIREMENT_FUND_EXP_AMT = RETIREMENT_FUND_EXP_AMT;
    }

    public String getPENSION() {
        return PENSION;
    }

    public void setPENSION(String PENSION) {
        this.PENSION = PENSION;
    }

    public String getPENSION_EXP_AMT() {
        return PENSION_EXP_AMT;
    }

    public void setPENSION_EXP_AMT(String PENSION_EXP_AMT) {
        this.PENSION_EXP_AMT = PENSION_EXP_AMT;
    }

    public String getINHERITANCE() {
        return INHERITANCE;
    }

    public void setINHERITANCE(String INHERITANCE) {
        this.INHERITANCE = INHERITANCE;
    }

    public String getINHERITANCE_EXP_AMT() {
        return INHERITANCE_EXP_AMT;
    }

    public void setINHERITANCE_EXP_AMT(String INHERITANCE_EXP_AMT) {
        this.INHERITANCE_EXP_AMT = INHERITANCE_EXP_AMT;
    }

    public String getOTHER() {
        return OTHER;
    }

    public void setOTHER(String OTHER) {
        this.OTHER = OTHER;
    }

    public String getOTHER_EXP_AMT() {
        return OTHER_EXP_AMT;
    }

    public void setOTHER_EXP_AMT(String OTHER_EXP_AMT) {
        this.OTHER_EXP_AMT = OTHER_EXP_AMT;
    }

    public String getOTHER_SOURCE_NRP() {
        return OTHER_SOURCE_NRP;
    }

    public void setOTHER_SOURCE_NRP(String OTHER_SOURCE_NRP) {
        this.OTHER_SOURCE_NRP = OTHER_SOURCE_NRP;
    }

    public String getRELATIONSHIP_WITH_FIN() {
        return RELATIONSHIP_WITH_FIN;
    }

    public void setRELATIONSHIP_WITH_FIN(String RELATIONSHIP_WITH_FIN) {
        this.RELATIONSHIP_WITH_FIN = RELATIONSHIP_WITH_FIN;
    }

    public String getDESC_RELATIONSHIP_WITH_FIN() {
        return DESC_RELATIONSHIP_WITH_FIN;
    }

    public void setDESC_RELATIONSHIP_WITH_FIN(String DESC_RELATIONSHIP_WITH_FIN) {
        this.DESC_RELATIONSHIP_WITH_FIN = DESC_RELATIONSHIP_WITH_FIN;
    }

    public String getFIN_SUPP_GUR_FMLY_PEP() {
        return FIN_SUPP_GUR_FMLY_PEP;
    }

    public void setFIN_SUPP_GUR_FMLY_PEP(String FIN_SUPP_GUR_FMLY_PEP) {
        this.FIN_SUPP_GUR_FMLY_PEP = FIN_SUPP_GUR_FMLY_PEP;
    }

    public String getEDD_CONDUCT_BY_NAME() {
        return EDD_CONDUCT_BY_NAME;
    }

    public void setEDD_CONDUCT_BY_NAME(String EDD_CONDUCT_BY_NAME) {
        this.EDD_CONDUCT_BY_NAME = EDD_CONDUCT_BY_NAME;
    }

    public String getEDD_CONDUCT_BY_DATE() {
        return EDD_CONDUCT_BY_DATE;
    }

    public void setEDD_CONDUCT_BY_DATE(String EDD_CONDUCT_BY_DATE) {
        this.EDD_CONDUCT_BY_DATE = EDD_CONDUCT_BY_DATE;
    }

    public String getAPPROVED_BY_NAME() {
        return APPROVED_BY_NAME;
    }

    public void setAPPROVED_BY_NAME(String APPROVED_BY_NAME) {
        this.APPROVED_BY_NAME = APPROVED_BY_NAME;
    }

    public String getAPPROVED_BY_DATE() {
        return APPROVED_BY_DATE;
    }

    public void setAPPROVED_BY_DATE(String APPROVED_BY_DATE) {
        this.APPROVED_BY_DATE = APPROVED_BY_DATE;
    }

    public String getAPPROVED_COMPL_AUTH_NAME() {
        return APPROVED_COMPL_AUTH_NAME;
    }

    public void setAPPROVED_COMPL_AUTH_NAME(String APPROVED_COMPL_AUTH_NAME) {
        this.APPROVED_COMPL_AUTH_NAME = APPROVED_COMPL_AUTH_NAME;
    }

    public String getAPPROVED_COMPL_AUTH_DATE() {
        return APPROVED_COMPL_AUTH_DATE;
    }

    public void setAPPROVED_COMPL_AUTH_DATE(String APPROVED_COMPL_AUTH_DATE) {
        this.APPROVED_COMPL_AUTH_DATE = APPROVED_COMPL_AUTH_DATE;
    }

    public String getCONDUCT_LOCAL_BANK_TRANSFER() {
        return CONDUCT_LOCAL_BANK_TRANSFER;
    }

    public void setCONDUCT_LOCAL_BANK_TRANSFER(String CONDUCT_LOCAL_BANK_TRANSFER) {
        this.CONDUCT_LOCAL_BANK_TRANSFER = CONDUCT_LOCAL_BANK_TRANSFER;
    }

    public String getCOUNTRY_CODE_INWARD_LOCAL1() {
        return COUNTRY_CODE_INWARD_LOCAL1;
    }

    public void setCOUNTRY_CODE_INWARD_LOCAL1(String COUNTRY_CODE_INWARD_LOCAL1) {
        this.COUNTRY_CODE_INWARD_LOCAL1 = COUNTRY_CODE_INWARD_LOCAL1;
    }

    public String getCOUNTRY_DESC_INWARD_LOCAL1() {
        return COUNTRY_DESC_INWARD_LOCAL1;
    }

    public void setCOUNTRY_DESC_INWARD_LOCAL1(String COUNTRY_DESC_INWARD_LOCAL1) {
        this.COUNTRY_DESC_INWARD_LOCAL1 = COUNTRY_DESC_INWARD_LOCAL1;
    }

    public String getCOUNTRY_CODE_OUTWARD_LOCAL1() {
        return COUNTRY_CODE_OUTWARD_LOCAL1;
    }

    public void setCOUNTRY_CODE_OUTWARD_LOCAL1(String COUNTRY_CODE_OUTWARD_LOCAL1) {
        this.COUNTRY_CODE_OUTWARD_LOCAL1 = COUNTRY_CODE_OUTWARD_LOCAL1;
    }

    public String getCOUNTRY_DESC_OUTWARD_LOCAL1() {
        return COUNTRY_DESC_OUTWARD_LOCAL1;
    }

    public void setCOUNTRY_DESC_OUTWARD_LOCAL1(String COUNTRY_DESC_OUTWARD_LOCAL1) {
        this.COUNTRY_DESC_OUTWARD_LOCAL1 = COUNTRY_DESC_OUTWARD_LOCAL1;
    }

    public String getNAME_OF_BENEFICIARY_LOCAL_TRANSFER_1() {
        return NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1;
    }

    public void setNAME_OF_BENEFICIARY_LOCAL_TRANSFER_1(String NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1) {
        this.NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1 = NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1;
    }

    public String getRELATIONSHIP_LOCAL_BENEFICIARY_1() {
        return RELATIONSHIP_LOCAL_BENEFICIARY_1;
    }

    public void setRELATIONSHIP_LOCAL_BENEFICIARY_1(String RELATIONSHIP_LOCAL_BENEFICIARY_1) {
        this.RELATIONSHIP_LOCAL_BENEFICIARY_1 = RELATIONSHIP_LOCAL_BENEFICIARY_1;
    }

    public String getRELATIONSHIP_LOCAL_BENEFICIARY_DESC_1() {
        return RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1;
    }

    public void setRELATIONSHIP_LOCAL_BENEFICIARY_DESC_1(String RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1) {
        this.RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1 = RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1;
    }

    public String getCOUNTRY_CODE_INWARD_LOCAL2() {
        return COUNTRY_CODE_INWARD_LOCAL2;
    }

    public void setCOUNTRY_CODE_INWARD_LOCAL2(String COUNTRY_CODE_INWARD_LOCAL2) {
        this.COUNTRY_CODE_INWARD_LOCAL2 = COUNTRY_CODE_INWARD_LOCAL2;
    }

    public String getCOUNTRY_DESC_INWARD_LOCAL2() {
        return COUNTRY_DESC_INWARD_LOCAL2;
    }

    public void setCOUNTRY_DESC_INWARD_LOCAL2(String COUNTRY_DESC_INWARD_LOCAL2) {
        this.COUNTRY_DESC_INWARD_LOCAL2 = COUNTRY_DESC_INWARD_LOCAL2;
    }

    public String getCOUNTRY_CODE_OUTWARD_LOCAL2() {
        return COUNTRY_CODE_OUTWARD_LOCAL2;
    }

    public void setCOUNTRY_CODE_OUTWARD_LOCAL2(String COUNTRY_CODE_OUTWARD_LOCAL2) {
        this.COUNTRY_CODE_OUTWARD_LOCAL2 = COUNTRY_CODE_OUTWARD_LOCAL2;
    }

    public String getCOUNTRY_DESC_OUTWARD_LOCAL2() {
        return COUNTRY_DESC_OUTWARD_LOCAL2;
    }

    public void setCOUNTRY_DESC_OUTWARD_LOCAL2(String COUNTRY_DESC_OUTWARD_LOCAL2) {
        this.COUNTRY_DESC_OUTWARD_LOCAL2 = COUNTRY_DESC_OUTWARD_LOCAL2;
    }

    public String getNAME_OF_BENEFICIARY_LOCAL_TRANSFER_2() {
        return NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2;
    }

    public void setNAME_OF_BENEFICIARY_LOCAL_TRANSFER_2(String NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2) {
        this.NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2 = NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2;
    }

    public String getRELATIONSHIP_LOCAL_BENEFICIARY_2() {
        return RELATIONSHIP_LOCAL_BENEFICIARY_2;
    }

    public void setRELATIONSHIP_LOCAL_BENEFICIARY_2(String RELATIONSHIP_LOCAL_BENEFICIARY_2) {
        this.RELATIONSHIP_LOCAL_BENEFICIARY_2 = RELATIONSHIP_LOCAL_BENEFICIARY_2;
    }

    public String getRELATIONSHIP_LOCAL_BENEFICIARY_DESC_2() {
        return RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2;
    }

    public void setRELATIONSHIP_LOCAL_BENEFICIARY_DESC_2(String RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2) {
        this.RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2 = RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2;
    }

    public String getCOUNTRY_CODE_INWARD_LOCAL3() {
        return COUNTRY_CODE_INWARD_LOCAL3;
    }

    public void setCOUNTRY_CODE_INWARD_LOCAL3(String COUNTRY_CODE_INWARD_LOCAL3) {
        this.COUNTRY_CODE_INWARD_LOCAL3 = COUNTRY_CODE_INWARD_LOCAL3;
    }

    public String getCOUNTRY_DESC_INWARD_LOCAL3() {
        return COUNTRY_DESC_INWARD_LOCAL3;
    }

    public void setCOUNTRY_DESC_INWARD_LOCAL3(String COUNTRY_DESC_INWARD_LOCAL3) {
        this.COUNTRY_DESC_INWARD_LOCAL3 = COUNTRY_DESC_INWARD_LOCAL3;
    }

    public String getCOUNTRY_CODE_OUTWARD_LOCAL3() {
        return COUNTRY_CODE_OUTWARD_LOCAL3;
    }

    public void setCOUNTRY_CODE_OUTWARD_LOCAL3(String COUNTRY_CODE_OUTWARD_LOCAL3) {
        this.COUNTRY_CODE_OUTWARD_LOCAL3 = COUNTRY_CODE_OUTWARD_LOCAL3;
    }

    public String getCOUNTRY_DESC_OUTWARD_LOCAL3() {
        return COUNTRY_DESC_OUTWARD_LOCAL3;
    }

    public void setCOUNTRY_DESC_OUTWARD_LOCAL3(String COUNTRY_DESC_OUTWARD_LOCAL3) {
        this.COUNTRY_DESC_OUTWARD_LOCAL3 = COUNTRY_DESC_OUTWARD_LOCAL3;
    }

    public String getNAME_OF_BENEFICIARY_LOCAL_TRANSFER_3() {
        return NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3;
    }

    public void setNAME_OF_BENEFICIARY_LOCAL_TRANSFER_3(String NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3) {
        this.NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3 = NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3;
    }

    public String getRELATIONSHIP_LOCAL_BENEFICIARY_DESC_3() {
        return RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3;
    }

    public void setRELATIONSHIP_LOCAL_BENEFICIARY_DESC_3(String RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3) {
        this.RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3 = RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3;
    }

    public String getRELATIONSHIP_LOCAL_BENEFICIARY_3() {
        return RELATIONSHIP_LOCAL_BENEFICIARY_3;
    }

    public void setRELATIONSHIP_LOCAL_BENEFICIARY_3(String RELATIONSHIP_LOCAL_BENEFICIARY_3) {
        this.RELATIONSHIP_LOCAL_BENEFICIARY_3 = RELATIONSHIP_LOCAL_BENEFICIARY_3;
    }

    public String getPURPOSE_OF_FOREIGN_TRANSACTION() {
        return PURPOSE_OF_FOREIGN_TRANSACTION;
    }

    public void setPURPOSE_OF_FOREIGN_TRANSACTION(String PURPOSE_OF_FOREIGN_TRANSACTION) {
        this.PURPOSE_OF_FOREIGN_TRANSACTION = PURPOSE_OF_FOREIGN_TRANSACTION;
    }

    public String getDESC_REASON_EDD() {
        return DESC_REASON_EDD;
    }

    public void setDESC_REASON_EDD(String DESC_REASON_EDD) {
        this.DESC_REASON_EDD = DESC_REASON_EDD;
    }

    public String getPURPOSE_OF_LOCAL_TRANSACTION() {
        return PURPOSE_OF_LOCAL_TRANSACTION;
    }

    public void setPURPOSE_OF_LOCAL_TRANSACTION(String PURPOSE_OF_LOCAL_TRANSACTION) {
        this.PURPOSE_OF_LOCAL_TRANSACTION = PURPOSE_OF_LOCAL_TRANSACTION;
    }

    public String getAMOUNT_OF_FOREIGN_TRANSACTION() {
        return AMOUNT_OF_FOREIGN_TRANSACTION;
    }

    public void setAMOUNT_OF_FOREIGN_TRANSACTION(String AMOUNT_OF_FOREIGN_TRANSACTION) {
        this.AMOUNT_OF_FOREIGN_TRANSACTION = AMOUNT_OF_FOREIGN_TRANSACTION;
    }

    public String getAMOUNT_OF_LOCAL_TRANSACTION() {
        return AMOUNT_OF_LOCAL_TRANSACTION;
    }

    public void setAMOUNT_OF_LOCAL_TRANSACTION(String AMOUNT_OF_LOCAL_TRANSACTION) {
        this.AMOUNT_OF_LOCAL_TRANSACTION = AMOUNT_OF_LOCAL_TRANSACTION;
    }

    public String getIS_NON_RESIDENT_INFORMATION() {
        return IS_NON_RESIDENT_INFORMATION;
    }

    public void setIS_NON_RESIDENT_INFORMATION(String IS_NON_RESIDENT_INFORMATION) {
        this.IS_NON_RESIDENT_INFORMATION = IS_NON_RESIDENT_INFORMATION;
    }

    public String getIS_HIGH_RISK_SEGMENT() {
        return IS_HIGH_RISK_SEGMENT;
    }

    public void setIS_HIGH_RISK_SEGMENT(String IS_HIGH_RISK_SEGMENT) {
        this.IS_HIGH_RISK_SEGMENT = IS_HIGH_RISK_SEGMENT;
    }

    public String getIS_THE_FINANCIAL_SUPPORTER() {
        return IS_THE_FINANCIAL_SUPPORTER;
    }

    public void setIS_THE_FINANCIAL_SUPPORTER(String IS_THE_FINANCIAL_SUPPORTER) {
        this.IS_THE_FINANCIAL_SUPPORTER = IS_THE_FINANCIAL_SUPPORTER;
    }
}
