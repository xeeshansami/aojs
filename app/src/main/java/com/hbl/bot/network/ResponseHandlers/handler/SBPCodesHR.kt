package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class SBPCodesHR(callBack: SBPCodesCallBack) : BaseRH<SBPCodesResponse>() {
    var callback: SBPCodesCallBack = callBack
    override fun onSuccess(response: SBPCodesResponse?) {
        response?.let { callback.SBPCodesSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.SBPCodesFailure(it) }
    }
}