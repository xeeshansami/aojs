package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class CustomerRiskRatingHR(callBack: CustomerRiskRatingCallBack) : BaseRH<RiskRatingResponse>() {
    var callback: CustomerRiskRatingCallBack = callBack
    override fun onSuccess(response: RiskRatingResponse?) {
        response?.let { callback.CustomerRiskRatingSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CustomerRiskRatingFailure(it) }
    }
}