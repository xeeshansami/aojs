package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.VerifyOTPData
import java.io.Serializable


class VerifyEmailResponse() : Serializable, Parcelable {
    @SerializedName("status")
    @Expose
     val status: String? = null

    @SerializedName("message")
    @Expose
     val message: String? = null

    @SerializedName("data")
    @Expose
    val data: ArrayList<VerifyOTPData>? = null

    constructor(source: Parcel) : this(
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<VerifyEmailResponse> = object : Parcelable.Creator<VerifyEmailResponse> {
            override fun createFromParcel(source: Parcel): VerifyEmailResponse = VerifyEmailResponse(source)
            override fun newArray(size: Int): Array<VerifyEmailResponse?> = arrayOfNulls(size)
        }
    }
}