package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.VerifyOTPCallBack
import com.hbl.bot.network.models.response.base.*

class VerifyOTPHR(callBack: VerifyOTPCallBack) : BaseRH<VerifyOTPResponse>() {
    var callback: VerifyOTPCallBack = callBack
    override fun onSuccess(response: VerifyOTPResponse?) {
        response?.let { callback.VerifyOTPSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.VerifyOTPFailure(it) }
    }
}