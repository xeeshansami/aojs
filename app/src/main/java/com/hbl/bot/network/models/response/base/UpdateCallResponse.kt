package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.*
import java.io.Serializable


class UpdateCallResponse() : Serializable, Parcelable {
    @SerializedName("status")
    @Expose
    val status: String? = null

    @SerializedName("message")
    @Expose
    val message: String? = null

    @SerializedName("data")
    @Expose
    val data: ArrayList<Call>? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<UpdateCallResponse> {
        override fun createFromParcel(parcel: Parcel): UpdateCallResponse {
            return UpdateCallResponse(parcel)
        }

        override fun newArray(size: Int): Array<UpdateCallResponse?> {
            return arrayOfNulls(size)
        }
    }
}