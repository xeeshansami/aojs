package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Capacity() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("CAPACITY_CODE")
    @Expose
    var CAPACITY_CODE: String? = null

    @SerializedName("CAPACITY_DESC")
    @Expose
    var CAPACITY_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<Capacity> {
        override fun createFromParcel(parcel: Parcel): Capacity {
            return Capacity(parcel)
        }

        override fun newArray(size: Int): Array<Capacity?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return CAPACITY_DESC.toString()
    }
}