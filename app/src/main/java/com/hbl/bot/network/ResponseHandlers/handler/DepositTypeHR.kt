package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class DepositTypeHR(callBack: DepositTypeCallBack) : BaseRH<DepositTypeResponse>() {
    var callback: DepositTypeCallBack = callBack
    override fun onSuccess(response: DepositTypeResponse?) {
        response?.let { callback.DepositTypeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.DepositTypeFailure(it) }
    }
}