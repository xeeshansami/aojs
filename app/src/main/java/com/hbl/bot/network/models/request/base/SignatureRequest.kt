package com.hbl.bot.network.models.request.base


import com.google.gson.annotations.SerializedName

class SignatureRequest {
    @SerializedName("SIGN_IMG")
    var minorSign: String = ""
    @SerializedName("SIGN_IMG1")
    var gaurdianSign: String = ""
    @SerializedName("TRACKING_ID")
    var tRACKINGID: String = ""
}