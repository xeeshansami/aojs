package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface RelationshipCallBack {
    fun RelationshipSuccess(response: RelationshipResponse)
    fun RelationshipFailure(response: BaseResponse)
}