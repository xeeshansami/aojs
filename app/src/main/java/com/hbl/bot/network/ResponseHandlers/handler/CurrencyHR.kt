package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class CurrencyHR(callBack: CurrencyCallBack) : BaseRH<CurrencyResponse>() {
    var callback: CurrencyCallBack = callBack
    override fun onSuccess(response: CurrencyResponse?) {
        response?.let { callback.CurrencySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CurrencyFailure(it) }
    }
}