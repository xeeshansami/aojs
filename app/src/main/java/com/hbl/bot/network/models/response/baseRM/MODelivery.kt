package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class MODelivery() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("MODE")
    @Expose
    var MODE: String? = null

    @SerializedName("MODE_DESC")
    @Expose
    var MODE_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<MODelivery> {
        override fun createFromParcel(parcel: Parcel): MODelivery {
            return MODelivery(parcel)
        }

        override fun newArray(size: Int): Array<MODelivery?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return MODE_DESC.toString()
    }
}