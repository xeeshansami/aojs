package com.hbl.bot.network.models.response.baseRM

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class HawCompanyData : Serializable {
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("OrgCode")
    var OrgCode: String? = ""

    @SerializedName("NameOfCompanyOrEmployer")
    var NameOfCompanyOrEmployer: String? = ""

    @SerializedName("IndustrySubCategory")
    var IndustrySubCategory: String? = ""

    @SerializedName("IndustryMainCategory")
    var IndustryMainCategory: String? = ""

    @SerializedName("OfficeAddress")
    var OfficeAddress: String? = ""



    companion object {
        private const val serialVersionUID = -3558286338197055835L
    }


    override fun toString(): String {
        return "$OrgCode - $NameOfCompanyOrEmployer"
    }
}