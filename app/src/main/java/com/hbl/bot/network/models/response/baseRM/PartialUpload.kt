package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class PartialUpload() : Serializable, Parcelable {
    @SerializedName("keyword")
    @Expose
    val keyword: String? = null

    @SerializedName("path")
    @Expose
    val path: String? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<PartialUpload> {
        override fun createFromParcel(parcel: Parcel): PartialUpload {
            return PartialUpload(parcel)
        }

        override fun newArray(size: Int): Array<PartialUpload?> {
            return arrayOfNulls(size)
        }
    }

}