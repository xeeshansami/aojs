package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable

class KonnectDebitCardInquiryRequest() : Parcelable {
    var tracking_id: String? = null
    var doc_id: String? = null
    var mobile_number: String? = null
    var flag: String? = null
    var branch_id:String? = null

    constructor(parcel: Parcel) : this() {
        tracking_id = parcel.readString()
        doc_id = parcel.readString()
        mobile_number = parcel.readString()
        flag = parcel.readString()
        branch_id = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(tracking_id)
        parcel.writeString(doc_id)
        parcel.writeString(mobile_number)
        parcel.writeString(flag)
        parcel.writeString(branch_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KonnectDebitCardInquiryRequest> {
        override fun createFromParcel(parcel: Parcel): KonnectDebitCardInquiryRequest {
            return KonnectDebitCardInquiryRequest(parcel)
        }

        override fun newArray(size: Int): Array<KonnectDebitCardInquiryRequest?> {
            return arrayOfNulls(size)
        }
    }
}