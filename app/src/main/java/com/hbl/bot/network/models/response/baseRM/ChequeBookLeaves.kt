package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ChequeBookLeaves() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("CHEQUEBOOK_LEAVES")
    @Expose
    var CHEQUEBOOK_LEAVES: String? = null

    @SerializedName("CHEQUEBOOK_LEAVES_DESC")
    @Expose
    var CHEQUEBOOK_LEAVES_DESC: String? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<ChequeBookLeaves> {
        override fun createFromParcel(parcel: Parcel): ChequeBookLeaves {
            return ChequeBookLeaves(parcel)
        }

        override fun newArray(size: Int): Array<ChequeBookLeaves?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return CHEQUEBOOK_LEAVES_DESC.toString()
    }
}