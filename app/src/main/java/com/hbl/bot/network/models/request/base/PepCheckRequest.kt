package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable

class PepCheckRequest() : Parcelable {
    var cnic = ""
    var dob = ""
    var name = ""

    constructor(parcel: Parcel) : this() {
        cnic = parcel.readString()!!
        dob = parcel.readString()!!
        name = parcel.readString()!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(cnic)
        parcel.writeString(dob)
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PepCheckRequest> {
        override fun createFromParcel(parcel: Parcel): PepCheckRequest {
            return PepCheckRequest(parcel)
        }

        override fun newArray(size: Int): Array<PepCheckRequest?> {
            return arrayOfNulls(size)
        }
    }
}