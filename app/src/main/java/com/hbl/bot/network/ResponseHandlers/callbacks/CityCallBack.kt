package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface CityCallBack {
    fun CitySuccess(response: CityResponse)
    fun CityFailure(response: BaseResponse)
}