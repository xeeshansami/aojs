package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class KonnectInquiry() : Parcelable {
    var RESPONSE_CODE: String? = null
    var RESPONSE_MESSAGE: String? = null
    var TRAN_ID: String? = null
    var OTP: String? = ""

    constructor(parcel: Parcel) : this() {
        RESPONSE_CODE = parcel.readString()
        RESPONSE_MESSAGE = parcel.readString()
        TRAN_ID = parcel.readString()
        OTP = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(RESPONSE_CODE)
        parcel.writeString(RESPONSE_MESSAGE)
        parcel.writeString(TRAN_ID)
        parcel.writeString(OTP)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KonnectInquiry> {
        override fun createFromParcel(parcel: Parcel): KonnectInquiry {
            return KonnectInquiry(parcel)
        }

        override fun newArray(size: Int): Array<KonnectInquiry?> {
            return arrayOfNulls(size)
        }
    }
}