package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class UrlHR(callBack: UrlCallBack) : BaseRH<UrlResponse>() {
    var callback: UrlCallBack = callBack
    override fun onSuccess(response: UrlResponse?) {
        response?.let { callback.Success(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.Failure(it) }
    }
}