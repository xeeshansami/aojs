package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.hbl.bot.network.models.response.baseRM.SubmitData

class AllSubmitResponse(
) : Parcelable {
    var data: ArrayList<SubmitData>? = null
    var message: String? = null
    var status: String? = null

    constructor(parcel: Parcel) : this() {
        message = parcel.readString()
        status = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(message)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AllSubmitResponse> {
        override fun createFromParcel(parcel: Parcel): AllSubmitResponse {
            return AllSubmitResponse(parcel)
        }

        override fun newArray(size: Int): Array<AllSubmitResponse?> {
            return arrayOfNulls(size)
        }
    }
}