
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CUST_SOLE_SELF {

    @SerializedName("STATUS_OF_OWNERSHIP")
    @Expose
    private String STATUS_OF_OWNERSHIP;

    public String getSTATUS_OF_OWNERSHIP() {
        return STATUS_OF_OWNERSHIP;
    }

    public void setSTATUS_OF_OWNERSHIP(String STATUS_OF_OWNERSHIP) {
        this.STATUS_OF_OWNERSHIP = STATUS_OF_OWNERSHIP;
    }
}
