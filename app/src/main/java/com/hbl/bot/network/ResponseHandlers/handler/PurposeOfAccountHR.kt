package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class PurposeOfAccountHR(callBack: PurposeOfAccountCallBack) : BaseRH<PurposeOfAccountResponse>() {
    var callback: PurposeOfAccountCallBack = callBack
    override fun onSuccess(response: PurposeOfAccountResponse?) {
        response?.let { callback.PurposeOfAccountSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PurposeOfAccountFailure(it) }
    }
}