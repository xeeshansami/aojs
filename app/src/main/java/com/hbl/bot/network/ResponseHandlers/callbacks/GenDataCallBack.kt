package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface GenDataCallBack {
    fun GenCifSuccess(response: CIFResponse)
    fun GenCifFailure(response: BaseResponse)
}