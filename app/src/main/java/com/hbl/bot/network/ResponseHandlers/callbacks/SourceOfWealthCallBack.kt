package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface SourceOfWealthCallBack {
    fun SourceOfWealthSuccess(response: SourceOfWealthResponse)
    fun SourceOfWealthFailure(response: BaseResponse)
}