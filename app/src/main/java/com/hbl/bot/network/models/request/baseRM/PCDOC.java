
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hbl.bot.network.models.response.baseRM.DocumentType;

import java.util.ArrayList;

public class PCDOC {

    public ArrayList<DocumentType> getMANDATORY_DATA() {
        return MANDATORY_DATA;
    }

    public void setMANDATORY_DATA(ArrayList<DocumentType> MANDATORY_DATA) {
        this.MANDATORY_DATA = MANDATORY_DATA;
    }

    public ArrayList<String> getOPTIONAL_DATA() {
        return OPTIONAL_DATA;
    }

    public void setOPTIONAL_DATA(ArrayList<String> OPTIONAL_DATA) {
        this.OPTIONAL_DATA = OPTIONAL_DATA;
    }

    public ArrayList<ImageBuffer> getLOAD_PARTIAL_OLD_IMAGES_BUFFER() {
        return LOAD_PARTIAL_OLD_IMAGES_BUFFER;
    }

    public void setLOAD_PARTIAL_OLD_IMAGES_BUFFER(ArrayList<ImageBuffer> LOAD_PARTIAL_OLD_IMAGES_BUFFER) {
        this.LOAD_PARTIAL_OLD_IMAGES_BUFFER = LOAD_PARTIAL_OLD_IMAGES_BUFFER;
    }

    public ArrayList<String> getFILES() {
        return FILES;
    }

    public void setFILES(ArrayList<String> FILES) {
        this.FILES = FILES;
    }

    public String getPARTIAL_DOC_ENABLE() {
        return PARTIAL_DOC_ENABLE;
    }

    public void setPARTIAL_DOC_ENABLE(String PARTIAL_DOC_ENABLE) {
        this.PARTIAL_DOC_ENABLE = PARTIAL_DOC_ENABLE;
    }

    public String getTAG_ACTIVE() {
        return TAG_ACTIVE;
    }

    public void setTAG_ACTIVE(String TAG_ACTIVE) {
        this.TAG_ACTIVE = TAG_ACTIVE;
    }

    public String getTRACKING_ID() {
        return TRACKING_ID;
    }

    public void setTRACKING_ID(String TRACKING_ID) {
        this.TRACKING_ID = TRACKING_ID;
    }

    @SerializedName("MANDATORY_DATA")
    @Expose
    public ArrayList<DocumentType> MANDATORY_DATA;


    @SerializedName("OPTIONAL_DATA")
    @Expose
    public ArrayList<String> OPTIONAL_DATA;


    @SerializedName("LOAD_PARTIAL_OLD_IMAGES_BUFFER")
    @Expose
    public ArrayList<ImageBuffer> LOAD_PARTIAL_OLD_IMAGES_BUFFER;


    @SerializedName("FILES")
    @Expose
    public ArrayList<String> FILES;


    @SerializedName("PARTIAL_DOC_ENABLE")
    @Expose
    public String PARTIAL_DOC_ENABLE;


    @SerializedName("TAG_ACTIVE")
    @Expose
    public String TAG_ACTIVE;

    @SerializedName("TRACKING_ID")
    @Expose
    public String TRACKING_ID;

}
