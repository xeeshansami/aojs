package com.hbl.bot.network.models.response.baseRM

import com.hbl.bot.network.models.response.base.*

interface ProffessionCallBack {
    fun ProffessionSuccess(response: ProfessionResponse)
    fun ProffessionFailure(response: BaseResponse)
}