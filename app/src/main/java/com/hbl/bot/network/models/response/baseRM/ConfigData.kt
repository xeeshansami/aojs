package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ConfigData() : Serializable, Parcelable {

    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("DOC_TYPE")
    @Expose
    val DOC_TYPE: String? = null

    @SerializedName("PROTOCOL")
    @Expose
    val PROTOCOL: String? = null

    @SerializedName("IP")
    @Expose
    val IP: String? = null

    @SerializedName("PORT")
    @Expose
    val PORT: String? = null

    @SerializedName("LOCAL_FOLDER_PATH")
    @Expose
    val LOCAL_FOLDER_PATH: String? = null

    @SerializedName("SERVER_FOLDER_PATH")
    @Expose
    val SERVER_FOLDER_PATH: String? = null

    @SerializedName("ARCHIVE_FOLDER_PATH")
    @Expose
    val ARCHIVE_FOLDER_PATH: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<ConfigData> {
        override fun createFromParcel(parcel: Parcel): ConfigData {
            return ConfigData(parcel)
        }

        override fun newArray(size: Int): Array<ConfigData?> {
            return arrayOfNulls(size)
        }
    }


}