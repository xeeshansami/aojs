package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.LoginCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.LoginResponse

class LoginBaseHR(callBack: LoginCallBack) : BaseRH<LoginResponse>() {
    var callback: LoginCallBack = callBack
    override fun onSuccess(response: LoginResponse?) {
        response?.let { callback.LoginSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.LoginFailure(it) }
    }
}