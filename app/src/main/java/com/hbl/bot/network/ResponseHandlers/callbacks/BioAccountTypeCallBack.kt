package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface BioAccountTypeCallBack {
    fun AccountTypSuccess(response: BioAccountTypeResponse)
    fun AccountTypFailure(response: BaseResponse)
}