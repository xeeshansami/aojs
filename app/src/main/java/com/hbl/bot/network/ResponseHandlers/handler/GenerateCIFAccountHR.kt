package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class GenerateCIFAccountHR(callBack: GenerateCIFAccountCallBack) :
    BaseRH<GenerateCIFAccountResponse>() {
    var callback: GenerateCIFAccountCallBack = callBack
    override fun onSuccess(response: GenerateCIFAccountResponse?) {
        response?.let { callback.GenerateCIFAccountSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.GenerateCIFAccountFailure(it) }
    }
}