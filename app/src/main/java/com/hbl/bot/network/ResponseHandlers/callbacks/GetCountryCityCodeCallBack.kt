package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface GetCountryCityCodeCallBack {
    fun GetCountryCityCodeSuccess(response: CountryCityCodeResponse)
    fun GetCountryCityCodeFailure(response: BaseResponse)
}