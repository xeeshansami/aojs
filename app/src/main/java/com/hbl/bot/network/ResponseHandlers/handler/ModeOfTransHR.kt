package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class ModeOfTransHR(callBack: ModeOfTransCallBack) : BaseRH<ModeOfTransResponse>() {
    var callback: ModeOfTransCallBack = callBack
    override fun onSuccess(response: ModeOfTransResponse?) {
        response?.let { callback.ModeOfTransSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ModeOfTransFailure(it) }
    }
}