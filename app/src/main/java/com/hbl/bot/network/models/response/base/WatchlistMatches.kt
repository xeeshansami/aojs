package com.hbl.bot.network.models.response.base

data class WatchlistMatches(
    val customerName: String,
    val matches: List<Any>
)