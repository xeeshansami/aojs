package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class BranchCode() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("PRIORITY")
    @Expose
    val PRIORITY: String? = null

    @SerializedName("BR_CODE")
    @Expose
    val BR_CODE: String? = null

    @SerializedName("BR_NAME")
    @Expose
    val BR_NAME: String? = null

    @SerializedName("BR_ADDR1")
    @Expose
    val BR_ADDR1: String? = null

    @SerializedName("BR_ADD2")
    @Expose
    val BR_ADD2: String? = null

    @SerializedName("BR_ADD3")
    @Expose
    val BR_ADD3: String? = null

    @SerializedName("BR_ADD4")
    @Expose
    val BR_ADD4: String? = null

    @SerializedName("CATLY")
    @Expose
    val CATLY: String? = null

    @SerializedName("BR_PH")
    @Expose
    val BR_PH: String? = null

    @SerializedName("BR_REG")
    @Expose
    val BR_REG: String? = null

    @SerializedName("IS_ACTIVE")
    @Expose
    val IS_ACTIVE: String? = null

    @SerializedName("IS_DASHBOARD_ALLOW")
    @Expose
    val IS_DASHBOARD_ALLOW: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<BranchCode> {
        override fun createFromParcel(parcel: Parcel): BranchCode {
            return BranchCode(parcel)
        }

        override fun newArray(size: Int): Array<BranchCode?> {
            return arrayOfNulls(size)
        }
    }
}