package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface GenderCallBack {
    fun GenderSuccess(response: GenderResponse)
    fun GenderFailure(response: BaseResponse)
}