package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class IndustrySubCategory() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("SOIISC_CODE")
    @Expose
    var SOIISC_CODE: String? = null

    @SerializedName("SOIISC_DESC")
    @Expose
    var SOIISC_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<IndustrySubCategory> {
        override fun createFromParcel(parcel: Parcel): IndustrySubCategory {
            return IndustrySubCategory(parcel)
        }

        override fun newArray(size: Int): Array<IndustrySubCategory?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return SOIISC_DESC.toString()
    }
}