package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface SignatureCallBack {
    fun Success(response: SignatureResponse)
    fun Failure(response: BaseResponse)
}