package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface CountCallBack {
    fun CountSuccess(response: CountResponse)
    fun CountFailure(response: BaseResponse)
}