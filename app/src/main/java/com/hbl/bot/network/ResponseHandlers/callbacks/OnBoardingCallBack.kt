package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface OnBoardingCallBack {
    fun OnBoardingSuccess(response: OnBoardingResponse)
    fun OnBoardingFailure(response: BaseResponse)
}