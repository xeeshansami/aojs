package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class PreferencesMailing() : Parcelable {
    var PREFERRED_ADDR: String = ""
    var PREFERRED_CODE: String = ""
    var _id: String = ""

    constructor(parcel: Parcel) : this() {
        PREFERRED_ADDR = parcel.readString().toString()
        PREFERRED_CODE = parcel.readString().toString()
        _id = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PREFERRED_ADDR)
        parcel.writeString(PREFERRED_CODE)
        parcel.writeString(_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PreferencesMailing> {
        override fun createFromParcel(parcel: Parcel): PreferencesMailing {
            return PreferencesMailing(parcel)
        }

        override fun newArray(size: Int): Array<PreferencesMailing?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return PREFERRED_ADDR.toString()
    }
}