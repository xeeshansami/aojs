package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.HawKeysCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.HawKeysResponse

class HawKeysBaseHR(callBack: HawKeysCallBack) : BaseRH<HawKeysResponse>() {
    var callback: HawKeysCallBack = callBack
    override fun onSuccess(response: HawKeysResponse?) {
        response?.let { callback.Success(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.Failure(it) }
    }
}