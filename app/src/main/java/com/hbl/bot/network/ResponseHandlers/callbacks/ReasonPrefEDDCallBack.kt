package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ReasonPrefEDDCallBack {
    fun ReasonPrefEDDSuccess(response: ReasonPrefEDDResponse)
    fun ReasonPrefEDDFailure(response: BaseResponse)
}