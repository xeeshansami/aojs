package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class KonnectBranchCity() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null
    @SerializedName("BR_NAME")
    @Expose
    val BR_NAME: String? = null
    @SerializedName("BR_CODE")
    @Expose
    val BR_CODE: String? = null

    @SerializedName("CITY")
    @Expose
    val CITY: String? = null
    @SerializedName("REGION")
    @Expose
    val REGION: String? = null
    @SerializedName("BUSINESS_GROUP")
    @Expose
    val BUSINESS_GROUP: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<KonnectBranchCity> {
        override fun createFromParcel(parcel: Parcel): KonnectBranchCity {
            return KonnectBranchCity(parcel)
        }

        override fun newArray(size: Int): Array<KonnectBranchCity?> {
            return arrayOfNulls(size)
        }
    }
}