package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class KonnectOpenAccount() : Parcelable {
    var RESPONSE_CODE: String? = null
    var RESPONSE_MESSAGE: String? = null
    var FINGER_INDICES: ArrayList<String>? = ArrayList()

    constructor(parcel: Parcel) : this() {
        RESPONSE_CODE = parcel.readString()
        RESPONSE_MESSAGE = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(RESPONSE_CODE)
        parcel.writeString(RESPONSE_MESSAGE)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KonnectOpenAccount> {
        override fun createFromParcel(parcel: Parcel): KonnectOpenAccount {
            return KonnectOpenAccount(parcel)
        }

        override fun newArray(size: Int): Array<KonnectOpenAccount?> {
            return arrayOfNulls(size)
        }
    }


}