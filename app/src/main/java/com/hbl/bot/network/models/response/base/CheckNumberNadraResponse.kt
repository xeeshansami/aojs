package com.hbl.bot.network.models.response.base


import com.google.gson.annotations.SerializedName

class CheckNumberNadraResponse {
    @SerializedName("data")
    var `data`: List<Any> = listOf()
    @SerializedName("message")
    var message: String = ""
    @SerializedName("status")
    var status: String = ""
}