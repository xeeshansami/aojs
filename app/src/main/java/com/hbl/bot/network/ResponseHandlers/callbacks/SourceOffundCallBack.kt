package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface SourceOffundCallBack {
    fun SourceOffundSuccess(response: SourceOffundResponse)
    fun SourceOffundFailure(response: BaseResponse)
}