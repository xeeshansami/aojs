package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class GenderHR(callBack: GenderCallBack) : BaseRH<GenderResponse>() {
    var callback: GenderCallBack = callBack
    override fun onSuccess(response: GenderResponse?) {
        response?.let { callback.GenderSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.GenderFailure(it) }
    }
}