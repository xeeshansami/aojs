package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class AccountTypeHR(callBack: AccountTypeCallBack) : BaseRH<AccountTypeResponse>() {
    var callback: AccountTypeCallBack = callBack
    override fun onSuccess(response: AccountTypeResponse?) {
        response?.let { callback.AccountTypeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.AccountTypeFailure(it) }
    }
}