package com.hbl.bot.network.models.response.base

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.HawKeys
import java.io.Serializable


class HawKeysResponse : Serializable {
    @SerializedName("status")
    @Expose
    var status: String? = null

    @SerializedName("message")
    @Expose
    var message: String? = null

    @SerializedName("data")
    @Expose
    var data: ArrayList<HawKeys>? = null

    companion object {
        private const val serialVersionUID = 2080986510565832595L
    }


}