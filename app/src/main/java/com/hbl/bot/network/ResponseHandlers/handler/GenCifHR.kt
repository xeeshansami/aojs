package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class GenCifHR(callBack: GenDataCallBack) : BaseRH<CIFResponse>() {
    var callback: GenDataCallBack = callBack
    override fun onSuccess(response: CIFResponse?) {
        response?.let { callback.GenCifSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.GenCifFailure(it) }
    }
}