package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface EstateFrequencyCallBack {
    fun EstateFrequencySuccess(response: EstateFrequencyResponse)
    fun EstateFrequencyFailure(response: BaseResponse)
}