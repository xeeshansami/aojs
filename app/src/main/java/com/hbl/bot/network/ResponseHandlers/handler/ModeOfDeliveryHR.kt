package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.ModeOfDeliveryCallBack
import com.hbl.bot.network.models.response.base.*

class ModeOfDeliveryHR(callBack: ModeOfDeliveryCallBack) : BaseRH<MODResponse>() {
    var callback: ModeOfDeliveryCallBack = callBack
    override fun onSuccess(response: MODResponse?) {
        response?.let { callback.ModeOfDeliverySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ModeOfDeliveryFailure(it) }
    }
}