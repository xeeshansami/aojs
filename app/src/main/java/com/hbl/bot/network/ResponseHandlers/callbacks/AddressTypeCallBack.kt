package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface AddressTypeCallBack {
    fun AddressTypeSuccess(response: AddressTypeResponse)
    fun AddressTypeFailure(response: BaseResponse)
}