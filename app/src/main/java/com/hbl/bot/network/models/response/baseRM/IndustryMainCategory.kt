package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class IndustryMainCategory() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("SOIIC_CODE")
    @Expose
    var SOIIC_CODE: String? = null

    @SerializedName("SOIIC_DESC")
    @Expose
    var SOIIC_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<IndustryMainCategory> {
        override fun createFromParcel(parcel: Parcel): IndustryMainCategory {
            return IndustryMainCategory(parcel)
        }

        override fun newArray(size: Int): Array<IndustryMainCategory?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return SOIIC_DESC.toString()
    }
}