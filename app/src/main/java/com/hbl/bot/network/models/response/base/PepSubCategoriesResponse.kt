package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.*
import java.io.Serializable


class PepSubCategoriesResponse() : Serializable, Parcelable {
    @SerializedName("status")
    @Expose
    private val status: String? = null

    @SerializedName("message")
    @Expose
    private val message: String? = null

    @SerializedName("data")
    @Expose
    val data: ArrayList<PepSubCategories>? = null

    constructor(source: Parcel) : this(
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<PepSubCategoriesResponse> =
            object : Parcelable.Creator<PepSubCategoriesResponse> {
                override fun createFromParcel(source: Parcel): PepSubCategoriesResponse =
                    PepSubCategoriesResponse(source)

                override fun newArray(size: Int): Array<PepSubCategoriesResponse?> =
                    arrayOfNulls(size)
            }
    }
}