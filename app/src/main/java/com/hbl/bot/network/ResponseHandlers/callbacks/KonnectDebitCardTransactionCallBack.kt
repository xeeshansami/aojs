package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface KonnectDebitCardTransactionCallBack {
    fun konnectDebitCardTransactionSuccess(response: KonnectDebitCardTransactionResponse)
    fun konnectDebitCardTransactionFailure(response: BaseResponse)
}