package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface NatureOfBusinessCallBack {
    fun NatureOfBusinessSuccess(response: NatureOfBusinessResponse)
    fun NatureOfBusinessFailure(response: BaseResponse)
}