package com.hbl.bot.network.models.request.base

import com.google.gson.annotations.SerializedName
import java.util.ArrayList

data class RemoteFile(

    @SerializedName("name") val name: String,
    @SerializedName("data") val data: MutableList<String> = ArrayList()
)