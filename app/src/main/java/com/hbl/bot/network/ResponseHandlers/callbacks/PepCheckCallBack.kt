package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PepCheckCallBack {
    fun PepCheckSuccess(response: PepCheckResponse)
    fun PepCheckFailure(response: BaseResponse)
}