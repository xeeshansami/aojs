package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class KonnectPurposeOfAccount() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    var _id: String? = null

    @SerializedName("PUR_ID")
    @Expose
    var PUR_ID: String? = null

    @SerializedName("AACT_LEVEL_ID")
    @Expose
    var AACT_LEVEL_ID: String? = null

    @SerializedName("PUR_CODE")
    @Expose
    var PUR_CODE: String? = null

    @SerializedName("PUR_DESC")
    @Expose
    var PUR_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<KonnectPurposeOfAccount> {
        override fun createFromParcel(parcel: Parcel): KonnectPurposeOfAccount {
            return KonnectPurposeOfAccount(parcel)
        }

        override fun newArray(size: Int): Array<KonnectPurposeOfAccount?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return PUR_DESC.toString()
    }
}