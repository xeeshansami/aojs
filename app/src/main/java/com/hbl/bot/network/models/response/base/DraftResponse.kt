package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.request.baseRM.Data

class DraftResponse(
) : Parcelable {
    @SerializedName("data")
    var data: ArrayList<Data> = ArrayList<Data>()
    var message: String? = null
    var status: String? = null

    constructor(parcel: Parcel) : this() {
        message = parcel.readString()
        status = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(message)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DraftResponse> {
        override fun createFromParcel(parcel: Parcel): DraftResponse {
            return DraftResponse(parcel)
        }

        override fun newArray(size: Int): Array<DraftResponse?> {
            return arrayOfNulls(size)
        }
    }
}