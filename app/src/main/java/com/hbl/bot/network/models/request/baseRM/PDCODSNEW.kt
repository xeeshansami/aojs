/* 
Copyright (c) 2020 Kotlin SubmitData Classes Generated from JSON powered by http://www.json2kotlin.com

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

For support, please feel free to contact me at https://www.linkedin.com/in/syedabsar */
package com.hbl.bot.network.models.request.baseRM

import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.DocumentType
import java.util.ArrayList


class PDCODSNEW {


    @SerializedName("MANDATORY_DATA")
    var MANDATORY_DATA: ArrayList<DocumentType> = arrayListOf<DocumentType>()

    @SerializedName("OPTIONAL_DATA")
    var OPTIONAL_DATA: ArrayList<Any> = arrayListOf<Any>()


    @SerializedName("LOAD_PARTIAL_OLD_IMAGES_BUFFER")
    var LOAD_PARTIAL_OLD_IMAGES_BUFFER: ArrayList<ImageBuffer> = arrayListOf<ImageBuffer>()


    @SerializedName("FILES")
    var FILES: ArrayList<String> = arrayListOf<String>()


    @SerializedName("PARTIAL_DOC_ENABLE")
    var PARTIAL_DOC_ENABLE = ""


    @SerializedName("TAG_ACTIVE")
    var TAG_ACTIVE = ""

    @SerializedName("TRACKING_ID")
    var TRACKING_ID = ""


}