package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface BusinessAreaCallBack {
    fun BusinessAreaSuccess(response: BusinessAreaResponse)
    fun BusinessAreaFailure(response: BaseResponse)
}