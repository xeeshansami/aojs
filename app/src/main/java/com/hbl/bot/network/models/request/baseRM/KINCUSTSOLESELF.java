
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KINCUSTSOLESELF {

    @SerializedName("DATE_OF_INCEPTION")
    @Expose
    private String DATE_OF_INCEPTION = "";
    @SerializedName("STATUS_OF_OWNERSHIP")
    @Expose
    public String STATUS_OF_OWNERSHIP = "";
    @SerializedName("BUSS_GEOG_AREAS")
    @Expose
    private String BUSS_GEOG_AREAS = "";
    @SerializedName("EXPC_COUNT_PART_1")
    @Expose
    private String EXPC_COUNT_PART_1 = "";
    @SerializedName("EXPC_COUNT_PART_2")
    @Expose
    private String EXPC_COUNT_PART_2 = "";
    @SerializedName("EXPC_COUNT_PART_3")
    @Expose
    private String EXPC_COUNT_PART_3 = "";

    public String getDATEOFINCEPTION() {
        return DATE_OF_INCEPTION;
    }

    public void setDATEOFINCEPTION(String dATEOFINCEPTION) {
        this.DATE_OF_INCEPTION = dATEOFINCEPTION;
    }

    public String getSTATUSOFOWNERSHIP() {
        return STATUS_OF_OWNERSHIP;
    }

    public void setSTATUSOFOWNERSHIP(String sTATUSOFOWNERSHIP) {
        this.STATUS_OF_OWNERSHIP = sTATUSOFOWNERSHIP;
    }

    public String getBUSSGEOGAREAS() {
        return BUSS_GEOG_AREAS;
    }

    public void setBUSSGEOGAREAS(String bUSSGEOGAREAS) {
        this.BUSS_GEOG_AREAS = bUSSGEOGAREAS;
    }

    public String getEXPCCOUNTPART1() {
        return EXPC_COUNT_PART_1;
    }

    public void setEXPCCOUNTPART1(String eXPCCOUNTPART1) {
        this.EXPC_COUNT_PART_1 = eXPCCOUNTPART1;
    }

    public String getEXPCCOUNTPART2() {
        return EXPC_COUNT_PART_2;
    }

    public void setEXPCCOUNTPART2(String eXPCCOUNTPART2) {
        this.EXPC_COUNT_PART_2 = eXPCCOUNTPART2;
    }

    public String getEXPCCOUNTPART3() {
        return EXPC_COUNT_PART_3;
    }

    public void setEXPCCOUNTPART3(String eXPCCOUNTPART3) {
        this.EXPC_COUNT_PART_3 = eXPCCOUNTPART3;
    }

}
