package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface GenerateCIFCallBack {
    fun GenerateCIFSuccess(response: GenerateCIFResponse)
    fun GenerateCIFFailure(response: BaseResponse)
}