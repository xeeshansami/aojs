package com.hbl.bot.network.models.response.baseRM

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DocsData : Serializable {
    @SerializedName("_id")
    var id: String? = null

    @SerializedName("ID_DOC_CODE")
    var iDDOCCODE: String? = null

    @SerializedName("ID_DOC_DESC")
    var iDDOCDESC: String? = null

    companion object {
        private const val serialVersionUID = -3558286338197055835L
    }


    override fun toString(): String {
        return iDDOCDESC.toString()
    }
}