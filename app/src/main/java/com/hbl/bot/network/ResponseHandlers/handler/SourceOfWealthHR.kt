package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class SourceOfWealthHR(callBack: SourceOfWealthCallBack) : BaseRH<SourceOfWealthResponse>() {
    var callback: SourceOfWealthCallBack = callBack
    override fun onSuccess(response: SourceOfWealthResponse?) {
        response?.let { callback.SourceOfWealthSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.SourceOfWealthFailure(it) }
    }
}