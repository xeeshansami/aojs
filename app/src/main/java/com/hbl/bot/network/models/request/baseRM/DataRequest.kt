package com.hbl.bot.network.models.request.baseRM

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DataRequest() : Serializable {
    @SerializedName("data")
    @Expose
    var data: Data? = null

    @SerializedName("payload")
    @Expose
    var payload: String? = null

}