package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class ETBNTBRequest() : Parcelable {
    var TRACKING_ID: String? = ""
    var CHANNEL: String? = null
    var CHANNELDATE: String? = null
    var CHANNELLID: String? = null
    var CHANNELSRLNO: String? = null
    var CHANNELTIME: String? = null
    var CNIC: String? = null
    var REMARK: String? = null
    var SERVICENAME: String? = null
    var TELLERID: String? = null
    var payload: String? = null

    constructor(parcel: Parcel) : this() {
        TRACKING_ID = parcel.readString()
        CHANNEL = parcel.readString()
        CHANNELDATE = parcel.readString()
        CHANNELLID = parcel.readString()
        CHANNELSRLNO = parcel.readString()
        CHANNELTIME = parcel.readString()
        CNIC = parcel.readString()
        REMARK = parcel.readString()
        SERVICENAME = parcel.readString()
        TELLERID = parcel.readString()
        payload = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(TRACKING_ID)
        parcel.writeString(CHANNEL)
        parcel.writeString(CHANNELDATE)
        parcel.writeString(CHANNELLID)
        parcel.writeString(CHANNELSRLNO)
        parcel.writeString(CHANNELTIME)
        parcel.writeString(CNIC)
        parcel.writeString(REMARK)
        parcel.writeString(SERVICENAME)
        parcel.writeString(TELLERID)
        parcel.writeString(payload)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ETBNTBRequest> {
        override fun createFromParcel(parcel: Parcel): ETBNTBRequest {
            return ETBNTBRequest(parcel)
        }

        override fun newArray(size: Int): Array<ETBNTBRequest?> {
            return arrayOfNulls(size)
        }
    }
}