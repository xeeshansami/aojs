package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CardProduct() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("PRD_CODE")
    @Expose
    var PRD_CODE: String? = null

    @SerializedName("PRD_NAME")
    @Expose
    var PRD_NAME: String? = null

    @SerializedName("PRD_ACTP")
    @Expose
    val PRD_ACTP: String? = null

    @SerializedName("PRD_ACCT_DESC")
    @Expose
    val PRD_ACCT_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<CardProduct> {
        override fun createFromParcel(parcel: Parcel): CardProduct {
            return CardProduct(parcel)
        }

        override fun newArray(size: Int): Array<CardProduct?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return PRD_NAME.toString()
    }
}