package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface HTMLCallBack {
    fun HTMLSuccess(response: HTMLResponse)
    fun HTMLFailure(response: BaseResponse)
}