package com.hbl.bot.network.models.response.base

import com.google.gson.annotations.SerializedName


data class DashboardResponse(

    @SerializedName("status") var status: String? = null,
    @SerializedName("message") var message: String? = null,
    @SerializedName("data") var data: ArrayList<String> = arrayListOf()

)