package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface CustomerAofAccountInfoCallBack {
    fun CustomerAofAccountInfoSuccess(response: AllSubmitResponse)
    fun CustomerAofAccountInfoFailure(response: BaseResponse)
}