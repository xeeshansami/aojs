package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class KINCUSTFINX(
) : Parcelable {
    var BUSS_INDUS_SOURCE_OF_INCOME = ""
    var CIF_NO_AVAILABLE = ""
    var DESC_BUSS_INDUS_SOURCE_OF_INCOME = ""
    var DESC_NATIONALITY_STAY_COUNTRY = ""
    var DESC_OCCUPATION_FIN_SUPPORTER = ""
    var DESC_RELATIONSHIP_FIN_SUPPORTER = ""
    var ID_DOCUMENT_NO = ""
    var ID_DOCUMENT_TYPE = ""
    var ID_DOCUMENT_TYPE_DESC = ""
    var MTHLY_INCOME_PKR = ""
    var NAME_FIN_SUPPORTER = ""
    var NATIONALITY_STAY_COUNTRY = ""
    var OCCUPATION_FIN_SUPPORTER = ""
    var RELATIONSHIP_FIN_SUPPORTER = ""

    constructor(parcel: Parcel) : this() {
        BUSS_INDUS_SOURCE_OF_INCOME = parcel.readString().toString()
        CIF_NO_AVAILABLE = parcel.readString().toString()
        DESC_BUSS_INDUS_SOURCE_OF_INCOME = parcel.readString().toString()
        DESC_NATIONALITY_STAY_COUNTRY = parcel.readString().toString()
        DESC_OCCUPATION_FIN_SUPPORTER = parcel.readString().toString()
        DESC_RELATIONSHIP_FIN_SUPPORTER = parcel.readString().toString()
        ID_DOCUMENT_NO = parcel.readString().toString()
        ID_DOCUMENT_TYPE = parcel.readString().toString()
        ID_DOCUMENT_TYPE_DESC = parcel.readString().toString()
        MTHLY_INCOME_PKR = parcel.readString().toString()
        NAME_FIN_SUPPORTER = parcel.readString().toString()
        NATIONALITY_STAY_COUNTRY = parcel.readString().toString()
        OCCUPATION_FIN_SUPPORTER = parcel.readString().toString()
        RELATIONSHIP_FIN_SUPPORTER = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(BUSS_INDUS_SOURCE_OF_INCOME)
        parcel.writeString(CIF_NO_AVAILABLE)
        parcel.writeString(DESC_BUSS_INDUS_SOURCE_OF_INCOME)
        parcel.writeString(DESC_NATIONALITY_STAY_COUNTRY)
        parcel.writeString(DESC_OCCUPATION_FIN_SUPPORTER)
        parcel.writeString(DESC_RELATIONSHIP_FIN_SUPPORTER)
        parcel.writeString(ID_DOCUMENT_NO)
        parcel.writeString(ID_DOCUMENT_TYPE)
        parcel.writeString(ID_DOCUMENT_TYPE_DESC)
        parcel.writeString(MTHLY_INCOME_PKR)
        parcel.writeString(NAME_FIN_SUPPORTER)
        parcel.writeString(NATIONALITY_STAY_COUNTRY)
        parcel.writeString(OCCUPATION_FIN_SUPPORTER)
        parcel.writeString(RELATIONSHIP_FIN_SUPPORTER)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KINCUSTFINX> {
        override fun createFromParcel(parcel: Parcel): KINCUSTFINX {
            return KINCUSTFINX(parcel)
        }

        override fun newArray(size: Int): Array<KINCUSTFINX?> {
            return arrayOfNulls(size)
        }
    }
}