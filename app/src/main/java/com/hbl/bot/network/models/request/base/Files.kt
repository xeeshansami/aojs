package com.hbl.bot.network.models.request.base

import com.google.gson.annotations.SerializedName


data class Files(

    @SerializedName("RemoteFile") val remoteFile: RemoteFile
)