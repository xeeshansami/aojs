package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class ReasonPrefEDDHR(callBack: ReasonPrefEDDCallBack) : BaseRH<ReasonPrefEDDResponse>() {
    var callback: ReasonPrefEDDCallBack = callBack
    override fun onSuccess(response: ReasonPrefEDDResponse?) {
        response?.let { callback.ReasonPrefEDDSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ReasonPrefEDDFailure(it) }
    }
}