package com.hbl.bot.network.ResponseHandlers.handler
import com.hbl.bot.network.ResponseHandlers.callbacks.PrintDocumentCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.PrintDocumentResponse

class PrintDocumentBaseHR(callBack: PrintDocumentCallBack) : BaseRH<PrintDocumentResponse>() {
    var callback: PrintDocumentCallBack=callBack
    override fun onSuccess(response: PrintDocumentResponse?) {
        response?.let { callback.PrintDocumentSuccess(it) }
    }
    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PrintDocumentFailure(it) }
    }
}