package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class NatureOfBusinessHR(callBack: NatureOfBusinessCallBack) : BaseRH<NatureOfBusinessResponse>() {
    var callback: NatureOfBusinessCallBack = callBack
    override fun onSuccess(response: NatureOfBusinessResponse?) {
        response?.let { callback.NatureOfBusinessSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.NatureOfBusinessFailure(it) }
    }
}