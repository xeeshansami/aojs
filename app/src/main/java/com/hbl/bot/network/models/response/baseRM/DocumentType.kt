package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DocumentType() : Serializable, Parcelable {
    @SerializedName("DOCID")
    @Expose
    var DOCID: String? = null

    @SerializedName("DOCDS")
    @Expose
    var DOCDS: String? = null


    @SerializedName("SUB_DOCID")
    @Expose
    val data: ArrayList<DocumentType>? = null

    @SerializedName("SEQ_NO")
    @Expose
    var SEQ_NO: String? = null

    @SerializedName("KEYWORD")
    @Expose
    val KEYWORD: String? = null

    @SerializedName("PARTIAL_KEY")
    @Expose
    var PARTIAL_KEY: Boolean? = null

    @SerializedName("PR_Doc_BTN_NAME")
    @Expose
    var PR_Doc_BTN_NAME: String? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<DocumentType> {
        override fun createFromParcel(parcel: Parcel): DocumentType {
            return DocumentType(parcel)
        }

        override fun newArray(size: Int): Array<DocumentType?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return DOCDS.toString()
    }
}