package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class ConsumerProductHR(callBack: ConsumerProductCallBack) : BaseRH<ConsumerProductResponse>() {
    var callback: ConsumerProductCallBack = callBack
    override fun onSuccess(response: ConsumerProductResponse?) {
        response?.let { callback.ConsumerProductSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ConsumerProductFailure(it) }
    }
}