package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class FindCurrency(
) : Parcelable {
    var CURR = ""

    constructor(parcel: Parcel) : this() {
        CURR = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(CURR)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<FindCurrency> {
        override fun createFromParcel(parcel: Parcel): FindCurrency {
            return FindCurrency(parcel)
        }

        override fun newArray(size: Int): Array<FindCurrency?> {
            return arrayOfNulls(size)
        }
    }
}