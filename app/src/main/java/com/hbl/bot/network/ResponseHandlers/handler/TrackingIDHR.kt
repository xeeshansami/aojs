package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class TrackingIDHR(callBack: TrackingIDCallBack) : BaseRH<TrackingIDResponse>() {
    var callback: TrackingIDCallBack = callBack
    override fun onSuccess(response: TrackingIDResponse?) {
        response?.let { callback.TrackingIDSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.TrackingIDFailure(it) }
    }
}