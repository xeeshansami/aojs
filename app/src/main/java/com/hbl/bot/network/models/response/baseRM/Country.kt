package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Country() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("COUNTRY_CODE")
    @Expose
    var COUNTRY_CODE: String? = null

    @SerializedName("COUNTRY_NAME")
    @Expose
    var COUNTRY_NAME: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<Country> {
        override fun createFromParcel(parcel: Parcel): Country {
            return Country(parcel)
        }

        override fun newArray(size: Int): Array<Country?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return COUNTRY_NAME.toString()
    }
}