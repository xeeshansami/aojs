package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.PepSubCategoriesCallBack
import com.hbl.bot.network.models.response.base.*

class PepSubCategoriesHR(callBack: PepSubCategoriesCallBack) : BaseRH<PepSubCategoriesResponse>() {
    var callback: PepSubCategoriesCallBack = callBack
    override fun onSuccess(response: PepSubCategoriesResponse?) {
        response?.let { callback.PepSubCategoriesSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PepSubCategoriesFailure(it) }
    }
}