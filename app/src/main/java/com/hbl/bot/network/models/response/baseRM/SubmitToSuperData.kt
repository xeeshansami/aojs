package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class SubmitToSuperData() : Serializable, Parcelable {

    @SerializedName("n")
    @Expose
    private val n: Int? = null

    @SerializedName("nModified")
    @Expose
    private val nModified: Int? = null

    @SerializedName("ok")
    @Expose
    private val ok: Int? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<SubmitToSuperData> {
        override fun createFromParcel(parcel: Parcel): SubmitToSuperData {
            return SubmitToSuperData(parcel)
        }

        override fun newArray(size: Int): Array<SubmitToSuperData?> {
            return arrayOfNulls(size)
        }
    }


}