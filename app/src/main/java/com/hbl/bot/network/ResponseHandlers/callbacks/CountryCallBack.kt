package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.CountryResponse

interface CountryCallBack {
    fun CountrySuccess(response: CountryResponse)
    fun CountryFailure(response: BaseResponse)
}