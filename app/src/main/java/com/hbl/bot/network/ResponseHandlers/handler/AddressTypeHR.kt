package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class AddressTypeHR(callBack: AddressTypeCallBack) : BaseRH<AddressTypeResponse>() {
    var callback: AddressTypeCallBack = callBack
    override fun onSuccess(response: AddressTypeResponse?) {
        response?.let { callback.AddressTypeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.AddressTypeFailure(it) }
    }
}