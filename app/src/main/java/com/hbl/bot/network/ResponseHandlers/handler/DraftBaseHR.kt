package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.DraftCallBack
import com.hbl.bot.network.models.response.base.AofAccountInfoResponse
import com.hbl.bot.network.models.response.base.BaseResponse

class DraftBaseHR(callBack: DraftCallBack) : BaseRH<AofAccountInfoResponse>() {
    var callback: DraftCallBack = callBack
    override fun onSuccess(response: AofAccountInfoResponse?) {
        response?.let { callback.DraftSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.DraftFailure(it) }
    }
}