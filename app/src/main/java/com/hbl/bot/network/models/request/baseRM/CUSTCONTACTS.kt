package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class CUSTCONTACTS(
) : Parcelable {
    var COUNTRY_DIAL_CODE = ""
    var EMAIL_ADDRESS = ""
    var IS_US_Dail_Code: Boolean = false
    var MOBILE_NO = ""
    var MOBILE_OPERATOR_CODE = ""
    var OFFICE_NO = ""
    var PREFERREDMAILADDRDESC = ""
    var PREFERRED_MAIL_ADDR = ""
    var RES_LANDLINE = ""

    constructor(parcel: Parcel) : this() {
        PREFERRED_MAIL_ADDR = parcel.readString().toString()
        PREFERREDMAILADDRDESC = parcel.readString().toString()
        RES_LANDLINE = parcel.readString().toString()
        COUNTRY_DIAL_CODE = parcel.readString().toString()
        MOBILE_NO = parcel.readString().toString()
        OFFICE_NO = parcel.readString().toString()
        EMAIL_ADDRESS = parcel.readString().toString()
        MOBILE_OPERATOR_CODE = parcel.readString().toString()
        IS_US_Dail_Code = (parcel.readValue(Boolean::class.java.classLoader) as? Boolean)!!
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PREFERRED_MAIL_ADDR)
        parcel.writeString(PREFERREDMAILADDRDESC)
        parcel.writeString(RES_LANDLINE)
        parcel.writeString(COUNTRY_DIAL_CODE)
        parcel.writeString(MOBILE_NO)
        parcel.writeString(OFFICE_NO)
        parcel.writeString(EMAIL_ADDRESS)
        parcel.writeString(MOBILE_OPERATOR_CODE)
        parcel.writeValue(IS_US_Dail_Code)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CUSTCONTACTS> {
        override fun createFromParcel(parcel: Parcel): CUSTCONTACTS {
            return CUSTCONTACTS(parcel)
        }

        override fun newArray(size: Int): Array<CUSTCONTACTS?> {
            return arrayOfNulls(size)
        }
    }
}