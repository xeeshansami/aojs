package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.CustomerSegmentResponse

interface CustomerSegmentCallBack {
    fun CustomerSegmentSuccess(response: CustomerSegmentResponse)
    fun CustomerSegmentFailure(response: BaseResponse)
}