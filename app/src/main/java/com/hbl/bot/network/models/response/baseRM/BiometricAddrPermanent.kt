package com.hbl.bot.network.models.response.baseRM

class BiometricAddrPermanent(
) {
    var ADDRESS_LINE1: String = ""
    var ADDRESS_LINE2: String = ""
    var ADDRESS_LINE3: String = ""
}