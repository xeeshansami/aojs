
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CUST_FIN {

    @SerializedName("NAME_FIN_SUPPORTER")
    @Expose
    private String NAME_FIN_SUPPORTER;

    @SerializedName("OCCUPATION_FIN_SUPPORTER")
    @Expose
    private String OCCUPATION_FIN_SUPPORTER;


    @SerializedName("BUSS_INDUS_SOURCE_OF_INCOME")
    @Expose
    private String BUSS_INDUS_SOURCE_OF_INCOME;

    @SerializedName("NATIONALITY_STAY_COUNTRY")
    @Expose
    private String NATIONALITY_STAY_COUNTRY;


    @SerializedName("ID_DOCUMENT_TYPE")
    @Expose
    private String ID_DOCUMENT_TYPE;

    @SerializedName("ID_DOCUMENT_NO")
    @Expose
    private String ID_DOCUMENT_NO;


    @SerializedName("MTHLY_INCOME_PKR")
    @Expose
    private String MTHLY_INCOME_PKR;

    @SerializedName("RELATIONSHIP_FIN_SUPPORTER")
    @Expose
    private String RELATIONSHIP_FIN_SUPPORTER;


    public String getNAME_FIN_SUPPORTER() {
        return NAME_FIN_SUPPORTER;
    }

    public void setNAME_FIN_SUPPORTER(String NAME_FIN_SUPPORTER) {
        this.NAME_FIN_SUPPORTER = NAME_FIN_SUPPORTER;
    }

    public String getOCCUPATION_FIN_SUPPORTER() {
        return OCCUPATION_FIN_SUPPORTER;
    }

    public void setOCCUPATION_FIN_SUPPORTER(String OCCUPATION_FIN_SUPPORTER) {
        this.OCCUPATION_FIN_SUPPORTER = OCCUPATION_FIN_SUPPORTER;
    }

    public String getBUSS_INDUS_SOURCE_OF_INCOME() {
        return BUSS_INDUS_SOURCE_OF_INCOME;
    }

    public void setBUSS_INDUS_SOURCE_OF_INCOME(String BUSS_INDUS_SOURCE_OF_INCOME) {
        this.BUSS_INDUS_SOURCE_OF_INCOME = BUSS_INDUS_SOURCE_OF_INCOME;
    }

    public String getNATIONALITY_STAY_COUNTRY() {
        return NATIONALITY_STAY_COUNTRY;
    }

    public void setNATIONALITY_STAY_COUNTRY(String NATIONALITY_STAY_COUNTRY) {
        this.NATIONALITY_STAY_COUNTRY = NATIONALITY_STAY_COUNTRY;
    }

    public String getID_DOCUMENT_TYPE() {
        return ID_DOCUMENT_TYPE;
    }

    public void setID_DOCUMENT_TYPE(String ID_DOCUMENT_TYPE) {
        this.ID_DOCUMENT_TYPE = ID_DOCUMENT_TYPE;
    }

    public String getID_DOCUMENT_NO() {
        return ID_DOCUMENT_NO;
    }

    public void setID_DOCUMENT_NO(String ID_DOCUMENT_NO) {
        this.ID_DOCUMENT_NO = ID_DOCUMENT_NO;
    }

    public String getMTHLY_INCOME_PKR() {
        return MTHLY_INCOME_PKR;
    }

    public void setMTHLY_INCOME_PKR(String MTHLY_INCOME_PKR) {
        this.MTHLY_INCOME_PKR = MTHLY_INCOME_PKR;
    }

    public String getRELATIONSHIP_FIN_SUPPORTER() {
        return RELATIONSHIP_FIN_SUPPORTER;
    }

    public void setRELATIONSHIP_FIN_SUPPORTER(String RELATIONSHIP_FIN_SUPPORTER) {
        this.RELATIONSHIP_FIN_SUPPORTER = RELATIONSHIP_FIN_SUPPORTER;
    }

    public String getCIF_NO_AVAILABLE() {
        return CIF_NO_AVAILABLE;
    }

    public void setCIF_NO_AVAILABLE(String CIF_NO_AVAILABLE) {
        this.CIF_NO_AVAILABLE = CIF_NO_AVAILABLE;
    }

    @SerializedName("CIF_NO_AVAILABLE")
    @Expose
    private String CIF_NO_AVAILABLE;


}
