package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Find(
) : Parcelable {
    @SerializedName("BR_CODE")
    var BR_CODE: String? = null

    constructor(parcel: Parcel) : this() {
        BR_CODE = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(BR_CODE)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Find> {
        override fun createFromParcel(parcel: Parcel): Find {
            return Find(parcel)
        }

        override fun newArray(size: Int): Array<Find?> {
            return arrayOfNulls(size)
        }
    }
}