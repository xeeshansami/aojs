package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class Find(
) : Parcelable {
    var BR_CODE = ""

    constructor(parcel: Parcel) : this() {
        BR_CODE = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(BR_CODE)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Find> {
        override fun createFromParcel(parcel: Parcel): Find {
            return Find(parcel)
        }

        override fun newArray(size: Int): Array<Find?> {
            return arrayOfNulls(size)
        }
    }
}