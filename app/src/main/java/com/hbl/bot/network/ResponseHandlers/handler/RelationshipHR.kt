package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class RelationshipHR(callBack: RelationshipCallBack) : BaseRH<RelationshipResponse>() {
    var callback: RelationshipCallBack = callBack
    override fun onSuccess(response: RelationshipResponse?) {
        response?.let { callback.RelationshipSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.RelationshipFailure(it) }
    }
}