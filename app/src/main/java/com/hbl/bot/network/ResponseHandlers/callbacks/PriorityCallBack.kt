package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PriorityCallBack {
    fun PrioritySuccess(response: PriorityResponse)
    fun PriorityFailure(response: BaseResponse)
}