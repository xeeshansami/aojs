package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class MySisRefHR(callBack: MySisRefCallBack) : BaseRH<MYSISREFResponse>() {
    var callback: MySisRefCallBack = callBack
    override fun onSuccess(response: MYSISREFResponse?) {
        response?.let { callback.MySisRefSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.MySisRefFailure(it) }
    }
}