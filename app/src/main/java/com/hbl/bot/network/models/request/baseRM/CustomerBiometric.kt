package com.hbl.bot.network.models.request.baseRM


import android.os.Parcel
import com.google.gson.annotations.SerializedName
import android.os.Parcelable
import com.hbl.bot.network.models.response.baseRM.RESPONSEFINGERINDEX
import java.io.Serializable

class CustomerBiometric() : Serializable, Parcelable {
    @SerializedName("ACCOUNT_BAT_CODE")
    var aCCOUNTBATCODE=""

    @SerializedName("ACCOUNT_BAT_NAME")
    var aCCOUNTBATNAME=""

    @SerializedName("BIOPURPOSE")
    var bIOPURPOSE: List<String?>? = null

    @SerializedName("BIOTRACKING_ID")
    var bIOTRACKINGID=""

    @SerializedName("BRANCH_ADDRESS")
    var bRANCHADDRESS=""

    @SerializedName("BRANCH_CODE")
    var bRANCHCODE=""

    @SerializedName("BRANCH_NAME")
    var bRANCHNAME=""

    @SerializedName("BRANCH_TYPE")
    var bRANCHTYPE=""

    @SerializedName("CHANNEL_ID")
    var cHANNELID=""

    @SerializedName("CITIZEN_NUMBER")
    var cITIZENNUMBER=""

    @SerializedName("CONTACT_NUMBER")
    var cONTACTNUMBER=""

    @SerializedName("DATETIME")
    var dATETIME=""

    @SerializedName("FINGER_INDEX")
    var fINGERINDEX=""

    @SerializedName("MOBILE_NO")
    var mOBILENO=""

    @SerializedName("NADRA_VERISYS_CHECK")
    var nADRAVERISYSCHECK=""

    @SerializedName("OLDER_DAYS")
    var oLDERDAYS=""

    @SerializedName("PROVINCE_CODE")
    var pROVINCECODE=""

    @SerializedName("PROVINCE_DESC")
    var pROVINCEDESC=""

    @SerializedName("RESPONSE_BIRTH_PLACE")
    var rESPONSEBIRTHPLACE=""

    @SerializedName("RESPONSE_CARD_TYPE")
    var rESPONSECARDTYPE=""

    @SerializedName("RESPONSE_CITIZEN_NUMBER")
    var rESPONSECITIZENNUMBER=""

    @SerializedName("RESPONSE_CITIZEN_STATUS")
    var rESPONSECITIZENSTATUS=""

    @SerializedName("RESPONSE_CODE")
    var rESPONSECODE=""

    @SerializedName("RESPONSE_DATE_OF_BIRTH")
    var rESPONSEDATEOFBIRTH=""

    @SerializedName("RESPONSE_EXPIRY_DATE")
    var rESPONSEEXPIRYDATE=""

    @SerializedName("RESPONSE_FATHER_HUSBAND_NAME")
    var rESPONSEFATHERHUSBANDNAME=""

    @SerializedName("RESPONSE_FINGER_INDEX")
    var rESPONSEFINGERINDEX: RESPONSEFINGERINDEX? = null

    @SerializedName("RESPONSE_MESSAGE")
    var rESPONSEMESSAGE=""

    @SerializedName("RESPONSE_NAME")
    var rESPONSENAME=""

    @SerializedName("RESPONSE_PERMANENT_ADDRESS")
    var rESPONSEPERMANENTADDRESS=""

    @SerializedName("RESPONSE_PHOTOGRAPH")
    var rESPONSEPHOTOGRAPH=""

    @SerializedName("RESPONSE_PRESENT_ADDRESS")
    var rESPONSEPRESENTADDRESS=""

    @SerializedName("RESPONSE_SESSION_ID")
    var rESPONSESESSIONID=""

    @SerializedName("ROLE")
    var rOLE: Int? = null

    @SerializedName("SERVICE_TYPE")
    var sERVICETYPE=""

    @SerializedName("SESSION_ID")
    var sESSIONID=""

    @SerializedName("TEMPLATE_TYPE")
    var tEMPLATETYPE=""

    @SerializedName("TRACKING_ID")
    var tRACKINGID=""

    @SerializedName("TRANSACTION_ID")
    var tRANSACTIONID=""

    @SerializedName("URLTYPE")
    var uRLTYPE=""

    @SerializedName("USER_ID")
    var uSERID=""

    @SerializedName("VERISYS_LOCATION")
    var vERISYSLOCATION=""

    constructor(parcel: Parcel) : this() {
        aCCOUNTBATCODE = parcel.readString().toString()
        aCCOUNTBATNAME = parcel.readString().toString()
        bIOPURPOSE = parcel.createStringArrayList()
        bIOTRACKINGID = parcel.readString().toString()
        bRANCHADDRESS = parcel.readString().toString()
        bRANCHCODE = parcel.readString().toString()
        bRANCHNAME = parcel.readString().toString()
        bRANCHTYPE = parcel.readString().toString()
        cHANNELID = parcel.readString().toString()
        cITIZENNUMBER = parcel.readString().toString()
        cONTACTNUMBER = parcel.readString().toString()
        dATETIME = parcel.readString().toString()
        fINGERINDEX = parcel.readString().toString()
        mOBILENO = parcel.readString().toString()
        nADRAVERISYSCHECK = parcel.readString().toString()
        oLDERDAYS = parcel.readString().toString()
        pROVINCECODE = parcel.readString().toString()
        pROVINCEDESC = parcel.readString().toString()
        rESPONSEBIRTHPLACE = parcel.readString().toString()
        rESPONSECARDTYPE = parcel.readString().toString()
        rESPONSECITIZENNUMBER = parcel.readString().toString()
        rESPONSECITIZENSTATUS = parcel.readString().toString()
        rESPONSECODE = parcel.readString().toString()
        rESPONSEDATEOFBIRTH = parcel.readString().toString()
        rESPONSEEXPIRYDATE = parcel.readString().toString()
        rESPONSEFATHERHUSBANDNAME = parcel.readString().toString()
        rESPONSEMESSAGE = parcel.readString().toString()
        rESPONSENAME = parcel.readString().toString()
        rESPONSEPERMANENTADDRESS = parcel.readString().toString()
        rESPONSEPHOTOGRAPH = parcel.readString().toString()
        rESPONSEPRESENTADDRESS = parcel.readString().toString()
        rESPONSESESSIONID = parcel.readString().toString()
        rOLE = parcel.readValue(Int::class.java.classLoader) as? Int
        sERVICETYPE = parcel.readString().toString()
        sESSIONID = parcel.readString().toString()
        tEMPLATETYPE = parcel.readString().toString()
        tRACKINGID = parcel.readString().toString()
        tRANSACTIONID = parcel.readString().toString()
        uRLTYPE = parcel.readString().toString()
        uSERID = parcel.readString().toString()
        vERISYSLOCATION = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(aCCOUNTBATCODE)
        parcel.writeString(aCCOUNTBATNAME)
        parcel.writeStringList(bIOPURPOSE)
        parcel.writeString(bIOTRACKINGID)
        parcel.writeString(bRANCHADDRESS)
        parcel.writeString(bRANCHCODE)
        parcel.writeString(bRANCHNAME)
        parcel.writeString(bRANCHTYPE)
        parcel.writeString(cHANNELID)
        parcel.writeString(cITIZENNUMBER)
        parcel.writeString(cONTACTNUMBER)
        parcel.writeString(dATETIME)
        parcel.writeString(fINGERINDEX)
        parcel.writeString(mOBILENO)
        parcel.writeString(nADRAVERISYSCHECK)
        parcel.writeString(oLDERDAYS)
        parcel.writeString(pROVINCECODE)
        parcel.writeString(pROVINCEDESC)
        parcel.writeString(rESPONSEBIRTHPLACE)
        parcel.writeString(rESPONSECARDTYPE)
        parcel.writeString(rESPONSECITIZENNUMBER)
        parcel.writeString(rESPONSECITIZENSTATUS)
        parcel.writeString(rESPONSECODE)
        parcel.writeString(rESPONSEDATEOFBIRTH)
        parcel.writeString(rESPONSEEXPIRYDATE)
        parcel.writeString(rESPONSEFATHERHUSBANDNAME)
        parcel.writeValue(rESPONSEFINGERINDEX)
        parcel.writeString(rESPONSEMESSAGE)
        parcel.writeString(rESPONSENAME)
        parcel.writeString(rESPONSEPERMANENTADDRESS)
        parcel.writeString(rESPONSEPHOTOGRAPH)
        parcel.writeString(rESPONSEPRESENTADDRESS)
        parcel.writeString(rESPONSESESSIONID)
        parcel.writeValue(rOLE)
        parcel.writeString(sERVICETYPE)
        parcel.writeString(sESSIONID)
        parcel.writeString(tEMPLATETYPE)
        parcel.writeString(tRACKINGID)
        parcel.writeString(tRANSACTIONID)
        parcel.writeString(uRLTYPE)
        parcel.writeString(uSERID)
        parcel.writeString(vERISYSLOCATION)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CustomerBiometric> {
        override fun createFromParcel(parcel: Parcel): CustomerBiometric {
            return CustomerBiometric(parcel)
        }

        override fun newArray(size: Int): Array<CustomerBiometric?> {
            return arrayOfNulls(size)
        }
    }
}