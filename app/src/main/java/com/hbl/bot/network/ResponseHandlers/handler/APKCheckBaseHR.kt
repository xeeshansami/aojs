package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.APKCheckCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.APKCheckResponse

class APKCheckBaseHR(callBack: APKCheckCallBack) : BaseRH<APKCheckResponse>() {
    var callback: APKCheckCallBack = callBack
    override fun onSuccess(response: APKCheckResponse?) {
        response?.let { callback.APKCheckSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.APKCheckFailure(it) }
    }
}