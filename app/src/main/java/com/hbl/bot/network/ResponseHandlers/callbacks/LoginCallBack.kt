package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.LoginResponse

interface LoginCallBack {
    fun LoginSuccess(response: LoginResponse)
    fun LoginFailure(response: BaseResponse)
}