
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KINCUSTCDD {

    @SerializedName("EXPECTED_INWARD_MTHLY_INCOME_PKR_1")
    @Expose
    public String EXPECTED_INWARD_MTHLY_INCOME_PKR_1 = "";
    @SerializedName("COUNTRY_INWARD_REMITT_1")
    @Expose
    public String COUNTRY_INWARD_REMITT_1 = "";
    @SerializedName("DESC_COUNTRY_INWARD_REMITT_1")
    @Expose
    public String DESC_COUNTRY_INWARD_REMITT_1 = "";
    @SerializedName("PURPOSE_INWARD_1")
    @Expose
    public String PURPOSE_INWARD_1 = "";
    @SerializedName("EXPECTED_INWARD_MTHLY_INCOME_PKR_2")
    @Expose
    public String EXPECTED_INWARD_MTHLY_INCOME_PKR_2 = "";
    @SerializedName("COUNTRY_INWARD_REMITT_2")
    @Expose
    public String COUNTRY_INWARD_REMITT_2 = "";
    @SerializedName("DESC_COUNTRY_INWARD_REMITT_2")
    @Expose
    public String DESC_COUNTRY_INWARD_REMITT_2 = "";
    @SerializedName("PURPOSE_INWARD_2")
    @Expose
    public String PURPOSE_INWARD_2 = "";
    @SerializedName("EXPECTED_INWARD_MTHLY_INCOME_PKR_3")
    @Expose
    public String EXPECTED_INWARD_MTHLY_INCOME_PKR_3 = "";
    @SerializedName("COUNTRY_INWARD_REMITT_3")
    @Expose
    public String COUNTRY_INWARD_REMITT_3 = "";
    @SerializedName("DESC_COUNTRY_INWARD_REMITT_3")
    @Expose
    public String DESC_COUNTRY_INWARD_REMITT_3 = "";
    @SerializedName("PURPOSE_INWARD_3")
    @Expose
    public String PURPOSE_INWARD_3 = "";
    @SerializedName("EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1")
    @Expose
    public String EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1 = "";
    @SerializedName("COUNTRY_OUTWARD_REMITT_1")
    @Expose
    public String COUNTRY_OUTWARD_REMITT_1 = "";
    @SerializedName("DESC_COUNTRY_OUTWARD_REMITT_1")
    @Expose
    public String DESC_COUNTRY_OUTWARD_REMITT_1 = "";
    @SerializedName("PURPOSE_OUTWARD_1")
    @Expose
    public String PURPOSE_OUTWARD_1 = "";
    @SerializedName("EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2")
    @Expose
    public String EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2 = "";
    @SerializedName("COUNTRY_OUTWARD_REMITT_2")
    @Expose
    public String COUNTRY_OUTWARD_REMITT_2 = "";
    @SerializedName("DESC_COUNTRY_OUTWARD_REMITT_2")
    @Expose
    public String DESC_COUNTRY_OUTWARD_REMITT_2 = "";
    @SerializedName("PURPOSE_OUTWARD_2")
    @Expose
    public String PURPOSE_OUTWARD_2 = "";
    @SerializedName("EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3")
    @Expose
    public String EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3 = "";
    @SerializedName("COUNTRY_OUTWARD_REMITT_3")
    @Expose
    public String COUNTRY_OUTWARD_REMITT_3 = "";
    @SerializedName("DESC_COUNTRY_OUTWARD_REMITT_3")
    @Expose
    public String DESC_COUNTRY_OUTWARD_REMITT_3 = "";
    @SerializedName("PURPOSE_OUTWARD_3")
    @Expose
    public String PURPOSE_OUTWARD_3 = "";
    @SerializedName("NAME_OF_BANK_1")
    @Expose
    public String NAME_OF_BANK_1 = "";
    @SerializedName("NAME_OF_BRANCH_1")
    @Expose
    public String NAME_OF_BRANCH_1 = "";
    @SerializedName("NAME_OF_BANK_2")
    @Expose
    public String NAME_OF_BANK_2 = "";
    @SerializedName("NAME_OF_BRANCH_2")
    @Expose
    public String NAME_OF_BRANCH_2 = "";
    @SerializedName("NAME_OF_BANK_3")
    @Expose
    public String NAME_OF_BANK_3 = "";
    @SerializedName("NAME_OF_BRANCH_3")
    @Expose
    public String NAME_OF_BRANCH_3 = "";
    @SerializedName("SOURCE_OF_INCOME_LANDLORD")
    @Expose
    public String SOURCE_OF_INCOME_LANDLORD = "";
    @SerializedName("DESC_SOURCE_OF_INCOME_LANDLORD")
    @Expose
    public String DESC_SOURCE_OF_INCOME_LANDLORD = "";
    @SerializedName("OTH_SOURCE_OF_INCOME")
    @Expose
    public String OTH_SOURCE_OF_INCOME = "";
    @SerializedName("SEASONAL_ACITIVITY_IN_ACCT")
    @Expose
    public String SEASONAL_ACITIVITY_IN_ACCT = "";
    @SerializedName("NATR_EXP_MTH_HIGH_TURN_OVER")
    @Expose
    public String NATR_EXP_MTH_HIGH_TURN_OVER = "";
    @SerializedName("AGRI_LAND_PASSBOOK_COPY_OBTN")
    @Expose
    public String AGRI_LAND_PASSBOOK_COPY_OBTN = "";
    @SerializedName("AGRI_SOURCE_OF_INCOME")
    @Expose
    public String AGRI_SOURCE_OF_INCOME = "";
    @SerializedName("AGRI_REASON")
    @Expose
    public String AGRI_REASON = "";
    @SerializedName("DATE_INCP_BUSS_PROF")
    @Expose
    public String DATE_INCP_BUSS_PROF = "";
    @SerializedName("DESC_BUSS_ACTY_PRD_SERV")
    @Expose
    public String DESC_BUSS_ACTY_PRD_SERV = "";
    @SerializedName("DESC_BUSS_GEO_AREA_OPR_SUPP")
    @Expose
    public String DESC_BUSS_GEO_AREA_OPR_SUPP = "";
    @SerializedName("CUST_SRC_OF_INCOME_INDUS")
    @Expose
    public String CUST_SRC_OF_INCOME_INDUS = "";
    @SerializedName("DESC_CUST_SRC_OF_INCOME_INDUS")
    @Expose
    public String DESC_CUST_SRC_OF_INCOME_INDUS = "";
    @SerializedName("CUST_SRC_OF_INDUS_TYPE")
    @Expose
    public String CUST_SRC_OF_INDUS_TYPE = "";
    @SerializedName("DESC_CUST_SRC_OF_INDUS_TYPE")
    @Expose
    public String DESC_CUST_SRC_OF_INDUS_TYPE = "";
    @SerializedName("HOLD_MAIL_INSTR")
    @Expose
    public String HOLD_MAIL_INSTR = "";
    @SerializedName("FOREIGN_ACCT_NO")
    @Expose
    public String FOREIGN_ACCT_NO = "";
    @SerializedName("NAME_COUNTER_PARTY_1")
    @Expose
    public String NAME_COUNTER_PARTY_1 = "";
    @SerializedName("PRD_SERV_PRVD_BUSS_TRAN_1")
    @Expose
    public String PRD_SERV_PRVD_BUSS_TRAN_1 = "";
    @SerializedName("NAME_COUNTER_PARTY_2")
    @Expose
    public String NAME_COUNTER_PARTY_2 = "";
    @SerializedName("PRD_SERV_PRVD_BUSS_TRAN_2")
    @Expose
    public String PRD_SERV_PRVD_BUSS_TRAN_2 = "";
    @SerializedName("NAME_COUNTER_PARTY_3")
    @Expose
    public String NAME_COUNTER_PARTY_3 = "";
    @SerializedName("PRD_SERV_PRVD_BUSS_TRAN_3")
    @Expose
    public String PRD_SERV_PRVD_BUSS_TRAN_3 = "";
    @SerializedName("FILLED_BY")
    @Expose
    public String FILLED_BY = "";
    @SerializedName("APPROVED_BY_AOF")
    @Expose
    public String APPROVED_BY_AOF = "";
    @SerializedName("APPROVED_BY_BR_MGR")
    @Expose
    public String APPROVED_BY_BR_MGR = "";
    @SerializedName("DESC_BREIF_REMARKS")
    @Expose
    public String DESC_BREIF_REMARKS = "";
    @SerializedName("IS_FOREIGN_FUND_IW")
    @Expose
    public String IS_FOREIGN_FUND_IW = "";
    @SerializedName("IS_FOREGIN_FUND_OW")
    @Expose
    public String IS_FOREGIN_FUND_OW = "";
    @SerializedName("IS_OTHER_BANK_ACCOUNT")
    @Expose
    public String IS_OTHER_BANK_ACCOUNT = "";
    @SerializedName("AREA_OF_OPERATION_SUPP")
    @Expose
    public String AREA_OF_OPERATION_SUPP = "";

    public String getEXPECTED_INWARD_MTHLY_INCOME_PKR_1() {
        return EXPECTED_INWARD_MTHLY_INCOME_PKR_1;
    }

    public void setEXPECTED_INWARD_MTHLY_INCOME_PKR_1(String EXPECTED_INWARD_MTHLY_INCOME_PKR_1) {
        this.EXPECTED_INWARD_MTHLY_INCOME_PKR_1 = EXPECTED_INWARD_MTHLY_INCOME_PKR_1;
    }

    public String getCOUNTRY_INWARD_REMITT_1() {
        return COUNTRY_INWARD_REMITT_1;
    }

    public void setCOUNTRY_INWARD_REMITT_1(String COUNTRY_INWARD_REMITT_1) {
        this.COUNTRY_INWARD_REMITT_1 = COUNTRY_INWARD_REMITT_1;
    }

    public String getDESC_COUNTRY_INWARD_REMITT_1() {
        return DESC_COUNTRY_INWARD_REMITT_1;
    }

    public void setDESC_COUNTRY_INWARD_REMITT_1(String DESC_COUNTRY_INWARD_REMITT_1) {
        this.DESC_COUNTRY_INWARD_REMITT_1 = DESC_COUNTRY_INWARD_REMITT_1;
    }

    public String getPURPOSE_INWARD_1() {
        return PURPOSE_INWARD_1;
    }

    public void setPURPOSE_INWARD_1(String PURPOSE_INWARD_1) {
        this.PURPOSE_INWARD_1 = PURPOSE_INWARD_1;
    }

    public String getEXPECTED_INWARD_MTHLY_INCOME_PKR_2() {
        return EXPECTED_INWARD_MTHLY_INCOME_PKR_2;
    }

    public void setEXPECTED_INWARD_MTHLY_INCOME_PKR_2(String EXPECTED_INWARD_MTHLY_INCOME_PKR_2) {
        this.EXPECTED_INWARD_MTHLY_INCOME_PKR_2 = EXPECTED_INWARD_MTHLY_INCOME_PKR_2;
    }

    public String getCOUNTRY_INWARD_REMITT_2() {
        return COUNTRY_INWARD_REMITT_2;
    }

    public void setCOUNTRY_INWARD_REMITT_2(String COUNTRY_INWARD_REMITT_2) {
        this.COUNTRY_INWARD_REMITT_2 = COUNTRY_INWARD_REMITT_2;
    }

    public String getDESC_COUNTRY_INWARD_REMITT_2() {
        return DESC_COUNTRY_INWARD_REMITT_2;
    }

    public void setDESC_COUNTRY_INWARD_REMITT_2(String DESC_COUNTRY_INWARD_REMITT_2) {
        this.DESC_COUNTRY_INWARD_REMITT_2 = DESC_COUNTRY_INWARD_REMITT_2;
    }

    public String getPURPOSE_INWARD_2() {
        return PURPOSE_INWARD_2;
    }

    public void setPURPOSE_INWARD_2(String PURPOSE_INWARD_2) {
        this.PURPOSE_INWARD_2 = PURPOSE_INWARD_2;
    }

    public String getEXPECTED_INWARD_MTHLY_INCOME_PKR_3() {
        return EXPECTED_INWARD_MTHLY_INCOME_PKR_3;
    }

    public void setEXPECTED_INWARD_MTHLY_INCOME_PKR_3(String EXPECTED_INWARD_MTHLY_INCOME_PKR_3) {
        this.EXPECTED_INWARD_MTHLY_INCOME_PKR_3 = EXPECTED_INWARD_MTHLY_INCOME_PKR_3;
    }

    public String getCOUNTRY_INWARD_REMITT_3() {
        return COUNTRY_INWARD_REMITT_3;
    }

    public void setCOUNTRY_INWARD_REMITT_3(String COUNTRY_INWARD_REMITT_3) {
        this.COUNTRY_INWARD_REMITT_3 = COUNTRY_INWARD_REMITT_3;
    }

    public String getDESC_COUNTRY_INWARD_REMITT_3() {
        return DESC_COUNTRY_INWARD_REMITT_3;
    }

    public void setDESC_COUNTRY_INWARD_REMITT_3(String DESC_COUNTRY_INWARD_REMITT_3) {
        this.DESC_COUNTRY_INWARD_REMITT_3 = DESC_COUNTRY_INWARD_REMITT_3;
    }

    public String getPURPOSE_INWARD_3() {
        return PURPOSE_INWARD_3;
    }

    public void setPURPOSE_INWARD_3(String PURPOSE_INWARD_3) {
        this.PURPOSE_INWARD_3 = PURPOSE_INWARD_3;
    }

    public String getEXPECTED_OUTWARD_MTHLY_INCOME_PKR_1() {
        return EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1;
    }

    public void setEXPECTED_OUTWARD_MTHLY_INCOME_PKR_1(String EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1) {
        this.EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1 = EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1;
    }

    public String getCOUNTRY_OUTWARD_REMITT_1() {
        return COUNTRY_OUTWARD_REMITT_1;
    }

    public void setCOUNTRY_OUTWARD_REMITT_1(String COUNTRY_OUTWARD_REMITT_1) {
        this.COUNTRY_OUTWARD_REMITT_1 = COUNTRY_OUTWARD_REMITT_1;
    }

    public String getDESC_COUNTRY_OUTWARD_REMITT_1() {
        return DESC_COUNTRY_OUTWARD_REMITT_1;
    }

    public void setDESC_COUNTRY_OUTWARD_REMITT_1(String DESC_COUNTRY_OUTWARD_REMITT_1) {
        this.DESC_COUNTRY_OUTWARD_REMITT_1 = DESC_COUNTRY_OUTWARD_REMITT_1;
    }

    public String getPURPOSE_OUTWARD_1() {
        return PURPOSE_OUTWARD_1;
    }

    public void setPURPOSE_OUTWARD_1(String PURPOSE_OUTWARD_1) {
        this.PURPOSE_OUTWARD_1 = PURPOSE_OUTWARD_1;
    }

    public String getEXPECTED_OUTWARD_MTHLY_INCOME_PKR_2() {
        return EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2;
    }

    public void setEXPECTED_OUTWARD_MTHLY_INCOME_PKR_2(String EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2) {
        this.EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2 = EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2;
    }

    public String getCOUNTRY_OUTWARD_REMITT_2() {
        return COUNTRY_OUTWARD_REMITT_2;
    }

    public void setCOUNTRY_OUTWARD_REMITT_2(String COUNTRY_OUTWARD_REMITT_2) {
        this.COUNTRY_OUTWARD_REMITT_2 = COUNTRY_OUTWARD_REMITT_2;
    }

    public String getDESC_COUNTRY_OUTWARD_REMITT_2() {
        return DESC_COUNTRY_OUTWARD_REMITT_2;
    }

    public void setDESC_COUNTRY_OUTWARD_REMITT_2(String DESC_COUNTRY_OUTWARD_REMITT_2) {
        this.DESC_COUNTRY_OUTWARD_REMITT_2 = DESC_COUNTRY_OUTWARD_REMITT_2;
    }

    public String getPURPOSE_OUTWARD_2() {
        return PURPOSE_OUTWARD_2;
    }

    public void setPURPOSE_OUTWARD_2(String PURPOSE_OUTWARD_2) {
        this.PURPOSE_OUTWARD_2 = PURPOSE_OUTWARD_2;
    }

    public String getEXPECTED_OUTWARD_MTHLY_INCOME_PKR_3() {
        return EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3;
    }

    public void setEXPECTED_OUTWARD_MTHLY_INCOME_PKR_3(String EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3) {
        this.EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3 = EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3;
    }

    public String getCOUNTRY_OUTWARD_REMITT_3() {
        return COUNTRY_OUTWARD_REMITT_3;
    }

    public void setCOUNTRY_OUTWARD_REMITT_3(String COUNTRY_OUTWARD_REMITT_3) {
        this.COUNTRY_OUTWARD_REMITT_3 = COUNTRY_OUTWARD_REMITT_3;
    }

    public String getDESC_COUNTRY_OUTWARD_REMITT_3() {
        return DESC_COUNTRY_OUTWARD_REMITT_3;
    }

    public void setDESC_COUNTRY_OUTWARD_REMITT_3(String DESC_COUNTRY_OUTWARD_REMITT_3) {
        this.DESC_COUNTRY_OUTWARD_REMITT_3 = DESC_COUNTRY_OUTWARD_REMITT_3;
    }

    public String getPURPOSE_OUTWARD_3() {
        return PURPOSE_OUTWARD_3;
    }

    public void setPURPOSE_OUTWARD_3(String PURPOSE_OUTWARD_3) {
        this.PURPOSE_OUTWARD_3 = PURPOSE_OUTWARD_3;
    }

    public String getNAME_OF_BANK_1() {
        return NAME_OF_BANK_1;
    }

    public void setNAME_OF_BANK_1(String NAME_OF_BANK_1) {
        this.NAME_OF_BANK_1 = NAME_OF_BANK_1;
    }

    public String getNAME_OF_BRANCH_1() {
        return NAME_OF_BRANCH_1;
    }

    public void setNAME_OF_BRANCH_1(String NAME_OF_BRANCH_1) {
        this.NAME_OF_BRANCH_1 = NAME_OF_BRANCH_1;
    }

    public String getNAME_OF_BANK_2() {
        return NAME_OF_BANK_2;
    }

    public void setNAME_OF_BANK_2(String NAME_OF_BANK_2) {
        this.NAME_OF_BANK_2 = NAME_OF_BANK_2;
    }

    public String getNAME_OF_BRANCH_2() {
        return NAME_OF_BRANCH_2;
    }

    public void setNAME_OF_BRANCH_2(String NAME_OF_BRANCH_2) {
        this.NAME_OF_BRANCH_2 = NAME_OF_BRANCH_2;
    }

    public String getNAME_OF_BANK_3() {
        return NAME_OF_BANK_3;
    }

    public void setNAME_OF_BANK_3(String NAME_OF_BANK_3) {
        this.NAME_OF_BANK_3 = NAME_OF_BANK_3;
    }

    public String getNAME_OF_BRANCH_3() {
        return NAME_OF_BRANCH_3;
    }

    public void setNAME_OF_BRANCH_3(String NAME_OF_BRANCH_3) {
        this.NAME_OF_BRANCH_3 = NAME_OF_BRANCH_3;
    }

    public String getSOURCE_OF_INCOME_LANDLORD() {
        return SOURCE_OF_INCOME_LANDLORD;
    }

    public void setSOURCE_OF_INCOME_LANDLORD(String SOURCE_OF_INCOME_LANDLORD) {
        this.SOURCE_OF_INCOME_LANDLORD = SOURCE_OF_INCOME_LANDLORD;
    }

    public String getDESC_SOURCE_OF_INCOME_LANDLORD() {
        return DESC_SOURCE_OF_INCOME_LANDLORD;
    }

    public void setDESC_SOURCE_OF_INCOME_LANDLORD(String DESC_SOURCE_OF_INCOME_LANDLORD) {
        this.DESC_SOURCE_OF_INCOME_LANDLORD = DESC_SOURCE_OF_INCOME_LANDLORD;
    }

    public String getOTH_SOURCE_OF_INCOME() {
        return OTH_SOURCE_OF_INCOME;
    }

    public void setOTH_SOURCE_OF_INCOME(String OTH_SOURCE_OF_INCOME) {
        this.OTH_SOURCE_OF_INCOME = OTH_SOURCE_OF_INCOME;
    }

    public String getSEASONAL_ACITIVITY_IN_ACCT() {
        return SEASONAL_ACITIVITY_IN_ACCT;
    }

    public void setSEASONAL_ACITIVITY_IN_ACCT(String SEASONAL_ACITIVITY_IN_ACCT) {
        this.SEASONAL_ACITIVITY_IN_ACCT = SEASONAL_ACITIVITY_IN_ACCT;
    }

    public String getNATR_EXP_MTH_HIGH_TURN_OVER() {
        return NATR_EXP_MTH_HIGH_TURN_OVER;
    }

    public void setNATR_EXP_MTH_HIGH_TURN_OVER(String NATR_EXP_MTH_HIGH_TURN_OVER) {
        this.NATR_EXP_MTH_HIGH_TURN_OVER = NATR_EXP_MTH_HIGH_TURN_OVER;
    }

    public String getAGRI_LAND_PASSBOOK_COPY_OBTN() {
        return AGRI_LAND_PASSBOOK_COPY_OBTN;
    }

    public void setAGRI_LAND_PASSBOOK_COPY_OBTN(String AGRI_LAND_PASSBOOK_COPY_OBTN) {
        this.AGRI_LAND_PASSBOOK_COPY_OBTN = AGRI_LAND_PASSBOOK_COPY_OBTN;
    }

    public String getAGRI_SOURCE_OF_INCOME() {
        return AGRI_SOURCE_OF_INCOME;
    }

    public void setAGRI_SOURCE_OF_INCOME(String AGRI_SOURCE_OF_INCOME) {
        this.AGRI_SOURCE_OF_INCOME = AGRI_SOURCE_OF_INCOME;
    }

    public String getAGRI_REASON() {
        return AGRI_REASON;
    }

    public void setAGRI_REASON(String AGRI_REASON) {
        this.AGRI_REASON = AGRI_REASON;
    }

    public String getDATE_INCP_BUSS_PROF() {
        return DATE_INCP_BUSS_PROF;
    }

    public void setDATE_INCP_BUSS_PROF(String DATE_INCP_BUSS_PROF) {
        this.DATE_INCP_BUSS_PROF = DATE_INCP_BUSS_PROF;
    }

    public String getDESC_BUSS_ACTY_PRD_SERV() {
        return DESC_BUSS_ACTY_PRD_SERV;
    }

    public void setDESC_BUSS_ACTY_PRD_SERV(String DESC_BUSS_ACTY_PRD_SERV) {
        this.DESC_BUSS_ACTY_PRD_SERV = DESC_BUSS_ACTY_PRD_SERV;
    }

    public String getDESC_BUSS_GEO_AREA_OPR_SUPP() {
        return DESC_BUSS_GEO_AREA_OPR_SUPP;
    }

    public void setDESC_BUSS_GEO_AREA_OPR_SUPP(String DESC_BUSS_GEO_AREA_OPR_SUPP) {
        this.DESC_BUSS_GEO_AREA_OPR_SUPP = DESC_BUSS_GEO_AREA_OPR_SUPP;
    }

    public String getCUST_SRC_OF_INCOME_INDUS() {
        return CUST_SRC_OF_INCOME_INDUS;
    }

    public void setCUST_SRC_OF_INCOME_INDUS(String CUST_SRC_OF_INCOME_INDUS) {
        this.CUST_SRC_OF_INCOME_INDUS = CUST_SRC_OF_INCOME_INDUS;
    }

    public String getDESC_CUST_SRC_OF_INCOME_INDUS() {
        return DESC_CUST_SRC_OF_INCOME_INDUS;
    }

    public void setDESC_CUST_SRC_OF_INCOME_INDUS(String DESC_CUST_SRC_OF_INCOME_INDUS) {
        this.DESC_CUST_SRC_OF_INCOME_INDUS = DESC_CUST_SRC_OF_INCOME_INDUS;
    }

    public String getCUST_SRC_OF_INDUS_TYPE() {
        return CUST_SRC_OF_INDUS_TYPE;
    }

    public void setCUST_SRC_OF_INDUS_TYPE(String CUST_SRC_OF_INDUS_TYPE) {
        this.CUST_SRC_OF_INDUS_TYPE = CUST_SRC_OF_INDUS_TYPE;
    }

    public String getDESC_CUST_SRC_OF_INDUS_TYPE() {
        return DESC_CUST_SRC_OF_INDUS_TYPE;
    }

    public void setDESC_CUST_SRC_OF_INDUS_TYPE(String DESC_CUST_SRC_OF_INDUS_TYPE) {
        this.DESC_CUST_SRC_OF_INDUS_TYPE = DESC_CUST_SRC_OF_INDUS_TYPE;
    }

    public String getHOLD_MAIL_INSTR() {
        return HOLD_MAIL_INSTR;
    }

    public void setHOLD_MAIL_INSTR(String HOLD_MAIL_INSTR) {
        this.HOLD_MAIL_INSTR = HOLD_MAIL_INSTR;
    }

    public String getFOREIGN_ACCT_NO() {
        return FOREIGN_ACCT_NO;
    }

    public void setFOREIGN_ACCT_NO(String FOREIGN_ACCT_NO) {
        this.FOREIGN_ACCT_NO = FOREIGN_ACCT_NO;
    }

    public String getNAME_COUNTER_PARTY_1() {
        return NAME_COUNTER_PARTY_1;
    }

    public void setNAME_COUNTER_PARTY_1(String NAME_COUNTER_PARTY_1) {
        this.NAME_COUNTER_PARTY_1 = NAME_COUNTER_PARTY_1;
    }

    public String getPRD_SERV_PRVD_BUSS_TRAN_1() {
        return PRD_SERV_PRVD_BUSS_TRAN_1;
    }

    public void setPRD_SERV_PRVD_BUSS_TRAN_1(String PRD_SERV_PRVD_BUSS_TRAN_1) {
        this.PRD_SERV_PRVD_BUSS_TRAN_1 = PRD_SERV_PRVD_BUSS_TRAN_1;
    }

    public String getNAME_COUNTER_PARTY_2() {
        return NAME_COUNTER_PARTY_2;
    }

    public void setNAME_COUNTER_PARTY_2(String NAME_COUNTER_PARTY_2) {
        this.NAME_COUNTER_PARTY_2 = NAME_COUNTER_PARTY_2;
    }

    public String getPRD_SERV_PRVD_BUSS_TRAN_2() {
        return PRD_SERV_PRVD_BUSS_TRAN_2;
    }

    public void setPRD_SERV_PRVD_BUSS_TRAN_2(String PRD_SERV_PRVD_BUSS_TRAN_2) {
        this.PRD_SERV_PRVD_BUSS_TRAN_2 = PRD_SERV_PRVD_BUSS_TRAN_2;
    }

    public String getNAME_COUNTER_PARTY_3() {
        return NAME_COUNTER_PARTY_3;
    }

    public void setNAME_COUNTER_PARTY_3(String NAME_COUNTER_PARTY_3) {
        this.NAME_COUNTER_PARTY_3 = NAME_COUNTER_PARTY_3;
    }

    public String getPRD_SERV_PRVD_BUSS_TRAN_3() {
        return PRD_SERV_PRVD_BUSS_TRAN_3;
    }

    public void setPRD_SERV_PRVD_BUSS_TRAN_3(String PRD_SERV_PRVD_BUSS_TRAN_3) {
        this.PRD_SERV_PRVD_BUSS_TRAN_3 = PRD_SERV_PRVD_BUSS_TRAN_3;
    }

    public String getFILLED_BY() {
        return FILLED_BY;
    }

    public void setFILLED_BY(String FILLED_BY) {
        this.FILLED_BY = FILLED_BY;
    }

    public String getAPPROVED_BY_AOF() {
        return APPROVED_BY_AOF;
    }

    public void setAPPROVED_BY_AOF(String APPROVED_BY_AOF) {
        this.APPROVED_BY_AOF = APPROVED_BY_AOF;
    }

    public String getAPPROVED_BY_BR_MGR() {
        return APPROVED_BY_BR_MGR;
    }

    public void setAPPROVED_BY_BR_MGR(String APPROVED_BY_BR_MGR) {
        this.APPROVED_BY_BR_MGR = APPROVED_BY_BR_MGR;
    }

    public String getDESC_BREIF_REMARKS() {
        return DESC_BREIF_REMARKS;
    }

    public void setDESC_BREIF_REMARKS(String DESC_BREIF_REMARKS) {
        this.DESC_BREIF_REMARKS = DESC_BREIF_REMARKS;
    }

    public String getIS_FOREIGN_FUND_IW() {
        return IS_FOREIGN_FUND_IW;
    }

    public void setIS_FOREIGN_FUND_IW(String IS_FOREIGN_FUND_IW) {
        this.IS_FOREIGN_FUND_IW = IS_FOREIGN_FUND_IW;
    }

    public String getIS_FOREGIN_FUND_OW() {
        return IS_FOREGIN_FUND_OW;
    }

    public void setIS_FOREGIN_FUND_OW(String IS_FOREGIN_FUND_OW) {
        this.IS_FOREGIN_FUND_OW = IS_FOREGIN_FUND_OW;
    }

    public String getIS_OTHER_BANK_ACCOUNT() {
        return IS_OTHER_BANK_ACCOUNT;
    }

    public void setIS_OTHER_BANK_ACCOUNT(String IS_OTHER_BANK_ACCOUNT) {
        this.IS_OTHER_BANK_ACCOUNT = IS_OTHER_BANK_ACCOUNT;
    }

    public String getAREA_OF_OPERATION_SUPP() {
        return AREA_OF_OPERATION_SUPP;
    }

    public void setAREA_OF_OPERATION_SUPP(String AREA_OF_OPERATION_SUPP) {
        this.AREA_OF_OPERATION_SUPP = AREA_OF_OPERATION_SUPP;
    }
}
