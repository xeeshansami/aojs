package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class DepositType() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    var _id: String? = null

    @SerializedName("DEP_CODE")
    @Expose
    var DEP_CODE: String? = null

    @SerializedName("DEP_DESC")
    @Expose
    var DEP_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<DepositType> {
        override fun createFromParcel(parcel: Parcel): DepositType {
            return DepositType(parcel)
        }

        override fun newArray(size: Int): Array<DepositType?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return DEP_DESC.toString()
    }
}