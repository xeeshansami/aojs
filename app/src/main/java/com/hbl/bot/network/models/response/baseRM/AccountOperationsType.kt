package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class AccountOperationsType() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("ACCT_OPER_TYPE_CODE")
    @Expose
    var ACCT_OPER_TYPE_CODE: String? = null

    @SerializedName("ACCT_OPER_TYPE_DESC")
    @Expose
    var ACCT_OPER_TYPE_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<AccountOperationsType> {
        override fun createFromParcel(parcel: Parcel): AccountOperationsType {
            return AccountOperationsType(parcel)
        }

        override fun newArray(size: Int): Array<AccountOperationsType?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return ACCT_OPER_TYPE_DESC.toString()
    }
}