package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class ProfessionHR(callBack: ProfessionCallBack) : BaseRH<ProfessionResponse>() {
    var callback: ProfessionCallBack = callBack
    override fun onSuccess(response: ProfessionResponse?) {
        response?.let { callback.ProfessionSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.ProfessionFailure(it) }
    }
}