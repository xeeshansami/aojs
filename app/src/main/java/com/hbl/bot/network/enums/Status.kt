package com.hbl.bot.network.enums

enum class Status {
    SUCCESS, ERROR, LOADING
}