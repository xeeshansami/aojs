package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface ProfessionCallBack {
    fun ProfessionSuccess(response: ProfessionResponse)
    fun ProfessionFailure(response: BaseResponse)
}