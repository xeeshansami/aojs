package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class SourceOffundHR(callBack: SourceOffundCallBack) : BaseRH<SourceOffundResponse>() {
    var callback: SourceOffundCallBack = callBack
    override fun onSuccess(response: SourceOffundResponse?) {
        response?.let { callback.SourceOffundSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.SourceOffundFailure(it) }
    }
}