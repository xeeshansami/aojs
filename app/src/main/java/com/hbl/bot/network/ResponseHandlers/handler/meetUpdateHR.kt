package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class meetUpdateHR(callBack: meetUpdateCallBack) : BaseRH<UpdateMeetResponse>() {
    var callback: meetUpdateCallBack = callBack
    override fun onSuccess(response: UpdateMeetResponse?) {
        response?.let { callback.meetUpdateSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.meetUpdateFailure(it) }
    }
}