package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class ETBNTB() : Parcelable {
    var ATA: String? = null
    var CIFNO: String? = null
    var CIFSTATUS: String? = null
    var CUSTMOBILE: String? = null
    var CUSTNAME: String? = null
    var CUSTSEGMENT: String? = null
    var CUSTTYPE: String? = null
    var ETBNTBFLAG: String? = null
    var RESPCODE: String? = null
    var RESPDESC: String? = null
    var REMEDIATE: String? = null

    constructor(parcel: Parcel) : this() {
        ATA = parcel.readString()
        CIFNO = parcel.readString()
        CIFSTATUS = parcel.readString()
        CUSTMOBILE = parcel.readString()
        CUSTNAME = parcel.readString()
        CUSTSEGMENT = parcel.readString()
        CUSTTYPE = parcel.readString()
        ETBNTBFLAG = parcel.readString()
        RESPCODE = parcel.readString()
        RESPDESC = parcel.readString()
        REMEDIATE = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ATA)
        parcel.writeString(CIFNO)
        parcel.writeString(CIFSTATUS)
        parcel.writeString(CUSTMOBILE)
        parcel.writeString(CUSTNAME)
        parcel.writeString(CUSTSEGMENT)
        parcel.writeString(CUSTTYPE)
        parcel.writeString(ETBNTBFLAG)
        parcel.writeString(RESPCODE)
        parcel.writeString(RESPDESC)
        parcel.writeString(REMEDIATE)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<ETBNTB> {
        override fun createFromParcel(parcel: Parcel): ETBNTB {
            return ETBNTB(parcel)
        }

        override fun newArray(size: Int): Array<ETBNTB?> {
            return arrayOfNulls(size)
        }
    }
}