package com.hbl.bot.network.models.response.base

import com.hbl.bot.network.models.response.baseRM.GenerateCIFAccountData

data class GenerateCIFAccountResponse(
    val `data`: List<GenerateCIFAccountData>,
    val message: String,
    val status: String
)