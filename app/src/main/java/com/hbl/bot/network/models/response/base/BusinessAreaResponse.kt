package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.hbl.bot.network.models.response.baseRM.BusinessArea

class BusinessAreaResponse(
) : Parcelable {
    var data = arrayListOf<BusinessArea>()
    var message = ""
    var status = ""

    constructor(parcel: Parcel) : this() {
        message = parcel.readString().toString()
        status = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(data)
        parcel.writeString(message)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BusinessAreaResponse> {
        override fun createFromParcel(parcel: Parcel): BusinessAreaResponse {
            return BusinessAreaResponse(parcel)
        }

        override fun newArray(size: Int): Array<BusinessAreaResponse?> {
            return arrayOfNulls(size)
        }
    }
}