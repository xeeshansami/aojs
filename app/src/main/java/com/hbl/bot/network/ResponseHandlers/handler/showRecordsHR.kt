package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class showRecordsHR(callBack: showRecordsCallBack) : BaseRH<showRecordsResponse>() {
    var callback: showRecordsCallBack = callBack
    override fun onSuccess(response: showRecordsResponse?) {
        response?.let { callback.showRecordsSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.showRecordsFailure(it) }
    }
}