package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class BranchCodeRequest() : Parcelable {
    @SerializedName("find")
    var find: Find = Find()

    @SerializedName("identifier")
    var identifier: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BranchCodeRequest> {
        override fun createFromParcel(parcel: Parcel): BranchCodeRequest {
            return BranchCodeRequest(parcel)
        }

        override fun newArray(size: Int): Array<BranchCodeRequest?> {
            return arrayOfNulls(size)
        }
    }
}