package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PrintDocumentCallBack  {
    fun PrintDocumentSuccess(response: PrintDocumentResponse)
    fun PrintDocumentFailure(response: BaseResponse)
}