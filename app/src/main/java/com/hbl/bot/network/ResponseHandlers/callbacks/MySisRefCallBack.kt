package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface MySisRefCallBack {
    fun MySisRefSuccess(response: MYSISREFResponse)
    fun MySisRefFailure(response: BaseResponse)
}