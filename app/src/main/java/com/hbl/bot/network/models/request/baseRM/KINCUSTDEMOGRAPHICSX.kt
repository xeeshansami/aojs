package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class KINCUSTDEMOGRAPHICSX() : Parcelable {
    var AGRI_LAND_PASSBOOK_COPY_OBTN = ""
    var CUSTTYPEDESC = ""
    var CUST_TYPE = ""
    var CUST_TYPE_CIF = ""
    var DESIGNATION = ""
    var EXPMODECRTRANSDESC = ""
    var EXP_CR_TURNOVER = ""
    var EXP_DB_TURNOVER = ""
    var EXP_MODE_CR_TRANS = ""
    var EXP_MON_INCOME = ""
    var FIN_INFO_BENF_OWNER = ""
    var MODE_OF_TRANSACTION: List<Any>? = ArrayList<String>()
    var NAME_OF_COMPANY = ""
    var NATR_EXP_MTH_HIGH_TURN_OVER = ""
    var NATUREBUSINESSDESC = ""
    var NATURE_BUSINESS = ""
    var NO_OF_CR_TRANS = ""
    var NO_OF_DB_TRANS = ""
    var OTHERSOURCEOFINCOMEDESC = ""
    var OTHEXPMODECRTRANSDESC: Any? = ArrayList<String>()
    var OTHNATBUSINESSDESC = ""
    var OTHSRCOFWEALTHDESC = ""
    var PROFESSION = ""
    var PROFESSIONDESC = ""
    var PURP_OF_CIF_CODE = ""
    var PURP_OF_CIF_DESC = ""
    var P_NUMBER = ""
    var SEASONAL_ACITIVITY_IN_ACCT = ""
    var SOURCEOFINCOMEDESC = ""
    var SOURCEOFWEALTHDESC = ""
    var SOURCE_OF_INCOME = ""
    var SOURCE_OF_WEALTH = ""

    constructor(parcel: Parcel) : this() {
        AGRI_LAND_PASSBOOK_COPY_OBTN = parcel.readString().toString()
        CUSTTYPEDESC = parcel.readString().toString()
        CUST_TYPE = parcel.readString().toString()
        CUST_TYPE_CIF = parcel.readString().toString()
        DESIGNATION = parcel.readString().toString()
        EXPMODECRTRANSDESC = parcel.readString().toString()
        EXP_CR_TURNOVER = parcel.readString().toString()
        EXP_DB_TURNOVER = parcel.readString().toString()
        EXP_MODE_CR_TRANS = parcel.readString().toString()
        EXP_MON_INCOME = parcel.readString().toString()
        FIN_INFO_BENF_OWNER = parcel.readString().toString()
        NAME_OF_COMPANY = parcel.readString().toString()
        NATR_EXP_MTH_HIGH_TURN_OVER = parcel.readString().toString()
        NATUREBUSINESSDESC = parcel.readString().toString()
        NATURE_BUSINESS = parcel.readString().toString()
        NO_OF_CR_TRANS = parcel.readString().toString()
        NO_OF_DB_TRANS = parcel.readString().toString()
        OTHERSOURCEOFINCOMEDESC = parcel.readString().toString()
        OTHNATBUSINESSDESC = parcel.readString().toString()
        OTHSRCOFWEALTHDESC = parcel.readString().toString()
        PROFESSION = parcel.readString().toString()
        PROFESSIONDESC = parcel.readString().toString()
        PURP_OF_CIF_CODE = parcel.readString().toString()
        PURP_OF_CIF_DESC = parcel.readString().toString()
        P_NUMBER = parcel.readString().toString()
        SEASONAL_ACITIVITY_IN_ACCT = parcel.readString().toString()
        SOURCEOFINCOMEDESC = parcel.readString().toString()
        SOURCEOFWEALTHDESC = parcel.readString().toString()
        SOURCE_OF_INCOME = parcel.readString().toString()
        SOURCE_OF_WEALTH = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(AGRI_LAND_PASSBOOK_COPY_OBTN)
        parcel.writeString(CUSTTYPEDESC)
        parcel.writeString(CUST_TYPE)
        parcel.writeString(CUST_TYPE_CIF)
        parcel.writeString(DESIGNATION)
        parcel.writeString(EXPMODECRTRANSDESC)
        parcel.writeString(EXP_CR_TURNOVER)
        parcel.writeString(EXP_DB_TURNOVER)
        parcel.writeString(EXP_MODE_CR_TRANS)
        parcel.writeString(EXP_MON_INCOME)
        parcel.writeString(FIN_INFO_BENF_OWNER)
        parcel.writeString(NAME_OF_COMPANY)
        parcel.writeString(NATR_EXP_MTH_HIGH_TURN_OVER)
        parcel.writeString(NATUREBUSINESSDESC)
        parcel.writeString(NATURE_BUSINESS)
        parcel.writeString(NO_OF_CR_TRANS)
        parcel.writeString(NO_OF_DB_TRANS)
        parcel.writeString(OTHERSOURCEOFINCOMEDESC)
        parcel.writeString(OTHNATBUSINESSDESC)
        parcel.writeString(OTHSRCOFWEALTHDESC)
        parcel.writeString(PROFESSION)
        parcel.writeString(PROFESSIONDESC)
        parcel.writeString(PURP_OF_CIF_CODE)
        parcel.writeString(PURP_OF_CIF_DESC)
        parcel.writeString(P_NUMBER)
        parcel.writeString(SEASONAL_ACITIVITY_IN_ACCT)
        parcel.writeString(SOURCEOFINCOMEDESC)
        parcel.writeString(SOURCEOFWEALTHDESC)
        parcel.writeString(SOURCE_OF_INCOME)
        parcel.writeString(SOURCE_OF_WEALTH)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KINCUSTDEMOGRAPHICSX> {
        override fun createFromParcel(parcel: Parcel): KINCUSTDEMOGRAPHICSX {
            return KINCUSTDEMOGRAPHICSX(parcel)
        }

        override fun newArray(size: Int): Array<KINCUSTDEMOGRAPHICSX?> {
            return arrayOfNulls(size)
        }
    }
}