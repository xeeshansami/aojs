package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface CurrencyCallBack {
    fun CurrencySuccess(response: CurrencyResponse)
    fun CurrencyFailure(response: BaseResponse)
}