package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class SourceOfIncomeLandLordHR(callBack: SourceOfincomeLandLordCallBack) :
    BaseRH<SourceOffundIncomeResponse>() {
    var callback: SourceOfincomeLandLordCallBack = callBack
    override fun onSuccess(response: SourceOffundIncomeResponse?) {
        response?.let { callback.SourceOfincomeLandLordSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.SourceOfincomeLandLordFailure(it) }
    }
}