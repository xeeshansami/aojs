
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CUSTCONTACT {

    @SerializedName("PREFERRED_MAIL_ADDR")
    @Expose
    public String pREFERREDMAILADDR;
    @SerializedName("PREFERREDMAILADDRDESC")
    @Expose
    public String pREFERREDMAILADDRDESC;
    @SerializedName("RES_LANDLINE")
    @Expose
    public String rESLANDLINE;
    @SerializedName("COUNTRY_DIAL_CODE")
    @Expose
    public String cOUNTRYDIALCODE;
    @SerializedName("MOBILE_NO")
    @Expose
    public String mOBILENO;
    @SerializedName("OFFICE_NO")
    @Expose
    public String oFFICENO;
    @SerializedName("EMAIL_ADDRESS")
    @Expose
    public String eMAILADDRESS;
    @SerializedName("MOBILE_OPERATOR_CODE")
    @Expose
    public String mOBILEOPERATORCODE;
    @SerializedName("IS_US_Dail_Code")
    @Expose
    public Boolean iSUSDailCode;

    public String getPREFERREDMAILADDR() {
        return pREFERREDMAILADDR;
    }

    public void setPREFERREDMAILADDR(String pREFERREDMAILADDR) {
        this.pREFERREDMAILADDR = pREFERREDMAILADDR;
    }

    public String getPREFERREDMAILADDRDESC() {
        return pREFERREDMAILADDRDESC;
    }

    public void setPREFERREDMAILADDRDESC(String pREFERREDMAILADDRDESC) {
        this.pREFERREDMAILADDRDESC = pREFERREDMAILADDRDESC;
    }

    public String getRESLANDLINE() {
        return rESLANDLINE;
    }

    public void setRESLANDLINE(String rESLANDLINE) {
        this.rESLANDLINE = rESLANDLINE;
    }

    public String getCOUNTRYDIALCODE() {
        return cOUNTRYDIALCODE;
    }

    public void setCOUNTRYDIALCODE(String cOUNTRYDIALCODE) {
        this.cOUNTRYDIALCODE = cOUNTRYDIALCODE;
    }

    public String getMOBILENO() {
        return mOBILENO;
    }

    public void setMOBILENO(String mOBILENO) {
        this.mOBILENO = mOBILENO;
    }

    public String getOFFICENO() {
        return oFFICENO;
    }

    public void setOFFICENO(String oFFICENO) {
        this.oFFICENO = oFFICENO;
    }

    public String getEMAILADDRESS() {
        return eMAILADDRESS;
    }

    public void setEMAILADDRESS(String eMAILADDRESS) {
        this.eMAILADDRESS = eMAILADDRESS;
    }

    public String getMOBILEOPERATORCODE() {
        return mOBILEOPERATORCODE;
    }

    public void setMOBILEOPERATORCODE(String mOBILEOPERATORCODE) {
        this.mOBILEOPERATORCODE = mOBILEOPERATORCODE;
    }

    public Boolean getISUSDailCode() {
        return iSUSDailCode;
    }

    public void setISUSDailCode(Boolean iSUSDailCode) {
        this.iSUSDailCode = iSUSDailCode;
    }

}
