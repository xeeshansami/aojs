package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface CustomerRiskRatingCallBack {
    fun CustomerRiskRatingSuccess(response: RiskRatingResponse)
    fun CustomerRiskRatingFailure(response: BaseResponse)
}