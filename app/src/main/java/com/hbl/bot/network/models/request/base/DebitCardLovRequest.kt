package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class DebitCardLovRequest() : Parcelable {
    @SerializedName("find")
    var find: DebitCardFind = DebitCardFind()

    @SerializedName("identifier")
    var identifier: String? = null

    @SerializedName("ACCT_TYPE")
    var ACCT_TYPE: String? = null

    @SerializedName("ACCT_CCY")
    var ACCT_CCY: String? = null

    @SerializedName("CUST_TYPE")
    var CUST_TYPE: String? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DebitCardLovRequest> {
        override fun createFromParcel(parcel: Parcel): DebitCardLovRequest {
            return DebitCardLovRequest(parcel)
        }

        override fun newArray(size: Int): Array<DebitCardLovRequest?> {
            return arrayOfNulls(size)
        }
    }
}