package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.SBPCodes
import java.io.Serializable


class SBPCodesResponse() : Serializable, Parcelable {
    @SerializedName("status")
    @Expose
    private val status: String? = null

    @SerializedName("message")
    @Expose
    private val message: String? = null

    @SerializedName("data")
    @Expose
    val data: ArrayList<SBPCodes>? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SBPCodesResponse> {
        override fun createFromParcel(parcel: Parcel): SBPCodesResponse {
            return SBPCodesResponse(parcel)
        }

        override fun newArray(size: Int): Array<SBPCodesResponse?> {
            return arrayOfNulls(size)
        }
    }
}