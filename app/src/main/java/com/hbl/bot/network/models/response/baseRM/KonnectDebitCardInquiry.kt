package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class KonnectDebitCardInquiry() : Parcelable {
    var RESPONSE_CODE: String? = null
    var RESPONSE_MESSAGE: String? = null
    var TRAN_ID: String? = null
    var ACCOUNT_TITLE: String? = ""
    var ACCOUNT_STATUS: String? = ""
    var ACCOUNT_BALANCE: String? = ""
    var ADDRESS: String? = ""
    var OTP: String? = ""

    constructor(parcel: Parcel) : this() {
        RESPONSE_CODE = parcel.readString()
        RESPONSE_MESSAGE = parcel.readString()
        TRAN_ID = parcel.readString()
        ACCOUNT_TITLE = parcel.readString()
        ACCOUNT_STATUS = parcel.readString()
        ACCOUNT_BALANCE = parcel.readString()
        ADDRESS = parcel.readString()
        OTP = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(RESPONSE_CODE)
        parcel.writeString(RESPONSE_MESSAGE)
        parcel.writeString(TRAN_ID)
        parcel.writeString(ACCOUNT_TITLE)
        parcel.writeString(ACCOUNT_STATUS)
        parcel.writeString(ACCOUNT_BALANCE)
        parcel.writeString(ADDRESS)
        parcel.writeString(OTP)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<KonnectDebitCardInquiry> {
        override fun createFromParcel(parcel: Parcel): KonnectDebitCardInquiry {
            return KonnectDebitCardInquiry(parcel)
        }

        override fun newArray(size: Int): Array<KonnectDebitCardInquiry?> {
            return arrayOfNulls(size)
        }
    }
}