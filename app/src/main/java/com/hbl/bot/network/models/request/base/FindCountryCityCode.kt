package com.hbl.bot.network.models.request.base

class FindCountryCityCode() {
    var COUNTRY_DIAL_CODE: String? = null
    var CITY_DIAL_CODE: String? = null
}
