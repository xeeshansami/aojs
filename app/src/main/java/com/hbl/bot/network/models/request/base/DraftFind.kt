package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class DraftFind(
) : Parcelable {
    @SerializedName("TRACKING_ID")
    var TRACKING_ID: String = ""

    constructor(parcel: Parcel) : this() {
        TRACKING_ID = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(TRACKING_ID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DraftFind> {
        override fun createFromParcel(parcel: Parcel): DraftFind {
            return DraftFind(parcel)
        }

        override fun newArray(size: Int): Array<DraftFind?> {
            return arrayOfNulls(size)
        }
    }
}