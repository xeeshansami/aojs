package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class CUSTCDDX(
) : Parcelable {
    var AGRI_LAND_PASSBOOK_COPY_OBTN: String? = null
    var AGRI_REASON: String? = null
    var AGRI_SOURCE_OF_INCOME: String? = null
    var APPROVED_BY_AOF: String? = null
    var APPROVED_BY_BR_MGR: String? = null
    var AREA_OF_OPERATION_SUPP: String? = null
    var COUNTRY_INWARD_REMITT_1: String? = null
    var COUNTRY_INWARD_REMITT_2: String? = null
    var COUNTRY_INWARD_REMITT_3: String? = null
    var COUNTRY_OUTWARD_REMITT_1: String? = null
    var COUNTRY_OUTWARD_REMITT_2: String? = null
    var COUNTRY_OUTWARD_REMITT_3: String? = null
    var CUST_SRC_OF_INCOME_INDUS: String? = null
    var CUST_SRC_OF_INDUS_TYPE: String? = null
    var DATE_INCP_BUSS_PROF: String? = null
    var DESC_BREIF_REMARKS: String? = null
    var DESC_BUSS_ACTY_PRD_SERV: String? = null
    var DESC_BUSS_GEO_AREA_OPR_SUPP: String? = null
    var DESC_COUNTRY_INWARD_REMITT_1: String? = null
    var DESC_COUNTRY_INWARD_REMITT_2: String? = null
    var DESC_COUNTRY_INWARD_REMITT_3: String? = null
    var DESC_COUNTRY_OUTWARD_REMITT_1: String? = null
    var DESC_COUNTRY_OUTWARD_REMITT_2: String? = null
    var DESC_COUNTRY_OUTWARD_REMITT_3: String? = null
    var DESC_CUST_SRC_OF_INCOME_INDUS: String? = null
    var DESC_CUST_SRC_OF_INDUS_TYPE: String? = null
    var DESC_SOURCE_OF_INCOME_LANDLORD: String? = null
    var EXPECTED_INWARD_MTHLY_INCOME_PKR_1: String? = null
    var EXPECTED_INWARD_MTHLY_INCOME_PKR_2: String? = null
    var EXPECTED_INWARD_MTHLY_INCOME_PKR_3: String? = null
    var EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1: String? = null
    var EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2: String? = null
    var EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3: String? = null
    var FILLED_BY: String? = null
    var FOREIGN_ACCT_NO: String? = null
    var HOLD_MAIL_INSTR: String? = null
    var IS_FOREGIN_FUND_OW: String? = null
    var IS_FOREIGN_FUND_IW: String? = null
    var IS_OTHER_BANK_ACCOUNT: String? = null
    var NAME_COUNTER_PARTY_1: String? = null
    var NAME_COUNTER_PARTY_2: String? = null
    var NAME_COUNTER_PARTY_3: String? = null
    var NAME_OF_BANK_1: String? = null
    var NAME_OF_BANK_2: String? = null
    var NAME_OF_BANK_3: String? = null
    var NAME_OF_BRANCH_1: String? = null
    var NAME_OF_BRANCH_2: String? = null
    var NAME_OF_BRANCH_3: String? = null
    var NATR_EXP_MTH_HIGH_TURN_OVER: String? = null
    var OTH_SOURCE_OF_INCOME: String? = null
    var PRD_SERV_PRVD_BUSS_TRAN_1: String? = null
    var PRD_SERV_PRVD_BUSS_TRAN_2: String? = null
    var PRD_SERV_PRVD_BUSS_TRAN_3: String? = null
    var PURPOSE_INWARD_1: String? = null
    var PURPOSE_INWARD_2: String? = null
    var PURPOSE_INWARD_3: String? = null
    var PURPOSE_OUTWARD_1: String? = null
    var PURPOSE_OUTWARD_2: String? = null
    var PURPOSE_OUTWARD_3: String? = null
    var SEASONAL_ACITIVITY_IN_ACCT: String? = null
    var SOURCE_OF_INCOME_LANDLORD: String? = null

    constructor(parcel: Parcel) : this() {
        AGRI_LAND_PASSBOOK_COPY_OBTN = parcel.readString()
        AGRI_REASON = parcel.readString()
        AGRI_SOURCE_OF_INCOME = parcel.readString()
        APPROVED_BY_AOF = parcel.readString()
        APPROVED_BY_BR_MGR = parcel.readString()
        AREA_OF_OPERATION_SUPP = parcel.readString()
        COUNTRY_INWARD_REMITT_1 = parcel.readString()
        COUNTRY_INWARD_REMITT_2 = parcel.readString()
        COUNTRY_INWARD_REMITT_3 = parcel.readString()
        COUNTRY_OUTWARD_REMITT_1 = parcel.readString()
        COUNTRY_OUTWARD_REMITT_2 = parcel.readString()
        COUNTRY_OUTWARD_REMITT_3 = parcel.readString()
        CUST_SRC_OF_INCOME_INDUS = parcel.readString()
        CUST_SRC_OF_INDUS_TYPE = parcel.readString()
        DATE_INCP_BUSS_PROF = parcel.readString()
        DESC_BREIF_REMARKS = parcel.readString()
        DESC_BUSS_ACTY_PRD_SERV = parcel.readString()
        DESC_BUSS_GEO_AREA_OPR_SUPP = parcel.readString()
        DESC_COUNTRY_INWARD_REMITT_1 = parcel.readString()
        DESC_COUNTRY_INWARD_REMITT_2 = parcel.readString()
        DESC_COUNTRY_INWARD_REMITT_3 = parcel.readString()
        DESC_COUNTRY_OUTWARD_REMITT_1 = parcel.readString()
        DESC_COUNTRY_OUTWARD_REMITT_2 = parcel.readString()
        DESC_COUNTRY_OUTWARD_REMITT_3 = parcel.readString()
        DESC_CUST_SRC_OF_INCOME_INDUS = parcel.readString()
        DESC_CUST_SRC_OF_INDUS_TYPE = parcel.readString()
        DESC_SOURCE_OF_INCOME_LANDLORD = parcel.readString()
        EXPECTED_INWARD_MTHLY_INCOME_PKR_1 = parcel.readString()
        EXPECTED_INWARD_MTHLY_INCOME_PKR_2 = parcel.readString()
        EXPECTED_INWARD_MTHLY_INCOME_PKR_3 = parcel.readString()
        EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1 = parcel.readString()
        EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2 = parcel.readString()
        EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3 = parcel.readString()
        FILLED_BY = parcel.readString()
        FOREIGN_ACCT_NO = parcel.readString()
        HOLD_MAIL_INSTR = parcel.readString()
        IS_FOREGIN_FUND_OW = parcel.readString()
        IS_FOREIGN_FUND_IW = parcel.readString()
        IS_OTHER_BANK_ACCOUNT = parcel.readString()
        NAME_COUNTER_PARTY_1 = parcel.readString()
        NAME_COUNTER_PARTY_2 = parcel.readString()
        NAME_COUNTER_PARTY_3 = parcel.readString()
        NAME_OF_BANK_1 = parcel.readString()
        NAME_OF_BANK_2 = parcel.readString()
        NAME_OF_BANK_3 = parcel.readString()
        NAME_OF_BRANCH_1 = parcel.readString()
        NAME_OF_BRANCH_2 = parcel.readString()
        NAME_OF_BRANCH_3 = parcel.readString()
        NATR_EXP_MTH_HIGH_TURN_OVER = parcel.readString()
        OTH_SOURCE_OF_INCOME = parcel.readString()
        PRD_SERV_PRVD_BUSS_TRAN_1 = parcel.readString()
        PRD_SERV_PRVD_BUSS_TRAN_2 = parcel.readString()
        PRD_SERV_PRVD_BUSS_TRAN_3 = parcel.readString()
        PURPOSE_INWARD_1 = parcel.readString()
        PURPOSE_INWARD_2 = parcel.readString()
        PURPOSE_INWARD_3 = parcel.readString()
        PURPOSE_OUTWARD_1 = parcel.readString()
        PURPOSE_OUTWARD_2 = parcel.readString()
        PURPOSE_OUTWARD_3 = parcel.readString()
        SEASONAL_ACITIVITY_IN_ACCT = parcel.readString()
        SOURCE_OF_INCOME_LANDLORD = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(AGRI_LAND_PASSBOOK_COPY_OBTN)
        parcel.writeString(AGRI_REASON)
        parcel.writeString(AGRI_SOURCE_OF_INCOME)
        parcel.writeString(APPROVED_BY_AOF)
        parcel.writeString(APPROVED_BY_BR_MGR)
        parcel.writeString(AREA_OF_OPERATION_SUPP)
        parcel.writeString(COUNTRY_INWARD_REMITT_1)
        parcel.writeString(COUNTRY_INWARD_REMITT_2)
        parcel.writeString(COUNTRY_INWARD_REMITT_3)
        parcel.writeString(COUNTRY_OUTWARD_REMITT_1)
        parcel.writeString(COUNTRY_OUTWARD_REMITT_2)
        parcel.writeString(COUNTRY_OUTWARD_REMITT_3)
        parcel.writeString(CUST_SRC_OF_INCOME_INDUS)
        parcel.writeString(CUST_SRC_OF_INDUS_TYPE)
        parcel.writeString(DATE_INCP_BUSS_PROF)
        parcel.writeString(DESC_BREIF_REMARKS)
        parcel.writeString(DESC_BUSS_ACTY_PRD_SERV)
        parcel.writeString(DESC_BUSS_GEO_AREA_OPR_SUPP)
        parcel.writeString(DESC_COUNTRY_INWARD_REMITT_1)
        parcel.writeString(DESC_COUNTRY_INWARD_REMITT_2)
        parcel.writeString(DESC_COUNTRY_INWARD_REMITT_3)
        parcel.writeString(DESC_COUNTRY_OUTWARD_REMITT_1)
        parcel.writeString(DESC_COUNTRY_OUTWARD_REMITT_2)
        parcel.writeString(DESC_COUNTRY_OUTWARD_REMITT_3)
        parcel.writeString(DESC_CUST_SRC_OF_INCOME_INDUS)
        parcel.writeString(DESC_CUST_SRC_OF_INDUS_TYPE)
        parcel.writeString(DESC_SOURCE_OF_INCOME_LANDLORD)
        parcel.writeString(EXPECTED_INWARD_MTHLY_INCOME_PKR_1)
        parcel.writeString(EXPECTED_INWARD_MTHLY_INCOME_PKR_2)
        parcel.writeString(EXPECTED_INWARD_MTHLY_INCOME_PKR_3)
        parcel.writeString(EXPECTED_OUTWARD_MTHLY_INCOME_PKR_1)
        parcel.writeString(EXPECTED_OUTWARD_MTHLY_INCOME_PKR_2)
        parcel.writeString(EXPECTED_OUTWARD_MTHLY_INCOME_PKR_3)
        parcel.writeString(FILLED_BY)
        parcel.writeString(FOREIGN_ACCT_NO)
        parcel.writeString(HOLD_MAIL_INSTR)
        parcel.writeString(IS_FOREGIN_FUND_OW)
        parcel.writeString(IS_FOREIGN_FUND_IW)
        parcel.writeString(IS_OTHER_BANK_ACCOUNT)
        parcel.writeString(NAME_COUNTER_PARTY_1)
        parcel.writeString(NAME_COUNTER_PARTY_2)
        parcel.writeString(NAME_COUNTER_PARTY_3)
        parcel.writeString(NAME_OF_BANK_1)
        parcel.writeString(NAME_OF_BANK_2)
        parcel.writeString(NAME_OF_BANK_3)
        parcel.writeString(NAME_OF_BRANCH_1)
        parcel.writeString(NAME_OF_BRANCH_2)
        parcel.writeString(NAME_OF_BRANCH_3)
        parcel.writeString(NATR_EXP_MTH_HIGH_TURN_OVER)
        parcel.writeString(OTH_SOURCE_OF_INCOME)
        parcel.writeString(PRD_SERV_PRVD_BUSS_TRAN_1)
        parcel.writeString(PRD_SERV_PRVD_BUSS_TRAN_2)
        parcel.writeString(PRD_SERV_PRVD_BUSS_TRAN_3)
        parcel.writeString(PURPOSE_INWARD_1)
        parcel.writeString(PURPOSE_INWARD_2)
        parcel.writeString(PURPOSE_INWARD_3)
        parcel.writeString(PURPOSE_OUTWARD_1)
        parcel.writeString(PURPOSE_OUTWARD_2)
        parcel.writeString(PURPOSE_OUTWARD_3)
        parcel.writeString(SEASONAL_ACITIVITY_IN_ACCT)
        parcel.writeString(SOURCE_OF_INCOME_LANDLORD)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CUSTCDDX> {
        override fun createFromParcel(parcel: Parcel): CUSTCDDX {
            return CUSTCDDX(parcel)
        }

        override fun newArray(size: Int): Array<CUSTCDDX?> {
            return arrayOfNulls(size)
        }
    }
}