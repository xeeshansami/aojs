package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface KonnectTrackingIDCallBack {
    fun konnectTrackingIDSuccess(response: KonnectTrackingIDResponse)
    fun konnectTrackingIDFailure(response: BaseResponse)
}