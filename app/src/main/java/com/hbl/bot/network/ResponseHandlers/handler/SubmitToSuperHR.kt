package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class SubmitToSuperHR(callBack: SubmitToSuperCallBack) : BaseRH<SubmitToSuperResponse>() {
    var callback: SubmitToSuperCallBack = callBack
    override fun onSuccess(response: SubmitToSuperResponse?) {
        response?.let { callback.Success(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.Failure(it) }
    }
}