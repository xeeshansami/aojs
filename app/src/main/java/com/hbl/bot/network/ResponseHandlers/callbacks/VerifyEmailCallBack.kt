package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface VerifyEmailCallBack {
    fun VerifyEmailSuccess(response: VerifyEmailResponse)
    fun VerifyEmailFailure(response: BaseResponse)
}