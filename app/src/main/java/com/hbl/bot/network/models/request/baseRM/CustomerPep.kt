package com.hbl.bot.network.models.request.baseRM


import android.os.Parcel
import com.google.gson.annotations.SerializedName
import android.os.Parcelable

class CustomerPep() : Parcelable {
    @SerializedName("ADDITIONAL_INFO")
    var aDDITIONALINFO: String? = null

    @SerializedName("ADVERSE_MEDIA_CUSTOMER")
    var aDVERSEMEDIACUSTOMER: String? = null

    @SerializedName("ANALYSIS_OF_PEP")
    var aNALYSISOFPEP: String? = null

    @SerializedName("COMMENT")
    var cOMMENT: String? = null

    @SerializedName("COUNTRY_SENIOR_POSITION_HELD")
    var cOUNTRYSENIORPOSITIONHELD: String? = null

    @SerializedName("DESC_COUNTRY_SENIOR_POSITION_HELD")
    var dESCCOUNTRYSENIORPOSITIONHELD: String? = null

    @SerializedName("DESC_REASON_PEP_INDIVIDUAL")
    var dESCREASONPEPINDIVIDUAL: String? = null

    @SerializedName("DESC_SOURCE_OF_FUND")
    var dESCSOURCEOFFUND: String? = null

    @SerializedName("DESC_SOURCE_OF_WEALTH")
    var dESCSOURCEOFWEALTH: String? = null

    @SerializedName("DETAIL_ADVERSE_MEDIA_CUSTOMER")
    var dETAILADVERSEMEDIACUSTOMER: String? = null

    @SerializedName("DOMESTIC_PEP")
    var dOMESTICPEP: String? = null

    @SerializedName("FOREIGN_PEP")
    var fOREIGNPEP: String? = null

    @SerializedName("OTHER_SOURCE_OF_FUND")
    var oTHERSOURCEOFFUND: String? = null

    @SerializedName("OTHER_SOURCE_OF_WEALTH")
    var oTHERSOURCEOFWEALTH: String? = null

    @SerializedName("PERIOD_SENIOR_POSITION_HELD")
    var pERIODSENIORPOSITIONHELD: String? = null

    @SerializedName("PROFILE_CUST_BUSS_OCCUPATION")
    var pROFILECUSTBUSSOCCUPATION: String? = null

    @SerializedName("REASON_PEP_INDIVIDUAL")
    var rEASONPEPINDIVIDUAL: String? = null

    @SerializedName("SENIOR_POSITION_HELD")
    var sENIORPOSITIONHELD: String? = null

    @SerializedName("SOURCE_OF_FUND")
    var sOURCEOFFUND: String? = null

    @SerializedName("SOURCE_OF_WEALTH")
    var sOURCEOFWEALTH: String? = null

    constructor(parcel: Parcel) : this() {
        aDDITIONALINFO = parcel.readString()
        aDVERSEMEDIACUSTOMER = parcel.readString()
        aNALYSISOFPEP = parcel.readString()
        cOMMENT = parcel.readString()
        cOUNTRYSENIORPOSITIONHELD = parcel.readString()
        dESCCOUNTRYSENIORPOSITIONHELD = parcel.readString()
        dESCREASONPEPINDIVIDUAL = parcel.readString()
        dESCSOURCEOFFUND = parcel.readString()
        dESCSOURCEOFWEALTH = parcel.readString()
        dETAILADVERSEMEDIACUSTOMER = parcel.readString()
        dOMESTICPEP = parcel.readString()
        fOREIGNPEP = parcel.readString()
        oTHERSOURCEOFFUND = parcel.readString()
        oTHERSOURCEOFWEALTH = parcel.readString()
        pERIODSENIORPOSITIONHELD = parcel.readString()
        pROFILECUSTBUSSOCCUPATION = parcel.readString()
        rEASONPEPINDIVIDUAL = parcel.readString()
        sENIORPOSITIONHELD = parcel.readString()
        sOURCEOFFUND = parcel.readString()
        sOURCEOFWEALTH = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(aDDITIONALINFO)
        parcel.writeString(aDVERSEMEDIACUSTOMER)
        parcel.writeString(aNALYSISOFPEP)
        parcel.writeString(cOMMENT)
        parcel.writeString(cOUNTRYSENIORPOSITIONHELD)
        parcel.writeString(dESCCOUNTRYSENIORPOSITIONHELD)
        parcel.writeString(dESCREASONPEPINDIVIDUAL)
        parcel.writeString(dESCSOURCEOFFUND)
        parcel.writeString(dESCSOURCEOFWEALTH)
        parcel.writeString(dETAILADVERSEMEDIACUSTOMER)
        parcel.writeString(dOMESTICPEP)
        parcel.writeString(fOREIGNPEP)
        parcel.writeString(oTHERSOURCEOFFUND)
        parcel.writeString(oTHERSOURCEOFWEALTH)
        parcel.writeString(pERIODSENIORPOSITIONHELD)
        parcel.writeString(pROFILECUSTBUSSOCCUPATION)
        parcel.writeString(rEASONPEPINDIVIDUAL)
        parcel.writeString(sENIORPOSITIONHELD)
        parcel.writeString(sOURCEOFFUND)
        parcel.writeString(sOURCEOFWEALTH)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CustomerPep> {
        override fun createFromParcel(parcel: Parcel): CustomerPep {
            return CustomerPep(parcel)
        }

        override fun newArray(size: Int): Array<CustomerPep?> {
            return arrayOfNulls(size)
        }
    }
}