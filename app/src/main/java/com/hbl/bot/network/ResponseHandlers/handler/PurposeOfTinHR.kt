package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class PurposeOfTinHR(callBack: PurposeOfTinCallBack) : BaseRH<PurposeOfTinResponse>() {
    var callback: PurposeOfTinCallBack = callBack
    override fun onSuccess(response: PurposeOfTinResponse?) {
        response?.let { callback.PurposeOfTinSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PurposeOfTinFailure(it) }
    }
}