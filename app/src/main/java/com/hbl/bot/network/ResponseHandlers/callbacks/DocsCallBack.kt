package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.DocsResponse

interface DocsCallBack {
    fun DocsSuccess(response: DocsResponse)
    fun DocsFailure(response: BaseResponse)
}