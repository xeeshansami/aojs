package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class RMCodes() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    //THIS IS DESC
    @SerializedName("C2RNM")
    @Expose
    var C2RNM: String? = null
    //THIS IS CODE
    @SerializedName("C2ACO")
    @Expose
    var C2ACO: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<RMCodes> {
        override fun createFromParcel(parcel: Parcel): RMCodes {
            return RMCodes(parcel)
        }

        override fun newArray(size: Int): Array<RMCodes?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return C2ACO.toString()
//        return C2ACO.toString()
    }
}