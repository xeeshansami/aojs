package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface KonnectDebitCardInquiryCallBack {
    fun konnectDebitCardInquirySuccess(response: KonnectDebitCardInquiryResponse)
    fun konnectDebitCardInquiryFailure(response: BaseResponse)
}