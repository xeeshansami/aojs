package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Gender() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("GENDER_CODE")
    @Expose
    var GENDER_CODE: String? = null

    @SerializedName("GENDER_DESC")
    @Expose
    var GENDER_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<Gender> {
        override fun createFromParcel(parcel: Parcel): Gender {
            return Gender(parcel)
        }

        override fun newArray(size: Int): Array<Gender?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return GENDER_DESC.toString()
    }
}