package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class BiometricCustInfo() : Parcelable {
    var FATHER_NAME: String = ""
    var FIRST_NAME: String = ""
    var LAST_NAME: String = ""
    var MIDDLE_NAME: String = ""

    constructor(parcel: Parcel) : this() {
        FATHER_NAME = parcel.readString().toString()
        FIRST_NAME = parcel.readString().toString()
        LAST_NAME = parcel.readString().toString()
        MIDDLE_NAME = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(FATHER_NAME)
        parcel.writeString(FIRST_NAME)
        parcel.writeString(LAST_NAME)
        parcel.writeString(MIDDLE_NAME)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BiometricCustInfo> {
        override fun createFromParcel(parcel: Parcel): BiometricCustInfo {
            return BiometricCustInfo(parcel)
        }

        override fun newArray(size: Int): Array<BiometricCustInfo?> {
            return arrayOfNulls(size)
        }
    }
}