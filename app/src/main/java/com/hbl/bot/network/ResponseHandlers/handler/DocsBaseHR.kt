package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.DocsCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.DocsResponse

class DocsBaseHR(callBack: DocsCallBack) : BaseRH<DocsResponse>() {
    var callback: DocsCallBack = callBack
    override fun onSuccess(response: DocsResponse?) {
        response?.let { callback.DocsSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.DocsFailure(it) }
    }
}