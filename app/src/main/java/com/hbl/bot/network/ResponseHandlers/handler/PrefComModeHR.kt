package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class PrefComModeHR(callBack: PrefComModeCallBack) : BaseRH<PrefComModeResponse>() {
    var callback: PrefComModeCallBack = callBack
    override fun onSuccess(response: PrefComModeResponse?) {
        response?.let { callback.PrefComModeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PrefComModeFailure(it) }
    }
}