package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class NatureOfBusiness() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("BUS_CODE")
    @Expose
    var BUS_CODE: String? = null

    @SerializedName("BUS_DESC")
    @Expose
    var BUS_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<NatureOfBusiness> {
        override fun createFromParcel(parcel: Parcel): NatureOfBusiness {
            return NatureOfBusiness(parcel)
        }

        override fun newArray(size: Int): Array<NatureOfBusiness?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return BUS_DESC.toString()
    }
}