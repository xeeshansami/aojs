package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class AddressType() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("ADDR_CODE")
    @Expose
    var ADDR_CODE: String? = null

    @SerializedName("ADDR_DESC")
    @Expose
    var ADDR_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<AddressType> {
        override fun createFromParcel(parcel: Parcel): AddressType {
            return AddressType(parcel)
        }

        override fun newArray(size: Int): Array<AddressType?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return ADDR_DESC.toString()
    }
}