package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface BoolCallBack {
    fun BoolSuccess(response: BoolResponse)
    fun BoolFailure(response: BaseResponse)
}