package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.HawCompanyCallBack
import com.hbl.bot.network.models.response.base.*

class HawCompanyHR(callBack: HawCompanyCallBack) : BaseRH<HawCompanyResponse>() {
    var callback: HawCompanyCallBack = callBack
    override fun onSuccess(response: HawCompanyResponse?) {
        response?.let { callback.HawCompanySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.HawCompanyFailure(it) }
    }
}