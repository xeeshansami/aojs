package com.hbl.bot.network.models.response.base

import com.hbl.bot.network.models.response.baseRM.GenerateCIFData

data class GenerateCIFResponse(
    val `data`: List<GenerateCIFData>,
    val message: String,
    val status: String
)