package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface CIFCallBack {
    fun CIFSuccess(response: AofAccountInfoResponse)
    fun CIFFailure(response: BaseResponse)
}