package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class PAGESETUP(
) : Parcelable {
     var DOCID: String=""
     var LEFT_1: String=""
     var LEFT_2: String=""
     var PAGE_CODE: String=""
     var PAGE_COUNT: String=""
     var PAGE_NAME: String=""
     var RESOL_X: String=""
     var RESOL_Y: String=""
     var SIGN_COUNT: String=""
     var SIGN_REQ: String=""
     var SIGN_RESOL_X: String=""
     var SIGN_RESOL_Y: String=""
     var TOP_1: String=""
     var TOP_1_CALC: String=""
     var TOP_2: String=""
     var TOP_2_CALC: String=""
     var _id: String=""

    constructor(parcel: Parcel) : this() {
        DOCID = parcel.readString().toString()
        LEFT_1 = parcel.readString().toString()
        LEFT_2 = parcel.readString().toString()
        PAGE_CODE = parcel.readString().toString()
        PAGE_COUNT = parcel.readString().toString()
        PAGE_NAME = parcel.readString().toString()
        RESOL_X = parcel.readString().toString()
        RESOL_Y = parcel.readString().toString()
        SIGN_COUNT = parcel.readString().toString()
        SIGN_REQ = parcel.readString().toString()
        SIGN_RESOL_X = parcel.readString().toString()
        SIGN_RESOL_Y = parcel.readString().toString()
        TOP_1 = parcel.readString().toString()
        TOP_1_CALC = parcel.readString().toString()
        TOP_2 = parcel.readString().toString()
        TOP_2_CALC = parcel.readString().toString()
        _id = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(DOCID)
        parcel.writeString(LEFT_1)
        parcel.writeString(LEFT_2)
        parcel.writeString(PAGE_CODE)
        parcel.writeString(PAGE_COUNT)
        parcel.writeString(PAGE_NAME)
        parcel.writeString(RESOL_X)
        parcel.writeString(RESOL_Y)
        parcel.writeString(SIGN_COUNT)
        parcel.writeString(SIGN_REQ)
        parcel.writeString(SIGN_RESOL_X)
        parcel.writeString(SIGN_RESOL_Y)
        parcel.writeString(TOP_1)
        parcel.writeString(TOP_1_CALC)
        parcel.writeString(TOP_2)
        parcel.writeString(TOP_2_CALC)
        parcel.writeString(_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PAGESETUP> {
        override fun createFromParcel(parcel: Parcel): PAGESETUP {
            return PAGESETUP(parcel)
        }

        override fun newArray(size: Int): Array<PAGESETUP?> {
            return arrayOfNulls(size)
        }
    }
}