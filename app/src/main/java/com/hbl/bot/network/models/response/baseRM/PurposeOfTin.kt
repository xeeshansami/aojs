package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class PurposeOfTin() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    var _id: String? = null

    @SerializedName("PURP_CODE")
    @Expose
    var PURP_CODE: String? = null

    @SerializedName("PURP_DESC")
    @Expose
    var PURP_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<PurposeOfTin> {
        override fun createFromParcel(parcel: Parcel): PurposeOfTin {
            return PurposeOfTin(parcel)
        }

        override fun newArray(size: Int): Array<PurposeOfTin?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return PURP_DESC.toString()
    }
}