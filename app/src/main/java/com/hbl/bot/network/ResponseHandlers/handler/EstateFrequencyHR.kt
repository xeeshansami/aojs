package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class EstateFrequencyHR(callBack: EstateFrequencyCallBack) : BaseRH<EstateFrequencyResponse>() {
    var callback: EstateFrequencyCallBack = callBack
    override fun onSuccess(response: EstateFrequencyResponse?) {
        response?.let { callback.EstateFrequencySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.EstateFrequencyFailure(it) }
    }
}