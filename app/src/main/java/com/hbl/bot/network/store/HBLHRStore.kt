package com.hbl.bot.network.store

import android.app.Application
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.ResponseHandlers.handler.*
import com.hbl.bot.network.apiInterface.APIInterface
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.request.baseRM.DataRequest
import com.hbl.bot.network.models.request.baseRM.ETBNTBRequest
import com.hbl.bot.network.models.response.baseRM.NadraVerify
import com.hbl.bot.network.retrofitBuilder.RetrofitBuilder
import com.hbl.bot.network.timeoutInterface.IOnConnectionTimeoutListener
import com.hbl.bot.utils.Config
import com.hbl.bot.utils.GlobalClass
import okhttp3.RequestBody

open class HBLHRStore : Application(), IOnConnectionTimeoutListener {

    override fun onConnectionTimeout() {}

    companion object {
        private val consumerStore: HBLHRStore? = null

        //    APIInterface globalBaseUrl = RetrofitBuilder.INSTANCE.getRetrofitInstance(GlobalClass.applicationContext, RetrofitEnums.URL_HBL);
        val instance: HBLHRStore?
            get() = consumerStore ?: HBLHRStore()
    }

    //:TODO post getLogin
    fun getLogin(
        url: RetrofitEnums?,
        loginRequest: LoginRequest?,
        loginCallBack: LoginCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,    url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getLogin(loginRequest)!!.enqueue(LoginBaseHR(loginCallBack))
    }

    //:TODO get getDocsLov
    fun getDocs(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: DocsCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getDocs(lovRequest)?.enqueue(DocsBaseHR(callback))
    }
    //:TODO get getHawCompanies
    fun getHawCompanies(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: HawCompanyCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getHawCompanies(lovRequest)?.enqueue(HawCompanyHR(callback))
    }

    //:TODO getPriority
    fun getPriority(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: PriorityCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPriority(lovRequest)?.enqueue(PriorityHR(callback))
    }

    //:TODO getWorkFlow
    fun getWorkFlow(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: WorkFlowCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getWorkFlow(lovRequest)?.enqueue(WorkFlowHR(callback))
    }

    //:TODO getMySisRef
    fun getMySisRef(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: MySisRefCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getMySisRef(lovRequest)?.enqueue(MySisRefHR(callback))
    }

    //:TODO getBusinessArea
    fun getBusinessArea(
        url: RetrofitEnums?,
        lovRequest: BusinessAreaRequest?,
        callback: BusinessAreaCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getBusinessArea(lovRequest)?.enqueue(BusinessAreaHR(callback))
    }

    //:TODO getAppStatus
    fun getAppStatus(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: AppStatusCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getAppStatus(lovRequest)?.enqueue(AppStatusHR(callback))
    }


    //:TODO get getDocsLov
    fun getCustomerSegment(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: CustomerSegmentCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCustomerSegments(lovRequest)
            ?.enqueue(CustomerSegmentHR(callback))
    }

    //:TODO get Countries
    fun getCountries(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: CountryCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCountries(lovRequest)?.enqueue(CountryHR(callback))
    }

    //:TODO get Bools
    fun getBools(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: BoolCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getBools(lovRequest)?.enqueue(BoolBaseHR(callback))
    }

    //:TODO get getCities
    fun getCities(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: CityCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCities(lovRequest)?.enqueue(CityHR(callback))
    }

    //:TODO getChequeBookLeaves
    fun getChequeBookLeaves(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: ChequeBookLeavesCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getChequeBookLeaves(lovRequest)?.enqueue(ChequeBookLeavesHR(callback))
    }


    //:TODO getModeOfDelivery
    fun getMOD(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: ModeOfDeliveryCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getMOD(lovRequest)?.enqueue(ModeOfDeliveryHR(callback))
    }

    //:TODOg getSourceOfFundLandLoard
    fun getSourceOfFundLandLoard(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: SourceOfincomeLandLordCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getSourceOfFundLandLoard(lovRequest)
            ?.enqueue(SourceOfIncomeLandLordHR(callback))
    }

    //:TODO get getPurposeCIF
    fun getPurposeCIF(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: PurposeCIFCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPurpOfCif(lovRequest)?.enqueue(PurposeCIFHR(callback))
    }
    //:TODO getPreferenceMailing
    fun getPreferenceMailing(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: PreferencesMailingCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPreferenceMailing(lovRequest)?.enqueue(PreferencesMailingBaseHR(callback))
    }

    //:TODO getSourceOffund
    fun getSourceOffund(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: SourceOffundCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getSourceOffund(lovRequest)?.enqueue(SourceOffundHR(callback))
    }

    //:TODO getCapacity
    fun getCapacity(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: CapacityCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCapacity(lovRequest)?.enqueue(CapacityHR(callback))
    }

    //:TODO getAccountOperationsInst
    fun getAccountOperationsInst(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: AccountOperationsInstCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getAccountOperationsInst(lovRequest)
            ?.enqueue(AccountOperationsInstHR(callback))
    }

    //:TODO getAccountOperationsType
    fun getAccountOperationsType(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: AccountOperationsTypeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getAccountOperationsType(lovRequest)
            ?.enqueue(AccountOperationsTypeHR(callback))
    }

    //:TODO getSourceOfIncome
    fun getSourceOfIncome(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: SourceOfIncomeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getSourceOfIncome(lovRequest)?.enqueue(SourceOfIncomeHR(callback))
    }

    //:TODO getPurposeOfTin
    fun getPurposeOfTin(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: PurposeOfTinCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPurposeOfTin(lovRequest)?.enqueue(PurposeOfTinHR(callback))
    }

    //:TODO getReasonePrefEDD
    fun getReasonePrefEDD(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: ReasonPrefEDDCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getReasonePrefEDD(lovRequest)?.enqueue(ReasonPrefEDDHR(callback))
    }

    //:TODO getAddressType
    fun getAddressType(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: AddressTypeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getAddressType(lovRequest)?.enqueue(AddressTypeHR(callback))
    }

    //:TODO getSourceOfWealth
    fun getSourceOfWealth(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: SourceOfWealthCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getSourceOfWealth(lovRequest)?.enqueue(SourceOfWealthHR(callback))
    }

    //:TODO getPep
    fun getPep(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: PepCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPep(lovRequest)?.enqueue(PepHR(callback))
    }

    //:TODO getPepCategories
    fun getPepCategories(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: PepCategoriesCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPepCategories(lovRequest)?.enqueue(PepCategoriesHR(callback))
    }

    //:TODO getPepSubCategories
    fun getPepSubCategories(
        url: RetrofitEnums?,
        request: PepSubCategoriesRequest?,
        callback: PepSubCategoriesCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPepSubCategories(request)?.enqueue(PepSubCategoriesHR(callback))
    }

    //:TODO getCountryCityCode
    fun getCountryCityCode(
        url: RetrofitEnums?,
        request: GetCountryCityCodeRequest?,
        callback: GetCountryCityCodeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCountryCityCode(request)
            ?.enqueue(GetCountryCityCodeHR(callback))
    }

    //:TODO get Title
    fun getTitle(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: TitleCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getTitle(lovRequest)?.enqueue(TitleHR(callback))
    }

    //:TODO get MaritalStatus
    fun getMarital(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: MaritalCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getMarital(lovRequest)?.enqueue(MaritalHR(callback))
    }

    //:TODO get Gender
    fun getGender(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: GenderCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getGender(lovRequest)?.enqueue(GenderHR(callback))
    }

    //:TODO getHawKeys
    fun getHawKeys(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: HawKeysCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getHawKeys(lovRequest)?.enqueue(HawKeysBaseHR(callback))
    }

    //:TODO postSignature
    fun postSignature(
        url: RetrofitEnums?,
        request: SignatureRequest?,
        callback: SignatureCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.postSignature(request)?.enqueue(SignatureHR(callback))
    }

    //:TODO get SBPCodes
    fun getSBPCodes(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: SBPCodesCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getSBPCodes(lovRequest)?.enqueue(SBPCodesHR(callback))
    }

    //:TODO getTrackingID
    fun getTrackingID(
        url: RetrofitEnums?,
        trackingIDRequest: TrackingIDRequest?,
        callback: TrackingIDCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getTrackingID(trackingIDRequest)?.enqueue(TrackingIDHR(callback))
    }

    //:TODO getBranchType
    fun getBranchType(
        url: RetrofitEnums?,
        branchRequest: BranchCodeRequest?,
        callback: BranchTypeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getBranchType(branchRequest)?.enqueue(BranchTypeHR(callback))
    }

    //:TODO get SBPCodes
    fun getBranchCode(
        url: RetrofitEnums?,
        branchRequest: BranchCodeRequest?,
        callback: BranchCodeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getBranchCode(branchRequest)?.enqueue(BranchCodeHR(callback))
    }

    //:TODO get SBPCodes
    fun getCardProducts(
        url: RetrofitEnums?,
        lovRequest: DebitCardLovRequest?,
        callback: CardProductCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCardProducts(lovRequest)?.enqueue(CardProductHR(callback))
    }

    //:TODO get RMCodes
    fun getRMCodes(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: RMCodesCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getRMCodes(lovRequest)?.enqueue(RMCodesHR(callback))
    }


    //:TODO get Sundry
    fun getSundry(
        url: RetrofitEnums?, lovRequest: LovRequest?, callback: SundryCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getSundry(lovRequest)?.enqueue(SundryHR(callback))
    }

    //:TODO get ConsumerProduct
    fun getConsumerProduct(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: ConsumerProductCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getConsumerProduct(lovRequest)
            ?.enqueue(ConsumerProductHR(callback))
    }
//:TODO get Relationship

    fun getRelationship(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: RelationshipCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getRelationship(lovRequest)?.enqueue(RelationshipHR(callback))
    }

    //:TODO get Province
    fun getProvince(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: ProvinceCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getProvince(lovRequest)?.enqueue(ProvinceHR(callback))
    }

    //:TODO getBioAccountType
    fun getBioAccountType(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: BioAccountTypeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getBioAccountType(lovRequest)?.enqueue(BioAccountTypeHR(callback))
    }

    //:TODO get BioAccountType
    fun getPrefComMode(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: PrefComModeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPrefComMode(lovRequest)?.enqueue(PrefComModeHR(callback))
    }

    //:TODO getEstateFrequency
    fun getEstateFrequency(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: EstateFrequencyCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getEstateFrequency(lovRequest)
            ?.enqueue(EstateFrequencyHR(callback))
    }

    //:TODO getPurposeOfAccount
    fun getPurposeOfAccount(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: PurposeOfAccountCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getPurposeOfAccount(lovRequest)
            ?.enqueue(PurposeOfAccountHR(callback))
    }

    //:TODO getDepositType
    fun getDepositType(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: DepositTypeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getDepositType(lovRequest)?.enqueue(DepositTypeHR(callback))
    }

    //:TODO getCurrency
    fun getCurrency(
        url: RetrofitEnums?,
        lovRequest: CurrenyLovRequest?,
        callback: CurrencyCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCurrency(lovRequest)?.enqueue(CurrencyHR(callback))
    }

    //:TODO getAccountType
    fun getAccountType(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: AccountTypeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getAccountType(lovRequest)?.enqueue(AccountTypeHR(callback))
    }

    //:TODO getIndustryMainCategory
    fun getIndustryMainCategory(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: IndustryMainCategoryBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getIndustryMainCategory(lovRequest)
            ?.enqueue(IndustryMainCategoryHR(callback))
    }

    //:TODO getIndustrySubCategory
    fun getIndustrySubCategory(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: IndustrySubCategoryBack
    ) {
        var privateInstanceRetrofit:  APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getIndustrySubCategory(lovRequest)
            ?.enqueue(IndustrySubCategoryHR(callback))
    }

    //:TODO getModeOfTrans
    fun getModeOfTrans(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: ModeOfTransCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getModeOfTrans(lovRequest)
            ?.enqueue(ModeOfTransHR(callback))
    }

    //:TODO getNatureOfBusiness
    fun getNatureOfBusiness(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: NatureOfBusinessCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getNatureOfBusiness(lovRequest)
            ?.enqueue(NatureOfBusinessHR(callback))
    }

    //:TODO getProfessions
    fun getProfessions(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: ProfessionCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getProfessions(lovRequest)
            ?.enqueue(ProfessionHR(callback))
    }
 //:TODO getBranchRegion
    fun getBranchRegion(
        url: RetrofitEnums?,
        lovRequest: LovRequest?,
        callback: BranchRegionCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getBranchRegion(lovRequest)
            ?.enqueue(BranchRegionHR(callback))
    }

//:TODO getUrl
    fun getUrl(
        url: RetrofitEnums?,
        request: ConfigRequest?,
        callback: UrlCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getUrl(request)
            ?.enqueue(UrlHR(callback))
    }

    //:TODO post NadraVerifyResponse
    fun postNadraVerfication(
        url: RetrofitEnums?,
        nadraVerifyRequest: NadraVerify?,
        callback: NadraVerifyCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.postNadraVerfication(nadraVerifyRequest)
            ?.enqueue(NadraVerifyHR(callback))
    }

    //:TODO postManualVerysis
    fun postManualVerysis(
        url: RetrofitEnums?,
        request: ManualVersysisRequest?,
        callback: NadraVerifyCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.postManualVerysis(request)
            ?.enqueue(NadraVerifyHR(callback))
    }

 //:TODO GetVerysisRequest
    fun getVerysis(
        url: RetrofitEnums?,
        nadraVerifyRequest: GetVerysisRequest?,
        callback: NadraVerifyCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getVerysis(nadraVerifyRequest)
            ?.enqueue(NadraVerifyHR(callback))
    }


    //:TODO get getDocTypes
    fun getDocumentType(
        url: RetrofitEnums?,
        lovRequest: DocLovRequest?,
        callback: DocumentTypeCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getDocumentType(lovRequest)?.enqueue(DocumentTypeHR(callback))
    }

    //:TODO get PartialUpload
    fun partialUpload(
        url: RetrofitEnums?,
        lovRequest: PartialUploadRequest?,
        callback: PartialUploadCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.PartialUpload(lovRequest)?.enqueue(PartialUploadHR(callback))
    }

    //:TODO get getConfig
    fun getConfig(
        url: RetrofitEnums?,
        lovRequest: ConfigRequest?,
        callback: ConfigCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getConfig(lovRequest)?.enqueue(ConfigHR(callback))
    }

    //:TODO get SubmitToSuper
    fun SubmitToSuper(
        url: RetrofitEnums?,
        lovRequest: SubmitToSuperRequest?,
        callback: SubmitToSuperCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.SubmitToSuper(lovRequest)?.enqueue(SubmitToSuperHR(callback))
    }

    //:TODO post postAofaccountInfo
    fun postAofaccountInfo(
        url: RetrofitEnums?,
        customerAofAccountInfoRequest: AofAccountInfoRequest?,
        callback: CustomerAofAccountInfoCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.postAofaccountInfo(customerAofAccountInfoRequest)
            ?.enqueue(CustomerAofAccountInfoHR(callback))
    }

    //:TODO postGenerateCIF
    fun postGenerateCIF(
        url: RetrofitEnums?,
        request: GenerateCIFRequest?,
        callback: GenerateCIFCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.postSubmitGenerateCIF(request)
            ?.enqueue(GenerateCIFHR(callback))
    }

    //:TODO postGenerateCIF
    fun postSubmitGenerateCIFAccount(
        url: RetrofitEnums?,
        request: GenerateCIFRequest?,
        callback: GenerateCIFAccountCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.postSubmitGenerateCIFAccount(request)
            ?.enqueue(GenerateCIFAccountHR(callback))
    }

    //:TODO postAofaccountInfo
    fun postRiskRating(
        url: RetrofitEnums?,
        customerAofAccountInfoRequest: AofAccountInfoRequest?,
        callback: CustomerRiskRatingCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.postRiskRating(customerAofAccountInfoRequest)
            ?.enqueue(CustomerRiskRatingHR(callback))
    }

    //:TODO post submitETBNTB
    fun submitETBNTB(
        url: RetrofitEnums?,
        etbntbRequest: ETBNTBRequest?,
        callback: ETBNTBCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.submitETBNTB(etbntbRequest)
            ?.enqueue(ETBNTBHR(callback))
    }

    //:TODO post HTML
    fun gettemplate(
        url: RetrofitEnums?,
        HTMLRequest: HTMLRequest?,
        callback: HTMLCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.gettemplate(HTMLRequest)
            ?.enqueue(HTMLHR(callback))
    }

    //:TODO post HTML
    fun genCIF(
        url: RetrofitEnums?,
        dataRequest: DataRequest?,
        callback: GenDataCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.genCIF(dataRequest)
            ?.enqueue(GenCifHR(callback))
    }

    //:TODO post HTML
    fun genCIFAndAccount(
        url: RetrofitEnums?,
        dataRequest: DataRequest?,
        callback: GenDataCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.genCIFAndAccount(dataRequest)
            ?.enqueue(GenCifHR(callback))
    }

    //:TODO post OnBoardingETBNTB
    fun OnBoarding(
        url: RetrofitEnums?,
        onBoardingRequest: OnBoardingRequest?,
        callback: OnBoardingCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.onboarding(onBoardingRequest)
            ?.enqueue(OnBoardingHR(callback))
    }

    //:TODO post callUpdate
    fun callUpdate(
        url: RetrofitEnums?,
        request: UpdateCallRequest,
        callback: callUpdateCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.callUpdate(request)
            ?.enqueue(callUpdateHR(callback))
    }

    //:TODO post getCIF
    fun getCIF(
        url: RetrofitEnums?,
        request: CIFRequest,
        callback: CIFCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCIF(request)
            ?.enqueue(CIFHR(callback))
    }

    //:TODO post VerifyAccount
    fun VerifyAccount(
        url: RetrofitEnums?,
        request: VerifyAccountRequest,
        callback: VerifyAccountCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.VerifyAccount(request)
            ?.enqueue(VerifyAccountHR(callback))
    }
//:TODO post checkNumberNadra
    fun checkNumberNadra(
        url: RetrofitEnums?,
        request: CheckNumberNadraRequest,
        callback: CheckNumberNadraCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.checkNumberNadra(request)
            ?.enqueue(CheckNumberNadraBaseHR(callback))
    }

    //:TODO post getKINCIF
    fun getKINCIF(
        url: RetrofitEnums?,
        request: CIFRequest,
        callback: KINCIFCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKINCIF(request)
            ?.enqueue(KINCIFHR(callback))
    }

    //:TODO post meetUpdate
    fun meetUpdate(
        url: RetrofitEnums?,
        request: UpdateMeetRequest,
        callback: meetUpdateCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.meetUpdate(request)
            ?.enqueue(meetUpdateHR(callback))
    }

    //:TODO pepCheck
    fun pepCheck(
        url: RetrofitEnums?,
        request: PepCheckRequest,
        callback: PepCheckCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.pepCheck(request)
            ?.enqueue(PepCheckHR(callback))
    }

    //:TODO post showRecords
    fun showRecords(
        url: RetrofitEnums?,
        request: showRecordsRequest,
        callback: showRecordsCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.showRecords(request)
            ?.enqueue(showRecordsHR(callback))
    }

    //:TODO post showRecords
    fun postDashboardItems(
        url: RetrofitEnums?,
        request: DashboardItemsRequest,
        callback: DashboardItemsCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.postDashboardItems(request)
            ?.enqueue(DashboardItemsBaseHR(callback))
    }


    //:TODO post showFilterRecords
    fun showFilterRecords(
        url: RetrofitEnums?,
        request: showFilterRecordsRequest,
        callback: showRecordsCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.showFilterRecords(request)
            ?.enqueue(showRecordsHR(callback))
    }

    //:TODO post getCount
    fun getCount(
        url: RetrofitEnums?,
        request: CountRequest,
        callback: CountCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getCount(request)
            ?.enqueue(CountHR(callback))
    }

    //:TODO post printDocs
    /*fun printDocs(
        url: RetrofitEnums?,
        request: PrintDocsRequest,
        callback: PrintDocCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.printDocs(request)
            ?.enqueue(PrintDocsHR(callback))
    }*/

    fun printDocs(
        url: RetrofitEnums?,
        printdoc: RequestBody,
        trackingid: RequestBody,
        cnic: RequestBody,
        mysisno: RequestBody,
        cifno: RequestBody,
        identifier: RequestBody,
        callback: PrintDocCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.printDocs(printdoc,trackingid,cnic,mysisno,cifno,identifier)
            ?.enqueue(PrintDocsHR(callback))
    }

    //:TODO callDraftData
    fun callDraftData(
        url: RetrofitEnums?,
        request: DraftRequest,
        callback: DraftCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.callDraftData(request)
            ?.enqueue(DraftBaseHR(callback))
    }

 //:TODO callDraftData
    fun callDraftMaintanance(
        url: RetrofitEnums?,
        request: DraftMaintainanceRequest,
        callback: DraftCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.callDraftMaintanance(request)
            ?.enqueue(DraftBaseHR(callback))
    }

    //:TODO post getPrintDocs
    fun getPrintDocs(
        url: RetrofitEnums?,
        request: PrintDocumentRequest,
        callback: PrintDocumentCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_PDF_CONNECT_TIME_OUT) }
        privateInstanceRetrofit?.getPrintDocs(request)
            ?.enqueue(PrintDocumentBaseHR(callback))
    }

    //:TODO post getPDF
    fun getPDF(
        url: RetrofitEnums?,
        request: PrintPDFRequest,
        callback: PrintPDFCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_PDF_CONNECT_TIME_OUT) }
        privateInstanceRetrofit?.getPDF(request)
            ?.enqueue(PrintPDFBaseHR(callback))
    }

    //:TODO post VerifyEmail
    fun getVerifyEmail(
        url: RetrofitEnums?,
        request: VerifyEmailRequest,
        callback: VerifyEmailCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.VerifyEmail(request)
            ?.enqueue(VerifyEmailHR(callback))
    }

    //:TODO post VerifyOTP
    fun getVerifyOTP(
        url: RetrofitEnums?,
        request: VerifyOTPRequest,
        callback: VerifyOTPCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.VerifyOTP(request)
            ?.enqueue(VerifyOTPHR(callback))
    }

   //:TODO post sendAppDetails
    fun sendAppDetails(
        url: RetrofitEnums?,
        request: AppDetailsRequest,
        callback: AppStatusCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.sendAppDetails(request)
            ?.enqueue(AppStatusHR(callback))
    }
    //:TODO post APKCheck
    fun APKCheck(
        url: RetrofitEnums?,
        request: APKCheckRequest,
        callback: APKCheckCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.APKCheck(request)
            ?.enqueue(APKCheckBaseHR(callback))
    }
    //:TODO post ManualVerysisRequest
    fun manualVerysisRequest(
        url: RetrofitEnums?,
        request: ManualVerysisRequest,
        callback: APKCheckCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.manualVerysis(request)
            ?.enqueue(APKCheckBaseHR(callback))
    }

    //:TODO getTrackingID
    fun getKonnectTrackingID(
        url: RetrofitEnums?,
        trackingIDRequest: TrackingIDRequest?,
        callback: KonnectTrackingIDCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectTrackingID(trackingIDRequest?.brcode)?.enqueue(KonnectTrackingIDHR(callback))
    }

    //:TODO getPurposeOfAccount
    fun getBranchCity(
        url: RetrofitEnums?,
        branchCode: String?,
        callback: KonnectBranchCityCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectBranchCity(branchCode)
            ?.enqueue(KonnectBranchCityHR(callback))
    }

    //:TODO getPurposeOfAccount
    fun getKonnectPurposeOfAccount(
        url: RetrofitEnums?,
//        lovRequest: LovRequest?,
        callback: KonnectPurposeOfAccountCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectPurposeOfAccount(/*lovRequest*/)
            ?.enqueue(KonnectPurposeOfAccountHR(callback))
    }

    fun getKonnectInquiry(
        url: RetrofitEnums?,
        konnectInquiryRequest: KonnectInquiryRequest?,
        callback: KonnectInquiryCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectInquiry(konnectInquiryRequest)
            ?.enqueue(KonnectInquiryHR(callback))
    }

    //:TODO post submitETBNTB
    fun getKonnectETBNTB(
        url: RetrofitEnums?,
        etbntbRequest: ETBNTBRequest?,
        callback: ETBNTBCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectETBNTB(etbntbRequest)
            ?.enqueue(ETBNTBHR(callback))
    }
    //:TODO post getCIF
    fun getKonnectCIF(
        url: RetrofitEnums?,
        request: CIFRequest,
        callback: CIFCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectCIF(request)
            ?.enqueue(CIFHR(callback))
    }
    fun getKonnectAccountOpen(
        url: RetrofitEnums?,
        request: KonnectOpenAccountRequest,
        callback: KonnectOpenAccountCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectOpenAccount(request)
            ?.enqueue(KonnectOpenAccountHR(callback))
    }
    fun getKonnectDebitCardInquiry(
        url: RetrofitEnums?,
        konnectDebitCardInquiryRequest: KonnectDebitCardInquiryRequest?,
        callback: KonnectDebitCardInquiryCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectDebitCardInquiry(konnectDebitCardInquiryRequest)
            ?.enqueue(KonnectDebitCardInquiryHR(callback))
    }
    fun getKonnectDebitCardTransaction(
        url: RetrofitEnums?,
        konnectDebitCardTransactionRequest: KonnectDebitCardTransactionRequest?,
        callback: KonnectDebitCardTransactionCallBack
    ) {
        var privateInstanceRetrofit: APIInterface? =
            GlobalClass.applicationContext?.let { RetrofitBuilder.getRetrofitInstance(it,  url!!, Config.API_CONNECT_TIMEOUT) }
        privateInstanceRetrofit?.getKonnectDebitCardTransaction(konnectDebitCardTransactionRequest)
            ?.enqueue(KonnectDebitCardTransactionHR(callback))
    }

}