package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable

class CUSTACCOUNTSX(
) : Parcelable {
    var ACCTTYPEDESC: String? = null
    var OPER_CODE: String? = null
    var ACCT_CCY: String? = null
    var ACCT_NO: String? = null
    var ACCT_SNG_JNT: String? = null
    var ACCT_SNG_JNT_DESC: String? = null
    var ACCT_TYPE: String? = null
    var BRANCHNAME: String? = null
    var BRANCH_CODE: String? = null
    var CIF_NO: String? = null
    var CONVENTIONAL_ISLAMIC: String? = null
    var CURRENCY: String? = null
    var CURRENCY_DESC: String? = null
    var DEBITCARDTYPEDESC: String? = null
    var DEBIT_CARD_REQ: String? = null
    var DEBIT_CARD_TYPE: String? = null
    var ESTATEMENT: String? = null
    var ESTATEMENTFREQDESC: String? = null
    var ESTATEMENT_FREQ: String? = null
    var FREE_INSURANCE: String? = null
    var HBL_NISA: String? = null
    var ID_DOC_NO: String? = null
    var INITDEPSOURCEDESC: String? = null
    var INIT_DEPOSIT: String? = null
    var INIT_DEP_SOURCE: String? = null
    var INTERNET_BANK: String? = null
    var NAME_ON_CARD: String? = null
    var OPERINSTDESC: String? = null
    var OPER_INSTRUCTION: String? = null
    var OTHER_PURPOSE_OF_ACCT_DESC: Any? = null
    var PHONE_BANK: String? = null
    var PREFERREDCOMMUMODEDESC: String? = null
    var PREFERRED_COMMU_MODE: String? = null
    var PURPOSEOFACCTDESC: String? = null
    var PURPOSE_OF_ACCT: String? = null
    var RM_CODE: String? = null
    var RM_DESC: String? = null
    var SMS_ALERT: String? = null
    var TITLE_OF_ACCT: String? = null
    var TITLE_SHORT_OF_ACCT: String? = null
    var ZAKAT_EXEMPT: String? = null

    constructor(parcel: Parcel) : this() {
        OPER_CODE = parcel.readString()
        ACCTTYPEDESC = parcel.readString()
        ACCT_CCY = parcel.readString()
        ACCT_NO = parcel.readString()
        ACCT_SNG_JNT = parcel.readString()
        ACCT_SNG_JNT_DESC = parcel.readString()
        ACCT_TYPE = parcel.readString()
        BRANCHNAME = parcel.readString()
        BRANCH_CODE = parcel.readString()
        CIF_NO = parcel.readString()
        CONVENTIONAL_ISLAMIC = parcel.readString()
        CURRENCY = parcel.readString()
        CURRENCY_DESC = parcel.readString()
        DEBITCARDTYPEDESC = parcel.readString()
        DEBIT_CARD_REQ = parcel.readString()
        DEBIT_CARD_TYPE = parcel.readString()
        ESTATEMENT = parcel.readString()
        ESTATEMENTFREQDESC = parcel.readString()
        ESTATEMENT_FREQ = parcel.readString()
        FREE_INSURANCE = parcel.readString()
        HBL_NISA = parcel.readString()
        ID_DOC_NO = parcel.readString()
        INITDEPSOURCEDESC = parcel.readString()
        INIT_DEPOSIT = parcel.readString()
        INIT_DEP_SOURCE = parcel.readString()
        INTERNET_BANK = parcel.readString()
        NAME_ON_CARD = parcel.readString()
        OPERINSTDESC = parcel.readString()
        OPER_INSTRUCTION = parcel.readString()
        PHONE_BANK = parcel.readString()
        PREFERREDCOMMUMODEDESC = parcel.readString()
        PREFERRED_COMMU_MODE = parcel.readString()
        PURPOSEOFACCTDESC = parcel.readString()
        PURPOSE_OF_ACCT = parcel.readString()
        RM_CODE = parcel.readString()
        RM_DESC = parcel.readString()
        SMS_ALERT = parcel.readString()
        TITLE_OF_ACCT = parcel.readString()
        TITLE_SHORT_OF_ACCT = parcel.readString()
        ZAKAT_EXEMPT = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(ACCTTYPEDESC)
        parcel.writeString(OPER_CODE)
        parcel.writeString(ACCT_CCY)
        parcel.writeString(ACCT_NO)
        parcel.writeString(ACCT_SNG_JNT)
        parcel.writeString(ACCT_SNG_JNT_DESC)
        parcel.writeString(ACCT_TYPE)
        parcel.writeString(BRANCHNAME)
        parcel.writeString(BRANCH_CODE)
        parcel.writeString(CIF_NO)
        parcel.writeString(CONVENTIONAL_ISLAMIC)
        parcel.writeString(CURRENCY)
        parcel.writeString(CURRENCY_DESC)
        parcel.writeString(DEBITCARDTYPEDESC)
        parcel.writeString(DEBIT_CARD_REQ)
        parcel.writeString(DEBIT_CARD_TYPE)
        parcel.writeString(ESTATEMENT)
        parcel.writeString(ESTATEMENTFREQDESC)
        parcel.writeString(ESTATEMENT_FREQ)
        parcel.writeString(FREE_INSURANCE)
        parcel.writeString(HBL_NISA)
        parcel.writeString(ID_DOC_NO)
        parcel.writeString(INITDEPSOURCEDESC)
        parcel.writeString(INIT_DEPOSIT)
        parcel.writeString(INIT_DEP_SOURCE)
        parcel.writeString(INTERNET_BANK)
        parcel.writeString(NAME_ON_CARD)
        parcel.writeString(OPERINSTDESC)
        parcel.writeString(OPER_INSTRUCTION)
        parcel.writeString(PHONE_BANK)
        parcel.writeString(PREFERREDCOMMUMODEDESC)
        parcel.writeString(PREFERRED_COMMU_MODE)
        parcel.writeString(PURPOSEOFACCTDESC)
        parcel.writeString(PURPOSE_OF_ACCT)
        parcel.writeString(RM_CODE)
        parcel.writeString(RM_DESC)
        parcel.writeString(SMS_ALERT)
        parcel.writeString(TITLE_OF_ACCT)
        parcel.writeString(TITLE_SHORT_OF_ACCT)
        parcel.writeString(ZAKAT_EXEMPT)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CUSTACCOUNTSX> {
        override fun createFromParcel(parcel: Parcel): CUSTACCOUNTSX {
            return CUSTACCOUNTSX(parcel)
        }

        override fun newArray(size: Int): Array<CUSTACCOUNTSX?> {
            return arrayOfNulls(size)
        }
    }
}