package com.hbl.bot.network.models.request.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class GEOLOCATION() : Parcelable {
    @SerializedName("HAVE_GOOGLE_SERVICES")
    @Expose
    var HAVE_GOOGLE_SERVICES: String? = null
    @SerializedName("LATITUDE")
    @Expose
    var LATITUDE: String? = null

    @SerializedName("LONGITUDE")
    @Expose
    var LONGITUDE: String? = null

    @SerializedName("GEO_LOCATION_ADDRESS")
    @Expose
    var GEO_LOCATION_ADDRESS: String? = null

    constructor(parcel: Parcel) : this() {
        HAVE_GOOGLE_SERVICES = parcel.readString()
        LATITUDE = parcel.readString()
        LONGITUDE = parcel.readString()
        GEO_LOCATION_ADDRESS = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(HAVE_GOOGLE_SERVICES)
        parcel.writeString(LATITUDE)
        parcel.writeString(LONGITUDE)
        parcel.writeString(GEO_LOCATION_ADDRESS)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GEOLOCATION> {
        override fun createFromParcel(parcel: Parcel): GEOLOCATION {
            return GEOLOCATION(parcel)
        }

        override fun newArray(size: Int): Array<GEOLOCATION?> {
            return arrayOfNulls(size)
        }
    }
}