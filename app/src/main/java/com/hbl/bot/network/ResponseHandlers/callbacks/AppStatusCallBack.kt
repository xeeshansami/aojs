package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface AppStatusCallBack {
    fun AppStatusSuccess(response: AppStatusResponse)
    fun AppStatusFailure(response: BaseResponse)
}