package com.hbl.bot.network.enums

import com.hbl.bot.utils.Config

enum class RetrofitEnums(var url: String) {
    URL_MAPS(Config.BASE_URL_MAP),
    URL_HBL(Config.BASE_URL_HBL),
    URL_KONNECT(Config.BASE_URL_HBL_KONNECT);
}