package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface showRecordsCallBack {
    fun showRecordsSuccess(response: showRecordsResponse)
    fun showRecordsFailure(response: BaseResponse)
}