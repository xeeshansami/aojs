package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface AccountOperationsInstCallBack {
    fun AccountOperationsInstSuccess(response: AccountOperationsInstResponse)
    fun AccountOperationsInstFailure(response: BaseResponse)
}