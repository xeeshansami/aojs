package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class PrintDocumentResponse(
) : Parcelable {
    var data: ArrayList<PrintDocsData> =ArrayList<PrintDocsData>()
    var message: String=""
    var status: String=""

    constructor(parcel: Parcel) : this() {
        message = parcel.readString().toString()
        status = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(message)
        parcel.writeString(status)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PrintDocumentResponse> {
        override fun createFromParcel(parcel: Parcel): PrintDocumentResponse {
            return PrintDocumentResponse(parcel)
        }

        override fun newArray(size: Int): Array<PrintDocumentResponse?> {
            return arrayOfNulls(size)
        }
    }
}