
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CUSTPEP {

    @SerializedName("REASON_PEP_INDIVIDUAL")
    @Expose
    public String REASON_PEP_INDIVIDUAL = "";

    public String getPEP_HIT_BTN_RESPONSE() {
        return PEP_HIT_BTN_RESPONSE;
    }

    public void setPEP_HIT_BTN_RESPONSE(String PEP_HIT_BTN_RESPONSE) {
        this.PEP_HIT_BTN_RESPONSE = PEP_HIT_BTN_RESPONSE;
    }

    @SerializedName("PEP_HIT_BTN_RESPONSE")
    @Expose
    public String PEP_HIT_BTN_RESPONSE = "";
    @SerializedName("DESC_REASON_PEP_INDIVIDUAL")
    @Expose
    public String DESC_REASON_PEP_INDIVIDUAL = "";
    @SerializedName("PROFILE_CUST_BUSS_OCCUPATION")
    @Expose
    public String PROFILE_CUST_BUSS_OCCUPATION = "";
    @SerializedName("SENIOR_POSITION_HELD")
    @Expose
    public String SENIOR_POSITION_HELD = "";
    @SerializedName("PERIOD_SENIOR_POSITION_HELD")
    @Expose
    public String PERIOD_SENIOR_POSITION_HELD = "";
    @SerializedName("COUNTRY_SENIOR_POSITION_HELD")
    @Expose
    public String COUNTRY_SENIOR_POSITION_HELD = "";
    @SerializedName("DESC_COUNTRY_SENIOR_POSITION_HELD")
    @Expose
    public String DESC_COUNTRY_SENIOR_POSITION_HELD = "";
    @SerializedName("SOURCE_OF_FUND")
    @Expose
    public String SOURCE_OF_FUND = "";
    @SerializedName("DESC_SOURCE_OF_FUND")
    @Expose
    public String DESC_SOURCE_OF_FUND = "";
    @SerializedName("OTHER_SOURCE_OF_FUND")
    @Expose
    public String OTHER_SOURCE_OF_FUND = "";
    @SerializedName("COMMENT")
    @Expose
    public String COMMENT = "";
    @SerializedName("SOURCE_OF_WEALTH")
    @Expose
    public String SOURCE_OF_WEALTH = "";
    @SerializedName("DESC_SOURCE_OF_WEALTH")
    @Expose
    public String DESC_SOURCE_OF_WEALTH = "";
    @SerializedName("OTHER_SOURCE_OF_WEALTH")
    @Expose
    public String OTHER_SOURCE_OF_WEALTH = "";
    @SerializedName("ANALYSIS_OF_PEP")
    @Expose
    public String ANALYSIS_OF_PEP = "";
    @SerializedName("ADDITIONAL_INFO")
    @Expose
    public String ADDITIONAL_INFO = "";
    @SerializedName("ADVERSE_MEDIA_CUSTOMER")
    @Expose
    public String ADVERSE_MEDIA_CUSTOMER = "";
    @SerializedName("DETAIL_ADVERSE_MEDIA_CUSTOMER")
    @Expose
    public String DETAIL_ADVERSE_MEDIA_CUSTOMER = "";
    @SerializedName("FOREIGN_PEP")
    @Expose
    public String FOREIGN_PEP = "";
    @SerializedName("DOMESTIC_PEP")
    @Expose
    public String DOMESTIC_PEP = "";
    @SerializedName("PEP_CATG_CODE")
    @Expose
    public String PEP_CATG_CODE = "";
    @SerializedName("PEP_CATG_DESC")
    @Expose
    public String PEP_CATG_DESC = "";
    @SerializedName("PEP_SUB_CATG_CODE")
    @Expose
    public String PEP_SUB_CATG_CODE = "";

    public String getPEP_CATG_CODE() {
        return PEP_CATG_CODE;
    }

    public void setPEP_CATG_CODE(String PEP_CATG_CODE) {
        this.PEP_CATG_CODE = PEP_CATG_CODE;
    }

    public String getPEP_CATG_DESC() {
        return PEP_CATG_DESC;
    }

    public void setPEP_CATG_DESC(String PEP_CATG_DESC) {
        this.PEP_CATG_DESC = PEP_CATG_DESC;
    }

    public String getPEP_SUB_CATG_CODE() {
        return PEP_SUB_CATG_CODE;
    }

    public void setPEP_SUB_CATG_CODE(String PEP_SUB_CATG_CODE) {
        this.PEP_SUB_CATG_CODE = PEP_SUB_CATG_CODE;
    }

    public String getPEP_SUB_CATG_DESC() {
        return PEP_SUB_CATG_DESC;
    }

    public void setPEP_SUB_CATG_DESC(String PEP_SUB_CATG_DESC) {
        this.PEP_SUB_CATG_DESC = PEP_SUB_CATG_DESC;
    }

    @SerializedName("PEP_SUB_CATG_DESC")
    @Expose
    public String PEP_SUB_CATG_DESC = "";

    public String getREASONPEPINDIVIDUAL() {
        return REASON_PEP_INDIVIDUAL;
    }

    public void setREASONPEPINDIVIDUAL(String rEASONPEPINDIVIDUAL) {
        this.REASON_PEP_INDIVIDUAL = rEASONPEPINDIVIDUAL;
    }

    public String getDESCREASONPEPINDIVIDUAL() {
        return DESC_REASON_PEP_INDIVIDUAL;
    }

    public void setDESCREASONPEPINDIVIDUAL(String dESCREASONPEPINDIVIDUAL) {
        this.DESC_REASON_PEP_INDIVIDUAL = dESCREASONPEPINDIVIDUAL;
    }

    public String getPROFILECUSTBUSSOCCUPATION() {
        return PROFILE_CUST_BUSS_OCCUPATION;
    }

    public void setPROFILECUSTBUSSOCCUPATION(String pROFILECUSTBUSSOCCUPATION) {
        this.PROFILE_CUST_BUSS_OCCUPATION = pROFILECUSTBUSSOCCUPATION;
    }

    public String getSENIORPOSITIONHELD() {
        return SENIOR_POSITION_HELD;
    }

    public void setSENIORPOSITIONHELD(String sENIORPOSITIONHELD) {
        this.SENIOR_POSITION_HELD = sENIORPOSITIONHELD;
    }

    public String getPERIODSENIORPOSITIONHELD() {
        return PERIOD_SENIOR_POSITION_HELD;
    }

    public void setPERIODSENIORPOSITIONHELD(String pERIODSENIORPOSITIONHELD) {
        this.PERIOD_SENIOR_POSITION_HELD = pERIODSENIORPOSITIONHELD;
    }

    public String getCOUNTRYSENIORPOSITIONHELD() {
        return COUNTRY_SENIOR_POSITION_HELD;
    }

    public void setCOUNTRYSENIORPOSITIONHELD(String cOUNTRYSENIORPOSITIONHELD) {
        this.COUNTRY_SENIOR_POSITION_HELD = cOUNTRYSENIORPOSITIONHELD;
    }

    public String getDESCCOUNTRYSENIORPOSITIONHELD() {
        return DESC_COUNTRY_SENIOR_POSITION_HELD;
    }

    public void setDESCCOUNTRYSENIORPOSITIONHELD(String dESCCOUNTRYSENIORPOSITIONHELD) {
        this.DESC_COUNTRY_SENIOR_POSITION_HELD = dESCCOUNTRYSENIORPOSITIONHELD;
    }

    public String getSOURCEOFFUND() {
        return SOURCE_OF_FUND;
    }

    public void setSOURCEOFFUND(String sOURCEOFFUND) {
        this.SOURCE_OF_FUND = sOURCEOFFUND;
    }

    public String getDESCSOURCEOFFUND() {
        return DESC_SOURCE_OF_FUND;
    }

    public void setDESCSOURCEOFFUND(String dESCSOURCEOFFUND) {
        this.DESC_SOURCE_OF_FUND = dESCSOURCEOFFUND;
    }

    public String getOTHERSOURCEOFFUND() {
        return OTHER_SOURCE_OF_FUND;
    }

    public void setOTHERSOURCEOFFUND(String oTHERSOURCEOFFUND) {
        this.OTHER_SOURCE_OF_FUND = oTHERSOURCEOFFUND;
    }

    public String getCOMMENT() {
        return COMMENT;
    }

    public void setCOMMENT(String cOMMENT) {
        this.COMMENT = cOMMENT;
    }

    public String getSOURCEOFWEALTH() {
        return SOURCE_OF_WEALTH;
    }

    public void setSOURCEOFWEALTH(String sOURCEOFWEALTH) {
        this.SOURCE_OF_WEALTH = sOURCEOFWEALTH;
    }

    public String getDESCSOURCEOFWEALTH() {
        return DESC_SOURCE_OF_WEALTH;
    }

    public void setDESCSOURCEOFWEALTH(String dESCSOURCEOFWEALTH) {
        this.DESC_SOURCE_OF_WEALTH = dESCSOURCEOFWEALTH;
    }

    public String getOTHERSOURCEOFWEALTH() {
        return OTHER_SOURCE_OF_WEALTH;
    }

    public void setOTHERSOURCEOFWEALTH(String oTHERSOURCEOFWEALTH) {
        this.OTHER_SOURCE_OF_WEALTH = oTHERSOURCEOFWEALTH;
    }

    public String getANALYSISOFPEP() {
        return ANALYSIS_OF_PEP;
    }

    public void setANALYSISOFPEP(String aNALYSISOFPEP) {
        this.ANALYSIS_OF_PEP = aNALYSISOFPEP;
    }

    public String getADDITIONALINFO() {
        return ADDITIONAL_INFO;
    }

    public void setADDITIONALINFO(String aDDITIONALINFO) {
        this.ADDITIONAL_INFO = aDDITIONALINFO;
    }

    public String getADVERSEMEDIACUSTOMER() {
        return ADVERSE_MEDIA_CUSTOMER;
    }

    public void setADVERSEMEDIACUSTOMER(String aDVERSEMEDIACUSTOMER) {
        this.ADVERSE_MEDIA_CUSTOMER = aDVERSEMEDIACUSTOMER;
    }

    public String getDETAILADVERSEMEDIACUSTOMER() {
        return DETAIL_ADVERSE_MEDIA_CUSTOMER;
    }

    public void setDETAILADVERSEMEDIACUSTOMER(String dETAILADVERSEMEDIACUSTOMER) {
        this.DETAIL_ADVERSE_MEDIA_CUSTOMER = dETAILADVERSEMEDIACUSTOMER;
    }

    public String getFOREIGNPEP() {
        return FOREIGN_PEP;
    }

    public void setFOREIGNPEP(String fOREIGNPEP) {
        this.FOREIGN_PEP = fOREIGNPEP;
    }

    public String getDOMESTICPEP() {
        return DOMESTIC_PEP;
    }

    public void setDOMESTICPEP(String dOMESTICPEP) {
        this.DOMESTIC_PEP = dOMESTICPEP;
    }

}
