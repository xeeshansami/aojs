package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Profession() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("PROF_CODE")
    @Expose
    var PROF_CODE: String? = null

    @SerializedName("PROF_DESC")
    @Expose
    var PROF_DESC: String? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<Profession> {
        override fun createFromParcel(parcel: Parcel): Profession {
            return Profession(parcel)
        }

        override fun newArray(size: Int): Array<Profession?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return PROF_DESC.toString()
    }
}