package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface BranchCodeCallBack {
    fun BranchCodeSuccess(response: BranchCodeResponse)
    fun BranchCodeFailure(response: BaseResponse)
}