package com.hbl.bot.network.models.response.base

data class WatchListResponse(
    val watchlistMatches: List<WatchlistMatches>
)