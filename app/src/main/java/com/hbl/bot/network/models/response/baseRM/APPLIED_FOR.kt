package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class APPLIED_FOR() : Serializable, Parcelable {
    @SerializedName("ACCOUNT")
    @Expose
    val ACCOUNT: String? = null

    @SerializedName("DESC_ACCOUNT")
    @Expose
    val DESC_ACCOUNT: String? = null

    @SerializedName("CREDIT_CARD")
    @Expose
    val CREDIT_CARD: String? = null

    @SerializedName("DESC_CREDIT_CARD")
    @Expose
    val DESC_CREDIT_CARD: String? = null

    @SerializedName("LOAN")
    @Expose
    val LOAN: String? = null

    @SerializedName("DESC_LOAN")
    @Expose
    val DESC_LOAN: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<APPLIED_FOR> {
        override fun createFromParcel(parcel: Parcel): APPLIED_FOR {
            return APPLIED_FOR(parcel)
        }

        override fun newArray(size: Int): Array<APPLIED_FOR?> {
            return arrayOfNulls(size)
        }
    }


}