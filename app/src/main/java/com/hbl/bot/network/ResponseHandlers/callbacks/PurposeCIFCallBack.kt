package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PurposeCIFCallBack {
    fun PurposeCIFSuccess(response: PurposeCIFResponse)
    fun PurposeCIFFailure(response: BaseResponse)
}