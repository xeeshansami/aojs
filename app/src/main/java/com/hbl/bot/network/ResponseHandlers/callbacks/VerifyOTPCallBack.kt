package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface VerifyOTPCallBack {
    fun VerifyOTPSuccess(response: VerifyOTPResponse)
    fun VerifyOTPFailure(response: BaseResponse)
}