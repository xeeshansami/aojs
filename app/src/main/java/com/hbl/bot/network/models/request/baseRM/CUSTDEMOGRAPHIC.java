
package com.hbl.bot.network.models.request.baseRM;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CUSTDEMOGRAPHIC {

    @SerializedName("CUST_TYPE")
    @Expose
    private String cUSTTYPE;
    @SerializedName("CUST_TYPE_CIF")
    @Expose
    private String cUSTTYPECIF;
    @SerializedName("CUSTTYPEDESC")
    @Expose
    private String cUSTTYPEDESC;
    @SerializedName("NAME_OF_COMPANY")
    @Expose
    private String nAMEOFCOMPANY;
    @SerializedName("PROFESSION")
    @Expose
    private String pROFESSION;
    @SerializedName("PROFESSIONDESC")
    @Expose
    private String pROFESSIONDESC;
    @SerializedName("DESIGNATION")
    @Expose
    private String dESIGNATION;
    @SerializedName("NATURE_BUSINESS")
    @Expose
    private String nATUREBUSINESS;
    @SerializedName("NATUREBUSINESSDESC")
    @Expose
    private String nATUREBUSINESSDESC;
    @SerializedName("OTHNATBUSINESSDESC")
    @Expose
    private String oTHNATBUSINESSDESC;
    @SerializedName("EXP_MON_INCOME")
    @Expose
    private String eXPMONINCOME;
    @SerializedName("EXP_CR_TURNOVER")
    @Expose
    private String eXPCRTURNOVER;
    @SerializedName("NO_OF_CR_TRANS")
    @Expose
    private String nOOFCRTRANS;
    @SerializedName("EXP_DB_TURNOVER")
    @Expose
    private String eXPDBTURNOVER;
    @SerializedName("NO_OF_DB_TRANS")
    @Expose
    private String nOOFDBTRANS;
    @SerializedName("SOURCE_OF_INCOME")
    @Expose
    private String sOURCEOFINCOME;
    @SerializedName("SOURCEOFINCOMEDESC")
    @Expose
    private String sOURCEOFINCOMEDESC;
    @SerializedName("OTHERSOURCEOFINCOMEDESC")
    @Expose
    private String oTHERSOURCEOFINCOMEDESC;
    @SerializedName("SOURCE_OF_WEALTH")
    @Expose
    private String sOURCEOFWEALTH;
    @SerializedName("SOURCEOFWEALTHDESC")
    @Expose
    private String sOURCEOFWEALTHDESC;
    @SerializedName("OTHSRCOFWEALTHDESC")
    @Expose
    private String oTHSRCOFWEALTHDESC;
    @SerializedName("EXP_MODE_CR_TRANS")
    @Expose
    private String eXPMODECRTRANS;
    @SerializedName("EXPMODECRTRANSDESC")
    @Expose
    private String eXPMODECRTRANSDESC;
    @SerializedName("OTHEXPMODECRTRANSDESC")
    @Expose
    private Object oTHEXPMODECRTRANSDESC;
    @SerializedName("FIN_INFO_BENF_OWNER")
    @Expose
    private String fININFOBENFOWNER;
    @SerializedName("PURP_OF_CIF_CODE")
    @Expose
    private String pURPOFCIFCODE;
    @SerializedName("PURP_OF_CIF_DESC")
    @Expose
    private String pURPOFCIFDESC;
    @SerializedName("SEASONAL_ACITIVITY_IN_ACCT")
    @Expose
    private String sEASONALACITIVITYINACCT;
    @SerializedName("NATR_EXP_MTH_HIGH_TURN_OVER")
    @Expose
    private String nATREXPMTHHIGHTURNOVER;
    @SerializedName("AGRI_LAND_PASSBOOK_COPY_OBTN")
    @Expose
    private String aGRILANDPASSBOOKCOPYOBTN;
    @SerializedName("P_NUMBER")
    @Expose
    private String pNUMBER;
    @SerializedName("MODE_OF_TRANSACTION")
    @Expose
    private List<Object> mODEOFTRANSACTION = null;

    public String getCUSTTYPE() {
        return cUSTTYPE;
    }

    public void setCUSTTYPE(String cUSTTYPE) {
        this.cUSTTYPE = cUSTTYPE;
    }

    public String getCUSTTYPECIF() {
        return cUSTTYPECIF;
    }

    public void setCUSTTYPECIF(String cUSTTYPECIF) {
        this.cUSTTYPECIF = cUSTTYPECIF;
    }

    public String getCUSTTYPEDESC() {
        return cUSTTYPEDESC;
    }

    public void setCUSTTYPEDESC(String cUSTTYPEDESC) {
        this.cUSTTYPEDESC = cUSTTYPEDESC;
    }

    public String getNAMEOFCOMPANY() {
        return nAMEOFCOMPANY;
    }

    public void setNAMEOFCOMPANY(String nAMEOFCOMPANY) {
        this.nAMEOFCOMPANY = nAMEOFCOMPANY;
    }

    public String getPROFESSION() {
        return pROFESSION;
    }

    public void setPROFESSION(String pROFESSION) {
        this.pROFESSION = pROFESSION;
    }

    public String getPROFESSIONDESC() {
        return pROFESSIONDESC;
    }

    public void setPROFESSIONDESC(String pROFESSIONDESC) {
        this.pROFESSIONDESC = pROFESSIONDESC;
    }

    public String getDESIGNATION() {
        return dESIGNATION;
    }

    public void setDESIGNATION(String dESIGNATION) {
        this.dESIGNATION = dESIGNATION;
    }

    public String getNATUREBUSINESS() {
        return nATUREBUSINESS;
    }

    public void setNATUREBUSINESS(String nATUREBUSINESS) {
        this.nATUREBUSINESS = nATUREBUSINESS;
    }

    public String getNATUREBUSINESSDESC() {
        return nATUREBUSINESSDESC;
    }

    public void setNATUREBUSINESSDESC(String nATUREBUSINESSDESC) {
        this.nATUREBUSINESSDESC = nATUREBUSINESSDESC;
    }

    public String getOTHNATBUSINESSDESC() {
        return oTHNATBUSINESSDESC;
    }

    public void setOTHNATBUSINESSDESC(String oTHNATBUSINESSDESC) {
        this.oTHNATBUSINESSDESC = oTHNATBUSINESSDESC;
    }

    public String getEXPMONINCOME() {
        return eXPMONINCOME;
    }

    public void setEXPMONINCOME(String eXPMONINCOME) {
        this.eXPMONINCOME = eXPMONINCOME;
    }

    public String getEXPCRTURNOVER() {
        return eXPCRTURNOVER;
    }

    public void setEXPCRTURNOVER(String eXPCRTURNOVER) {
        this.eXPCRTURNOVER = eXPCRTURNOVER;
    }

    public String getNOOFCRTRANS() {
        return nOOFCRTRANS;
    }

    public void setNOOFCRTRANS(String nOOFCRTRANS) {
        this.nOOFCRTRANS = nOOFCRTRANS;
    }

    public String getEXPDBTURNOVER() {
        return eXPDBTURNOVER;
    }

    public void setEXPDBTURNOVER(String eXPDBTURNOVER) {
        this.eXPDBTURNOVER = eXPDBTURNOVER;
    }

    public String getNOOFDBTRANS() {
        return nOOFDBTRANS;
    }

    public void setNOOFDBTRANS(String nOOFDBTRANS) {
        this.nOOFDBTRANS = nOOFDBTRANS;
    }

    public String getSOURCEOFINCOME() {
        return sOURCEOFINCOME;
    }

    public void setSOURCEOFINCOME(String sOURCEOFINCOME) {
        this.sOURCEOFINCOME = sOURCEOFINCOME;
    }

    public String getSOURCEOFINCOMEDESC() {
        return sOURCEOFINCOMEDESC;
    }

    public void setSOURCEOFINCOMEDESC(String sOURCEOFINCOMEDESC) {
        this.sOURCEOFINCOMEDESC = sOURCEOFINCOMEDESC;
    }

    public String getOTHERSOURCEOFINCOMEDESC() {
        return oTHERSOURCEOFINCOMEDESC;
    }

    public void setOTHERSOURCEOFINCOMEDESC(String oTHERSOURCEOFINCOMEDESC) {
        this.oTHERSOURCEOFINCOMEDESC = oTHERSOURCEOFINCOMEDESC;
    }

    public String getSOURCEOFWEALTH() {
        return sOURCEOFWEALTH;
    }

    public void setSOURCEOFWEALTH(String sOURCEOFWEALTH) {
        this.sOURCEOFWEALTH = sOURCEOFWEALTH;
    }

    public String getSOURCEOFWEALTHDESC() {
        return sOURCEOFWEALTHDESC;
    }

    public void setSOURCEOFWEALTHDESC(String sOURCEOFWEALTHDESC) {
        this.sOURCEOFWEALTHDESC = sOURCEOFWEALTHDESC;
    }

    public String getOTHSRCOFWEALTHDESC() {
        return oTHSRCOFWEALTHDESC;
    }

    public void setOTHSRCOFWEALTHDESC(String oTHSRCOFWEALTHDESC) {
        this.oTHSRCOFWEALTHDESC = oTHSRCOFWEALTHDESC;
    }

    public String getEXPMODECRTRANS() {
        return eXPMODECRTRANS;
    }

    public void setEXPMODECRTRANS(String eXPMODECRTRANS) {
        this.eXPMODECRTRANS = eXPMODECRTRANS;
    }

    public String getEXPMODECRTRANSDESC() {
        return eXPMODECRTRANSDESC;
    }

    public void setEXPMODECRTRANSDESC(String eXPMODECRTRANSDESC) {
        this.eXPMODECRTRANSDESC = eXPMODECRTRANSDESC;
    }

    public Object getOTHEXPMODECRTRANSDESC() {
        return oTHEXPMODECRTRANSDESC;
    }

    public void setOTHEXPMODECRTRANSDESC(Object oTHEXPMODECRTRANSDESC) {
        this.oTHEXPMODECRTRANSDESC = oTHEXPMODECRTRANSDESC;
    }

    public String getFININFOBENFOWNER() {
        return fININFOBENFOWNER;
    }

    public void setFININFOBENFOWNER(String fININFOBENFOWNER) {
        this.fININFOBENFOWNER = fININFOBENFOWNER;
    }

    public String getPURPOFCIFCODE() {
        return pURPOFCIFCODE;
    }

    public void setPURPOFCIFCODE(String pURPOFCIFCODE) {
        this.pURPOFCIFCODE = pURPOFCIFCODE;
    }

    public String getPURPOFCIFDESC() {
        return pURPOFCIFDESC;
    }

    public void setPURPOFCIFDESC(String pURPOFCIFDESC) {
        this.pURPOFCIFDESC = pURPOFCIFDESC;
    }

    public String getSEASONALACITIVITYINACCT() {
        return sEASONALACITIVITYINACCT;
    }

    public void setSEASONALACITIVITYINACCT(String sEASONALACITIVITYINACCT) {
        this.sEASONALACITIVITYINACCT = sEASONALACITIVITYINACCT;
    }

    public String getNATREXPMTHHIGHTURNOVER() {
        return nATREXPMTHHIGHTURNOVER;
    }

    public void setNATREXPMTHHIGHTURNOVER(String nATREXPMTHHIGHTURNOVER) {
        this.nATREXPMTHHIGHTURNOVER = nATREXPMTHHIGHTURNOVER;
    }

    public String getAGRILANDPASSBOOKCOPYOBTN() {
        return aGRILANDPASSBOOKCOPYOBTN;
    }

    public void setAGRILANDPASSBOOKCOPYOBTN(String aGRILANDPASSBOOKCOPYOBTN) {
        this.aGRILANDPASSBOOKCOPYOBTN = aGRILANDPASSBOOKCOPYOBTN;
    }

    public String getPNUMBER() {
        return pNUMBER;
    }

    public void setPNUMBER(String pNUMBER) {
        this.pNUMBER = pNUMBER;
    }

    public List<Object> getMODEOFTRANSACTION() {
        return mODEOFTRANSACTION;
    }

    public void setMODEOFTRANSACTION(List<Object> mODEOFTRANSACTION) {
        this.mODEOFTRANSACTION = mODEOFTRANSACTION;
    }

}
