package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class TitleHR(callBack: TitleCallBack) : BaseRH<TitleResponse>() {
    var callback: TitleCallBack = callBack
    override fun onSuccess(response: TitleResponse?) {
        response?.let { callback.TitleSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.TitleFailure(it) }
    }
}