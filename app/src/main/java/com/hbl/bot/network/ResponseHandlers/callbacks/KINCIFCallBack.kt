package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface KINCIFCallBack {
    fun KINCIFSuccess(response: KINAccountInfoResponse)
    fun KINCIFFailure(response: BaseResponse)
}