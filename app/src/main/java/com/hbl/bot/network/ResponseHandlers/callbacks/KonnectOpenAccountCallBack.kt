package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface KonnectOpenAccountCallBack {
    fun konnectOpenAccountSuccess(response: KonnectOpenAccountResponse)
    fun konnectOpenAccountFailure(response: BaseResponse)
}