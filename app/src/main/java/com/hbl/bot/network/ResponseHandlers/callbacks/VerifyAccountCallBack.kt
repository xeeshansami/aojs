package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface VerifyAccountCallBack {
    fun VerifyAccountSuccess(response: VerifyAccountResponse)
    fun VerifyAccountFailure(response: BaseResponse)
}