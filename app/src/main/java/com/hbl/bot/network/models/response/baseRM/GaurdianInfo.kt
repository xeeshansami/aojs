package com.hbl.bot.network.models.response.baseRM


import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class GaurdianInfo() : Parcelable {
    @SerializedName("CIF")
    var cIF: String = ""
    @SerializedName("CIF_STATUS")
    var cIFSTATUS: String = ""
    @SerializedName("CNIC")
    var cNIC: String = ""
    @SerializedName("CUST_MOBILE")
    var cUSTMOBILE: String = ""
    @SerializedName("CUST_TYPE")
    var cUSTTYPE: String = ""
    @SerializedName("NAME")
    var nAME: String = ""
    @SerializedName("RESPONSE_MSG")
    var rESPONSEMSG: String = ""

    constructor(parcel: Parcel) : this() {
        cIF = parcel.readString().toString()
        cIFSTATUS = parcel.readString().toString()
        cNIC = parcel.readString().toString()
        cUSTMOBILE = parcel.readString().toString()
        cUSTTYPE = parcel.readString().toString()
        nAME = parcel.readString().toString()
        rESPONSEMSG = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(cIF)
        parcel.writeString(cIFSTATUS)
        parcel.writeString(cNIC)
        parcel.writeString(cUSTMOBILE)
        parcel.writeString(cUSTTYPE)
        parcel.writeString(nAME)
        parcel.writeString(rESPONSEMSG)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<GaurdianInfo> {
        override fun createFromParcel(parcel: Parcel): GaurdianInfo {
            return GaurdianInfo(parcel)
        }

        override fun newArray(size: Int): Array<GaurdianInfo?> {
            return arrayOfNulls(size)
        }
    }
}