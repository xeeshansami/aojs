package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface KonnectPurposeOfAccountCallBack {
    fun PurposeOfAccountSuccess(response: KonnectPurposeOfAccountResponse)
    fun PurposeOfAccountFailure(response: BaseResponse)
}