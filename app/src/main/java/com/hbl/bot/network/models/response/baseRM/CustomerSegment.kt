package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CustomerSegment() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    var id: String? = null

    @SerializedName("CUST_SEGMENT")
    @Expose
    var cUSTSEGMENT: String? = null

    @SerializedName("SEGMENT_DESC")
    @Expose
    var sEGMENTDESC: String? = null

    @SerializedName("CUST_TYPE")
    @Expose
    val cUSTTYPE: String? = null

    @SerializedName("INDVI_NINDVI")
    @Expose
    val iNDVININDVI: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<CustomerSegment> {
        override fun createFromParcel(parcel: Parcel): CustomerSegment {
            return CustomerSegment(parcel)
        }

        override fun newArray(size: Int): Array<CustomerSegment?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return sEGMENTDESC.toString()
    }
}