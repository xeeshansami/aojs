package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface BranchRegionCallBack {
    fun Success(response: BranchRegionResponse)
    fun Failure(response: BaseResponse)
}