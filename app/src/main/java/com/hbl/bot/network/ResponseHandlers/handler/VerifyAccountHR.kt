package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.VerifyAccountCallBack
import com.hbl.bot.network.models.response.base.*

class VerifyAccountHR(callBack: VerifyAccountCallBack) : BaseRH<VerifyAccountResponse>() {
    var callback: VerifyAccountCallBack = callBack
    override fun onSuccess(response: VerifyAccountResponse?) {
        response?.let { callback.VerifyAccountSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.VerifyAccountFailure(it) }
    }
}