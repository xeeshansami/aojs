package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class SourceOffund() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("SOF_CODE")
    @Expose
    var SOF_CODE: String? = null

    @SerializedName("SOF_DESC")
    @Expose
    var SOF_DESC: String? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<SourceOffund> {
        override fun createFromParcel(parcel: Parcel): SourceOffund {
            return SourceOffund(parcel)
        }

        override fun newArray(size: Int): Array<SourceOffund?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return SOF_DESC.toString()
    }
}