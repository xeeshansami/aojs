package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class SBPCodes() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("SBP_CODE")
    @Expose
    var SBP_CODE: String? = null

    @SerializedName("SBP_DESC")
    @Expose
    var SBP_DESC: String? = null

    @SerializedName("GPILM")
    @Expose
    val GPILM: String? = null

    @SerializedName("GPMIL")
    @Expose
    val GPMIL: String? = null

    @SerializedName("GPTMR")
    @Expose
    val GPTMR: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<SBPCodes> {
        override fun createFromParcel(parcel: Parcel): SBPCodes {
            return SBPCodes(parcel)
        }

        override fun newArray(size: Int): Array<SBPCodes?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return SBP_DESC.toString()
    }
}