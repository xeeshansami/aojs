package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class BranchCodeHR(callBack: BranchCodeCallBack) : BaseRH<BranchCodeResponse>() {
    var callback: BranchCodeCallBack = callBack
    override fun onSuccess(response: BranchCodeResponse?) {
        response?.let { callback.BranchCodeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.BranchCodeFailure(it) }
    }
}