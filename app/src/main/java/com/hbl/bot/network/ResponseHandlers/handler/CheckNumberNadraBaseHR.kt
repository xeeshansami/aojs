package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.CheckNumberNadraCallBack
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.CheckNumberNadraResponse

class CheckNumberNadraBaseHR(callBack: CheckNumberNadraCallBack) : BaseRH<CheckNumberNadraResponse>() {
    var callback: CheckNumberNadraCallBack = callBack
    override fun onSuccess(response: CheckNumberNadraResponse?) {
        response?.let { callback.CheckNumberNadraSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CheckNumberNadraFailure(it) }
    }
}