package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class BusinessArea(
) : Parcelable {
    var BR_CODE = ""
    var BR_NAME = ""
    var BUSINESS_AREA = ""
    var REG_CODE = ""
    var REG_NAME = ""
    var _id: String = ""

    constructor(parcel: Parcel) : this() {
        BR_CODE = parcel.readString().toString()
        BR_NAME = parcel.readString().toString()
        BUSINESS_AREA = parcel.readString().toString()
        REG_CODE = parcel.readString().toString()
        REG_NAME = parcel.readString().toString()
        _id = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(BR_CODE)
        parcel.writeString(BR_NAME)
        parcel.writeString(BUSINESS_AREA)
        parcel.writeString(REG_CODE)
        parcel.writeString(REG_NAME)
        parcel.writeString(_id)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BusinessArea> {
        override fun createFromParcel(parcel: Parcel): BusinessArea {
            return BusinessArea(parcel)
        }

        override fun newArray(size: Int): Array<BusinessArea?> {
            return arrayOfNulls(size)
        }
    }
}