package com.hbl.bot.network.models.response.base

import com.hbl.bot.network.models.response.baseRM.MySISRef

class MYSISREFResponse(
) {
    var data = arrayListOf<MySISRef>()
    var message = ""
    var status = ""
}