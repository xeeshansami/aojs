package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class HTMLHR(callBack: HTMLCallBack) : BaseRH<HTMLResponse>() {
    var callback: HTMLCallBack = callBack
    override fun onSuccess(response: HTMLResponse?) {
        response?.let { callback.HTMLSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.HTMLFailure(it) }
    }
}