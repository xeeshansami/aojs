package com.hbl.bot.network.ViewModels

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.hbl.bot.network.Repositories.DashboardRepository
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.response.base.*

class DashboardViewModel() : ViewModel() {
    var dashboardRepository = DashboardRepository()
    var trackingIDLiveData: LiveData<TrackingIDResponse>? = null
    var workFlowLiveData: LiveData<WorkFlowResponse>? = null
    var mySiSREFLiveData: LiveData<MYSISREFResponse>? = null
    var businessAreaLiveData: LiveData<BusinessAreaResponse>? = null

    /*call trackingID Live Data*/
    fun getTrackingIDLiveData(
        request: TrackingIDRequest,
        activity: FragmentActivity?
    ): LiveData<TrackingIDResponse>? {
        if (trackingIDLiveData == null) {
            return dashboardRepository.getTrackingID(request, activity)
        } else {
            return trackingIDLiveData
        }
    }

    /*call WorkFlow Live Data*/
    fun getWorkFlowLiveData(
        request: LovRequest,
        activity: FragmentActivity?
    ): LiveData<WorkFlowResponse>? {
        if (workFlowLiveData == null) {
            return dashboardRepository.getWorkFlow(request, activity)
        } else {
            return workFlowLiveData
        }
    }

    /*call MySisRef Live Data*/
    fun getMySisRef(
        request: LovRequest,
        activity: FragmentActivity?
    ): LiveData<MYSISREFResponse>? {
        if (mySiSREFLiveData == null) {
            return dashboardRepository.getMYSISRef(request, activity)
        } else {
            return mySiSREFLiveData
        }
    }

    /*call BusinessArea Live Data*/
    fun getBusinessArea(
        request: BusinessAreaRequest,
        activity: FragmentActivity?
    ): LiveData<BusinessAreaResponse>? {
        if (businessAreaLiveData == null) {
            return dashboardRepository.getBusinessArea(request, activity)
        } else {
            return businessAreaLiveData
        }
    }

}