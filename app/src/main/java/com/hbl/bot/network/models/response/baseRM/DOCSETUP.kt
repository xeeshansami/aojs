package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class DOCSETUP(
) : Parcelable {
     var COUNT: Int=0
     var DOCID: String=""

    constructor(parcel: Parcel) : this() {
        COUNT = parcel.readInt()
        DOCID = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(COUNT)
        parcel.writeString(DOCID)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DOCSETUP> {
        override fun createFromParcel(parcel: Parcel): DOCSETUP {
            return DOCSETUP(parcel)
        }

        override fun newArray(size: Int): Array<DOCSETUP?> {
            return arrayOfNulls(size)
        }
    }
}