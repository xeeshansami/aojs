package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.SignatureCallBack
import com.hbl.bot.network.models.response.base.*

class SignatureHR(callBack: SignatureCallBack) : BaseRH<SignatureResponse>() {
    var callback: SignatureCallBack = callBack
    override fun onSuccess(response: SignatureResponse?) {
        response?.let { callback.Success(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.Failure(it) }
    }
}