package com.hbl.bot.network.models.response.baseRM;

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class LoginData() : Serializable, Parcelable {
    @SerializedName("IS_KONNECT_BRANCH")
    @Expose
    var IS_KONNECT_BRANCH = "0"

    @SerializedName("IS_HAW_USER")
    @Expose
    var IS_HAW_USER = "0"

    @SerializedName("USER_BRANCH_SELECTION")
    @Expose
    var USER_BRANCH_SELECTION = "0"

    @SerializedName("HIGH_RISK_BRANCH")
    @Expose
    var HIGH_RISK_BRANCH = ""

    @SerializedName("user_email")
    @Expose
    var user_email = ""

    @SerializedName("user_last_login_time")
    @Expose
    var user_last_login_time = ""

    @SerializedName("user_full_name")
    @Expose
    var user_full_name = ""

    @SerializedName("app")
    @Expose
    var app = ""

    @SerializedName("user_login")
    @Expose
    var user_login = ""

    @SerializedName("USER_ID")
    @Expose
    private var uSERID = ""

    @SerializedName("BRANCH_CODE")
    @Expose
    private var bRANCHCODE = ""

    @SerializedName("BRANCH_NAME")
    @Expose
    private var bRANCHNAME = ""

    @SerializedName("BRANCH_ADDRESS")
    @Expose
    private var bRANCHADDRESS = ""

    @SerializedName("BRANCH_TYPE")
    @Expose
    private var bRANCHTYPE = ""

    @SerializedName("ROLE")
    @Expose
    private var rOLE = 0

/*	@SerializedName("ROLE_NAME")
	@Expose
	private var rOLENAME: List<RoleName?>? = null*/

    @SerializedName("COD")
    @Expose
    private var cOD = ""

    @SerializedName("MISYS_RESP_DESC")
    @Expose
    private var mISYSRESPDESC = ""

    @SerializedName("TOKEN")
    @Expose
    private var tOKEN = ""

    @SerializedName("REFRESH_TOKEN")
    @Expose
    private var rEFRESHTOKEN = ""

    @SerializedName("USER_REGION_CODE")
    @Expose
    private var uSERREGIONCODE = ""

    @SerializedName("USER_REGION_DESC")
    @Expose
    private var uSERREGIONDESC = ""

    @SerializedName("MAKER_CHECKER")
    @Expose
    private var mAKERCHECKER = ""

    fun getUSERID(): String? {
        return uSERID
    }

    fun setUSERID(uSERID: String) {
        this.uSERID = uSERID
    }

    fun getBRANCHCODE(): String? {
        return bRANCHCODE
    }

    fun setBRANCHCODE(bRANCHCODE: String) {
        this.bRANCHCODE = bRANCHCODE
    }

    fun getBRANCHNAME(): String? {
        return bRANCHNAME
    }

    fun setBRANCHNAME(bRANCHNAME: String) {
        this.bRANCHNAME = bRANCHNAME
    }

    fun getBRANCHADDRESS(): String? {
        return bRANCHADDRESS
    }

    fun setBRANCHADDRESS(bRANCHADDRESS: String) {
        this.bRANCHADDRESS = bRANCHADDRESS
    }

    fun getBRANCHTYPE(): String {
        return bRANCHTYPE
    }

    fun setBRANCHTYPE(bRANCHTYPE: String) {
        this.bRANCHTYPE = bRANCHTYPE
    }

    fun getROLE(): Int? {
        return rOLE
    }

    fun setROLE(rOLE: Int) {
        this.rOLE = rOLE
    }

    /*fun getROLENAME(): List<RoleName?>? {
        return rOLENAME
    }

    fun setROLENAME(rOLENAME: List<RoleName?>?) {
        this.rOLENAME = rOLENAME
    }*/

    fun getCOD(): String {
        return cOD
    }

    fun setCOD(cOD: String) {
        this.cOD = cOD
    }

    fun getMISYSRESPDESC(): String {
        return mISYSRESPDESC
    }

    fun setMISYSRESPDESC(mISYSRESPDESC: String) {
        this.mISYSRESPDESC = mISYSRESPDESC
    }

    fun getTOKEN(): String {
        return tOKEN
    }

    fun setTOKEN(tOKEN: String) {
        this.tOKEN = tOKEN
    }

    fun getREFRESHTOKEN(): String {
        return rEFRESHTOKEN
    }

    fun setREFRESHTOKEN(rEFRESHTOKEN: String) {
        this.rEFRESHTOKEN = rEFRESHTOKEN
    }

    fun getUSERREGIONCODE(): String {
        return uSERREGIONCODE
    }

    fun setUSERREGIONCODE(uSERREGIONCODE: String) {
        this.uSERREGIONCODE = uSERREGIONCODE
    }

    fun getUSERREGIONDESC(): String {
        return uSERREGIONDESC
    }

    fun setUSERREGIONDESC(uSERREGIONDESC: String) {
        this.uSERREGIONDESC = uSERREGIONDESC
    }

    fun getMAKERCHECKER(): String {
        return mAKERCHECKER
    }

    fun setMAKERCHECKER(mAKERCHECKER: String) {
        this.mAKERCHECKER = mAKERCHECKER
    }

    constructor(source: Parcel) : this(
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<LoginData> = object : Parcelable.Creator<LoginData> {
            override fun createFromParcel(source: Parcel): LoginData = LoginData(source)
            override fun newArray(size: Int): Array<LoginData?> = arrayOfNulls(size)
        }
    }
}