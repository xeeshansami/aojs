package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface UrlCallBack {
    fun Success(response: UrlResponse)
    fun Failure(response: BaseResponse)
}