package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class SubmitData() : Parcelable {
    var Tracking_id: String? = null
    var CIF_NO: String? = null
    var ACCT_NO: String? = null
    var IBAN: String? = null

    constructor(parcel: Parcel) : this() {
        Tracking_id = parcel.readString()
        CIF_NO = parcel.readString()
        ACCT_NO = parcel.readString()
        IBAN = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(Tracking_id)
        parcel.writeString(CIF_NO)
        parcel.writeString(ACCT_NO)
        parcel.writeString(IBAN)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SubmitData> {
        override fun createFromParcel(parcel: Parcel): SubmitData {
            return SubmitData(parcel)
        }

        override fun newArray(size: Int): Array<SubmitData?> {
            return arrayOfNulls(size)
        }
    }
}