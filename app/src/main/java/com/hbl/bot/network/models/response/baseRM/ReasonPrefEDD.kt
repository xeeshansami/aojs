package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ReasonPrefEDD() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("REASON_CODE")
    @Expose
    var REASON_CODE: String? = null

    @SerializedName("REASON_DESC")
    @Expose
    var REASON_DESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<ReasonPrefEDD> {
        override fun createFromParcel(parcel: Parcel): ReasonPrefEDD {
            return ReasonPrefEDD(parcel)
        }

        override fun newArray(size: Int): Array<ReasonPrefEDD?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return REASON_DESC.toString()
    }
}