package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class PartialUploadHR(callBack: PartialUploadCallBack) : BaseRH<PartialUploadResponse>() {
    var callback: PartialUploadCallBack = callBack
    override fun onSuccess(response: PartialUploadResponse?) {
        response?.let { callback.PartialUploadSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PartialUploadFailure(it) }
    }
}