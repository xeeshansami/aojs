package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.PepCategoriesCallBack
import com.hbl.bot.network.models.response.base.*

class PepCategoriesHR(callBack: PepCategoriesCallBack) : BaseRH<PepCategoriesResponse>() {
    var callback: PepCategoriesCallBack = callBack
    override fun onSuccess(response: PepCategoriesResponse?) {
        response?.let { callback.PepCategoriesuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PepCategoriesFailure(it) }
    }
}