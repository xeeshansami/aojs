package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class SundryHR(callBack: SundryCallBack) : BaseRH<SundryResponse>() {
    var callback: SundryCallBack = callBack
    override fun onSuccess(response: SundryResponse?) {
        response?.let { callback.SundrySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.SundryFailure(it) }
    }
}