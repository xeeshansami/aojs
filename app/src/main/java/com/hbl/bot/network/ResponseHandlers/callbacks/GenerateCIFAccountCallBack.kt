package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface GenerateCIFAccountCallBack {
    fun GenerateCIFAccountSuccess(response: GenerateCIFAccountResponse)
    fun GenerateCIFAccountFailure(response: BaseResponse)
}