package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.hbl.bot.network.models.request.baseRM.Find

class BusinessAreaRequest(
) : Parcelable {
    var find: Find = Find()
    var identifier = ""

    constructor(parcel: Parcel) : this() {
        identifier = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(identifier)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BusinessAreaRequest> {
        override fun createFromParcel(parcel: Parcel): BusinessAreaRequest {
            return BusinessAreaRequest(parcel)
        }

        override fun newArray(size: Int): Array<BusinessAreaRequest?> {
            return arrayOfNulls(size)
        }
    }
}