package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class IndustrySubCategoryHR(callBack: IndustrySubCategoryBack) :
    BaseRH<IndustrySubCategoryResponse>() {
    var callback: IndustrySubCategoryBack = callBack
    override fun onSuccess(response: IndustrySubCategoryResponse?) {
        response?.let { callback.IndustrySubCategorySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.IndustrySubCategoryyFailure(it) }
    }
}