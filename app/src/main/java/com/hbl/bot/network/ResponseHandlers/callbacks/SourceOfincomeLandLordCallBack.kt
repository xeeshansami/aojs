package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface SourceOfincomeLandLordCallBack {
    fun SourceOfincomeLandLordSuccess(response: SourceOffundIncomeResponse)
    fun SourceOfincomeLandLordFailure(response: BaseResponse)
}