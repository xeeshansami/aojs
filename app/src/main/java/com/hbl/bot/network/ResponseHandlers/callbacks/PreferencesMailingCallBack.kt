package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PreferencesMailingCallBack {
    fun PreferencesMailingSuccess(response: PreferencesMailingResponse)
    fun PreferencesMailingFailure(response: BaseResponse)
}