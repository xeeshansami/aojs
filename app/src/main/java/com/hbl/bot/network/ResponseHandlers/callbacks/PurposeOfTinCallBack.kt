package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PurposeOfTinCallBack {
    fun PurposeOfTinSuccess(response: PurposeOfTinResponse)
    fun PurposeOfTinFailure(response: BaseResponse)
}