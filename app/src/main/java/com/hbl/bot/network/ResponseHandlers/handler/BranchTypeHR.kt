package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class BranchTypeHR(callBack: BranchTypeCallBack) : BaseRH<BranchTypeResponse>() {
    var callback: BranchTypeCallBack = callBack
    override fun onSuccess(response: BranchTypeResponse?) {
        response?.let { callback.BranchTypeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.BranchTypeFailure(it) }
    }
}