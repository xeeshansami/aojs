package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class CountHR(callBack: CountCallBack) : BaseRH<CountResponse>() {
    var callback: CountCallBack = callBack
    override fun onSuccess(response: CountResponse?) {
        response?.let { callback.CountSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CountFailure(it) }
    }
}