package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class KonnectBranchCityHR(callBack: KonnectBranchCityCallBack) :
    BaseRH<KonnectBranchCityResponse>() {
    var callback: KonnectBranchCityCallBack = callBack
    override fun onSuccess(response: KonnectBranchCityResponse?) {
        response?.let { callback.konnectBranchCitySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.konnectBranchCityFailure(it) }
    }
}