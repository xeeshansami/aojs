package com.hbl.bot.network.ResponseHandlers.handler

import android.annotation.SuppressLint
import android.content.Intent
import android.util.Log
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.ToastUtils
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.SocketTimeoutException


abstract class BaseRH<T> : Callback<T> {
    var status = ""
    var message = ""
    var ETBNTBFLAG = ""
    val globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass

    @SuppressLint("WrongConstant")
    override fun onResponse(call: Call<T>, response: Response<T>) {
        try {
            if (response.isSuccessful) {//200
                val body = response.body()
                var baseResponse = Gson().toJson(body)
                var jsonResponse = JSONObject(baseResponse)
                status = jsonResponse.get("status") as String
                message = jsonResponse.get("message") as String
                if (status.equals("00") || status.equals("02")) {
                    globalClass.statusCode = status
                    onSuccess(body)
                } else if (status.equals("97")) {
                    globalClass.statusCode = status
                    // 97 Session Expire and move to logout //
                    //globalClass.getLogoutData();
                    ToastUtils.normalShowToast(GlobalClass.applicationContext, message,3)
                    globalClass.hideLoader();
                    sendStatus()
                } else if (status.equals("01") && message.equals("This tracking id already found in db, please create new record again.")) {
                    /*When submit identifier api response error  "This tracking id already found in db, please create new record again."
                    then move to update api response call*/
                    globalClass.statusCode = status
                    onSuccess(body)
                    sendStatus()
                }else if (status.equals("01") && message.equals("Already have an Account")) {
                    /*When submit identifier api response error  "This tracking id already found in db, please create new record again."
                    then move to update api response call*/
                    globalClass.statusCode = status
                    onSuccess(body)
                    sendStatus()
                }else if (status.equals("01")&&message.equals("This device is not registered, please re-install the application")) {
                    /*When submit identifier api response error  "This tracking id already found in db, please create new record again."
                    then move to update api response call*/
//                    ToastUtils.normalShowToast(GlobalClass.applicationContext, message,3)
                    globalClass.hideLoader();
                    onSuccess(body)
                    sendStatus()
                }else if (status.equals("01")) {
                    /*When submit identifier api response error  "This tracking id already found in db, please create new record again."
                    then move to update api response call*/
                    ToastUtils.normalShowToast(GlobalClass.applicationContext, message,3)
                    globalClass.hideLoader();
                    sendStatus()
                } else if (status.equals("99")) { /*99 catch error*/
                    sendStatus()
                    ToastUtils.normalShowToast(
                        GlobalClass.applicationContext,
                        globalClass.resources.getString(R.string.request_failed_db_error),3
                    )
                    globalClass.statusCode = status
                    globalClass.hideLoader();
                } else {
                    sendStatus()
                    ToastUtils.normalShowToast(GlobalClass.applicationContext, message,3)
                    globalClass.statusCode = status
                    globalClass.hideLoader();
                }
            } else {
                sendStatus()
                ToastUtils.normalShowToast(
                    GlobalClass.applicationContext,
                    globalClass.resources.getString(R.string.something_went_wrong),3
                )
                globalClass.statusCode = status
                globalClass.hideLoader();
            }
        } catch (ex: Exception) {
            sendStatus()
            ToastUtils.normalShowToast(
                GlobalClass.applicationContext,
                globalClass.resources.getString(R.string.something_went_wrong),3
            )
            globalClass.hideLoader();
            ToastUtils.normalShowToast(
                GlobalClass.applicationContext,
                globalClass.resources.getString(R.string.something_went_wrong),3
            )
        }

    }

    private fun sendStatus() {
        val intent = Intent(Constants.STATUS_BROADCAST)
        // You can also include some extra data.
        LocalBroadcastManager.getInstance(globalClass).sendBroadcast(intent)
    }

    override fun onFailure(call: Call<T>, t: Throwable) {
        t.message?.let { Log.i("JsonError", it) }
        if (t is SocketTimeoutException || t.message!!.contains("timeout")) { /*When server no response in 30 seconds*/
            onFailure(
                BaseResponse(
                    "-1",
                    "Connection timeout, kindly make sure your network connection should be worked fine and please try again"
                )
            )
        } else if (t.message!!.contains(" Unable to resolve")) {
            onFailure(
                BaseResponse(
                    "-1",
                    "Internet Connectivity Issue, kindly make sure your network connection should be worked fine and please try again"
                )
            )
        } else { /*When something unexpected error occurred.*/
            onFailure(
                BaseResponse(
                    "-1",
                    "Something went wrong, please try again later."
                )
            )
        }
    }

    protected abstract fun onSuccess(response: T?)
    protected abstract fun onFailure(response: BaseResponse?)
}