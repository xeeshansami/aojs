package com.hbl.bot.network.models.request.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class DebitCardFind(
) : Parcelable {
    @SerializedName("PRD_ACTP")
    var PRD_ACTP: String? = null

    constructor(parcel: Parcel) : this() {
        PRD_ACTP = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(PRD_ACTP)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<DebitCardFind> {
        override fun createFromParcel(parcel: Parcel): DebitCardFind {
            return DebitCardFind(parcel)
        }

        override fun newArray(size: Int): Array<DebitCardFind?> {
            return arrayOfNulls(size)
        }
    }
}