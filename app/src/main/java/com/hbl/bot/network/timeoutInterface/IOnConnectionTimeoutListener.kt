package com.hbl.bot.network.timeoutInterface

/**
 */
interface IOnConnectionTimeoutListener {
    fun onConnectionTimeout()
}