package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class AccountType() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    var _id: String? = null

    @SerializedName("CUST_SEGMENT")
    @Expose
    var CUST_SEGMENT: String? = null

    @SerializedName("SEGMENT_DESC")
    @Expose
    var SEGMENT_DESC: String? = null

    @SerializedName("ACCT_TYPE")
    @Expose
    var ACCT_TYPE: String? = null

    @SerializedName("ACCT_TYPE_DESC")
    @Expose
    var ACCT_TYPE_DESC: String? = null

    @SerializedName("Convential_Islamic")
    @Expose
    var Convential_Islamic: String? = null

    @SerializedName("ACCT_CCY")
    @Expose
    var ACCT_CCY: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<AccountType> {
        override fun createFromParcel(parcel: Parcel): AccountType {
            return AccountType(parcel)
        }

        override fun newArray(size: Int): Array<AccountType?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return ACCT_TYPE_DESC.toString()
    }
}