package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class AccountOperationsTypeHR(callBack: AccountOperationsTypeCallBack) :
    BaseRH<AccountOperationsTypeResponse>() {
    var callback: AccountOperationsTypeCallBack = callBack
    override fun onSuccess(response: AccountOperationsTypeResponse?) {
        response?.let { callback.AccountOperationsTypeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.AccountOperationsTypeFailure(it) }
    }
}