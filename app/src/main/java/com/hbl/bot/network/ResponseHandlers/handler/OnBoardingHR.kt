package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class OnBoardingHR(callBack: OnBoardingCallBack) : BaseRH<OnBoardingResponse>() {
    var callback: OnBoardingCallBack = callBack
    override fun onSuccess(response: OnBoardingResponse?) {
        response?.let { callback.OnBoardingSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.OnBoardingFailure(it) }
    }
}