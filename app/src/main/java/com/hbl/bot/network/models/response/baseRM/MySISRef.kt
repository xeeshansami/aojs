package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable

class MySISRef(
) : Parcelable {
    var MYSIS_REF = ""

    constructor(parcel: Parcel) : this() {
        MYSIS_REF = parcel.readString().toString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(MYSIS_REF)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<MySISRef> {
        override fun createFromParcel(parcel: Parcel): MySISRef {
            return MySISRef(parcel)
        }

        override fun newArray(size: Int): Array<MySISRef?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return MYSIS_REF.toString()
    }
}