package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class KonnectPurposeOfAccountHR(callBack: KonnectPurposeOfAccountCallBack) : BaseRH<KonnectPurposeOfAccountResponse>() {
    var callback: KonnectPurposeOfAccountCallBack = callBack
    override fun onSuccess(response: KonnectPurposeOfAccountResponse?) {
        response?.let { callback.PurposeOfAccountSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PurposeOfAccountFailure(it) }
    }
}