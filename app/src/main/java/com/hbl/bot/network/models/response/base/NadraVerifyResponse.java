
package com.hbl.bot.network.models.response.base;

import java.io.Serializable;
import java.util.List;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.hbl.bot.network.models.response.baseRM.NadraVerify;

public class NadraVerifyResponse implements Serializable, Parcelable {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<NadraVerify> data = null;
    public final static Creator<NadraVerifyResponse> CREATOR = new Creator<NadraVerifyResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public NadraVerifyResponse createFromParcel(Parcel in) {
            return new NadraVerifyResponse(in);
        }

        public NadraVerifyResponse[] newArray(int size) {
            return (new NadraVerifyResponse[size]);
        }

    };
    private final static long serialVersionUID = 7886283897149044318L;

    protected NadraVerifyResponse(Parcel in) {
        this.status = ((String) in.readValue((String.class.getClassLoader())));
        this.message = ((String) in.readValue((String.class.getClassLoader())));
        in.readList(this.data, (NadraVerify.class.getClassLoader()));
    }

    public NadraVerifyResponse() {
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<NadraVerify> getData() {
        return data;
    }

    public void setData(List<NadraVerify> data) {
        this.data = data;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(status);
        dest.writeValue(message);
        dest.writeList(data);
    }

    public int describeContents() {
        return 0;
    }

}
