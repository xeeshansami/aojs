package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface PrefComModeCallBack {
    fun PrefComModeSuccess(response: PrefComModeResponse)
    fun PrefComModeFailure(response: BaseResponse)
}