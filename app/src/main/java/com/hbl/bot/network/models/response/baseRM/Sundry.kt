package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Sundry() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("SUNDRY_CODE")
    @Expose
    var SUNDRY_CODE: String? = null

    @SerializedName("SUNDRY_DESC")
    @Expose
    var SUNDRY_DESC: String? = null

    @SerializedName("C3ULV")
    @Expose
    val C3ULV: String? = null

    @SerializedName("C3MIL")
    @Expose
    val C3MIL: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<Sundry> {
        override fun createFromParcel(parcel: Parcel): Sundry {
            return Sundry(parcel)
        }

        override fun newArray(size: Int): Array<Sundry?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return "${SUNDRY_DESC.toString()}, ${SUNDRY_CODE.toString()}"
    }
}