package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.CIFCallBack
import com.hbl.bot.network.models.response.base.*

class CIFHR(callBack: CIFCallBack) : BaseRH<AofAccountInfoResponse>() {
    var callback: CIFCallBack = callBack
    override fun onSuccess(response: AofAccountInfoResponse?) {
        response?.let { callback.CIFSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CIFFailure(it) }
    }
}