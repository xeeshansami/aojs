package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class IndustryMainCategoryHR(callBack: IndustryMainCategoryBack) :
    BaseRH<IndustryMainCategoryResponse>() {
    var callback: IndustryMainCategoryBack = callBack
    override fun onSuccess(response: IndustryMainCategoryResponse?) {
        response?.let { callback.IndustryMainCategorySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.IndustryMainCategoryFailure(it) }
    }
}