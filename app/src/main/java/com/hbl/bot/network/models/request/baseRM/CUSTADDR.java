
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CUSTADDR {

    @SerializedName("ADDRESS_TYPE")
    @Expose
    public String aDDRESSTYPE;
    @SerializedName("ADDRESS_LINE1")
    @Expose
    public String aDDRESSLINE1;
    @SerializedName("ADDRESS_LINE2")
    @Expose
    public String aDDRESSLINE2;
    @SerializedName("ADDRESS_LINE3")
    @Expose
    public String aDDRESSLINE3;
    @SerializedName("NEAREST_LANDMARK")
    @Expose
    public String nEARESTLANDMARK;
    @SerializedName("COUNTRY")
    @Expose
    public String cOUNTRY;
    @SerializedName("COUNTRY_NAME")
    @Expose
    public String cOUNTRYNAME;
    @SerializedName("CITY")
    @Expose
    public String cITY;
    @SerializedName("CITY_NAME")
    @Expose
    public String cITYNAME;
    @SerializedName("POSTCODE")
    @Expose
    public String pOSTCODE;
    @SerializedName("TYPE_OF_ADDRESS_CODE")
    @Expose
    public String tYPEOFADDRESSCODE;
    @SerializedName("TYPE_OF_ADDRESS_DESC")
    @Expose
    public String tYPEOFADDRESSDESC;

    public String getADDRESSTYPE() {
        return aDDRESSTYPE;
    }

    public void setADDRESSTYPE(String aDDRESSTYPE) {
        this.aDDRESSTYPE = aDDRESSTYPE;
    }

    public String getADDRESSLINE1() {
        return aDDRESSLINE1;
    }

    public void setADDRESSLINE1(String aDDRESSLINE1) {
        this.aDDRESSLINE1 = aDDRESSLINE1;
    }

    public String getADDRESSLINE2() {
        return aDDRESSLINE2;
    }

    public void setADDRESSLINE2(String aDDRESSLINE2) {
        this.aDDRESSLINE2 = aDDRESSLINE2;
    }

    public String getADDRESSLINE3() {
        return aDDRESSLINE3;
    }

    public void setADDRESSLINE3(String aDDRESSLINE3) {
        this.aDDRESSLINE3 = aDDRESSLINE3;
    }

    public String getNEARESTLANDMARK() {
        return nEARESTLANDMARK;
    }

    public void setNEARESTLANDMARK(String nEARESTLANDMARK) {
        this.nEARESTLANDMARK = nEARESTLANDMARK;
    }

    public String getCOUNTRY() {
        return cOUNTRY;
    }

    public void setCOUNTRY(String cOUNTRY) {
        this.cOUNTRY = cOUNTRY;
    }

    public String getCOUNTRYNAME() {
        return cOUNTRYNAME;
    }

    public void setCOUNTRYNAME(String cOUNTRYNAME) {
        this.cOUNTRYNAME = cOUNTRYNAME;
    }

    public String getCITY() {
        return cITY;
    }

    public void setCITY(String cITY) {
        this.cITY = cITY;
    }

    public String getCITYNAME() {
        return cITYNAME;
    }

    public void setCITYNAME(String cITYNAME) {
        this.cITYNAME = cITYNAME;
    }

    public String getPOSTCODE() {
        return pOSTCODE;
    }

    public void setPOSTCODE(String pOSTCODE) {
        this.pOSTCODE = pOSTCODE;
    }

    public String getTYPEOFADDRESSCODE() {
        return tYPEOFADDRESSCODE;
    }

    public void setTYPEOFADDRESSCODE(String tYPEOFADDRESSCODE) {
        this.tYPEOFADDRESSCODE = tYPEOFADDRESSCODE;
    }

    public String getTYPEOFADDRESSDESC() {
        return tYPEOFADDRESSDESC;
    }

    public void setTYPEOFADDRESSDESC(String tYPEOFADDRESSDESC) {
        this.tYPEOFADDRESSDESC = tYPEOFADDRESSDESC;
    }

}
