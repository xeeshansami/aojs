package com.hbl.bot.network.Repositories

import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.ResponseHandlers.handler.*
import com.hbl.bot.network.apiInterface.APIInterface
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.retrofitBuilder.RetrofitBuilder
import com.hbl.bot.utils.Config
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.ToastUtils

class DashboardRepository() {
    val globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
    var apiInterface: APIInterface? = null

    init {
        apiInterface =
            GlobalClass.applicationContext?.let {
                RetrofitBuilder.getRetrofitInstance(
                    it,
                    RetrofitEnums.URL_HBL
                ,Config.API_CONNECT_TIMEOUT
                )
            }
    }

    fun getTrackingID(
        request: TrackingIDRequest,
        activity: FragmentActivity?
    ): MutableLiveData<TrackingIDResponse> {
        globalClass?.showDialog(activity)
        val data: MutableLiveData<TrackingIDResponse> = MutableLiveData<TrackingIDResponse>()
        apiInterface?.getTrackingID(request)?.enqueue(TrackingIDHR(object : TrackingIDCallBack {
            override fun TrackingIDSuccess(response: TrackingIDResponse) {
                if (!response.data.isNullOrEmpty())
                    data.value = response
            }

            override fun TrackingIDFailure(response: BaseResponse) {
                data.value = null
                ToastUtils.normalShowToast(activity, response.message, 1)
                globalClass?.hideLoader()
            }

        }))
        return data
    }


    fun getWorkFlow(
        request: LovRequest,
        activity: FragmentActivity?
    ): MutableLiveData<WorkFlowResponse> {
        globalClass?.showDialog(activity)
        val data: MutableLiveData<WorkFlowResponse> = MutableLiveData<WorkFlowResponse>()
        apiInterface?.getWorkFlow(request)?.enqueue(WorkFlowHR(object : WorkFlowCallBack {
            override fun WorkFlowSuccess(response: WorkFlowResponse) {
                if (!response.data.isNullOrEmpty())
                    data.value = response
            }

            override fun WorkFlowFailure(response: BaseResponse) {
                data.value = null
                ToastUtils.normalShowToast(activity, response.message, 1)
                globalClass?.hideLoader()
            }

        }))
        return data
    }

    fun getMYSISRef(
        request: LovRequest,
        activity: FragmentActivity?
    ): MutableLiveData<MYSISREFResponse> {
        val data: MutableLiveData<MYSISREFResponse> = MutableLiveData<MYSISREFResponse>()
        apiInterface?.getMySisRef(request)?.enqueue(MySisRefHR(object : MySisRefCallBack {
            override fun MySisRefSuccess(response: MYSISREFResponse) {
                if (!response.data.isNullOrEmpty())
                    data.value = response
            }

            override fun MySisRefFailure(response: BaseResponse) {
                data.value = null
                ToastUtils.normalShowToast(activity, response.message, 1)
                globalClass?.hideLoader()
            }

        }))
        return data
    }

    fun getBusinessArea(
        request: BusinessAreaRequest,
        activity: FragmentActivity?
    ): MutableLiveData<BusinessAreaResponse> {
        val data: MutableLiveData<BusinessAreaResponse> = MutableLiveData<BusinessAreaResponse>()
        apiInterface?.getBusinessArea(request)
            ?.enqueue(BusinessAreaHR(object : BusinessAreaCallBack {
                override fun BusinessAreaSuccess(response: BusinessAreaResponse) {
                    if (!response.data.isNullOrEmpty())
                        data.value = response
                    globalClass?.hideLoader()
                }

                override fun BusinessAreaFailure(response: BaseResponse) {
                    data.value = null
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    globalClass?.hideLoader()
                }
            }))
        return data
    }

}