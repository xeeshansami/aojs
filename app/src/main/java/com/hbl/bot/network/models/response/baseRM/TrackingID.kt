package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class TrackingID() : Serializable, Parcelable {
    @SerializedName("_batch")
    @Expose
    private var batch: List<Batch?>? =
        null

    @SerializedName("_cursor")
    @Expose
    private var cursor: Cursor? = null

    fun getBatch(): List<Batch?>? {
        return batch
    }

    fun setBatch(batch: List<Batch?>?) {
        this.batch = batch
    }

    fun getCursor(): Cursor? {
        return cursor
    }

    fun setCursor(cursor: Cursor?) {
        this.cursor = cursor
    }

    constructor(source: Parcel) : this(
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<TrackingID> = object : Parcelable.Creator<TrackingID> {
            override fun createFromParcel(source: Parcel): TrackingID = TrackingID(source)
            override fun newArray(size: Int): Array<TrackingID?> = arrayOfNulls(size)
        }
    }
}