package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface KonnectInquiryCallBack {
    fun konnectInquirySuccess(response: KonnectInquiryResponse)
    fun konnectInquiryFailure(response: BaseResponse)
}