package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface IndustrySubCategoryBack {
    fun IndustrySubCategorySuccess(response: IndustrySubCategoryResponse)
    fun IndustrySubCategoryyFailure(response: BaseResponse)
}