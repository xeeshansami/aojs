package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable

class PrintDocsData(
) : Parcelable {
    var DOC_SETUP: ArrayList<DOCSETUP> = ArrayList<DOCSETUP>()
    var PAGE_SETUP: ArrayList<PAGESETUP> = ArrayList<PAGESETUP>()

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PrintDocsData> {
        override fun createFromParcel(parcel: Parcel): PrintDocsData {
            return PrintDocsData(parcel)
        }

        override fun newArray(size: Int): Array<PrintDocsData?> {
            return arrayOfNulls(size)
        }
    }
}