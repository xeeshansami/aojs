package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class KonnectInquiryHR(callBack: KonnectInquiryCallBack) :
    BaseRH<KonnectInquiryResponse>() {
    var callback: KonnectInquiryCallBack = callBack
    override fun onSuccess(response: KonnectInquiryResponse?) {
        response?.let { callback.konnectInquirySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.konnectInquiryFailure(it) }
    }
}