package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class KonnectDebitCardInquiryHR(callBack: KonnectDebitCardInquiryCallBack) :
    BaseRH<KonnectDebitCardInquiryResponse>() {
    var callback: KonnectDebitCardInquiryCallBack = callBack
    override fun onSuccess(response: KonnectDebitCardInquiryResponse?) {
        response?.let { callback.konnectDebitCardInquirySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.konnectDebitCardInquiryFailure(it) }
    }
}