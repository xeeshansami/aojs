package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class BioAccountTypeHR(callBack: BioAccountTypeCallBack) : BaseRH<BioAccountTypeResponse>() {
    var callback: BioAccountTypeCallBack = callBack
    override fun onSuccess(response: BioAccountTypeResponse?) {
        response?.let { callback.AccountTypSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.AccountTypFailure(it) }
    }
}