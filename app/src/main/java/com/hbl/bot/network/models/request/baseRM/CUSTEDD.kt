package com.hbl.bot.network.models.request.baseRM

import com.google.gson.annotations.SerializedName
import com.google.gson.annotations.Expose

class CUSTEDD {
    @SerializedName("REASON_EDD")
    @Expose
    var rEASON_EDD = ""

    @SerializedName("DESC_REASON_EDD")
    @Expose
    var dESC_REASON_EDD = ""

    @SerializedName("FOREIGN_CONTACT_NO")
    @Expose
    var fOREIGN_CONTACT_NO = ""

    @SerializedName("FOREIGN_RESIDENT_ADDRESS_1")
    @Expose
    var fOREIGN_RESIDENT_ADDRESS_1 = ""

    @SerializedName("FOREIGN_RESIDENT_ADDRESS_2")
    @Expose
    var fOREIGN_RESIDENT_ADDRESS_2 = ""

    @SerializedName("FOREIGN_OFFICE_ADDRESS_1")
    @Expose
    var fOREIGN_OFFICE_ADDRESS_1 = ""

    @SerializedName("FOREIGN_OFFICE_ADDRESS_2")
    @Expose
    var fOREIGN_OFFICE_ADDRESS_2 = ""

    @SerializedName("AGRI_INCOME")
    @Expose
    var aGRI_INCOME = ""

    @SerializedName("AGRI_INCOME_EXP_AMT")
    @Expose
    var aGRI_INCOME_EXP_AMT = ""

    @SerializedName("AGRI_RENTAL_INCOME")
    @Expose
    var aGRI_RENTAL_INCOME = ""

    @SerializedName("AGRI_RENTAL_INCOME_EXP_AMT")
    @Expose
    var aGRI_RENTAL_INCOME_EXP_AMT = ""

    @SerializedName("BUSINESS_INCOME")
    @Expose
    var bUSINESS_INCOME = ""

    @SerializedName("BUSINESS_INCOME_EXP_AMT")
    @Expose
    var bUSINESS_INCOME_EXP_AMT = ""

    @SerializedName("RENTAL_INCOME")
    @Expose
    var rENTAL_INCOME = ""

    @SerializedName("RENTAL_INCOME_EXP_AMT")
    @Expose
    var rENTAL_INCOME_EXP_AMT = ""

    @SerializedName("RENTAL_INCOME_RES_PROJ")
    @Expose
    var rENTAL_INCOME_RES_PROJ = ""

    @SerializedName("RENTAL_INCOME_RES_PROJ_EXP_AMT")
    @Expose
    var rENTAL_INCOME_RES_PROJ_EXP_AMT = ""

    @SerializedName("OTHER_SOURCE")
    @Expose
    var oTHER_SOURCE = ""

    @SerializedName("OTHER_SOURCE_2")
    @Expose
    var oTHER_SOURCE_2 = ""

    @SerializedName("CONDUCT_FOREIGN_BANK_TRANSFER")
    @Expose
    var cONDUCT_FOREIGN_BANK_TRANSFER = ""

    @SerializedName("COUNTRY_CODE_INWARD_FOREIGN1")
    @Expose
    var cOUNTRY_CODE_INWARD_FOREIGN1 = ""

    @SerializedName("COUNTRY_DESC_INWARD_FOREIGN1")
    @Expose
    var cOUNTRY_DESC_INWARD_FOREIGN1 = ""

    @SerializedName("COUNTRY_CODE_OUTWARD_FOREIGN1")
    @Expose
    var cOUNTRY_CODE_OUTWARD_FOREIGN1 = ""

    @SerializedName("COUNTRY_DESC_OUTWARD_FOREIGN1")
    @Expose
    var cOUNTRY_DESC_OUTWARD_FOREIGN1 = ""

    @SerializedName("NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1")
    @Expose
    var nAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1 = ""

    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_1")
    @Expose
    var rELATIONSHIP_FOREIGN_BENEFICIARY_1 = ""

    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1")
    @Expose
    var rELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1 = ""

    @SerializedName("COUNTRY_CODE_INWARD_FOREIGN2")
    @Expose
    var cOUNTRY_CODE_INWARD_FOREIGN2 = ""

    @SerializedName("COUNTRY_DESC_INWARD_FOREIGN2")
    @Expose
    var cOUNTRY_DESC_INWARD_FOREIGN2 = ""

    @SerializedName("COUNTRY_CODE_OUTWARD_FOREIGN2")
    @Expose
    var cOUNTRY_CODE_OUTWARD_FOREIGN2 = ""

    @SerializedName("COUNTRY_DESC_OUTWARD_FOREIGN2")
    @Expose
    var cOUNTRY_DESC_OUTWARD_FOREIGN2 = ""

    @SerializedName("NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2")
    @Expose
    var nAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2 = ""

    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_2")
    @Expose
    var rELATIONSHIP_FOREIGN_BENEFICIARY_2 = ""

    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2")
    @Expose
    var rELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2 = ""

    @SerializedName("COUNTRY_CODE_INWARD_FOREIGN3")
    @Expose
    var cOUNTRY_CODE_INWARD_FOREIGN3 = ""

    @SerializedName("COUNTRY_DESC_INWARD_FOREIGN3")
    @Expose
    var cOUNTRY_DESC_INWARD_FOREIGN3 = ""

    @SerializedName("COUNTRY_CODE_OUTWARD_FOREIGN3")
    @Expose
    var cOUNTRY_CODE_OUTWARD_FOREIGN3 = ""

    @SerializedName("COUNTRY_DESC_OUTWARD_FOREIGN3")
    @Expose
    var cOUNTRY_DESC_OUTWARD_FOREIGN3 = ""

    @SerializedName("NAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3")
    @Expose
    var nAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3 = ""

    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_3")
    @Expose
    var rELATIONSHIP_FOREIGN_BENEFICIARY_3 = ""

    @SerializedName("RELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3")
    @Expose
    var rELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3 = ""

    @SerializedName("ID_NO")
    @Expose
    var iD_NO = ""

    @SerializedName("NATIONALITY")
    @Expose
    var nATIONALITY = ""

    @SerializedName("DESC_NATIONALITY")
    @Expose
    var dESC_NATIONALITY = ""

    @SerializedName("BNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY")
    @Expose
    var bNK_TRANF_CONF_ACTIVITY_OF_CCOUNTRY = ""

    @SerializedName("ACCT_HOLDR_MNT_OTH_ACCT_IN_PAK")
    @Expose
    var aCCT_HOLDR_MNT_OTH_ACCT_IN_PAK = ""

    @SerializedName("NAME_OF_INSTITUTION_1")
    @Expose
    var nAME_OF_INSTITUTION_1 = ""

    @SerializedName("NAME_OF_INSTITUTION_2")
    @Expose
    var nAME_OF_INSTITUTION_2 = ""

    @SerializedName("NAME_OF_INSTITUTION_3")
    @Expose
    var nAME_OF_INSTITUTION_3 = ""

    @SerializedName("AVG_MTHLY_BAL_IN_THOSE_ACCTS")
    @Expose
    var aVG_MTHLY_BAL_IN_THOSE_ACCTS = ""

    @SerializedName("FOREIGN_CONTACT_NO_HIGH_RISK_NRP")
    @Expose
    var fOREIGN_CONTACT_NO_HIGH_RISK_NRP = ""

    @SerializedName("RESIDENTIAL_ADDRESS")
    @Expose
    var rESIDENTIAL_ADDRESS = ""

    @SerializedName("OFFICE_ADDRESS_FOREIGN_COUNTRY")
    @Expose
    var oFFICE_ADDRESS_FOREIGN_COUNTRY = ""

    @SerializedName("SAL_OF_FIN_SUPPORTER")
    @Expose
    var sAL_OF_FIN_SUPPORTER = ""

    @SerializedName("SAL_OF_FIN_SUPPORTER_EXP_AMT")
    @Expose
    var sAL_OF_FIN_SUPPORTER_EXP_AMT = ""

    @SerializedName("RENTAL_INCOME_FIN_SUPPORTER")
    @Expose
    var rENTAL_INCOME_FIN_SUPPORTER = ""

    @SerializedName("RENTAL_INCOME_FIN_SUPPORTER_EXP_AMT")
    @Expose
    var rENTAL_INCOME_FIN_SUPPORTER_EXP_AMT = ""

    @SerializedName("BUSS_INCOME_FIN_SUPPORTER")
    @Expose
    var bUSS_INCOME_FIN_SUPPORTER = ""

    @SerializedName("BUSS_INCOME_FIN_SUPPORTER_EXP_AMT")
    @Expose
    var bUSS_INCOME_FIN_SUPPORTER_EXP_AMT = ""

    @SerializedName("RETIREMENT_FUND")
    @Expose
    var rETIREMENT_FUND = ""

    @SerializedName("RETIREMENT_FUND_EXP_AMT")
    @Expose
    var rETIREMENT_FUND_EXP_AMT = ""

    @SerializedName("PENSION")
    @Expose
    var pENSION = ""

    @SerializedName("PENSION_EXP_AMT")
    @Expose
    var pENSION_EXP_AMT = ""

    @SerializedName("INHERITANCE")
    @Expose
    var iNHERITANCE = ""

    @SerializedName("INHERITANCE_EXP_AMT")
    @Expose
    var iNHERITANCE_EXP_AMT = ""

    @SerializedName("OTHER")
    @Expose
    var oTHER = ""

    @SerializedName("OTHER_EXP_AMT")
    @Expose
    var oTHER_EXP_AMT = ""

    @SerializedName("OTHER_SOURCE_NRP")
    @Expose
    var oTHER_SOURCE_NRP = ""

    @SerializedName("RELATIONSHIP_WITH_FIN")
    @Expose
    var rELATIONSHIP_WITH_FIN = ""

    @SerializedName("DESC_RELATIONSHIP_WITH_FIN")
    @Expose
    var dESC_RELATIONSHIP_WITH_FIN = ""

    @SerializedName("FIN_SUPP_GUR_FMLY_PEP")
    @Expose
    var fIN_SUPP_GUR_FMLY_PEP = ""

    @SerializedName("EDD_CONDUCT_BY_NAME")
    @Expose
    var eDD_CONDUCT_BY_NAME = ""

    @SerializedName("EDD_CONDUCT_BY_DATE")
    @Expose
    var eDD_CONDUCT_BY_DATE = ""

    @SerializedName("APPROVED_BY_NAME")
    @Expose
    var aPPROVED_BY_NAME = ""

    @SerializedName("APPROVED_BY_DATE")
    @Expose
    var aPPROVED_BY_DATE = ""

    @SerializedName("APPROVED_COMPL_AUTH_NAME")
    @Expose
    var aPPROVED_COMPL_AUTH_NAME = ""

    @SerializedName("APPROVED_COMPL_AUTH_DATE")
    @Expose
    var aPPROVED_COMPL_AUTH_DATE = ""

    @SerializedName("CONDUCT_LOCAL_BANK_TRANSFER")
    @Expose
    var cONDUCT_LOCAL_BANK_TRANSFER = ""

    @SerializedName("COUNTRY_CODE_INWARD_LOCAL1")
    @Expose
    var cOUNTRY_CODE_INWARD_LOCAL1 = ""

    @SerializedName("COUNTRY_DESC_INWARD_LOCAL1")
    @Expose
    var cOUNTRY_DESC_INWARD_LOCAL1 = ""

    @SerializedName("COUNTRY_CODE_OUTWARD_LOCAL1")
    @Expose
    var cOUNTRY_CODE_OUTWARD_LOCAL1 = ""

    @SerializedName("COUNTRY_DESC_OUTWARD_LOCAL1")
    @Expose
    var cOUNTRY_DESC_OUTWARD_LOCAL1 = ""

    @SerializedName("NAME_OF_BENEFICIARY_LOCAL_TRANSFER_1")
    @Expose
    var nAME_OF_BENEFICIARY_LOCAL_TRANSFER_1 = ""

    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_1")
    @Expose
    var rELATIONSHIP_LOCAL_BENEFICIARY_1 = ""

    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_DESC_1")
    @Expose
    var rELATIONSHIP_LOCAL_BENEFICIARY_DESC_1 = ""

    @SerializedName("COUNTRY_CODE_INWARD_LOCAL2")
    @Expose
    var cOUNTRY_CODE_INWARD_LOCAL2 = ""

    @SerializedName("COUNTRY_DESC_INWARD_LOCAL2")
    @Expose
    var cOUNTRY_DESC_INWARD_LOCAL2 = ""

    @SerializedName("COUNTRY_CODE_OUTWARD_LOCAL2")
    @Expose
    var cOUNTRY_CODE_OUTWARD_LOCAL2 = ""

    @SerializedName("COUNTRY_DESC_OUTWARD_LOCAL2")
    @Expose
    var cOUNTRY_DESC_OUTWARD_LOCAL2 = ""

    @SerializedName("NAME_OF_BENEFICIARY_LOCAL_TRANSFER_2")
    @Expose
    var nAME_OF_BENEFICIARY_LOCAL_TRANSFER_2 = ""

    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_2")
    @Expose
    var rELATIONSHIP_LOCAL_BENEFICIARY_2 = ""

    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_DESC_2")
    @Expose
    var rELATIONSHIP_LOCAL_BENEFICIARY_DESC_2 = ""

    @SerializedName("COUNTRY_CODE_INWARD_LOCAL3")
    @Expose
    var cOUNTRY_CODE_INWARD_LOCAL3 = ""

    @SerializedName("COUNTRY_DESC_INWARD_LOCAL3")
    @Expose
    var cOUNTRY_DESC_INWARD_LOCAL3 = ""

    @SerializedName("COUNTRY_CODE_OUTWARD_LOCAL3")
    @Expose
    var cOUNTRY_CODE_OUTWARD_LOCAL3 = ""

    @SerializedName("COUNTRY_DESC_OUTWARD_LOCAL3")
    @Expose
    var cOUNTRY_DESC_OUTWARD_LOCAL3 = ""

    @SerializedName("NAME_OF_BENEFICIARY_LOCAL_TRANSFER_3")
    @Expose
    var nAME_OF_BENEFICIARY_LOCAL_TRANSFER_3 = ""

    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_DESC_3")
    @Expose
    var rELATIONSHIP_LOCAL_BENEFICIARY_DESC_3 = ""

    @SerializedName("RELATIONSHIP_LOCAL_BENEFICIARY_3")
    @Expose
    var rELATIONSHIP_LOCAL_BENEFICIARY_3 = ""

    @SerializedName("PURPOSE_OF_FOREIGN_TRANSACTION")
    @Expose
    var pURPOSE_OF_FOREIGN_TRANSACTION = ""

    @SerializedName("PURPOSE_OF_LOCAL_TRANSACTION")
    @Expose
    var pURPOSE_OF_LOCAL_TRANSACTION = ""

    @SerializedName("AMOUNT_OF_FOREIGN_TRANSACTION")
    @Expose
    var aMOUNT_OF_FOREIGN_TRANSACTION = ""

    @SerializedName("AMOUNT_OF_LOCAL_TRANSACTION")
    @Expose
    var aMOUNT_OF_LOCAL_TRANSACTION = ""

    @SerializedName("IS_NON_RESIDENT_INFORMATION")
    @Expose
    var iS_NON_RESIDENT_INFORMATION = "N"

    @SerializedName("IS_HIGH_RISK_SEGMENT")
    @Expose
    var iS_HIGH_RISK_SEGMENT = "N"

    @SerializedName("IS_THE_FINANCIAL_SUPPORTER")
    @Expose
    var iS_THE_FINANCIAL_SUPPORTER = "N"
}