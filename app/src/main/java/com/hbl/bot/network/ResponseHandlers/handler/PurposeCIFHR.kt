package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class PurposeCIFHR(callBack: PurposeCIFCallBack) : BaseRH<PurposeCIFResponse>() {
    var callback: PurposeCIFCallBack = callBack
    override fun onSuccess(response: PurposeCIFResponse?) {
        response?.let { callback.PurposeCIFSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.PurposeCIFFailure(it) }
    }
}