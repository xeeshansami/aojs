package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.models.response.base.*

class CapacityHR(callBack: CapacityCallBack) : BaseRH<CapacityResponse>() {
    var callback: CapacityCallBack = callBack
    override fun onSuccess(response: CapacityResponse?) {
        response?.let { callback.CapacitySuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.CapacityFailure(it) }
    }
}