package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class CIFData() : Serializable, Parcelable {
    @SerializedName("ACCOUNTNO")
    @Expose
    val ACCOUNTNO: String? = null

    @SerializedName("RESPCODE")
    @Expose
    var RESPCODE: String? = null


    @SerializedName("CIFNO")
    @Expose
    var CIFNO: String? = null


    @SerializedName("IBAN")
    @Expose
    var IBAN: String? = null

    @SerializedName("RESPDESC")
    @Expose
    var RESPDESC: String? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<CIFData> {
        override fun createFromParcel(parcel: Parcel): CIFData {
            return CIFData(parcel)
        }

        override fun newArray(size: Int): Array<CIFData?> {
            return arrayOfNulls(size)
        }
    }

}