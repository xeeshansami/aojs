package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.BranchCode
import java.io.Serializable


class BranchCodeResponse() : Serializable, Parcelable {
    @SerializedName("status")
    @Expose
    private val status: String? = null

    @SerializedName("message")
    @Expose
    val message: String? = null

    @SerializedName("data")
    @Expose
    val data: ArrayList<BranchCode>? = null

    constructor(source: Parcel) : this(
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<BranchCodeResponse> =
            object : Parcelable.Creator<BranchCodeResponse> {
                override fun createFromParcel(source: Parcel): BranchCodeResponse =
                    BranchCodeResponse(source)

                override fun newArray(size: Int): Array<BranchCodeResponse?> = arrayOfNulls(size)
            }
    }
}