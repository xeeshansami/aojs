package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PreferencesMailingResponse() : Parcelable {
    @SerializedName("data")
    @Expose
    val data: ArrayList<PreferencesMailing> = ArrayList<PreferencesMailing>()
    val message: String = ""
    val status: String = ""

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {

    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PreferencesMailingResponse> {
        override fun createFromParcel(parcel: Parcel): PreferencesMailingResponse {
            return PreferencesMailingResponse(parcel)
        }

        override fun newArray(size: Int): Array<PreferencesMailingResponse?> {
            return arrayOfNulls(size)
        }
    }
}