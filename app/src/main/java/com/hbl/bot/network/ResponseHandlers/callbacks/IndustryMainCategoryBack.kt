package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface IndustryMainCategoryBack {
    fun IndustryMainCategorySuccess(response: IndustryMainCategoryResponse)
    fun IndustryMainCategoryFailure(response: BaseResponse)
}