package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class SubTrackingID() : Serializable, Parcelable {
    @SerializedName("_cursor")
    @Expose
    val _cursor: String? = null

    @SerializedName("_batch")
    @Expose
    val _batch: ArrayList<TrackingID>? = null

    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<SubTrackingID> {
        override fun createFromParcel(parcel: Parcel): SubTrackingID {
            return SubTrackingID(parcel)
        }

        override fun newArray(size: Int): Array<SubTrackingID?> {
            return arrayOfNulls(size)
        }
    }
}