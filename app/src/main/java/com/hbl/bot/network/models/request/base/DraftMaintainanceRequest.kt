package com.hbl.bot.network.models.request.base

import com.google.gson.annotations.SerializedName

class DraftMaintainanceRequest() {
    @SerializedName("identifier")
    var identifier: String = ""

    @SerializedName("DOC_ID")
    var DOC_ID: String = ""
}