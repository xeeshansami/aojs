package com.hbl.bot.network.models.request.base

import com.google.gson.annotations.SerializedName

class DraftRequest() {
    @SerializedName("find")
    var find = DraftFind()

    @SerializedName("identifier")
    var identifier: String = ""

    @SerializedName("DOC_ID")
    var DOC_ID: String = ""
}