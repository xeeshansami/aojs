package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface CardProductCallBack {
    fun CardProductSuccess(response: CardProductResponse)
    fun CardProductFailure(response: BaseResponse)
}