package com.hbl.bot.network.models.response.base

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.EstateFrequency
import java.io.Serializable


class EstateFrequencyResponse() : Serializable, Parcelable {
    @SerializedName("status")
    @Expose
    private val status: String? = null

    @SerializedName("message")
    @Expose
    private val message: String? = null

    @SerializedName("data")
    @Expose
    val data: ArrayList<EstateFrequency>? = null

    constructor(source: Parcel) : this(
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {}

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<EstateFrequencyResponse> =
            object : Parcelable.Creator<EstateFrequencyResponse> {
                override fun createFromParcel(source: Parcel): EstateFrequencyResponse =
                    EstateFrequencyResponse(source)

                override fun newArray(size: Int): Array<EstateFrequencyResponse?> =
                    arrayOfNulls(size)
            }
    }
}