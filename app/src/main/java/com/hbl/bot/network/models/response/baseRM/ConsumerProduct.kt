package com.hbl.bot.network.models.response.baseRM

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.io.Serializable


class ConsumerProduct() : Serializable, Parcelable {
    @SerializedName("_id")
    @Expose
    val _id: String? = null

    @SerializedName("PRDT_CODE")
    @Expose
    var PRDT_CODE: String? = null

    @SerializedName("PRDT_DESC")
    @Expose
    var PRDT_DESC: String? = null


    constructor(parcel: Parcel) : this() {
    }

    override fun writeToParcel(p0: Parcel?, p1: Int) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun describeContents(): Int {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    companion object CREATOR : Parcelable.Creator<ConsumerProduct> {
        override fun createFromParcel(parcel: Parcel): ConsumerProduct {
            return ConsumerProduct(parcel)
        }

        override fun newArray(size: Int): Array<ConsumerProduct?> {
            return arrayOfNulls(size)
        }
    }

    override fun toString(): String {
        return PRDT_DESC.toString()
    }
}