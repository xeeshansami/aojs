package com.hbl.bot.network.ResponseHandlers.callbacks

import com.hbl.bot.network.models.response.base.*

interface SBPCodesCallBack {
    fun SBPCodesSuccess(response: SBPCodesResponse)
    fun SBPCodesFailure(response: BaseResponse)
}