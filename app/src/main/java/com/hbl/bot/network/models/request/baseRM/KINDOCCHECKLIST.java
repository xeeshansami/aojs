
package com.hbl.bot.network.models.request.baseRM;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KINDOCCHECKLIST {

    @SerializedName("DOCNAME")
    @Expose
    private String dOCNAME = "";
    @SerializedName("DOC_KEYWORD")
    @Expose
    private String dOCKEYWORD = "";
    @SerializedName("SEQ_NO")
    @Expose
    private Integer sEQNO = 0;
    @SerializedName("MANDT")
    @Expose
    private String mANDT = "";
    @SerializedName("DOC_COUNT")
    @Expose
    private Integer dOCCOUNT = 0;
    @SerializedName("ADDITIONAL_DOC_COUNT")
    @Expose
    private Integer aDDITIONALDOCCOUNT = 0;
    @SerializedName("New")
    @Expose
    private Integer _new = 0;
    @SerializedName("UPLOADED")
    @Expose
    private Integer uPLOADED = 0;
    @SerializedName("DOC_REQUIRED")
    @Expose
    private Boolean dOCREQUIRED = false;

    public String getDOCNAME() {
        return dOCNAME;
    }

    public void setDOCNAME(String dOCNAME) {
        this.dOCNAME = dOCNAME;
    }

    public String getDOCKEYWORD() {
        return dOCKEYWORD;
    }

    public void setDOCKEYWORD(String dOCKEYWORD) {
        this.dOCKEYWORD = dOCKEYWORD;
    }

    public Integer getSEQNO() {
        return sEQNO;
    }

    public void setSEQNO(Integer sEQNO) {
        this.sEQNO = sEQNO;
    }

    public String getMANDT() {
        return mANDT;
    }

    public void setMANDT(String mANDT) {
        this.mANDT = mANDT;
    }

    public Integer getDOCCOUNT() {
        return dOCCOUNT;
    }

    public void setDOCCOUNT(Integer dOCCOUNT) {
        this.dOCCOUNT = dOCCOUNT;
    }

    public Integer getADDITIONALDOCCOUNT() {
        return aDDITIONALDOCCOUNT;
    }

    public void setADDITIONALDOCCOUNT(Integer aDDITIONALDOCCOUNT) {
        this.aDDITIONALDOCCOUNT = aDDITIONALDOCCOUNT;
    }

    public Integer getNew() {
        return _new;
    }

    public void setNew(Integer _new) {
        this._new = _new;
    }

    public Integer getUPLOADED() {
        return uPLOADED;
    }

    public void setUPLOADED(Integer uPLOADED) {
        this.uPLOADED = uPLOADED;
    }

    public Boolean getDOCREQUIRED() {
        return dOCREQUIRED;
    }

    public void setDOCREQUIRED(Boolean dOCREQUIRED) {
        this.dOCREQUIRED = dOCREQUIRED;
    }

}
