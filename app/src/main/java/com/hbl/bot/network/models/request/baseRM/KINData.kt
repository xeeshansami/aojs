package com.hbl.bot.network.models.request.baseRM

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import com.hbl.bot.network.models.response.baseRM.KINNadraVerify
import java.io.Serializable


class KINData() : Serializable {
    @SerializedName("_id")
    @Expose
    var _id = ""

    @SerializedName("TRACKING_ID")
    @Expose
    var TRACKING_ID = ""

    @SerializedName("MYSIS_REF")
    @Expose
    var MYSIS_REF = ""

    @SerializedName("MYSIS_OLD_REF")
    @Expose
    var MYSIS_OLD_REF = ""

    @SerializedName("ID_DOC_NO")
    @Expose
    var ID_DOC_NO = ""

    @SerializedName("ID_DOCUMENT_TYPE")
    @Expose
    var ID_DOCUMENT_TYPE = ""

    @SerializedName("NAME")
    @Expose
    var NAME = ""

    @SerializedName("FULL_NAME")
    @Expose
    var FULL_NAME = ""

    @SerializedName("WORK_FLOW_CODE")
    @Expose
    var WORK_FLOW_CODE = ""

    @SerializedName("WORK_FLOW_CODE_DESC")
    @Expose
    var WORK_FLOW_CODE_DESC = ""

    @SerializedName("DOC_INSERTED")
    @Expose
    var DOC_INSERTED = "1"

    @SerializedName("PROCESS_CODE")
    @Expose
    var PROCESS_CODE = ""

    @SerializedName("STATUS")
    @Expose
    var STATUS = ""

    @SerializedName("STATUSDESC")
    @Expose
    var STATUSDESC = ""

    @SerializedName("PICKEDBY")
    @Expose
    var PICKEDBY = ""

    @SerializedName("PICKEDBY_ROLE")
    @Expose
    var PICKEDBY_ROLE = ""

    @SerializedName("LAST_ACTION_DATE")
    @Expose
    var LAST_ACTION_DATE = ""

    @SerializedName("LAST_ACTION_USER")
    @Expose
    var LAST_ACTION_USER = ""

    @SerializedName("LAST_ACTION_USER_ID")
    @Expose
    var LAST_ACTION_USER_ID = ""

    @SerializedName("PRIORITY")
    @Expose
    var PRIORITY = ""

    @SerializedName("RISK_RATING")
    @Expose
    var RISK_RATING = ""

    @SerializedName("RISK_RATING_TOTAL")
    @Expose
    var RISK_RATING_TOTAL: Int = 0

    @SerializedName("CHANNEL")
    @Expose
    var CHANNEL = ""

    @SerializedName("PURPOSE_OF_FORM")
    @Expose
    var PURPOSE_OF_FORM = ""

    @SerializedName("PURPOSEOFFORM_DESC")
    @Expose
    var PURPOSEOFFORM_DESC = ""

    @SerializedName("USER_BRANCH")
    @Expose
    var USER_BRANCH = ""

    @SerializedName("USER_REGION")
    @Expose
    var USER_REGION = ""

    @SerializedName("INITIATED_RM_ID")
    @Expose
    var INITIATED_RM_ID = ""

    @SerializedName("INITIATED_USER_ID")
    @Expose
    var INITIATED_USER_ID = ""

    @SerializedName("APPROPRIATE_RELATION")
    @Expose
    var APPROPRIATE_RELATION = ""

    @SerializedName("APPRRELATIONDESC")
    @Expose
    var APPRRELATIONDESC = ""

    @SerializedName("BUSINESS_AREA")
    @Expose
    var BUSINESS_AREA = ""

    @SerializedName("AREA")
    @Expose
    var AREA = ""

    @SerializedName("DISCREPENT_APPROVAL")
    @Expose
    var DISCREPENT_APPROVAL = ""

    @SerializedName("ADMIN_TRIBAL_AREA")
    @Expose
    var ADMIN_TRIBAL_AREA = ""

    @SerializedName("EMAIL_NOT_ALLOWED")
    @Expose
    var EMAIL_NOT_ALLOWED = 0

    @SerializedName("FILE_UPLOADED")
    @Expose
    var FILE_UPLOADED = ""

    @SerializedName("ETBNTBFLAG")
    @Expose
    var ETBNTBFLAG = ""

    @SerializedName("REMEDIATE")
    @Expose
    var REMEDIATE = ""

    @SerializedName("HIGH_RISK_BRANCH")
    @Expose
    var HIGH_RISK_BRANCH = ""

    @SerializedName("PARTIAL_DOC_ENABLE")
    @Expose
    var PARTIAL_DOC_ENABLE = ""

    @SerializedName("DOC_LOAD")
    @Expose
    var DOC_LOAD = ""

    @SerializedName("AOF_APPROVALS")
    @Expose
    var AOF_APPROVALS: ArrayList<KINAOFAPPROVAL> = arrayListOf<KINAOFAPPROVAL>()

    @SerializedName("CUST_BIOMETRIC")
    @Expose
    var CUST_BIOMETRIC: ArrayList<KINNadraVerify> = arrayListOf<KINNadraVerify>()

    @SerializedName("CUST_INFO")
    @Expose
    var CUST_INFO: ArrayList<KINCUSTINFO> = arrayListOf<KINCUSTINFO>()

    @SerializedName("CUST_PEP")
    @Expose
    var CUST_PEP: ArrayList<KINCUSTPEP> = arrayListOf<KINCUSTPEP>()

    @SerializedName("CUST_FIN")
    @Expose
    var CUST_FIN: ArrayList<KINCUSTFINX> = arrayListOf<KINCUSTFINX>()

    @SerializedName("CUST_SOLE_SELF")
    @Expose
    var CUST_SOLE_SELF: ArrayList<KINCUSTSOLESELF> = arrayListOf<KINCUSTSOLESELF>()

    @SerializedName("CUST_EDD")
    @Expose
    var CUST_EDD: ArrayList<KINCUSTEDD> = arrayListOf<KINCUSTEDD>()

    @SerializedName("CUST_CDD")
    @Expose
    var CUST_CDD: ArrayList<KINCUSTCDD> = arrayListOf<KINCUSTCDD>()

    @SerializedName("CUST_CONTACTS")
    @Expose
    var CUST_CONTACTS: ArrayList<KINCUSTCONTACTS> = arrayListOf<KINCUSTCONTACTS>()

    @SerializedName("CUST_ADDR")
    @Expose
    var CUST_ADDR: ArrayList<KINCUSTADDRRESS> = arrayListOf<KINCUSTADDRRESS>()

    @SerializedName("CUST_ACCOUNTS")
    @Expose
    var CUST_ACCOUNTS: ArrayList<KINCUST_ACCOUNTS> = arrayListOf<KINCUST_ACCOUNTS>()

    @SerializedName("CUST_DEMOGRAPHICS")
    @Expose
    var CUST_DEMOGRAPHICS: ArrayList<KINCUSTDEMOGRAPHICSX> = arrayListOf<KINCUSTDEMOGRAPHICSX>()

    @SerializedName("CUST_NEXTOFKIN")
    @Expose
    var CUST_NEXTOFKIN: ArrayList<KINCUSTNEXTOFKIN> = arrayListOf<KINCUSTNEXTOFKIN>()

    @SerializedName("CUST_DISCREPANCIES")
    @Expose
    var CUST_DISCREPANCIES: ArrayList<Any> = arrayListOf<Any>()

    @SerializedName("CUST_DOCUMENT_DISCREPANCIES")
    @Expose
    var CUST_DOCUMENT_DISCREPANCIES: ArrayList<Any> = arrayListOf<Any>()

    @SerializedName("ACTION_EVENT_INFO")
    @Expose
    var ACTION_EVENT_INFO: ArrayList<String> = arrayListOf<String>()

    @SerializedName("CUST_JOINT")
    @Expose
    var CUST_JOINT: ArrayList<String> = arrayListOf<String>()

    @SerializedName("CUST_STATUS")
    @Expose
    var CUST_STATUS: ArrayList<KINCUSTStatus> = arrayListOf<KINCUSTStatus>()

    @SerializedName("USER_INFO")
    @Expose
    var USER_INFO: ArrayList<KINUSERINFO> = arrayListOf<KINUSERINFO>()

    @SerializedName("DOC_CHECKLIST")
    @Expose
    var DOC_CHECKLIST: ArrayList<KINDOCCHECKLIST> = arrayListOf<KINDOCCHECKLIST>()

    @SerializedName("CUST_DOCUMENTS")
    @Expose
    var CUST_DOCUMENTS: ArrayList<KINCUSTDOCUMENT> = arrayListOf<KINCUSTDOCUMENT>()

    @SerializedName("NON_IN_ACCOUNTS")
    @Expose
    var NON_IN_ACCOUNTS: ArrayList<Any> = arrayListOf<Any>()

    @SerializedName("GUARDIAN_INFO")
    @Expose
    var GUARDIAN_INFO: ArrayList<Any> = arrayListOf<Any>()

    @SerializedName("NON_IN_DEMOGRAPHICS")
    @Expose
    var NON_IN_DEMOGRAPHICS: ArrayList<Any> = arrayListOf<Any>()

    @SerializedName("NON_IN_SIGNATORY")
    @Expose
    var NON_IN_SIGNATORY: ArrayList<Any> = arrayListOf<Any>()

    @SerializedName("NON_IN_ADDR")
    @Expose
    var NON_IN_ADDR: ArrayList<Any> = arrayListOf<Any>()

    @SerializedName("NON_IN_CDD")
    @Expose
    var NON_IN_CDD: ArrayList<Any> = arrayListOf<Any>()

    @SerializedName("PROCESSINGDATE")
    @Expose
    var PROCESSINGDATE = ""

    @SerializedName("OLD_MISYS_REF")
    @Expose
    var OLD_MISYS_REF = ""

    @SerializedName("PEP_UPDATE")
    @Expose
    var PEP_UPDATE: Int = 0
}