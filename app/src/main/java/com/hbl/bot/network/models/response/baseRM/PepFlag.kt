package com.hbl.bot.network.models.response.baseRM

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PepFlag {
    @SerializedName("FLAG")
    @Expose
    val FLAG: String? = null
}