package com.hbl.bot.network.models.response.baseRM

data class GenerateCIFData(
    val CIFNO: String,
    val RESPCODE: String,
    val RESPDESC: String
)