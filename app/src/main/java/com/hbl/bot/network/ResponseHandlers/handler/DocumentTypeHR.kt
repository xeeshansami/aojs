package com.hbl.bot.network.ResponseHandlers.handler

import com.hbl.bot.network.ResponseHandlers.callbacks.DocumentTypeCallBack
import com.hbl.bot.network.models.response.base.*

class DocumentTypeHR(callBack: DocumentTypeCallBack) : BaseRH<DocTypeResponse>() {
    var callback: DocumentTypeCallBack = callBack
    override fun onSuccess(response: DocTypeResponse?) {
        response?.let { callback.DocumentTypeSuccess(it) }
    }

    override fun onFailure(response: BaseResponse?) {
        response?.let { callback.DocumentTypeFailure(it) }
    }
}