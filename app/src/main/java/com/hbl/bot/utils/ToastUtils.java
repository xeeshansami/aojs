package com.hbl.bot.utils;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.hbl.bot.R;

public class ToastUtils {
    public static Boolean IS_ENABLED = true;

    @SuppressLint("WrongConstant")
    public static void exceptionToast(Context context, String message) {
        try {
            Toast toast = Toast.makeText(context, message, Toast.LENGTH_LONG);
            TextView textView = toast.getView().findViewById(android.R.id.message);
            textView.setTextColor(Color.WHITE);
            textView.setTextSize(10);
            toast.setGravity(Gravity.BOTTOM | Gravity.CENTER_HORIZONTAL, 0, 200);
            textView.setShadowLayer(0, 0, 0, Color.TRANSPARENT);
            toast.getView().getBackground().clearColorFilter();
            toast.getView().getBackground().setColorFilter(Color.BLACK, PorterDuff.Mode.DARKEN);
            toast.show();
        } catch (Exception e) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            Log.i("Exception", e.getMessage());
        }
    }

    @SuppressLint("WrongConstant")
    public static void normalShowToast(Context context, String message, int whichToast) {
        if (whichToast == Constants.EXCEPTION_TOAST) {//EXCEPTION
            exceptionToast(context, message);
        } else if (whichToast == Constants.SUCCESS_TOAST) {//SUCCESS
            showToastSuccess(context, message);
        } else if (whichToast == Constants.ERROR_TOAST_LONG) {//UI/LOGICAL ERROR
            showToastErr(context, message, Constants.ERROR_TOAST_LONG);
        } else if (whichToast == Constants.KONNECT_TOAST_LONG) {//UI/LOGICAL ERROR
            showToastErr(context, message, Constants.KONNECT_TOAST_LONG);
        } else {
            showToastErr(context, message);
        }
    }

    @SuppressLint("WrongConstant")
    public static void showToastSuccess(Context context, String message) {
        try {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast, (ViewGroup) ((Activity) context).findViewById(R.id.custom_toast_layout));
            View parentLayout = ((Activity) context).findViewById(android.R.id.content);        // Create the Snackbar
            Snackbar snackbar = Snackbar.make(parentLayout, "", 2000);
            // Get the Snackbar's layout view
            Snackbar.SnackbarLayout sk = (Snackbar.SnackbarLayout) snackbar.getView();
            // Inflate our custom view
            View snackView = inflater.inflate(R.layout.custom_toast, null);
            // Configure the view
            TextView txtvw = snackView.findViewById(R.id.txtvw);
            txtvw.setText(message);
            //If the view is not covering the whole snackbar layout, add this line
            layout.setPadding(5, 5, 5, 5);
            // Add the view to the Snackbar's layout
            sk.addView(snackView, 0);
            View view = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP | Gravity.CENTER;
            params.topMargin = 100;
            view.setLayoutParams(params);
//        playSound(context, 1);
            // Show the Snackbar
            snackbar.show();
        } catch (Exception e) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }

    }

    @SuppressLint("WrongConstant")
    public static void showToastErr(Context context, String message) {
        try{
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_error, (ViewGroup) ((Activity) context).findViewById(R.id.custom_toast_layout));
        View parentLayout = ((Activity) context).findViewById(android.R.id.content);        // Create the Snackbar
        Snackbar snackbar = Snackbar.make(parentLayout, "", Snackbar.LENGTH_LONG);
        // Get the Snackbar's layout view
        Snackbar.SnackbarLayout sk = (Snackbar.SnackbarLayout) snackbar.getView();
        // Inflate our custom view
        View snackView = inflater.inflate(R.layout.custom_toast_error, null);
        // Configure the view
        //If the view is not covering the whole snackbar layout, add this line
        TextView txtvw = snackView.findViewById(R.id.txtvw);
        txtvw.setText(message);
        layout.setPadding(5, 5, 5, 5);
        // Add the view to the Snackbar's layout
        sk.addView(snackView, 0);
        View view = snackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP | Gravity.CENTER;
        params.topMargin = 100;
        view.setLayoutParams(params);
//            playSound(context, 0);
        // Show the Snackbar
        snackbar.show();
        }catch (Exception e){
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("WrongConstant")
    public static void showToastErr(Context context, String message, int whichError) {
        try {
            Snackbar snackbar;
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            View layout = inflater.inflate(R.layout.custom_toast_error, (ViewGroup) ((Activity) context).findViewById(R.id.custom_toast_layout));
            View parentLayout = ((Activity) context).findViewById(android.R.id.content);        // Create the Snackbar
            if (whichError == 1) {
                snackbar = Snackbar.make(parentLayout, "", Snackbar.LENGTH_LONG);
            } else if (whichError == Constants.KONNECT_TOAST_LONG) {
                snackbar = Snackbar.make(parentLayout, "", 5000);
            } else {
                snackbar = Snackbar.make(parentLayout, "", 3000);
            }
            // Get the Snackbar's layout view
            Snackbar.SnackbarLayout sk = (Snackbar.SnackbarLayout) snackbar.getView();
            // Inflate our custom view
            View snackView = inflater.inflate(R.layout.custom_toast_error, null);
            // Configure the view
            //If the view is not covering the whole snackbar layout, add this line
            TextView txtvw = snackView.findViewById(R.id.txtvw);
            txtvw.setText(message);
            layout.setPadding(5, 5, 5, 5);
            // Add the view to the Snackbar's layout
            sk.addView(snackView, 0);
            View view = snackbar.getView();
            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
            params.gravity = Gravity.TOP | Gravity.CENTER;
            params.topMargin = 100;
            view.setLayoutParams(params);
//            playSound(context, 0);
            // Show the Snackbar
            snackbar.show();
        } catch (Exception e) {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("WrongConstant")
    public static void showToast(int color, Context context, String message, int time) {
        LayoutInflater inflater = ((Activity) context).getLayoutInflater();
        View layout = inflater.inflate(R.layout.custom_toast_error, (ViewGroup) ((Activity) context).findViewById(R.id.custom_toast_layout));
        View parentLayout = ((Activity) context).findViewById(android.R.id.content);        // Create the Snackbar
        Snackbar snackbar = Snackbar.make(parentLayout, "", Snackbar.LENGTH_LONG);
        // Get the Snackbar's layout view
        Snackbar.SnackbarLayout sk = (Snackbar.SnackbarLayout) snackbar.getView();
        // Inflate our custom view
        View snackView = inflater.inflate(R.layout.custom_toast_error, null);
        // Configure the view
        //If the view is not covering the whole snackbar layout, add this line
        TextView txtvw = snackView.findViewById(R.id.txtvw);
        txtvw.setText(message);
        layout.setPadding(5, 5, 5, 5);
        // Add the view to the Snackbar's layout
        sk.addView(snackView, 0);
        View view = snackbar.getView();
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.gravity = Gravity.TOP | Gravity.CENTER;
        params.topMargin = 50;
        view.setLayoutParams(params);
//            playSound(context, 0);
        // Show the Snackbar
        snackbar.setDuration(time);
        snackbar.show();
    }

    /*public static void playSound(Context context, int type) {
        if (type == 0) {
            MediaPlayer mediaplayer = MediaPlayer.create(context, R.raw.alert);
            //You Can Put Your File Name Instead Of abc
            mediaplayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.reset();
                    mediaPlayer.release();
                }
            });
            mediaplayer.start();
        } else {
            MediaPlayer mediaplayer = MediaPlayer.create(context, R.raw.done);
            //You Can Put Your File Name Instead Of abc
            mediaplayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.reset();
                    mediaPlayer.release();
                }
            });
            mediaplayer.start();
        }

    }*/
}
