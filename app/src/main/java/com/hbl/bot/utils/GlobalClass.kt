package com.hbl.bot.utils

import android.Manifest
import android.app.Activity
import android.app.ActivityManager
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import android.text.*
import android.text.method.DigitsKeyListener
import android.text.style.ForegroundColorSpan
import android.util.Base64
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.hbl.bot.R
import com.hbl.bot.network.models.request.baseRM.KINData
import com.hbl.bot.network.models.response.base.PreferencesMailing
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.network.models.response.baseRM.Currency
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.ui.customviews.Dialog2Callback
import com.hbl.bot.ui.customviews.SingleButtonsDialog
import com.hbl.bot.ui.customviews.TwoButtonsDialog
import com.hbl.bot.utils.managers.SharedPreferenceManager
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

//import android.app.NotificationChannel;
//import android.app.NotificationManager;
class GlobalClass : HBLHRStore() {
    var isAllFieldsDisbaled = false
    var error_box1: TextView? = null
    var error_box2: TextView? = null
    var error_box3: TextView? = null
    var cancel_action: Button? = null
    var appStatus = ""
    var alertDialogBuilder: AlertDialog.Builder? = null
    var alertDialog: AlertDialog? = null
    var maritalholder = 100
    var bloodtypeholder = 100
    var religionholder = 100
    var mothertongeholder = 100
    var maritaloldposition = 0
    var bloodoldposition = 0
    var religionoldposition = 0
    var mothertongeoldposition = 0
    var statusCode = ""
    var ETBNTBFLAG = ""
    var configData: ConfigData? = null
    var documentType: List<DocumentType>? = null
    var documentTypeS: List<DocumentType>? = null
    var kinData: ArrayList<KINData>? = null
    var onboardings: List<Onboarding>? = null
    var MaritualOldValue: String? = null
    var BloodTypeOldValue: String? = null
    var ReligionOldValue: String? = null
    var MotherTongueOldValue: String? = null
    var BloodTypeNewValue: String? = null
    var ReligionNewValue: String? = null
    var MotherTongueNewValue: String? = null
    var listener: setOnitemClickListner? = null
    private var timer: Timer? = null
    var hrToken = ""
    var hrRefreshToken = ""
    var tokenExpiresTime = ""
    var startTime: Long = 0
    var countryCode = ""
    var serviceCode: String? = null
    var message: String? = null
    var method: String? = null
    var networkStatus = ""
    var pdfStatus: String? = null
    var filterList: ArrayList<Rows>? = null
    var file: File? = null
    var medicalHTML = String()
    val currentDate: String
        get() {
            val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.getDefault())
            return sdf.format(Date())
        }
    val currentYear: String
        get() {
            val sdf = SimpleDateFormat("yyyy", Locale.getDefault())
            return sdf.format(Date())
        }


    override fun onCreate() {
        try {
            super.onCreate()
            Companion.applicationContext = applicationContext
            sharedPreferenceManager = SharedPreferenceManager().getInstance(this)
            appName = getString(R.string.app_name)
            getBasicData()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@GlobalClass,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@GlobalClass), e)
        }
        //        new LoggingExceptionHandler(this);
    }

    fun eTBCaseDisbaledFields() {
        isAllFieldsDisbaled = sharedPreferenceManager?.etbntb?.getOrNull(0)?.ETBNTBFLAG == "C"
    }

    fun fileDelete(path: String?) {
        try {
            val fileDir = File(path)
            if (fileDir.exists()) {
                fileDir.delete()
            }
        } catch (ex: Exception) {
            Log.i(
                Constants.TAG, """
     File not file 
     ${ex.message}
     """.trimIndent()
            )
        }
    }

    fun formateChange(time: String): String? {
        return if (time.matches("\\d{4}-\\d{2}-\\d{2}".toRegex())) {
            val inputPattern = "yyyy-MM-dd"
            val outputPattern = "dd-MM-yyyy"
            val inputFormat = SimpleDateFormat(inputPattern)
            val outputFormat = SimpleDateFormat(outputPattern)
            var date: Date? = null
            var str: String? = null
            try {
                date = inputFormat.parse(time)
                str = outputFormat.format(date)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            str
        } else {
            time
        }
    }

    val logoutData: Unit
        get() {
            val intent = Intent(Companion.applicationContext, NavigationDrawerActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            hideLoader()
        }

    fun clearSP() {
        sharedPreferenceManager!!.clearSP()
    }

    fun clearModel(key: String?) {
        sharedPreferenceManager!!.removeStringInSharedPreferences(key, "remove")
    }

    fun clearAllLovs() {
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_MODE_OF_DELIVERY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.PARTIAL_DOCUMETNS,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_CHEQUE_BOOK_LEAVES,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.NADRA_RESPONSE_DATA,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_NATURE_OF_BUSINESS,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_PREFERENCE_MAILING,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_PURPOSE_OF_CIF,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_PURPOSE_OF_TIN,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_RELATIONSHIP,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_FINANCIAL_SUPPORTER_PROFESSION,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_PROFESSION,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_CAPACITY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_PEP_CATEGORY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_DOCTYPE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_COUNTRIES,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_SOURCE_OF_LANDLORDS,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_INDUSTRY_MAIN_CATEGORY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_RESON_PREF_EDD,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_SPECIFIC_COUNTRIES,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_CITIES,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_SBP_CODE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_PEP,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_TITLE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_MATERIAL_STATUS,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_GENDER,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_CONSUMER_PRODUCT,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_ACCOUNT_TYPE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_CURRENCY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_ACCOUNT_OPERATIONNAL_TYPE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_ESTATE_FREQUENCY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_DEPOSIT_TYPE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_RM_CODE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.PURPOSE_OF_ACCOUNT,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.OPERATIONAL_INSTRUCTION,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_SUNDRY_CODE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_ADDRESS_TYPE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_CUST_SEGMENT,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_SOURCE_OF_FUND,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_SOURCE_OF_FUND_INCOME,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_SOURCE_OF_WEALTH,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_MODE_OF_TANS,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_BOOL,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_PREF_MODE,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.LOV_CARD_PRODUCT,
            "remove"
        )
    }


    fun clearETBNTB() {
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.ETBNTB_KEY,
            "remove"
        )
    }

    fun clearSubmitAOF() {
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.AOF_ACCOUNT_IFNO,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.AOFB_MODELS,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_DEMO_GRAPHICS_KEY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_SOLE_SELF,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_CDD_KEY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_EDD_KEY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_FIN_KEY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_BIOMATRIC_KEY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_USER_INFO,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_STATUS,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_ADDRESS_KEY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_CONTACTS_KEY,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_ACCOUNT,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_NEXTOFKIN,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTINFO,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_INFORMATION,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.CUSTOMER_PEP,
            "remove"
        )
        sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.BIOMETRIC_ACCOUNT,
            "remove"
        )
    }

    fun setDisbaled(layout: ConstraintLayout) {
        layout.isEnabled = false
        layout.alpha = 0.5f
    }

    fun setDisbaled(editText: EditText, check: Boolean) {
        editText.isEnabled = check
        editText.alpha = 0.5f
    }

    fun setDisbaled(spinner: Spinner) {
        spinner.isEnabled = false
        spinner.alpha = 0.5f
    }

    fun setDisbaled(spinner: Spinner, check: Boolean) {
        spinner.isEnabled = check
        spinner.alpha = 0.5f
    }

    fun setDisbaled(textView: TextView) {
        textView.isEnabled = false
        textView.alpha = 0.5f
    }

    fun setDisbaled(imageView: ImageView) {
        imageView.isEnabled = false
        imageView.alpha = 0.5f
    }

    fun setDisbaled(button: Button) {
        button.isEnabled = false
        button.alpha = 0.5f
    }

    fun setDisbaled(button: Button, isEnabled: Boolean?) {
        button.isEnabled = isEnabled!!
        button.alpha = 0.5f
    }

    fun setDisbaled(button: ImageView, isEnabled: Boolean?) {
        button.isEnabled = isEnabled!!
        button.alpha = 0.5f
    }

    fun setDisbaled(textView: TextView, isEnabled: Boolean?) {
        textView.isEnabled = isEnabled!!
        textView.alpha = 0.5f
    }

    fun setDisbaled(textView: ConstraintLayout, isEnabled: Boolean?) {
        textView.isEnabled = isEnabled!!
        textView.alpha = 0.5f
    }

    fun setDisbaled(button: ImageButton, isEnabled: Boolean?) {
        button.isEnabled = isEnabled!!
        button.alpha = 0.5f
    }

    fun setDisbaled(editText: EditText) {
        editText.isEnabled = false
        editText.alpha = 0.5f
    }


    fun setEnabled(spinner: Spinner, isEnabled: Boolean?) {
        spinner.isEnabled = true
        spinner.alpha = 1.0f
    }

    fun setEnabled(layout: ConstraintLayout, isEnabled: Boolean?) {
        layout.isEnabled = isEnabled!!
        layout.alpha = 1.0f
    }

    fun setEnabled(imageView: ImageView, isEnabled: Boolean?) {
        imageView.isEnabled = true
        imageView.alpha = 1.0f
    }

    fun setEnabled(imageView: ImageButton, isEnabled: Boolean?) {
        imageView.isEnabled = true
        imageView.alpha = 1.0f
    }

    fun setEnabled(textView: TextView, isEnabled: Boolean?) {
        textView.isEnabled = isEnabled!!
        textView.alpha = 1.0f
    }

    fun setEnabled(button: Button, isEnabled: Boolean?) {
        button.isEnabled = isEnabled!!
        button.alpha = 1.0f
    }

    fun setEnabled(editText: EditText, isEnabled: Boolean?) {
        editText.isEnabled = isEnabled!!
        editText.alpha = 1.0f
    }

    fun dobValidityCheck(currentDate: String?, birthdateFromNadra: String?): Int {
        val mon: Int
        var year = 0
        //  loadsave();
        try {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
            val date1 = simpleDateFormat.parse(currentDate)
            val date2 = simpleDateFormat.parse(birthdateFromNadra)
            val days = ((date1.time - date2.time) / (1000 * 60 * 60 * 24)).toInt()
            mon = days / 30
            year = days / 365
            Log.i("Testing", "days: $days")
            Log.i("Testing", "months:$mon")
            Log.i("Testing", "years:$year")
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return year
    }

    fun yearCurrentValidate(currentDate: String?): Int {
        val mon: Int
        var year = 0
        try {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
            val date1 = simpleDateFormat.parse(currentDate)
            val date2 = simpleDateFormat.parse(currentDate)
            val days = ((date1.time - date2.time) / (1000 * 60 * 60 * 24)).toInt()
            mon = days / 30
            year = days / 365
            Log.i("Testing", "days: $days")
            Log.i("Testing", "months:$mon")
            Log.i("Testing", "years:$year")
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return year
    }

    fun yearFromDOBValidate(issueDate: String?, dob: String?): Int {
        val mon: Int
        var year = 0
        //  loadsave();
        try {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
            val date1 = simpleDateFormat.parse(issueDate)
            val date2 = simpleDateFormat.parse(dob)
            val days = ((date1.time - date2.time) / (1000 * 60 * 60 * 24)).toInt()
            mon = days / 30
            year = days / 365
            Log.i("Testing", "days: $days")
            Log.i("Testing", "months:$mon")
            Log.i("Testing", "years:$year")
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return year
    }

    fun compareDateValidate(issueDate: String?, dob: String?): Boolean {
        //  loadsave();
        var check = false
        try {
            val simpleDateFormat = SimpleDateFormat("dd-MM-yyyy")
            val date1 = simpleDateFormat.parse(issueDate)
            val date2 = simpleDateFormat.parse(dob)
            check = !date1.after(date2)
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return check
    }

    fun parseFormate(time: String?): String? {
        val inputPattern = "dd-m-yyyy"
        val outputPattern = "dd-MM-yyyy"
        val inputFormat = SimpleDateFormat(inputPattern)
        val outputFormat = SimpleDateFormat(outputPattern)
        var date: Date? = null
        var str: String? = null
        try {
            date = inputFormat.parse(time)
            str = outputFormat.format(date)
        } catch (e: ParseException) {
            e.printStackTrace()
        }
        return str
    }

    fun edittextTypeCount(editText: EditText, count: Int, type: Int) {
        if (type == Constants.INPUT_TYPE_NUMBER) {
            editText.inputType =
                InputType.TYPE_CLASS_NUMBER or InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
            val FilterArray = arrayOfNulls<InputFilter>(1)
            FilterArray[0] = InputFilter.LengthFilter(count)
            editText.filters = FilterArray
            editText.keyListener =
                DigitsKeyListener.getInstance(
                    Companion.applicationContext!!.resources.getString(
                        R.string.numericKeys
                    )
                )
        } else if (type == Constants.INPUT_TYPE_ALPHANUMERIC) {
            editText.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            editText.filters = arrayOf(
                alphaNumericFilterEdittext,
                InputFilter.LengthFilter(count)
            )
        } else if (type == Constants.INPUT_TYPE_ALPHA) {
            editText.keyListener =
                DigitsKeyListener.getInstance(
                    Companion.applicationContext!!.resources.getString(
                        R.string.alphaKeys
                    )
                )
            editText.inputType = InputType.TYPE_CLASS_TEXT
            val FilterArray = arrayOfNulls<InputFilter>(1)
            FilterArray[0] = InputFilter.LengthFilter(count)
            editText.filters = FilterArray
        } else if (type == Constants.INPUT_TYPE_ALPHA_BLOCK) {
            editText.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            editText.filters = arrayOf(
                alphaFilterEdittext,
                InputFilter.LengthFilter(count)
            )
        } else if (type == Constants.INPUT_TYPE_ALPHA_BLOCK_LASTNAME) {
            editText.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            editText.filters = arrayOf(
                alphaFilterEdittextLastName,
                InputFilter.LengthFilter(count)
            )
        } else if (type == 4) {
            editText.inputType = InputType.TYPE_CLASS_TEXT
            editText.filters = arrayOf(
                customEmailAlphaFilters,
                InputFilter.LengthFilter(count)
            )
        } else if (type == Constants.INPUT_TYPE_CUSTOM_EMAIL_ALPHA) {
            editText.keyListener =
                DigitsKeyListener.getInstance(
                    Companion.applicationContext!!.resources.getString(
                        R.string.emailKeys
                    )
                )
            editText.inputType = InputType.TYPE_CLASS_TEXT
            val FilterArray = arrayOfNulls<InputFilter>(1)
            FilterArray[0] = InputFilter.LengthFilter(count)
            editText.filters = FilterArray
        } else if (type == Constants.INPUT_TYPE_CUSTOM_ADDRESS_ALPHANUMERIC) {
            /*  editText.setInputType(InputType.TYPE_CLASS_TEXT);
            InputFilter[] FilterArray = new InputFilter[1];
            FilterArray[0] = new InputFilter.LengthFilter(count);
            editText.setFilters(FilterArray);*/
            editText.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            editText.filters = arrayOf(
                alphaNumericAddressFilterSpaceEdittext,
                InputFilter.LengthFilter(count)
            )
        } else if (type == Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC) {
            editText.inputType = InputType.TYPE_TEXT_FLAG_CAP_CHARACTERS
            editText.filters = arrayOf(
                alphaNumericFilterSpaceEdittext,
                InputFilter.LengthFilter(count)
            )
        }
    }

    class AlphaNumericInputFilter : InputFilter {
        override fun filter(
            source: CharSequence, start: Int, end: Int,
            dest: Spanned, dstart: Int, dend: Int,
        ): CharSequence {

            // Only keep characters that are alphanumeric
            val builder = StringBuilder()
            for (i in start until end) {
                val c = source[i]
                if (Character.isLetterOrDigit(c)) {
                    builder.append(c)
                }
            }

            // If all characters are valid, return null, otherwise only return the filtered characters
            val allCharactersValid = builder.length == end - start
            return if (allCharactersValid) null.toString() else builder.toString()
        }
    }

    fun onKeyboardEdittext(editText: EditText) {
        editText.requestFocus()
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(editText, InputMethodManager.SHOW_FORCED)
        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    var numericFilterEdittext =
        label@ InputFilter { source: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[1234567890B]*").matcher(source[i].toString()).matches()) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var numericDashFilterEdittext =
        label1@ InputFilter { source: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[1234567890-]*").matcher(source[i].toString()).matches()) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var alphaNumericFilterEdittext =
        label@ InputFilter { src: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[A-Z0-9]").matcher(src[i].toString()).matches()) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var alphaNumericFilterSpaceEdittext =
        label@ InputFilter { src: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[A-Z0-9 ]").matcher(src[i].toString()).matches()) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var alphaNumericAddressFilterSpaceEdittext =
        label@ InputFilter { src: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[A-Z 0-9!#$%&(){|}~:;<=>?@*+,./^_`\\'\\\" \\t\\r\\n\\f-]+")
                        .matcher(
                            src[i].toString()
                        ).matches()
                ) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var alphaFilterEdittext =
        label@ InputFilter { src: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[A-Z ]").matcher(src[i].toString()).matches()) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var alphaFilterEdittextLastName =
        label@ InputFilter { src: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[A-Z_ ]").matcher(src[i].toString()).matches()) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var customAlphaFilters =
        label@ InputFilter { source: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz._ ]*")
                        .matcher(
                            source[i].toString()
                        ).matches()
                ) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var customAlphaFiltersblock =
        label@ InputFilter { source: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789~`@#$%^&/()_+='.>,<]")
                        .matcher(
                            source[i].toString()
                        ).matches()
                ) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var customEmailAlphaFilters =
        label@ InputFilter { source: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (Pattern.compile("[a-zA-Z0-9._-]+@[a-z]+\\\\.+[a-z]+]*")
                        .matcher(source[i].toString()).matches()
                ) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }
    var customAddressAlphaFilters =
        label@ InputFilter { source: CharSequence, start: Int, end: Int, dest: Spanned?, dstart: Int, dend: Int ->
            var i = start
            while (i < end) {
                if (!Pattern.compile("[ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz 0123456789~`@#$%^&/()_+='.>,<]*")
                        .matcher(
                            source[i].toString()
                        ).matches()
                ) {
                    return@InputFilter ""
                }
                ++i
            }
            null
        }

    fun findSpinnerPositionFromCountryCode(
        listToFindFrom: List<Country>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].COUNTRY_NAME.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].COUNTRY_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromCountryCodeGetIndex(
        listToFindFrom: List<Country>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].COUNTRY_NAME.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCountryCodeIndex(
        listToFindFrom: List<Country>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].COUNTRY_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCountryNameSetCode(
        listToFindFrom: List<Country>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].COUNTRY_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCapacityCode(
        listToFindFrom: List<Capacity>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CAPACITY_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].CAPACITY_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromCapacityIndex(
        listToFindFrom: List<Capacity>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CAPACITY_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromPurposeOfTinCode(
        listToFindFrom: List<PurposeOfTin>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PURP_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PURP_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromPurposeOfTinIndex(
        listToFindFrom: List<PurposeOfTin>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PURP_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromRelationshipCode(
        listToFindFrom: List<Relationship>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].RELATION_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].RELATION_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromRelationshipIndex(
        listToFindFrom: List<Relationship>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].RELATION_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromRelationshipByDescIndex(
        listToFindFrom: List<Relationship>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].RELATION_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromReasonPrefEDDCode(
        listToFindFrom: List<ReasonPrefEDD>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].REASON_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].REASON_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromReasonPrefEDDIndex(
        listToFindFrom: List<ReasonPrefEDD>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].REASON_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromDocTypeCode(
        listToFindFrom: List<DocsData>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].iDDOCDESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].iDDOCCODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromDocTypeIndexValue(
        listToFindFrom: List<DocsData>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].iDDOCDESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCompanyNameIndexValue(
        listToFindFrom: List<HawCompanyData>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if ((listToFindFrom[i].OrgCode + " - " + listToFindFrom[i].NameOfCompanyOrEmployer).equals(
                        strToFind,
                        ignoreCase = true
                    )
                ) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCompanyNameValue(
        listToFindFrom: List<HawCompanyData>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].NameOfCompanyOrEmployer.equals(
                        strToFind,
                        ignoreCase = true
                    )
                ) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromBranchRegionValue(
        listToFindFrom: List<BranchRegionCode>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if ((listToFindFrom[i].Branch_Code + " - " + listToFindFrom[i].Branch_Name).equals(
                        strToFind,
                        ignoreCase = true
                    )
                ) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromBranchRegionDESC(
        listToFindFrom: List<BranchRegionCode>?,
        strToFind: String?,
    ): String {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if ((listToFindFrom[i].Branch_Code + " - " + listToFindFrom[i].Branch_Name).equals(
                        strToFind,
                        ignoreCase = true
                    )
                ) {
                    return listToFindFrom[i].Branch_Code + "-" + listToFindFrom[i].Branch_Name!! + "-" + listToFindFrom[i].Region_Code + "-" + listToFindFrom[i].Region_Name
                }
            }
        }
        return ""
    }


    fun findSpinnerPositionFromDocTypeIndex(
        listToFindFrom: List<DocsData>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].iDDOCCODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun isValidNumber(number: String): Boolean {
        val getNumber = number.substring(0, 1)
        return getNumber === "03"
    }

    fun findSpinnerPositionFromCustomerSegmentCode(
        listToFindFrom: List<CustomerSegment>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].sEGMENTDESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].cUSTSEGMENT
                }
            }
        }
        return ""
    }

    fun findSpinnerIndexFromCustomerSegmentCode(
        listToFindFrom: List<CustomerSegment>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].sEGMENTDESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromOccupationCode(
        listToFindFrom: List<Profession>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PROF_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PROF_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromOccupationIndex(
        listToFindFrom: List<Profession>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PROF_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromNationOfBusinessCode(
        listToFindFrom: List<NatureOfBusiness>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].BUS_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].BUS_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromNationOfBusinessIndex(
        listToFindFrom: List<NatureOfBusiness>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].BUS_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromTitleCode(
        listToFindFrom: List<Title>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].TITLE_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].TITLE_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromTitleIndex(listToFindFrom: List<Title>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].TITLE_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromOpTypeIndex(
        listToFindFrom: List<AccountOperationsType>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].ACCT_OPER_TYPE_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromAccTypeIndex(
        listToFindFrom: List<AccountType>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].ACCT_TYPE_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromPuposeOfAccountIndex(
        listToFindFrom: List<PurposeOfAccount>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PUR_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromEstateFreqIndex(
        listToFindFrom: List<EstateFrequency>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].E_STATE_FRQ.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromOperInstIndex(
        listToFindFrom: List<AccountOperationsInst>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].OPER_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromestateIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromprefIndex(
        listToFindFrom: List<PrefComMode>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].COMMU_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromecardTypeIndex(
        listToFindFrom: List<CardProduct>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PRD_NAME.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCardTypeIndex(
        listToFindFrom: List<CardProduct>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PRD_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }


    fun findSpinnerPositionFromNisaIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFrompoaIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_NAME.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromERIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_NAME.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromNisaReqIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromInternetIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromfreeINsIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromZakatIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromSMSIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromPBIndex(listToFindFrom: List<Bool>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromDepTypeIndex(
        listToFindFrom: List<DepositType>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].DEP_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCurrencyIndex(
        listToFindFrom: List<Currency>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CURR_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromPriorityIndex(
        listToFindFrom: List<Priority>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PRITY_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromGenderCode(
        listToFindFrom: List<Gender>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].GENDER_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].GENDER_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromGenderIndex(listToFindFrom: List<Gender>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].GENDER_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromMeritalStatusCode(
        listToFindFrom: List<MaritalStatus>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].MARITAL_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].MARITAL_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromMeritalStatusIndex(
        listToFindFrom: List<MaritalStatus>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].MARITAL_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCurrencyOfFundCode(
        listToFindFrom: List<Currency>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CURR_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].CURR_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromCurrencyOfCode(
        listToFindFrom: List<Currency>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CURR_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].CURR
                }
            }
        }
        return ""
    }

    fun findSpinnerCodeFromAccountType(
        listToFindFrom: List<AccountType>?,
        strToFind: String?,
    ): String? {
        var account: String? = ""
        try {
            if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
                for (i in listToFindFrom.indices) {
                    if (listToFindFrom[i].ACCT_TYPE_DESC.equals(strToFind, ignoreCase = true)) {
                        account = listToFindFrom[i].ACCT_TYPE
                    }
                }
            }
        } catch (e: Exception) {
            Log.i("SystemException", e.message!!)
        } finally {
            return account
        }
    }

    fun findSpinnerPositionFromDepositTypeCode(
        listToFindFrom: List<DepositType>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].DEP_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].DEP_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromPurposeOfAccountCode(
        listToFindFrom: List<PurposeOfAccount>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PUR_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PUR_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerKonnectPositionFromPurposeOfAccountCode(
        listToFindFrom: List<KonnectPurposeOfAccount>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PUR_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PUR_ID
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromSourceOfFundCode(
        listToFindFrom: List<SourceOffund>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOF_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].SOF_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromSourceOfFundIndex(
        listToFindFrom: List<SourceOffund>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOF_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromSourceOfFundLandLordCode(
        listToFindFrom: List<SourceOfIncomeLandLord>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOI_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].SOI_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromSourceOfFundLandLordIndex(
        listToFindFrom: List<SourceOfIncomeLandLord>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOI_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromIndustryMainCategoryCode(
        listToFindFrom: List<IndustryMainCategory>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOIIC_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].SOIIC_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromIndustryMainCategoryIndex(
        listToFindFrom: List<IndustryMainCategory>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOIIC_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromIndustryHawMainCategoryIndex(
        listToFindFrom: List<IndustryMainCategory>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOIIC_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromIndustryMainCategoryUsingCodeIndex(
        listToFindFrom: List<IndustryMainCategory>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOIIC_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromIndustrySubCategoryCode(
        listToFindFrom: List<IndustrySubCategory>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOIISC_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].SOIISC_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromIndustrySubCategoryIndex(
        listToFindFrom: List<IndustrySubCategory>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOIISC_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromHawIndustrySubCategoryIndex(
        listToFindFrom: List<IndustrySubCategory>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOIISC_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromIndustrySubCategoryUsingCodeIndex(
        listToFindFrom: List<IndustrySubCategory>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOIISC_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromModeOfTansCode(
        listToFindFrom: List<ModeOfTans>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].TRAN_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].TRAN_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromModeOfTansIndex(
        listToFindFrom: List<ModeOfTans>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].TRAN_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromBoolCode(listToFindFrom: List<Bool>?, strToFind: String): String? {
        var value: String? = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_NAME == strToFind) {
                    value = listToFindFrom[i].LB_CODE
                    break
                }
            }
        }
        Log.i("booleanCode", " $value")
        return value
    }

    fun findSpinnerPositionFromNISABoolCode(
        listToFindFrom: List<Bool>?,
        strToFind: String
    ): String? {
        var value: String? = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_NAME == strToFind) {
                    value = listToFindFrom[i].LB_CODE
                    break
                }
            }
        }
        Log.i("booleanCode", " $value")
        return value
    }

    fun findSpinnerPositionFromDebitCardBoolCode(
        listToFindFrom: List<Bool>?,
        strToFind: String
    ): String? {
        var value: String? = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_NAME == strToFind) {
                    value = listToFindFrom[i].LB_CODE
                    break
                }
            }
        }
        Log.i("booleanCode", " $value")
        return value
    }

    fun findSpinnerPositionFromBoolHoldMailInstructionCode(
        listToFindFrom: List<Bool>?,
        strToFind: String
    ): String? {
        var value: String? = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_NAME == strToFind) {
                    value = listToFindFrom[i].LB_CODE
                    break
                }
            }
        }
        Log.i("booleanCode", " $value")
        return value
    }

    fun findSpinnerPositionFromBoolDESC(listToFindFrom: List<Bool>?, strToFind: String): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE == strToFind) {
                    return i
                    break
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromPreferenceMailingCode(
        listToFindFrom: ArrayList<PreferencesMailing>?,
        strToFind: String,
    ): String {
        var value = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PREFERRED_ADDR == strToFind) {
                    value = listToFindFrom[i].PREFERRED_CODE
                    break
                }
            }
        }
        Log.i("booleanCode", " $value")
        return value
    }

    fun findSpinnerPositionFromPreferenceMailingIndex(
        listToFindFrom: List<PreferencesMailing>?,
        strToFind: String,
    ): Int {
        val value = 0
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PREFERRED_ADDR == strToFind) {
                    return i
                }
            }
        }
        Log.i("booleanCode", " $value")
        return 0
    }

    fun findSpinnerPositionFromPepCateCode(
        listToFindFrom: List<PepCategories>?,
        strToFind: String,
    ): String? {
        var value: String? = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PEP_CATG_DESC == strToFind) {
                    value = listToFindFrom[i].PEP_CATG_CODE
                    break
                }
            }
        }
        Log.i("booleanCode", " $value")
        return value
    }

    fun findSpinnerPositionFromPepCateIndex(
        listToFindFrom: List<PepCategories>?,
        strToFind: String,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PEP_CATG_DESC == strToFind) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromPepSubCateCode(
        listToFindFrom: List<PepSubCategories>?,
        strToFind: String,
    ): String? {
        var value: String? = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PEP_SUB_CATG_DESC == strToFind) {
                    value = listToFindFrom[i].PEP_SUB_CATG_CODE
                    break
                }
            }
        }
        Log.i("booleanCode", " $value")
        return value
    }

    fun findSpinnerPositionFromPepSubCateIndex(
        listToFindFrom: List<PepSubCategories>?,
        strToFind: String,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PEP_SUB_CATG_DESC == strToFind) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromBoolDesc(listToFindFrom: List<Bool>?, strToFind: String): String? {
        var value: String? = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE == strToFind) {
                    value = listToFindFrom[i].LB_CODE
                    break
                }
            }
        }
        Log.i("booleanDesc", " $value")
        return value
    }

    fun findSpinnerPositionFromBoolPepDesc(
        listToFindFrom: List<Bool>?,
        strToFind: String
    ): String? {
        var value: String? = ""
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_NAME == strToFind) {
                    value = listToFindFrom[i].LB_CODE
                    break
                }
            }
        }
        Log.i("booleanDesc", " $value")
        return value
    }

    fun findSpinnerPositionFromBoolIndex(listToFindFrom: List<Bool>?, strToFind: String): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE == strToFind) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromHoldMailBoolIndex(
        listToFindFrom: List<Bool>?,
        strToFind: String
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_CODE == strToFind) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerSBPConsumerListPositionFromBoolIndex(
        listToFindFrom: List<Bool>?,
        strToFind: String
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].LB_NAME == strToFind) {
                    return i
                }
            }
        }
        return 0
    }


    fun findSpinnerPositionFromEstateFrequencyCode(
        listToFindFrom: List<EstateFrequency>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].E_STATE_FRQ.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].E_STATE_FRQ_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromSourceOfWealthCode(
        listToFindFrom: List<SourceOfWealth>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOW_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].SOW_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromSourceOfWealthIndex(
        listToFindFrom: List<SourceOfWealth>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SOW_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromPepCode(
        listToFindFrom: List<PurposeCIF>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PURP_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PURP_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromPepResonseCode(
        listToFindFrom: List<Pep>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].REASON_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].REASON_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromPepResonseIndex(
        listToFindFrom: List<Pep>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].REASON_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromPepCategoriesCode(
        listToFindFrom: List<PepCategories>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PEP_CATG_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PEP_CATG_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromCityDESC(listToFindFrom: List<City>?, strToFind: String?): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CITY_NAME.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].CITY_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromCityCode(listToFindFrom: List<City>?, strToFind: String?): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CITY_NAME.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].CITY_CODE
                }
            }
        }
        return ""
    }


    fun findSpinnerPositionFromCityIndex(listToFindFrom: List<City>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CITY_NAME.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCityCRSIndex(listToFindFrom: List<City>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CITY_CODE.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromAddressTypeCode(
        listToFindFrom: List<AddressType>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].ADDR_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].ADDR_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromAddressTypeIndex(
        listToFindFrom: List<AddressType>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].ADDR_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromSBPCode(
        listToFindFrom: List<SBPCodes>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SBP_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].SBP_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromSBPIndex(listToFindFrom: List<SBPCodes>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SBP_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromRMCode(listToFindFrom: List<RMCodes>?, strToFind: String?): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].C2ACO.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].C2RNM
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromChequeBookLeaves(
        listToFindFrom: List<ChequeBookLeaves>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CHEQUEBOOK_LEAVES_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].CHEQUEBOOK_LEAVES
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromModeOfDelivery(
        listToFindFrom: List<MODelivery>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].MODE_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].MODE
                }
            }
        }
        return ""
    }

    fun findSpinnerIndexFromRMCode(listToFindFrom: List<RMCodes>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].C2RNM.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerIndexFromRMDESC(listToFindFrom: List<RMCodes>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].C2ACO.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerIndexFromModeOfDelivery(
        listToFindFrom: List<MODelivery>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].MODE_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerIndexFromChequeBookLeaves(
        listToFindFrom: List<ChequeBookLeaves>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].CHEQUEBOOK_LEAVES_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromSundryCode(
        listToFindFrom: List<Sundry>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if ((listToFindFrom[i].SUNDRY_DESC + ", " + listToFindFrom[i].SUNDRY_CODE).equals(
                        strToFind,
                        ignoreCase = true
                    )
                ) {
                    return listToFindFrom[i].SUNDRY_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromSundryIndex(listToFindFrom: List<Sundry>?, strToFind: String?): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].SUNDRY_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromCustomerProductCode(
        listToFindFrom: List<ConsumerProduct>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PRDT_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PRDT_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromCustomerProductIndex(
        listToFindFrom: List<ConsumerProduct>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PRDT_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromProvinceCode(
        listToFindFrom: List<Province>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PR_NAME.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PR_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromProvinceIndex(
        listToFindFrom: List<Province>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PR_NAME.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromBioAccountTypeCode(
        listToFindFrom: List<BioAccountType>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].BAT_NAME.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].BAT_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromBioAccountTypeIndex(
        listToFindFrom: List<BioAccountType>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].BAT_NAME.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun findSpinnerPositionFromAccountTypeCode(
        listToFindFrom: List<AccountType>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].ACCT_TYPE_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].ACCT_TYPE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromAccountIslamic(
        listToFindFrom: List<AccountType>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].ACCT_TYPE_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].Convential_Islamic
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromAccountOperationTypeCode(
        listToFindFrom: List<AccountOperationsType>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].ACCT_OPER_TYPE_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].ACCT_OPER_TYPE_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromPriorityCode(
        listToFindFrom: List<Priority>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PRITY_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PRITY_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromAccountOperationInstCode(
        listToFindFrom: List<AccountOperationsInst>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].OPER_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].OPER_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromPrefComModeCode(
        listToFindFrom: List<PrefComMode>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].COMMU_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].COMMU_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromCardProductCode(
        listToFindFrom: List<CardProduct>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PRD_NAME.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PRD_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromPURPOFCode(
        listToFindFrom: ArrayList<PurposeCIF>?,
        strToFind: String?,
    ): String? {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PURP_DESC.equals(strToFind, ignoreCase = true)) {
                    return listToFindFrom[i].PURP_CODE
                }
            }
        }
        return ""
    }

    fun findSpinnerPositionFromPURPOFIndex(
        listToFindFrom: ArrayList<PurposeCIF>?,
        strToFind: String?,
    ): Int {
        if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
            for (i in listToFindFrom.indices) {
                if (listToFindFrom[i].PURP_DESC.equals(strToFind, ignoreCase = true)) {
                    return i
                }
            }
        }
        return 0
    }

    fun showConfirmationDialog(
        context: Context?,
        header: String?,
        message: String?,
        posTitle: String?,
        negTitle: String?,
        callback: DialogCallback?,
    ) {
        val dialog: Dialog = TwoButtonsDialog(
            context!!,
            header!!,
            message!!,
            posTitle!!,
            negTitle!!,
            (callback as com.hbl.bot.ui.customviews.DialogCallback?)!!
        )
        dialog.show()
    }

    fun showLoader(context: Context?) {
        /*  mProgressDialog = new TransparentProgressDialog(context);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();*/
    }

    fun showDialog(context: Context?) {
        progressDialog = getProgressDialogInstance(context)
        progressDialog!!.setCancelable(false)
        progressDialog!!.show()
    }

    fun hideLoader() {
        if (progressDialog != null && progressDialog!!.isShowing) {
            progressDialog!!.cancel()
            progressDialog = null
        }

    }

    fun getAndroidVersion(sdk: Int): String? {
        return when (sdk) {
            1 -> "(Android 1.0)"
            2 -> "Petit Four " + "(Android 1.1)"
            3 -> "Cupcake " + "(Android 1.5)"
            4 -> "Donut " + "(Android 1.6)"
            5 -> "Eclair " + "(Android 2.0)"
            6 -> "Eclair " + "(Android 2.0.1)"
            7 -> "Eclair " + "(Android 2.1)"
            8 -> "Froyo " + "(Android 2.2)"
            9 -> "Gingerbread " + "(Android 2.3)"
            10 -> "Gingerbread " + "(Android 2.3.3)"
            11 -> "Honeycomb " + "(Android 3.0)"
            12 -> "Honeycomb " + "(Android 3.1)"
            13 -> "Honeycomb " + "(Android 3.2)"
            14 -> "Ice Cream Sandwich " + "(Android 4.0)"
            15 -> "Ice Cream Sandwich " + "(Android 4.0.3)"
            16 -> "Jelly Bean " + "(Android 4.1)"
            17 -> "Jelly Bean " + "(Android 4.2)"
            18 -> "Jelly Bean " + "(Android 4.3)"
            19 -> "KitKat " + "(Android 4.4)"
            20 -> "KitKat Watch " + "(Android 4.4)"
            21 -> "Lollipop " + "(Android 5.0)"
            22 -> "Lollipop " + "(Android 5.1)"
            23 -> "Marshmallow " + "(Android 6.0)"
            24 -> "Nougat " + "(Android 7.0)"
            25 -> "Nougat " + "(Android 7.1.1)"
            26 -> "Oreo " + "(Android 8.0)"
            27 -> "Oreo " + "(Android 8.1)"
            28 -> "Pie " + "(Android 9.0)"
            29 -> "Q " + "(Android 10.0)"
            30 -> "R " + "(Android 11.0)"
            31 -> "S " + "(Android 12.0)"
            else -> ""
        }
    }

    fun datepicker(context: Context?, listener: DatePickerDialog?) {
        val now = Calendar.getInstance()
        val datePickerDialog = DatePickerDialog(context!!, { view, year, month, dayOfMonth ->
            mYear = year
            mMonth = month
            mDayOfMonth = dayOfMonth
        }, now[Calendar.YEAR], now[Calendar.MONTH], now[Calendar.DAY_OF_MONTH])
        datePickerDialog.show()
    }

    fun error_box(context: Context): View {
        val li = LayoutInflater.from(context)
        val promptsView = li.inflate(R.layout.popup_error_box, null)
        alertDialogBuilder = AlertDialog.Builder(context)
        alertDialog = alertDialogBuilder!!.create()
        // set prompts.xml to alertdialog builder
        alertDialogBuilder!!.setView(promptsView)
        // create alert dialog
        val toolbar: Toolbar = promptsView.findViewById(R.id.toolbar)
        toolbar.subtitle = "Alert"
        toolbar.title = "Finger Print Device"
        //        toolbar.setNavigationIcon(R.drawable.ic_download);
        toolbar.setTitleTextColor(context.resources.getColor(R.color.colorPrimary))
        toolbar.setSubtitleTextColor(context.resources.getColor(R.color.colorPrimary))
        error_box1 = promptsView.findViewById(R.id.error1)
        error_box2 = promptsView.findViewById(R.id.error2)
        error_box3 = promptsView.findViewById(R.id.error3)
        cancel_action = promptsView.findViewById(R.id.cancel_action)
        cancel_action!!.setOnClickListener(View.OnClickListener {
            alertDialog!!.dismiss()
            hideLoader()
        })
        return promptsView
    }

    fun showConfirmationDialog(
        context: Context?,
        title: String?,
        message: String?,
        posTitle: String?,
        negTitle: String?,
        callback: com.hbl.bot.ui.customviews.DialogCallback?,
    ) {
        val dialog =
            TwoButtonsDialog(context!!, title!!, message!!, posTitle!!, negTitle!!, callback!!)
        dialog.setCancelable(false)
        dialog.show()
    }

    fun showSingleBtnConfirmationDialog(
        context: Context?,
        title: String?,
        message: String?,
        posTitle: String?,
        callback: Dialog2Callback?,
    ) {
        val dialog = SingleButtonsDialog(context!!, title!!, message!!, posTitle!!, callback!!)
        dialog.setCancelable(false)
        dialog.show()
    }

    fun showSessionDialog(
        context: Context?,
        message: String?,
        posTitle: String?,
        callback: DialogCallback?,
    ) {
        val dialog = OneButtonsDialog(context, message, posTitle, callback)
        dialog.show()
    }

    fun dismissDialog() {
        progressDialog!!.dismiss()
    }

    fun startUserSession() {
        cancelTimer()
        timer = Timer()
        timer!!.schedule(object : TimerTask() {
            override fun run() {
//                listener.onSessionLogout();
            }
        }, Constants.SESSION_DISCONNECT_TIMEOUT)
    }

    private fun cancelTimer() {
        if (timer != null) timer!!.cancel()
    }

    fun onUserInteracted() {
        startUserSession()
    }

    fun openFile(context: Context, file: File) {
        val uri = FileProvider.getUriForFile(
            Companion.applicationContext!!,
            "com.hbl.hr.provider",
            File(file.path)
        )
        try {
            val intentUrl = Intent(Intent.ACTION_VIEW)
            intentUrl.setDataAndType(uri, "application/pdf")
            intentUrl.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            intentUrl.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
            context.startActivity(intentUrl)
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(this, "No PDF Viewer Installed", 1)
        }
    }


    /**
     * diff methods for root device check */
    /*

 / *   */
    /** @author Kevin Kowalewski
     */
    /*
    public class RootUtil {
        public static boolean isDeviceRooted() {
            return checkRootMethod1() || checkRootMethod2() || checkRootMethod3();
        }

        private static boolean checkRootMethod1() {
            String buildTags = android.os.Build.TAGS;
            return buildTags != null && buildTags.contains("test-keys");
        }

        private static boolean checkRootMethod2() {
            String[] paths = { "/system/app/Superuser.apk", "/sbin/su", "/system/bin/su", "/system/xbin/su", "/data/local/xbin/su", "/data/local/bin/su", "/system/sd/xbin/su",
                    "/system/bin/failsafe/su", "/data/local/su", "/su/bin/su"};
            for (String path : paths) {
                if (new File(path).exists()) return true;
            }
            return false;
        }

        private static boolean checkRootMethod3() {
            Process process = null;
            try {
                process = Runtime.getRuntime().exec(new String[] { "/system/xbin/which", "su" });
                BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
                if (in.readLine() != null) return true;
                return false;
            } catch (Throwable t) {
                return false;
            } finally {
                if (process != null) process.destroy();
            }
        }
    }*/
    /**
     * diff methods for root device check
     */
    /*
    public static boolean isRooted(Context context) {
    boolean isEmulator = isEmulator(context);
    String buildTags = Build.TAGS;
    if(!isEmulator && buildTags != null && buildTags.contains("test-keys")) {
        return true;
    } else {
        File file = new File("/system/app/Superuser.apk");
        if(file.exists()) {
            return true;
        } else {
            file = new File("/system/xbin/su");
            return !isEmulator && file.exists();
        }
    }
}*/

    companion object {
        var channel_ID = ""
        var acctoken = ""
        var trackingID = ""
        var latlng = ""
        var appName = ""
        var versionCode = ""
        var versionName = ""
        var apkSize = ""
        var appCreatedDate = ""
        var appUpdatedDate = ""
        var mysisiID = ""
        var userLogin = ""
        const val CHANNEL_ID = "exampleServiceChannel"
        private val consumerStore: GlobalClass? = null
        val instance: GlobalClass?
            get() = consumerStore ?: GlobalClass()
        var loaderTime: Long = 30000
        var mYear = 0
        var mMonth = 0
        var mDayOfMonth = 0
        var CINC: String? = null
        var workFlowCode: String? = null
        var workFlowDesc: String? = null
        var selectedItem = 0
        const val KEY_TOKEN = "KEY_TOKEN"

        @JvmField
        var otp = ""

        @JvmField
        var verifyEmpEmail = ""

        @JvmField
        var resendTime = ""
        var DRToken = ""

         fun clearAppData(context: Context) {
            try {
                if (Build.VERSION_CODES.KITKAT <= Build.VERSION.SDK_INT) {
                    ((context as NavigationDrawerActivity).getSystemService(ACTIVITY_SERVICE) as ActivityManager).clearApplicationUserData()
                } else {
                    Runtime.getRuntime()
                        .exec("pm clear " +(context as NavigationDrawerActivity).getApplicationContext().getPackageName())
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

        fun deleteCache(context: Context) {
            try {
                val dir = context.cacheDir
                deleteDir(dir)
            } catch (e: java.lang.Exception) {
                e.printStackTrace()
            }
        }

        fun isGooglePlayServicesAvailable(activity: Activity?): Boolean {
            val googleApiAvailability = GoogleApiAvailability.getInstance()
            val status = googleApiAvailability.isGooglePlayServicesAvailable(activity)
            if (status != ConnectionResult.SUCCESS) {
                return false
            }
            return true
        }

        fun deleteDir(dir: File?): Boolean {
            return if (dir != null && dir.isDirectory) {
                val children = dir.list()
                for (i in children.indices) {
                    val success = deleteDir(File(dir, children[i]))
                    if (!success) {
                        return false
                    }
                }
                dir.delete()
            } else if (dir != null && dir.isFile) {
                dir.delete()
            } else {
                false
            }
        }

        fun getIMEIDeviceId(context: Context): String? {
            var deviceId = ""
            try {
                val mTelephony = context.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (context.checkSelfPermission(Manifest.permission.READ_PHONE_STATE) !== PackageManager.PERMISSION_GRANTED) {
                        return ""
                    }
                }
                assert(mTelephony != null)
                if (mTelephony.imei != null) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                      deviceId=  mTelephony.imei
                    }
                }
                Log.d("deviceId", deviceId)
            } catch (e: UnsatisfiedLinkError) {
                deviceId=""
            } catch (e: SecurityException) {
                deviceId=""
            } catch (e: NullPointerException) {
                deviceId=""
            } catch (e: IllegalArgumentException) {
                deviceId=""
            } catch (e: NumberFormatException) {
                deviceId=""
            } catch (e: InterruptedException) {
                deviceId=""
            } catch (e: RuntimeException) {
                deviceId=""
            } catch (e: IOException) {
                deviceId=""
            } catch (e: FileNotFoundException) {
                deviceId=""
            } catch (e: ClassNotFoundException) {
                deviceId=""
            } catch (e: ActivityNotFoundException) {
                deviceId=""
            } catch (e: IndexOutOfBoundsException) {
                deviceId=""
            } catch (e: ArrayIndexOutOfBoundsException) {
                deviceId=""
            } catch (e: ClassCastException) {
                deviceId=""
            } catch (e: TypeCastException) {
                deviceId=""
            } catch (e: SecurityException) {
                deviceId=""
            } catch (e: IllegalStateException) {
                deviceId=""
            } catch (e: OutOfMemoryError) {
                deviceId=""
            } catch (e: RuntimeException) {
                deviceId=""
            } catch (e: Exception) {
                deviceId=""
            }
            return deviceId
        }

        fun deviceID(): String {
            return Settings.Secure.getString(
                applicationContext!!.contentResolver,
                Settings.Secure.ANDROID_ID
            )
        }


        var imei = ""
        var id_do: String? = null
        var checkApproval = "pending"
        var IDDOC = 1
        var NADRA = 0
        var ADDR = 0
        var UNSC = 0
        var AOFCIF = 0
        var TnC = 0
        var POI = 0
        var checksum: String? = null
        var EDDFILL = 0
        var SSCARD = 0
        var KFS = 0
        var MYMEET = 0
        var NEWCUST = 0
        var HTML: String? = null
        var PDF: String? = null
        var MeetTime: String? = null
        var Meetdate: String? = null
        var CallTime: String? = null
        var FilledEDD = false
        var Calldate: String? = null
        var fullCallDateTime: String? = null
        var fullMeetDateTime: String? = null

        @JvmField
        var applicationContext: Context? = null

        @JvmField
        var sharedPreferenceManager: SharedPreferenceManager? = null

        //    TransparentProgressDialog progressDialog;
        private var progressDialog: TransparentProgressDialog? = null
        var Base64PrintDoc: String? = null
        var isfromFilter = false

        @JvmStatic
        fun getCurrentTimeAndDate(format: String?): String {
            val sdf = SimpleDateFormat(format, Locale.getDefault())
            return sdf.format(Date())
        }

        fun getBasicData() {
            sharedPreferenceManager!!.aofAccountInfo.TRACKING_ID.let { trackingID = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.LAT_LNG)
                .let { latlng = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.CHANNEL_ID)
                .let { channel_ID = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.VERSION_CODE)
                .let { versionCode = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.VERSION_NAME)
                .let { versionName = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.TABLET_APP_SIZE)
                .let { apkSize = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.APP_CREATED_DATE)
                .let { appCreatedDate = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.APP_UPDATE_DATE)
                .let { appUpdatedDate = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.MYSISID)
                .let { mysisiID = it }
            sharedPreferenceManager!!.getStringFromSharedPreferences(SharedPreferenceManager.USER_LOGIN)
                .let { userLogin = it }
        }

        var isDummyDataTrue = false
        fun getDate(milliSeconds: Long, dateFormat: String?): String {
            // Create a DateFormatter object for displaying date in specified format.
            val formatter = SimpleDateFormat(dateFormat)

            // Create a calendar object that will convert the date and time value in milliseconds to date.
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            return formatter.format(calendar.time)
        }

        fun delFile(path: String) {
            val fdelete = File(path)
            if (fdelete.exists()) {
                if (fdelete.delete()) {
                    println("file Deleted :$path")
                } else {
                    println("file not Deleted :$path")
                }
            }
        }

        fun clearText(textView: TextView) {
            textView.text = ""
        }

        fun clearText(editText: EditText) {
            editText.setText("")
        }

        fun addYears(oldDate: String?, numberOfYears: Int): String {
            val numberOfDays = 365 * numberOfYears
            var dateFormat = SimpleDateFormat("dd-MM-yyyy")
            val c = Calendar.getInstance()
            try {
                c.time = dateFormat.parse(oldDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.add(Calendar.YEAR, numberOfYears)
            dateFormat = SimpleDateFormat("dd-MM-yyyy")
            val newDate = Date(c.timeInMillis)
            return dateFormat.format(newDate)
        }

        fun minusYears(oldDate: String?, numberOfYears: Int): String {
            val numberOfDays = 365 * numberOfYears
            var dateFormat = SimpleDateFormat("dd-MM-yyyy")
            val c = Calendar.getInstance()
            try {
                c.time = dateFormat.parse(oldDate)
            } catch (e: ParseException) {
                e.printStackTrace()
            }
            c.add(Calendar.YEAR, -numberOfYears)
            dateFormat = SimpleDateFormat("dd-MM-yyyy")
            val newDate = Date(c.timeInMillis)
            return dateFormat.format(newDate)
        }

        fun isNotSelectString(value1: String, value2: String): Boolean {
            return value1 != value2
        }

        @JvmStatic
        fun isValidEmail(target: String?): Boolean {
            return if (target == null) {
                false
            } else {
                //android Regex to check the email address Validation
                Patterns.EMAIL_ADDRESS.matcher(target).matches()
            }
        }


        fun findSpinnerPositionFromValue(listToFindFrom: List<*>?, strToFind: String?): Int {
            if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
                for (i in listToFindFrom.indices) {
                    if (listToFindFrom[i].toString().equals(strToFind, ignoreCase = true)) {
                        return i
                    }
                }
            }
            return 0
        }

        fun spinnerSearchList(listToFindFrom: List<*>?, strToFind: String?): Int {
            if (listToFindFrom != null && listToFindFrom.size > 0 && !TextUtils.isEmpty(strToFind)) {
                for (i in listToFindFrom.indices) {
                    if (listToFindFrom[i].toString().equals(strToFind, ignoreCase = true)) {
                        return i
                    }
                }
            }
            return 0
        }

        fun decodeBase64AndSetImage(img: String?): Bitmap? {
            return if (!TextUtils.isEmpty(img)) {
                val decodedString =
                    Base64.decode(img, Base64.DEFAULT)
                BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            } else {
                null
            }
        }

        fun getProgressDialogInstance(context: Context?): TransparentProgressDialog? {
            if (progressDialog == null) progressDialog = TransparentProgressDialog(
                context!!
            )
            return progressDialog
        }

        @JvmStatic
        fun textColor(text: String?, startIndex: Int, lastIndex: Int, textColor: Int): Spannable {
            val spannable: Spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(textColor), startIndex,
                lastIndex,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return spannable
        }

        fun textColor(text: String?, lastIndex: Int, textColor: Int): Spannable {
            val spannable: Spannable = SpannableString(text)
            spannable.setSpan(
                ForegroundColorSpan(textColor), 0,
                lastIndex,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            return spannable
        }

        fun setErrorMsg(msg: String, viewId: EditText) {
            val fgcspan = ForegroundColorSpan(Color.RED)
            val ssbuilder = SpannableStringBuilder(msg)
            ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
            viewId.error = ssbuilder
        }

        fun setErrorMsg(msg: String, viewId: EditText, icon: Int) {
            val icon2 = applicationContext!!.resources.getDrawable(icon)
            icon2.setBounds(0, 0, icon2.intrinsicWidth, icon2.intrinsicHeight)
            val fgcspan = ForegroundColorSpan(
                ContextCompat.getColor(
                    applicationContext!!, R.color.green
                )
            )
            val ssbuilder = SpannableStringBuilder(msg)
            ssbuilder.setSpan(fgcspan, 0, msg.length, 0)
            viewId.setError(ssbuilder, icon2)
        }// ignore

        // try executing commands
        // get from build info
        val isRooted: Boolean
            // check if /system/app/Superuser.apk is present
            get() {
                // get from build info
                val buildTags = Build.TAGS
                if (buildTags != null && buildTags.contains("test-keys")) {
                    return true
                }
                // check if /system/app/Superuser.apk is present
                try {
                    val file = File("/system/app/Superuser.apk")
                    if (file.exists()) {
                        return true
                    }
                } catch (e1: Exception) {
                    // ignore
                }
                // try executing commands
                return (canExecuteCommand("/system/xbin/which su")
                        || canExecuteCommand("/system/bin/which su") || canExecuteCommand("which su"))
            }

        // executes a command on the system
        private fun canExecuteCommand(command: String): Boolean {
            val executedSuccesfully: Boolean
            executedSuccesfully = try {
                Runtime.getRuntime().exec(command)
                true
            } catch (e: Exception) {
                false
            }
            return executedSuccesfully
        }

        fun getFileAPK(): File {
            val pm = applicationContext!!.packageManager
            val applicationInfo: ApplicationInfo =
                pm.getApplicationInfo(applicationContext!!.packageName, 0)
            val file: File = File(applicationInfo.publicSourceDir)
            return file
        }

        fun lastModifiedDate(): String {
            val pm = applicationContext!!.packageManager
            val packageInfo: PackageInfo =
                pm.getPackageInfo(
                    applicationContext!!.packageName,
                    PackageManager.GET_PERMISSIONS
                )
            val updateTime = Date(packageInfo.lastUpdateTime)
            val sdf = SimpleDateFormat("dd-MMM-yyyy hh:mm a")
            return sdf.format(updateTime)
        }

        fun firstModifiedDate(): String {
            val pm = applicationContext!!.packageManager
            val packageInfo: PackageInfo =
                pm.getPackageInfo(
                    applicationContext!!.packageName,
                    PackageManager.GET_PERMISSIONS
                )
            val installTime = Date(packageInfo.firstInstallTime)
            val sdf = SimpleDateFormat("dd-MMM-yyyy hh:mm a")
            return sdf.format(installTime)
        }

        fun getFolderSizeLabel(file: File): String? {
            val size =
                getFolderSize(file).toDouble() / 1000.0 // Get size and convert bytes into KB.
            return if (size >= 1024) {
                String.format("%.2f MB", (size / 1024))
            } else {
                String.format("%.2f KB", size)
            }
        }

        fun getSizeKBOrMB(size: Int): String? {
            var value = size / 1024
            return if (value >= 1024) {
                var value = value / 1024
                String.format("%d/MB", value)
            } else {
                String.format("%d/KB", size)
            }
        }

        fun getFolderSize(file: File): Long {
            var size: Long = 0
            if (file.isDirectory) {
                for (child in file.listFiles()) {
                    size += getFolderSize(child)
                }
            } else {
                size = file.length()
            }
            return size
        }
    }

}