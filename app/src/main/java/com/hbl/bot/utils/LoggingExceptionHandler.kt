package com.hbl.bot.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.hbl.bot.R
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.utils.mail.GMailSender
import com.hbl.bot.utils.managers.SharedPreferenceManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class LoggingExceptionHandler(
    private val context: Context
) : Thread.UncaughtExceptionHandler {
    var cnx: Context? = null
    var sharedPreferenceManager: SharedPreferenceManager? = null
    var username = ""
    var dateTime = ""
    private val rootHandler: Thread.UncaughtExceptionHandler = Thread.getDefaultUncaughtExceptionHandler()
    override fun uncaughtException(thread: Thread, ex: Throwable) {
        try {
            Log.d(
                TAG, """
     called for ${thread.name}
     ${ex.message}
     ${ex.stackTrace}
     ${ex.localizedMessage}
     ${ex.suppressed}
     ${ex.javaClass.simpleName}
     ${ex.cause}
     """.trimIndent()
            )
            username = this.sharedPreferenceManager?.loginData?.getUSERID().toString()
            dateTime = GlobalClass.getCurrentTimeAndDate(Constants.fomratDateTime).toString()
            sendEmail(
                this.context,
                "StackTrace: " + thread.stackTrace,
                "State: " + thread.state,
                "ThreadGroup: " + thread.threadGroup,
                "UncaugthExceptionHandler: " + thread.uncaughtExceptionHandler,
                "ID: " +ex.suppressed,
                "ClassName: " + ex.javaClass.simpleName,
                "Message: " +ex.cause+ex.message+"Username: $username CRASH ${ex.cause} DateTime: $dateTime",
                "LocalizedMessage: " + ex.localizedMessage,
            )
            Log.e(
                TAG, "Username: $username" + " CRASH" + "\n" + ex.cause + "\n" +
                        thread.stackTrace + "\n" +
                        thread.state + "\n" +
                        thread.threadGroup + "\n" +
                        thread.uncaughtExceptionHandler + "\n" +
                        thread.id + "\n" +
                        thread.name + "\n" +
                        ex.message + "\n" +
                        ex.localizedMessage + "\n" +
                        ex.suppressed + "\n" +
                        ex.javaClass.simpleName + "\n" +
                        ex.cause
            )
            // assume we would write each error in one file ...
        } catch (e: Exception) {
            sendEmail(
                this.context,
                "StackTrace: " + thread.stackTrace,
                "State: " + thread.state,
                "ThreadGroup: " + thread.threadGroup,
                "UncaugthExceptionHandler: " + thread.uncaughtExceptionHandler,
                "ID: " +ex.suppressed,
                "ClassName: " + ex.javaClass.simpleName,
                "Message: " +ex.cause+ex.message+"Username: $username CRASH ${ex.cause} DateTime: $dateTime",
                "LocalizedMessage: " + ex.localizedMessage,
            )
            Log.e(TAG, "Exception Logger failed!", e)
        }
    }

    companion object {
        private val TAG = LoggingExceptionHandler::class.java.simpleName
    }

    init {
        // we should store the current exception handler -- to invoke it for all not handled exceptions ...
        // we replace the exception handler now with us -- we will properly dispatch the exceptions ...
        Thread.setDefaultUncaughtExceptionHandler(this)
        this.cnx = context
        this.sharedPreferenceManager =  GlobalClass.sharedPreferenceManager
    }



    @SuppressLint("WrongConstant")
    fun sendEmail(
        context: Context,
        activityName: String,
        stackTrace: String,
        message: String,
        localizedMessage: String,
        suppressed: String,
        classSimpleName: String,
        cuase: String,
        methodName: String,
    ) {
        Log.i("SystemExceptions", message)
        ToastUtils.normalShowToast(GlobalClass.applicationContext,GlobalClass.applicationContext!!.getString(R.string.something_went_wrong),1)
        SendEmail.username = GlobalClass.sharedPreferenceManager?.loginData?.getUSERID().toString()
        SendEmail.dateTime = GlobalClass.getCurrentTimeAndDate(Constants.fomratDateTime).toString()
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                sender.sendMail("Username: ${SendEmail.username} CRASH $cuase DateTime: ${SendEmail.dateTime}\n",
                    "Username: ${SendEmail.username} CRASH " + cuase + " DateTime: ${SendEmail.dateTime}" + "\n" +
                            "MethodName: " + methodName + "\n" +
                            "Class/Activity: " + activityName + "\n" +
                            stackTrace + "\n" +
                            message + "\n" +
                            localizedMessage + "\n" +
                            suppressed + "\n" +
                            classSimpleName + "\n" +
                            cuase,
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", message)
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
            GlobalScope.launch(Dispatchers.Main) {
                var intent = Intent(context, NavigationDrawerActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.startActivity(intent)
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(10);
            }
        }.start()
    }


}