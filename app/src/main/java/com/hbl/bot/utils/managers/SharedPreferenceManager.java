package com.hbl.bot.utils.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.hbl.bot.model.AOFBModels;
import com.hbl.bot.network.models.request.baseRM.CUSTCDD;
import com.hbl.bot.network.models.request.baseRM.CUSTCONTACTS;
import com.hbl.bot.network.models.request.baseRM.CUSTDEMOGRAPHICSX;
import com.hbl.bot.network.models.request.baseRM.CUSTEDD;
import com.hbl.bot.network.models.request.baseRM.CUSTPEP;
import com.hbl.bot.network.models.request.baseRM.Data;
import com.hbl.bot.network.models.request.baseRM.CUSTADDRRESS;
import com.hbl.bot.network.models.request.baseRM.CUSTFINX;
import com.hbl.bot.network.models.request.baseRM.CUSTNEXTOFKIN;
import com.hbl.bot.network.models.request.baseRM.CUSTStatus;
import com.hbl.bot.network.models.request.baseRM.*;
import com.hbl.bot.network.models.request.baseRM.USERINFO;
import com.hbl.bot.network.models.response.base.APKCheckResponse;
import com.hbl.bot.network.models.response.base.MYSISREFResponse;
import com.hbl.bot.network.models.response.base.PreferencesMailing;
import com.hbl.bot.network.models.response.base.WorkFlowResponse;
import com.hbl.bot.network.models.response.baseRM.AccountOperationsInst;
import com.hbl.bot.network.models.response.baseRM.AccountOperationsType;
import com.hbl.bot.network.models.response.baseRM.AccountType;
import com.hbl.bot.network.models.response.baseRM.AddressType;
import com.hbl.bot.network.models.response.baseRM.AppStatus;
import com.hbl.bot.network.models.response.baseRM.BioAccountType;
import com.hbl.bot.network.models.response.baseRM.Bool;
import com.hbl.bot.network.models.response.baseRM.BranchCode;
import com.hbl.bot.network.models.response.baseRM.BranchRegionCode;
import com.hbl.bot.network.models.response.baseRM.BranchType;
import com.hbl.bot.network.models.response.baseRM.BusinessArea;
import com.hbl.bot.network.models.response.baseRM.Capacity;
import com.hbl.bot.network.models.response.baseRM.CardProduct;
import com.hbl.bot.network.models.response.baseRM.ChequeBookLeaves;
import com.hbl.bot.network.models.response.baseRM.City;
import com.hbl.bot.network.models.response.baseRM.ConsumerProduct;
import com.hbl.bot.network.models.response.baseRM.Country;
import com.hbl.bot.network.models.response.baseRM.Currency;
import com.hbl.bot.network.models.response.baseRM.CustomerSegment;
import com.hbl.bot.network.models.response.baseRM.DepositType;
import com.hbl.bot.network.models.response.baseRM.DocsData;
import com.hbl.bot.network.models.response.baseRM.ETBNTB;
import com.hbl.bot.network.models.response.baseRM.EstateFrequency;
import com.hbl.bot.network.models.response.baseRM.GaurdianInfo;
import com.hbl.bot.network.models.response.baseRM.Gender;
import com.hbl.bot.network.models.response.baseRM.HawCompanyData;
import com.hbl.bot.network.models.response.baseRM.HawKeys;
import com.hbl.bot.network.models.response.baseRM.IndustryMainCategory;
import com.hbl.bot.network.models.response.baseRM.KonnectPurposeOfAccount;
import com.hbl.bot.network.models.response.baseRM.LoginData;
import com.hbl.bot.network.models.response.baseRM.MODelivery;
import com.hbl.bot.network.models.response.baseRM.MaritalStatus;
import com.hbl.bot.network.models.response.baseRM.ModeOfTans;
import com.hbl.bot.network.models.response.baseRM.NadraVerify;
import com.hbl.bot.network.models.response.baseRM.NatureOfBusiness;
import com.hbl.bot.network.models.response.baseRM.Pep;
import com.hbl.bot.network.models.response.baseRM.PepCategories;
import com.hbl.bot.network.models.response.baseRM.PrefComMode;
import com.hbl.bot.network.models.response.baseRM.Priority;
import com.hbl.bot.network.models.response.baseRM.Profession;
import com.hbl.bot.network.models.response.baseRM.Province;
import com.hbl.bot.network.models.response.baseRM.PurposeCIF;
import com.hbl.bot.network.models.response.baseRM.PurposeOfAccount;
import com.hbl.bot.network.models.response.baseRM.PurposeOfTin;
import com.hbl.bot.network.models.response.baseRM.RMCodes;
import com.hbl.bot.network.models.response.baseRM.ReasonPrefEDD;
import com.hbl.bot.network.models.response.baseRM.Relationship;
import com.hbl.bot.network.models.response.baseRM.SBPCodes;
import com.hbl.bot.network.models.response.baseRM.SourceOfIncomeLandLord;
import com.hbl.bot.network.models.response.baseRM.SourceOfWealth;
import com.hbl.bot.network.models.response.baseRM.SourceOffund;
import com.hbl.bot.network.models.response.baseRM.Sundry;
import com.hbl.bot.network.models.response.baseRM.Title;
import com.hbl.bot.network.models.response.baseRM.TrackingID;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by zeeshan on 05-21-2020
 */

public class SharedPreferenceManager {
    Object object = new Object();
    //Store and Retrieve
    public static final String ETBNTB_KEY = "ETBNTB_KEY";
    public static final String CHANNEL_ID = "CHANNEL_ID";
    public static final String LAT_LNG = "LAT_LNG";
    public static final String VERSION_CODE = "VERSION_CODE";
    public static final String VERSION_NAME = "VERSION_NAME";
    public static final String TABLET_ID = "TABLET_ID";
    public static final String TABLET_APP_SIZE = "TABLET_APP_SIZE";
    public static final String MYSISID = "USER_ID";
    public static final String APP_CREATED_DATE = "APP_CREATED_DATE";
    public static final String APP_UPDATE_DATE = "APP_UPDATE_DATE";
    public static final String REMEMBER = "REMEMBER";
    public static final String USER_LOGIN = "E:\\Project\\HBL\\Tablet\\spark_app_android\\app\\src\\main\\java\\com\\hbl\\bot\\utils\\managers\\SharedPreferenceManager.java";
    public static final String COUNT = "COUNT";
    public static final String ACCOUNT_CNIC = "ACCOUNT_CNIC";
    public static final String OBJECT_KEY = "OBJECT_KEY";
    public static final String LOGIN_DATA = "LOGIN_DATA";
    public static final String BRANCH_CODE_KEY = "BRANCH_CODE_KEY";
    public static final String CUSTOMER_DEMO_GRAPHICS_KEY = "CUSTOMER_DEMO_GRAPHICS_KEY";
    public static final String CUSTOMER_CDD_KEY = "CUSTOMER_CDD_KEY";
    public static final String CUSTOMER_EDD_KEY = "CUSTOMER_EDD_KEY";
    public static final String CUSTOMER_FIN_KEY = "CUSTOMER_FIN_KEY";
    public static final String CUSTOMER_BIOMATRIC_KEY = "CUSTOMER_BIOMATRIC_KEY";
    public static final String CUSTOMER_USER_INFO = "CUSTOMER_USER_INFO";
    public static final String CUSTOMER_STATUS = "CUSTOMER_STATUS";
    public static final String CUSTOMER_ADDRESS_KEY = "CUSTOMER_ADDRESS_KEY";
    public static final String CUSTOMER_CONTACTS_KEY = "CUSTOMER_CONTACTS_KEY";
    public static final String BRANCH_TYPE_KEY = "BRANCH_TYPE_KEY";
    public static final String TRACKING_ID_KEY = "TRACKING_ID_KEY";
    public static final String MYSISREF_KEY = "MYSISREF_KEY";
    public static final String BUSINESSAREA_KEY = "BUSINESSAREA_KEY";
    public static final String APPSTATUS_KEY = "APPSTATUS_KEY";
    public static final String WORKFLOW_KEY = "WORKFLOW_KEY";
    public static final String AOF_ACCOUNT_IFNO = "AOF_ACCOUNT_IFNO";
    public static final String CUSTOMER_ACCOUNT = "CUSTOMER_ACCOUNT";
    public static final String PARTIAL_DOCUMETNS = "PARTIAL_DOCUMETNS";
    public static final String CUSTOMER_NEXTOFKIN = "CUSTOMER_NEXTOFKIN";
    public static final String CUSTOMER_DOCUMENT = "CUSTOMER_DOCUMENT";
    public static final String DOC_CHECKLIST = "DOC_CHECKLIST";
    public static final String CUSTOMER_DOC_LIST = "CUSTOMER_DOC_LIST";
    public static final String CUSTINFO = "CUSTINFO";
    public static final String CUSTOMER_INFORMATION = "CUSTOMER_INFORMATION";
    public static final String CUSTOMER_SOLE_SELF = "CUSTOMER_SOLE_SELF";
    public static final String CUSTOMER_GAURDIAN_INFO = "CUSTOMER_GAURDIAN_INFO";
    public static final String CUSTOMER_PEP = "CUSTOMER_PEP";
    public static final String BIOMETRIC_ACCOUNT = "BIOMETRIC_ACCOUNT";
    public static final String ACCOUNT_NUMBER_GENERATED = "ACCOUNT_NUMBER_GENERATED";
    public static final String AOFB_MODELS = "AOFB_MODELS";
    public static final String LOV_DOCTYPE = "LOV_DOCTYPE";
    public static final String LOV_COMPANY_NAME = "LOV_COMPANY_NAME";
    public static final String LOV_BRANCH_REGOION = "LOV_BRANCH_REGOION";
    public static final String LOV_COUNTRIES = "LOV_COUNTRIES";
    public static final String LOV_SOURCE_OF_LANDLORDS = "LOV_SOURCE_OF_LANDLORDS";
    public static final String LOV_INDUSTRY_MAIN_CATEGORY = "LOV_INDUSTRY_MAIN_CATEGORY";
    public static final String LOV_RESON_PREF_EDD = "LOV_RESON_PREF_EDD";
    public static final String LOV_SPECIFIC_COUNTRIES = "LOV_SPECIFIC_COUNTRIES";
    public static final String LOV_CITIES = "LOV_CITIES";
    public static final String LOV_SBP_CODE = "LOV_SBP_CODE";
    public static final String LOV_PEP = "LOV_PEP";
    public static final String LOV_TITLE = "LOV_TITLE";
    public static final String LOV_MATERIAL_STATUS = "LOV_MATERIAL_STATUS";
    public static final String LOV_GENDER = "LOV_GENDER";
    public static final String LOV_CONSUMER_PRODUCT = "LOV_CONSUMER_PRODUCT";
    public static final String LOV_ACCOUNT_TYPE = "LOV_ACCOUNT_TYPE";
    public static final String LOV_CURRENCY = "LOV_CURRENCY";
    public static final String LOV_PRIORITY = "LOV_PRIORITY";
    public static final String LOV_ACCOUNT_OPERATIONNAL_TYPE = "LOV_ACCOUNT_OPERATIONNAL_TYPE";
    public static final String LOV_ESTATE_FREQUENCY = "LOV_ESTATE_FREQUENCY";
    public static final String LOV_DEPOSIT_TYPE = "LOV_DEPOSIT_TYPE";
    public static final String LOV_RM_CODE = "LOV_RM_CODE";
    public static final String LOV_PROVINCE = "LOV_PROVINCE";
    public static final String LOV_BIOMETRIC_TYPE = "LOV_BIOMETRIC_TYPE";
    public static final String PURPOSE_OF_ACCOUNT = "PURPOSE_OF_ACCOUNT";
    public static final String KONNECT_PURPOSE_OF_ACCOUNT = "KONNECT_PURPOSE_OF_ACCOUNT";
    public static final String OPERATIONAL_INSTRUCTION = "OPERATIONAL_INSTRUCTION";
    public static final String LOV_SUNDRY_CODE = "LOV_SUNDRY_CODE";
    public static final String LOV_ADDRESS_TYPE = "LOV_ADDRESS_TYPE";
    public static final String LOV_CUST_SEGMENT = "LOV_CUST_SEGMENT";
    public static final String LOV_SOURCE_OF_FUND = "LOV_SOURCE_OF_FUND";
    public static final String LOV_SOURCE_OF_FUND_INCOME = "LOV_SOURCE_OF_FUND_INCOME";
    public static final String LOV_SOURCE_OF_WEALTH = "LOV_SOURCE_OF_WEALTH";
    public static final String LOV_MODE_OF_TANS = "LOV_MODE_OF_TANS";
    public static final String LOV_BOOL = "LOV_BOOL";
    public static final String LOV_SATISFACTORY_PHYSICAL_VERFICATION = "LOV_SATISFACTORY_PHYSICAL_VERFICATION";
    public static final String LOV_ELEMENT_OF_ANONYMITY_IN_TRANSACTIONS = "LOV_ELEMENT_OF_ANONYMITY_IN_TRANSACTIONS";
    public static final String LOV_CHEQUE_BOOK_LEAVES = "LOV_CHEQUE_BOOK_LEAVES";
    public static final String LOV_MODE_OF_DELIVERY = "LOV_MODE_OF_DELIVERY";
    public static final String LOV_PREF_MODE = "LOV_PREF_MODE";
    public static final String LOV_CARD_PRODUCT = "LOV_CARD_PRODUCT";
    public static final String LOV_PEP_CATEGORY = "LOV_PEP_CATEGORY";
    public static final String LOV_CAPACITY = "LOV_CAPACITY";
    public static final String LOV_PROFESSION = "LOV_PROFESSION";
    public static final String LOV_FINANCIAL_SUPPORTER_PROFESSION = "LOV_FINANCIAL_SUPPORTER_PROFESSION";
    public static final String LOV_RELATIONSHIP = "LOV_RELATIONSHIP";
    public static final String LOV_PURPOSE_OF_TIN = "LOV_PURPOSE_OF_TIN";
    public static final String LOV_PURPOSE_OF_CIF = "LOV_PURPOSE_OF_CIF";
    public static final String LOV_PREFERENCE_MAILING = "LOV_PREFERENCE_MAILING";
    public static final String LOV_NATURE_OF_BUSINESS = "LOV_NATURE_OF_BUSINESS";
    public static final String NADRA_RESPONSE_DATA = "NADRA_RESPONSE_DATA";
    public static final String DOWNLOAD_APK_DATA = "DOWNLOAD_APK_DATA";
    public static final String HAW_KEYS_KEY = "HAW_KEYS_KEY";
    public static final String D_TOKEN = "D_TOKEN";
    //Is Fingerprint Authentication Enabled
    public static final String IS_FINGERPRINT_AUTH_ENABLED = "IS_FINGERPRINT_AUTH_ENABLED";
    public static SharedPreferences sSharedPreferences;
    public static final SharedPreferenceManager sharedPrefManagerInstance = new SharedPreferenceManager();

    public SharedPreferenceManager getInstance(Context context) {
        sSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefManagerInstance;
    }

    public SharedPreferenceManager() {
    }

    public void storeStringInSharedPreferences(String key, String content) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(key, content);
        editor.apply();
    }

    public String getStringFromSharedPreferences(String key) {
        return sSharedPreferences.getString(key, "");
    }

    public void removeStringInSharedPreferences(String key, String remove) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        if (key.equals("logout")) {
            editor.clear();
            editor.commit();
        } else if (remove.equals("remove")) {
            editor.remove(key).commit();
        }
    }

    public void clearSP() {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.clear().commit();
    }

    public void storeLongInSharedPreferences(String key, long content) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putLong(key, content);
        editor.apply();
    }

    public long getLongFromSharedPreferences(String key) {
        return sSharedPreferences.getLong(key, 0L);
    }

    public void storeBooleanInSharedPreferences(String key, boolean status) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putBoolean(key, status);
        editor.apply();
    }

    public boolean getBooleanFromSharedPreferences(String key) {
        return sSharedPreferences.getBoolean(key, false);
    }

    public void storeFloatInSharedPreferences(String key, float content) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putFloat(key, content);
        editor.apply();
    }

    public float getFloatFromSharedPreferences(String key) {
        return sSharedPreferences.getFloat(key, 0.0F);
    }


    public void storeIntInSharedPreferences(String key, int content) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putInt(key, content);
        editor.apply();
    }

    public int getIntFromSharedPreferences(String key) {
        return sSharedPreferences.getInt(key, 0);
    }


    public <Object> void setObjectList(ArrayList<Object> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        setObject(json);
    }

    public void setObject(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(OBJECT_KEY, value);
        editor.commit();
    }

    public ArrayList<Object> getObjectList() {
        ArrayList<Object> companyList = new ArrayList<>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(OBJECT_KEY, json);
            Type type = new TypeToken<ArrayList<Object>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovDoctype(ArrayList<DocsData> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_DOCTYPE, json);
        editor.commit();
    }

    public ArrayList<DocsData> getLovDoctype() {
        ArrayList<DocsData> companyList = new ArrayList<DocsData>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_DOCTYPE, json);
            Type type = new TypeToken<ArrayList<DocsData>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setETBNTB(ArrayList<ETBNTB> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(ETBNTB_KEY, json);
        editor.commit();
    }

    public ArrayList<ETBNTB> getETBNTB() {
        ArrayList<ETBNTB> companyList = new ArrayList<ETBNTB>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(ETBNTB_KEY, json);
            Type type = new TypeToken<ArrayList<ETBNTB>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setHawKeys(ArrayList<HawKeys> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(HAW_KEYS_KEY, json);
        editor.commit();
    }

    public ArrayList<HawKeys> getHawKeys() {
        ArrayList<HawKeys> companyList = new ArrayList<HawKeys>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(HAW_KEYS_KEY, json);
            Type type = new TypeToken<ArrayList<HawKeys>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }


    public void setCompanyName(ArrayList<HawCompanyData> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_COMPANY_NAME, json);
        editor.commit();
    }

    public ArrayList<HawCompanyData> getCompanyName() {
        ArrayList<HawCompanyData> companyList = new ArrayList<HawCompanyData>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_COMPANY_NAME, json);
            Type type = new TypeToken<ArrayList<HawCompanyData>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }
 public void setBranchRegion(ArrayList<BranchRegionCode> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_BRANCH_REGOION, json);
        editor.commit();
    }

    public ArrayList<BranchRegionCode> getBranchRegion() {
        ArrayList<BranchRegionCode> companyList = new ArrayList<BranchRegionCode>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_BRANCH_REGOION, json);
            Type type = new TypeToken<ArrayList<BranchRegionCode>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setNadraResponseData(NadraVerify data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(NADRA_RESPONSE_DATA, json);
        editor.commit();
    }

    public NadraVerify getNadraResponseData() {
        NadraVerify companyList = new NadraVerify();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(NADRA_RESPONSE_DATA, json);
            Type type = new TypeToken<NadraVerify>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setDownloadAPK(APKCheckResponse data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(DOWNLOAD_APK_DATA, json);
        editor.commit();
    }

    public APKCheckResponse getDownloadAPK() {
        APKCheckResponse companyList = new APKCheckResponse();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(DOWNLOAD_APK_DATA, json);
            Type type = new TypeToken<APKCheckResponse>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovCustSegment(ArrayList<CustomerSegment> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_CUST_SEGMENT, json);
        editor.commit();
    }

    public ArrayList<CustomerSegment> getLovCustSegment() {
        ArrayList<CustomerSegment> companyList = new ArrayList<CustomerSegment>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_CUST_SEGMENT, json);
            Type type = new TypeToken<ArrayList<CustomerSegment>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovSourceOffund(ArrayList<SourceOffund> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_SOURCE_OF_FUND, json);
        editor.commit();
    }

    public ArrayList<SourceOffund> getLovSourceOffund() {
        ArrayList<SourceOffund> companyList = new ArrayList<SourceOffund>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_SOURCE_OF_FUND, json);
            Type type = new TypeToken<ArrayList<SourceOffund>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovProvince(ArrayList<Province> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PROVINCE, json);
        editor.commit();
    }

    public ArrayList<Province> getProvince() {
        ArrayList<Province> companyList = new ArrayList<Province>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PROVINCE, null);
            Type type = new TypeToken<ArrayList<Province>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovBioAccountType(ArrayList<BioAccountType> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_BIOMETRIC_TYPE, json);
        editor.commit();
    }

    public ArrayList<BioAccountType> getBioAccountType() {
        ArrayList<BioAccountType> companyList = new ArrayList<BioAccountType>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_BIOMETRIC_TYPE, null);
            Type type = new TypeToken<ArrayList<BioAccountType>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovSourceOffundIncome(ArrayList<SourceOffund> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_SOURCE_OF_FUND_INCOME, json);
        editor.commit();
    }

    public ArrayList<SourceOffund> getLovSourceOffundIncome() {
        ArrayList<SourceOffund> companyList = new ArrayList<SourceOffund>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_SOURCE_OF_FUND_INCOME, json);
            Type type = new TypeToken<ArrayList<SourceOffund>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovSourceOfWealth(ArrayList<SourceOfWealth> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_SOURCE_OF_WEALTH, json);
        editor.commit();
    }

    public ArrayList<SourceOfWealth> getLovSourceOfWealth() {
        ArrayList<SourceOfWealth> companyList = new ArrayList<SourceOfWealth>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_SOURCE_OF_WEALTH, json);
            Type type = new TypeToken<ArrayList<SourceOfWealth>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovModeOfTans(ArrayList<ModeOfTans> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_MODE_OF_TANS, json);
        editor.commit();
    }

    public ArrayList<ModeOfTans> getLovModeOfTans() {
        ArrayList<ModeOfTans> companyList = new ArrayList<ModeOfTans>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_MODE_OF_TANS, json);
            Type type = new TypeToken<ArrayList<ModeOfTans>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovBool(ArrayList<Bool> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_BOOL, json);
        editor.commit();
    }

    public ArrayList<Bool> getLovBool() {
        ArrayList<Bool> companyList = new ArrayList<Bool>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_BOOL, json);
            Type type = new TypeToken<ArrayList<Bool>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovSatisfactoryPhysicalVerification(ArrayList<Bool> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_SATISFACTORY_PHYSICAL_VERFICATION, json);
        editor.commit();
    }

    public ArrayList<Bool> getLovSatisfactoryPhysicalVerification() {
        ArrayList<Bool> companyList = new ArrayList<Bool>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_SATISFACTORY_PHYSICAL_VERFICATION, json);
            Type type = new TypeToken<ArrayList<Bool>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovElementOfAnonymityInTransactions(ArrayList<Bool> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_ELEMENT_OF_ANONYMITY_IN_TRANSACTIONS, json);
        editor.commit();
    }

    public ArrayList<Bool> getLovElementOfAnonymityInTransactions() {
        ArrayList<Bool> companyList = new ArrayList<Bool>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_ELEMENT_OF_ANONYMITY_IN_TRANSACTIONS, json);
            Type type = new TypeToken<ArrayList<Bool>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovChequeBookLeaves(ArrayList<ChequeBookLeaves> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_CHEQUE_BOOK_LEAVES, json);
        editor.commit();
    }

    public ArrayList<ChequeBookLeaves> getLovChequeBookLeaves() {
        ArrayList<ChequeBookLeaves> companyList = new ArrayList<ChequeBookLeaves>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_CHEQUE_BOOK_LEAVES, json);
            Type type = new TypeToken<ArrayList<ChequeBookLeaves>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovModeOfDelivery(ArrayList<MODelivery> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_MODE_OF_DELIVERY, json);
        editor.commit();
    }


    public ArrayList<MODelivery> getLovModeOfDelivery() {
        ArrayList<MODelivery> companyList = new ArrayList<MODelivery>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_MODE_OF_DELIVERY, json);
            Type type = new TypeToken<ArrayList<MODelivery>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovPrefMode(ArrayList<PrefComMode> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PREF_MODE, json);
        editor.commit();
    }

    public ArrayList<PrefComMode> getLovPrefMode() {
        ArrayList<PrefComMode> companyList = new ArrayList<PrefComMode>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PREF_MODE, json);
            Type type = new TypeToken<ArrayList<PrefComMode>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovCardType(ArrayList<CardProduct> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_CARD_PRODUCT, json);
        editor.commit();
    }

    public ArrayList<CardProduct> getLovCardType() {
        ArrayList<CardProduct> companyList = new ArrayList<CardProduct>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_CARD_PRODUCT, json);
            Type type = new TypeToken<ArrayList<CardProduct>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovPepCategories(ArrayList<PepCategories> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PEP_CATEGORY, json);
        editor.commit();
    }

    public ArrayList<PepCategories> getLovPepCategories() {
        ArrayList<PepCategories> companyList = new ArrayList<PepCategories>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PEP_CATEGORY, json);
            Type type = new TypeToken<ArrayList<PepCategories>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovCapacity(ArrayList<Capacity> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_CAPACITY, json);
        editor.commit();
    }

    public ArrayList<Capacity> getLovCapacity() {
        ArrayList<Capacity> companyList = new ArrayList<Capacity>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_CAPACITY, json);
            Type type = new TypeToken<ArrayList<Capacity>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovProfession(ArrayList<Profession> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PROFESSION, json);
        editor.commit();
    }

    public ArrayList<Profession> getLovProfession() {
        ArrayList<Profession> companyList = new ArrayList<Profession>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PROFESSION, json);
            Type type = new TypeToken<ArrayList<Profession>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovFinancialSupporterProfession(ArrayList<Profession> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_FINANCIAL_SUPPORTER_PROFESSION, json);
        editor.commit();
    }

    public ArrayList<Profession> getLovFinancialSupporterProfession() {
        ArrayList<Profession> companyList = new ArrayList<Profession>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_FINANCIAL_SUPPORTER_PROFESSION, json);
            Type type = new TypeToken<ArrayList<Profession>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovRelationship(ArrayList<Relationship> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_RELATIONSHIP, json);
        editor.commit();
    }

    public ArrayList<Relationship> getLovRelationship() {
        ArrayList<Relationship> companyList = new ArrayList<Relationship>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_RELATIONSHIP, json);
            Type type = new TypeToken<ArrayList<Relationship>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovNatureOfBusiness(ArrayList<NatureOfBusiness> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_NATURE_OF_BUSINESS, json);
        editor.commit();
    }

    public ArrayList<NatureOfBusiness> getLovNatureOfBusiness() {
        ArrayList<NatureOfBusiness> companyList = new ArrayList<NatureOfBusiness>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_NATURE_OF_BUSINESS, json);
            Type type = new TypeToken<ArrayList<NatureOfBusiness>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovPurposeOfTin(ArrayList<PurposeOfTin> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PURPOSE_OF_TIN, json);
        editor.commit();
    }

    public ArrayList<PurposeOfTin> getLovPurposeOfTin() {
        ArrayList<PurposeOfTin> companyList = new ArrayList<PurposeOfTin>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PURPOSE_OF_TIN, json);
            Type type = new TypeToken<ArrayList<PurposeOfTin>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovPurposeCIF(ArrayList<PurposeCIF> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PURPOSE_OF_CIF, json);
        editor.commit();
    }

    public ArrayList<PurposeCIF> getLovPurposeCIF() {
        ArrayList<PurposeCIF> companyList = new ArrayList<PurposeCIF>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PURPOSE_OF_CIF, json);
            Type type = new TypeToken<ArrayList<PurposeCIF>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovPreferencesMailing(ArrayList<PreferencesMailing> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PREFERENCE_MAILING, json);
        editor.commit();
    }

    public ArrayList<PreferencesMailing> getLovPreferencesMailing() {
        ArrayList<PreferencesMailing> companyList = new ArrayList<PreferencesMailing>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PREFERENCE_MAILING, json);
            Type type = new TypeToken<ArrayList<PreferencesMailing>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovSpecificCountry(ArrayList<Country> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_SPECIFIC_COUNTRIES, json);
        editor.commit();
    }

    public ArrayList<Country> getLovSpecificCountry() {
        ArrayList<Country> companyList = new ArrayList<Country>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_SPECIFIC_COUNTRIES, json);
            Type type = new TypeToken<ArrayList<Country>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovCountries(ArrayList<Country> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_COUNTRIES, json);
        editor.commit();
    }

    public ArrayList<Country> getLovCountries() {
        ArrayList<Country> companyList = new ArrayList<Country>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_COUNTRIES, json);
            Type type = new TypeToken<ArrayList<Country>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovSourceOffundLandLord(ArrayList<SourceOfIncomeLandLord> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_SOURCE_OF_LANDLORDS, json);
        editor.commit();
    }

    public ArrayList<SourceOfIncomeLandLord> getLovSourceOffundLandLord() {
        ArrayList<SourceOfIncomeLandLord> companyList = new ArrayList<SourceOfIncomeLandLord>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_SOURCE_OF_LANDLORDS, json);
            Type type = new TypeToken<ArrayList<SourceOfIncomeLandLord>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovIndustryMainCategory(ArrayList<IndustryMainCategory> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_INDUSTRY_MAIN_CATEGORY, json);
        editor.commit();
    }

    public ArrayList<IndustryMainCategory> getLovIndustryMainCategory() {
        ArrayList<IndustryMainCategory> companyList = new ArrayList<IndustryMainCategory>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_INDUSTRY_MAIN_CATEGORY, json);
            Type type = new TypeToken<ArrayList<IndustryMainCategory>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovReasonPrefEDD(ArrayList<ReasonPrefEDD> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_RESON_PREF_EDD, json);
        editor.commit();
    }

    public ArrayList<ReasonPrefEDD> getLovReasonPrefEDD() {
        ArrayList<ReasonPrefEDD> companyList = new ArrayList<ReasonPrefEDD>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_RESON_PREF_EDD, json);
            Type type = new TypeToken<ArrayList<ReasonPrefEDD>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovCities(ArrayList<City> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_CITIES, json);
        editor.commit();
    }

    public ArrayList<City> getLovCities() {
        ArrayList<City> companyList = new ArrayList<City>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_CITIES, json);
            Type type = new TypeToken<ArrayList<City>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovSBPCodes(ArrayList<SBPCodes> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_SBP_CODE, json);
        editor.commit();
    }

    public ArrayList<SBPCodes> getLovSBPCodes() {
        ArrayList<SBPCodes> companyList = new ArrayList<SBPCodes>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_SBP_CODE, json);
            Type type = new TypeToken<ArrayList<SBPCodes>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovPep(ArrayList<Pep> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PEP, json);
        editor.commit();
    }

    public ArrayList<Pep> getLovPep() {
        ArrayList<Pep> companyList = new ArrayList<Pep>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PEP, json);
            Type type = new TypeToken<ArrayList<Pep>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovTitle(ArrayList<Title> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_TITLE, json);
        editor.commit();
    }

    public ArrayList<Title> getLovTitle() {
        ArrayList<Title> companyList = new ArrayList<Title>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_TITLE, json);
            Type type = new TypeToken<ArrayList<Title>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovMaterialStatus(ArrayList<MaritalStatus> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_MATERIAL_STATUS, json);
        editor.commit();
    }

    public ArrayList<MaritalStatus> getLovMaterialStatus() {
        ArrayList<MaritalStatus> companyList = new ArrayList<MaritalStatus>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_MATERIAL_STATUS, json);
            Type type = new TypeToken<ArrayList<MaritalStatus>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovGender(ArrayList<Gender> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_GENDER, json);
        editor.commit();
    }

    public ArrayList<Gender> getLovGender() {
        ArrayList<Gender> companyList = new ArrayList<Gender>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_GENDER, json);
            Type type = new TypeToken<ArrayList<Gender>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovConsumerProduct(ArrayList<ConsumerProduct> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_CONSUMER_PRODUCT, json);
        editor.commit();
    }

    public ArrayList<ConsumerProduct> getLovConsumerProduct() {
        ArrayList<ConsumerProduct> companyList = new ArrayList<ConsumerProduct>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_CONSUMER_PRODUCT, json);
            Type type = new TypeToken<ArrayList<ConsumerProduct>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovAccountType(ArrayList<AccountType> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_ACCOUNT_TYPE, json);
        editor.commit();
    }

    public ArrayList<AccountType> getLovAccountType() {
        ArrayList<AccountType> companyList = new ArrayList<AccountType>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_ACCOUNT_TYPE, json);
            Type type = new TypeToken<ArrayList<AccountType>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovOperationalType(ArrayList<AccountOperationsType> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_ACCOUNT_OPERATIONNAL_TYPE, json);
        editor.commit();
    }

    public ArrayList<AccountOperationsType> getLovOperationalType() {
        ArrayList<AccountOperationsType> companyList = new ArrayList<AccountOperationsType>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_ACCOUNT_OPERATIONNAL_TYPE, json);
            Type type = new TypeToken<ArrayList<AccountOperationsType>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;

    }

    public void setLovEstateFrequency(ArrayList<EstateFrequency> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_ESTATE_FREQUENCY, json);
        editor.commit();
    }

    public ArrayList<EstateFrequency> getLovEstateFrequency() {
        ArrayList<EstateFrequency> companyList = new ArrayList<EstateFrequency>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_ESTATE_FREQUENCY, json);
            Type type = new TypeToken<ArrayList<EstateFrequency>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;

    }

    public void setLovCurrency(ArrayList<Currency> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_CURRENCY, json);
        editor.commit();
    }

    public ArrayList<Currency> getLovCurrency() {
        ArrayList<Currency> companyList = new ArrayList<Currency>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_CURRENCY, json);
            Type type = new TypeToken<ArrayList<Currency>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovPriority(ArrayList<Priority> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_PRIORITY, json);
        editor.commit();
    }

    public ArrayList<Priority> getLovPriority() {
        ArrayList<Priority> companyList = new ArrayList<Priority>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_PRIORITY, json);
            Type type = new TypeToken<ArrayList<Priority>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovDepositType(ArrayList<DepositType> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_DEPOSIT_TYPE, json);
        editor.commit();
    }

    public ArrayList<DepositType> getLovDepositType() {
        ArrayList<DepositType> companyList = new ArrayList<DepositType>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_DEPOSIT_TYPE, json);
            Type type = new TypeToken<ArrayList<DepositType>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;

    }


    public void setLovRMCodes(ArrayList<RMCodes> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_RM_CODE, json);
        editor.commit();
    }

    public ArrayList<RMCodes> getLovRMCodes() {
        ArrayList<RMCodes> companyList = new ArrayList<RMCodes>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_RM_CODE, json);
            Type type = new TypeToken<ArrayList<RMCodes>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovPurposeOfAcct(ArrayList<PurposeOfAccount> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(PURPOSE_OF_ACCOUNT, json);
        editor.commit();
    }

    public ArrayList<PurposeOfAccount> getLovPurposeOfAcct() {
        ArrayList<PurposeOfAccount> companyList = new ArrayList<PurposeOfAccount>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(PURPOSE_OF_ACCOUNT, json);
            Type type = new TypeToken<ArrayList<PurposeOfAccount>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovKonnectPurposeOfAcct(ArrayList<KonnectPurposeOfAccount> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(PURPOSE_OF_ACCOUNT, json);
        editor.commit();
    }

    public ArrayList<KonnectPurposeOfAccount> getLovKonnectPurposeOfAcct() {
        ArrayList<KonnectPurposeOfAccount> companyList = new ArrayList<KonnectPurposeOfAccount>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(PURPOSE_OF_ACCOUNT, json);
            Type type = new TypeToken<ArrayList<KonnectPurposeOfAccount>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovOperatioanlInst(ArrayList<AccountOperationsInst> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(OPERATIONAL_INSTRUCTION, json);
        editor.commit();
    }

    public ArrayList<AccountOperationsInst> getLovOperatioanlInst() {
        ArrayList<AccountOperationsInst> companyList = new ArrayList<AccountOperationsInst>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(OPERATIONAL_INSTRUCTION, json);
            Type type = new TypeToken<ArrayList<AccountOperationsInst>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovSundry(ArrayList<Sundry> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_SUNDRY_CODE, json);
        editor.commit();
    }

    public ArrayList<Sundry> getLovSundry() {
        ArrayList<Sundry> companyList = new ArrayList<Sundry>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_SUNDRY_CODE, json);
            Type type = new TypeToken<ArrayList<Sundry>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLovAddressType(ArrayList<AddressType> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOV_ADDRESS_TYPE, json);
        editor.commit();
    }

    public ArrayList<AddressType> getLovAddressType() {
        ArrayList<AddressType> companyList = new ArrayList<AddressType>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOV_ADDRESS_TYPE, json);
            Type type = new TypeToken<ArrayList<AddressType>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }


    public void setBranchCode(BranchCode data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setBranchCodeToJson(json);
    }

    public void setBranchCodeToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(BRANCH_CODE_KEY, value);
        editor.commit();
    }

    public BranchCode getBranchCode() {
        BranchCode companyList = new BranchCode();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(BRANCH_CODE_KEY, json);
            Type type = new TypeToken<BranchCode>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setAOFBModels(AOFBModels data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setAOFBModelsToJson(json);
    }

    public void setAOFBModelsToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(AOFB_MODELS, value);
        editor.commit();
    }

    public AOFBModels getAOFBModels() {
        AOFBModels companyList = new AOFBModels();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(AOFB_MODELS, json);
            Type type = new TypeToken<AOFBModels>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerStatus(CUSTStatus data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerStatusToJson(json);
    }

    public void setCustomerStatusToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_STATUS, value);
        editor.commit();
    }

    public CUSTStatus getCustomerStatus() {
        CUSTStatus companyList = new CUSTStatus();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_STATUS, json);
            Type type = new TypeToken<CUSTStatus>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerUserInfo(USERINFO data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerUserInfoToJson(json);
    }

    public void setCustomerUserInfoToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_USER_INFO, value);
        editor.commit();
    }

    public USERINFO getCustomerUserInfo() {
        USERINFO companyList = new USERINFO();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_USER_INFO, json);
            Type type = new TypeToken<USERINFO>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerBiomatric(NadraVerify data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerBiomatricToJson(json);
    }

    public void setCustomerBiomatricToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_BIOMATRIC_KEY, value);
        editor.commit();
    }

    public NadraVerify getCustomerBiomatric() {
        NadraVerify companyList = new NadraVerify();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_BIOMATRIC_KEY, json);
            Type type = new TypeToken<NadraVerify>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerDemoGraphics(CUSTDEMOGRAPHICSX data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerDemoGraphicsToJson(json);
    }

    public void setCustomerDemoGraphicsToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_DEMO_GRAPHICS_KEY, value);
        editor.commit();
    }

    public CUSTDEMOGRAPHICSX getCustomerDemoGraphics() {
        CUSTDEMOGRAPHICSX companyList = new CUSTDEMOGRAPHICSX();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_DEMO_GRAPHICS_KEY, json);
            Type type = new TypeToken<CUSTDEMOGRAPHICSX>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerAddress(ArrayList<CUSTADDRRESS> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerAddressToJson(json);
    }

    public void setCustomerAddressToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_ADDRESS_KEY, value);
        editor.commit();
    }

    public ArrayList<CUSTADDRRESS> getCustomerAddress() {
        ArrayList<CUSTADDRRESS> companyList = new ArrayList<>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_ADDRESS_KEY, json);
            Type type = new TypeToken<ArrayList<CUSTADDRRESS>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerCDD(CUSTCDD data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerCDDToJson(json);
    }

    public void setCustomerCDDToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_CDD_KEY, value);
        editor.commit();
    }

    public CUSTCDD getCustomerCDD() {
        CUSTCDD companyList = new CUSTCDD();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_CDD_KEY, json);
            Type type = new TypeToken<CUSTCDD>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerEDD(CUSTEDD data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerEDDToJson(json);
    }

    public void setCustomerEDDToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_EDD_KEY, value);
        editor.commit();
    }

    public CUSTEDD getCustomerEDD() {
        CUSTEDD companyList = new CUSTEDD();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_EDD_KEY, json);
            Type type = new TypeToken<CUSTEDD>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerFIN(CUSTFINX data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerFINToJson(json);
    }

    public void setCustomerFINToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_FIN_KEY, value);
        editor.commit();
    }

    public CUSTFINX getCustomerFIN() {
        CUSTFINX companyList = new CUSTFINX();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_FIN_KEY, json);
            Type type = new TypeToken<CUSTFINX>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerContacts(CUSTCONTACTS data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerContactsToJson(json);
    }

    public void setCustomerContactsToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_CONTACTS_KEY, value);
        editor.commit();
    }

    public CUSTCONTACTS getCustomerContacts() {
        CUSTCONTACTS companyList = new CUSTCONTACTS();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_CONTACTS_KEY, json);
            Type type = new TypeToken<CUSTCONTACTS>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }


    public void setAofAccountInfo(Data data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setAofAccountInfoToJson(json);
    }

    public void setAofAccountInfoToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(AOF_ACCOUNT_IFNO, value);
        editor.commit();
    }

    public Data getAofAccountInfo() {
        Data companyList = new Data();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(AOF_ACCOUNT_IFNO, json);
            Type type = new TypeToken<Data>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerNextOfKin(CUSTNEXTOFKIN data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setNextOfKiToJson(json);
    }

    public void setNextOfKiToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_NEXTOFKIN, value);
        editor.commit();
    }

    public CUSTNEXTOFKIN getCustomerNextOfKin() {
        CUSTNEXTOFKIN companyList = new CUSTNEXTOFKIN();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_NEXTOFKIN, json);
            Type type = new TypeToken<CUSTNEXTOFKIN>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }


    public CUSTDOCUMENT getCustomerDocument() {
        CUSTDOCUMENT companyList = new CUSTDOCUMENT();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_DOCUMENT, json);
            Type type = new TypeToken<CUSTDOCUMENT>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerDocument(CUSTDOCUMENT data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerDocumentToJson(json);
    }

    public void setCustomerDocumentToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_DOCUMENT, value);
        editor.commit();
    }


    public ArrayList<DOCCHECKLIST> getDocCheckList() {
        ArrayList<DOCCHECKLIST> companyList = new ArrayList<DOCCHECKLIST>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(DOC_CHECKLIST, json);
            Type type = new TypeToken<ArrayList<DOCCHECKLIST>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setDocCheckList(ArrayList<DOCCHECKLIST> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setDocCheckListToJson(json);
    }

    public void setDocCheckListToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(DOC_CHECKLIST, value);
        editor.commit();
    }

    public ArrayList<CUSTDOCUMENT> getCustomerDocList() {
        ArrayList<CUSTDOCUMENT> companyList = new ArrayList<CUSTDOCUMENT>();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_DOC_LIST, json);
            Type type = new TypeToken<ArrayList<CUSTDOCUMENT>>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustDocList(ArrayList<CUSTDOCUMENT> data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_DOC_LIST, json);
        editor.commit();
    }


    public void setCustomerAccountToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_ACCOUNT, value);
        editor.commit();
    }

    public void setCustomerAccount(CUST_ACCOUNTS data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerAccountToJson(json);
    }

    public CUST_ACCOUNTS getCustomerAccount() {
        CUST_ACCOUNTS companyList = new CUST_ACCOUNTS();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_ACCOUNT, json);
            Type type = new TypeToken<CUST_ACCOUNTS>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }


    public void setParticalDocsToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(PARTIAL_DOCUMETNS, value);
        editor.commit();
    }

    public void setPartialDocs(PDCODSNEW data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setParticalDocsToJson(json);
    }

    public PDCODSNEW getPartialDocs() {
        PDCODSNEW companyList = new PDCODSNEW();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(PARTIAL_DOCUMETNS, json);
            Type type = new TypeToken<PDCODSNEW>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerInfo(CUSTINFO data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerInformationToJson(json);
    }

    public void setCustomerInformationToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_INFORMATION, value);
        editor.commit();
    }

    public CUSTINFO getCustomerInfo() {
        CUSTINFO companyList = new CUSTINFO();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_INFORMATION, json);
            Type type = new TypeToken<CUSTINFO>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerSoleSelf(CUSTSOLESELF data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerSoleSelfToJson(json);
    }

    public void setCustomerSoleSelfToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_SOLE_SELF, value);
        editor.commit();
    }

    public CUSTSOLESELF getCustomerSoleSelf() {
        CUSTSOLESELF companyList = new CUSTSOLESELF();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_SOLE_SELF, json);
            Type type = new TypeToken<CUSTSOLESELF>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setGaurdianInfo(GaurdianInfo value) {
        Gson gson = new Gson();
        String json = gson.toJson(value);
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_GAURDIAN_INFO, json);
        editor.commit();
    }

    public GaurdianInfo getGaurdianInfo() {
        GaurdianInfo companyList = new GaurdianInfo();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_GAURDIAN_INFO, json);
            Type type = new TypeToken<GaurdianInfo>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setCustomerPep(CUSTPEP data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setCustomerPepToJson(json);
    }

    public void setCustomerPepToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(CUSTOMER_PEP, value);
        editor.commit();
    }

    public CUSTPEP getCustomerPep() {
        CUSTPEP companyList = new CUSTPEP();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(CUSTOMER_PEP, json);
            Type type = new TypeToken<CUSTPEP>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setBranchType(BranchType data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setBranchTypeToJson(json);
    }

    public void setBranchTypeToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(BRANCH_TYPE_KEY, value);
        editor.commit();
    }

    public BranchType getBranchType() {
        BranchType companyList = new BranchType();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(BRANCH_TYPE_KEY, json);
            Type type = new TypeToken<BranchType>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setTrackingID(TrackingID data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setTrackingIDToJson(json);
    }

    public void setTrackingIDToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(TRACKING_ID_KEY, value);
        editor.commit();
    }

    public TrackingID getTrackingID() {
        TrackingID companyList = new TrackingID();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(TRACKING_ID_KEY, json);
            Type type = new TypeToken<TrackingID>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }


    public void setMySisRef(MYSISREFResponse data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setMySisRefJSON(json);
    }

    public void setMySisRefJSON(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(MYSISREF_KEY, value);
        editor.commit();
    }

    public MYSISREFResponse getMySisRef() {
        MYSISREFResponse companyList = new MYSISREFResponse();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(MYSISREF_KEY, json);
            Type type = new TypeToken<MYSISREFResponse>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setBusinessArea(BusinessArea data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setBusinessAreaJSON(json);
    }

    public void setBusinessAreaJSON(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(BUSINESSAREA_KEY, value);
        editor.commit();
    }

    public BusinessArea getBusinessArea() {
        BusinessArea companyList = new BusinessArea();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(BUSINESSAREA_KEY, json);
            Type type = new TypeToken<BusinessArea>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setAppStatus(AppStatus data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setAppStatusJSON(json);
    }

    public void setAppStatusJSON(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(APPSTATUS_KEY, value);
        editor.commit();
    }

    public AppStatus getAppStatus() {
        AppStatus companyList = new AppStatus();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(APPSTATUS_KEY, json);
            Type type = new TypeToken<AppStatus>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setWorkFlow(WorkFlowResponse data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setWorkFlowJson(json);
    }

    public void setWorkFlowJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(WORKFLOW_KEY, value);
        editor.commit();
    }

    public WorkFlowResponse getWorkFlow() {
        WorkFlowResponse companyList = new WorkFlowResponse();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(WORKFLOW_KEY, json);
            Type type = new TypeToken<WorkFlowResponse>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setLoginData(LoginData data) {
        Gson gson = new Gson();
        String json = gson.toJson(data);
        setLoginDataToJson(json);
    }

    public void setLoginDataToJson(String value) {
        SharedPreferences.Editor editor = sSharedPreferences.edit();
        editor.putString(LOGIN_DATA, value);
        editor.commit();
    }

    public LoginData getLoginData() {
        LoginData companyList = new LoginData();
        String json = new Gson().toJson(companyList);
        if (sSharedPreferences != null) {
            Gson gson = new Gson();
            String string = sSharedPreferences.getString(LOGIN_DATA, json);
            Type type = new TypeToken<LoginData>() {
            }.getType();
            companyList = gson.fromJson(string, type);
            return companyList;
        }
        return companyList;
    }

    public void setIntCount(int value) {
        storeIntInSharedPreferences(COUNT, value);
    }

    public int getIntCount() {
        //If value is less than 0
        if (getIntFromSharedPreferences(COUNT) < 0) {
            setIntCount(0);
            return 0;
        } else {
            //else return whole value
            return getIntFromSharedPreferences(COUNT);
        }
    }

    public void setIsFingerprintAuthEnabled(boolean isEnabled) {
        storeBooleanInSharedPreferences(IS_FINGERPRINT_AUTH_ENABLED, isEnabled);
    }

    public boolean getIsFingerprintAuthEnabled() {
        return getBooleanFromSharedPreferences(IS_FINGERPRINT_AUTH_ENABLED);
    }
}
