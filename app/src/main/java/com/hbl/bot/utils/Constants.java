package com.hbl.bot.utils;

import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.text.format.Formatter;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static android.content.Context.WIFI_SERVICE;

public class Constants {
    public static final int TOAST_TIME = 2000;
    public static final String fomratDateTime = "yyyy-MM-dd HH:mm:ss";
    public static final String fomratDateTimeException = "hh:mm:s a | dd-MMM-yyyy";
    public static final String fomratDate = "dd-mm-yyyy";
    public static final String fomratDate2 = "yyyy-mm-dd";
    public static final String fomratTime = "HH:mm:ss";
    public static final String ACTIVITY_KEY = "ACTIVITY_KEY";
    public static final String PROVINCE_IDENTIFIER = "bioprovince";
    public static final String BIO_ACCOUNT_TYPE_IDENTIEFIER = "bioaccttype";
    public static final String PRINT_PDF_IDENTIEFIER = "printdocuments";
    public static final String PRINT_DOCUMENT_IDENTIEFIER = "pagesetupprintdocuments";
    public static final int PEP_REMOVER = 0;
    public static final int FATCA_REMOVER = 1;
    public static final String POST = "POST";
    public static final String PORT_NUMBER = "9131";
    public static final String MANUAL_VERYSIS_BIO_TRACKING_ID = "BLK";
    public static final String GET_VERYSIS_BIO_TRACKING_ID = "BIO";
    public static final String BIOMETRIC_BIO_TRACKING_ID = "FOM";
    public static final String BIOMETRIC_BIO_BM = "BM";
    public static final int INPUT_TYPE_ALPHA_BLOCK_LASTNAME = 9;
    public static final String CONSTANT_TRACKINGID = "0000000000-991";
    public static final String BROADCAST = "checkinternet";
    public static final int EXCEPTION_TOAST = 3;
    public static final int SUCCESS_TOAST = 2;
    public static final int ERROR_TOAST = 1;
    public static final int ERROR_TOAST_LONG = 0;
    public static final int KONNECT_TOAST_LONG = 5;
    public static WifiManager wManager;
    public static final long SESSION_DISCONNECT_TIMEOUT = 1000 * 60 * 1000; // 5 min = 5 * 60 * 1000 ms
    @Nullable
    public static final String DOCS_IDENTIFIER = "doctype";
    @Nullable
    public static final String HAW_SEGMENT_IDENTIFIER = "hawcustsegment";
    @Nullable
    public static final String HAW_KEYS_IDENTIFIER = "hawkeys";
    @Nullable
    public static final String HAW_DOCS_IDENTIFIER = "hawdoctype";
    @Nullable
    public static final String HAW_COMPANY_IDENTIFIER = "hawcompanylist";
    @Nullable
    public static final String BRANCH_REGION_IDENTIFIER = "branchregion";
    @Nullable
    public static final String CUSTOMER_SEGMENT_IDENTIFIER = "custsegment";
    @Nullable
    public static final String COUNTRIES_IDENTIFIER = "country";
    @Nullable
    public static final String MYMEET_IDENTIFIER = "mm";
    @Nullable
    public static final String NEWCUST_IDENTIFIER = "nctc";
    @Nullable
    public static final String SOURCE_OF_INCOME = "sourceofincome";
    @Nullable
    public static final String INPUT_VALUES = "edittext";
    @Nullable
    public static final String KEY_NAME_DEBIT_TRANSACTION = "Number of debit transaction";
    @Nullable
    public static final String KEY_NAME_INITIAL_DEPOSIT = "Initial Deposit";
    @Nullable
    public static final String ONBOARDING_IDENTIFIER = "leads";
    @Nullable
    public static final String COUNTRY_MATRIX = "nationality";
    @Nullable
    public static final String BOOL_IDENTIFIER = "bool";
    @Nullable
    public static final String BOOL_PHONE_BANKING_IDENTIFIER = "Phone Banking";
    @Nullable
    public static final String BOOL_ZAKAT_IDENTIFIER = "Zakat Exemption";
    @Nullable
    public static final String BOOL_FREE_INSURANCE_IDENTIFIER = "Free insurance";
    @Nullable
    public static final String BOOL_CONSUMER_PRODUCT_IDENTIFIER = "Consumer Product";
    @Nullable
    public static final String BOOL_SMS_ALERT_IDENTIFIER = "SMS alerts";
    @Nullable
    public static final String BOOL_HOLD_MAIL_INSTRUCTION_IDENTIFIER = "Hold mail instructions";
    @Nullable
    public static final String BOOL_SATISFACTORY_PHYSICAL_VERIFICATION = "satisfactoryphysicalverification";
    @Nullable
    public static final String BOOL_ELEMENT_OF_ATT = "elementofatt";
    @Nullable
    public static final String PRIORITY_IDENTIFIER = "priority";
    @Nullable
    public static final String CITIES_IDENTIFIER = "city";
    @Nullable
    public static final String PURPOSE_CIF_IDENTIFIER = "purpofcif";
    @Nullable
    public static final String TITLE_IDENTIFIER = "title";

    @Nullable
    public static final String MARITIAL_IDENTIFIER = "marital";
    @Nullable
    public static final String GENDER_IDENTIFIER = "gender";
    @Nullable
    public static final String SBP_CODES_IDENTIFIER = "sbpcodes";
    @Nullable
    public static final String RM_CODES_IDENTIFIER = "rmcodes";
    @Nullable
    public static final String SUNDRY_IDENTIFIER = "sundry";
    @Nullable
    public static final String HAW_SUNDRY_IDENTIFIER = "hawsundry";
    @Nullable
    public static final String CONSUMER_PRODUCT_IDENTIFIER = "consumerproduct";
    @Nullable
    public static final String RELATIONSHIP_IDENTIFIER = "relation";
    @NotNull
    public static final String CCODE = "CCODE";
    @Nullable
    public static final String INDUSTRY_MAIN_CATEGORY_IDENTIFIER = "sourceofindusmaster";
    @Nullable
    public static final String INDUSTRY_SUB_CATEGORY_IDENTIFIER = "sourceofindusdetail";
    @Nullable
    public static final String PEP_IDENTIFIER = "pepresn";
    @Nullable
    public static final String SOURCE_OFFUND_IDENTIFIER = "sourceoffund";
    @Nullable
    public static final String SOURCE_OFWEALTH_IDENTIFIER = "sourceofwealth";
    @Nullable
    public static final String ADDRESS_TYPE_IDENTIFIER = "addresstype";
    @Nullable
    public static final String PURPOSE_OFTIN_IDENTIFIER = "purpOfTin";
    @Nullable
    public static final String CAPACITY_IDENTIFIER = "capacity";
    @Nullable
    public static final String PROFESSIONS_IDENTIFIER = "profession";
    @Nullable
    public static final String NATURE_OFBUSINESS_IDENTIFIER = "business";
    @Nullable
    public static final String NATURE_OF_BUSINESS_KEY = "Nature Of Business";
    @Nullable
    public static final String COUNTRY_CODE_KEY = "Country Code";
    @Nullable
    public static final String MODE_OFTRANS_INDENTIFIER = "modeoftrans";
    @Nullable
    public static final String SOURCE_OF_INCOME_IDENTIFIER = "sourceoffund";
    @Nullable
    public static final String REASON_PREF_EDD_IDENTIFIER = "resnPrefedd";
    @Nullable
    public static final String ACCOUNT_OPERATIONS_INST_IDENTIFIER = "operationinst";
    @Nullable
    public static final String URL_STOP_IDENTIFIER = "printdocumentsstopurl";
    @Nullable
    public static final String ACCOUNT_OPERATIONS_TYPE_IDENTIFIER = "accountoperationtype";
    @Nullable
    public static final String ACCOUNT_TYPE_IDENTIEFIER = "accttype";
    @Nullable
    public static final String ACCOUNT_TYPE_BULK_ACCOUNT_IDENTIEFIER = "hawaccttype";
    @Nullable
    public static final String HAW_ACCOUNT_DEBIT_CARD_IDENTIEFIER = "hawcardproduct";
    @Nullable
    public static final String CURRENCY_IDENTIFIER = "currency";
    @Nullable
    public static final String DEPOSIT_TYPE_IDENTIFIER = "deposittype";
    @Nullable
    public static final String DEBIT_CARD_IDENTIFIER = "debitcard";
    @Nullable
    public static final String PURPOSE_OF_ACCOUNT_IDENTIFIER = "purposeofact";
    @Nullable
    public static final String ESTATE_FREQUENCY_IDENTIFIER = "estatfreq";
    @Nullable
    public static final String PREF_COM_MODE_IDENTIFIER = "prefcommumode";
    @Nullable
    public static final String HAW_KEY_DEBIT_CARD_REQUIRED = "Debit Card";
    @Nullable
    public static final String CARD_PRODUCT_IDENTIFIER = "cardproduct";
    @Nullable
    public static final String BRANCH_CODE_INDENTIFIER = "branchcode";
    @Nullable
    public static final String BRANCH_TYPE_INDENTIFIER = "branchtypes";
    @Nullable
    public static final String DOCUMENT_TYPE_INDENTIFIER = "getdoclistm";
    @Nullable
    public static final String PARTIAL_UPLOAD_INDENTIFIER = "partialupload";
    @Nullable
    public static final String PARTIAL_SSD_UPLOAD_INDENTIFIER = "partialuploadsscard";
    @Nullable
    public static final String COMFIG_INDENTIFIER = "getconfig";
    @Nullable
    public static final String AOF_SUBMIT_IDENTIFIER = "aofsubmit";
    @Nullable
    public static final String PAYLOAD = "U2FsdGVkX1+eMelgQeJlI51PUXnmebgWMVjt8eGRb7kTZ14o9CGJ17PkRN/R7Ol/LVVjoXNA3uAtF05FVtvweDF/ZQKbqShdXACJgx0H6tI=";
    @Nullable
    public static final String TAG = "HBL_TAG";
    @Nullable
    public static final String SUBMIT_DRAFT_IDENTIFIER = "submitDraft";
    @Nullable
    public static final String UPDATE_DRAFT_IDENTIFIER = "updateDraft";
    @Nullable
    public static final String FINAL_DRAFT_IDENTIFIER = "finalDraft";
    @Nullable
    public static final String MYSISREF_IDENTIFIER = "mysisref";
    @Nullable
    public static final String BUSINESSAREA_IDENTIFIER = "businessarea";

    @Nullable
    public static final String DASHBOARD_ITEM_IDENTIFIER = "setSelectionLogs";
    @Nullable
    public static final String HTML_IDENTIFIER = "printdocuments";
    @Nullable
    public static final String WORKFLOW_IDENTIFIER = "workflows";
    @Nullable
    public static final int INPUT_TYPE_NUMBER = 0;
    @Nullable
    public static final int INPUT_TYPE_ALPHANUMERIC = 1;
    @Nullable
    public static final int INPUT_TYPE_ALPHA = 2;
    @Nullable
    public static final int INPUT_TYPE_CUSTOM_ALPHA = 3;
    @Nullable
    public static final int INPUT_TYPE_CUSTOM_EMAIL_ALPHA = 4;
    @Nullable
    public static final int INPUT_TYPE_CUSTOM_ADDRESS_ALPHANUMERIC = 5;
    @Nullable
    public static final int INPUT_TYPE_ALPHA_BLOCK = 7;
    @Nullable
    public static final int INPUT_TYPE_CUSTOM_ALPHANUMERIC = 8;
    @NotNull
    public static final String ID_DOC_TYPE = "ID_DOC_TYPE";
    @NotNull
    public static final String CUST_SEGMENT = "CUST_SEGMENT";
    @NotNull
    public static final String PURPOSE_OF_CIF = "purpofcif";
    public static final int INPUT_TYPE_CUSTOM_NUMBER = 6;
    @Nullable
    public static final String ACCOUNT_NUMBER_KEY = "account_number_key";
    @Nullable
    public static final String ACCOUNT_PAGE = "ACCOUNT_PAGE";
    @Nullable
    public static final String CIF_NUMBER_KEY = "cif_number_key";
    @Nullable
    public static final String ACCOUNT_NUMBER_MESSAGE_KEY = "account_number_message_key";
    public static final String IBAN_NUMBER_KEY = "iban_number_key";
    @NotNull
    public static final String APPSTATUS_IDENTIFIER = "appstatus";
    @NotNull
    public static final String PEP_CATEGORIES_IDENTIFIER = "pepcategories";
    @NotNull
    public static final String PEP_SUB_CATEGORIES_IDENTIFIER = "pepsubcategories";
    @Nullable
    public static final String COUNTRY_CANADA_IDENTIFIER = "countrycanada";
    @Nullable
    public static final String DRAFT_INDENTIFIER = "getmaintenancedata";
    @Nullable
    public static final String CNIC_MAINTANACE_INDENTIFIER = "getdatabyiddoc";
    @Nullable
    public static final String ACCOUNT_CNIC = "ACCOUNT_CNIC";
    @Nullable
    public static final String NADRA_RESPONSE_DATA = "nadraResponseData";
    @NotNull
    public static final String PREFERENCES_MAILING_IDENTIFIER = "prefmailingadd";
    @Nullable
    public static final String STATUS_BROADCAST = "STATUS_BROADCAST";
    @Nullable
    public static final String STATUS_EMAIL_SENT = "STATUS_EMAIL_SENT";
    @NotNull
    public static final String CHEQUE_BOOK_LEAVES_IDENTIFIER = "chequebookleaves";
    @NotNull
    public static final String MODE_OF_DELIVERY_IDENTIFIER = "modeofdelivery";
    @Nullable
    public static final String DOCUMENT_UPLOAD_SCREEN = "DOCUMENT_UPLOAD_SCREEN";
    @NotNull
    public static final String TOKEN = "TOKEN";
    @NotNull
    public static final int CIF_1_SCREEN = 0;
    @NotNull
    public static final int CIF_1_SHOW_RECORD = 8;
    @NotNull
    public static final int HAW = 6;
    @NotNull
    public static final int SPARK = 7;
    @NotNull
    public static final int CIF_1_4_SCREEN = 1;
    @NotNull
    public static final int ACCOUNT_OPENING_SCREEN = 2;
    @NotNull
    public static final int CIF_14_SCREEN = 3;
    @NotNull
    public static final int CIF_13_SCREEN = 5;
    @NotNull
    public static final int CIF_10_SCREEN = 4;
    @NotNull
    public static final int KONNECT_ACCOUNT = 6;
    @NotNull
    public static final int KONNECT_DEBITCARD = 7;
    @Nullable
    public static final String TYPE_ACCOUNT_VALIDATION_IDENTIFIER = "typeacctvalidation";
    @Nullable
    public static final String TYPE_ACCOUNT_VALIDATION_IDENTIFIER_HAW = "hawacctvalidation";
    @NotNull
    public static final String DOCS_IDENTIFIER_FIN = "doctypefin";
    @Nullable
    public static final String APP_DETAILS_IDENTIFIER = "appDetails";
    @Nullable
    public static final String MANUAL_VERYSIS_IDENTIFIER = "getDocumentNumberInVerisys";
    @Nullable
    public static final String FILTER_TRACKING_ID = "FILTER_TRACKING_ID";
    @Nullable
    public static final String FILTER_DOCUMENT_ID = "FILTER_DOCUMENT_ID";

    public static String getMacAddress() {
        wManager = (WifiManager) GlobalClass.applicationContext.getSystemService(WIFI_SERVICE);
        WifiInfo info = wManager.getConnectionInfo();
        return info.getMacAddress();
    }

    public static String getOs() {
        return "ANDROID";
    }

    public static String getIP() {
        wManager = (WifiManager) GlobalClass.applicationContext.getSystemService(WIFI_SERVICE);
        return Formatter.formatIpAddress(wManager.getConnectionInfo().getIpAddress());
    }
}
