package com.hbl.bot.utils

import android.content.Context
import android.util.Log
import com.hbl.bot.ui.activities.spark.CIFRootActivity


/*PAGE OF MODELS STATUS FOR DRAWER UPDATE*/
class POMStatus(context: Context) {
    var context: Context = context
    var customerInfo = (this.context as CIFRootActivity).sharedPreferenceManager.customerInfo
    var customerDemoGraphicx =
        (this.context as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics
    var customerPep = (this.context as CIFRootActivity).sharedPreferenceManager.customerPep
    var customerContacts =
        (this.context as CIFRootActivity).sharedPreferenceManager.customerContacts
    var customerAddress = (this.context as CIFRootActivity).sharedPreferenceManager.customerAddress
    var customerFIN = (this.context as CIFRootActivity).sharedPreferenceManager.customerFIN
    var customerCdd = (this.context as CIFRootActivity).sharedPreferenceManager.customerCDD
    var customerEdd = (this.context as CIFRootActivity).sharedPreferenceManager.customerEDD
    var customerAccount = (this.context as CIFRootActivity).sharedPreferenceManager.customerAccount
    var customerNOK = (this.context as CIFRootActivity).sharedPreferenceManager.customerNextOfKin

    fun customerIDInfoPage1(): Int {
        var count = 0
        /*if count==7 all fields are null so status will be lineup*/
        /*if count<7 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerInfo.ID_DOCUMENT_TYPE.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.ID_DOCUMENT_NO.isNullOrEmpty()) {
            count++
        }
        if (customerDemoGraphicx.CUST_TYPE.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.NATIONALITY.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.COUNTRY_OF_BIRTH.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.COUNTRY_OF_RES.isNullOrEmpty()) {
            count++
        }
        if (customerDemoGraphicx.PURP_OF_CIF_CODE.isNullOrEmpty()) {
            count++
        }
        Log.i("CustomerInfo", ": $count")
        return count
    }

    fun customerBiometricPage1(): Int {
        var count = 0
        var customerBiometric =
            (this.context as CIFRootActivity).sharedPreferenceManager.customerBiomatric
        /*if count==4 all fields are null so status will be lineup*/
        /*if count<4 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerBiometric.mobileno.isNullOrEmpty()) {
            count++
        }
        if (customerBiometric.provincecode.isNullOrEmpty()) {
            count++
        }
        if (customerBiometric.accountbatcode.isNullOrEmpty()) {
            count++
        }
        if (customerBiometric.fingerindex.isNullOrEmpty()) {
            count++
        }
        Log.i("CustomerBiometric", ": $count")
        return count
    }

    fun customerIdDetailsPage1(): Int {
        var count = 0
        /*if count==3 all fields are null so status will be lineup*/
        /*if count<3some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerInfo.ISSUE_DATE.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.EXPIRY_DATE.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.DATE_OF_BIRTH.isNullOrEmpty()) {
            count++
        }
        return count
    }

    fun customerPersonalInfoPage1And2(): Int {
        var count = 0
        /*if count==13 all fields are null so status will be lineup*/
        /*if count<13 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerInfo.TITLE_CODE.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.MARITAL_STATUS.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.FULL_NAME.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.FIRST_NAME.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.LAST_NAME.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.FATHER_NAME.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.MOTHER_NAME.isNullOrEmpty()) {
            count++
        }

        /*Page 2 SBP, RM, SUNDRY CODES*/
        if (customerInfo.CONSUMER_PRODUCT.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.PEP.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.RM_CODE.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.SBP_CODE.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.SUNDRY_ANALYSIS.isNullOrEmpty()) {
            count++
        }
        return count;
    }

    fun customerContactInfoPage1(): Int {
        var count = 0
        /*if count==5 all fields are null so status will be lineup*/
        /*if count<5 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerContacts.PREFERRED_MAIL_ADDR.isNullOrEmpty()) {
            count++
        }
        if (customerContacts.COUNTRY_DIAL_CODE.isNullOrEmpty()) {
            count++
        }
        if (customerContacts.MOBILE_NO.isNullOrEmpty()) {
            count++
        }
        if (customerContacts.EMAIL_ADDRESS.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.INTERNET_MOB_BNK_REQ.isNullOrEmpty()) {
            count++
        }

        return count
    }

    fun customerAddressPage1(): Int {
        var count = 0
        /*if count==6 all fields are null so status will be lineup*/
        /*if count<6 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerAddress.size != 0) {
            Log.i("customerIndex", "1")
            if (customerAddress.getOrNull(0)?.ADDRESS_LINE1.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(0)?.ADDRESS_LINE2.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(0)?.ADDRESS_LINE3.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(0)?.COUNTRY.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(0)?.CITY.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(0)?.TYPE_OF_ADDRESS_CODE.isNullOrEmpty()) {
                count++
            }
            return count
        } else {
            return 6
        }
    }

    fun customerAddressPage2(): Int {
        var count = 0
        /*if count==5 all fields are null so status will be lineup*/
        /*if count<5 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerAddress.size != 0) {
            Log.i("customerIndex", "2")
            if (customerAddress.getOrNull(1)?.ADDRESS_LINE1.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(1)?.ADDRESS_LINE2.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(1)?.ADDRESS_LINE3.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(1)?.COUNTRY.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(1)?.CITY.isNullOrEmpty()) {
                count++
            }

            return count
        } else {
            return 5
        }
    }

    fun customerAddressPage3(): Int {
        var count = 0
        /*if count==5 all fields are null so status will be lineup*/
        /*if count<5 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerAddress.size != 0) {
            Log.i("customerIndex", "3")
            if (customerAddress.getOrNull(2)?.ADDRESS_LINE1.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(2)?.ADDRESS_LINE2.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(2)?.ADDRESS_LINE3.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(2)?.COUNTRY.isNullOrEmpty()) {
                count++
            }
            if (customerAddress.getOrNull(2)?.CITY.isNullOrEmpty()) {
                count++
            }
            return count
        } else {
            return 5
        }
    }

    fun customerFATCAPage1And2(): Int {
        var count = 0
        /*if count==4 all fields are null so status will be lineup*/
        /*if count<4 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerInfo.US_ADDRESS.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.US_ADDRESS_2.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.FATCA_COUNTRY_CODE.isNullOrEmpty()) {
            count++
        }
        /*FATCA Page 2*/

        if (customerInfo.IRS_FORM_W8_SUBMITTED.isNullOrEmpty() &&
            customerInfo.IRS_FORM_W9_SUBMITTED.isNullOrEmpty()
        ) {
            count++
        }

        return count
    }

    fun customerCRSPage1And2(): Int {
        var count = 0
        /*if count==3 all fields are null so status will be lineup*/
        /*if count<3 som    e fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerInfo.NAME_OF_COUNTRY1.isNullOrEmpty()) {
            count++
        }
        if (customerInfo.TIN1.isNullOrEmpty() && customerInfo.REASON_IN_CASE_OF_NO_TIN1.isNullOrEmpty()) {
            count++
        }

        if (customerInfo.CAPACITY_CODE.isNullOrEmpty()) {
            count++
        }
        return count
    }

    fun customerPepPage1_2_3(): Int {
        var count = 0
        /*if count==11 all fields are null so status will be lineup*/
        /*if count<11 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerPep.REASON_PEP_INDIVIDUAL.isNullOrEmpty()) {
            count++
        }

        if (customerPep.PROFILE_CUST_BUSS_OCCUPATION.isNullOrEmpty()) {
            count++
        }

        if (customerPep.PERIOD_SENIOR_POSITION_HELD.isNullOrEmpty()) {
            count++
        }

        if (customerPep.COUNTRY_SENIOR_POSITION_HELD.isNullOrEmpty()) {
            count++
        }

        /*PEP PAGE 2*/
        if (customerPep.SOURCE_OF_FUND.isNullOrEmpty()) {
            count++
        }
        if (customerPep.SOURCE_OF_WEALTH.isNullOrEmpty()) {
            count++
        }
        /*PEP PAGE 3*/
        if (customerPep.ANALYSIS_OF_PEP.isNullOrEmpty()) {
            count++
        }
        if (customerPep.FOREIGN_PEP.isNullOrEmpty()) {
            count++
        }
        if (customerPep.DOMESTIC_PEP.isNullOrEmpty()) {
            count++
        }
        if (customerPep.PEP_CATG_CODE.isNullOrEmpty()) {
            count++
        }
        if (customerPep.PEP_SUB_CATG_CODE.isNullOrEmpty()) {
            count++
        }

        return count
    }

    fun customerDemographicsPage1_2_3(): Int {
        var count = 0
        /*if count==10 all fields are null so status will be lineup*/
        /*if count<10 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerDemoGraphicx.CUST_TYPE.isNullOrEmpty()) {
            count++
        }

        if (customerDemoGraphicx.PROFESSION.isNullOrEmpty()) {
            count++
        }
        /*Customer Demographics Page 2*/
        if (customerDemoGraphicx.EXP_MON_INCOME.isNullOrEmpty()) {
            count++
        }

        if (customerDemoGraphicx.EXP_CR_TURNOVER.isNullOrEmpty()) {
            count++
        }

        if (customerDemoGraphicx.NO_OF_CR_TRANS.isNullOrEmpty()) {
            count++
        }

        if (customerDemoGraphicx.NO_OF_DB_TRANS.isNullOrEmpty()) {
            count++
        }

        if (customerDemoGraphicx.EXP_DB_TURNOVER.isNullOrEmpty()) {
            count++
        }

        if (customerDemoGraphicx.SOURCE_OF_INCOME.isNullOrEmpty()) {
            count++
        }

        /*Customer Demographics Page 3*/
        if (customerDemoGraphicx.EXP_MODE_CR_TRANS.isNullOrEmpty()) {
            count++
        }

        return count
    }

    fun customerFinancialSupporterPage1(): Int {
        var count = 0
        /*if count==8 all fields are null so status will be lineup*/
        /*if count<8 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerFIN.NAME_FIN_SUPPORTER.isNullOrEmpty()) {
            count++
        }
        if (customerFIN.OCCUPATION_FIN_SUPPORTER.isNullOrEmpty()) {
            count++
        }
        if (customerFIN.BUSS_INDUS_SOURCE_OF_INCOME.isNullOrEmpty()) {
            count++
        }
        if (customerFIN.NATIONALITY_STAY_COUNTRY.isNullOrEmpty()) {
            count++
        }
        if (customerFIN.ID_DOCUMENT_TYPE.isNullOrEmpty()) {
            count++
        }
        if (customerFIN.ID_DOCUMENT_NO.isNullOrEmpty()) {
            count++
        }
        if (customerFIN.MTHLY_INCOME_PKR.isNullOrEmpty()) {
            count++
        }
        if (customerFIN.RELATIONSHIP_FIN_SUPPORTER.isNullOrEmpty()) {
            count++
        }
        return count
    }

    fun customerCDDDetails(): Int {
        var count = 0
        /*if count==3 all fields are null so status will be lineup*/
        /*if count<3 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerCdd.cUST_SRC_OF_INDUS_TYPE.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.cUST_SRC_OF_INCOME_INDUS.isNullOrEmpty()) {
            count++
        }

        if (customerCdd.hOLD_MAIL_INSTR.isNullOrEmpty()) {
            count++
        }

        return count
    }

    fun customerInwardOutWardPage(): Int {
        var count = 0
        /*if count==6 all fields are null so status will be lineup*/
        /*if count<6 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (
            customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_1.toString().isNullOrEmpty()) {
            count++
        }
        if (customerCdd.dESC_COUNTRY_INWARD_REMITT_1.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.pURPOSE_INWARD_1.isNullOrEmpty()) {
            count++
        }

        if (customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_1.toString().isNullOrEmpty()) {
            count++
        }
        if (customerCdd.dESC_COUNTRY_OUTWARD_REMITT_1.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.pURPOSE_OUTWARD_1.isNullOrEmpty()) {
            count++
        }



        return count
    }

    fun customerOtherBankInfo(): Int {
        var count = 0
        /*if count==2 all fields are null so status will be lineup*/
        /*if count<2 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerCdd.nAME_OF_BANK_1.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.nAME_OF_BRANCH_1.isNullOrEmpty()) {
            count++
        }
       /* if (customerCdd.NAME_OF_BANK_2.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.NAME_OF_BRANCH_2.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.NAME_OF_BANK_3.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.NAME_OF_BRANCH_3.isNullOrEmpty()) {
            count++
        }*/
        return count
    }

    fun customerVerifyCDD(): Int {
        var count = 0
        /*if count==4 all fields are null so status will be lineup*/
        /*if count<4 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/

        if (customerCdd.dESC_BREIF_REMARKS.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.fILLED_BY.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.aPPROVED_BY_AOF.isNullOrEmpty()) {
            count++
        }
        if (customerCdd.aPPROVED_BY_BR_MGR.isNullOrEmpty()) {
            count++
        }
        return count
    }

    fun customerEDDDetails(): Int {
        var count = 0
        /*if count==1 all fields are null so status will be lineup*/
        /*if count<1 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerEdd.rEASON_EDD.isNullOrEmpty()) {
            count++
        }
        if (customerEdd.fOREIGN_CONTACT_NO.isNullOrEmpty()) {
            count++
        }
        return count
    }

    fun customerEddAdditionalSource(): Int {
        var count = 0
        /*if count==1 all fields are null so status will be lineup*/
        /*if count< some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/

        if (customerEdd.aGRI_INCOME_EXP_AMT.isNullOrEmpty() &&
            customerEdd.aGRI_RENTAL_INCOME_EXP_AMT.toString().isNullOrEmpty() &&
            customerEdd.bUSINESS_INCOME_EXP_AMT.toString().isNullOrEmpty() &&
            customerEdd.rENTAL_INCOME_EXP_AMT.toString().isNullOrEmpty() &&
            customerEdd.rENTAL_INCOME_RES_PROJ_EXP_AMT.toString().isNullOrEmpty()
        ) {
            count++
        }
        return count
    }

    fun customerEddAdditionalQueries(): Int {
        var count = 0
        /*if count==6 all fields are null so status will be lineup*/
        /*if count<6 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerEdd.cOUNTRY_CODE_INWARD_FOREIGN1.isNullOrEmpty()) {
            count++
        }
        if (customerEdd.cOUNTRY_CODE_OUTWARD_FOREIGN1.isNullOrEmpty()) {
            count++
        }
        if (customerEdd.nAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1.isNullOrEmpty()) {
            count++
        }
        if (customerEdd.rELATIONSHIP_FOREIGN_BENEFICIARY_1.isNullOrEmpty()) {
            count++
        }
        if (customerEdd.pURPOSE_OF_FOREIGN_TRANSACTION.isNullOrEmpty()) {
            count++
        }
        if (customerEdd.aMOUNT_OF_FOREIGN_TRANSACTION.toString().isNullOrEmpty()) {
            count++
        }
        return count
    }

    fun customerEddFinancialSupporter(): Int {
        var count = 0
        /*if count==2 all fields are null so status will be lineup*/
        /*if count<2 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/
        if (customerEdd.rELATIONSHIP_WITH_FIN.isNullOrEmpty()) {
            count++
        }
        /*EDD Financial Supporter Page 2*/
        if (customerEdd.sAL_OF_FIN_SUPPORTER_EXP_AMT.isNullOrEmpty() ||
            customerEdd.rENTAL_INCOME_FIN_SUPPORTER_EXP_AMT.isNullOrEmpty() ||
            customerEdd.bUSS_INCOME_FIN_SUPPORTER_EXP_AMT.isNullOrEmpty() ||
            customerEdd.rETIREMENT_FUND_EXP_AMT.isNullOrEmpty() ||
            customerEdd.pENSION_EXP_AMT.isNullOrEmpty() ||
            customerEdd.iNHERITANCE_EXP_AMT.isNullOrEmpty()
        ) {
            count++
        }
        return count
    }

    fun customerEddVerification(): Int {
        var count = 0
        /*if count==4 all fields are null so status will be lineup*/
        /*if count<4 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/

        /*EDD Financial Supporter Page 2*/
        if (customerEdd.eDD_CONDUCT_BY_NAME.isNullOrEmpty()) {
            count++
        }

        if (customerEdd.aPPROVED_BY_NAME.isNullOrEmpty()) {
            count++
        }

        if (customerEdd.aPPROVED_COMPL_AUTH_NAME.isNullOrEmpty()) {
            count++
        }

        if (customerEdd.eDD_CONDUCT_BY_DATE.isNullOrEmpty()) {
            count++
        }

        return count
    }

    fun Account_Page_1_2_3_4(): Int {
        var count = 0
        /*if count==9 all fields are null so status will be lineup*/
        /*if count<9 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/

        /*Account page 1*/
        if (customerAccount.ACCT_SNG_JNT.isNullOrEmpty()) {
            count++
        }

        if (customerAccount.TITLE_OF_ACCT.isNullOrEmpty()) {
            count++
        }

        if (customerAccount.ACCT_TYPE.isNullOrEmpty()) {
            count++
        }

        if (customerAccount.INIT_DEP_SOURCE.isNullOrEmpty()) {
            count++
        }

        if (customerAccount.INIT_DEPOSIT.isNullOrEmpty()) {
            count++
        }


        /*Account page 2*/
        if (customerAccount.PURPOSEOFACCTDESC.isNullOrEmpty()) {
            count++
        }


        /*Account page 3*/
        if (customerAccount.OPERINSTDESC.isNullOrEmpty()) {
            count++
        }

        /*Account page 4*/
        if (customerAccount.PREFERREDCOMMUMODEDESC.isNullOrEmpty()) {
            count++
        }

        return count
    }


    fun Account_Page_1(): Int {
        var count = 0
        /*if count==9 all fields are null so status will be lineup*/
        /*if count<9 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/

        /*Account page 1*/
        if (customerAccount.ACCT_SNG_JNT.isNullOrEmpty()) {
            count++
        }

        if (customerAccount.TITLE_OF_ACCT.isNullOrEmpty()) {
            count++
        }

        if (customerAccount.ACCT_TYPE.isNullOrEmpty()) {
            count++
        }

        if (customerAccount.INIT_DEP_SOURCE.isNullOrEmpty()) {
            count++
        }

        if (customerAccount.INIT_DEPOSIT.isNullOrEmpty()) {
            count++
        }

        return count
    }


    fun NextOfKin_Page_1_2_3(): Int {
        var count = 0
        /*if count==9 all fields are null so status will be lineup*/
        /*if count<9 some fields is null or not so status will be pending*/
        /*if count==0 all fields are filled so status will be done*/

        /*KIN page 1*/
        if (customerNOK.NAME.isNullOrEmpty()) {
            count++
        }
        if (customerNOK.PRIM_PHONE.isNullOrEmpty()) {
            count++
        }
        if (customerNOK.RELATIONSHIP_DESC.isNullOrEmpty()) {
            count++
        }

        if (customerNOK.COUNTRY_NAME.isNullOrEmpty()) {
            count++
        }
        if (customerNOK.CITY_NAME.isNullOrEmpty()) {
            count++
        }

        if (customerNOK.ADDRESS_LINE1.isNullOrEmpty()) {
            count++
        }

        /*KIN page 2*/

        if (customerNOK.ADDRESS_LINE2.isNullOrEmpty()) {
            count++
        }

        if (customerNOK.ADDRESS_LINE3.isNullOrEmpty()) {
            count++
        }

//        if (customerNOK.ADDRESS_LINE4.isNullOrEmpty()) {
//            count++
//        }




        return count
    }

    fun logs(value: String) {
        Log.i("drawerData", ": $value")
    }

}