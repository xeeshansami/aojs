package com.hbl.bot.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.hbl.bot.R;


public class OneButtonsDialog extends Dialog implements
        View.OnClickListener {

    public Context c;
    public Dialog d;
    public Button yes;
    String message;
    String positiveTitle;
    String negativeTitle;
    DialogCallback callback;
    TextView tvMessage;

    public OneButtonsDialog(Context a, String message, String positiveTitle, DialogCallback callback) {
        super(a);
        this.c = a;
        this.callback = callback;
        this.message = message;
        this.positiveTitle = positiveTitle;

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

//        setContentView(R.layout.one_button_dialog);
        yes = findViewById(R.id.bt_confirm);

        tvMessage = findViewById(R.id.tv_message);


        tvMessage.setText(message);
        yes.setText(positiveTitle);


        yes.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_confirm:
                callback.onPositiveClicked();
                break;
            default:
                break;
        }
        this.dismiss();
    }
}