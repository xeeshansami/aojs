package com.hbl.bot.utils

import android.content.Context
import com.hbl.bot.network.models.response.base.AofAccountInfoResponse
import com.hbl.bot.utils.managers.SharedPreferenceManager

class SparkDataSetupInSp(context: Context, aofData: AofAccountInfoResponse) {
    var aofData = AofAccountInfoResponse()
    var context = context

    @JvmField
    public var globalClass: GlobalClass? = null

    @JvmField
    var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()

    init {
        this.aofData = aofData
        globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
        sharedPreferenceManager.getInstance(globalClass)
        draftAOFDataInSP()
        setDraftInAllAOFSubModels()
        setDataInNadraResponse()
    }

    private fun setDataInNadraResponse() {
        sharedPreferenceManager.nadraResponseData = sharedPreferenceManager.customerBiomatric
    }

    private fun draftAOFDataInSP() {
        sharedPreferenceManager.setAofAccountInfo(
            this.aofData.data.get(
                0
            )
        )
    }

    private fun setDraftInAllAOFSubModels() {
        this.aofData.data.getOrNull(0).let { data ->
            if (!data?.CUST_INFO.isNullOrEmpty()) {
                data?.CUST_INFO?.getOrNull(0).let { sharedPreferenceManager.customerInfo = it }
            }
            if (!data?.CUST_BIOMETRIC.isNullOrEmpty()) {
                data?.CUST_BIOMETRIC?.getOrNull(0)
                    .let { sharedPreferenceManager.customerBiomatric = it }
            }
            if (!data?.CUST_PEP.isNullOrEmpty()) {
                data?.CUST_PEP?.getOrNull(0).let { sharedPreferenceManager.customerPep = it }
            }
            if (!data?.CUST_FIN.isNullOrEmpty()) {
                data?.CUST_FIN?.getOrNull(0).let { sharedPreferenceManager.customerFIN = it }
            }
            if (!data?.CUST_SOLE_SELF.isNullOrEmpty()) {
                data?.CUST_SOLE_SELF?.getOrNull(0)
                    .let { sharedPreferenceManager.customerSoleSelf = it }
            }
            if (!data?.CUST_EDD.isNullOrEmpty()) {
                data?.CUST_EDD?.getOrNull(0).let { sharedPreferenceManager.customerEDD = it }
            }
            if (!data?.CUST_CDD.isNullOrEmpty()) {
                data?.CUST_CDD?.getOrNull(0).let { sharedPreferenceManager.customerCDD = it }
            }
            if (!data?.CUST_CONTACTS.isNullOrEmpty()) {
                data?.CUST_CONTACTS?.getOrNull(0)
                    .let { sharedPreferenceManager.customerContacts = it }
            }
            if (!data?.CUST_ADDR.isNullOrEmpty()) {
                data?.CUST_ADDR.let { sharedPreferenceManager.customerAddress = it }
            }
            if (!data?.CUST_ACCOUNTS.isNullOrEmpty()) {
                data?.CUST_ACCOUNTS?.getOrNull(0)
                    .let { sharedPreferenceManager.customerAccount = it }
            }
            if (!data?.CUST_DEMOGRAPHICS.isNullOrEmpty()) {
                data?.CUST_DEMOGRAPHICS?.getOrNull(0)
                    .let { sharedPreferenceManager.customerDemoGraphics = it }
            }
            if (!data?.CUST_NEXTOFKIN.isNullOrEmpty()) {
                data?.CUST_NEXTOFKIN?.getOrNull(0)
                    .let { sharedPreferenceManager.customerNextOfKin = it }
            }
            if (!data?.CUST_STATUS.isNullOrEmpty()) {
                data?.CUST_STATUS?.getOrNull(0).let { sharedPreferenceManager.customerStatus = it }
            }
            if (!data?.GUARDIAN_INFO.isNullOrEmpty()) {
                data?.GUARDIAN_INFO?.getOrNull(0).let { sharedPreferenceManager.setGaurdianInfo(it) }
            }
            if (!data?.USER_INFO.isNullOrEmpty()) {
                data?.USER_INFO?.getOrNull(0).let { sharedPreferenceManager.customerUserInfo = it }
            }
            if (!data?.CUST_DOCUMENTS.isNullOrEmpty()) {
                data?.CUST_DOCUMENTS?.getOrNull(0)
                    .let { sharedPreferenceManager.customerDocument = it }
            }
            if (!data?.DOC_CHECKLIST.isNullOrEmpty()) {
                data?.DOC_CHECKLIST?.let { sharedPreferenceManager.docCheckList = it }
            }
            if (data?.PARTIAL_DOCS != null) {
                data?.PARTIAL_DOCS?.let { sharedPreferenceManager.partialDocs = it }
            }
        }
    }

}