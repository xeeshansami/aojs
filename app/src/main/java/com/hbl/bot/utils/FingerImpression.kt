package com.hbl.bot.utils

enum class FingerImpression {
    SELECT,RightThumb, RightIndex, RightMiddle, RightRing, RightLittle,
    LeftThumb, LeftIndex, LeftMiddle, LeftRing, LeftLittle,
    ;

    inline fun <reified T : Enum<T>> allImpressions(): Array<T> {
        return enumValues()
    }
}