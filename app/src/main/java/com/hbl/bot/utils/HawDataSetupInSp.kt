package com.hbl.bot.utils

import android.content.ActivityNotFoundException
import android.content.Context
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.BranchCodeCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.BranchTypeCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.BranchCodeRequest
import com.hbl.bot.network.models.response.base.AofAccountInfoResponse
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.BranchCodeResponse
import com.hbl.bot.network.models.response.base.BranchTypeResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.ui.activities.HawShowRecords
import com.hbl.bot.utils.managers.SharedPreferenceManager
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException
import java.lang.Exception

class HawDataSetupInSp(context: Context, aofData: AofAccountInfoResponse, var isShowRecord: Boolean) {
    var aofData = AofAccountInfoResponse()
    var context = context

    @JvmField
    public var globalClass: GlobalClass? = null

    @JvmField
    var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()

    init {
        this.aofData = aofData
        globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
        sharedPreferenceManager.getInstance(globalClass)
        draftAOFDataInSP()
        setDraftInAllAOFSubModels()
        setDataInNadraResponse()
//        initAllModels()
        if(isShowRecord) {
            setBranchAndOtherCodes(this.aofData)
        }
        var login = sharedPreferenceManager.loginData
        var branchCode = this.aofData.data.getOrNull(0)?.USER_BRANCH
        login.setBRANCHCODE(branchCode!!)
//        login.setBRANCHNAME(array[1])
//        login.setBRANCHTYPE((context as HawActivity).sharedPreferenceManager.branchType.BR_TYPE.toString())
//        login.setBRANCHADDRESS(branchCode.BR_ADDR1.toString() + " " + branchCode.BR_ADD2.toString() + " " + branchCode.BR_ADD3.toString() + " " + branchCode.BR_ADD4.toString())
        sharedPreferenceManager.loginData = login
    }

    private fun setDataInNadraResponse() {
        sharedPreferenceManager.nadraResponseData = sharedPreferenceManager.customerBiomatric
    }

    fun initAllModels(){
        (context as HawActivity).init()
    }

    private fun draftAOFDataInSP() {
        sharedPreferenceManager.setAofAccountInfo(
            this.aofData.data.get(
                0
            )
        )
    }

    fun setBranchAndOtherCodes(aofData: AofAccountInfoResponse) {
        var dataBranchCodeArr= aofData.data.getOrNull(0)?.BranchCode.toString()
        var array=dataBranchCodeArr.split("-")
        callBranchCode(array[0])
    }

    fun callBranchCode(branchCode: String) {
        globalClass?.showDialog(context)
        var hblhrStore: HBLHRStore = HBLHRStore()
        var branchCodeRequest = BranchCodeRequest()
        branchCodeRequest.find.BR_CODE = branchCode
        branchCodeRequest?.identifier = Constants.BRANCH_CODE_INDENTIFIER
        val gson = Gson()
        val json = gson.toJson(branchCodeRequest)
        hblhrStore!!.getBranchCode(
            RetrofitEnums.URL_HBL,
            branchCodeRequest,
            object : BranchCodeCallBack {
                override fun BranchCodeSuccess(response: BranchCodeResponse) {
                    GlobalClass.sharedPreferenceManager!!.setBranchCode(response?.data?.get(0))
                    callBranchType(branchCode)

                }

                override fun BranchCodeFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(context, response.message, 1)
                    globalClass?.hideLoader()
                }
            })
    }

    fun callBranchType(branchCode: String) {
        var branchCodeRequest = BranchCodeRequest()
        branchCodeRequest.find.BR_CODE=branchCode
        branchCodeRequest?.identifier = Constants.BRANCH_TYPE_INDENTIFIER
        val gson = Gson()
        val json = gson.toJson(branchCodeRequest)
        HBLHRStore!!.instance!!.getBranchType(
            RetrofitEnums.URL_HBL,
            branchCodeRequest,
            object : BranchTypeCallBack {
                override fun BranchTypeSuccess(response: BranchTypeResponse) {
                    try {
                        GlobalClass.sharedPreferenceManager!!.setBranchType(
                            response?.data?.get(
                                0
                            )
                        )
                        globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(

                            (context as HawShowRecords),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            context,
                            (context as HawShowRecords).getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (context as HawShowRecords),
                            (e as Exception)
                        )
                    }
                }

                override fun BranchTypeFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(context, response.message, 1)
                    (context as HawShowRecords).globalClass?.hideLoader()
                }
            })
    }
    
    
    private fun setDraftInAllAOFSubModels() {
        this.aofData.data.getOrNull(0).let { data ->
            if (!data?.CUST_INFO.isNullOrEmpty()) {
                data?.CUST_INFO?.getOrNull(0).let { sharedPreferenceManager.customerInfo = it }
            }
            if (!data?.CUST_BIOMETRIC.isNullOrEmpty()) {
                data?.CUST_BIOMETRIC?.getOrNull(0)
                    .let { sharedPreferenceManager.customerBiomatric = it }
            }
            if (!data?.CUST_PEP.isNullOrEmpty()) {
                data?.CUST_PEP?.getOrNull(0).let { sharedPreferenceManager.customerPep = it }
            }
            if (!data?.CUST_FIN.isNullOrEmpty()) {
                data?.CUST_FIN?.getOrNull(0).let { sharedPreferenceManager.customerFIN = it }
            }
            if (!data?.CUST_SOLE_SELF.isNullOrEmpty()) {
                data?.CUST_SOLE_SELF?.getOrNull(0)
                    .let { sharedPreferenceManager.customerSoleSelf = it }
            }
            if (!data?.CUST_EDD.isNullOrEmpty()) {
                data?.CUST_EDD?.getOrNull(0).let { sharedPreferenceManager.customerEDD = it }
            }
            if (!data?.CUST_CDD.isNullOrEmpty()) {
                data?.CUST_CDD?.getOrNull(0).let { sharedPreferenceManager.customerCDD = it }
            }
            if (!data?.CUST_CONTACTS.isNullOrEmpty()) {
                data?.CUST_CONTACTS?.getOrNull(0)
                    .let { sharedPreferenceManager.customerContacts = it }
            }
            if (!data?.CUST_ADDR.isNullOrEmpty()) {
                data?.CUST_ADDR.let { sharedPreferenceManager.customerAddress = it }
            }
            if (!data?.CUST_ACCOUNTS.isNullOrEmpty()) {
                data?.CUST_ACCOUNTS?.getOrNull(0)
                    .let { sharedPreferenceManager.customerAccount = it }
            }
            if (!data?.CUST_DEMOGRAPHICS.isNullOrEmpty()) {
                data?.CUST_DEMOGRAPHICS?.getOrNull(0)
                    .let { sharedPreferenceManager.customerDemoGraphics = it }
            }
            if (!data?.CUST_NEXTOFKIN.isNullOrEmpty()) {
                data?.CUST_NEXTOFKIN?.getOrNull(0)
                    .let { sharedPreferenceManager.customerNextOfKin = it }
            }
            if (!data?.CUST_STATUS.isNullOrEmpty()) {
                data?.CUST_STATUS?.getOrNull(0).let { sharedPreferenceManager.customerStatus = it }
            }
            if (!data?.GUARDIAN_INFO.isNullOrEmpty()) {
                data?.GUARDIAN_INFO?.getOrNull(0).let { sharedPreferenceManager.setGaurdianInfo(it) }
            }
            if (!data?.USER_INFO.isNullOrEmpty()) {
                data?.USER_INFO?.getOrNull(0).let { sharedPreferenceManager.customerUserInfo = it }
            }
            if (!data?.CUST_DOCUMENTS.isNullOrEmpty()) {
                data?.CUST_DOCUMENTS?.getOrNull(0)
                    .let { sharedPreferenceManager.customerDocument = it }
            }
            if (!data?.DOC_CHECKLIST.isNullOrEmpty()) {
                data?.DOC_CHECKLIST?.let { sharedPreferenceManager.docCheckList = it }
            }
            if (data?.PARTIAL_DOCS != null) {
                data?.PARTIAL_DOCS?.let { sharedPreferenceManager.partialDocs = it }
            }
        }
    }

}