package com.hbl.bot.utils

import android.content.Context
import com.hbl.bot.network.models.request.baseRM.CUSTEDD
import com.hbl.bot.network.models.request.baseRM.CUSTPEP
import com.hbl.bot.ui.activities.spark.CIFRootActivity

/*REMOVE SHAREDPREFERENCES MODELS OF SPECIFICS FIELDS*/
class RMVSPModels(context: Context) {

    var context: Context? = null

    @JvmField
    var customerEdd: CUSTEDD = CUSTEDD()

    @JvmField
    var customerPep: CUSTPEP = CUSTPEP()

    init {
        this.context = context
    }

    fun enterKey(key: Int) {
        /*CHECK KEY AND REMOVE SPECIFIC FIELDS*/
        if (key == Constants.PEP_REMOVER) {
            rmvPEP()
        } else if (key == Constants.FATCA_REMOVER) {
            rmvFATCA()
        }
    }

    fun rmvPEP() {
        /*Remove Specific Fields*/
        /*PEP PAGE 1*/
        (this.context as CIFRootActivity).customerPep.REASON_PEP_INDIVIDUAL = ""
        (this.context as CIFRootActivity).customerPep.REASON_PEP_INDIVIDUAL = ""
        (this.context as CIFRootActivity).customerPep.DESC_REASON_PEP_INDIVIDUAL = ""
        (this.context as CIFRootActivity).customerPep.PROFILE_CUST_BUSS_OCCUPATION = ""
        (this.context as CIFRootActivity).customerPep.PERIOD_SENIOR_POSITION_HELD = ""
        (this.context as CIFRootActivity).customerPep.COUNTRY_SENIOR_POSITION_HELD = ""
        (this.context as CIFRootActivity).customerPep.DESC_COUNTRY_SENIOR_POSITION_HELD = ""

        /*PEP PAGE 2*/
        (this.context as CIFRootActivity).customerPep.SOURCE_OF_FUND = ""
        (this.context as CIFRootActivity).customerPep.DESC_SOURCE_OF_FUND = ""
        (this.context as CIFRootActivity).customerPep.OTHER_SOURCE_OF_FUND = ""
        (this.context as CIFRootActivity).customerPep.SOURCE_OF_WEALTH = ""
        (this.context as CIFRootActivity).customerPep.DESC_SOURCE_OF_WEALTH = ""
        (this.context as CIFRootActivity).customerPep.OTHER_SOURCE_OF_WEALTH = ""
        (this.context as CIFRootActivity).customerPep.ADDITIONAL_INFO = ""

        /*PEP PAGE 3*/
        (this.context as CIFRootActivity).customerPep.ANALYSIS_OF_PEP = ""
        (this.context as CIFRootActivity).customerPep.FOREIGN_PEP = ""
        (this.context as CIFRootActivity).customerPep.DOMESTIC_PEP = ""
        (this.context as CIFRootActivity).customerPep.ADVERSE_MEDIA_CUSTOMER = ""
        (this.context as CIFRootActivity).sharedPreferenceManager.customerPep =
            ((this.context as CIFRootActivity).customerPep)
        (this.context as CIFRootActivity).customerPepList.add(((this.context as CIFRootActivity).customerPep))
    }

    fun rmvEDD() {
        /*Remove Specific Fields*/
        /*PEP PAGE 1*/
        (this.context as CIFRootActivity).customerEdd.rEASON_EDD = ""
        (this.context as CIFRootActivity).customerEdd.dESC_REASON_EDD = ""
        (this.context as CIFRootActivity).customerEdd.iS_NON_RESIDENT_INFORMATION = ""
        (this.context as CIFRootActivity).customerEdd.fOREIGN_CONTACT_NO = ""
        (this.context as CIFRootActivity).customerEdd.fOREIGN_RESIDENT_ADDRESS_1 = ""
        (this.context as CIFRootActivity).customerEdd.fOREIGN_RESIDENT_ADDRESS_2 = ""
        (this.context as CIFRootActivity).customerEdd.fOREIGN_OFFICE_ADDRESS_1 = ""
        (this.context as CIFRootActivity).customerEdd.fOREIGN_OFFICE_ADDRESS_2 = ""
        (this.context as CIFRootActivity).customerEdd.aGRI_INCOME_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.aGRI_RENTAL_INCOME_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.bUSINESS_INCOME_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.rENTAL_INCOME_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.rENTAL_INCOME_RES_PROJ_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.oTHER_SOURCE_2 = ""
        (this.context as CIFRootActivity).customerEdd.cONDUCT_FOREIGN_BANK_TRANSFER = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_CODE_INWARD_FOREIGN1 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_DESC_INWARD_FOREIGN1 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_CODE_OUTWARD_FOREIGN1 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_DESC_OUTWARD_FOREIGN1 = ""
        (this.context as CIFRootActivity).customerEdd.nAME_OF_BENEFICIARY_FOREIGN_TRANSFER_1 = ""
        (this.context as CIFRootActivity).customerEdd.rELATIONSHIP_FOREIGN_BENEFICIARY_1 = ""
        (this.context as CIFRootActivity).customerEdd.rELATIONSHIP_FOREIGN_BENEFICIARY_DESC_1 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_CODE_INWARD_FOREIGN2 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_DESC_INWARD_FOREIGN2 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_CODE_OUTWARD_FOREIGN2 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_DESC_OUTWARD_FOREIGN2 = ""
        (this.context as CIFRootActivity).customerEdd.nAME_OF_BENEFICIARY_FOREIGN_TRANSFER_2 = ""
        (this.context as CIFRootActivity).customerEdd.rELATIONSHIP_FOREIGN_BENEFICIARY_2 = ""
        (this.context as CIFRootActivity).customerEdd.rELATIONSHIP_FOREIGN_BENEFICIARY_DESC_2 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_CODE_INWARD_FOREIGN3 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_DESC_INWARD_FOREIGN3 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_CODE_OUTWARD_FOREIGN3 = ""
        (this.context as CIFRootActivity).customerEdd.cOUNTRY_DESC_OUTWARD_FOREIGN3 = ""
        (this.context as CIFRootActivity).customerEdd.nAME_OF_BENEFICIARY_FOREIGN_TRANSFER_3 = ""
        (this.context as CIFRootActivity).customerEdd.rELATIONSHIP_FOREIGN_BENEFICIARY_3 = ""
        (this.context as CIFRootActivity).customerEdd.rELATIONSHIP_FOREIGN_BENEFICIARY_DESC_3 = ""
        (this.context as CIFRootActivity).customerEdd.pURPOSE_OF_FOREIGN_TRANSACTION = ""
        (this.context as CIFRootActivity).customerEdd.aMOUNT_OF_FOREIGN_TRANSACTION = ""
        (this.context as CIFRootActivity).customerEdd.iD_NO = ""
        (this.context as CIFRootActivity).customerEdd.nATIONALITY = ""
        (this.context as CIFRootActivity).customerEdd.dESC_NATIONALITY = ""
        (this.context as CIFRootActivity).customerEdd.fOREIGN_CONTACT_NO_HIGH_RISK_NRP = ""
        (this.context as CIFRootActivity).customerEdd.rESIDENTIAL_ADDRESS = ""
        (this.context as CIFRootActivity).customerEdd.oFFICE_ADDRESS_FOREIGN_COUNTRY = ""
        (this.context as CIFRootActivity).customerEdd.rELATIONSHIP_WITH_FIN = ""
        (this.context as CIFRootActivity).customerEdd.dESC_RELATIONSHIP_WITH_FIN = ""
        (this.context as CIFRootActivity).customerEdd.fIN_SUPP_GUR_FMLY_PEP = ""
        (this.context as CIFRootActivity).customerEdd.sAL_OF_FIN_SUPPORTER_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.rENTAL_INCOME_FIN_SUPPORTER_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.bUSS_INCOME_FIN_SUPPORTER_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.rETIREMENT_FUND_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.pENSION_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.iNHERITANCE_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.oTHER_EXP_AMT = ""
        (this.context as CIFRootActivity).customerEdd.oTHER_SOURCE_NRP = ""
        (this.context as CIFRootActivity).customerEdd.eDD_CONDUCT_BY_NAME = ""
        (this.context as CIFRootActivity).customerEdd.aPPROVED_BY_NAME = ""
        (this.context as CIFRootActivity).customerEdd.aPPROVED_COMPL_AUTH_NAME = ""
        (this.context as CIFRootActivity).customerEdd.eDD_CONDUCT_BY_DATE = ""
        (this.context as CIFRootActivity).customerEdd = customerEdd
        (this.context as CIFRootActivity).sharedPreferenceManager.customerEDD = customerEdd

    }

    fun rmvFATCA() {
        /*FATCA PAGE 1*/
        (this.context as CIFRootActivity).customerInfo.US_ADDRESS = ""
        (this.context as CIFRootActivity).customerInfo.US_ADDRESS_2 = ""
        (this.context as CIFRootActivity).customerInfo.US_TELEPHONE = ""
        (this.context as CIFRootActivity).customerInfo.US_RES_CARD_NO = ""
        (this.context as CIFRootActivity).customerInfo.EXPIRY_DATE_OF_OVERSEAS_CARD = ""
        (this.context as CIFRootActivity).customerInfo.FATCA_COUNTRY_CODE = ""
        (this.context as CIFRootActivity).customerInfo.FATCA_COUNTRY_DESC = ""

        /*FATCA PAGE 2*/
        (this.context as CIFRootActivity).customerInfo.IRS_FORM_W8_SUBMITTED = ""
        (this.context as CIFRootActivity).customerInfo.TIN = ""
        (this.context as CIFRootActivity).customerInfo.SPEND_DAYS = ""
        (this.context as CIFRootActivity).customerInfo.RENOUNCED_US_CITIZENSHIP = ""
        (this.context as CIFRootActivity).customerInfo.TAX_RES_COUNTRY = ""
        (this.context as CIFRootActivity).customerInfo.TAX_RES_COUNTRY_NAME = ""
        (this.context as CIFRootActivity).customerInfo.IRS_FORM_W9_SUBMITTED = ""

        (this.context as CIFRootActivity).sharedPreferenceManager.customerInfo =
            (this.context as CIFRootActivity).customerInfo
    }

    fun rmvCRS() {
        /*CRS PAGE 1*/
        (this.context as CIFRootActivity).customerInfo.EXPIRY_DATE_OF_OVERSEAS_CARD = ""
        (this.context as CIFRootActivity).customerInfo.OVERSEAS_CARD_NUMBER = ""
        (this.context as CIFRootActivity).customerInfo.NAME_OF_COUNTRY1 = ""
        (this.context as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC1 = ""
        (this.context as CIFRootActivity).customerInfo.NAME_OF_COUNTRY1 = ""
        (this.context as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC1 = ""
        (this.context as CIFRootActivity).customerInfo.TIN1 = ""
        (this.context as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN1 = ""
        (this.context as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN1 = ""
        (this.context as CIFRootActivity).customerInfo.NAME_OF_COUNTRY2 = ""
        (this.context as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC2 = ""
        (this.context as CIFRootActivity).customerInfo.TIN2 = ""
        (this.context as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN2 = ""
        (this.context as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN2 = ""
        (this.context as CIFRootActivity).customerInfo.NAME_OF_COUNTRY3 = ""
        (this.context as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC3 = ""
        (this.context as CIFRootActivity).customerInfo.TIN3 = ""
        (this.context as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN3 = ""
        (this.context as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN3 = ""

        /*CRS PAGE 2*/
        (this.context as CIFRootActivity).customerInfo.PASSPORT_EXPIRATION_DATE = ""
        (this.context as CIFRootActivity).customerInfo.CAPACITY_CODE = ""
        (this.context as CIFRootActivity).customerInfo.CAPACITY_DESC = ""
        (this.context as CIFRootActivity).customerInfo.SECOND_NATIONALITY_ID = ""
        (this.context as CIFRootActivity).customerInfo.TAX_PAYER_BIRTH_CITY_CODE = ""
        (this.context as CIFRootActivity).customerInfo.TAX_PAYER_BIRTH_CITY_NAME = ""
        (this.context as CIFRootActivity).customerInfo.IS_FORCE_CRS = ""
        (this.context as CIFRootActivity).customerInfo.DATE_CRS_FORM_SIGNED = ""

        (this.context as CIFRootActivity).sharedPreferenceManager.customerInfo =
            (this.context as CIFRootActivity).customerInfo
    }
}