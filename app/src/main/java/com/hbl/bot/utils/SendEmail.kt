package com.hbl.bot.utils

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.os.Handler
import android.os.Looper
import android.util.Log
import com.hbl.bot.utils.mail.GMailSender
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException


object SendEmail {
    @JvmField
    public var globalClass: GlobalClass? = null
    var versionName = ""
    var versionCode = ""
    var tabletID = ""
    var trakingID = ""
    var mySisID = ""
    var username = ""
    var dateTime = ""
    var subject = ""
    var plusCrashSubject = ""
    var systemInfo = ""

    init {
        globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
        if (!GlobalClass.sharedPreferenceManager?.trackingID?.getBatch()
                ?.get(0)?.itemDescription.toString().isNullOrEmpty()
        ) {
            trakingID = GlobalClass.sharedPreferenceManager?.trackingID?.getBatch()
                ?.get(0)?.itemDescription.toString()
        } else {
            trakingID = "NA"
            trakingID = "NA"
        }
        dateTime = GlobalClass.getCurrentTimeAndDate(Constants.fomratDateTimeException).toString()
        if (Config.BASE_URL_HBL.contains(Config.LIVE)) {
            subject = "Live - DateTime: $dateTime | Username: ${GlobalClass.userLogin}"
        } else if (Config.BASE_URL_HBL.contains(Config.RP_TEST) || Config.BASE_URL_HBL.contains(
                Config.HOSTPOT)
        ) {
            subject = "Test - DateTime: $dateTime | Username: ${GlobalClass.userLogin}"
        }
        systemInfo = "----------SystemInfo----------\n" +
                "1).Date Time: $dateTime\n" +
                "2).MySisID: ${GlobalClass.mysisiID}\n" +
                "3).Username: ${GlobalClass.userLogin}\n" +
                "4).TabletID: ${GlobalClass.deviceID()}\n" +
                "5).Tracking ID: $trakingID\n" +
                "6).Version Name: ${GlobalClass.versionName}\n" +
                "7).Version Code: ${GlobalClass.versionCode}\n\n"
        globalClass?.hideLoader()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendAttachmentEmail(
        context: Context, e: File, messageSubject: String, messageBody: String, className: String,
    ) {
        Log.i("SystemExceptions", "$systemInfo sending email...")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                sender.sendMail(context, e, subject,
                    systemInfo +
                            "----------ScreenshortInfo----------\n" +
                            "Subject: ${messageSubject}\n" +
                            "Body: ${messageBody}\n" +
                            "FileName: ${e.name}\n" +
                            "Class/Fragment/Activity: $className",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com", "mohammad.zeeshan1@hbl.com")
                Log.i("SystemExceptions", "Attachment Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: UnsatisfiedLinkError,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: NoClassDefFoundError,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: ArrayIndexOutOfBoundsException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: NullPointerException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: IllegalArgumentException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: NumberFormatException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: InterruptedException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: RuntimeException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: FileNotFoundException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: IOException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: ClassNotFoundException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: ActivityNotFoundException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: IndexOutOfBoundsException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: TypeCastException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: SecurityException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: IllegalStateException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: OutOfMemoryError,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: Exception,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, e: ClassCastException,
    ) {
        Log.i("SystemExceptions", systemInfo +
                "----------ExceptionInfo----------\n" +
                "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                "5).Crash Message: ${e.message}\n" +
                "7).Stack Trace Message: ${e.stackTrace}\n" +
                "8).Localize Message: ${e.localizedMessage}\n" +
                "9).Suppressed: ${e.suppressed}" +
                "10).Full Detail: ${e.stackTrace.get(0)}}")
        Thread {
            try {
                val sender = GMailSender("app.bugreport1@gmail.com", "hbl@1234")
                plusCrashSubject =
                    subject + " | Class: ${e.stackTrace[0].fileName} | Method: ${e.stackTrace[0].methodName} | WhyCrash: ${e.javaClass.simpleName}"
                sender.sendMail(plusCrashSubject,
                    systemInfo +
                            "----------ExceptionInfo----------\n" +
                            "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                            "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                            "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                            "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                            "5).Crash Message: ${e.message}\n" +
                            "7).Stack Trace Message: ${e.stackTrace}\n" +
                            "8).Localize Message: ${e.localizedMessage}\n" +
                            "9).Suppressed: ${e.suppressed}" +
                            "10).Full Detail: ${e.stackTrace.get(0)}}",
                    "app.bugreport1@gmail.com",
                    "app.bugreport1@gmail.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }

    @SuppressLint("WrongConstant")
    @JvmStatic
    fun sendEmail(
        context: Context, data: String
    ) {
        Thread {
            try {
                val sender = GMailSender("info.xeeshan@gmail.com", "AalooMatar786*")
                sender.sendMail("Android 11 Device Email Populated",
                    systemInfo +"\n"+data,
                    "info.xeeshan@gmail.com",
                    "mohammad.zeeshan1@hbl.com")
                Log.i("SystemExceptions", "Email successfully sent!")
            } catch (e: java.lang.Exception) {
                Log.i("SystemExceptions", systemInfo +
                        "----------ExceptionInfo----------\n" +
                        "1).Why CRASH: ${e.javaClass.simpleName}\n" +
                        "2).Method Name: ${e.stackTrace[0].methodName}\n" +
                        "3).Class/Fragment/Activity: ${e.stackTrace.get(0).fileName}\n" +
                        "4).Line Number: ${e.stackTrace[0].lineNumber}\n" +
                        "5).Crash Message: ${e.message}\n" +
                        "7).Stack Trace Message: ${e.stackTrace}\n" +
                        "8).Localize Message: ${e.localizedMessage}\n" +
                        "9).Suppressed: ${e.suppressed}" +
                        "10).Full Detail: ${e.stackTrace.get(0)}}")
                Handler(Looper.getMainLooper()).post(Runnable {
                })
            }
        }.start()
    }


}