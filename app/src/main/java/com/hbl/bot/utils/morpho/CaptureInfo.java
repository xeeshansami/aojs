package com.hbl.bot.utils.morpho;

import com.morpho.morphosmart.sdk.CompressionAlgorithm;
import com.morpho.morphosmart.sdk.SecurityLevel;
import com.morpho.morphosmart.sdk.TemplateType;

public class CaptureInfo {
    private static CaptureInfo	mInstance	= null;
    private TemplateType templateType = TemplateType.MORPHO_NO_PK_FP;
    private SecurityLevel securityLevel	= SecurityLevel.FFD_SECURITY_LEVEL_LOW_HOST;
    private CompressionAlgorithm compressionAlgorithm = CompressionAlgorithm.NO_IMAGE;

    public static CaptureInfo getInstance() {
        if (mInstance == null)
        {
            mInstance = new CaptureInfo();
        }
        return mInstance;
    }
    private CaptureInfo() {

    }

    public void setTemplateType(TemplateType templateType) {
        this.templateType = templateType;
    }

    public TemplateType getTemplateType() {
        return templateType;
    }

    public void setSecurityLevel(SecurityLevel securityLevel) {
        this.securityLevel = securityLevel;
    }

    public SecurityLevel getSecurityLevel()
    {
        return securityLevel;
    }

    public CompressionAlgorithm getCompressionAlgorithm() {
        return compressionAlgorithm;
    }


    public void setCompressionAlgorithm(CompressionAlgorithm compressionAlgorithm) {
        this.compressionAlgorithm = compressionAlgorithm;
    }
}
