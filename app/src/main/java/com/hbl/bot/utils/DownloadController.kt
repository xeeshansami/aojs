package com.hbl.bot.utils

//import com.badoualy.stepperindicator.BuildConfig
//import com.hbl.bot.BuildConfig
import android.app.DownloadManager
import android.content.Context
import android.content.Intent
import org.apache.commons.codec.binary.Hex;

import android.database.Cursor
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.*
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.ProgressBar
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.FileProvider
import com.hbl.bot.BuildConfig
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.AppStatusCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.AppDetailsRequest
import com.hbl.bot.network.models.response.base.AppStatusResponse
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.ui.activities.SplashActivity
import org.apache.commons.codec.digest.DigestUtils
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.nio.file.Files
import java.nio.file.Paths


class DownloadController(
    private val context: Context,
    private val url: String,
    private val apkName: String,
    private val msg: String,
) {

    private var dl_progress: Int = 0
    private var fName = "";

    init {
        this.fName = apkName
        var destination = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/"
        destination += apkName
        // execute this when the downloader must be fired
        val file = File(destination)
        if(file.exists()){
            file.deleteRecursively()
        }
        if (file.exists()) file.delete()
        var downloadTask = DownloadTask(context, apkName, msg);
        downloadTask.execute(url);
    }


    companion object {
        //        private const val FILE_NAME = "BOT_V1.apk"
        private const val FILE_BASE_PATH = "file://"
        private const val MIME_TYPE = "application/vnd.android.package-archive"
        private const val PROVIDER_PATH = ".provider"
        private const val APP_INSTALL_PATH = "\"application/vnd.android.package-archive\""
    }

        fun enqueueDownload() {
        var handler = Handler()
//        progressBar = ProgressDialog(context)

        var destination = context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/"
        destination += apkName
        val uri = Uri.parse("$FILE_BASE_PATH$destination")
        val file = File(destination)
        if (file.exists()) file.delete()

        val downloadManager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager
        val downloadUri = Uri.parse(url)
        val request = DownloadManager.Request(downloadUri)
        request.setMimeType(MIME_TYPE)
        request.setTitle(context.getString(R.string.title_file_download))
        request.setDescription(context.getString(R.string.downloading))
        // set destination
        request.setDestinationUri(uri)

        // Enqueue a new download and same the referenceId
        var downloadid = downloadManager.enqueue(request)

        ToastUtils.normalShowToast(context, context.getString(R.string.downloading), 2)
        Thread(Runnable {
            //some method here
            var downloading = true
            while (downloading) {
                val q = DownloadManager.Query()
                q.setFilterById(downloadid)
                val cursor: Cursor = downloadManager.query(q)
                cursor.moveToFirst()
                val bytes_downloaded = cursor.getInt(
                    cursor
                        .getColumnIndex(DownloadManager.COLUMN_BYTES_DOWNLOADED_SO_FAR)
                )
                val bytes_total =
                    cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_TOTAL_SIZE_BYTES))

                if (cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS)) == DownloadManager.STATUS_SUCCESSFUL) {
                    ToastUtils.normalShowToast(context, "APK Downloaded", 2)
//                    showInstallOption(destination, uri)
                    downloading = false
                }

                dl_progress = (bytes_downloaded * 100L / bytes_total).toInt()
                handler.post {
                    /*    progressBar.setTitle("Downloading Update")
                        progressBar.setCancelable(false)
                        progressBar.setMessage("Application Downloading, please wait ($dl_progress%)")
                        progressBar.show()*/

                }


            }
        }).start()

    }

    private class DownloadTask(private val context: Context, apkName: String, msg: String) :
        AsyncTask<String?, Int?, String?>() {
        var alertDialogBuilder: AlertDialog.Builder? = null
        var progressBar: ProgressBar? = null
        var alertDialog: AlertDialog? = null
        var percentageView: TextView? = null
        var messageBody: TextView? = null
        var messageBody2: TextView? = null
        var timeView: TextView? = null
        var mbsView: TextView? = null
        var apkName = ""
        var msg = ""
        fun downloaderAlert(context: Context, title: String, subtitle: String, msg: String) {
            alertDialogBuilder = AlertDialog.Builder((context as NavigationDrawerActivity))
            val li = LayoutInflater.from(context)
            val promptsView = li.inflate(R.layout.popup_downloader, null)
            alertDialog = alertDialogBuilder!!.create()
            alertDialog!!.setView(promptsView)
            alertDialog!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog!!.setCancelable(false)
            alertDialog!!.show()
            val toolbar: Toolbar = promptsView.findViewById(R.id.toolbar)
            toolbar.title = "NEW UPDATE"
            toolbar.subtitle = title
            toolbar.setTitleTextColor(context.resources.getColor(R.color.colorWhite))
            toolbar.setSubtitleTextColor(context.resources.getColor(R.color.colorWhite))
            messageBody = promptsView.findViewById<TextView>(R.id.messageBody)
            timeView = promptsView.findViewById<TextView>(R.id.timeView)
            percentageView = promptsView.findViewById<TextView>(R.id.percentageView)
            mbsView = promptsView.findViewById<TextView>(R.id.mbsView)
            messageBody2 = promptsView.findViewById<TextView>(R.id.messageBody2)
            progressBar = promptsView.findViewById<ProgressBar>(R.id.progressBar)
            progressBar!!.isIndeterminate = false
            var cancel_action = promptsView.findViewById<Button>(R.id.cancel_action)
            messageBody!!.text = subtitle
            messageBody2!!.text = msg
            cancel_action!!.setOnClickListener(View.OnClickListener {
                ToastUtils.normalShowToast(
                    context,
                    "Downloading Forcefully Cancelled: Logging out now...",
                    1
                )
                Handler().postDelayed({
                    (context as NavigationDrawerActivity).finish()
                    (context as NavigationDrawerActivity).startActivity(
                        Intent(
                            context as NavigationDrawerActivity,
                            SplashActivity::class.java
                        )
                    )
                }, 1000)
                alertDialog!!.dismiss()
            })

            alertDialog!!.setOnDismissListener {

            }
        }

        init {
            this.apkName = apkName
            this.msg = msg
            // instantiate it within the onCreate method
            // instantiate it within the onCreate method
            (context as NavigationDrawerActivity).runOnUiThread {
                downloaderAlert(
                    context,
                    context.getString(R.string.title_file_download),
                    context.getString(R.string.downloading),
                    msg
                )
            }

        }

        fun fromCountToTimeByInterval(count: Int, interval: Int): String {
//            val minutes = count * interval / 1000 / 60
//            val seconds = count * interval / 1000 % 60
//            val milliSeconds = (count * interval % 1000) / 10

            val hours = count * interval / 3600
            val minutes = count * interval % 3600 / 60
            val seconds = count * interval % 60

            return String.format("%02d:%02d:%02d", hours, minutes, seconds)
//             String.format("%02d:%02d.%02d", minutes, seconds, milliSeconds)
        }

        private var mWakeLock: PowerManager.WakeLock? = null

        @RequiresApi(Build.VERSION_CODES.O)
        override fun doInBackground(vararg sUrl: String?): String? {
            var url: URL? = null
            var input: InputStream? = null
            var output: OutputStream? = null
            var connection: HttpURLConnection? = null
            try {
                var newUrl = ""
                if (!sUrl[0]!!.contains("https")) {
                    newUrl = sUrl[0]!!.replace("http", "https");
                    url = URL(newUrl)
                } else {
                    url = URL(sUrl[0]!!)
                }
                connection = url.openConnection() as HttpURLConnection
                connection.useCaches = false
                connection.connect()
                Log.i("APKURL", newUrl)
                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() !== HttpURLConnection.HTTP_OK) {
                    Log.i(
                        "APKURL", "Server returned HTTP " + connection.getResponseCode()
                            .toString() + " " + connection.getResponseMessage()
                    )
                    return "Server returned HTTP " + connection.getResponseCode()
                        .toString() + " " + connection.getResponseMessage()
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                val fileLength: Int = connection.getContentLength()

                // download the file
                input = connection.getInputStream()
                var destination =
                    context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/"
                destination += apkName
                output = FileOutputStream(destination)
                val data = ByteArray(4096)
                var total: Long = 0
                var count: Int

                val startTime = System.nanoTime()
                val elapsedTime = System.nanoTime() - startTime



                while (input.read(data).also { count = it } != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close()
                        return null
                    }
                    total += count.toLong()
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((total * 100 / fileLength).toInt())
                    val kilobytes: Long = (total / 1024)
                    val megabytes: Long = (kilobytes / 1024)

                    val allTimeForDownloading: Long = elapsedTime * total / fileLength
                    var remainingTime = allTimeForDownloading - elapsedTime
                    var time = remainingTime * -1000
                    remainingTime = Math.sqrt(Math.pow(remainingTime.toDouble(), 2.0)).toLong()
                    Log.i("TimeSpeed", "${megabytes} $kilobytes")
                    (context as NavigationDrawerActivity).runOnUiThread {
                        mbsView!!.text =
                            "File Size: " + megabytes.toString() + "/" + fileLength / (1024 * 1024) + " MB(s)"
//                        timeView!!.text = convertSeconds(time.toInt()/1000)+
//                        timeView!!.setText(fromCountToTimeByInterval(remainingTime.toInt(),1)+" min(s) remaining");
                        timeView!!.setText(
                            "Estimated time left: " + fromCountToTimeByInterval(
                                remainingTime.toInt(),
                                1
                            )
                        );
                        Log.i("Downloading", fromCountToTimeByInterval(remainingTime.toInt(), 1))
                    }
                    output.write(data, 0, count)
                }
            } catch (e: Exception) {
                return e.toString()
            } finally {
                try {
                    output?.close()
                    input?.close()
                } catch (ignored: IOException) {
                }
                connection?.disconnect()
            }
            return null
        }

        override fun onPreExecute() {
            super.onPreExecute()
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download

            val pm = context.getSystemService(Context.POWER_SERVICE) as PowerManager
            mWakeLock = pm.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK,
                javaClass.name
            )
            mWakeLock!!.acquire()

        }

        override fun onProgressUpdate(vararg progress: Int?) {
            super.onProgressUpdate(*progress)
            // if we get here, length is known, now set indeterminate to false
            progressBar!!.isIndeterminate = false
            progressBar!!.max = 100
            progress[0]?.let {
                progressBar!!.progress = it
                percentageView!!.text = "$it%"
            }
        }

        override fun onPostExecute(result: String?) {
            mWakeLock!!.release()
            alertDialog!!.dismiss()
            if (result != null) {
                if(result!!.contains("Software caused connection abort")){
                    ToastUtils.normalShowToast(
                        context.applicationContext,
                        "You have a network connection issue, please check your internet and try download again for update",
                        1
                    )
                }else{
                    ToastUtils.normalShowToast(
                        context.applicationContext,
                        "Download error: $result",
                        1
                    )
                }

                Log.i("DownloadError",result.toString())
                Handler().postDelayed({
                    ToastUtils.normalShowToast(
                        context.applicationContext,
                        "Logging out...",
                        1
                    )
                    (context as NavigationDrawerActivity).finish()
                    (context as NavigationDrawerActivity).startActivity(
                        Intent(
                            context as NavigationDrawerActivity,
                            SplashActivity::class.java
                        )
                    )
                }, 3000)
            } else {
                ToastUtils.normalShowToast(
                    context,
                    "Apk File downloaded",
                    2
                )
                var destination =
                    context.getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS).toString() + "/"
                destination += apkName
                val uri = Uri.parse("$FILE_BASE_PATH$destination")
                showInstallOption(destination, uri)
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        private fun showInstallOption(
            destination: String,
            uri: Uri,
        ) {
            var checkcum =
                getSHA256OfFile("/storage/emulated/0/Android/data/com.hbl.bot/files/Download/$apkName")

            if (checkcum.equals(GlobalClass.checksum)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    val contentUri = FileProvider.getUriForFile(
                        context,
                        BuildConfig.APPLICATION_ID + PROVIDER_PATH,
                        File(destination)
                    )
                    val install = Intent(Intent.ACTION_VIEW)
                    install.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
                    install.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
                    install.putExtra(Intent.EXTRA_NOT_UNKNOWN_SOURCE, true)
                    install.data = contentUri
                    context.startActivity(install)
//                    context.unregisterReceiver(this)
                    // finish()
                } else {
                    val install = Intent(Intent.ACTION_VIEW)
                    install.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                    install.setDataAndType(
                        uri,
                        APP_INSTALL_PATH
                    )
                    context.startActivity(install)
//                    context.unregisterReceiver(this)
                    // finish()
                }
            } else {
                ToastUtils.normalShowToast(context, "checksum miss matched", 1)
                val intent = Intent(context, NavigationDrawerActivity::class.java)
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                context.startActivity(intent)

            }


        }


        fun sendUpdateWhenAppDownload() {
            var request = AppDetailsRequest()
            request.identifier = Constants.APP_DETAILS_IDENTIFIER
            request.apkSize = GlobalClass.apkSize
            request.applicationID = GlobalClass.deviceID()
            request.applicationName = GlobalClass.appName
            request.createdApkDate = GlobalClass.appCreatedDate
            request.updateApkDate = GlobalClass.appUpdatedDate
            request.userID = GlobalClass.mysisiID
            request.versionCode = GlobalClass.versionCode
            request.versionName = GlobalClass.versionName
            request.IMEI ="${GlobalClass.getIMEIDeviceId(context)}"
            request.packageName ="com.hbl.bot"
//            request.androidApiVersion = globalClass!!.getAndroidVersion(android.os.Build.VERSION.SDK_INT).toString()
            HBLHRStore?.instance?.sendAppDetails(RetrofitEnums.URL_HBL, request, object :
                AppStatusCallBack {
                override fun AppStatusSuccess(response: AppStatusResponse) {
                }

                override fun AppStatusFailure(response: BaseResponse) {
                }
            })
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun getSHA256OfFile(filePath: String?): String? {
            if (File(filePath).exists()) {
                sendUpdateWhenAppDownload()
            }

            val array = Files.readAllBytes(Paths.get(filePath))
            val genchecksum = DigestUtils.sha256(array)
            val hex = Hex.encodeHex(genchecksum)
            return String(hex)
        }


    }
}