package com.hbl.bot.utils;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.hbl.bot.R;


public class TextDialog extends Dialog implements
        View.OnClickListener {
    public Context c;
    public Dialog d;
    public Button yes;
    public EditText editText;
    TextInputLayout inputLayout;
    DialogCallback callback;
    public TextDialog(Context a, DialogCallback callback) {
        super(a);
        this.c = a;
        this.callback = callback;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.text_dialog_new);
        inputLayout = findViewById(R.id.inputLayout);
        yes = findViewById(R.id.bt_confirm);
        editText = findViewById(R.id.chilleditText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @SuppressLint("ResourceAsColor")
            @Override
            public void afterTextChanged(Editable s) {
                if (GlobalClass.isValidEmail(s.toString()) && s.toString().endsWith("@hbl.com")) {
                    editText.requestFocus();
                    editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_done_24, 0);
                    inputLayout.setErrorEnabled(false);
                    inputLayout.setErrorTextColor(ColorStateList.valueOf(R.color.green));
                    inputLayout.setHintTextColor(ColorStateList.valueOf(R.color.colorGrayDarkText));
                    inputLayout.setError("Valid Email");
                } else {
                    editText.requestFocus();
                    inputLayout.setErrorEnabled(true);
                    inputLayout.setHintTextColor(ColorStateList.valueOf(R.color.colorGrayDarkText));
                    inputLayout.setErrorTextColor(ColorStateList.valueOf(Color.RED));
                    inputLayout.setError("Invalid Email");
                    editText.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_baseline_cancel_24, 0);
                }
            }
        });
        yes.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_confirm:
                GlobalClass.verifyEmpEmail = editText.getText().toString();
                callback.onPositiveClicked();
                break;
//            case R.id.bt_cancel:
//                callback.onNegativeClicked();
//                break;
            default:
                break;
        }
//        this.dismiss();
    }
}