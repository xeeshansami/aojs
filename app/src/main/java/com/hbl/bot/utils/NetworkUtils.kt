package com.example.bidfeed.utils

import android.annotation.SuppressLint
import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.hbl.bot.utils.GlobalClass

class NetworkUtils {

    companion object {

        @SuppressLint("NewApi")
        fun isInternetAvailable(): Boolean {
            (GlobalClass.applicationContext!!.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager).run {
                return this.getNetworkCapabilities(this.activeNetwork)?.hasCapability(
                    NetworkCapabilities.NET_CAPABILITY_INTERNET
                ) ?: false
            }
        }
    }
}