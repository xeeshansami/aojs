package com.hbl.bot.utils.morpho;

import android.util.Log;

import com.hyf.fingersdk.HYFFingerAlg;
import com.morpho.morphosmart.sdk.TemplateList;
import com.morpho.morphosmart.sdk.TemplateType;

public class Uitls {
    public static int bytes2Int(byte[] src, int offset) {
        int value = 0;
        value = (int) ((src[offset + 3] & 0xFF)
                | ((src[offset + 2] & 0xFF) << 8)
                | ((src[offset + 1] & 0xFF) << 16)
                | ((src[offset] & 0xFF) << 24));
        return value;
    }

    public static short bytes2Short(byte[] src, int offset) {
        short value;
        value = (short) ((src[offset + 1] & 0xFF)
                | ((src[offset] & 0xFF) << 8));
        return value;
    }

    public static int GetWSQMinutiaeCount(TemplateList templateList) {
        byte[] image = templateList.getImage(0).getCompressedImage();
        int count = HYFFingerAlg.getWsqMinutiaeCount(image);
        return count;
    }

    public static int GetWSQNFIQCount(TemplateList templateList) {
        int image = templateList.getTemplate(0).getTemplateQuality();
        int count = HYFFingerAlg.getNFIQVale(image);
        return count;
    }

    public static int GetTemplateMinutiaeNumBer(TemplateList templateList) {
        byte[] template = templateList.getTemplate(0).getData();
        String templateType = templateList.getTemplate(0).getTemplateType().toString();

        if (templateType.equals(TemplateType.MORPHO_PK_ISO_FMR.toString())) {
            return template[27];
        } else if (templateType.equals(TemplateType.MORPHO_PK_ANSI_378_2009.toString())) {
            return template[37];
        } else if (templateType.equals(TemplateType.MORPHO_PK_ISO_FMR_2011.toString())) {
            ///General header
            //Format Identifier  4 bytes  464D5200 Hex (‘F’ ‘M’ ‘R’ 00 Hex ) FMR-finger minutiae record
            //Version Number     4 bytes  30333000 Hex(‘0’ ‘3’ ‘0’ 00 Hex )
            //Length of record   4 bytes >=54=15+39
            //Number of Finger Representations  2 bytes  0001 Hex to 0160 Hex (10 fingers + 1 unknown + 11 multiple finger combinations) times 16 = 352
            //Device Certification Block Flag  1 byte

            ///Finger minutiae representation header
            //Representation length                 4 bytes
            //Capture date and time                 9 bytes
            //Capture device technology identifier  1 byte
            //Capture device vendor identifier      2 bytes
            //Capture device type identifier        2 bytes
            //Quality record                        1 to 1+255*5 bytes
            //Certification record                  0 to 1+255*3 bytes
            //Finger position                       1 byte
            //Representation number                 1 byte  0 to 15
            //Image spatial sampling rate (horiz)   2 bytes
            //Image spatial sampling rate (vert)    2 bytes
            //Impression type                       1 byte
            //Size of scanned image in X-Dir        2 bytes
            //Size of scanned image in Y-Dir        2 bytes
            //Minutiae field length                 4 bits
            //Ridge ending type                     4 bits
            //Number of minutiae                    1 byte  1 to 255
            int fid = bytes2Int(template, 0);
            int ver = bytes2Int(template, 4);
            int len = bytes2Int(template, 8);
            short fpNum = bytes2Short(template, 12);
            byte blockFlag = template[14];
            int mc = 0;

            if (fid == 0x464D5200 && ver == 0x30333000) {
                int pos = 15;
                int rlen = 0;
                int qrlen = 0;
                int crlen = 0;
                int npos = pos;
                for (int i = 0; i < fpNum; i++) {
                    rlen = bytes2Int(template, 0);
                    npos += rlen;
                    pos += 18;
                    qrlen = template[pos] * 5 + 1;

                    pos += qrlen;
                    if (blockFlag == 1) {
                        crlen = template[pos] * 3 + 1;
                    }

                    pos += crlen;
                    pos += 12;
                    mc += template[pos];

                    Log.d("MorphoSample", "mc:" + mc + " pos:" + pos);
                    pos = npos;

                }
                return mc / fpNum;
            }
        }
        return 0;
    }

    public static String ByteArrayToHexString(byte[] bytes) {
        final char[] hexArray = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        char[] hexChars = new char[bytes.length * 2]; // Each byte has two hex characters (nibbles)
        int v;
        for (int j = 0; j < bytes.length; j++) {
            v = bytes[j] & 0xFF; // Cast bytes[j] to int, treating as unsigned value
            hexChars[j * 2] = hexArray[v >>> 4]; // Select hex character from upper nibble
            hexChars[j * 2 + 1] = hexArray[v & 0x0F]; // Select hex character from lower nibble
        }
        return new String(hexChars);
    }

    public static byte[] HexStringToByteArray(String s) throws IllegalArgumentException {
        int len = s.length();
        if (len % 2 == 1) {
            throw new IllegalArgumentException("Hex string must have even number of characters");
        }
        byte[] data = new byte[len / 2]; // Allocate 1 byte per 2 hex characters
        for (int i = 0; i < len; i += 2) {
            // Convert each character into a integer (base-16), then bit-shift into place
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }
}
