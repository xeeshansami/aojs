package com.hbl.bot.utils;

import android.app.Dialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.broooapps.otpedittext2.OnCompleteListener;
import com.broooapps.otpedittext2.OtpEditText;
import com.hbl.bot.R;

public class TwoButtonsDialogNew extends Dialog implements
        View.OnClickListener, TextWatcher {

    public Context c;
    public Dialog d;
    public Button yes, no;
    TextView timer,errorTv;
    public String otp;

    private View view;

    DialogCallback callback;
    public OtpEditText otpText;
    public String error;


    public TwoButtonsDialogNew(Context a,String otp, DialogCallback callback) {
        super(a);
        this.otp = otp;
        this.c = a;
        this.callback = callback;


    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        setContentView(R.layout.two_buttons_dialog_new);
        errorTv = findViewById(R.id.error);
        yes = findViewById(R.id.bt_confirm);
        no = findViewById(R.id.bt_cancel);
        timer = findViewById(R.id.timer);
        otpText = findViewById(R.id.otpText);
        startTimer();
        otpText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (count == 8) {
                    otpText.setAlpha(0.5f);
                    InputMethodManager inputMethodManager = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(otpText.getWindowToken(), 0);
                } else {
                    otpText.setAlpha(1f);
                    InputMethodManager inputMethodManager = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.showSoftInputFromInputMethod(otpText.getWindowToken(), 0);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        otpText.setOnCompleteListener(new OnCompleteListener() {
            @Override
            public void onComplete(String value) {
                if (value.length() == 8) {
                    GlobalClass.otp = value;
                }
            }
        });
        otpText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE || actionId == EditorInfo.IME_ACTION_SEARCH) {
                    // do something..
                    closeKeyborad();
                }
                return true;
            }
        });
        otpText.setText(this.otp.trim());
        errorTv.setText(error);
        yes.setOnClickListener(this);
        no.setOnClickListener(this);
    }

    private void closeKeyborad() {
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) c.getSystemService(Context.INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startTimer() {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // Do something after 5s = 5000ms
//                        Intent i = new Intent(OfflineActivity.this,LoginActivity.class);
//                        startActivity(i);
//                LocalTime localTime = LocalTime.parse(GlobalClass.resendTime);

                long millis = 0;
                try {
//                    LocalTime localTime = null;
//                    localTime = LocalTime.parse("00:00:59");
//
//
//                 millis = localTime.toSecondOfDay() * 1000;
                    String source = GlobalClass.resendTime;
                    String[] tokens = source.split(":");
                    int secondsToMs = Integer.parseInt(tokens[2]) * 1000;
                    int minutesToMs = Integer.parseInt(tokens[1]) * 60000;
                    int hoursToMs = Integer.parseInt(tokens[0]) * 3600000;
                    millis = secondsToMs + minutesToMs + hoursToMs;
                } catch (Exception e) {
                    Log.d("a", e.toString());
                }
                new CountDownTimer(30000, 1000) {
                    public void onTick(long millisUntilFinished) {
                        timer.setText("00:" + String.format("%02d", (millisUntilFinished / 1000)));
                    }

                    public void onFinish() {
//                        timer.setText("FINISH!!");
                        timer.setText("00:00");
                        no.setEnabled(true);
                        no.setBackgroundResource(R.drawable.submit_otp_btn_grenn);
                    }
                }.start();
            }
        }, 1000);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.bt_confirm:
                this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                GlobalClass.otp = otpText.getOtpValue();
                callback.onPositiveClicked();
                break;
            case R.id.bt_cancel:
                callback.onNegativeClicked();
                no.setEnabled(false);
                no.setBackgroundResource(R.drawable.resend_otp_btn);
                startTimer();
                break;
            default:
                break;
        }
//        this.dismiss();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {


    }

    @Override
    public void afterTextChanged(Editable s) {

    }
}