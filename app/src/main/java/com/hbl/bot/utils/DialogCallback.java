package com.hbl.bot.utils;


public interface DialogCallback {
    void onNegativeClicked();

    void onPositiveClicked();

}
