package com.hbl.bot.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import com.hbl.bot.R
import com.hbl.bot.ui.customviews.DialogCallback

class Utils(context: Context?) {

    lateinit var listener2: setOnitemClickListner
    lateinit var listener3: setOnitemClickListner
    var context: Context = context!!

    companion object {
        var alertDialogBuilder: AlertDialog.Builder? = null
        lateinit var listener: setOnitemClickListner
        var alertDialogBuilderMV: androidx.appcompat.app.AlertDialog.Builder? = null
        var dialog: AlertDialog? = null
        var dialogMV: androidx.appcompat.app.AlertDialog? = null
        fun failedAwokeCalls(context: Context, btn1ClickListner: setOnitemClickListner) {
            listener = btn1ClickListner
            alertDialogBuilder = AlertDialog.Builder(context)
            val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val view = inflater.inflate(R.layout.popup_failed_call_awoke, null)
            alertDialogBuilder!!.setView(view)
            alertDialogBuilder!!.setCancelable(false)
            dialog = alertDialogBuilder!!.create()
            var retry_action = view.findViewById<Button>(R.id.retry_action)
            retry_action.setOnClickListener(View.OnClickListener { view1: View? ->
                listener.onClick(view1!!)
                dialog!!.dismiss()
                dialogMV!!.dismiss()

            })
            dialog!!.show()
        }
        fun ML(tag: String, value: String) {
            Log.i(tag, value)
        }

        fun getDateSplited(date: String, value: Int): String? {
            try {
                val items = date.split(" ".toRegex()).toTypedArray()
                println("splitDate" + items[0] + " " + items[1])
                return if (value == 1) {
                    items[1]
                } else {
                    items[0]
                }
            } catch (ex: Exception) {
                val items = date.split("T".toRegex()).toTypedArray()
                println("splitDate" + items[0] + " " + items[1])
                return if (value == 1) {
                    items[1]
                } else {
                    items[0]
                }
            }
        }

        public fun alertBox(
            context: Context,
            title: String,
            message: String,
            posBtnTitle: String,
            negBtnTitle: String,
        ) {
            (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(context,
                title,
                message,
                posBtnTitle,
                negBtnTitle,
                object : DialogCallback {
                    override fun onNegativeClicked() {
                        super.onNegativeClicked()
                    }

                    override fun onPositiveClicked() {
                        super.onPositiveClicked()
                    }
                })
        }

        fun makeCall(to: String, context: Context) {
            val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + to))
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
            context.startActivity(intent)
        }

        fun openMap(query: String, context: Context) {

            var encodedQuery = Uri.encode(query)
            val uriString: String = "geo:0,0?q=$encodedQuery"
            val uri = Uri.parse(uriString)
            val intent = Intent(Intent.ACTION_VIEW, uri)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK;
            context.startActivity(intent)
        }

        fun sendEmail(to: String, context: Context) {
            val emailIntent = Intent(
                Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto", to, null
                )
            )
            emailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK;

            context.startActivity(Intent.createChooser(emailIntent, "Send email..."))
        }

        @JvmStatic
        fun myLogs(
            context: Context,
            message: String?,
            isPrintLog: Boolean,
        ) {
            Log.i(context.javaClass.simpleName, message.toString())
        }

        @JvmStatic
        fun myLogs(context: Context, message: String) {
            errorBox(context, message)
        }

        @JvmStatic
        fun errorBox(context: Context, message: String) {
            val alertDialog =
                AlertDialog.Builder(context)
            alertDialog.setTitle("SOMETHING WENT WRONG")
            alertDialog.setMessage(context.javaClass.simpleName + " class error \n" + message)
            alertDialog.setCancelable(false)
            alertDialog.setPositiveButton(
                "OK"
            ) { dialog, which -> (context as Activity).finish() }
            alertDialog.show()
        }
    }






    fun makeCall(to: String, context: Context) {
        val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + to))
        context.startActivity(intent)
    }




}
