package com.hbl.bot.ui.customviews

import android.content.Context
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.util.AttributeSet
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import com.hbl.bot.R
import com.hbl.bot.utils.GlobalClass
import kotlinx.android.synthetic.main.view_image_slider.view.*

class ImageSliderView(
    context: Context,
    attrs: AttributeSet

) : ConstraintLayout(context, attrs), View.OnClickListener {
    var currentPageIndex = -1
    var pagesBitmap = mutableListOf<Bitmap>()
    lateinit var pageChangeListener: PageChangeListener
    lateinit var iNextButton: iNextButton

    init {
        inflate(context, R.layout.view_image_slider, this)
        ivNextPage.setOnClickListener(this)
        ivPreviousPage.setOnClickListener(this)
//        val attributes = context.obtainStyledAttributes(attrs, R.styleable.FormSectionHeader)
//        attributes.recycle()

    }

    /*    fun getPageBitmap():Bitmap{
            return ivPDFPage.drawable.toBitmap()
        }*/
    fun getPageBitmap(): Bitmap {
        return ivPDFPage.drawingCache
    }

    fun setPageBitmap(image: Bitmap?) {
//        ivbgImage.setImageBitmap(image)
//        ivPDFPage.destroyDrawingCache()

        val drawable = BitmapDrawable(image)
        ivPDFPage.background = drawable
    }


    fun displayPDFDocument() {
        if (pagesBitmap.size > 0) currentPageIndex = 0
        showPage()
    }

    fun showPage() {
//        object : Thread() {
//            override fun run() {
//               context.runOnUiThread(Runnable {
        pdfProfressBarMsgID.visibility = View.GONE
        progressBarID.visibility = View.GONE
        setPageBitmap(pagesBitmap[currentPageIndex])
        tvPageNumber.text = "Page: " + (currentPageIndex + 1).toString() + "/" + pagesBitmap.size
        pageChangeListener.onNextPage(currentPageIndex)

//                })
//            }
//        }.start()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivNextPage -> {
                nextPageConfimationAlert("ALERT!","-Are you sure you want to Save this page and proceed to the next page?","YES","NO")
            }
            R.id.ivPreviousPage -> {
                if (currentPageIndex < pagesBitmap.size + 1 && currentPageIndex > 0) {
                    currentPageIndex--
                    showPage()
                }
            }
        }
    }
    private fun nextPageConfimationAlert(
        title: String,
        msg: String,
        button1: String,
        button2: String

    ) {
        (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(
            context,
            title,
            msg,
            button1,
            button2,
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
                }

                override fun onPositiveClicked() {
                    super.onPositiveClicked()
                    pageChangeListener.hitNextPage()
                    if (currentPageIndex < pagesBitmap.size - 1) {
                        iNextButton.fNextButton()
                        currentPageIndex++
                        showPage()
                    }
                }
            })
    }

}



interface PageChangeListener {
    fun onNextPage(index: Int)
    fun hitNextPage()
}
interface iNextButton {
    fun fNextButton()
}
