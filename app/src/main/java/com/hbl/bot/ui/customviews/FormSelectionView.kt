package com.hbl.bot.ui.customviews

import android.app.Dialog
import android.content.Context
import android.text.Editable
import android.text.TextWatcher
import android.util.AttributeSet
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.*
import android.widget.AdapterView.OnItemClickListener
import androidx.constraintlayout.widget.ConstraintLayout
import com.hbl.bot.R
import com.hbl.bot.network.models.response.base.PreferencesMailing
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.ui.fragments.spark.cif.CIFStep1_4_Morpho_fingerprint_6_34
import com.hbl.bot.utils.GlobalClass
import kotlinx.android.synthetic.main.view_form_selection.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class FormSelectionView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    var countryObj = Country()
    var sOIObj = SourceOfIncomeLandLord()
    var relationshipObj = Relationship()
    var cityObj = City()
    var chequeBookLeaves = ChequeBookLeaves()
    var modeOfDelivery = MODelivery()
    var addressTypeObj = AddressType()
    var sourceOfFundObj = SourceOffund()
    var purposeOfTinObj = PurposeOfTin()
    var sourceOfWealthObj = SourceOfWealth()
    var fragment =
        CIFStep1_4_Morpho_fingerprint_6_34()
    val dialog = Dialog(context, R.style.CustomDialog)
    private var showRemove: Boolean = true

    init {
        var view = inflate(context, R.layout.view_form_selection, this)
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.FormSelectionView)
        tv_title.text = attributes.getString(R.styleable.FormSelectionView_titleText)
        tvTitleUrdu.text = attributes.getString(R.styleable.FormSelectionView_titleTextUrdu)
        mandateSpinner.text = attributes.getString(R.styleable.FormSelectionView_mandatory)
        showRemove = attributes.getBoolean(R.styleable.FormSelectionView_showRemoveIcon, true)
        attributes.recycle()

        resources!!.getString(R.string.select).let {
            modeOfDelivery.MODE = "0"
            modeOfDelivery.MODE_DESC = it
        }
        resources!!.getString(R.string.select).let {
            chequeBookLeaves.CHEQUEBOOK_LEAVES = "0"
            chequeBookLeaves.CHEQUEBOOK_LEAVES_DESC = it
        }
        resources!!.getString(R.string.select).let {
            countryObj.COUNTRY_CODE = "0"
            countryObj.COUNTRY_NAME = it
        }
        resources!!.getString(R.string.select).let {
            relationshipObj.RELATION_CODE = "0"
            relationshipObj.RELATION_DESC = it
        }
        resources!!.getString(R.string.select).let {
            cityObj.CITY_CODE = "0"
            cityObj.CITY_NAME = it
        }
        resources!!.getString(R.string.select).let {
            addressTypeObj.ADDR_CODE = "0"
            addressTypeObj.ADDR_DESC = it
        }
        resources!!.getString(R.string.select).let {
            sourceOfFundObj.SOF_CODE = "0"
            sourceOfFundObj.SOF_DESC = it
        }
        resources!!.getString(R.string.select).let {
            sOIObj.SOI_CODE = "0"
            sOIObj.SOI_DESC = it
        }
        resources!!.getString(R.string.select).let {
            purposeOfTinObj.PURP_CODE = "0"
            purposeOfTinObj.PURP_DESC = it
        }
        resources!!.getString(R.string.select).let {
            sourceOfWealthObj.SOW_CODE = "0"
            sourceOfWealthObj.SOW_DESC = it
        }
    }


    fun setItemForDocs(item: ArrayList<DocsData>) {
        var obj = DocsData()
        resources!!.getString(R.string.select).let {
            obj.iDDOCCODE = "0"
            obj.iDDOCDESC = it
        }
        /*      val values = ArrayList<DocsData>()
              val hashSet = HashSet<DocsData>()
              hashSet.addAll(item)
              item.clear()
              values.addAll(hashSet)*/
        if (item.size != 0 && !item[0].iDDOCDESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
//        remainSelection(item.size)
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
//        retrieveAllItems(spinner)
    }

    fun setItemForMediumOfCharge(item: ArrayList<String>) {
        item.add(0, resources!!.getString(R.string.select))
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }
    fun setItemForBeneficialOwner(item: ArrayList<String>) {
        item.add(0, resources!!.getString(R.string.select))
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }
    fun setKonnectIsPepData(item: ArrayList<String>) {
        if (item.size != 0 && !item[0].equals(resources!!.getString(R.string.select))) {
            item.add(0, resources!!.getString(R.string.select))
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }
    fun setKonnectFatcaStatus(item: ArrayList<String>) {
        if (item.size != 0 && !item[0].equals(resources!!.getString(R.string.select))) {
            item.add(0, resources!!.getString(R.string.select))
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }
    fun setItemForHawCompanyData(item: ArrayList<HawCompanyData>) {
        var obj = HawCompanyData()
        resources!!.getString(R.string.select).let {
            obj.OrgCode = ""
            obj.NameOfCompanyOrEmployer = it
        }
        /*      val values = ArrayList<DocsData>()
              val hashSet = HashSet<DocsData>()
              hashSet.addAll(item)
              item.clear()
              values.addAll(hashSet)*/
        if (item.size != 0 && !item[0].NameOfCompanyOrEmployer.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
//        remainSelection(item.size)
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
//        retrieveAllItems(spinner)
    }

    fun setItemForBranchRegionData(item: ArrayList<BranchRegionCode>) {
        var obj = BranchRegionCode()
        resources!!.getString(R.string.select).let {
            obj.Branch_Code = "0"
            obj.Branch_Name = it
        }
        /*      val values = ArrayList<DocsData>()
              val hashSet = HashSet<DocsData>()
              hashSet.addAll(item)
              item.clear()
              values.addAll(hashSet)*/
        if (item.size != 0 && !item[0].Branch_Name.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
//        remainSelection(item.size)
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
//        retrieveAllItems(spinner)
    }

    fun setItemForModeOfTrans(item: ArrayList<ModeOfTans>) {
        var obj = ModeOfTans()
        resources!!.getString(R.string.select).let {
            obj.TRAN_CODE = "0"
            obj.TRAN_DESC = it
        }
        if (item.size != 0 && !item[0].TRAN_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun getSpinnerValue(): Int {
        return spinner.selectedItemPosition
    }

    fun setItemForCustomerSegment(item: ArrayList<CustomerSegment>): ArrayList<CustomerSegment> {
        var obj = CustomerSegment()
        resources!!.getString(R.string.select).let {
            obj.cUSTSEGMENT = "0"
            obj.sEGMENTDESC = it
        }
        /*  val values = ArrayList<CustomerSegment>()
          val hashSet = HashSet<CustomerSegment>()
          hashSet.addAll(item)
          values.clear()
          values.addAll(hashSet)*/
        if (item.size != 0 && !item.get(0).sEGMENTDESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
        return item
    }

    fun setItemForNatureOfBusiness(item: ArrayList<NatureOfBusiness>) {
        var obj = NatureOfBusiness()
        resources!!.getString(R.string.select).let {
            obj.BUS_CODE = "0"
            obj.BUS_DESC = it
        }
        if (item.size != 0 && !item[0].BUS_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForProfessions(item: ArrayList<Profession>) {
        var obj = Profession()
        resources!!.getString(R.string.select).let {
            obj.PROF_CODE = "0"
            obj.PROF_DESC = it
        }
/*        val values = ArrayList<Profession>()
        val hashSet = HashSet<Profession>()
        hashSet.addAll(item)
        values.clear()
        values.addAll(hashSet)*/
        if (item.size != 0 && !item[0].PROF_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForSourceOfIncome(item: ArrayList<SourceOffund>) {
        var obj = SourceOffund()
        if (item.size != 0) {
            if (item[0].SOF_CODE != "0") {
                item.add(0, sourceOfFundObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForCountries(item: ArrayList<Country>) {
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCountry(item: ArrayList<Country>) {
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPepReasonCountry(item: ArrayList<Country>) {
        spinner.id = R.id.normalCountry
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForSourceOfIncomeLandLord(item: ArrayList<SourceOfIncomeLandLord>) {
        spinner.id = R.id.fivLOBII1
        if (item.size != 0 && !item[0].SOI_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, sOIObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivNOCountry(item: ArrayList<Country>) {
        spinner.id = R.id.NOCountry
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCOOC(item: ArrayList<Country>) {
        spinner.id = R.id.COOC
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun getSpinner(id: Int): Spinner {
        spinner.id = id
        return spinner;
    }

    fun getSpinner(): Spinner {
        return spinner
    }

    fun getLayout(id: Int): ConstraintLayout {
        spinnerLayout.id = id
        return spinnerLayout;
    }

    fun getLayout(): ConstraintLayout {
        return spinnerLayout;
    }

    fun setItemForfivSecondNationality(item: ArrayList<Country>) {
        spinner.id = R.id.cif1SecondNationality
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
        /* spinner.setOnTouchListener { v, event ->
             showDialog(item)
             false
         }*/
    }

    fun setItemForfivNationality(item: ArrayList<Country>): Spinner {
        spinner.id = R.id.cif1Nationality
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        spinner.setSelection(1)
//        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
        return spinner
    }

    fun setItemForfivCOR(item: ArrayList<Country>) {
        spinner.id = R.id.COR
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfsvCountryOfBirth(item: ArrayList<Country>) {
        spinner.id = R.id.CountryOfBirth
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCountryInward1(item: ArrayList<Country>) {
        spinner.id = R.id.CountryInward1
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCountryInward2(item: ArrayList<Country>) {
        spinner.id = R.id.CountryInward2
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCountryInward3(item: ArrayList<Country>) {
        spinner.id = R.id.CountryInward3
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCountryOutward1(item: ArrayList<Country>) {
        spinner.id = R.id.CountryOutward1
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCountryOutward2(item: ArrayList<Country>) {
        spinner.id = R.id.CountryOutward2
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCountryOutward3(item: ArrayList<Country>) {
        spinner.id = R.id.CountryOutward3
        if (item.size != 0) {
            if (item[0].COUNTRY_CODE != "0") {
                item.add(0, countryObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForAccountOperationsInst(item: ArrayList<AccountOperationsInst>) {
        var obj = AccountOperationsInst()
        resources!!.getString(R.string.select).let {
            obj.OPER_CODE = "0"
            obj.OPER_DESC = it
        }
        if (item.size != 0 && !item[0].OPER_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForAccountOperationsType(item: ArrayList<AccountOperationsType>) {
        var obj = AccountOperationsType()
        resources!!.getString(R.string.select).let {
            obj.ACCT_OPER_TYPE_CODE = "0"
            obj.ACCT_OPER_TYPE_DESC = it
        }
        if (item.size != 0 && !item[0].ACCT_OPER_TYPE_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForEstateFrequency(item: ArrayList<EstateFrequency>) {
        var obj = EstateFrequency()
        resources!!.getString(R.string.select).let {
            obj.E_STATE_FRQ_CODE = "0"
            obj.E_STATE_FRQ = it
        }
        if (item.size != 0 && !item[0].E_STATE_FRQ.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForCurrency(item: ArrayList<Currency>) {
        var obj = Currency()
        resources!!.getString(R.string.select).let {
            obj.CURR_CODE = "0"
            obj.CURR = "Select"
            obj.CURR_DESC = it
        }
        if (item.size != 0 && !item[0].CURR_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPriority(item: ArrayList<Priority>) {
        var obj = Priority()
        resources!!.getString(R.string.select).let {
            obj.PRITY_CODE = "0"
            obj.PRITY_DESC = it
        }
        if (item.size != 0 && !item[0].PRITY_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForDepositType(item: ArrayList<DepositType>) {
        var obj = DepositType()
        resources!!.getString(R.string.select).let {
            obj.DEP_CODE = "0"
            obj.DEP_DESC = it
        }
        if (item.size != 0 && !item[0].DEP_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPurposeOfTin1(item: ArrayList<PurposeOfTin>) {
        spinner.id = R.id.fivReasonTin1
        if (item.size != 0) {
            if (item[0].PURP_CODE != "0") {
                item.add(0, purposeOfTinObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPurposeOfTin2(item: ArrayList<PurposeOfTin>) {
        spinner.id = R.id.fivReasonTin2
        if (item.size != 0) {
            if (item[0].PURP_CODE != "0") {
                item.add(0, purposeOfTinObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPurposeOfTin3(item: ArrayList<PurposeOfTin>) {
        spinner.id = R.id.fivReasonTin3
        if (item.size != 0) {
            if (item[0].PURP_CODE != "0") {
                item.add(0, purposeOfTinObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForCapacity(item: ArrayList<Capacity>) {
        var obj = Capacity()
        resources!!.getString(R.string.select).let {
            obj.CAPACITY_CODE = "0"
            obj.CAPACITY_DESC = it
        }
        if (item.size != 0 && !item[0].CAPACITY_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPurpOfCif(item: ArrayList<PurposeCIF>) {
        var obj = PurposeCIF()
        resources!!.getString(R.string.select).let {
            obj.PURP_CODE = "0"
            obj.PURP_DESC = it
        }
        if (item.size != 0 && !item[0].PURP_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPreferencesMailing(item: ArrayList<PreferencesMailing>) {
        var obj = PreferencesMailing()
        resources!!.getString(R.string.select).let {
            obj.PREFERRED_CODE = "0"
            obj.PREFERRED_ADDR = it
        }
        if (item.size != 0 && !item[0].PREFERRED_ADDR.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForBools(item: ArrayList<Bool>) {
        spinner.id = R.id.Bools
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForBoolRelationShip(item: ArrayList<Bool>) {
        spinner.id = R.id.relationshipBool
        /*       var obj = Bool()
               resources!!.getString(R.string.select).let {
                   obj.LB_CODE = "0"
                   obj.LB_NAME = it
               }
               item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivZakat(item: ArrayList<Bool>) {
        spinner.id = R.id.Zakat
        /*    var obj = Bool()
            resources!!.getString(R.string.select).let {
                obj.LB_CODE = "0"
                obj.LB_NAME = it
            }
            item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivNetBank(item: ArrayList<Bool>) {
        spinner.id = R.id.NetBank
        /*      var obj = Bool()
              resources!!.getString(R.string.select).let {
                  obj.LB_CODE = "0"
                  obj.LB_NAME = it
              }
              item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivPhoneBank(item: ArrayList<Bool>) {
        spinner.id = R.id.PhoneBank
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivSMS(item: ArrayList<Bool>) {
        spinner.id = R.id.SMS
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivBankInfo(item: ArrayList<Bool>) {
        spinner.id = R.id.BankInfo
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivOutwardRemittance(item: ArrayList<Bool>) {
        spinner.id = R.id.OutwardRemittance
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivInwardRemittance(item: ArrayList<Bool>) {
        spinner.id = R.id.InwardRemittance
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivIrisFormW8(item: ArrayList<Bool>) {
        spinner.id = R.id.IrisFormW8
        /*      var obj = Bool()
              resources!!.getString(R.string.select).let {
                  obj.LB_CODE = "0"
                  obj.LB_NAME = it
              }
              item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivIrisFormW9(item: ArrayList<Bool>) {
        spinner.id = R.id.IrisFormW9
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivDomesticPep(item: ArrayList<Bool>) {
        spinner.id = R.id.DomesticPep
        /*      var obj = Bool()
              resources!!.getString(R.string.select).let {
                  obj.LB_CODE = "0"
                  obj.LB_NAME = it
              }
              item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivForeignPep(item: ArrayList<Bool>) {
        spinner.id = R.id.ForeignPep
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivTaxResidencey(item: ArrayList<Bool>) {
        spinner.id = R.id.TaxResidencey
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivCitizenship(item: ArrayList<Bool>) {
        spinner.id = R.id.Citizenship
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivSpendDays(item: ArrayList<Bool>) {
        spinner.id = R.id.SpendDays
        /* var obj = Bool()
         resources!!.getString(R.string.select).let {
             obj.LB_CODE = "0"
             obj.LB_NAME = it
         }
         item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivHBLNisa(item: ArrayList<Bool>) {
        spinner.id = R.id.HBLNisa
        /*    var obj = Bool()
            resources!!.getString(R.string.select).let {
                obj.LB_CODE = "0"
                obj.LB_NAME = it
            }
            item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
//        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivfivCardRequird(item: ArrayList<Bool>) {
        spinner.id = R.id.fivCardRequird
        /*   var obj = Bool()
           resources!!.getString(R.string.select).let {
               obj.LB_CODE = "0"
               obj.LB_NAME = it
           }
           item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivStatment(item: ArrayList<Bool>) {
        spinner.id = R.id.Statment
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivInsurance(item: ArrayList<Bool>) {
        spinner.id = R.id.Insurance
/*        var obj = Bool()
        resources!!.getString(R.string.select).let {
            obj.LB_CODE = "0"
            obj.LB_NAME = it
        }
        item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivExistingRelation(item: ArrayList<Bool>) {
        spinner.id = R.id.fivExistingRelation1
        /* var obj = Bool()
         resources!!.getString(R.string.select).let {
             obj.LB_CODE = "0"
             obj.LB_NAME = it
         }
         item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPartyMandate(item: ArrayList<Bool>) {
        spinner.id = R.id.PartyMandate
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivReason(item: ArrayList<Bool>) {
        spinner.id = R.id.Reason
        /* var obj = Bool()
         resources!!.getString(R.string.select).let {
             obj.LB_CODE = "0"
             obj.LB_NAME = it
         }
         item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForCardProducts(item: ArrayList<CardProduct>) {
        var obj = CardProduct()
        resources!!.getString(R.string.select).let {
            obj.PRD_CODE = "0"
            obj.PRD_NAME = it
        }
        if (item.size != 0 && !item[0].PRD_NAME.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPrefComMode(item: ArrayList<PrefComMode>) {
        var obj = PrefComMode()
        resources!!.getString(R.string.select).let {
            obj.COMMU_CODE = "0"
            obj.COMMU_DESC = it
        }
        if (item.size != 0 && !item[0].COMMU_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForIndustryMainCategory(item: ArrayList<IndustryMainCategory>) {
        var obj = IndustryMainCategory()
        resources!!.getString(R.string.select).let {
            obj.SOIIC_CODE = "0"
            obj.SOIIC_DESC = it
        }
        if (item.size != 0 && !item[0].SOIIC_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForIndustrySubCategory(item: ArrayList<IndustrySubCategory>) {
        var obj = IndustrySubCategory()
        resources!!.getString(R.string.select).let {
            obj.SOIISC_CODE = "0"
            obj.SOIISC_DESC = it
        }
        if (item.size != 0 && !item[0].SOIISC_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForFingers() {
        var adapter = ArrayAdapter(
            context,
            R.layout.view_spinner_item,
            R.id.text1,
            resources!!.getStringArray(R.array.fingers)
        )
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
//        remainSelection(item.size)
        invalidate();
        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {

            }
        }
    }

    fun setItemForCities(item: ArrayList<City>) {
        if (item.size != 0) {
            if (item[0].CITY_CODE != "0") {
                item.add(0, cityObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForCity(item: ArrayList<City>) {
        spinner.id = R.id.city
        resources!!.getString(R.string.select).let {
            cityObj.CITY_CODE = "0"
            cityObj.CITY_NAME = it
        }
        if (item.size != 0) {
            if (item[0].CITY_CODE != "0") {
                item.add(0, cityObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForAddressType(item: ArrayList<AddressType>) {
        if (item.size != 0) {
            if (item[0].ADDR_CODE != "0") {
                item.add(0, addressTypeObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForAddType(item: ArrayList<AddressType>) {
        spinner.id = R.id.addType
        if (item.size != 0) {
            if (item[0].ADDR_CODE != "0") {
                item.add(0, addressTypeObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForChequeBookLeaves(item: ArrayList<ChequeBookLeaves>) {
        spinner.id = R.id.fivNoOfCheque
        if (item.size != 0) {
            if (item[0].CHEQUEBOOK_LEAVES != "0") {
                item.add(0, chequeBookLeaves)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForModeOfDelivery(item: ArrayList<MODelivery>) {
        spinner.id = R.id.fivModeOfDelivery
        if (item.size != 0) {
            if (item[0].MODE != "0") {
                item.add(0, modeOfDelivery)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForBioAccountType(item: ArrayList<BioAccountType>) {
        var obj = BioAccountType()
        resources!!.getString(R.string.select).let {
            obj.BAT_CODE = "0"
            obj.BAT_NAME = it
        }
        if (item.size != 0 && !item[0].BAT_NAME.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForAccountType(item: ArrayList<AccountType>) {
        var obj = AccountType()
        resources!!.getString(R.string.select).let {
            obj.ACCT_TYPE = "0"
            obj.ACCT_TYPE_DESC = it
        }
        if (item.size != 0 && !item[0].ACCT_TYPE_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForProvince(item: ArrayList<Province>) {
        var obj = Province()
        resources!!.getString(R.string.select).let {
            obj.PR_CODE = "0"
            obj.PR_NAME = it
        }
        if (item.size != 0 && !item[0].PR_NAME.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPurposeCIF(item: ArrayList<PurposeCIF>) {
        var obj = PurposeCIF()
        resources!!.getString(R.string.select).let {
            obj.PURP_CODE = "0"
            obj.PURP_DESC = it
        }
        if (item.size != 0 && !item[0].PURP_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForTitle(item: ArrayList<Title>) {
        var obj = Title()
        resources!!.getString(R.string.select).let {
            obj.TITLE_CODE = "0"
            obj.TITLE_DESC = it
        }
        if (item.size != 0 && !item[0].TITLE_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
        remainSelection(item.size)
    }

    fun setItemForMarital(item: ArrayList<MaritalStatus>) {
        var obj = MaritalStatus()
        resources!!.getString(R.string.select).let {
            obj.MARITAL_CODE = "0"
            obj.MARITAL_DESC = it
        }
        if (item.size != 0 && !item[0].MARITAL_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPep(item: ArrayList<Pep>) {
        var obj = Pep()
        resources!!.getString(R.string.select).let {
            obj.REASON_CODE = "0"
            obj.REASON_DESC = it
        }
        if (item.size != 0 && !item[0].REASON_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForSourceOffund(item: ArrayList<SourceOffund>) {
        spinner.id = R.id.SourceOffund
        if (item.size != 0) {
            if (item[0].SOF_CODE != "0") {
                item.add(0, sourceOfFundObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivSourceOfIncome(item: ArrayList<SourceOffund>) {
        spinner.id = R.id.SourceOfIncome
        if (item.size != 0) {
            if (item[0].SOF_CODE != "0") {
                item.add(0, sourceOfFundObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForfivSourceFunds(item: ArrayList<SourceOffund>) {
        spinner.id = R.id.OtherSourceOffund
        if (item.size != 0) {
            if (item[0].SOF_CODE != "0") {
                item.add(0, sourceOfFundObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForSourceOfWealth(item: ArrayList<SourceOfWealth>) {
        spinner.id = R.id.SourceOfWealth
        if (item.size != 0) {
            if (item[0].SOW_CODE != "0") {
                item.add(0, sourceOfWealthObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForOtherSourceOfWealth(item: ArrayList<SourceOfWealth>) {
        spinner.id = R.id.otherSourceOfWealth
        if (item.size != 0) {
            if (item[0].SOW_CODE != "0") {
                item.add(0, sourceOfWealthObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForGender(item: ArrayList<Gender>) {
        var obj = Gender()
        resources!!.getString(R.string.select).let {
            obj.GENDER_CODE = "0"
            obj.GENDER_DESC = it
        }
        if (item.size != 0 && !item[0].GENDER_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForSBPCodes(item: ArrayList<SBPCodes>) {
        spinner.id = R.id.SBPCodes
        var obj = SBPCodes()
        resources!!.getString(R.string.select).let {
            obj.SBP_CODE = "0"
            obj.SBP_DESC = it
        }
        if (item.size != 0 && !item[0].SBP_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForRMCodes(item: ArrayList<RMCodes>) {
        spinner.id = R.id.RMCodes
        var obj = RMCodes()
        resources!!.getString(R.string.select).let {
            obj.C2ACO = "0"
            obj.C2RNM = it
        }
        if (item.size != 0 && !item[0].C2RNM.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForPurposeOfAccount(item: ArrayList<PurposeOfAccount>) {
        var obj = PurposeOfAccount()
        resources!!.getString(R.string.select).let {
            obj.PUR_CODE = "0"
            obj.PUR_DESC = it
        }
        if (item.size != 0 && !item[0].PUR_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForKonnectPurposeOfAccount(item: ArrayList<KonnectPurposeOfAccount>) {
        var obj = KonnectPurposeOfAccount()
        resources!!.getString(R.string.select).let {
            obj.PUR_CODE = "0"
            obj.PUR_DESC = it
        }
        if (item.size != 0 && !item[0].PUR_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForNoOfCheaque(item: ArrayList<String>) {
//        var obj = String()
//        resources!!.getString(R.string.select).let {
//            obj.PUR_CODE = "0"
//            obj.PUR_DESC = it
//        }
//        if (item.size != 0 && !item[0].PUR_DESC.equals(resources!!.getString(R.string.select))) {
//            item.add(0, obj)
//        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForModeofAddress(item: ArrayList<String>) {
//        var obj = String()
//        resources!!.getString(R.string.select).let {
//            obj.PUR_CODE = "0"
//            obj.PUR_DESC = it
//        }
//        if (item.size != 0 && !item[0].PUR_DESC.equals(resources!!.getString(R.string.select))) {
//            item.add(0, obj)
//        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForSundry(item: ArrayList<Sundry>) {
        spinner.id = R.id.Sundry
        var obj = Sundry()
        resources!!.getString(R.string.select).let {
            obj.SUNDRY_CODE = ""
            obj.SUNDRY_DESC = it
        }
        if (item.size != 0 && !item[0].SUNDRY_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForConsumerProductBool(item: ArrayList<Bool>) {
        spinner.id = R.id.ConsumerProductBool
        /*   var obj = Bool()
           resources!!.getString(R.string.select).let {
               obj.LB_CODE = "0"
               obj.LB_NAME = it
           }
           item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForConsumerProduct(item: ArrayList<ConsumerProduct>) {
        spinner.id = R.id.ConsumerProduct
        var obj = ConsumerProduct()
        resources!!.getString(R.string.select).let {
            obj.PRDT_CODE = "0"
            obj.PRDT_DESC = it
        }
        if (item.size != 0 && !item[0].PRDT_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForRelationship(item: ArrayList<Relationship>) {
        spinner.id = R.id.fivCountryInward1
        if (item.size != 0) {
            if (item[0].RELATION_CODE != "0") {
                item.add(0, relationshipObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForRelationship2(item: ArrayList<Relationship>) {
        spinner.id = R.id.fivCountryInward2
        if (item.size != 0) {
            if (item[0].RELATION_CODE != "0") {
                item.add(0, relationshipObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForRelationship3(item: ArrayList<Relationship>) {
        spinner.id = R.id.fivCountryInward3
        if (item.size != 0) {
            if (item[0].RELATION_CODE != "0") {
                item.add(0, relationshipObj)
            }
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForReasonPrefEDD(item: ArrayList<ReasonPrefEDD>) {
        var obj = ReasonPrefEDD()
        resources!!.getString(R.string.select).let {
            obj.REASON_CODE = "0"
            obj.REASON_DESC = it
        }
        if (item.size != 0 && !item[0].REASON_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setItemForDocType(item: ArrayList<DocumentType>) {
        var obj = DocumentType()
        resources!!.getString(R.string.select).let {
            obj.SEQ_NO = "0"
            obj.DOCID = it
        }
        if (item.size != 0 && !item[0].DOCID.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    //for autofill Lov
    /*fun remainSelection(size: Int) {
        if (size > 1) {
            spinner.setSelection(1)
        }
    }*/

    //for live
    fun remainSelection(size: Int) {
        if (size == 2) {
            spinner.setSelection(1)
        }
    }

    fun setitemForPepCategories(item: ArrayList<PepCategories>) {
        var obj = PepCategories()
        resources!!.getString(R.string.select).let {
            obj.PEP_CATG_CODE = "0"
            obj.PEP_CATG_DESC = it
        }
        if (item.size != 0 && !item[0].PEP_CATG_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }

    fun setitemForPepSubCategories(item: ArrayList<PepSubCategories>) {
        var obj = PepSubCategories()
        resources!!.getString(R.string.select).let {
            obj.PEP_SUB_CATG_CODE = "0"
            obj.PEP_SUB_CATG_DESC = it
        }
        if (item.size != 0 && !item[0].PEP_SUB_CATG_DESC.equals(resources!!.getString(R.string.select))) {
            item.add(0, obj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        remainSelection(item.size)
        invalidate();
        spinner.setOnTouchListener { v, event ->
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //your code
                showDialog(item)
            }
            return@setOnTouchListener true
        }
    }


    fun retrieveAllItems(theSpinner: Spinner): List<Any>? {
        val adapter: Adapter = theSpinner.adapter
        val n = adapter.count
        val obj: MutableList<Any> = ArrayList<Any>(n)
        for (i in 0 until n) {
            val user: Any = adapter.getItem(i) as Any
            obj.add(user)
        }
        return obj
    }

    var dataAdapter: ArrayAdapter<Any>? = null

    @JvmName("showDialogForCountry1")
    fun showDialog(list: List<Any>?) {
        try {
            dialog.setContentView(R.layout.search_dialog_list)
            val myFilter = dialog.findViewById<View>(R.id.et_dialog_search_list) as EditText
            val dialogButton = dialog.findViewById<View>(R.id.action_cancel) as ImageButton
            if (dialog != null && !dialog.isShowing) {
                //then show your dialog..else not
                dialog.show()
                Log.i("clickss", "show")
            } else {
                dialog.dismiss()
                Log.i("clickss", "dimiss")
            }
            dialogButton.setOnClickListener { dialog.dismiss() }
            val listView = dialog.findViewById<ListView>(R.id.select_dialog_listview)
            GlobalScope.launch(Dispatchers.Main) {
                dataAdapter = ArrayAdapter<Any>(
                    context,
                    R.layout.simple_spinner_item, R.id.text1, list!!
                )
                dataAdapter?.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                listView.adapter = dataAdapter
                dataAdapter?.notifyDataSetChanged()
            }
            // Assign adapter to ListView
            listView.isTextFilterEnabled = true
            myFilter.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable) {}
                override fun beforeTextChanged(
                    s: CharSequence,
                    start: Int,
                    count: Int,
                    after: Int
                ) {
                }

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                    dataAdapter?.filter?.filter(s.toString())
                }
            })
            listView.onItemClickListener =
                OnItemClickListener { parent, view, position, id ->
                    if (myFilter.text.toString().trim().isNullOrEmpty()) {
                        spinner.setSelection(position)
                    } else {
                        var compareValue = dataAdapter?.getItem(position)
                        spinner.setSelection(
                            GlobalClass.spinnerSearchList(
                                list as MutableList<String>?,
                                compareValue.toString()!!
                            )
                        )
                    }
                    dialog.dismiss()
                }

        } catch (ex: IndexOutOfBoundsException) {
            dialog.dismiss()
        } catch (ex: Exception) {
            dialog.dismiss()
        }
    }
}
