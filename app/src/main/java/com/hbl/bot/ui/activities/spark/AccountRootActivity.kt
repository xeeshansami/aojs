package com.hbl.bot.ui.activities.spark

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.navigation.NavigationView
import com.hbl.bot.R
import com.hbl.bot.ui.adapter.DrawerAdapter
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.request.baseRM.*
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.ui.fragments.spark.accounts.CIFStep9
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.RecyclerTouchListener
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.activity_account.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_cifroot.siIndicator

class AccountRootActivity : AppCompatActivity(), DrawerLayout.DrawerListener {
    var TAG: String = this.javaClass.simpleName
    private lateinit var drawerAdapter: DrawerAdapter
    private lateinit var appBarConfiguration: AppBarConfiguration

    @JvmField
    var cnicNumber = ""

    @JvmField
    var riskRating = ""

    @JvmField
    var expiryDate = ""

    @JvmField
    var officeNumberFATCA = 0

    @JvmField
    var residenceNumberFATCA = 0

    @JvmField
    var isCheckedUSDialCode = 0

    @JvmField
    var riskRatingTotal = 0
    val viewModel: SharedCIFViewModel by viewModels()

    @JvmField
    var sbpCodesList: ArrayList<SBPCodes> = ArrayList<SBPCodes>()

    @JvmField
    var natureOfBusinessList: ArrayList<NatureOfBusiness> = ArrayList<NatureOfBusiness>()

    @JvmField
    var rmCodesList: ArrayList<RMCodes> = ArrayList<RMCodes>()

    @JvmField
    var sundryList: ArrayList<Sundry> = ArrayList<Sundry>()

    @JvmField
    var customerSegmentList: ArrayList<CustomerSegment> = ArrayList<CustomerSegment>()

    @JvmField
    var consumerProductList: ArrayList<ConsumerProduct> = ArrayList<ConsumerProduct>()

    @JvmField
    var cardProductList: ArrayList<CardProduct> = ArrayList<CardProduct>()

    @JvmField
    var prefComModeList: ArrayList<PrefComMode> = ArrayList<PrefComMode>()

    @JvmField
    var boolList: ArrayList<Bool> = ArrayList<Bool>()

    @JvmField
    var estateFrequencyList: ArrayList<EstateFrequency> = ArrayList<EstateFrequency>()

    @JvmField
    var modeOfTansList: ArrayList<ModeOfTans> = ArrayList<ModeOfTans>()

    @JvmField
    var customerFIN: CUSTFINX = CUSTFINX()

    @JvmField
    var customerEdd: CUSTEDD = CUSTEDD()

    @JvmField
    var customerCdd: CUSTCDD = CUSTCDD()

    @JvmField
    var customerSOLESelf: CUSTSOLESELF = CUSTSOLESELF()

    @JvmField
    var industryMainCategoryList: ArrayList<IndustryMainCategory> =
        ArrayList<IndustryMainCategory>()

    @JvmField
    var industrySubCategoryList: ArrayList<IndustrySubCategory> = ArrayList<IndustrySubCategory>()

    @JvmField
    var sourceOfFundList: ArrayList<SourceOffund> = ArrayList<SourceOffund>()

    @JvmField
    var sourceOfFundLandLordList: ArrayList<SourceOfIncomeLandLord> =
        ArrayList<SourceOfIncomeLandLord>()

    @JvmField
    var sourceOfWealthList: ArrayList<SourceOfWealth> = ArrayList<SourceOfWealth>()

    @JvmField
    var titleList: ArrayList<Title> = ArrayList<Title>()

    @JvmField
    var capacityList: ArrayList<Capacity> = ArrayList<Capacity>()

    @JvmField
    var specificCountryList: ArrayList<Country> = ArrayList<Country>()

    @JvmField
    var allCountryList: ArrayList<Country> = ArrayList<Country>()
    var countryList: ArrayList<Country> = ArrayList<Country>()

    @JvmField
    var purposeOfTinList: ArrayList<PurposeOfTin> = ArrayList<PurposeOfTin>()

    @JvmField
    var professionList: ArrayList<Profession> = ArrayList<Profession>()

    @JvmField
    var depositTypeList: ArrayList<DepositType> = ArrayList<DepositType>()

    @JvmField
    var puposeOfAccountList: ArrayList<PurposeOfAccount> = ArrayList<PurposeOfAccount>()

    @JvmField
    var currencyList: ArrayList<Currency> = ArrayList<Currency>()

    @JvmField
    var accountTypeList: ArrayList<AccountType> = ArrayList<AccountType>()

    @JvmField
    var accountOperationInstList: ArrayList<AccountOperationsInst> =
        ArrayList<AccountOperationsInst>()

    @JvmField
    var accountOperationTypeList: ArrayList<AccountOperationsType> =
        ArrayList<AccountOperationsType>()

    @JvmField
    var relationshipList: ArrayList<Relationship> = ArrayList<Relationship>()

    @JvmField
    var typeOfAddressList: ArrayList<AddressType> = ArrayList<AddressType>()

    @JvmField
    var cityList: ArrayList<City> = ArrayList<City>()

    @JvmField
    var genderList: ArrayList<Gender> = ArrayList<Gender>()

    @JvmField
    var maritalStatusList: ArrayList<MaritalStatus> = ArrayList<MaritalStatus>()

    @JvmField
    var pepList: ArrayList<PurposeCIF> = ArrayList<PurposeCIF>()

    @JvmField
    var docTypeList: ArrayList<DocsData> = ArrayList<DocsData>()

    @JvmField
    var pepCategoriesList: ArrayList<PepCategories> = ArrayList<PepCategories>()

    @JvmField
    var pepSubCategoriesList: ArrayList<PepSubCategories> = ArrayList<PepSubCategories>()

    @JvmField
    var customerContacts: CUSTCONTACTS = CUSTCONTACTS()

    @JvmField
    var customerPep: CUSTPEP = CUSTPEP()

    @JvmField
    var customerInfo: CUSTINFO = CUSTINFO()

    @JvmField
    var customerAddress: CUSTADDRRESS = CUSTADDRRESS()

    /*    @JvmField
        var customerAddress: CUSTADDRRESS = CUSTADDRRESS()*/
    @JvmField
    var customerDemoGraphicx: CUSTDEMOGRAPHICSX = CUSTDEMOGRAPHICSX()

    @JvmField
    var aofAccountInfoRequest: Data =
        Data()

    @JvmField
    var customerPepList: ArrayList<CUSTPEP> = ArrayList<CUSTPEP>()

    @JvmField
    var customerInfoList: ArrayList<CUSTINFO> = ArrayList<CUSTINFO>()

    @JvmField
    var customerContactList: ArrayList<CUSTCONTACTS> = ArrayList<CUSTCONTACTS>()

    @JvmField
    var customerAddressList: ArrayList<CUSTADDRRESS> = ArrayList<CUSTADDRRESS>()

    @JvmField
    var customerBiometricList: ArrayList<CustomerBiometric> = ArrayList<CustomerBiometric>()

    @JvmField
    var customerBiometric: NadraVerify = NadraVerify()

    @JvmField
    var customerAccountsList: ArrayList<CustomerAccounts> = ArrayList<CustomerAccounts>()

    @JvmField
    var reasonPrefEDDList: ArrayList<ReasonPrefEDD> = ArrayList<ReasonPrefEDD>()

    @JvmField
    var customerAccounts: CUST_ACCOUNTS = CUST_ACCOUNTS()

    @JvmField
    var customerNextOfKin: CUSTNEXTOFKIN = CUSTNEXTOFKIN()

    @JvmField
    var etbntbFLAG = ""

    @JvmField
    var REMEDIATE = ""

    @JvmField
    var fullName = ""

    @JvmField
    var mobileNumber = ""

    @JvmField
    var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()

    @JvmField
    var globalClass: GlobalClass? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
        sharedPreferenceManager.getInstance(this)
        title = ""
        val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
        val navView: NavigationView = findViewById(R.id.nav_view)
        val navController = findNavController(R.id.cifHostFragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.CIFStep1, R.id.CIFStep2, R.id.CIFStep3
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.CIFStep1, R.id.CIFStep1_3,
                R.id.CIFStep1_6, R.id.CIFStep1_7, R.id.CIFStep1_9,
                R.id.CIFStep1_11, R.id.CIFStep1_12, R.id.CIFStep1_13 -> {
                    toolbar.visibility = View.VISIBLE
//                    tvTitle.text = "Customer Basic Information"
                    tvTitle.text = "Customer ID Information"
                }
                R.id.CIFStep1_4, R.id.CIFStep1_5 -> {
                    toolbar.visibility = View.VISIBLE
//                    tvTitle.text = "Customer Basic Information"
                    tvTitle.text = "Biometric Information"
                }
                R.id.CIFStep2, R.id.CIFStep2_3, R.id.CIFStep2_5, R.id.CIFStep2_7 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Customer Contact Details"
                }
                R.id.CIFStep3, R.id.CIFStep3, R.id.CIFStep3_2, R.id.CIFStep3_4, R.id.CIFStep3_5 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "FATCA Information"
                }
                R.id.CIFStep4, R.id.CIFStep4, R.id.CIFStep4_2, R.id.CIFStep4_4, R.id.CIFStep4_5 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Demographic (CDD)"
                }
                R.id.CIFStep5, R.id.CIFStep5, R.id.CIFStep5_3, R.id.CIFStep5_5, R.id.CIFStep5_7 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Customer Due Diligence"
                }
                R.id.CIFStep6, R.id.CIFStep6, R.id.CIFStep6_4, R.id.CIFStep6_5,
                R.id.CIFStep6_7, R.id.CIFStep6_9, R.id.CIFStep6_11 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Customer Enhance Due Diligence"
                }
                R.id.CIFStep7, R.id.CIFStep7, R.id.CIFStep7_2, R.id.CIFStep7_3, R.id.CIFStep7_4 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Accounts"
                }

                R.id.CIFStep10, R.id.CIFStep10 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Next Of KIN"
                }

                R.id.CIFStep10, R.id.CIFStep11 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Next Of KIN"
                }

                R.id.CIFStep10 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Next Of KIN"
                }

                R.id.CIFStep10, R.id.CIFStep12_2 -> {
                    toolbar.visibility = View.VISIBLE
                    siIndicator.visibility = View.VISIBLE
                    tvTitle.text = "Next Of KIN"
                }

                R.id.CIFStep14, R.id.CIFStep14 -> {
                    toolbar.visibility = View.VISIBLE
                    tvTitle.text = "Document Upload"
                }

                R.id.CIFStep9 -> {
                    toolbar.visibility = View.VISIBLE
                    siIndicator.visibility = View.INVISIBLE
                    tvTitle.text = ""
                }

                R.id.CIFStep15 -> {
                    toolbar.visibility = View.VISIBLE
                    siIndicator.visibility = View.INVISIBLE
                    tvTitle.text = ""
                }

                R.id.CIFStep16 -> {
                    toolbar.visibility = View.VISIBLE
                    siIndicator.visibility = View.INVISIBLE
                    tvTitle.text = ""
                }
                R.id.openAccountStep1 -> {
                    toolbar.visibility = View.VISIBLE

                    tvTitle.text = getString(R.string.open_account)
                }
                else -> {
                    toolbar.visibility = View.VISIBLE
                }
            }
        }


        ivAvatar.setOnClickListener {
            // Check for current fragment
            val host: NavHostFragment = supportFragmentManager
                .findFragmentById(R.id.cifHostFragment) as NavHostFragment

            val fragment = host.childFragmentManager.fragments.get(0)
            if (fragment is CIFStep9) {
                navController.popBackStack()

            } else {
                navController.navigate(R.id.CIFStep9)

            }
        }
        viewModel.showAvatar.observe(this, androidx.lifecycle.Observer {
            if (it)
                ivAvatar.visibility = View.VISIBLE
            else ivAvatar.visibility = View.GONE

        })

        viewModel.currentFragmentIndex.observe(this, androidx.lifecycle.Observer {
            siIndicator.currentStep = it
            // it is your current fragment index
        })

        viewModel.totalSteps.observe(this, androidx.lifecycle.Observer {
            siIndicator.stepCount = it
        })
        recyclerViewSetup()
        drawerLayout.setDrawerListener(this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.clear()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
        return true
    }


    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.cifHostFragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.cifHostFragment) as NavHostFragment? ?: return
//            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment? ?: return
        val fragmen = host.childFragmentManager.fragments.get(0)
        fragmen.onActivityResult(requestCode, resultCode, intent)
        host.onActivityResult(requestCode, resultCode, intent)
        viewModel.isPhotoTaken.value = true

    }

    private fun recyclerViewSetup() {
        viewModel.initiateDrawer(this)
        drawerAdapter = DrawerAdapter(viewModel.mList)
        rvDrawer.apply {
            layoutManager = LinearLayoutManager(this@AccountRootActivity)
            adapter = drawerAdapter
        }

        rvDrawer.addOnItemTouchListener(
            RecyclerTouchListener(
                this,
                rvDrawer,
                object :
                    RecyclerTouchListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        changeStartDestination(viewModel.mList[position].title!!)
                    }

                    override fun onLongClick(view: View?, position: Int) {

                    }
                })
        )
    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
    }

    override fun onDrawerOpened(drawerView: View) {
        drawerAdapter.notifyDataSetChanged()

    }


    @SuppressLint("WrongConstant")
    fun changeStartDestination(category: String) {
        drawer_layout.closeDrawer(Gravity.START, true)
        when (category) {
            getString(R.string.customer_id_info) -> switchFragment(R.id.CIFStep1)
            getString(R.string.customer_nationality) -> switchFragment(R.id.CIFStep1_2)
            getString(R.string.purpose_of_CIF) -> switchFragment(R.id.CIFStep1_15)
            getString(R.string.bio_metric_Information) -> switchFragment(R.id.CIFStep1_4)
            getString(R.string.verification_status) -> switchFragment(R.id.CIFStep1_5)
            getString(R.string.customer_id_details) -> switchFragment(R.id.CIFStep1_6)
            getString(R.string.customer_bank_information) -> switchFragment(R.id.CIFStep1_7)
            getString(R.string.customer_personal_information) -> switchFragment(R.id.CIFStep1_9)
            getString(R.string.pep_details) -> switchFragment(R.id.CIFStep1_11)
            getString(R.string.contact_information) -> switchFragment(R.id.CIFStep2)
            getString(R.string.current_address) -> switchFragment(R.id.CIFStep2_3)
            getString(R.string.permanent_address) -> switchFragment(R.id.CIFStep2_5)
            getString(R.string.office_address) -> switchFragment(R.id.CIFStep2_7)
            getString(R.string.fata_for_us_citizens_details) -> switchFragment(R.id.CIFStep3)
            getString(R.string.other_country_tax_payer) -> switchFragment(R.id.CIFStep3_4)
            getString(R.string.customer_demographic) -> switchFragment(R.id.CIFStep4)
            getString(R.string.financial_supporter_details) -> switchFragment(R.id.CIFStep4_5)
            getString(R.string.cdd_details) -> switchFragment(R.id.CIFStep5)
            getString(R.string.foreign_inward_outward_remittance_details) -> switchFragment(R.id.CIFStep5_3)
            getString(R.string.other_bank_information_details) -> switchFragment(R.id.CIFStep5_5)
            getString(R.string.Verification_customer_due_dillgence) -> switchFragment(R.id.CIFStep5_7)
            getString(R.string.edd_details) -> switchFragment(R.id.CIFStep6)
            getString(R.string.edd_additional_source_of_income) -> switchFragment(R.id.CIFStep6_4)
            getString(R.string.edd_financial_supporter_source_of_income) -> switchFragment(R.id.CIFStep6_7)
            getString(R.string.edd_financial_supporter_source_of_income) -> switchFragment(R.id.CIFStep6_9)
            getString(R.string.edd_verification) -> switchFragment(R.id.CIFStep6_11)
            getString(R.string.account) -> switchFragment(R.id.CIFStep7)
            getString(R.string.next_of_kin) -> switchFragment(R.id.CIFStep10)


        }
    }

    private fun switchFragment(startDestId: Int) {
//        val fragmentContainer = view?.findViewById<View>(R.id.nav_host)
//        val navController = Navigation.findNavController(fragmentContainer!!)
        val navController = findNavController(R.id.cifHostFragment)
        val inflater = navController.navInflater
        val graph = navController.graph
        graph.startDestination = startDestId
        navController.graph = graph
    }

    override fun onBackPressed() {
        val navController = findNavController(R.id.cifHostFragment)
        if (navController.currentDestination?.id == R.id.CIFStep7) {
            Log.i(TAG, "Not Up Finish All Fragment")
            finish()
        } else {
            Log.i(TAG, "Up")
            navController.popBackStack()
        }
    }
}
