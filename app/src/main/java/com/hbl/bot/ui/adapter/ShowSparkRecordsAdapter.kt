package com.hbl.bot.ui.adapter

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Rows
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import kotlinx.android.synthetic.main.row_records.view.*

class ShowSparkRecordsAdapter(val mCtx: Context, key: Int, private val mList: ArrayList<Rows>) :
    RecyclerView.Adapter<ShowSparkRecordsAdapter.MyViewHolder>() {
    var key = 0
    val globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
    var bundle = Bundle()

    init {
        this.key = key
    }

    open inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtBCode = view.txtBCode!!
        val txtTrackingID = view.txtTrackingID!!
        val hdTitle = view.hdTitle!!
        val txtCNIC = view.txtCNIC!!
        val itemRecord = view.itemRecord!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_records, parent, false)
        return MyViewHolder(layout)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val list = mList[position]
        holder.txtTrackingID.text = list.TRACKING_ID
        holder.hdTitle.text = list.NAME
        holder.txtCNIC.text = list.ID_DOC_NO
        holder.txtBCode.text = list.USER_BRANCH.toString()

        holder.itemRecord.setOnClickListener(View.OnClickListener {
            callDraftAOF(list.TRACKING_ID!!, list.WORK_FLOW_CODE)
        })
    }

    private fun callDraftAOF(trackingID: String, workFlowCode: String) {
        globalClass.showDialog(mCtx)
        var request = DraftRequest()
        request.find.TRACKING_ID = trackingID
        request.identifier = Constants.DRAFT_INDENTIFIER
        var baseResponse = Gson().toJson(request)
        HBLHRStore.instance?.callDraftData(
            RetrofitEnums.URL_HBL,
            request,
            object : DraftCallBack {
                override fun DraftSuccess(response: AofAccountInfoResponse) {
                    globalClass?.clearSubmitAOF()
                    globalClass?.clearAllLovs()
                    response.let {
                        SparkDataSetupInSp(this@ShowSparkRecordsAdapter.mCtx, it)
                    }
                    callToActivity(workFlowCode)
                }

                override fun DraftFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(mCtx, response.message, 1)
                    globalClass?.hideLoader()
                }
            })
    }

    fun callToActivity(workFlowCode: String) {
        if (workFlowCode == "2") {
            bundle.putInt(Constants.ACTIVITY_KEY, Constants.ACCOUNT_OPENING_SCREEN)
        } else {
            bundle.putInt(Constants.ACTIVITY_KEY, Constants.CIF_1_SHOW_RECORD)
        }
        val intent = Intent(mCtx, CIFRootActivity::class.java)
        intent.putExtras(bundle)
        clearPreLoadedFields()
        globalClass?.hideLoader()
        mCtx.startActivity(intent)
    }

    fun clearPreLoadedFields() {
        GlobalClass.sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.TRACKING_ID_KEY,
            "remove"
        )
        GlobalClass.sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.MYSISREF_KEY,
            "remove"
        )
        GlobalClass.sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.WORKFLOW_KEY,
            "remove"
        )
        GlobalClass.sharedPreferenceManager!!.removeStringInSharedPreferences(
            SharedPreferenceManager.BUSINESSAREA_KEY,
            "remove"
        )
    }
}