package com.hbl.bot.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.hbl.bot.R
import kotlinx.android.synthetic.main.view_form_section_header.view.*

class FormSectionHeader(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    init {
        inflate(context, R.layout.view_form_section_header, this)
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.FormSectionHeader)
        header_tv_title.text = attributes.getString(R.styleable.FormSectionHeader_headerTitle)
        attributes.recycle()
    }

    fun getTextView(): TextView {
        return header_tv_title
    }
}
