package com.hbl.bot.ui.activities

import android.annotation.SuppressLint
import android.app.Activity
import android.app.ActivityManager
import android.content.*
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.graphics.*
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.StrictMode
import android.text.Html
import android.text.format.DateFormat
import android.util.Log
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.*
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.badoualy.stepperindicator.StepperIndicator
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.GoogleApiAvailability
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.material.navigation.NavigationView
import com.hbl.bot.R
import com.hbl.bot.model.AOFBModels
import com.hbl.bot.model.DrawerItem
import com.hbl.bot.model.DrawerModel
import com.hbl.bot.network.ResponseHandlers.callbacks.AppStatusCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.AppDetailsRequest
import com.hbl.bot.network.models.request.baseRM.*
import com.hbl.bot.network.models.response.base.AppStatusResponse
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.PreferencesMailing
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.services.Networkservice
import com.hbl.bot.ui.adapter.DrawerAdapter
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.HawSharedCIFViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.header.view.*
import java.io.*
import java.util.*


open class HawActivity : AppCompatActivity(), DrawerLayout.DrawerListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    var currentAddress = ""
    var TAG2="GeoLocationGenerateACC_CIF"
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocation: Location? = null
    private var locationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    var TAG: String = this.javaClass.simpleName
    private lateinit var drawerAdapter: DrawerAdapter
    private lateinit var appBarConfiguration: AppBarConfiguration
    public var siIndicator: StepperIndicator? = null
    public var ivRefresh: AppCompatImageView? = null
    public var ivHome: AppCompatImageView? = null
    public var ivSSSendEmal: AppCompatImageView? = null
    var pOMStatus: HawPOMStatus? = null

    @JvmField
    var branchRegionTypeList: ArrayList<BranchRegionCode> = ArrayList<BranchRegionCode>()

    @JvmField
    var hawCompanyTypeList: ArrayList<HawCompanyData> = ArrayList<HawCompanyData>()
    var listOfPages = mutableListOf<DrawerModel>()

    init {

    }

    var intentFilter = IntentFilter()

    @JvmField
    var cnicNumber = ""

    @JvmField
    var riskRating = ""

    @JvmField
    var expiryDate = ""

    @JvmField
    var officeNumberFATCA = 0

    @JvmField
    var residenceNumberFATCA = 0

    @JvmField
    var isCheckedUSDialCode = 0

    @JvmField
    var riskRatingTotal = ""
    val viewModel: HawSharedCIFViewModel by viewModels()

    @JvmField
    var sbpCodesList: ArrayList<SBPCodes> = ArrayList<SBPCodes>()

    @JvmField
    var aofbModels = AOFBModels()

    @JvmField
    var natureOfBusinessList: ArrayList<NatureOfBusiness> = ArrayList<NatureOfBusiness>()

    @JvmField
    var rmCodesList: ArrayList<RMCodes> = ArrayList<RMCodes>()

    @JvmField
    var sundryList: ArrayList<Sundry> = ArrayList<Sundry>()

    @JvmField
    var customerSegmentList: ArrayList<CustomerSegment> = ArrayList<CustomerSegment>()

    @JvmField
    var consumerProductList: ArrayList<ConsumerProduct> = ArrayList<ConsumerProduct>()

    @JvmField
    var cardProductList: ArrayList<CardProduct> = ArrayList<CardProduct>()

    @JvmField
    var prefComModeList: ArrayList<PrefComMode> = ArrayList<PrefComMode>()

    @JvmField
    var boolList: ArrayList<Bool> = ArrayList<Bool>()

    @JvmField
    var chequeBookLeavesList: ArrayList<ChequeBookLeaves> = ArrayList<ChequeBookLeaves>()

    @JvmField
    var modeOfDeliveryList: ArrayList<MODelivery> = ArrayList<MODelivery>()

    @JvmField
    var estateFrequencyList: ArrayList<EstateFrequency> = ArrayList<EstateFrequency>()

    @JvmField
    var modeOfTansList: ArrayList<ModeOfTans> = ArrayList<ModeOfTans>()

    @JvmField
    var customerFIN: CUSTFINX = CUSTFINX()

    @JvmField
    var customerEdd: CUSTEDD = CUSTEDD()

    @JvmField
    var customerCdd: CUSTCDD = CUSTCDD()

    @JvmField
    var customerSOLESelf: CUSTSOLESELF = CUSTSOLESELF()

    @JvmField
    var customerStatus: CUSTStatus = CUSTStatus()

    @JvmField
    var industryMainCategoryList: ArrayList<IndustryMainCategory> =
        ArrayList<IndustryMainCategory>()

    @JvmField
    var industrySubCategoryList: ArrayList<IndustrySubCategory> = ArrayList<IndustrySubCategory>()

    @JvmField
    var sourceOfFundList: ArrayList<SourceOffund> = ArrayList<SourceOffund>()

    @JvmField
    var sourceOfFundLandLordList: ArrayList<SourceOfIncomeLandLord> =
        ArrayList<SourceOfIncomeLandLord>()

    @JvmField
    var sourceOfWealthList: ArrayList<SourceOfWealth> = ArrayList<SourceOfWealth>()

    @JvmField
    var titleList: ArrayList<Title> = ArrayList<Title>()

    @JvmField
    var capacityList: ArrayList<Capacity> = ArrayList<Capacity>()

    @JvmField
    var specificCountryList: ArrayList<Country> = ArrayList<Country>()

    @JvmField
    var allCountryList: ArrayList<Country> = ArrayList<Country>()
    var countryList: ArrayList<Country> = ArrayList<Country>()

    @JvmField
    var purposeOfTinList: ArrayList<PurposeOfTin> = ArrayList<PurposeOfTin>()

    @JvmField
    var professionList: ArrayList<Profession> = ArrayList<Profession>()

    @JvmField
    var professionFinancialSupporterList: ArrayList<Profession> = ArrayList<Profession>()

    @JvmField
    var depositTypeList: ArrayList<DepositType> = ArrayList<DepositType>()

    @JvmField
    var puposeOfAccountList: ArrayList<PurposeOfAccount> = ArrayList<PurposeOfAccount>()

    @JvmField
    var currencyList: ArrayList<com.hbl.bot.network.models.response.baseRM.Currency> =
        ArrayList<com.hbl.bot.network.models.response.baseRM.Currency>()

    @JvmField
    var priorityList: ArrayList<Priority> = ArrayList<Priority>()

    @JvmField
    var accountTypeList: ArrayList<AccountType> = ArrayList<AccountType>()

    @JvmField
    var accountOperationInstList: ArrayList<AccountOperationsInst> =
        ArrayList<AccountOperationsInst>()

    @JvmField
    var accountOperationTypeList: ArrayList<AccountOperationsType> =
        ArrayList<AccountOperationsType>()

    @JvmField
    var relationshipList: ArrayList<Relationship> = ArrayList<Relationship>()

    @JvmField
    var typeOfAddressList: ArrayList<AddressType> = ArrayList<AddressType>()

    @JvmField
    var cityList: ArrayList<City> = ArrayList<City>()

    @JvmField
    var genderList: ArrayList<Gender> = ArrayList<Gender>()

    @JvmField
    var maritalStatusList: ArrayList<MaritalStatus> = ArrayList<MaritalStatus>()

    @JvmField
    var pepList: ArrayList<PurposeCIF> = ArrayList<PurposeCIF>()

    @JvmField
    var preferencesList: ArrayList<PreferencesMailing> = ArrayList<PreferencesMailing>()

    @JvmField
    var pepResonseList: ArrayList<Pep> = ArrayList<Pep>()

    @JvmField
    var docTypeList: ArrayList<DocsData> = ArrayList<DocsData>()

    @JvmField
    var pepCategoriesList: ArrayList<PepCategories> = ArrayList<PepCategories>()

    @JvmField
    var pepSubCategoriesList: ArrayList<PepSubCategories> = ArrayList<PepSubCategories>()

    @JvmField
    var customerContacts: CUSTCONTACTS = CUSTCONTACTS()

    @JvmField
    var customerPep: CUSTPEP = CUSTPEP()

    @JvmField
    var customerInfo: CUSTINFO = CUSTINFO()

    @JvmField
    var customerAddress: CUSTADDRRESS = CUSTADDRRESS()

    /*    @JvmField
        var customerAddress: CUSTADDRRESS = CUSTADDRRESS()*/
    @JvmField
    var customerDemoGraphicx: CUSTDEMOGRAPHICSX = CUSTDEMOGRAPHICSX()

    @JvmField
    var aofAccountInfoRequest: Data = Data()

    @JvmField
    var customerGaurdianInfo: GaurdianInfo = GaurdianInfo()

    @JvmField
    var customerPepList: ArrayList<CUSTPEP> = ArrayList<CUSTPEP>()

    @JvmField
    var customerInfoList: ArrayList<CUSTINFO> = ArrayList<CUSTINFO>()

    @JvmField
    var customerContactList: ArrayList<CUSTCONTACTS> = ArrayList<CUSTCONTACTS>()

    @JvmField
    var customerAddressList: ArrayList<CUSTADDRRESS> = ArrayList<CUSTADDRRESS>()

    @JvmField
    var customerBiometricList: ArrayList<CustomerBiometric> = ArrayList<CustomerBiometric>()

    @JvmField
    var customerBiometric: NadraVerify = NadraVerify()

    @JvmField
    var customerAccountsList: ArrayList<CustomerAccounts> = ArrayList<CustomerAccounts>()

    @JvmField
    var reasonPrefEDDList: ArrayList<ReasonPrefEDD> = ArrayList<ReasonPrefEDD>()

    @JvmField
    var customerAccounts: CUST_ACCOUNTS = CUST_ACCOUNTS()


    @JvmField
    var partialDocumets: PDCODSNEW = PDCODSNEW()


    @JvmField
    var customerNextOfKin: CUSTNEXTOFKIN = CUSTNEXTOFKIN()

    @JvmField
    var customerDocument: CUSTDOCUMENT = CUSTDOCUMENT()

    @JvmField
    var docCheckList: ArrayList<DOCCHECKLIST> = ArrayList<DOCCHECKLIST>()

    @JvmField
    var etbntbFLAG = ""

    @JvmField
    var REMEDIATE = ""

    @JvmField
    var fullName = ""

    @JvmField
    var mobileNumber = ""

    @JvmField
    var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()

    @JvmField
    public var globalClass: GlobalClass? = null
    var bundle = Bundle()
    var backInt = 0
    fun hideNavigationBar() {
        try {
            window.decorView.apply {
                // Hide both the navigation bar and the status bar.
                // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
                // a general rule, you should design your app to hide the status bar whenever you
                // hide the navigation bar.
                systemUiVisibility =
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    fun isApplicationSentToBackground(context: Context): Boolean {
        var check = false
        try {
            val am = context.getSystemService(ACTIVITY_SERVICE) as ActivityManager
            val tasks = am.getRunningTasks(1)
            if (!tasks.isEmpty()) {
                val topActivity: ComponentName = tasks[0].topActivity!!
                check = !topActivity.getPackageName().equals(context.packageName)
            }

        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        } finally {
            return check
        }
    }

    companion object {
        private var instance: HawActivity? = null

        fun getInstance(): HawActivity? {
            return instance
        }
    }

     fun isGooglePlayServicesAvailable(activity: Activity?): Boolean {
        val googleApiAvailability = GoogleApiAvailability.getInstance()
        val status = googleApiAvailability.isGooglePlayServicesAvailable(activity)
        if (status != ConnectionResult.SUCCESS) {
            return false
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        try {
            LocalBroadcastManager.getInstance(this).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_EMAIL_SENT
                )
            );
            LocalBroadcastManager.getInstance(this)
                .registerReceiver(broadcastReceiver, intentFilter)
            strickedMode()
            if(isGooglePlayServicesAvailable(this@HawActivity)!!){
                Log.i("isGooglePlayServiceHave","true")
            }else{
                Log.i("isGooglePlayServiceHave","false")
            }
            locationGet()
            globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
            sharedPreferenceManager.getInstance(globalClass)
            Thread.setDefaultUncaughtExceptionHandler(LoggingExceptionHandler(globalClass as GlobalClass));
            setContentView(R.layout.haw_activity_main)
//            hideNavigationBar()
            siIndicator = findViewById(R.id.siIndicator!!)
            ivRefresh = findViewById(R.id.ivRefresh)
            ivHome = findViewById(R.id.ivHome)
            ivSSSendEmal = findViewById(R.id.ivSSSendEmal)
            val toolbar: Toolbar = findViewById(R.id.toolbar)
            setSupportActionBar(toolbar)
            /*toolbar.setNavigationOnClickListener(object : setOnitemClickListner{
                override fun onClick(view: View?) {
                    onBackPressed()
                }
            })*/
            init()
            when {
                intent.extras!!.getInt(Constants.ACTIVITY_KEY) == Constants.CIF_1_4_SCREEN -> {
                    backInt = intent.extras!!.getInt(Constants.ACTIVITY_KEY)
                    switchFragment(R.id.CIFStep1_4)
                }
                intent.extras!!.getInt(Constants.ACTIVITY_KEY) == Constants.ACCOUNT_OPENING_SCREEN -> {
                    backInt = intent.extras!!.getInt(Constants.ACTIVITY_KEY)
                    switchFragment(R.id.openHawAccountStep1)
                }
                intent.extras!!.getInt(Constants.ACTIVITY_KEY) == Constants.CIF_14_SCREEN -> {
                    backInt = intent.extras!!.getInt(Constants.ACTIVITY_KEY)
                    switchFragment(R.id.CIFStep14)
                }
                intent.extras!!.getInt(Constants.ACTIVITY_KEY) == Constants.CIF_10_SCREEN -> {
                    backInt = intent.extras!!.getInt(Constants.ACTIVITY_KEY)
                    switchFragment(R.id.CIFStep10)
                }
                intent.extras!!.getInt(Constants.ACTIVITY_KEY) == Constants.CIF_1_SHOW_RECORD -> {
//                    backInt = intent.extras!!.getInt(Constants.ACTIVITY_KEY)
                    switchFragment(R.id.CIFStep1)
                    start()
                }
                else -> {
                    backInt = intent.extras!!.getInt(Constants.ACTIVITY_KEY)
                    switchFragment(R.id.hawComapnySelect)
                    start()
                }
            }
//
//        if(intent.hasExtra(Constants.DOCUMENT_UPLOAD_SCREEN)) {
//            switchFragment(R.id.HawDocumentUploadFragment45)
//            start()
//        }else{
//            switchFragment(R.id.HawCifAccountSummaryFragment42)
//            start()
//        }

//            switchFragment(R.id.CIFStep1_4)
//            start()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }



    private fun strickedMode() {
        if (Build.VERSION.SDK_INT > 9) {
            val policy: StrictMode.ThreadPolicy =
                StrictMode.ThreadPolicy.Builder().permitAll().build()
            StrictMode.setThreadPolicy(policy)
        }
    }


    override fun onAttachedToWindow() {
        try {
            super.onAttachedToWindow()
            val window: Window = window
            //eliminates color banding
            window.setFormat(PixelFormat.RGBA_8888)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }


    private fun updateAppData() {
        packageManager.getPackageInfo(
            packageName,
            0
        ).versionName.let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.VERSION_NAME,
                it
            )
        }
        packageManager.getPackageInfo(
            packageName,
            0
        ).versionCode.let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.VERSION_CODE,
                it.toString()
            )
        }
        GlobalClass.deviceID().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.TABLET_ID,
                it
            )
        }
        GlobalClass.getFolderSizeLabel(GlobalClass.getFileAPK()).let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.TABLET_APP_SIZE,
                it
            )
        }
        GlobalClass.firstModifiedDate().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.APP_CREATED_DATE,
                it
            )
        }
        GlobalClass.lastModifiedDate().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.APP_UPDATE_DATE,
                it
            )
        }
        sharedPreferenceManager.loginData.getUSERID().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.MYSISID,
                it
            )
        }
    }


    fun getNetworkSpeed() {
        if (Networkservice.isOnline(applicationContext)) {
            startService()
        } else Toast.makeText(
            applicationContext,
            "Please check your network connection",
            Toast.LENGTH_SHORT
        ).show()
    }

    fun startService() {
        intentFilter.addAction(Constants.BROADCAST)
        val serviceIntent = Intent(this, Networkservice::class.java)
        startService(serviceIntent)
    }

    var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (intent.action.equals(Constants.BROADCAST)) {
                if (intent.getStringExtra("online_status").equals("true")) {
                    nextWorkSpeed.setText(intent.getStringExtra("networkSpeed") + "\nkB\\s")
//                    Log.d("data", "true")
                } else {
//                    Toast.makeText(applicationContext, "false", Toast.LENGTH_SHORT).show()
//                    Log.d("data", "false")
                }
            }
        }
    }

    fun start() {
        try {
            updateAppData()
            getNetworkSpeed()
            title = ""
            val drawerLayout: DrawerLayout = findViewById(R.id.drawer_layout)
            val navView: NavigationView = findViewById(R.id.nav_view)
            try {
                val versionName =
                    this.packageManager.getPackageInfo(this.packageName, 0).versionName
                val versionCode =
                    this.packageManager.getPackageInfo(this.packageName, 0).versionCode
                if (!sharedPreferenceManager.trackingID.getBatch()
                        ?.get(0)?.itemDescription.isNullOrEmpty()
                ) {
                    val trackingID =
                        sharedPreferenceManager.trackingID.getBatch()?.get(0)?.itemDescription
                    //TrackingID
                    nav_view!!.getHeaderView(0).trackingID.text =
                        Html.fromHtml("Tracking ID: $trackingID")
                    nav_view!!.getHeaderView(0).trackingID.paintFlags =
                        nav_view!!.getHeaderView(0).trackingID.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                } else if (!sharedPreferenceManager.aofAccountInfo.TRACKING_ID.isNullOrEmpty()) {
                    //TrackingID
                    nav_view!!.getHeaderView(0).trackingID.text =
                        Html.fromHtml("Tracking ID: ${sharedPreferenceManager.aofAccountInfo.TRACKING_ID}")
                    nav_view!!.getHeaderView(0).trackingID.paintFlags =
                        nav_view!!.getHeaderView(0).trackingID.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                }
                //VersionName
                nav_view!!.getHeaderView(0).versionNameTxt.text =
                    Html.fromHtml("VersionName: $versionName")
                nav_view!!.getHeaderView(0).versionNameTxt.paintFlags =
                    nav_view!!.getHeaderView(0).versionNameTxt.paintFlags or Paint.UNDERLINE_TEXT_FLAG
                //VersionCoe
                nav_view!!.getHeaderView(0).versionNoTxt.text =
                    Html.fromHtml("VersionCode: $versionCode")
                nav_view!!.getHeaderView(0).versionNoTxt.paintFlags =
                    nav_view!!.getHeaderView(0).versionNoTxt.paintFlags or Paint.UNDERLINE_TEXT_FLAG

            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            val navController = findNavController(R.id.cifHostFragment)
            // Passing each menu ID as a set of Ids because each
            // menu should be considered as top level destinations.
            appBarConfiguration = AppBarConfiguration(
                setOf(
                    R.id.CIFStep1,
                    R.id.CIFStep1_3,
                    R.id.CIFStep1_4,
                    R.id.CIFStep1_5,
                    R.id.CIFStep1_6,
                    R.id.CIFStep1_7,
                    R.id.CIFStep1_9,
                    R.id.CIFStep1_11,
                    R.id.CIFStep1_12,
                    R.id.CIFStep1_13,
                    R.id.CIFStep2,
                    R.id.CIFStep2_3,
                    R.id.CIFStep2_5,
                    R.id.CIFStep2_7,
                    R.id.CIFStep3,
                    R.id.CIFStep3_2,
                    R.id.CIFStep3_4,
                    R.id.CIFStep3_5,
                    R.id.CIFStep4,
                    R.id.CIFStep4_2,
                    R.id.CIFStep4_4,
                    R.id.CIFStep4_5,
                    R.id.CIFStep4_6,
                    R.id.CIFStep4_7,
                    R.id.CIFStep4_8,
                    R.id.CIFStep5,
                    R.id.CIFStep5_3,
                    R.id.CIFStep5_5,
                    R.id.CIFStep5_7,
                    R.id.CIFStep6,
                    R.id.CIFStep6_1,
                    R.id.CIFStep6_4,
                    R.id.CIFStep6_5,
                    R.id.CIFStep6_7,
                    R.id.CIFStep6_9,
                    R.id.CIFStep6_11,
                    R.id.CIFStep7,
                    R.id.CIFStep7_2,
                    R.id.CIFStep7_3,
                    R.id.CIFStep7_4,
                    R.id.CIFStep9,
                    R.id.CIFStep10,
                    R.id.CIFStep11, R.id.prooceedToSummary,
                    R.id.openAccountStep1
                ), drawerLayout
            )
            setupActionBarWithNavController(navController, appBarConfiguration)
            navView.setupWithNavController(navController)
            navController.addOnDestinationChangedListener { _, destination, _ ->
                when (destination.id) {
                    R.id.CIFStep1, R.id.CIFStep1_3,
                    R.id.CIFStep1_6, R.id.CIFStep1_7, R.id.CIFStep1_9,
                    R.id.CIFStep1_11, R.id.CIFStep1_12, R.id.CIFStep1_13,
                    -> {
                        toolbar.visibility = View.VISIBLE
//                    tvTitle.text = "Customer Basic Information"
//                        tvTitle.text = "Page: Customer ID Information"
                    }
                    R.id.CIFStep1_4, R.id.CIFStep1_5 -> {
                        toolbar.visibility = View.VISIBLE
//                    tvTitle.text = "Customer Basic Information"
//                        tvTitle.text = "Page: Biometric Information"
                    }
                    R.id.CIFStep2, R.id.CIFStep2_3, R.id.CIFStep2_5, R.id.CIFStep2_7 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Customer Contact Details"
                    }
                    R.id.CIFStep3, R.id.CIFStep3_2 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: FATCA Information"
                    }
                    R.id.CIFStep3_4, R.id.CIFStep3_5 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = resources!!.getString(R.string.other_country_tax_payer)
                    }
                    R.id.CIFStep4, R.id.CIFStep4, R.id.CIFStep4_2, R.id.CIFStep4_4, R.id.CIFStep4_5 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Demographic (CDD)"
                    }
                    R.id.CIFStep5, R.id.CIFStep5, R.id.CIFStep5_3, R.id.CIFStep5_5, R.id.CIFStep5_7 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Customer Due Diligence"
                    }
                    R.id.CIFStep6, R.id.CIFStep6, R.id.CIFStep6_4, R.id.CIFStep6_5,
                    R.id.CIFStep6_7, R.id.CIFStep6_9, R.id.CIFStep6_11,
                    -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Customer Enhance Due Diligence"
                    }
                    R.id.CIFStep7, R.id.CIFStep7, R.id.CIFStep7_2, R.id.CIFStep7_3, R.id.CIFStep7_4 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Accounts"
                    }

                    R.id.CIFStep10, R.id.CIFStep10 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Next Of KIN"
                    }

                    R.id.CIFStep10, R.id.CIFStep11 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Next Of KIN"
                    }

                    R.id.CIFStep10 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Next Of KIN"
                    }

                    R.id.CIFStep10, R.id.CIFStep12_2 -> {
                        toolbar.visibility = View.VISIBLE
                        siIndicator!!.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Next Of KIN"
                    }

                    R.id.CIFStep14, R.id.CIFStep14 -> {
                        toolbar.visibility = View.VISIBLE
//                        tvTitle.text = "Page: Document Upload"
                    }

                    R.id.CIFStep9 -> {
                        toolbar.visibility = View.VISIBLE
                        siIndicator!!.visibility = View.INVISIBLE
                        tvTitle.text = ""
                    }

                    R.id.CIFStep15 -> {
                        toolbar.visibility = View.VISIBLE
                        siIndicator!!.visibility = View.INVISIBLE
                        tvTitle.text = ""
                    }

                    R.id.CIFStep16 -> {
                        toolbar.visibility = View.VISIBLE
                        siIndicator!!.visibility = View.INVISIBLE
                        tvTitle.text = ""
                    }
                    R.id.openAccountStep1 -> {
                        toolbar.visibility = View.VISIBLE

                        tvTitle.text = getString(R.string.open_account)
                    }
                    R.id.prooceedToSummary -> {
                        toolbar.visibility = View.GONE
                    }
                    else -> {
                        toolbar.visibility = View.VISIBLE
                    }
                }
            }

            ivAvatar.setOnClickListener {
                // Check for current fragment
                about()
            }
            ivHome!!.setOnClickListener {
                // Check for current fragment
                finish()
            }
            ivSSSendEmal!!.setOnClickListener {
                // Check for current fragment
                globalClass?.showDialog(this)
                screenshot(getWindow().getDecorView().getRootView(), "snap");
            }
            viewModel.showAvatar.observe(this, androidx.lifecycle.Observer {
                if (it)
                    ivAvatar.visibility = View.VISIBLE
                else ivAvatar.visibility = View.GONE

            })

            viewModel.currentFragmentIndex.observe(this, androidx.lifecycle.Observer {
                siIndicator!!.currentStep = it
                // it is your current fragment index
            })

            viewModel.totalSteps.observe(this, androidx.lifecycle.Observer {
                siIndicator!!.stepCount = it
            })

            drawerLayout.setDrawerListener(this)
            recyclerViewSetup()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    protected fun screenshot(view: View, filename: String): File? {
        val date = Date()

        // Here we are initialising the format of our image name
        val format = DateFormat.format("yyyy-MM-dd_hh:mm:ss", date)
        try {
            // Initialising the directory of storage
            val dirpath: String = Environment.getExternalStorageDirectory().toString() + ""
            val file = File(dirpath)
            if (!file.exists()) {
                val mkdir = file.mkdir()
            }

            // File name
            val path = "$dirpath/$filename-$format.jpeg"
            view.isDrawingCacheEnabled = true
            val bitmap: Bitmap = Bitmap.createBitmap(view.drawingCache)
            view.isDrawingCacheEnabled = false
            val imageurl = File(path)
            val outputStream = FileOutputStream(imageurl)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
            outputStream.flush()
            outputStream.close()
            globalClass?.hideLoader()
            viewAndSendScreenshortAlert(imageurl)
            return imageurl;
        } catch (io: FileNotFoundException) {
            io.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        globalClass?.hideLoader()
        return null
    }

    fun viewAndSendScreenshortAlert(file: File) {
        try {
            var dialogBuilder = AlertDialog.Builder(this@HawActivity)
            val layoutView: View = layoutInflater.inflate(R.layout.snap_popup, null)
            dialogBuilder.setView(layoutView)
            var alertDialog = dialogBuilder.create()
            alertDialog.show()
            alertDialog.setCancelable(false)
            var subjectBody = layoutView.findViewById<EditText>(R.id.subjectBody)
            var messageBody = layoutView.findViewById<EditText>(R.id.messageBody)
            var snapImage = layoutView.findViewById<ImageView>(R.id.snapImage)
            var sendAction = layoutView.findViewById<Button>(R.id.sendAction)
            snapImage.setImageURI(Uri.parse(file.path))
            var cancel_action = layoutView.findViewById<Button>(R.id.cancel_action)
            sendAction.setOnClickListener(View.OnClickListener {
                if (!subjectBody.text.toString().trim().isNullOrEmpty()) {
                    globalClass?.showDialog(this)
                    if (file.exists() && file.length() > 0) {
                        val bm = BitmapFactory.decodeFile(file.path)
                        val bOut = ByteArrayOutputStream()
                        bm.compress(Bitmap.CompressFormat.JPEG, 100, bOut)
                    }
                    alertDialog.dismiss()
                    SendEmail.sendAttachmentEmail(
                        this,
                        file,
                        subjectBody.text.toString().trim(),
                        messageBody.text.toString().trim(),
                        this.javaClass.simpleName
                    )
                } else {
                    ToastUtils.normalShowToast(this, "Please insert a subject line", 3)
                }
            })
            cancel_action.setOnClickListener(View.OnClickListener {
                GlobalClass.delFile(file.path)
                globalClass?.hideLoader()
                alertDialog.dismiss()
            })
            alertDialog.show()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    fun avatarDialog() {
        try {
            var dialogBuilder = AlertDialog.Builder(this@HawActivity)
            val layoutView: View = layoutInflater.inflate(R.layout.customer_popup, null)
            dialogBuilder.setView(layoutView)
            var alertDialog = dialogBuilder.create()
            alertDialog.show()
            var cancel_action = layoutView.findViewById<Button>(R.id.cancel_action)
            cancel_action.setOnClickListener(View.OnClickListener {
                alertDialog.dismiss()
            })
            alertDialog.show()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    fun init() {
        try {
            aofAccountInfoRequest = (sharedPreferenceManager.aofAccountInfo)
            fullName = aofAccountInfoRequest.NAME
            riskRating = aofAccountInfoRequest.RISK_RATING.toString()
            riskRatingTotal = aofAccountInfoRequest.RISK_RATING_TOTAL
            etbntbFLAG = aofAccountInfoRequest.ETBNTBFLAG
            REMEDIATE = aofAccountInfoRequest.REMEDIATE
            customerFIN = (sharedPreferenceManager.customerFIN)
            customerInfo = (sharedPreferenceManager.customerInfo)
            customerPep = (sharedPreferenceManager.customerPep)
            customerCdd = (sharedPreferenceManager.customerCDD)
            customerAddressList = (sharedPreferenceManager.customerAddress)
            customerAccounts = (sharedPreferenceManager.customerAccount)
            customerGaurdianInfo = (sharedPreferenceManager.gaurdianInfo)
            partialDocumets = (sharedPreferenceManager.partialDocs)
            customerBiometric = (sharedPreferenceManager.customerBiomatric)
            customerContacts = (sharedPreferenceManager.customerContacts)
            customerDemoGraphicx = (sharedPreferenceManager.customerDemoGraphics)
            customerEdd = (sharedPreferenceManager.customerEDD)
            customerDocument = (sharedPreferenceManager.customerDocument)
            customerNextOfKin = (sharedPreferenceManager.customerNextOfKin)
            customerSOLESelf = (sharedPreferenceManager.customerSoleSelf)
            customerStatus = (sharedPreferenceManager.customerStatus)
            customerDocument = (sharedPreferenceManager.customerDocument)
            docCheckList = (sharedPreferenceManager.docCheckList)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    private fun locationGet() {
        mGoogleApiClient = GoogleApiClient.Builder(this@HawActivity)
            .addConnectionCallbacks(this@HawActivity)
            .addOnConnectionFailedListener(this@HawActivity)
            .addApi(LocationServices.API)
            .build()
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager?
    }
    fun getAddress(lat: Double, lng: Double) {
        val gCoder = Geocoder(this@HawActivity)
        var addresses: List<Address>? = null
        try {
            addresses = gCoder.getFromLocation(lat, lng, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (addresses != null && addresses.isNotEmpty()) {
            currentAddress= addresses[0].getAddressLine(0)
            Log.i(TAG2, addresses[0].getAddressLine(0))
        }
    }
    override fun onConnected(bundle: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                this@HawActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this@HawActivity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        startLocationUpdates()
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (mLocation == null) {
            startLocationUpdates()
        }
        if (mLocation != null) {
            val latitude = mLocation!!.getLatitude()
            val longitude = mLocation!!.getLongitude()
        } else {
            // Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }
    private fun startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000)        // 10 seconds, in milliseconds
            .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                this@HawActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this@HawActivity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.i(
                TAG2,
                "Location Allowed"
            )
            return
        }else{
            Log.i(
                TAG2,
                "Location Allowed"
            )
        }
        val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient!!,
                mLocationRequest!!,
                object :com.google.android.gms.location.LocationListener{
                    override fun onLocationChanged(p0: Location) {
                        Log.i(
                            TAG2,
                            "${p0!!.latitude},${p0!!.longitude}"
                        )
                        currentLatitude = p0!!.latitude
                        currentLongitude = p0!!.longitude
                    }

                })
            Log.i(
                TAG2,
                "$currentLatitude WORKS $currentLongitude"
            )
        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.latitude
            currentLongitude = location.longitude
            //If everything went fine lets get latitude and longitude
            getAddress(currentLatitude,currentLongitude)
            Log.i(
                TAG2,
                "$currentLatitude WORKS $currentLongitude"
            )
        }
        Log.d("reque", "--->>>>")
    }
    override fun onConnectionSuspended(i: Int) {
        Log.i(TAG2, "Connection Suspended")
        mGoogleApiClient!!.connect()
    }
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i(TAG2, "Connection failed. Error: " + connectionResult.errorCode)
    }
    override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }
    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.disconnect()
        }
    }



    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
//        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.cifHostFragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            val host: NavHostFragment =
                supportFragmentManager.findFragmentById(R.id.cifHostFragment) as NavHostFragment?
                    ?: return
//            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment? ?: return
            val fragmen = host.childFragmentManager.fragments.get(0)
            fragmen.onActivityResult(requestCode, resultCode, intent)
            host.onActivityResult(requestCode, resultCode, intent)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        try {
            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                orientation(false)
            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                orientation(true)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    fun orientation(isRotate: Boolean) {
        try {
            hideKeyboard()
            rvDrawer.apply {
                if (isRotate) {
                    layoutManager = GridLayoutManager(
                        this@HawActivity,
                        1
                    )
                } else {
                    layoutManager = GridLayoutManager(
                        this@HawActivity,
                        2
                    )
                }
                adapter!!.notifyDataSetChanged()
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    fun recyclerViewSetup() {
        try {
            viewModel.initiateDrawer(this)
            drawerAdapter = DrawerAdapter(viewModel.mList)
            rvDrawer.apply {
                val orientation = this.resources.configuration.orientation
                if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                    // code for portrait mode
                    layoutManager = GridLayoutManager(
                        this@HawActivity,
                        1
                    )
                } else {
                    // code for landscape mode
                    layoutManager = GridLayoutManager(
                        this@HawActivity,
                        2
                    )
                }

                adapter = drawerAdapter
            }

            rvDrawer.addOnItemTouchListener(
                RecyclerTouchListener(
                    this,
                    rvDrawer,
                    object :
                        RecyclerTouchListener.ClickListener {
                        override fun onClick(view: View, position: Int) {
                            changeStartDestination(viewModel.mList[position].title!!)
                        }

                        override fun onLongClick(view: View?, position: Int) {

                        }
                    })
            )
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    override fun onDrawerStateChanged(newState: Int) {

    }

    override fun onDrawerSlide(drawerView: View, slideOffset: Float) {

    }

    override fun onDrawerClosed(drawerView: View) {
    }

    override fun onDrawerOpened(drawerView: View) {
        hideKeyboard()
//        drawerAdapter.notifyDataSetChanged()
    }

    fun hideKeyboard() {
        try {
            val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
            //Find the currently focused view, so we can grab the correct window token from it.
            //Find the currently focused view, so we can grab the correct window token from it.
            var view: View = getCurrentFocus()!!
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            //If no view currently has focus, create a new one, just so we can grab a window token from it
            if (view == null) {
                view = View(this)
            }
            imm.hideSoftInputFromWindow(view.windowToken, 0)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    fun fileDelete(path: String?) {
        try {
            val fileDir = File(path)
            if (fileDir.exists()) {
                fileDir.delete()
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    @SuppressLint("WrongConstant")
    fun changeStartDestination(category: String) {
        try {
            //TODO: If user filled the account model and then change the customer segment hardly then he will not be jump to any page over drawer menu
            var changeCUSTTYPE =
                sharedPreferenceManager.aofAccountInfo.isForcelyChangedCustomerSegment
            /*24 pages of models*/
            when (category) {
                getString(R.string.customer_id_info) -> {
                    switchFragment(R.id.CIFStep1)
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.bio_metric_Information) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep1_4
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.customer_id_details) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep1_6
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.customer_personal_information) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep1_7
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                /*conditions*/
                getString(R.string.pep_details) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(category) == DrawerItem.PEP) {
                        switchFragment(R.id.CIFStep1_9)
                    } else if (setFilledPagesList(category) == DrawerItem.ITEM_DONE) {
                        switchFragment(R.id.CIFStep1_11)
                    }
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                /*====*/
                getString(R.string.contact_information) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep2
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.current_address) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep2_3
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.permanent_address) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep2_5
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.office_address) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep2_7
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.fata_for_us_citizens_details) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(category) == DrawerItem.FATCA) {
                        switchFragment(
                            R.id.CIFStep2_7
                        )
                    } else if (!changeCUSTTYPE && setFilledPagesList(category) == DrawerItem.ITEM_DONE) {
                        switchFragment(R.id.CIFStep3)
                    }
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.other_country_tax_payer) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(category) == DrawerItem.CRS) switchFragment(
                        R.id.CIFStep2_7
                    )
                    else if (!changeCUSTTYPE && setFilledPagesList(category) == DrawerItem.ITEM_DONE) switchFragment(
                        R.id.CIFStep3_4
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.customer_demographic) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep4
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.financial_supporter_details) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep4_8
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.cdd_details) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep5
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.foreign_inward_outward_remittance_details) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(R.id.CIFStep5_3)
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.other_bank_information_details) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep5_5
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.Verification_customer_due_dillgence) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(R.id.CIFStep5_7)
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.edd_details) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep6
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.edd_additional_source_of_income) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep6_4
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.edd_additional_quries) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep6_5
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.edd_financial_supporter_source_of_income) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(R.id.CIFStep6_9)
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.edd_verification) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep6_11
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.account) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(category) == DrawerItem.ITEM_DONE) switchFragment(
                        R.id.CIFStep7
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
                getString(R.string.next_of_kin) -> {
                    if (!changeCUSTTYPE && setFilledPagesList(
                            category
                        ) == DrawerItem.ITEM_DONE
                    ) switchFragment(
                        R.id.CIFStep10
                    )
                    drawer_layout.closeDrawer(Gravity.START, true)
                }
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    private fun switchFragment(startDestId: Int) {
        try {
//        val fragmentContainer = view?.findViewById<View>(R.id.nav_host)
//        val navController = Navigation.findNavController(fragmentContainer!!)
            val navController = findNavController(R.id.cifHostFragment)
            val inflater = navController.navInflater
            val graph = navController.graph
            graph.startDestination = startDestId
            navController.graph = graph
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    @SuppressLint("WrongConstant")
    fun setFilledPagesList(category: String?): DrawerItem {
        try {
            /*24 pages of models*/
            var check = false
            pOMStatus = HawPOMStatus(this@HawActivity)
            listOfPages.clear()
            //:TODO customer ID information
            if (pOMStatus?.customerIDInfoPage1() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.customer_id_info),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.customer_id_info),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO customer Biometric
            if (pOMStatus?.customerBiometricPage1() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.bio_metric_Information),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.bio_metric_Information),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO customer ID Details
            if (pOMStatus?.customerIdDetailsPage1() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.customer_id_details),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.customer_id_details),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO customer personal information
            if (pOMStatus?.customerPersonalInfoPage1And2() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.customer_personal_information),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.customer_personal_information),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO customer PEP
            if (pOMStatus?.customerInfo?.PEP.equals("1") && pOMStatus?.customerPepPage1_2_3() != 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.pep_details),
                        DrawerItem.PEP
                    )
                )
            } else if (pOMStatus?.customerPepPage1_2_3() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.pep_details),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.pep_details),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO customer Contact Info
            if (pOMStatus?.customerContactInfoPage1() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.contact_information),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.contact_information),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO customerAddress1 Current
            if (pOMStatus?.customerAddressPage1() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.current_address),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.current_address),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO customerAddress2 Permanent
            if (pOMStatus?.customerAddressPage2() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.permanent_address),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.permanent_address),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO customerAddress3 Office
            if (pOMStatus?.customerAddressPage3() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.office_address),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.office_address),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO  FATCA Information
            if (customerInfo.FATCA == 1 && pOMStatus?.customerFATCAPage1And2() != 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.fata_for_us_citizens_details),
                        DrawerItem.FATCA
                    )
                )
            } else if (pOMStatus?.customerFATCAPage1And2() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.fata_for_us_citizens_details),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.fata_for_us_citizens_details),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO Customer CRS
            if (customerInfo.IS_FORCE_CRS == "1" && pOMStatus?.customerCRSPage1And2() != 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.other_country_tax_payer),
                        DrawerItem.CRS
                    )
                )
            } else if (pOMStatus?.customerCRSPage1And2() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.other_country_tax_payer),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.other_country_tax_payer),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO Demographic (CDD)
            if (pOMStatus?.customerDemographicsPage1_2_3() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.customer_demographic),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.customer_demographic),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO Financial Supporter Page
            if (pOMStatus?.customerFinancialSupporterPage1() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.financial_supporter_details),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.financial_supporter_details),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO CDD Details
            if (pOMStatus?.customerCDDDetails() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.cdd_details),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.cdd_details),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO Foreign Inward/Onward Remittance Details
            if (pOMStatus?.customerInwardOutWardPage() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.foreign_inward_outward_remittance_details),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.foreign_inward_outward_remittance_details),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO Other Bank Infor
            if (pOMStatus?.customerOtherBankInfo() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.other_bank_information_details),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.other_bank_information_details),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO Verification Customer Due Diligence
            if (pOMStatus?.customerVerifyCDD() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.Verification_customer_due_dillgence),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.Verification_customer_due_dillgence),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }


            //:TODO Customer Enhance Due Diligence
            //:TODO EDD Details
            if (pOMStatus?.customerEDDDetails() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_details),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_details),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO EDD-Additional Source of Income
            if (pOMStatus?.customerEddAdditionalSource() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_additional_source_of_income),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_additional_source_of_income),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }


            //:TODO EDD - Additional Queries
            if (pOMStatus?.customerEddAdditionalQueries() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_additional_quries),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_additional_quries),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO EDD Financial Supporter of income
            if (pOMStatus?.customerEddFinancialSupporter() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_financial_supporter_source_of_income),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_financial_supporter_source_of_income),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            //:TODO EDD Verification
            if (pOMStatus?.customerEddVerification() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_verification),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.edd_verification),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }
            //:TODO Account
            if (pOMStatus?.Account_Page_1_2_3_4() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.account),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.account),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }


            //:TODO NOK
            if (pOMStatus?.NextOfKin_Page_1_2_3() == 0) {
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.next_of_kin),
                        DrawerItem.ITEM_DONE
                    )
                )
            }else{
                listOfPages.add(
                    DrawerModel(
                        getString(R.string.next_of_kin),
                        DrawerItem.ITEM_LINEUP
                    )
                )
            }

            if (listOfPages.size != 0 && listOfPages[listOfPages.size - 1].item == DrawerItem.ITEM_DONE && listOfPages[listOfPages.size - 1].title == category) {
                drawer_layout.closeDrawer(Gravity.START, true)
                return DrawerItem.ITEM_DONE
            } else if (listOfPages.size != 0 && listOfPages[listOfPages.size - 1].item == DrawerItem.PEP && listOfPages[listOfPages.size - 1].title == category) {
                drawer_layout.closeDrawer(Gravity.START, true)
                return DrawerItem.PEP
            } else if (listOfPages.size != 0 && listOfPages[listOfPages.size - 1].item == DrawerItem.FATCA && listOfPages[listOfPages.size - 1].title == category) {
                drawer_layout.closeDrawer(Gravity.START, true)
                return DrawerItem.FATCA
            } else if (listOfPages.size != 0 && listOfPages[listOfPages.size - 1].item == DrawerItem.CRS && listOfPages[listOfPages.size - 1].title == category) {
                drawer_layout.closeDrawer(Gravity.START, true)
                return DrawerItem.CRS
            } else{
                var position = -1
                for (x in 0 until listOfPages.size) {
                    if (listOfPages.get(x).title!!.equals(category, true)) {
                        position = x!!
                        break
                    }
                }
                return listOfPages.get(position).item!!
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
            return DrawerItem.ITEM_LINEUP
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
            return DrawerItem.ITEM_LINEUP
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
            return DrawerItem.ITEM_LINEUP
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
            return DrawerItem.ITEM_LINEUP
        }
    }

    override fun onBackPressed() {
        try {
            val navController = findNavController(R.id.cifHostFragment)
            if (backInt == 0 || backInt == 1 || backInt == 2
            ) {
                finish()
            } else {
                navController.popBackStack()
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }


    @SuppressLint("WrongConstant")
    private fun about() {
        try {
            var dialogBuilder = AlertDialog.Builder(this)
            val layoutView: View = layoutInflater.inflate(R.layout.about_app_popup, null)
            dialogBuilder.setView(layoutView)
            var alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()
            var shareBtn = layoutView.findViewById<ImageView>(R.id.shareBtn)
            var ok_action = layoutView.findViewById<Button>(R.id.ok_action)
            var toolbar = layoutView.findViewById<Toolbar>(R.id.toolbar)
            toolbar.title = "About"
            var appname = layoutView.findViewById<TextView>(R.id.appname)
            appname.text = resources.getString(R.string.app_name)
            var UserID = layoutView.findViewById<TextView>(R.id.UserID)
            UserID.text = sharedPreferenceManager.loginData.getUSERID()
            var applicationID = layoutView.findViewById<TextView>(R.id.applicationID)
            var androidVersion = layoutView.findViewById<TextView>(R.id.androidVersion)
            var version = layoutView.findViewById<TextView>(R.id.version)
            var versionCod = layoutView.findViewById<TextView>(R.id.versionCode)
            var apkSize = layoutView.findViewById<TextView>(R.id.apkSize)
            var createDateOfFile = layoutView.findViewById<TextView>(R.id.createDateOfFile)
            var updaDateOfFile = layoutView.findViewById<TextView>(R.id.updaDateOfFile)
            var packageNameText = layoutView.findViewById<TextView>(R.id.packageName)
            packageNameText.text = packageManager.getPackageInfo(packageName, 0).packageName
            try {
                val versionName =
                    packageManager.getPackageInfo(
                        packageName,
                        0
                    ).versionName
                val versionCode =
                    packageManager.getPackageInfo(
                        packageName,
                        0
                    ).versionCode
                version.text = "$versionName"
                versionCod.text = "$versionCode"
                applicationID.text = "${GlobalClass.deviceID()}"
                androidVersion.text = "${globalClass!!.getAndroidVersion(android.os.Build.VERSION.SDK_INT)}"
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            if (GlobalClass.getFileAPK().exists()) {
                apkSize.text = GlobalClass.getFolderSizeLabel(GlobalClass.getFileAPK())
                createDateOfFile.text = GlobalClass.firstModifiedDate()
                updaDateOfFile.text = GlobalClass.lastModifiedDate()
            } else {
                apkSize.text = "N/A"
                createDateOfFile.text = "N/A"
                updaDateOfFile.text = "N/A"
            }
            ok_action.setOnClickListener(View.OnClickListener {
                alertDialog.dismiss()
            })
            shareBtn.setOnClickListener(View.OnClickListener {
                globalClass?.showDialog(this)
                var request = AppDetailsRequest()
                request.identifier = Constants.APP_DETAILS_IDENTIFIER
                request.apkSize = apkSize.text.toString()
                request.applicationID = applicationID.text.toString()
                request.applicationName = appname.text.toString()
                request.createdApkDate = createDateOfFile.text.toString()
                request.updateApkDate = updaDateOfFile.text.toString()
                request.userID = sharedPreferenceManager.loginData.getUSERID()
                request.versionCode = versionCod.text.toString()
                request.versionName = version.text.toString()
                request.androidApiVersion = globalClass!!.getAndroidVersion(android.os.Build.VERSION.SDK_INT).toString()
                request.IMEI = "${GlobalClass.getIMEIDeviceId(this@HawActivity)}"
                request.packageName =(this@HawActivity).packageManager.getPackageInfo(
                    (this@HawActivity).packageName,
                    0
                ).packageName
                HBLHRStore?.instance?.sendAppDetails(RetrofitEnums.URL_HBL, request, object :
                    AppStatusCallBack {
                    override fun AppStatusSuccess(response: AppStatusResponse) {
                        try {
                            ToastUtils.normalShowToast(this@HawActivity, response.message, 2)
                            alertDialog.dismiss()
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (this@HawActivity),
                                (e as ActivityNotFoundException)
                            )
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (this@HawActivity),
                                (e as IndexOutOfBoundsException)
                            )
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (this@HawActivity),
                                (e as ArrayIndexOutOfBoundsException)
                            )
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(
                                this@HawActivity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((this@HawActivity), (e as Exception))
                        }
                        this@HawActivity.globalClass?.hideLoader()
                    }

                    override fun AppStatusFailure(response: BaseResponse) {
                        Log.d("text", response.toString())
                        this@HawActivity.globalClass?.hideLoader()
                    }
                })
            })
            alertDialog.show()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawActivity,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawActivity), (e as Exception))
        }
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            try {
                if (intent.action == Constants.STATUS_EMAIL_SENT) {
                    ToastUtils.normalShowToast(
                        this@HawActivity,
                        "Screenshot has been sent to the admin",
                        3
                    )
                    globalClass?.hideLoader()
                }
            } catch (ex: Exception) {
            }
        }
    }

    override fun onRestart() {
        super.onRestart()
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(mStatusCodeResponse)
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(broadcastReceiver)
        super.onDestroy()
    }
     fun visitBranchPopup(msg:String) {
        val builder = AlertDialog.Builder(this)
        builder.setTitle("ALERT")
        builder.setCancelable(false)
        builder.setMessage(msg)
        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
        }
        builder.show()
    }
}
