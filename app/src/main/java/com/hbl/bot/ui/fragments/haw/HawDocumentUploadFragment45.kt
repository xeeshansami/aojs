package com.hbl.bot.ui.cif

import android.Manifest
import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.*
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.ViewGroup
import android.view.animation.DecelerateInterpolator
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.google.gson.Gson
import com.hbl.bot.BuildConfig
import com.hbl.bot.R
import com.hbl.bot.model.DocumentCategoryState
import com.hbl.bot.model.HomeGridModel
import com.hbl.bot.network.ResponseHandlers.callbacks.ConfigCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.DocumentTypeCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.PartialUploadCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.SubmitToSuperCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.request.baseRM.CUSTDOCUMENT
import com.hbl.bot.network.models.request.baseRM.DOCCHECKLIST
import com.hbl.bot.network.models.request.baseRM.ImageBuffer
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.DocumentType
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.ui.adapter.CIF14Model
import com.hbl.bot.ui.adapter.CustomDocumentUploadSpinnerAdapter
import com.hbl.bot.ui.adapter.PictureGridAdapter
import com.hbl.bot.ui.customviews.DialogCallback
import com.hbl.bot.utils.*
import com.hbl.bot.utils.extensions.checkPermission
import com.hbl.bot.utils.extensions.requestAllPermissions
import com.hbl.bot.utils.extensions.shouldRequestPermissionRationale
import com.hbl.bot.viewModels.SharedCIFViewModel
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_cifstep14.*
import kotlinx.android.synthetic.main.view_skip_next_bottom.*
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import kotlin.concurrent.schedule


/**
 * A simple [Fragment] subclass.
 */
class HawDocumentUploadFragment45 : Fragment(), View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private var postitionBeforeSelection = 0
    private var currentAnimator: Animator? = null
    lateinit var frameLayout: FrameLayout
    var globalClass: GlobalClass = GlobalClass()
    private var shortAnimationDuration: Int = 0
    var selectioncount = 0
    private var inSelectionMode: Boolean = false
    var FinalPath = ""
    lateinit var cross_image: ImageView
    lateinit var expandedImageView: ImageView
    var selectedDocumentTypeIndex = 0
    var DocumentType: ArrayList<String>? = null
    var filePath = ""
    var IDDOC: Int = 0
    var NADRA: Int = 0
    var ADDR: Int = 0
    var UNSC: Int = 0
    var AOFCIF: Int = 0
    var TnC: Int = 0
    var POI: Int = 0
    var SuccessCounter: Int = 0
    var EDDFILL: Int = 0
    var SSCARD: Int = 0
    var KFS: Int = 0
    var buttonEnable = false
    var isComplete = false
    val custDocumentsList: MutableList<CustDocument> = ArrayList()
    val IdDocImagePath: MutableList<String> = ArrayList()
    val IdDocImageList: MutableList<String> = ArrayList()
    val ToSendImageList: MutableList<String> = ArrayList()
    val ToSendPathList: MutableList<String> = ArrayList()
    val MainCustDocumentsList: MutableList<CUSTDOCUMENT> = ArrayList()
    val docCheckListList: ArrayList<DOCCHECKLIST> = ArrayList()
    val viewModel: SharedCIFViewModel by activityViewModels()
    var count: Int = 0
    var doctypedupS: ArrayList<DocumentType> = ArrayList()
    var custImageBuffer: ArrayList<ImageBuffer> = ArrayList()
    var custImageBufferOld: ArrayList<ImageBuffer> = ArrayList()
    var countid: Int = 0
    var mergedSP: ArrayList<DocumentType> = ArrayList()
    private val permissions =
        arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
    private lateinit var pictureGridAdapter: PictureGridAdapter
    val mList = mutableListOf<HomeGridModel>()
    private lateinit var adapter: CustomDocumentUploadSpinnerAdapter
    var myView: View? = null

    companion object {
        var mCurrentPhotoPath: String? = null
        const val PERMISSION_REQUEST_STORAGE = 0
    }

    var mCurrentPhotoUri: Uri? = null

    // Storage for camera image URI components
    private val CAPTURED_PHOTO_PATH_KEY = "mCurrentPhotoPath"
    private val CAPTURED_PHOTO_URI_KEY = "mCapturedImageURI"

    // Required for camera operations in order to save the image file on resume.
    private var mCurrentPhotoPath: String? = null
    private var mCapturedImageURI: Uri? = null

    //    var  pathList = ArrayList<CIF14Model>()
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.COM_UPLOAD_DOC == "1") {
                showSkipDialog()
            } else if (myView == null) {
                myView = inflater.inflate(R.layout.fragment_cifstep14, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    private fun showSkipDialog() {
        val builder = AlertDialog.Builder((activity as HawActivity))
        builder.setTitle("Information")
        builder.setCancelable(false)
        builder.setMessage((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.COM_UPLOAD_DOC_DESC)
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            findNavController().navigate(R.id.action_CIFStep14_to_prooceedToSummary)
            (activity as HawActivity).recyclerViewSetup()
        }
        builder.show()
    }


    private fun sendSupervisorApi(path: String?, flag: String?) {
        try {


            var request = SubmitToSuperRequest()
            var custOtherDocument = CUSTDOCUMENT()
//            var custSSDDocument = cust_document()
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                request.tracking_id = it
            }

            custOtherDocument.docid = "01"
            custOtherDocument.doctype = "Other Documents"
            custOtherDocument.docname =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID + "/" + (activity as HawActivity).sharedPreferenceManager.getStringFromSharedPreferences(
                    (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ID_DOC_NO
                ) + "_" + (activity as HawActivity).sharedPreferenceManager.customerInfo.CIF_NO + "_" + (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.toString() + "-01.pdf"
            custOtherDocument.docpath = path
            custOtherDocument.docstatus = "01"
            custOtherDocument.doC_MAP = custDocumentsList
            custOtherDocument.additionaldocmap = ""

            if (MainCustDocumentsList.size == 0) {
                MainCustDocumentsList.add(0, custOtherDocument)
            } else if (MainCustDocumentsList.size >= 1) {
                MainCustDocumentsList[0] = custOtherDocument
            }
//            MainCustDocumentsList.set(0, custOtherDocument)
//
//            custSSDDocument.DOC_ID = "02"
//            custSSDDocument.DOC_TYPME = "sscard"
////            custSSDDocument.DOC_NAME = "1403200786-24/3540459480967_CHRK75_@10032858-01.pdf"
////            custSSDDocument.DOC_PATH =
////                globalClass.getConfigData().PROTOCOL + globalClass.getConfigData().IP + ":" + globalClass.getConfigData().PORT + "/1403200786-24/3540459480967_CHRK75_@10032858-01.pdf"
////            custSSDDocument.DOC_STATUS = "01"
////            custSSDDocument.DOC_AP = custDocumentsList
//            custSSDDocument.ADDITIONAL_DOC_MAP = ""
//            MainCustDocumentsList.add(custSSDDocument)

            request.cust_documents = MainCustDocumentsList
            request.doc_checklist = docCheckListList

            request._TYPE = "S"
            request.COM_UPLOAD_DOC = flag
            request.P_S_DOC_LIST.OPTIONAL_DATA
            request.P_S_DOC_LIST.MANDATORY_DATA = doctypedupS
            request.P_S_DOC_LIST.FILES?.add(0, path.toString())
            request.P_S_DOC_LIST.LOAD_PARTIAL_OLD_IMAGES_BUFFER = custImageBuffer

            request.P_S_DOC_LIST.TRACKING_ID =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID
            request.P_S_DOC_LIST.TAG_ACTIVE = "0"
            request.P_S_DOC_LIST.PARTIAL_DOC_ENABLE = "1"
            request.identifier = "sendtosupervisor"
            val gson = Gson()
            val json = gson.toJson(request)
            saveAndNext(flag)
            HBLHRStore.instance?.SubmitToSuper(
                RetrofitEnums.URL_HBL,
                request,
                object : SubmitToSuperCallBack {
                    override fun Success(response: SubmitToSuperResponse) {
                        try {
                            if (postitionBeforeSelection == globalClass.documentType!!.size - 1) {
                                btCompleted.isEnabled = true
                                btCamera.isEnabled = false
                                ibUpload.isEnabled = false
                                spDocumentType.isEnabled = false
                                (activity as HawActivity).globalClass?.setEnabled(btCompleted!!,
                                    true)
                                (activity as HawActivity).globalClass?.setDisbaled(ibUpload!!,
                                    false)
                                (activity as HawActivity).globalClass?.setDisbaled(btCamera!!,
                                    false)
                                (activity as HawActivity).globalClass?.setDisbaled(
                                    spDocumentType!!,
                                    false
                                )
                                viewModel.types[selectedDocumentTypeIndex].images.clear()
                                pictureGridAdapter.notifyDataSetChanged()
                                (activity as HawActivity).globalClass?.setDisbaled(
                                    cbSelectAll!!,
                                    false
                                )

                            } else {
                                ToastUtils.normalShowToast(context, response.message.toString(), 2)
                            }
                            SuccessCounter++
//                        if (globalClass.documentType.size == SuccessCounter) {
//
//                        } else {
//
//                        }
//                        Log.d("submit", response.message.toString())

//                        findNavController().navigate(R.id.action_CIFStep14_to_dashboard)
//                        if (isComplete == true) {
//                            findNavController().navigate(R.id.action_CIFStep14_to_CIFStep9)
//                        }
                            if (flag != "0") {
                                findNavController().navigate(R.id.action_CIFStep14_to_prooceedToSummary)
                                ToastUtils.normalShowToast(
                                    activity,
                                    "All documents uploaded successfully back to dashboard now", 2)
                            }
                            globalClass.hideLoader()
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as UnsatisfiedLinkError))
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as NullPointerException))
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IllegalArgumentException))
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as NumberFormatException))
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as InterruptedException))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as RuntimeException))
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as FileNotFoundException))
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ClassNotFoundException))
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ActivityNotFoundException))
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IndexOutOfBoundsException))
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ArrayIndexOutOfBoundsException))
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ClassCastException))
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as TypeCastException))
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as SecurityException))
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IllegalStateException))
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as OutOfMemoryError))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as RuntimeException))
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                        }
                    }

                    override fun Failure(response: BaseResponse) {
                        try {
                            globalClass.hideLoader()
//                            Utils.failedAwokeCalls((activity as HawActivity)) {
//                                sendSupervisorApi(path,
//                                    flag)
//                            }
//                        Log.d("submit", response.message.toString())
                            ToastUtils.normalShowToast(activity, response.message, 1)
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as UnsatisfiedLinkError))
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as NullPointerException))
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IllegalArgumentException))
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as NumberFormatException))
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as InterruptedException))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as RuntimeException))
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as FileNotFoundException))
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ClassNotFoundException))
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ActivityNotFoundException))
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IndexOutOfBoundsException))
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ArrayIndexOutOfBoundsException))
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ClassCastException))
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as TypeCastException))
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as SecurityException))
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IllegalStateException))
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as OutOfMemoryError))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as RuntimeException))
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                        }
                    }
                })

        } catch (ex: Exception) {
            ToastUtils.normalShowToast(
                context,
                resources.getString(R.string.something_went_wrong), 1)

        }
//        globalClass.showDialog(activity)

    }

    private fun callUploadApi(base64image: MutableList<String>, keyword: String) {
//        try {
//        globalClass.showDialog(activity)
//        var mySys = ""
//        if( (activity as HawActivity).sharedPreferenceManager.mySisRef.data[0].MYSIS_REF.isNullOrEmpty())
//        {
//            mySys = (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF[0].toString()
//        }
//        else
//        {
//            mySys= (activity as HawActivity).sharedPreferenceManager.mySisRef.data[0].MYSIS_REF
//        }


        var request = PartialUploadRequest(
            keyword,
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID,
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ID_DOC_NO,
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO[0].CIF_NO.toString(),
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.toString(),
            Files(RemoteFile("", base64image)),
            "",
            Constants.PARTIAL_UPLOAD_INDENTIFIER
        )

        HBLHRStore.instance?.partialUpload(
            RetrofitEnums.URL_HBL,
            request,
            object : PartialUploadCallBack {
                override fun PartialUploadSuccess(response: PartialUploadResponse) {
                    try {
                        IdDocImagePath.clear()
                        IdDocImageList.clear()
                        ToSendPathList.clear()
                        ToSendImageList.clear()
                        cbSelectAll.isEnabled = true
                        cbSelectAll.isChecked = false

                        viewModel.types[selectedDocumentTypeIndex].images.forEach {
                            if (it.isSelected) {
                                it.isUploaded = it.isSelected
                                it.isSelected = false


                            }

//            Log.d("hi", IdDocImageList.toString())
                        }

                        globalClass.hideLoader()
                        setSpinerSelection()
                        adapter.notifyDataSetChanged()
                        FinalPath = response.data?.get(0)?.path.toString()
                        sendSupervisorApi(response.data?.get(0)?.path, "0")
                        ToastUtils.normalShowToast(context, response.message.toString(), 2)
                        globalClass.hideLoader()

//                    when {
//
//                        response.data?.get(0)?.keyword.equals("IDDOC") -> {
////                            spDocumentType?.adapter = adapter
//                            GlobalClass.IDDOC == 1
////                            selectioncount++
////                            spDocumentType.setSelection(selectioncount)
//                        }
//                        response.data?.get(0)?.keyword.equals("NADRA") -> {
//                            GlobalClass.NADRA == 1
//                            selectioncount++
////                            spDocumentType?.adapter = adapter
////                            spDocumentType.setSelection(selectioncount)
//
//                        }
//                        response.data?.get(0)?.keyword.equals("ADDR") -> {
//                            GlobalClass.ADDR == 1
////                            spDocumentType?.adapter = adapter
////                            selectioncount++
////                            spDocumentType.setSelection(selectioncount)
//                        }
//                        response.data?.get(0)?.keyword.equals("UNSC") -> {
//                            GlobalClass.UNSC == 1
////                            spDocumentType?.adapter = adapter
//                            selectioncount++
////                            spDocumentType.setSelection(selectioncount)
//                        }
//
//
//                        response.data?.get(0)?.keyword.equals("TnC") -> {
//                            GlobalClass.TnC == 1
////                            spDocumentType?.adapter = adapter
//                            selectioncount++
//                            spDocumentType.setSelection(selectioncount)
//                        }
//                        response.data?.get(0)?.keyword.equals("POI") -> {
////                            spDocumentType?.adapter = adapter
//                            GlobalClass.POI == 1
//                            selectioncount++
////                            spDocumentType.setSelection(selectioncount)
//                        }
//                        response.data?.get(0)?.keyword.equals("EDDFILL") -> {
//                            GlobalClass.EDDFILL == 1
////                            spDocumentType?.adapter = adapter
//                            selectioncount++
////                            spDocumentType.setSelection(selectioncount)
//                        }
//                    }


//                    adapter.notifyAll()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun PartialUploadFailure(response: BaseResponse) {
                    try {
                        globalClass.hideLoader()
//                        Utils.failedAwokeCalls((activity as HawActivity)) {
//                            callUploadApi(base64image,
//                                keyword)
//                        }
                        ToastUtils.normalShowToast(activity, response.message, 1)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }
            })

//        } catch (ex: Exception) {
//            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show()
//
//        }


    }

//    private fun callSSUploadApi(base64image: MutableList<String>, keyword: String) {
////        try {
////            globalClass.showDialog(activity)
//        var request = PartialUploadRequest(
//            keyword,
//            "2893748923748_CH1QDO_@10028929-01",
//            Files(RemoteFile("2893748923748_CH1QDO_@10028929-02", base64image)),
//            "",
//            Constants.PARTIAL_SSD_UPLOAD_INDENTIFIER
//        )
//
//        HBLHRStore.instance?.partialUpload(
//            RetrofitEnums.URL_HBL,
//            request,
//            object : PartialUploadCallBack {
//                override fun PartialUploadSuccess(response: PartialUploadResponse) {
//                    IdDocImagePath.clear()
//                    IdDocImageList.clear()
//                    ToSendPathList.clear()
//                    ToSendImageList.clear()
//                    cbSelectAll.isEnabled = true
//                    cbSelectAll.isChecked = false
//                    viewModel.types[selectedDocumentTypeIndex].images.forEach {
//                        if (it.isSelected) {
//                            it.isUploaded = it.isSelected
//                            it.isSelected = false
//
//
//                        }
//
////            Log.d("hi", IdDocImageList.toString())
//                    }
//                    globalClass.hideLoader()
//                    Toast.makeText(context, response.message.toString(), Toast.LENGTH_SHORT)
//                        .show()
//
//                    if (response.data?.get(0)?.keyword.equals("SSCARD")) {
//                        GlobalClass.KFS = 1
//                        GlobalClass.SSCARD = 0
//                        spDocumentType?.adapter = adapter
//                        spDocumentType.setSelection(9)
//
//                    }
//                    adapter.notifyDataSetChanged()
//                }
//
//                override fun PartialUploadFailure(response: BaseResponse) {
//                    globalClass.hideLoader()
//
//                    ToastUtils.normalShowToast(
//                        activity!!.applicationContext,
//                        "Error in Uploading"
//                        , ""
//                    )
//                }
//            })
//
////        } catch (ex: Exception) {
////            Toast.makeText(context, "Something went wrong", Toast.LENGTH_SHORT).show()
////        }
//
//
//    }

    private fun callConfig() {
        var lovRequest = ConfigRequest()
        lovRequest.identifier = Constants.COMFIG_INDENTIFIER
        HBLHRStore.instance?.getConfig(RetrofitEnums.URL_HBL, lovRequest, object : ConfigCallBack {
            override fun ConfigSuccess(response: ConfigResponse) {
                globalClass.configData = response.data?.get(0)
            }

            override fun ConfigFailure(response: BaseResponse) {
            }
        })
    }

    private fun callDocumentType() {
        globalClass.showDialog(activity)
        var lovRequest = DocLovRequest()
        lovRequest.tracking_id =
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID
        lovRequest.identifier = Constants.DOCUMENT_TYPE_INDENTIFIER
        lovRequest._SEGCD = (activity as HawActivity).customerDemoGraphicx.CUST_TYPE
        lovRequest._SEGTY = (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE
        lovRequest._WORKFLOW = GlobalClass.workFlowCode
        lovRequest._TYPE = "S"
        HBLHRStore.instance?.getDocumentType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : DocumentTypeCallBack {
                override fun DocumentTypeSuccess(response: DocTypeResponse) {
                    try {
                        globalClass.documentType = response.data

                        globalClass.hideLoader()

//                    val adapter = response.data?.let {
//                        ArrayAdapter(
//                            activity!!.applicationContext,
//                            R.layout.view_spinner_upload,
//                            R.id.text1,
//                            it
//                        )
//+
//
//                    }


//                    loadData()

//                    response.data?.let { spDocumentType?.adapter = setDatainSpinner(it) }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }


                override fun DocumentTypeFailure(response: BaseResponse) {
                    try {
                        ToastUtils.normalShowToast(activity, response.message, 1)
//                        Utils.failedAwokeCalls((activity as HawActivity)) { callDocumentType() }
                        globalClass.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

            })
    }

    override fun onPause() {
        super.onPause()
        var fragmentManager = requireActivity().getSupportFragmentManager();
        var fragmentTransaction = fragmentManager!!.beginTransaction();
        fragmentTransaction.addToBackStack(null);
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            (activity as HawActivity).siIndicator!!.visibility = View.GONE
            (activity as HawActivity).toolbar.visibility = View.VISIBLE
            (activity as HawActivity).setSupportActionBar((activity as HawActivity).toolbar);
            (activity as HawActivity).getSupportActionBar()!!.setDisplayHomeAsUpEnabled(true);
            (activity as HawActivity).toolbar.setNavigationOnClickListener {
                (activity as HawActivity).finish()
            }
            var fragmentManager = requireActivity().getSupportFragmentManager();
            var fragmentTransaction = fragmentManager!!.beginTransaction();
            fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commitAllowingStateLoss();
            shortAnimationDuration = resources.getInteger(android.R.integer.config_shortAnimTime)

            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)

            cross_image = view.findViewById(R.id.cross_image)
            expandedImageView = view.findViewById(R.id.expanded_image)
            frameLayout = view.findViewById(R.id.container)

            cross_image.setOnClickListener(this)
            btCamera.setOnClickListener(this)
            ibUpload.setOnClickListener(this)
            ibUpload.isEnabled = true
            btSkip.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btCompleted.setOnClickListener(this)
            (activity as HawActivity).globalClass?.setDisbaled(btCompleted!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(cbSelectAll!!, false)
            cbSelectAll.setOnCheckedChangeListener(this)
            recyclerViewSetup()
            recyclerViewListenerSetup()
            canAddNewPhotos()
//        callDocumentType()
            setDatainSpinner()
            setSpinerSelection()
            setSpinnerCondition()
//        callConfig()

            try {
                var totalcount =
                    (activity as HawActivity).sharedPreferenceManager.customerDocument.doC_MAP.size
                var tagcount =
                    (activity as HawActivity).sharedPreferenceManager.customerDocument.doC_MAP[totalcount - 1].tags.size
                count =
                    (activity as HawActivity).sharedPreferenceManager.customerDocument.doC_MAP[totalcount - 1].tags[tagcount - 1]


//        adapter = DocumentUploadSpinnerAdapter(activity!!.applicationContext, viewModel.types)
//        val adapter = ArrayAdapter(activity!!.applicationContext, R.layout.view_spinner_upload, R.id.text1, types)
//        spDocumentType?.adapter = adapter
//        adapter.
//        adapter.setDropDownViewResource(R.layout.view_spinner_dropped_upload);
                spDocumentType?.onItemSelectedListener =
                    object : AdapterView.OnItemSelectedListener {
                        override fun onNothingSelected(parent: AdapterView<*>?) {

                        }

                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View?,
                            position: Int,
                            id: Long,
                        ) {
                            try {
                                val type = parent?.getItemAtPosition(position).toString()
                                selectedDocumentTypeIndex = position
                                recyclerViewSetup()
                                tvDocumentCategory.text = spDocumentType!!.getItemAtPosition(position).toString()
                                if(spDocumentType!!.getItemAtPosition(position).toString().equals("ID Document",true)){
                                    include.visibility=View.GONE
                                }else{
                                    include.visibility=View.VISIBLE
                                }
                                canAddNewPhotos()
//                pri
//                ntln(type)
                            } catch (e: UnsatisfiedLinkError) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as UnsatisfiedLinkError))
                            } catch (e: NullPointerException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as NullPointerException))
                            } catch (e: IllegalArgumentException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as IllegalArgumentException))
                            } catch (e: NumberFormatException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as NumberFormatException))
                            } catch (e: InterruptedException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as InterruptedException))
                            } catch (e: RuntimeException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as RuntimeException))
                            } catch (e: IOException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as IOException))
                            } catch (e: FileNotFoundException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as FileNotFoundException))
                            } catch (e: ClassNotFoundException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as ClassNotFoundException))
                            } catch (e: ActivityNotFoundException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as ActivityNotFoundException))
                            } catch (e: IndexOutOfBoundsException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as IndexOutOfBoundsException))
                            } catch (e: ArrayIndexOutOfBoundsException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as ArrayIndexOutOfBoundsException))
                            } catch (e: ClassCastException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as ClassCastException))
                            } catch (e: TypeCastException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as TypeCastException))
                            } catch (e: SecurityException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as SecurityException))
                            } catch (e: IllegalStateException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as IllegalStateException))
                            } catch (e: OutOfMemoryError) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as OutOfMemoryError))
                            } catch (e: RuntimeException) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity),
                                    (e as RuntimeException))
                            } catch (e: Exception) {
                                ToastUtils.normalShowToast(activity,
                                    getString(R.string.something_went_wrong),
                                    3)
                                SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                            }
                        }

                    }

                viewModel.showAvatar.value = true
                viewModel.isPhotoTaken.observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                    if (mCurrentPhotoUri != null && it) {
                        if (cbSelectAll.isChecked) {
//                    Log.d("pic1", mCurrentPhotoUri.toString())
                            viewModel.types[selectedDocumentTypeIndex].images.add(
                                CIF14Model(
                                    mCurrentPhotoUri!!,
                                    isSelected = true,
                                    isUploaded = false
                                )
                            )

                        } else {
                            viewModel.types[selectedDocumentTypeIndex].images.add(
                                CIF14Model(
                                    mCurrentPhotoUri!!,
                                    isSelected = false,
                                    isUploaded = false
                                )
                            )

                        }
                        pictureGridAdapter.notifyDataSetChanged()
                        (activity as HawActivity).globalClass?.setEnabled(cbSelectAll, true)

                        mCurrentPhotoPath = null
                    }

                })
                viewModel.totalSteps.value = 5
                viewModel.currentFragmentIndex.value = 1
            } catch (E: Exception) {
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        try {
            super.onActivityResult(requestCode, resultCode, data)
            if (requestCode == 1010 && resultCode != -1) {
//            viewModel.types[selectedDocumentTypeIndex].images.removeAt(viewModel.types[selectedDocumentTypeIndex].images.size-1)
                mCurrentPhotoUri
                val file = File(mCurrentPhotoUri!!.path)
                file.delete()
            } else {
                viewModel.isPhotoTaken.value = true
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setSpinnerCondition() {
        spDocumentType?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {


            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
    }

    private fun setSpinerSelection() {
        var isMatched = false
        for (i in 0 until doctypedupS.size) {
            if (doctypedupS[i].PARTIAL_KEY == true) {
                spDocumentType?.setSelection(i + 1)
                if (i + 1 < doctypedupS.size) {
                    isMatched = true
                    spDocumentType?.setSelection(i + 1)
                }
            }
        }
        if (!isMatched) {
            completeDocumentAlert((activity as HawActivity),"All documents have been uploaded\nPress \'Continue\' to proceed")
        }
        GlobalClass.selectedItem = spDocumentType.selectedItemPosition
    }

    fun completeDocumentAlert(context: Context, message: String) {
        val alertDialog =
            AlertDialog.Builder(context)
        alertDialog.setTitle("Congratulations")
        alertDialog.setMessage(message)
        alertDialog.setCancelable(false)
        alertDialog.setPositiveButton(
            "Continue"
        ) { dialog, which -> btCompleted.callOnClick() }
        alertDialog.show()
    }


    private fun setDatainSpinner() {
        try {
            (activity as HawActivity).sharedPreferenceManager.partialDocs.MANDATORY_DATA!!.let {
                doctypedupS = it
            }


            (activity as HawActivity).sharedPreferenceManager.partialDocs.LOAD_PARTIAL_OLD_IMAGES_BUFFER!!.let {
                custImageBufferOld = it
                custImageBuffer = it
            }

            SuccessCounter = (activity as HawActivity).sharedPreferenceManager.partialDocs.LOAD_PARTIAL_OLD_IMAGES_BUFFER!!.size
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.PARTIAL_DOCS.MANDATORY_DATA!!.let {
                globalClass.documentType = it
            }
            var dataAdapter=(activity as HawActivity).sharedPreferenceManager.aofAccountInfo.PARTIAL_DOCS.MANDATORY_DATA!!
//            for(x in dataAcomdapter){
//                if(!x.KEYWORD.isNullOrEmpty()) {
//                    if (x.KEYWORD!!.contains("KFS")) {
//                        dataAdapter.remove(x)
//                    }
//                }
//            }
            adapter = CustomDocumentUploadSpinnerAdapter(
                (activity as HawActivity),
                dataAdapter!!
            )
            spDocumentType?.adapter = adapter
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun loadData() {
        var IDDOC = 0
        var NADRA = 0
        var ADDR = 0
        var UNSC = 0
        var TnC = 0
        var POI = 0
        var EDDFILL = 0
        var SSCARD = 0
        var KFS = 0
        if (!(activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST.isNullOrEmpty()) {
            for (i in 0 until globalClass.documentType!!.size) {
                if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "IDDOC") {
                    IDDOC = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "NADRA") {
                    NADRA = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "ADDR") {
                    ADDR = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "UNSC") {
                    UNSC = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "TnC") {
                    TnC = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "POI") {
                    POI = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "AOFCIF") {
                    AOFCIF = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "EDDFILL") {
                    EDDFILL = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "SSCARD") {
                    SSCARD = 1
                } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.DOC_CHECKLIST[i].dockeyword == "KFS") {
                    KFS = 1
                }


                Log.d(
                    "cc",
                    IDDOC.toString() + ADDR.toString() + TnC.toString() + POI.toString() + EDDFILL.toString()
                )
            }
        }

    }


    fun encoder(filePath: MutableList<String>) {

        try {
            for (i in 0 until filePath.size) {
                var bmpOriginal = MediaStore.Images.Media.getBitmap(
                    context?.contentResolver, Uri.parse(
                        filePath[i]
                    )
                )
                val width: Int
                val height: Int
                height = bmpOriginal.getHeight()
                width = bmpOriginal.getWidth()
                val bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565)
                val c = Canvas(bmpGrayscale)
                val paint = Paint()
                val cm = ColorMatrix()
                cm.setSaturation(0.toFloat())
                val f = ColorMatrixColorFilter(cm)
                paint.setColorFilter(f)
                c.drawBitmap(bmpOriginal, 0.toFloat(), 0.toFloat(), paint)

                val byteArrayOutputStream = ByteArrayOutputStream()
                bmpGrayscale.compress(Bitmap.CompressFormat.JPEG, 60, byteArrayOutputStream)
                val byteArray = byteArrayOutputStream.toByteArray()
                val encoded = Base64.encodeToString(byteArray, Base64.DEFAULT)

//            val bytes = File(filePath[i]).readBytes()
                IdDocImageList.add(
                    encoded.toString()
                    //                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                    Base64.getEncoder().encodeToString(bytes)
//                } else {
//                    ToastUtils.normalShowToast(activity!!.applicationContext, "Not O").toString()
//                }
                )
//            var base64 = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//                Base64.getEncoder().encodeToString(bytes)
//            } else {
//                ToastUtils.normalShowToast(activity!!.applicationContext, "Not O").toString()
//            }
            }
        } catch (e: Exception) {

        }
    }

    override fun onDestroy() {
        super.onDestroy()

    }

    private fun recyclerViewSetup() {
        pictureGridAdapter = PictureGridAdapter(viewModel.types[selectedDocumentTypeIndex].images)
        rvPictures.apply {
            layoutManager = GridLayoutManager(requireContext(), 3)
            adapter = pictureGridAdapter
        }
        spDocumentType
//        tvDocumentCategory.text = viewModel.types[selectedDocumentTypeIndex].category + "  "
        setCompleteButtonState()
    }

    private fun setNextButtonState() {
        btNext.isEnabled = viewModel.types.none { it.state == DocumentCategoryState.Mandatory }
    }

    private fun setCompleteButtonState() {
        btCompleted.isEnabled = buttonEnable == true
//            viewModel.types[selectedDocumentTypeIndex].images.any { it.isUploaded }
//        btCompleted.isEnabled =
//             if(spDocumentType.selectedItemPosition == )
        if (btCompleted.isEnabled) {
            (activity as HawActivity).globalClass?.setEnabled(btCompleted!!, true)
            (activity as HawActivity).globalClass?.setDisbaled(ibUpload!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(btCamera!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(spDocumentType!!, false)
            viewModel.types[selectedDocumentTypeIndex].images.clear()
            btCamera.isEnabled = false
            btCamera.setAlpha(100)
            ibUpload.isEnabled = false
            ibUpload.setAlpha(100)
            btSkip.isEnabled = false
        }
    }

    private fun recyclerViewListenerSetup() {

        rvPictures.addOnItemTouchListener(
            RecyclerTouchListener(
                requireContext(),
                rvPictures,
                object : RecyclerTouchListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        try {
                            if (inSelectionMode) {
                                if (!viewModel.types[selectedDocumentTypeIndex].images[position].isUploaded && !cbSelectAll.isChecked) {
                                    viewModel.types[selectedDocumentTypeIndex].images[position].isSelected =
                                        !viewModel.types[selectedDocumentTypeIndex].images[position].isSelected
                                    if (!viewModel.types[selectedDocumentTypeIndex].images[position].isSelected) {
                                        try {
                                            ToSendPathList.removeAt(position)
                                        } catch (ex: Exception) {
                                        }
                                    } else {
                                        ToSendPathList.add(IdDocImagePath[position])

                                    }
                                    pictureGridAdapter.notifyDataSetChanged()
                                    setUploadCount()
                                }


                            } else {
                                zoomImageFromThumb(
                                    view,
                                    viewModel.types[selectedDocumentTypeIndex].images[position].imageURI
                                )

                            }
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as UnsatisfiedLinkError))
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as NullPointerException))
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IllegalArgumentException))
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as NumberFormatException))
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as InterruptedException))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as RuntimeException))
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as FileNotFoundException))
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ClassNotFoundException))
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ActivityNotFoundException))
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IndexOutOfBoundsException))
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ArrayIndexOutOfBoundsException))
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ClassCastException))
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as TypeCastException))
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as SecurityException))
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IllegalStateException))
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as OutOfMemoryError))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as RuntimeException))
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {
                        try {
                            if (!viewModel.types[selectedDocumentTypeIndex].images[position].isUploaded && !cbSelectAll.isChecked) {
                                viewModel.types[selectedDocumentTypeIndex].images[position].isSelected =
                                    !viewModel.types[selectedDocumentTypeIndex].images[position].isSelected
//                            ToSendPathList.add(IdDocImagePath[position])

                                if (!viewModel.types[selectedDocumentTypeIndex].images[position].isSelected) {
                                    try {
                                        ToSendPathList.removeAt(position)
                                    } catch (ex: Exception) {
                                    }
                                } else {

                                    ToSendPathList.add(IdDocImagePath[position])
                                }
                            }
                            pictureGridAdapter.notifyDataSetChanged()
                            setUploadCount()
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as UnsatisfiedLinkError))
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as NullPointerException))
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IllegalArgumentException))
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as NumberFormatException))
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as InterruptedException))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as RuntimeException))
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as FileNotFoundException))
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ClassNotFoundException))
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ActivityNotFoundException))
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IndexOutOfBoundsException))
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ArrayIndexOutOfBoundsException))
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as ClassCastException))
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as TypeCastException))
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as SecurityException))
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as IllegalStateException))
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as OutOfMemoryError))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity),
                                (e as RuntimeException))
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(activity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                        }
                    }


                })
        )
    }

    private fun selectAllImages() {
        viewModel.types[selectedDocumentTypeIndex].images.forEach {
            if (!it.isUploaded) {
                it.isSelected = true



                pictureGridAdapter.notifyDataSetChanged()

            }
        }
//        cbSelectAll.isEnabled = false
        setUploadCount()
    }

    private fun unselectAllImages() {
        viewModel.types[selectedDocumentTypeIndex].images.forEach {
            if (!it.isUploaded) {
                it.isSelected = false
//            IdDocImagePath.clear()
//            IdDocImageList.clear()
                ToSendPathList.clear()
                ToSendImageList.clear()
                pictureGridAdapter.notifyDataSetChanged()

            }
        }
        setUploadCount()
    }

    private fun setUploadCount() {
        if (viewModel.types[selectedDocumentTypeIndex].images.any { it.isSelected }) {
            tvUploadCount.text =
                getString(R.string.upload) + " (" + viewModel.types[selectedDocumentTypeIndex].images.filter { it.isSelected }.size + ")"
            inSelectionMode = true
//            ibUpload.isEnabled = true

        } else {
            tvUploadCount.text = getString(R.string.upload)
            inSelectionMode = false


        }

    }

    fun canAddNewPhotos() {
        val state =
            viewModel.types[selectedDocumentTypeIndex].state == DocumentCategoryState.OptionalCompleted ||
                    viewModel.types[selectedDocumentTypeIndex].state == DocumentCategoryState.MandatoryCompleted
        if (state) {
            clDocCatAction.visibility = GONE
        } else {
            clDocCatAction.visibility = VISIBLE
        }
//        btCamera.isEnabled = !state
//        ibUpload.isEnabled = !state
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.cross_image -> container.visibility = View.INVISIBLE
            R.id.btCamera -> checkForPermissions()
            R.id.ibUpload -> {
                try {
                    if (viewModel.types[selectedDocumentTypeIndex].images.any { it.isSelected }) {
                        globalClass.showDialog(activity)

                        Timer().schedule(100) {
                            //do something
                            postitionBeforeSelection = spDocumentType.selectedItemPosition
                            uploadImage()
                        }

                    } else {
                        ToastUtils.normalShowToast(context, "Please select one or more images ", 1)

                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
            R.id.btSkip -> confirmSkipUpload()
            R.id.btCompleted -> {
                try {
                    for (i in 0 until doctypedupS.size) {
                        doctypedupS[i].PARTIAL_KEY = false
                    }
                    globalClass.showDialog(activity)
                    isComplete = true
                    sendSupervisorApi(FinalPath, "1")  // replace
//                if (viewModel.types[selectedDocumentTypeIndex].state != DocumentCategoryState.MandatoryCompleted &&
//                    viewModel.types[selectedDocumentTypeIndex].state != DocumentCategoryState.OptionalCompleted
//                ) {
//                    confirmCompletedUpload()
//                }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
//            R.id.btNext -> /*confirmNext()*/


        }

    }

    private fun confirmSkipUpload() {
        (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(requireContext(),
            "Do you want to skip uploading documents?",
            "After Selecting YES, you can not upload more documents.",
            "No",
            "Yes",
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
//                    saveAndNext("1")
                    findNavController().navigate(R.id.action_CIFStep14_to_prooceedToSummary)

                }

                override fun onPositiveClicked() {
                    super.onPositiveClicked()
                }
            })
    }

    private fun updateCategoryState() {
        if (viewModel.types[selectedDocumentTypeIndex].state == DocumentCategoryState.Optional) {
            viewModel.types[selectedDocumentTypeIndex].state =
                DocumentCategoryState.OptionalCompleted
        } else if (viewModel.types[selectedDocumentTypeIndex].state == DocumentCategoryState.Mandatory) {
            viewModel.types[selectedDocumentTypeIndex].state =
                DocumentCategoryState.MandatoryCompleted

        }
    }

    private fun confirmCompletedUpload() {
        (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(requireContext(),
            "Have you finished uploading documents?",
            "Are you sure you have uploaded all documents?",
            "No",
            "Yes",
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
//                    updateCategoryState()
//                    setNextButtonState()
//                    canAddNewPhotos()
//                    adapter.notifyDataSetChanged()
//                    sendSupervisorApi(response.data.get(0).path.toString())

//                    saveAndNext(flag)
                }

                override fun onPositiveClicked() {
                    super.onPositiveClicked()
                }
            })
    }

    private fun saveAndNext(flag: String?) {


        (activity as HawActivity).customerDocument = MainCustDocumentsList[0]
        (activity as HawActivity).docCheckList = docCheckListList
        (activity as HawActivity).sharedPreferenceManager.setCustomerDocument((activity as HawActivity).customerDocument)
        (activity as HawActivity).sharedPreferenceManager.setDocCheckList((activity as HawActivity).docCheckList)

        (activity as HawActivity).customerDocument.let {
            if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.CUST_DOCUMENTS.size >= 1) {
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.CUST_DOCUMENTS.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DOCUMENTS.add(
                    it
                )
            }

        }
        (activity as HawActivity).docCheckList.let {
            (activity as HawActivity).aofAccountInfoRequest.DOC_CHECKLIST.addAll(it)
        }

        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }


    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun confirmNext() {
        isComplete = false
        (requireContext().applicationContext as GlobalClass).showConfirmationDialog(
            requireContext(),
            "Have you finished uploading documents?",
            "After Selecting YES, you can not upload more documents.",
            "No",
            "Yes",
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
                    findNavController().navigate(R.id.action_CIFStep14_to_CIFStep15)
                }

                override fun onPositiveClicked() {
                    super.onPositiveClicked()
                }
            })
    }

    private fun uploadImage() {

//        try {
//        var base64image  = encoder(filePath)
        var keyword = ""
        var selecteditem: String = spDocumentType.selectedItem.toString()
        for (i in 0 until globalClass.documentType!!.size) {
            when (selecteditem) {
                globalClass.documentType!!.get(i).DOCDS -> {
//                Log.d("size", ToSendPathList.toString())
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()

                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.PARTIAL_DOCS.MANDATORY_DATA!![i] =
                        doctypedupS[i]
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!
                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()

                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }



                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList[0] = custDocument
                    }


                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 1
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 1
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
//                        docCheckListList.add(docChecklist)
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }

                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)


//                    custImageBufferOld.addAll(custImageBuffer)

//                    IDDOC = 1


                }
                globalClass.documentType!!.get(i).DOCDS -> {
//                Log.d("size", ToSendPathList.toString())
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()

                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.PARTIAL_DOCS.MANDATORY_DATA!![i] =
                        doctypedupS[i]
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!
                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }
//                        custDocumentsList?.add(custDocument)
                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }

                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 1
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 1
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
//                        docCheckListList.add(docChecklist)
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }

                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)
//                    custImageBufferOld.addAll(custImageBuffer)

                }
                globalClass.documentType!!.get(i).DOCDS -> {
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()
                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!

                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }//                        custDocumentsList?.add(custDocument)

                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }

                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 1
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 1
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
//                        docCheckListList.add(docChecklist)
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }

                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)
//                    custImageBufferOld.addAll(custImageBuffer)

                }
                globalClass.documentType!!.get(i).DOCDS -> {
//                Log.d("size", ToSendPathList.toString())
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()
                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!

                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }//                        custDocumentsList?.add(custDocument)
                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }
                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 1
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 1
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
//                        docCheckListList.add(docChecklist)
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }

                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)
//                    custImageBufferOld.addAll(custImageBuffer)

                }
                globalClass.documentType!!.get(i).DOCDS -> {
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()
                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!

                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }



                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }



                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }

                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 2
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 2
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }
                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)
//                    custImageBufferOld.addAll(custImageBuffer)

                }
                globalClass.documentType!!.get(i).DOCDS -> {
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()
                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!

                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }



                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }

                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 1
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 1
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }
                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)
//                    custImageBufferOld.addAll(custImageBuffer)

                }
                globalClass.documentType!!.get(i).DOCDS -> {
//                Log.d("size", ToSendPathList.toString())
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()
                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!

                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }



                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }

                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 1
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 1
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }
                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)

//                    custImageBufferOld.addAll(custImageBuffer)
                }
                globalClass.documentType!!.get(i).DOCDS -> {
//                Log.d("size", ToSendPathList.toString())
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()
                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!

                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }



                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }

                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 1
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 1
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }

                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)

//                    custImageBufferOld.addAll(custImageBuffer)
                }
                globalClass.documentType!!.get(i).DOCDS -> {
//                Log.d("size", ToSendPathList.toString())
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()
                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!

                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }



                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }

                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 2
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 2
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }
                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)
//                    custImageBufferOld.addAll(custImageBuffer)

                }
                globalClass.documentType!!.get(i).DOCDS -> {
//                Log.d("size", ToSendPathList.toString())
                    if (cbSelectAll.isChecked) {
                        encoder(IdDocImagePath)
                    } else {
                        encoder(ToSendPathList)

                    }
//                Log.d("hi", IdDocImageList.toString())
                    var custDocument = CustDocument()
                    var docChecklist = DOCCHECKLIST()
                    var iBuffer = ImageBuffer()
                    doctypedupS[i - 1].PARTIAL_KEY = false
                    doctypedupS[i].PARTIAL_KEY = true
                    doctypedupS[i].PR_Doc_BTN_NAME = "File Uploaded"
                    custDocument.keyword = globalClass.documentType!!.get(i).KEYWORD
                    keyword = globalClass.documentType!!.get(i).KEYWORD!!
                    custDocument.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    for (i in 0 until IdDocImageList.size) {
                        count++
                        custDocument.tags?.add(count)
                    }



                    if (custDocumentsList.size == 0) {
                        custDocumentsList.add(0, custDocument)
                    } else if (custDocumentsList.size >= 1) {
                        custDocumentsList.set(0, custDocument)
                    }

                    docChecklist.docname = globalClass.documentType!!.get(i).DOCDS
                    docChecklist.dockeyword = globalClass.documentType!!.get(i).KEYWORD
                    docChecklist.seqno = globalClass.documentType!!.get(i).SEQ_NO?.toInt()
                    docChecklist.mandt = "M"
                    docChecklist.doccount = 1
                    docChecklist.additionaldoccount = 0
                    docChecklist.new = 1
                    docChecklist.uploaded = 1
                    docChecklist.docrequired = false
                    if (docCheckListList.size == 0) {
                        docCheckListList.add(0, docChecklist)
                    } else if (docCheckListList.size >= 1) {
                        docCheckListList.set(0, docChecklist)
                    }
                    iBuffer.id = countid++
                    iBuffer.istagged = true
                    iBuffer.taggedBy = globalClass.documentType!!.get(i).KEYWORD
                    iBuffer.seq = globalClass.documentType!!.get(i).SEQ_NO?.toInt()!!
                    custImageBuffer.add(iBuffer)

//                    custImageBufferOld.addAll(custImageBuffer)
                }


            }

        }


//        }


//        viewModel.types[selecteddocumentType!!Index].images.forEach {
//            if (it.isSelected) {
////                it.isUploaded = it.isSelected
//                it.isSelected = false
//
//
//            }
//
////            Log.d("hi", IdDocImageList.toString())
//        }
        Handler(Looper.getMainLooper()).post(object : Runnable {
            public override fun run() {
                pictureGridAdapter.notifyDataSetChanged()
                setUploadCount()
                setCompleteButtonState()
            }
        })

//        setCompleteButtonState()

//        Log.d("cus", custDocumentsList.toString())
//        Log.d("cus", docCheckListList.toString())

//        } catch (ex: Exception) {
//            Toast.makeText(context, ex.toString(), Toast.LENGTH_SHORT).show()
//
//
//        }

        if (cbSelectAll.isChecked) {
            callUploadApi(IdDocImageList, keyword)
        } else {
            callUploadApi(IdDocImageList, keyword)
        }
    }

    private fun checkPermission() {

        if ((activity as AppCompatActivity).checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            checkForPermissions()
        } else {
            askPermission()
        }

    }

    private fun askPermission() {
        // Permission has not been granted and must be requested.
        if ((activity as AppCompatActivity).shouldRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            // Provide an additional rationale to the user if the permission was not granted

            (activity as AppCompatActivity).requestAllPermissions(
                permissions,
                PERMISSION_REQUEST_STORAGE
            )

        } else {
            // Request the permission with array.
            (activity as AppCompatActivity).requestAllPermissions(
                permissions,
                PERMISSION_REQUEST_STORAGE
            )
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<out String>, grantResults: IntArray,
    ) {
        try {
            if (requestCode == PERMISSION_REQUEST_STORAGE) {
                // Request for camera permission.
                if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    checkForPermissions()
                } else {
                    // Permission request was denied.
                }
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }


//    override fun onResume() {
//        super.onResume()
//        if (!ibUpload.isEnabled) {
//            (activity as HawActivity).globalClass?.setEnabled(btCompleted, true)
//            viewModel.types[selecteddocumentType!!Index].images.clear()
//
//
//        } else {
//            (activity as HawActivity).globalClass?.setDisbaled(btCompleted, false)
//
//        }
//    }

    private fun checkForPermissions() {
        Permissions.check(
            activity /*context*/,
            permissions,
            null /*rationale*/,
            null /*options*/,
            object : PermissionHandler() {
                override fun onGranted() {
                    // do your task.
                    openCamera()
                }

                override fun onDenied(
                    context: Context?,
                    deniedPermissions: java.util.ArrayList<String?>?,
                ) {
                    ToastUtils.normalShowToast(context, "Permission denied", 1)
                }
            })
    }

    @SuppressLint("UseRequireInsteadOfGet")
    private fun openCamera() {
        try {
            val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val file = createImageFile()
            filePath = file.toString()
            if (file != null) {
                val uriSavedImage: Uri = FileProvider.getUriForFile(
                    requireContext(),
                    BuildConfig.APPLICATION_ID + ".provider", file
                )
                mCurrentPhotoUri = uriSavedImage
                IdDocImagePath.add(uriSavedImage.toString())
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, uriSavedImage)
                (activity as HawActivity).startActivityForResult(cameraIntent, 1010)
            }
        } catch (ex: Exception) {
            ToastUtils.normalShowToast(context, ex.toString(), 1)
        }
    }


    @Throws(IOException::class)
    private fun createImageFile(): File? {
        // Create an image file name
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = File(
            Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_DCIM
            ), "Camera"
        )
        if (!storageDir.exists()) {
            storageDir.mkdir()
        }
        val image = File.createTempFile(
            imageFileName,  /* prefix */
            ".jpg",  /* suffix */
            storageDir /* directory */
        )
        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.absolutePath
        return image
    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if (isChecked) {
            selectAllImages()
        } else {
            unselectAllImages()
        }
    }

    private fun zoomImageFromThumb(thumbView: View, uri: Uri) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        currentAnimator?.cancel()

        // Load the high-resolution "zoomed-in" image.

        expandedImageView.setImageURI(uri)

        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        val startBoundsInt = Rect()
        val finalBoundsInt = Rect()
        val globalOffset = Point()

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBoundsInt)
        frameLayout
            .getGlobalVisibleRect(finalBoundsInt, globalOffset)
        startBoundsInt.offset(-globalOffset.x, -globalOffset.y)
        finalBoundsInt.offset(-globalOffset.x, -globalOffset.y)

        val startBounds = RectF(startBoundsInt)
        val finalBounds = RectF(finalBoundsInt)

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        val startScale: Float
        if ((finalBounds.width() / finalBounds.height() > startBounds.width() / startBounds.height())) {
            // Extend start bounds horizontally
            startScale = startBounds.height() / finalBounds.height()
            val startWidth: Float = startScale * finalBounds.width()
            val deltaWidth: Float = (startWidth - startBounds.width()) / 2
            startBounds.left -= deltaWidth.toInt()
            startBounds.right += deltaWidth.toInt()
        } else {
            // Extend start bounds vertically
            startScale = startBounds.width() / finalBounds.width()
            val startHeight: Float = startScale * finalBounds.height()
            val deltaHeight: Float = (startHeight - startBounds.height()) / 2f
            startBounds.top -= deltaHeight.toInt()
            startBounds.bottom += deltaHeight.toInt()
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.alpha = 0f
        frameLayout.visibility = VISIBLE
        expandedImageView.visibility = View.VISIBLE

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.pivotX = 0f
        expandedImageView.pivotY = 0f

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        currentAnimator = AnimatorSet().apply {
            play(
                ObjectAnimator.ofFloat(
                    expandedImageView,
                    View.X,
                    startBounds.left,
                    finalBounds.left
                )
            ).apply {
                with(
                    ObjectAnimator.ofFloat(
                        expandedImageView,
                        View.Y,
                        startBounds.top,
                        finalBounds.top
                    )
                )
                with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X, startScale, 1f))
                with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y, startScale, 1f))
            }
            duration = shortAnimationDuration.toLong()
            interpolator = DecelerateInterpolator()
            addListener(object : AnimatorListenerAdapter() {

                override fun onAnimationEnd(animation: Animator) {
                    currentAnimator = null
                }

                override fun onAnimationCancel(animation: Animator) {
                    currentAnimator = null
                }
            })
            start()
        }

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        expandedImageView.setOnClickListener {
            currentAnimator?.cancel()

            // Animate the four positioning/sizing properties in parallel,
            // back to their original values.
            currentAnimator = AnimatorSet().apply {
                play(ObjectAnimator.ofFloat(expandedImageView, View.X, startBounds.left)).apply {
                    with(ObjectAnimator.ofFloat(expandedImageView, View.Y, startBounds.top))
                    with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X, startScale))
                    with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_Y, startScale))
                }
                duration = shortAnimationDuration.toLong()
                interpolator = DecelerateInterpolator()
                addListener(object : AnimatorListenerAdapter() {

                    override fun onAnimationEnd(animation: Animator) {
                        thumbView.alpha = 1f
                        frameLayout.visibility = View.GONE
                        expandedImageView.visibility = View.GONE
                        currentAnimator = null
                    }

                    override fun onAnimationCancel(animation: Animator) {
                        thumbView.alpha = 1f
                        frameLayout.visibility = View.GONE
                        expandedImageView.visibility = View.GONE
                        currentAnimator = null
                    }
                })
                start()
            }
        }
    }
}

