package com.hbl.bot.ui.adapter

import android.content.Context
import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import com.hbl.bot.R
import com.hbl.bot.model.DocumentCategoryState
import com.hbl.bot.model.ModelDocumentUpload


class DocumentUploadSpinnerAdapter(
    var context: Context,
    var documentUpload: Array<ModelDocumentUpload>

) : BaseAdapter(), SpinnerAdapter {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view = View.inflate(context, R.layout.view_spinner_upload, null)
        val textView = view.findViewById<TextView>(R.id.text1)
        textView.text = documentUpload[position].category
//        if (MainActivity.selectedCar == position) {
//            textView.setTextColor(Color.parseColor("#bdbdbd"))
//        }

        return view
    }

    override fun getItem(position: Int): Any {
        return documentUpload[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()

    }

    override fun getCount(): Int {
        return documentUpload.size
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {

        var view: View = View.inflate(context, R.layout.view_spinner_dropped_upload, null)
        val textView = view.findViewById<TextView>(R.id.text1)
        val tvCompleted = view.findViewById<TextView>(R.id.text2)
        tvCompleted.text = ""
        textView.text = documentUpload[position].category

        when (documentUpload[position].state) {
            DocumentCategoryState.OptionalCompleted -> {
                tvCompleted.text = "Completed"
            }
            DocumentCategoryState.MandatoryCompleted -> {
                tvCompleted.text = "Completed"
                textView.setTextColor(Color.RED)
            }
            DocumentCategoryState.Mandatory -> {
                textView.setTextColor(Color.RED)
            }
            DocumentCategoryState.Optional -> {
                textView.setTextColor(Color.BLACK)

            }

        }
        return view
    }


}