package com.hbl.bot.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hbl.bot.R
import com.hbl.bot.network.models.response.baseRM.DocumentType
import kotlinx.android.synthetic.main.row_document_upload.view.*

class DocumentUploadAdapter(mList: ArrayList<DocumentType>) :
    RecyclerView.Adapter<DocumentUploadAdapter.MyViewHolder>() {
    var mList: ArrayList<DocumentType> = mList

    open inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val documentDesc = view.documentDesc!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_document_upload, parent, false)
        return MyViewHolder(layout)
    }

    override fun getItemViewType(position: Int): Int {
        return mList.size
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val list = mList[position]
        holder.documentDesc.setText(list.DOCDS)
    }

}