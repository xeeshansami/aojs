package com.hbl.bot.ui.fragments.accounts

import java.io.FileNotFoundException
import java.io.IOException
import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.DebitCardLovRequest
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.CardProduct
import com.hbl.bot.network.models.response.baseRM.PrefComMode
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep7_4.*
import kotlinx.android.synthetic.main.fragment_cifstep7_4.btBack
import kotlinx.android.synthetic.main.fragment_cifstep7_4.btNext
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import kotlinx.android.synthetic.main.view_form_selection.view.*
import java.lang.ClassCastException
import java.util.concurrent.Executors

/**
 * A simple [Fragment] subclass.
 */
class HawAccount4Fragment37 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivHBLNisaSP: Spinner? = null
    var fivCardRequirdSP: Spinner? = null
    var fivCardTypeSP: Spinner? = null
    var NameOfCardET: EditText? = null
    var fivModeSP: Spinner? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep7_4, container, false)
                (activity as HawActivity).globalClass?.showDialog(activity)

            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(5)
        viewModel.currentFragmentIndex.setValue(3)
        val txt = resources.getString(R.string.account)
        val txt1 = " (4/5 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            header()
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            init()
            disabilityFields()
            load()
            /* if (GlobalClass.isDummyDataTrue) {
                 loadDummyData()
             }*/
            setLengthAndTpe()
            setConditions()
            onBackPress(view)
//            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setConditions() {
        if ((activity as HawActivity).sharedPreferenceManager.customerInfo.gender == "F"
            && (activity as HawActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK" &&
            (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
        ) {
//            fivHBLNisaSP?.setSelection(1)
        } else if ((activity as HawActivity).sharedPreferenceManager.customerInfo.gender == "M"
        ) {
//            fivHBLNisaSP?.setSelection(0)
//            (activity as HawActivity).globalClass?.setDisbaled(fivHBLNisaSP!!)
        } else {
//            fivHBLNisaSP?.setSelection(0)
//            (activity as HawActivity).globalClass?.setEnabled(fivHBLNisaSP!!, true)
        }

        fivCardRequirdSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (fivCardRequirdSP?.selectedItem.toString() == "No") {
                        (activity as HawActivity).globalClass?.setDisbaled(fivCardTypeSP!!)
                        fivCardTypeSP?.setSelection(0)
                        (activity as HawActivity).globalClass?.setDisbaled(NameOfCardET!!)
                        NameOfCardET?.setText("")
                        fivCardType.mandateSpinner.text = ""
                        NameOfCard.mandateInput.text = ""
                        fivMode.mandateSpinner.text = ""
                    } else if (fivCardRequirdSP?.selectedItem.toString() == "Yes") {
                        (activity as HawActivity).globalClass?.setEnabled(fivCardTypeSP!!, true)
                        (activity as HawActivity).globalClass?.setEnabled(NameOfCardET!!, true)
                        fivCardType.mandateSpinner.text = "*"
                        NameOfCard.mandateInput.text = "*"
                        fivMode.mandateSpinner.text = "*"
                        callCardProducts()
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }


        fivHBLNisaSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                if ((activity as HawActivity).sharedPreferenceManager.customerAccount.ACCT_SNG_JNT != "02" &&
                    ((activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
                            || (activity as HawActivity).customerAccounts.ACCT_CCY == "PKR")
                ) {

                    if (fivHBLNisaSP?.selectedItem.toString() == "Yes") {
                        if ((activity as HawActivity).sharedPreferenceManager.customerInfo.gender != "F"
                            && (activity as HawActivity).sharedPreferenceManager.customerInfo.NATIONALITY != "PK"
                            && (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES != "PK"
                        ) {

                            ToastUtils.normalShowToast(
                                context,
                                "For Nisa Your Gender Should be Female or Nationality should be Pakistan",
                                1
                            )
                        } else {
                            callCardProducts()


                        }

                    } else if (fivHBLNisaSP?.selectedItem.toString() == "No") {
                        callCardProducts()
                    }
                }


            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
    }


    fun setItemForCardProducts(item: ArrayList<CardProduct>) {
        try {
            var obj = CardProduct()
            resources.getString(R.string.select).let {
                obj.PRD_CODE = "0"
                obj.PRD_NAME = it
            }
            if (item.size != 0 && !item[0].PRD_NAME.equals(resources.getString(R.string.select))) {
                item.add(0, obj)
            }
            val adapter: ArrayAdapter<CardProduct> = object : ArrayAdapter<CardProduct>(
                activity as HawActivity, R.layout.view_spinner_item, R.id.text1, item
            ) {
                override fun isEnabled(position: Int): Boolean {
                    if ((activity as HawActivity).customerAccounts.PRITY_CODE == "N" && !(activity as HawActivity).customerAccounts.PRITY_CODE.isNullOrEmpty()) {
                        if ((item[position].PRD_CODE == "D9" || item[position].PRD_CODE == "H1")) {
                            return false
                        } else {
                            return true
                        }
                    } else {
                        return true
                    }
                }

                override fun getDropDownView(
                    position: Int, convertView: View?,
                    parent: ViewGroup,
                ): View {
                    val view = super.getDropDownView(position, convertView, parent)
                    val tv = view.findViewById<View>(R.id.text1) as TextView
                    if ((activity as HawActivity).customerAccounts.PRITY_CODE == "N" && !(activity as HawActivity).customerAccounts.PRITY_CODE.isNullOrEmpty()) {
                        if ((item[position].PRD_CODE == "D9" || item[position].PRD_CODE == "H1")) {
                            tv.setTextColor(Color.GRAY)
                            view.setBackgroundColor(resources.getColor(R.color.disabled))
                        }
                    } else {
                        tv.setTextColor(resources.getColor(R.color.green))
                    }
                    return view
                }
            }
            adapter.setDropDownViewResource(R.layout.view_spinner_list)
            fivCardTypeSP?.adapter = adapter
//        adapter.notifyDataSetChanged()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setCardProduct(it: ArrayList<CardProduct>) {
        try {
            setItemForCardProducts(it)
            (activity as HawActivity).cardProductList = it
            (activity as HawActivity).sharedPreferenceManager.lovCardType = it
            (activity as HawActivity).customerAccounts.DEBITCARDTYPEDESC.let { item ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromecardTypeIndex(
                    it,
                    item
                ).let {
                    if (it != 0) {
                        fivCardTypeSP?.setSelection(it!!)
                    } else {
                        var list = (activity as HawActivity).sharedPreferenceManager.hawKeys
                        for (x in 0 until list.size) {
                            if (list[x].IDENTIFIER == Constants.DEBIT_CARD_IDENTIFIER && list[x].NAME == (activity as HawActivity).customerAccounts.ACCT_TYPE) {
                                (activity as HawActivity).globalClass?.findSpinnerPositionFromCardTypeIndex(
                                    (activity as HawActivity).sharedPreferenceManager.lovCardType,
                                    list[x].LOV_CODE!!
                                ).let {
                                    fivCardTypeSP?.setSelection(it!!)
                                }
                            }
                        }
                    }
                }
            }

            (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setLengthAndTpe() {
        (activity as HawActivity).globalClass?.edittextTypeCount(
            NameOfCardET!!,
            19,
            Constants.INPUT_TYPE_ALPHA_BLOCK
        )

    }

    fun init() {
        fivHBLNisaSP = fivHBLNisa.getSpinner(R.id.HBLNisaID)
        fivCardRequirdSP = fivCardRequird.getSpinner(R.id.fivCardRequirdID)
        fivCardTypeSP = fivCardType.getSpinner(R.id.cardTypeID)
        NameOfCardET = NameOfCard.getTextFromEditText(R.id.nameOFCart)
        fivModeSP = fivMode.getSpinner(R.id.fibModeID)
    }

    private fun disabilityFields() {
//        if((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
        (activity as HawActivity).globalClass?.setDisbaled(fivHBLNisaSP!!, false)
        fivHBLNisaSP?.setSelection(0)
//            (activity as HawActivity).globalClass?.setDisbaled(fivCardRequirdSP!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fivCardTypeSP!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(NameOfCardET!!,false)
        (activity as HawActivity).globalClass?.setDisbaled(fivModeSP!!, false)
//        }
    }

    fun loadDummyData() {
//        NameOfCardET?.setText("Debit Card")

    }

    fun load() {
        (activity as HawActivity).customerAccounts.NAME_ON_CARD.let { t ->
            if (!t.isNullOrEmpty()) {
                NameOfCardET?.setText(t)
            } else  {
                (activity as HawActivity).sharedPreferenceManager.customerInfo.FIRST_NAME.let {
                    NameOfCardET?.setText(it)
                }
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()

        } else {
            setBool((activity as HawActivity).sharedPreferenceManager.lovBool)
        }



        (activity as HawActivity).boolList.let {
//            fivHBLNisa.setItemForfivHBLNisa(it)
////            fivHBLNisaSP?.setSelection(0)
//            if ((activity as HawActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC == "Individual Account") {
//                if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
//                        0
//                    ).gender == "F"
//                    && (activity as HawActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
//                    && (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
//                ) {
//                    fivHBLNisaSP?.setSelection(1)
//                } else {
//                    fivHBLNisaSP?.setSelection(0)
//
//
//                }
//            } else if ((activity as HawActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC == "Joint Account") {
//                if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
//                        0
//                    ).gender == "F"
//                    && (activity as HawActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
//                    && (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
//                ) {
//                    fivHBLNisaSP?.setSelection(1)
//
//                } else {
//                    fivHBLNisaSP?.setSelection(0)
//
//
//                }
//
//            }
//
//
//            fivCardRequird.setItemForfivfivCardRequird(it)

        }
        if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS.get(0).PHONE_BANK == "1") {
            fivCardRequirdSP?.setSelection(1)
        } else if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS.get(0).PHONE_BANK == "0") {
            ToastUtils.normalShowToast(
                activity,
                "Debit cards fields are disabled due to phone banking is No, please mask as Yes", 1
            )
            fivCardRequirdSP?.setSelection(0)
            (activity as HawActivity).globalClass?.setDisbaled(fivCardRequirdSP!!)
        }
        //TODO: When customer segment and account type will be Freelancer then all of debit card feilds are disbaled except PKR & USD
        var accountType =
            (activity as HawActivity).sharedPreferenceManager.customerAccount.ACCT_TYPE
        var currency =
            (activity as HawActivity).sharedPreferenceManager.customerAccount.CURRENCY_DESC
        if ((accountType == "7T" || accountType == "7W") && (currency != "PKR" && currency != "USD")) {
            ToastUtils.normalShowToast(
                activity,
                getString(R.string.currency_account_type_error) + " this $currency currency",
                1
            )
            (activity as HawActivity).globalClass?.setDisbaled(fivCardRequirdSP!!)
            fivCardRequirdSP?.setSelection(0)
        }
    }

    private fun setBool(it: ArrayList<Bool>) {
        try {
            fivHBLNisa.setItemForfivHBLNisa(it)
            (activity as HawActivity).boolList = it
            (activity as HawActivity).sharedPreferenceManager.lovBool = it
            (activity as HawActivity).customerAccounts.HBL_NISA.let { nisa ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromNisaIndex(
                    it,
                    nisa
                ).let {
                    fivHBLNisaSP?.setSelection(it!!)
                }

            }

            if ((activity as HawActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC == "Individual Account") {
                if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
                        0
                    ).gender == "F"
                    && (activity as HawActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
                    && (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
                ) {
                    fivHBLNisaSP?.setSelection(1)
                } else {
                    fivHBLNisaSP?.setSelection(0)


                }
            } else if ((activity as HawActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC == "Joint Account") {
                if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
                        0
                    ).gender == "F"
                    && (activity as HawActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
                    && (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
                ) {
                    fivHBLNisaSP?.setSelection(1)

                } else {
                    fivHBLNisaSP?.setSelection(0)

                }

            }

            fivCardRequird.setItemForfivfivCardRequird(it)

            (activity as HawActivity).customerAccounts.DEBIT_CARD_REQ.let { debitCardReq ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromNisaReqIndex(
                    it,
                    debitCardReq
                ).let {
                    if (it != 0) {
                        fivCardRequirdSP?.setSelection(it!!)
                    } else {
                        var list = (activity as HawActivity).sharedPreferenceManager.hawKeys
                        for (x in 0 until list.size) {
                            if (list[x].IDENTIFIER == Constants.HAW_KEY_DEBIT_CARD_REQUIRED) {
                                (activity as HawActivity).globalClass?.findSpinnerPositionFromprefIndex(
                                    (activity as HawActivity).prefComModeList,
                                    list[x].LOV_DESC!!
                                ).let {
                                    fivCardRequirdSP?.setSelection(it!!)
                                }
                            }
                        }
                    }
                }
            }


            if ((activity as HawActivity).sharedPreferenceManager.lovPrefMode.isNullOrEmpty()) {
                callPrefComMode()

            } else {
                setPrefComMode((activity as HawActivity).sharedPreferenceManager.lovPrefMode)
            }

        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setPrefComMode(it: ArrayList<PrefComMode>) {
        try {
            (activity as HawActivity).prefComModeList = it
            (activity as HawActivity).sharedPreferenceManager.lovPrefMode = it
            fivMode.setItemForPrefComMode(it)
            (activity as HawActivity).customerAccounts.PREFERREDCOMMUMODEDESC.let { item ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromprefIndex(
                    it,
                    item
                ).let {
                    if (it != 0) {
                        fivModeSP?.setSelection(it!!)
                    } else {
                        var list = (activity as HawActivity).sharedPreferenceManager.hawKeys
                        for (x in 0 until list.size) {
                            if (list[x].IDENTIFIER == Constants.PREF_COM_MODE_IDENTIFIER) {
                                (activity as HawActivity).globalClass?.findSpinnerPositionFromprefIndex(
                                    (activity as HawActivity).prefComModeList,
                                    list[x].LOV_DESC!!
                                ).let {
                                    fivModeSP?.setSelection(it!!)
                                }
                            }
                        }
                    }
                }
            }
            (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }


    private fun callBool() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let {
                        setBool(it)

                    }


                }

                override fun BoolFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }


    fun callCardProducts() {
        try {
            (activity as HawActivity).globalClass?.showDialog(activity)
            var lovRequest = DebitCardLovRequest()
            var condition = null
            var iden: String? = null
//            if ((activity as HawActivity).customerAccounts.OPER_INSTRUCTION == "02") {//photo
//                iden = "cardproductphoto"
//            } else if ((activity as HawActivity).customerAccounts.OPER_INSTRUCTION != "02" &&//self
//                (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES != "PK" && //country not pkr
//                (activity as HawActivity).customerAccounts.ACCT_CCY == "PKR" //currency pkr
//            ) {
//                iden = "cardproductphotonrp"
//            } else if ((activity as HawActivity).customerAccounts.OPER_INSTRUCTION != "02" && //self
//                ((activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK" ||// residence pkr
//                        (activity as HawActivity).customerAccounts.ACCT_CCY != "PKR") //currency not pkr
//                && (activity as HawActivity).sharedPreferenceManager.customerInfo.gender == "F"// gender female
//            ) {
//                lovRequest.ACCT_TYPE = (activity as HawActivity).customerAccounts.ACCT_TYPE
//                iden = "cardproducthblnisa"
//            } else {
//                iden = "cardproduct"
//                lovRequest.find.PRD_ACTP = (activity as HawActivity).customerAccounts.ACCT_TYPE
//            }
//            lovRequest.CUST_TYPE =
//                (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
//            lovRequest.ACCT_CCY =
//                (activity as HawActivity).sharedPreferenceManager.customerAccount.CURRENCY
            lovRequest.identifier = Constants.HAW_ACCOUNT_DEBIT_CARD_IDENTIEFIER
            lovRequest.ACCT_TYPE = (activity as HawActivity).customerAccounts.ACCT_TYPE
            var gson = Gson().toJson(lovRequest)
            HBLHRStore.instance?.getCardProducts(
                RetrofitEnums.URL_HBL,
                lovRequest, object : CardProductCallBack {
                    override fun CardProductSuccess(response: CardProductResponse) {
                        response.data?.let {
                            setCardProduct(it)
                        }
                    }

                    override fun CardProductFailure(response: BaseResponse) {
//                        Utils.failedAwokeCalls((activity as HawActivity)) { callCardProducts() }
                        ToastUtils.normalShowToast(activity, response.message, 1)
                        (activity as HawActivity).globalClass?.hideLoader()
                    }
                })
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    fun callPrefComMode() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.PREF_COM_MODE_IDENTIFIER
        HBLHRStore.instance?.getPrefComMode(
            RetrofitEnums.URL_HBL,
            lovRequest, object : PrefComModeCallBack {
                override fun PrefComModeSuccess(response: PrefComModeResponse) {
                    response.data?.let {
                        setPrefComMode(it)
                    }
//                    fivModeSP?.setSelection(1)

//                    load()

                }

                override fun PrefComModeFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            (activity as HawActivity).runOnUiThread {
                                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep7_4)
                    findNavController().navigate(R.id.action_CIFStep7_4_to_CIFStep7_3)
        }
    }

    private fun validation(): Boolean {

        if (fivHBLNisaSP?.selectedItem.toString() == "Yes") {
            if ((activity as HawActivity).sharedPreferenceManager.customerInfo.gender == "F"
                && (activity as HawActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
                && (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    context,
                    "For Nisa Your Gender Should be Female or Nationality should be Pakistan",
                    1
                )
                return false
            }
        } else if (fivCardRequirdSP?.selectedItem.toString() == "Yes") {
            if (fivCardTypeSP?.selectedItemPosition == 0) {
                ToastUtils.normalShowToast(
                    activity,
                    "Please select card type",
                    1
                )
                return false
            } else if (TextUtils.isEmpty(NameOfCardET?.text.toString())) {
                ToastUtils.normalShowToast(
                    activity,
                    "Please select fill name of card",
                    1
                )
                return false
            } else {
                return true
            }
        } else if (fivModeSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select communication mode",
                1
            )
            return false
        } else {
            return true
        }

    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep7_4)
                    findNavController().navigate(R.id.action_CIFStep7_4_to_CIFStep7_3)
            }
            true
        }
        false
    }

    fun send() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in HawActivity*/
        /*set the values from widgets and another models*/
        saveAndNext()
        callAofCustomerAccount()
        var testModel = Gson().toJson((activity as HawActivity).aofAccountInfoRequest)
    }

    fun callAofCustomerAccount() {
        /*  (activity as HawActivity).globalClass?.showDialog(activity)
          var customerAofAccountInfoRequest: CustomerAofAccountInfoResponse =
              CustomerAofAccountInfoResponse()
          customerAofAccountInfoRequest.identifier = Constants.AOF_SUBMIT_IDENTIFIER
          customerAofAccountInfoRequest.data = "{[]}"
          HBLHRStore.instance?.postAofaccountInfo(
              RetrofitEnums.URL_HBL,
              customerAofAccountInfoRequest, object : CustomerAofAccountInfoCallBack {
                  @SuppressLint("WrongConstant")
                  override fun CustomerAofAccountInfoSuccess(response: CustomerAofAccountInfoResponse) {
                      response.data?.let {
                      };
                      Toast.makeText(
                          activity,
                          response.message,
                          5000
                      ).show()
                      activity?.finish()
                      (activity as HawActivity).globalClass?.hideLoader()
                  }

                  override fun CustomerAofAccountInfoFailure(response: BaseResponse) {
                      ToastUtils.normalShowToast(activity, response.message)
                      (activity as HawActivity).globalClass?.hideLoader()
                  }
              })*/
    }

    fun saveAndNext() {
        (activity as HawActivity).globalClass?.findSpinnerPositionFromNISABoolCode(
            (activity as HawActivity).boolList,
            fivHBLNisaSP?.selectedItem.toString()
        ).let {
            (activity as HawActivity).customerAccounts.HBL_NISA = it!!
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromDebitCardBoolCode(
            (activity as HawActivity).boolList,
            fivCardRequirdSP?.selectedItem.toString()
        ).let {
            (activity as HawActivity).customerAccounts.DEBIT_CARD_REQ = it!!
            (activity as HawActivity).customerAccounts.IS_DEBIT_CARD_TYPE_CHANGED = it!!
            (activity as HawActivity).customerAccounts.IS_DEBIT_CARD = it!!
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCardProductCode(
            (activity as HawActivity).cardProductList,
            fivCardTypeSP?.selectedItem.toString()
        ).let {
            if (it != resources.getString(R.string.select) && it != "0" && fivCardRequirdSP?.selectedItemPosition != 0) {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.DEBIT_CARD_TYPE = it.toString()
                (activity as HawActivity).customerAccounts.DEBITCARDTYPEDESC =
                    fivCardTypeSP?.selectedItem.toString()
                NameOfCardET?.text.toString()
                    .let { (activity as HawActivity).customerAccounts.NAME_ON_CARD = it }
            } else {
                (activity as HawActivity).customerAccounts.DEBIT_CARD_TYPE = ""
                (activity as HawActivity).customerAccounts.DEBITCARDTYPEDESC = ""
                (activity as HawActivity).customerAccounts.NAME_ON_CARD
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromPrefComModeCode(
            (activity as HawActivity).prefComModeList,
            fivModeSP?.selectedItem.toString()
        ).let {
            if (it != resources.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.PREFERRED_COMMU_MODE = it.toString()
                (activity as HawActivity).customerAccounts.PREFERREDCOMMUMODEDESC =
                    fivModeSP?.selectedItem.toString()
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount =
            (activity as HawActivity).customerAccounts
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as HawActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as HawActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as HawActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .isNullOrEmpty()
        ) {
            aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as HawActivity).aofAccountInfoRequest = it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                        callCountries()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callCountries() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    response.data?.let {
                        (activity as HawActivity).sharedPreferenceManager.lovCountries = it
                    }
                    callCities()
                }

                override fun CountryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) { callCountries() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    fun callCities() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.CITIES_IDENTIFIER
        HBLHRStore.instance?.getCities(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CityCallBack {
                override fun CitySuccess(response: CityResponse) {
                    response.data?.let {
                        (activity as HawActivity).sharedPreferenceManager.lovCities = it
                        conditionalPageGo()
                        (activity as HawActivity).recyclerViewSetup()
                    }
                    (activity as HawActivity).globalClass?.hideLoader()
                }

                override fun CityFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) { callCountries() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun conditionalPageGo() {
        if (findNavController().currentDestination?.id == R.id.CIFStep7_4) {
            if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7"
                && (activity as HawActivity).sharedPreferenceManager.customerAccount.ACCT_SNG_JNT == "3"
            ) {
                findNavController().navigate(R.id.action_CIFStep7_4_to_CIFStep7_5)
            } else {
                findNavController().navigate(R.id.action_CIFStep7_4_to_CIFStep10)
            }
        }
    }


    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
