package com.hbl.bot.ui.fragments.haw.cif

import java.io.FileNotFoundException
import java.io.IOException
import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.request.baseRM.ETBNTBRequest
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.BranchRegionCode
import com.hbl.bot.network.models.response.baseRM.DocsData
import com.hbl.bot.network.models.response.baseRM.HawCompanyData
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.select_company_name_fragment.*
import kotlinx.android.synthetic.main.select_company_name_fragment.btBack
import kotlinx.android.synthetic.main.select_company_name_fragment.btNext
import kotlinx.android.synthetic.main.select_company_name_fragment.formSectionHeader
import kotlinx.android.synthetic.main.select_company_name_fragment.fsvDocumentType_spinner
import kotlinx.android.synthetic.main.view_form_input.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.lang.ClassCastException
import java.lang.Exception

/**
 * A simple [Fragment] subclass.
 */
class HawCompanyCIF : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var statusCode = "02"
    var msg = ""
    var documentNumberet: EditText? = null
    var fivAreaNameET: EditText? = null
    var fivLandmarkET: EditText? = null
    var fsvDocumentType_spinnerSP: Spinner? = null
    var selectCompanyNameSP: Spinner? = null
    var selectBranchCodeSP: Spinner? = null
    var fivReasonET: EditText? = null
    var fivExistingRelationSP: Spinner? = null
    var pOMStatus: HawPOMStatus? = null

    fun checkAllPagesModelsFilled(): Boolean {
        pOMStatus = HawPOMStatus(activity as HawActivity)
        //:TODO NOK PAGE CHECK
        return pOMStatus?.NextOfKin_Page_1_2_3() == 0
    }

    private fun showSkipDialog() {
        val builder = AlertDialog.Builder((activity as HawActivity))
        builder.setTitle("Information")
        builder.setCancelable(false)
        builder.setMessage((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.COM_PRINT_DOC_DESC)
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            try {
                if (findNavController().currentDestination?.id == R.id.hawComapnySelect) {
                    findNavController().navigate(R.id.action_hawComapnySelect_to_CIFStep14)
                }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail(
                    (activity as HawActivity),
                    (e as ArrayIndexOutOfBoundsException)
                )
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as Exception))
            } finally {

            }
        }
        builder.show()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.COM_PRINT_DOC == "1") {
                showSkipDialog()
            } else if (findNavController().currentDestination?.id == R.id.hawComapnySelect && checkAllPagesModelsFilled()) {
                findNavController().navigate(R.id.action_hawComapnySelect_to_CIFStep16)
            } else if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.select_company_name_fragment, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.value = 1
        viewModel.currentFragmentIndex.value = 0
        val txt = resources.getString(R.string.haw_company_name)
        val txt1 = " (1/1 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            init()
            load()
            conditionDoctype()
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun conditionDoctype() {
        fsvDocumentType_spinnerSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                i: Int,
                l: Long,
            ) {
                try {
                    documentNumberCustomConditions(i)
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
    }

    fun documentNumberCustomConditions(position: Int) {
        /*call CustomerSegment when doctype select condition name and value*/
        var doctypeIndex =
            (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeIndexValue(
                (activity as HawActivity).sharedPreferenceManager.lovDoctype,
                (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE_DESC
            )
        /*Condition when click doctype list auto generate document number pattern according to doctype*/
        if (doctypeIndex != position) {
            documentNumberet?.setText("")
        }
        if (position == 0 && !(activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(0)?.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as HawActivity).globalClass?.setDisbaled(documentNumberet!!)
        } else if (position == 1 && (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(
                0
            )?.ETBNTBFLAG.isNullOrEmpty()
        ) {
            /*CNIC 13 Numeric*/
            (activity as HawActivity).globalClass?.setEnabled(documentNumberet!!, true)
            documentSetPrefix(position, "CNIC")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumberet!!,
                13,
                Constants.INPUT_TYPE_NUMBER
            )
        } else /*if (position == 2 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            *//*PASSPORT 20 AlphaNumeric*//*
            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "06")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumber,
                19,
                INPUT_TYPE_ALPHANUMERIC
            )
        } else */ if (position == 2 && (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(
                0
            )?.ETBNTBFLAG.isNullOrEmpty()
        ) {
            /*SNIC 13 Numeric*/
            (activity as HawActivity).globalClass?.setEnabled(documentNumberet!!, true)
            documentSetPrefix(position, "SNIC")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumberet!!,
                13,
                Constants.INPUT_TYPE_NUMBER
            )
        } else if (position == 4 && (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(
                0
            )?.ETBNTBFLAG.isNullOrEmpty()
        ) {
//            CRC 20 AlphaNumeric
            (activity as HawActivity).globalClass?.setEnabled(documentNumberet!!, true)
            documentSetPrefix(position, "CRC")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumberet!!,
                13,
                Constants.INPUT_TYPE_NUMBER
            )
        } else if (position == 3 && (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(
                0
            )?.ETBNTBFLAG.isNullOrEmpty()
        ) {
            /*NICOP 13 Numeric*/
            (activity as HawActivity).globalClass?.setEnabled(documentNumberet!!, true)
            documentSetPrefix(position, "NICOP")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumberet!!,
                13,
                Constants.INPUT_TYPE_NUMBER
            )
        } else if (position == 6 && (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(
                0
            )?.ETBNTBFLAG.isNullOrEmpty()
        ) {
            /*CRC 13 Numeric*/
            (activity as HawActivity).globalClass?.setEnabled(documentNumberet!!, true)
            documentSetPrefix(position, "CRC")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumberet!!,
                13,
                Constants.INPUT_TYPE_NUMBER
            )
        } else if (position == 7 && (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(
                0
            )?.ETBNTBFLAG.isNullOrEmpty()
        ) {
            /*POC 13 AlphaNumeric*/
            (activity as HawActivity).globalClass?.setEnabled(documentNumberet!!, true)
            documentSetPrefix(position, "POC")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumberet!!,
                13,
                Constants.INPUT_TYPE_ALPHANUMERIC
            )
        } else if (position == 8 && (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(
                0
            )?.ETBNTBFLAG.isNullOrEmpty()
        ) {
            /*BirthCertificate 18 Numeric*/
            (activity as HawActivity).globalClass?.setEnabled(documentNumberet!!, true)
            documentSetPrefix(position, "08")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumberet!!,
                18,
                Constants.INPUT_TYPE_NUMBER
            )
        } else if (position == 9 && (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(
                0
            )?.ETBNTBFLAG.isNullOrEmpty()
        ) {
            /*POR Afghan Refugee Card 13 AlphaNumeric*/
            (activity as HawActivity).globalClass?.setEnabled(documentNumberet!!, true)
            documentSetPrefix(position, "POR")
            (activity as HawActivity).globalClass?.edittextTypeCount(
                documentNumberet!!,
                13,
                Constants.INPUT_TYPE_ALPHANUMERIC
            )
        }
    }


    fun documentSetPrefix(position: Int, label: String) {
        documentNumberet?.requestFocus()
        if (position == 2 && label.equals("06") && !(activity as HawActivity).customerInfo.iddocumenttype.equals(
                "06"
            )
        ) {
            documentNumberet?.setPadding(22, 0, 0, 0)
            startPrefixOfDocument.visibility = View.VISIBLE
            startPrefixOfDocument?.setText("P")
        } else if (position == 8 && label.equals("08") && !(activity as HawActivity).customerInfo.iddocumenttype.equals(
                "08"
            )
        ) {
            documentNumberet?.setPadding(22, 0, 0, 0)
            startPrefixOfDocument.visibility = View.VISIBLE
            startPrefixOfDocument?.setText("B")
        } else {
            documentNumberet?.setPadding(0, 0, 0, 0)
            startPrefixOfDocument.visibility = View.GONE
            startPrefixOfDocument?.setText("")
        }
    }


    fun load() {
        (activity as HawActivity).globalClass?.clearETBNTB()
        (activity as HawActivity).globalClass?.showDialog(activity)
        (activity as HawActivity).customerInfo.ID_DOCUMENT_NO.let { t ->
            documentNumberet?.setText(t)
        }
//        if ((activity as HawActivity).sharedPreferenceManager.companyName.isNullOrEmpty()) {
        callDocs()

//        } else {
//            hawComapnyData((activity as HawActivity).sharedPreferenceManager.companyName)
//        }
    }

    private fun callHawCompany() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.HAW_COMPANY_IDENTIFIER
        HBLHRStore.instance?.getHawCompanies(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : HawCompanyCallBack {
                override fun HawCompanySuccess(response: HawCompanyResponse) {
                    try {
                        response.data?.let {
                            hawComapnyData(it)
                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: java.lang.ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as java.lang.ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun HawCompanyFailure(response: BaseResponse) {
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun callBranchRegion() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BRANCH_REGION_IDENTIFIER
        HBLHRStore.instance?.getBranchRegion(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : BranchRegionCallBack {
                override fun Success(response: BranchRegionResponse) {
                    try {
                        response.data?.let {
                            branchRegionData(it)
                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: java.lang.ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as java.lang.ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun Failure(response: BaseResponse) {
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }


    fun hawComapnyData(it: ArrayList<HawCompanyData>) {
        selectCompanyName.setItemForHawCompanyData(it)
        (activity as HawActivity).hawCompanyTypeList = it
        (activity as HawActivity).sharedPreferenceManager.companyName = it
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCompanyNameValue(
            it,
            (activity as HawActivity).customerInfo.NameOfCompanyOrEmployer
        ).let {
            selectCompanyNameSP?.setSelection(it!!)
        }
//      fsvDocumentType_spinner.remainSelection(it.size)
        if ((activity as HawActivity).sharedPreferenceManager.loginData.USER_BRANCH_SELECTION == "1") {
            selectBranchCodeSP?.visibility = View.VISIBLE
            selectBranchCode.visibility = View.VISIBLE
            callBranchRegion()
        } else {
            selectBranchCodeSP?.visibility = View.GONE
            selectBranchCode.visibility = View.GONE
            (activity as HawActivity).globalClass?.hideLoader()
        }
//        } else {
//            branchRegionData((activity as HawActivity).sharedPreferenceManager.branchRegion)
//        }
    }

    fun branchRegionData(it: ArrayList<BranchRegionCode>) {
        selectBranchCode.setItemForBranchRegionData(it)
        (activity as HawActivity).branchRegionTypeList = it
        (activity as HawActivity).sharedPreferenceManager.branchRegion = it
        (activity as HawActivity).globalClass?.findSpinnerPositionFromBranchRegionValue(
            (activity as HawActivity).branchRegionTypeList,
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BranchCode
        ).let {
            selectBranchCodeSP?.setSelection(it!!)
        }
//        fsvDocumentType_spinner.remainSelection(it.size)
        (activity as HawActivity).globalClass?.hideLoader()
    }

    fun callBranchType(branchCode: String) {
        var branchCodeRequest = BranchCodeRequest()
        branchCodeRequest.find.BR_CODE = branchCode
        branchCodeRequest?.identifier = Constants.BRANCH_TYPE_INDENTIFIER
        val gson = Gson()
        val json = gson.toJson(branchCodeRequest)
        HBLHRStore!!.instance!!.getBranchType(
            RetrofitEnums.URL_HBL,
            branchCodeRequest,
            object : BranchTypeCallBack {
                override fun BranchTypeSuccess(response: BranchTypeResponse) {
                    try {
                        GlobalClass.sharedPreferenceManager!!.setBranchType(
                            response?.data?.get(
                                0
                            )
                        )
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(

                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as Exception)
                        )
                    }
                }

                override fun BranchTypeFailure(response: BaseResponse) {
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callBranchCode(branchCode: String) {
        var hblhrStore: HBLHRStore = HBLHRStore()
        var branchCodeRequest = BranchCodeRequest()
        branchCodeRequest.find.BR_CODE = branchCode
        branchCodeRequest?.identifier = Constants.BRANCH_CODE_INDENTIFIER
        val gson = Gson()
        val json = gson.toJson(branchCodeRequest)
        hblhrStore!!.getBranchCode(
            RetrofitEnums.URL_HBL,
            branchCodeRequest,
            object : BranchCodeCallBack {
                override fun BranchCodeSuccess(response: BranchCodeResponse) {
                    GlobalClass.sharedPreferenceManager!!.setBranchCode(response?.data?.get(0))
                    callBranchType(branchCode)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

                override fun BranchCodeFailure(response: BaseResponse) {
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun init() {

        (activity as HawActivity).actionBar?.displayOptions = 0
        documentNumberet = documentNumber_et.getTextFromEditText(R.id.documentNumber_et)
        fsvDocumentType_spinnerSP = fsvDocumentType_spinner.getSpinner(R.id.fsvDocumentType_spinner)
        selectCompanyNameSP = selectCompanyName.getSpinner(R.id.selectCompanyName)
        selectBranchCodeSP = selectBranchCode.getSpinner(R.id.selectBranchCode)
        (activity as HawActivity).globalClass?.edittextTypeCount(
            documentNumberet!!,
            13,
            Constants.INPUT_TYPE_NUMBER
        )
        selectBranchCodeSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (selectBranchCodeSP?.selectedItemPosition != 0) {
                        callBranchCode((activity as HawActivity).branchRegionTypeList[pos].Branch_Code!!)
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
    }

    fun callDocs() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.HAW_DOCS_IDENTIFIER
        HBLHRStore.instance?.getDocs(RetrofitEnums.URL_HBL, lovRequest, object : DocsCallBack {
            override fun DocsSuccess(response: DocsResponse) {
                try {
                    response.data?.let {
                        docTypeData(it)
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun DocsFailure(response: BaseResponse) {
                (activity as HawActivity).globalClass?.setEnabled(
                    btNext!!,
                    true
                )
                //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                ToastUtils.normalShowToast(activity, response.message, 1)
                (activity as HawActivity).globalClass?.hideLoader()
            }
        })
    }

    fun docTypeData(it: ArrayList<DocsData>) {
        fsvDocumentType_spinner.setItemForDocs(it)
        (activity as HawActivity).docTypeList = it
        (activity as HawActivity).sharedPreferenceManager.lovDoctype = it
        callHawCompany()
        (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeIndexValue(
            it,
            (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE_DESC
        ).let {
            fsvDocumentType_spinnerSP?.setSelection(it!!)
        }
//        fsvDocumentType_spinner.remainSelection(it.size)
    }

    fun subETBNTB() {
        var etbntbRequest = ETBNTBRequest()
        etbntbRequest.CHANNEL = "9"
        etbntbRequest.CHANNELLID = "asd"
        etbntbRequest.TELLERID = "asd"
        etbntbRequest.CHANNELDATE = "asd"
        etbntbRequest.CHANNELTIME = "asd"
        etbntbRequest.CHANNELSRLNO = "asd"
        etbntbRequest.SERVICENAME = "C"
        etbntbRequest.REMARK = "asd"
        documentNumberet?.text.toString().trim().let {
            etbntbRequest.CNIC = it
        }
        etbntbRequest.payload =
            "U2FsdGVkX1+eMelgQeJlI51PUXnmebgWMVjt8eGRb7kTZ14o9CGJ17PkRN/R7Ol/LVVjoXNA3uAtF05FVtvweDF/ZQKbqShdXACJgx0H6tI="
        var json = Gson().toJson(etbntbRequest)
        HBLHRStore.instance?.submitETBNTB(
            RetrofitEnums.URL_HBL,
            etbntbRequest,
            object : ETBNTBCallBack {
                override fun ETBNTBSuccess(response: ETBNTBResponse) {
                    statusCode = response.status!!
                    msg = response.message!!
                    //Goto Account Page
                    if (response.status == "00" && response.data?.get(0)?.ETBNTBFLAG == "C") {
                        Log.i(
                            "ETBNTBFLAG",
                            "${response.status} ${response.data?.get(0)?.ETBNTBFLAG}"
                        )
                        (activity as HawActivity).sharedPreferenceManager.etbntb = response.data
                        callCIF(documentNumberet?.text.toString(), response.message!!)
                    } else if (response.status == "00" && response.data?.get(0)?.ETBNTBFLAG == "N") {
                        (activity as HawActivity).sharedPreferenceManager.etbntb = response.data
                        (activity as HawActivity).globalClass?.isAllFieldsDisbaled = false
                        //Simple cif flow
                        etbntbAlert(
                            AofAccountInfoResponse(),
                            "Information",
                            response.message!!,
                            true, false, response.data?.get(0)?.ETBNTBFLAG!!
                        )
                    } else if (response.status == "00" && response.data?.get(0)?.ETBNTBFLAG == "E") {
                        (activity as HawActivity).sharedPreferenceManager.etbntb = response.data
                        (activity as HawActivity).globalClass?.isAllFieldsDisbaled = false
                        checkAccountExistApi(
                            documentNumberet?.text.toString(),
                            "",
                            AofAccountInfoResponse(),
                            msg, response.data?.get(0)?.ETBNTBFLAG!!
                        )
                    } else {
                        (activity as HawActivity).sharedPreferenceManager.etbntb = response.data
                        (activity as HawActivity).globalClass?.isAllFieldsDisbaled = false
                        etbntbAlert(
                            AofAccountInfoResponse(),
                            "Information",
                            response.message!!,
                            false, false, response.data?.get(0)?.ETBNTBFLAG!!
                        )
                        (activity as HawActivity).globalClass?.setEnabled(
                            btNext!!,
                            true
                        )
                        (activity as HawActivity).globalClass?.hideLoader()
                    }
                }

                override fun ETBNTBFailure(response: BaseResponse) {
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callCIF(cnic: String, msg: String) {
        var cifRequest = CIFRequest()
        cifRequest.payload = Constants.PAYLOAD
        cifRequest.scnic = cnic
        HBLHRStore.instance?.getCIF(RetrofitEnums.URL_HBL, cifRequest, object : CIFCallBack {
            override fun CIFSuccess(response: AofAccountInfoResponse) {
                if (response.status == "00" && response.data.getOrNull(0)?.CUST_DEMOGRAPHICS?.getOrNull(
                        0
                    )?.CUST_TYPE == "A1"
                ) {
                    (activity as HawActivity).globalClass?.clearSubmitAOF()
                    (activity as HawActivity).globalClass?.clearAllLovs()
                    response.let {
                        checkAccountExistApi(
                            cnic,
                            response.data!!.getOrNull(0)?.CUST_ACCOUNTS?.getOrNull(0)?.ACCT_TYPE!!,
                            response!!,
                            msg,
                            "C"
                        )
                    }
                    (activity as HawActivity).globalClass?.hideLoader()
                } else if (response.status == "00" && response.data.getOrNull(0)?.CUST_DEMOGRAPHICS?.getOrNull(
                        0
                    )?.CUST_TYPE != "A1"
                ) {
                    etbntbAlert(
                        AofAccountInfoResponse(),
                        "Information",
                        "User CIF exists and user Customer Segment is not a Salaried Person that is why the account can't be opened\n\nThe customer segment of existing CIF is ${
                            response.data!![0].CUST_DEMOGRAPHICS.getOrNull(
                                0
                            )?.CUST_TYPE
                        }-${response.data!![0].CUST_DEMOGRAPHICS.getOrNull(0)?.CUSTTYPEDESC}",
                        false, false,
                        "C"
                    )
                    (activity as HawActivity).globalClass?.hideLoader()
                } else {
                    etbntbAlert(
                        AofAccountInfoResponse(),
                        "Information",
                        response.message!!,
                        false,
                        false,
                        "C"
                    )
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            }

            override fun CIFFailure(response: BaseResponse) {
                (activity as HawActivity).globalClass?.hideLoader()
                (activity as HawActivity).globalClass?.setEnabled(
                    btNext!!,
                    true
                )
            }
        })

    }

    private fun etbntbAlert(
        response: AofAccountInfoResponse,
        header: String,
        cnic: String,
        isMove: Boolean,
        isCifCalled: Boolean,
        ETBNTB_FLAG: String
    ) {
        var dialogBuilder = AlertDialog.Builder(activity as HawActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.popup_etbntb, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.setCancelable(false)
        alertDialog.show()
        var toolbar = layoutView.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        var desctext1 = layoutView.findViewById<TextView>(R.id.desctext1)
        var yes = layoutView.findViewById<Button>(R.id.yes_action)
        toolbar.title = header
        desctext1.text = cnic
        yes.text = "OK"
        yes.setOnClickListener(View.OnClickListener {
            if (isMove) {
                if (isCifCalled) {
                    HawDataSetupInSp(activity as HawActivity, response!!, false)
                }
                if (ETBNTB_FLAG == "E") {
                    saveAndNext(true,ETBNTB_FLAG)
                } else {
                    saveAndNext(false,ETBNTB_FLAG)
                }
//                checkAccountExistApi(cnic,response.data!!.getOrNull(0)?.CUST_ACCOUNTS?.getOrNull(0)?.ACCT_TYPE!!,response!!)
            }
            (activity as HawActivity).globalClass?.setEnabled(
                btNext!!,
                true
            )
            alertDialog.dismiss()
        })
        alertDialog.show()
    }

    fun checkAccountExistApi(
        cnic: String,
        accType: String,
        baseResponse: AofAccountInfoResponse,
        msg: String,
        ETBNTB_FLAG: String
    ) {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var verifyAccount = VerifyAccountRequest()
        verifyAccount.cnic = cnic
        verifyAccount.ACCT_TYPE = accType
        verifyAccount.identifier = Constants.TYPE_ACCOUNT_VALIDATION_IDENTIFIER_HAW
        HBLHRStore.instance?.VerifyAccount(
            RetrofitEnums.URL_HBL,
            verifyAccount,
            object : VerifyAccountCallBack {
                override fun VerifyAccountSuccess(response: VerifyAccountResponse) {
                    if (response.status == "00" && ETBNTB_FLAG.equals("C",true)) {
                        etbntbAlert(
                            baseResponse!!,
                            "Case: ETB",
                            "Customer CIF already exists\n\nCustomer CNIC: $cnic\n\nClick \"OK\" to proceed ETB Case to OPEN ACCOUNT\n\n$msg",
                            true, true, ETBNTB_FLAG
                        )
                    } else if (response.status == "00" && ETBNTB_FLAG.equals("E",true)) {
                        etbntbAlert(
                            baseResponse!!,
                            "Case: ETB",
                            "Customer cif not opened from ZNA\n" +
                                    "Click \"OK\" to proceed For CIF AND ACCOUNT",
                            true, false, ETBNTB_FLAG
                        )
                    } else {
                        /*When Account does not exist*/
                        etbntbAlert(
                            baseResponse!!,
                            "Case: ETB",
                            "Consumer CNIC: $cnic\n\n Customer ${response.message.toString()}",
                            false, false,""
                        )
                        (activity as HawActivity).globalClass?.hideLoader()
//                        (activity as HawActivity).globalClass?.setDisbaled(
//                            btNext!!
//                        )
                    }

                }

                override fun VerifyAccountFailure(response: BaseResponse) {
                    (activity as HawActivity).globalClass?.hideLoader()
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    ToastUtils.normalShowToast(
                        context,
                        response.message.toString(), 1
                    )
                }

            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        subETBNTB()
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
            R.id.btBack -> {
                activity?.finish()
            }
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep11)
                    findNavController().navigate(R.id.action_CIFStep11_to_CIFStep10)
            }
            true
        }
        false
    }

    fun callTrackingID(check: Boolean) {
        var trackingIDRequest = TrackingIDRequest()
        trackingIDRequest?.brcode =
            (activity as HawActivity).sharedPreferenceManager.getLoginData()
                .getBRANCHCODE()
        trackingIDRequest?.brcode =
            (activity as HawActivity).sharedPreferenceManager.getLoginData()
                .getBRANCHCODE()
        var baseResponse = Gson().toJson(trackingIDRequest)
        HBLHRStore.instance?.getTrackingID(
            RetrofitEnums.URL_HBL,
            trackingIDRequest,
            object : TrackingIDCallBack {
                override fun TrackingIDSuccess(response: TrackingIDResponse) {
                    try {
                        (activity as HawActivity).sharedPreferenceManager.setTrackingID(
                            response?.data?.get(0)
                        )
                        callBusinessArea(check)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as Exception)
                        )
                    }
                }

                override fun TrackingIDFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callBusinessArea(check: Boolean) {
        var lov = BusinessAreaRequest()
        lov.identifier = Constants.BUSINESSAREA_IDENTIFIER
        lov.find.BR_CODE =
            GlobalClass.sharedPreferenceManager!!.getLoginData().getBRANCHCODE().toString()
        HBLHRStore.instance?.getBusinessArea(
            RetrofitEnums.URL_HBL,
            lov,
            object : BusinessAreaCallBack {
                override fun BusinessAreaSuccess(response: BusinessAreaResponse) {
                    try {
                        (activity as HawActivity).sharedPreferenceManager.setBusinessArea(
                            response.data.get(0)
                        )
                        sendToWay(check)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as Exception)
                        )
                    }
                }

                override fun BusinessAreaFailure(response: BaseResponse) {
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun sendToWay(check: Boolean) {
        if (check) {
            activity?.runOnUiThread {
                (activity as HawActivity).recyclerViewSetup()
                if (findNavController().currentDestination?.id == R.id.hawComapnySelect) {
                    findNavController().navigate(R.id.action_company_select_to_CIFStep1)
                }
                (activity as HawActivity).globalClass?.setEnabled(
                    btNext!!,
                    true
                )
                (activity as HawActivity).globalClass?.hideLoader()
            }
        } else {
            (activity as HawActivity).globalClass?.eTBCaseDisbaledFields()
            (activity as HawActivity).init()
            (activity as HawActivity).recyclerViewSetup()
            if (findNavController().currentDestination?.id == R.id.hawComapnySelect) {
                findNavController().navigate(R.id.action_company_select_to_CIFStep1)
            }
        }
        (activity as HawActivity).globalClass?.hideLoader()
    }


    fun saveAndNext(check: Boolean,ETBNTB_FLAG:String) {
        ((activity as HawActivity).aofAccountInfoRequest) =
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo
        ((activity as HawActivity).customerInfo) =
            (activity as HawActivity).sharedPreferenceManager.customerInfo
        ((activity as HawActivity).customerDemoGraphicx) =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics
        ((activity as HawActivity).customerCdd) =
            (activity as HawActivity).sharedPreferenceManager.customerCDD
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCompanyNameIndexValue(
            (activity as HawActivity).hawCompanyTypeList,
            selectCompanyNameSP?.selectedItem.toString()
        ).let {
            if (it != 0 && !(activity as HawActivity).globalClass?.isAllFieldsDisbaled!! && !ETBNTB_FLAG.equals("C",true)) {
                // when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerDemoGraphicx.NAME_OF_COMPANY =
                    (activity as HawActivity).hawCompanyTypeList[it!!].NameOfCompanyOrEmployer!!
                (activity as HawActivity).customerCdd.cUST_SRC_OF_INDUS_TYPE =
                    (activity as HawActivity).hawCompanyTypeList[it!!].IndustryMainCategory!!
                (activity as HawActivity).customerCdd.cUST_SRC_OF_INCOME_INDUS =
                    (activity as HawActivity).hawCompanyTypeList[it!!].IndustrySubCategory!!
                (activity as HawActivity).customerInfo.OfficeAddress =
                    (activity as HawActivity).hawCompanyTypeList[it!!].OfficeAddress
                (activity as HawActivity).customerInfo.OrgCode =
                    (activity as HawActivity).hawCompanyTypeList[it!!].OrgCode
                (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG = ""
            }
        }
        documentNumberet?.text.toString().trim().let {
            //TODO: insert document number
            ((activity as HawActivity).aofAccountInfoRequest).ID_DOC_NO = it
            (activity as HawActivity).customerInfo.ID_DOCUMENT_NO = it
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeCode(
            (activity as HawActivity).docTypeList,
            fsvDocumentType_spinnerSP?.selectedItem.toString()
        ).let {
            //TODO: set iDDOCUMENTNO
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                ((activity as HawActivity).aofAccountInfoRequest).ID_DOCUMENT_TYPE =
                    it.toString()
                (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE = it.toString()
                (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE_DESC =
                    fsvDocumentType_spinnerSP?.selectedItem.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromBranchRegionDESC(
            (activity as HawActivity).branchRegionTypeList,
            selectBranchCodeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {
                // when select there is no item selected "Choose an item"  <--- hint
                var array = it.split("-")
                var login = (activity as HawActivity).sharedPreferenceManager.loginData
                var branchCode = (activity as HawActivity).sharedPreferenceManager.branchCode
                ((activity as HawActivity).aofAccountInfoRequest).BranchCode = array[0]
                login.setBRANCHCODE(array[0])
                login.setBRANCHNAME(array[1])
                login.setBRANCHTYPE((activity as HawActivity).sharedPreferenceManager.branchType.BR_TYPE.toString())
                login.setBRANCHADDRESS(branchCode.BR_ADDR1.toString() + " " + branchCode.BR_ADD2.toString() + " " + branchCode.BR_ADD3.toString() + " " + branchCode.BR_ADD4.toString())
                (activity as HawActivity).sharedPreferenceManager.loginData = login
            }
        }

//        //TODO: set trackingID
//        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
//                .isNullOrEmpty()
//        ) {
//            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
//                (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
//            }
//        } else {
//            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
//                ?.itemDescription.let {
//                    (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
//                }
//        }

        /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in HawActivity*/
        (activity as HawActivity).sharedPreferenceManager.customerInfo =
            ((activity as HawActivity).customerInfo)
        (activity as HawActivity).sharedPreferenceManager.customerCDD =
            ((activity as HawActivity).customerCdd)
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics =
            ((activity as HawActivity).customerDemoGraphicx)
        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo =
            ((activity as HawActivity).aofAccountInfoRequest)
        callTrackingID(check)
    }

    fun validation(): Boolean {
        val length = documentNumberet?.text.toString().length
        return if (fsvDocumentType_spinnerSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.errDOCTTypeETBN), 1
            )
            false
        } else if (length == 0) {
            documentNumberet?.setError(resources!!.getString(R.string.errCNICETBN));
            documentNumberet?.requestFocus();
            return false
        } else if (length < 13) {
            documentNumberet?.setError(resources!!.getString(R.string.errorCNIC));
            documentNumberet?.requestFocus();
            return false
        } else if (selectCompanyNameSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.companyNameErr), 1
            )
            false
        } else if (selectBranchCodeSP?.selectedItemPosition == 0 && (activity as HawActivity).sharedPreferenceManager.loginData.USER_BRANCH_SELECTION == "1") {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.branchRegionErr), 1
            )
            false
        } else {
            true
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as HawActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as HawActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as HawActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .isNullOrEmpty()
        ) {
            aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as HawActivity).aofAccountInfoRequest = it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as HawActivity).globalClass?.hideLoader()
                        if (findNavController().currentDestination?.id == R.id.hawComapnySelect)
                            findNavController().navigate(R.id.action_company_select_to_CIFStep1)
                        (activity as HawActivity).recyclerViewSetup()
                        (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    (activity as HawActivity).globalClass?.setEnabled(
                        btNext!!,
                        true
                    )
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
