package com.hbl.bot.ui.customviews;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Spinner;

@SuppressLint("AppCompatCustomView")
public class CustomSpinner extends Spinner {
    public CustomSpinner(Context context) {
        super(context);
    }

    public CustomSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

   /* public void showDialog(String title, List<Object> countriesList, TextView textView) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.search_dialog_list);
        TextView text = (TextView) dialog.findViewById(R.id.header_list_popup_btn);
        EditText myFilter = (EditText) dialog.findViewById(R.id.et_dialog_search_list);
        text.setText(title);
        ImageButton dialogButton = (ImageButton) dialog.findViewById(R.id.action_cancel);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        ListView listView = dialog.findViewById(R.id.select_dialog_listview);
        ArrayAdapter<Object> dataAdapter = new ArrayAdapter<Object>(getContext(),
                android.R.layout.simple_spinner_item, countriesList);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);
        listView.setTextFilterEnabled(true);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Countries entry = (Countries) parent.getAdapter().getItem(position);
                textView.setText(entry.getTERRITORYSHORTNAME());
                territoryCode = entry.getTERRITORYCODE();
                dialog.dismiss();
            }
        });
        myFilter.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                dataAdapter.getFilter().filter(s.toString());
            }
        });
        dataAdapter.notifyDataSetChanged();
        dialog.show();
    }*/
}
