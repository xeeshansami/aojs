package com.hbl.bot.ui.fragments.accounts


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep7.*
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.math.BigInteger
import java.util.concurrent.Executors

/**
 * A simple [Fragment] subclass.
 */
class HawAccount1Fragment34 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivPrioritySP: Spinner? = null
    var AccountTypeCode: String? = null
    var riskStatusET: EditText? = null
    var fivAccountOTypeSP: Spinner? = null
    var fiVAccountTitleET: EditText? = null
    var fiVNameOfPropectiveRemittenceET: EditText? = null
    var fivAccountTypeSP: Spinner? = null
    var fivRelationshipAccountsSP: Spinner? = null
    var fivCurrencySP: Spinner? = null
    var fivInitialSourceSP: Spinner? = null
    var fiVDepositSourceET: EditText? = null
    var nadraVerify = NadraVerify()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep7, container, false)

            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(5)
        viewModel.currentFragmentIndex.setValue(0)
        val txt = resources.getString(R.string.account)
        val txt1 = " (1/5 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            header()
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            btRiskRating.setOnClickListener(this)
            header()
            init()
            disabilityFields()
            load()
            /*  if (GlobalClass.isDummyDataTrue) {
                  loadDummyData()
              }*/
            setLengthAndTpe()
            try {
                setConditions()

            } catch (e: Exception) {
            }
//            disabilityFields()
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    fun init() {
        fivAccountOTypeSP = fivAccountOType.getSpinner(R.id.accountOperationOfTypeID)
        fivPrioritySP = fivPriority.getSpinner(R.id.fivPriority)
        (activity as HawActivity).globalClass?.setDisbaled(fivAccountOTypeSP!!)
        riskStatusET = fiVRiskRating.getTextFromEditText(R.id.riskStatusID)
        fiVAccountTitleET = fiVAccountTitle.getTextFromEditText(R.id.fiVAccountTitle1)
        fiVNameOfPropectiveRemittenceET =
            fiVNameOfPropectiveRemittence.getTextFromEditText(R.id.fiVNameOfPropectiveRemittence)
        fivAccountTypeSP = fivAccountType.getSpinner()
        fivRelationshipAccountsSP = fivRelationshipAccounts.getSpinner()
        fivCurrencySP = fivCurrency.getSpinner(R.id.fivCurrencyID)
        fivInitialSourceSP = fivInitialSource.getSpinner()
        fiVDepositSourceET = fiVDepositSource.getTextFromEditText(R.id.fiVDepositSource)
        (activity as HawActivity).globalClass?.setDisbaled(fivCurrencySP!!)
        (activity as HawActivity).globalClass?.setDisbaled(btNext!!)
    }

    private fun disabilityFields() {
//        if((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
//            (activity as HawActivity).globalClass?.setDisbaled(fivAccountOTypeSP!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fivPrioritySP!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(riskStatusET!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fiVAccountTitleET!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fiVNameOfPropectiveRemittenceET!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fivRelationshipAccountsSP!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fivAccountTypeSP!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fivInitialSourceSP!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fiVDepositSourceET!!,false)
//            (activity as HawActivity).globalClass?.setDisbaled(fivCurrencySP!!,false)
//        }
        (activity as HawActivity).globalClass?.setDisbaled(fivInitialSourceSP!!, false)
    }

    private fun load() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        arguments?.getParcelable<NadraVerify>(Constants.NADRA_RESPONSE_DATA)?.let {
            nadraVerify = it
        }

        (activity as HawActivity).customerInfo.FIRST_NAME.let { t ->
            fiVAccountTitleET?.setText(
                t
            )
        }
        (activity as HawActivity).customerAccounts.INIT_DEPOSIT.let { it ->
//            if (t.isNullOrEmpty()) {
//                fiVDepositSourceET?.setText(
//                    "0"
//                )
//            } else {
            if (!it.isNullOrEmpty()) {
                fiVDepositSourceET?.setText(it)
            } else {
                var list = (activity as HawActivity).sharedPreferenceManager.hawKeys
                for (x in 0 until list.size) {
                    if (list[x].IDENTIFIER == Constants.INPUT_VALUES && list[x].NAME == Constants.KEY_NAME_INITIAL_DEPOSIT) {
                        list[x].LOV_DESC.let {
                            fiVDepositSourceET?.setText(it)
                        }
                    }
                }
            }
//            }
        }


        if ((activity as HawActivity).sharedPreferenceManager.lovOperationalType.isNullOrEmpty()) {
            callAccountOperationsType()

        } else {
            setAccountOperationsType((activity as HawActivity).sharedPreferenceManager.lovOperationalType)
        }


    }

    private fun setAccountOperationsType(it: ArrayList<AccountOperationsType>) {
        try {
            (activity as HawActivity).accountOperationTypeList = it
            fivAccountOType.setItemForAccountOperationsType(it)
            (activity as HawActivity).sharedPreferenceManager.lovOperationalType = it
//        if (checkMinorAccountCondition()) {
            (activity as HawActivity).customerAccounts.ACCT_OPER_TYPE_DESC.let { operationaltype ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromOpTypeIndex(
                    it,
                    operationaltype
                ).let {
                    fivAccountOTypeSP?.setSelection(it!!)
                    if (it == 0) {
                        checkMinorAccountCondition()
                    }
                }
            }
//        }
            if ((activity as HawActivity).sharedPreferenceManager.lovPriority.isNullOrEmpty()) {
                callPriority()
            } else {
                setPriority((activity as HawActivity).sharedPreferenceManager.lovPriority)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun checkMinorAccountCondition(): Boolean {
        return if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
            fivAccountOTypeSP?.setSelection(3)
            false
        } else if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE != "A7") {
            fivAccountOTypeSP?.setSelection(1)
            false
        } else {
            true
        }
    }

    fun callPriority() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.PRIORITY_IDENTIFIER
        HBLHRStore.instance?.getPriority(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : PriorityCallBack {
                override fun PrioritySuccess(response: PriorityResponse) {
                    response.data?.let {
                        setPriority(it)
                    };
                }

                override fun PriorityFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setPriority(it: ArrayList<Priority>) {
        try {
            (activity as HawActivity).priorityList = it
            fivPriority.setItemForPriority(it)
            (activity as HawActivity).sharedPreferenceManager.lovPriority = it
            (activity as HawActivity).customerAccounts.PRITY_DESC.let { currDesc ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromPriorityIndex(
                    it,
                    currDesc
                ).let {
                    fivPrioritySP?.setSelection(it!!)
                }

            }

//            if ((activity as HawActivity).sharedPreferenceManager.lovAccountType.isNullOrEmpty()) {
            getAccountType()
/*
            } else {
                setAccountType((activity as HawActivity).sharedPreferenceManager.lovAccountType)
            }
*/
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setAccountType(it: ArrayList<AccountType>) {
        try {
            var newIt = ArrayList<AccountType>()
            var expectedMonthlyIncome =
                (activity as HawActivity).customerDemoGraphicx.EXP_MON_INCOME.toInt()
            if (expectedMonthlyIncome >= 25000) {
                for (x in it) {
                    if (x.ACCT_TYPE.equals("GD", true) || x.ACCT_TYPE.equals("IR", true)) {
                        var obj = AccountType()
                        obj.ACCT_CCY = x.ACCT_CCY
                        obj.ACCT_TYPE = x.ACCT_TYPE
                        obj.ACCT_TYPE_DESC = x.ACCT_TYPE_DESC
                        obj.CUST_SEGMENT = x.CUST_SEGMENT
                        obj.SEGMENT_DESC = x.SEGMENT_DESC
                        obj._id = x._id
                        obj.Convential_Islamic = x.Convential_Islamic
                        newIt.add(obj)
                    }
                }
            }
            if (newIt.size != 0) {
                (activity as HawActivity).accountTypeList = newIt
                fivAccountType.setItemForAccountType(newIt)
                (activity as HawActivity).sharedPreferenceManager.lovAccountType = newIt
                (activity as HawActivity).customerAccounts.ACCTTYPEDESC.let { accttype ->
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromAccTypeIndex(
                        newIt,
                        accttype
                    ).let {
                        fivAccountTypeSP?.setSelection(it!!)
                    }

                }
            } else {
                (activity as HawActivity).accountTypeList = it
                fivAccountType.setItemForAccountType(it)
                (activity as HawActivity).sharedPreferenceManager.lovAccountType = it
                (activity as HawActivity).customerAccounts.ACCTTYPEDESC.let { accttype ->
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromAccTypeIndex(
                        it,
                        accttype
                    ).let {
                        fivAccountTypeSP?.setSelection(it!!)
                    }
                }
            }
            if ((activity as HawActivity).sharedPreferenceManager.lovCurrency.isNullOrEmpty()) {
                callCurrency("PKR")
            } else {
                setCurrency((activity as HawActivity).sharedPreferenceManager.lovCurrency)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    fun callRelationship() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.RELATIONSHIP_IDENTIFIER
        HBLHRStore.instance?.getRelationship(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : RelationshipCallBack {
                override fun RelationshipSuccess(response: RelationshipResponse) {
                    response.data?.let {
                        setRelationsip(it)
                    };
                }

                override fun RelationshipFailure(response: BaseResponse) {
                    Utils.failedAwokeCalls((activity as HawActivity)) { callRelationship() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setRelationsip(it: ArrayList<Relationship>) {
        try {
            (activity as HawActivity).relationshipList = it
            fivRelationshipAccounts.setItemForRelationship(it)
            (activity as HawActivity).sharedPreferenceManager.lovRelationship = it
            (activity as HawActivity).customerAccounts.RELATIONSHIP_DESC.let { relation ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromRelationshipByDescIndex(
                    it,
                    relation
                ).let {
                    fivRelationshipAccountsSP?.setSelection(it!!)

                }
            }
            (activity as HawActivity).sharedPreferenceManager.customerAccount.NAME_PROSPECTIVE_REMITTER.let { t ->
                fiVNameOfPropectiveRemittenceET?.setText(
                    t
                )
            }
            (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setCurrency(it: ArrayList<Currency>) {
        try {
//        fivCurrencySP?.setSelection(1)
            (activity as HawActivity).currencyList = it
            fivCurrency.setItemForCurrency(it)
            (activity as HawActivity).sharedPreferenceManager.lovCurrency = it


            (activity as HawActivity).customerAccounts.CURRENCY_DESC.let { currDesc ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromCurrencyIndex(
                    it,
                    currDesc
                ).let {
                    fivCurrencySP?.setSelection(it!!)
                    fivCurrencySP?.setSelection(1)
                }

            }


            if ((activity as HawActivity).sharedPreferenceManager.lovDepositType.isNullOrEmpty()) {
                callDepositType()

            } else {
                setDepositType((activity as HawActivity).sharedPreferenceManager.lovDepositType)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }

    }

    private fun setDepositType(it: ArrayList<DepositType>) {
        try {
            (activity as HawActivity).depositTypeList = it
            fivInitialSource.setItemForDepositType(it)
            (activity as HawActivity).sharedPreferenceManager.lovDepositType = it
            if (!(activity as HawActivity).customerAccounts.INITDEPSOURCEDESC.isNullOrEmpty()) {
                (activity as HawActivity).customerAccounts.INITDEPSOURCEDESC.let { item ->
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromDepTypeIndex(
                        it,
                        item
                    ).let {
                        if (it != 0) {
                            fivInitialSourceSP?.setSelection(it!!)
                        }
                    }
                }
            } else {
                "Cash".let { item ->
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromDepTypeIndex(
                        it,
                        item
                    ).let {
                        if (it != 0) {
                            fivInitialSourceSP?.setSelection(it!!)
                        }
                    }
                }
            }
            (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }


    private fun setConditions() {
        //added by Abdullah on the request of Saqib Bhai (If account no is available so Short Title and Account type will be disable)
        if (!(activity as HawActivity).sharedPreferenceManager.customerAccount.ACCT_NO.isNullOrEmpty()) {
            (activity as HawActivity).globalClass?.setDisbaled(fiVAccountTitleET!!)
            (activity as HawActivity).globalClass?.setDisbaled(fiVAccountTitle!!)
            (activity as HawActivity).globalClass?.setDisbaled(fivAccountTypeSP!!)
            (activity as HawActivity).globalClass?.setDisbaled(fivAccountType!!)
        } else {
            (activity as HawActivity).globalClass?.setEnabled(fiVAccountTitleET!!, true)
            (activity as HawActivity).globalClass?.setEnabled(fiVAccountTitle!!, true)
            (activity as HawActivity).globalClass?.setEnabled(fivAccountTypeSP!!, true)
            (activity as HawActivity).globalClass?.setEnabled(fivAccountType!!, true)
        }


        fivAccountTypeSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                try {
                    (activity as HawActivity).globalClass?.findSpinnerCodeFromAccountType(
                        (activity as HawActivity).accountTypeList,
                        fivAccountTypeSP?.selectedItem.toString()
                    ).let {
                        if (it != resources!!.getString(R.string.select) && it != "0") {
                            (activity as HawActivity).globalClass?.setDisbaled(btNext!!)
                            // when select there is no item selected "Choose an item"  <--- hint
                            var dualNationality =
                                (activity as HawActivity).sharedPreferenceManager.customerInfo.DUAL_NATIONALITY
                            var COUNTRY_OF_RES =
                                (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES
                            var accountType =
                                (activity as HawActivity).globalClass?.findSpinnerPositionFromAccountTypeCode(
                                    (activity as HawActivity).sharedPreferenceManager.lovAccountType,
                                    fivAccountTypeSP?.selectedItem.toString()
                                )
                            //TODO: This account type is not allowed for dual national customers
                            if ((dualNationality == "1" && COUNTRY_OF_RES != "PK") && (accountType.equals(
                                    "7R",
                                    true
                                ) || accountType.equals("7S", true))
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.dualNationalityCORAccErr),
                                    1
                                )
                            } else if ((dualNationality == "1") && (accountType.equals(
                                    "7R",
                                    true
                                ) || accountType.equals("7S", true))
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.dualNationalityAccErr),
                                    1
                                )
                            } else if ((COUNTRY_OF_RES != "PK") && (accountType.equals(
                                    "7R",
                                    true
                                ) || accountType.equals("7S", true))
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.corAccErr),
                                    1
                                )
                            } else {
                                //TODO: If customer second(Dual)YES nationality yes then Account type will not be selected "7R", "7S"
                                if (accountType.equals("7S", true) || accountType.equals(
                                        "7V",
                                        true
                                    )
                                ) {
                                    callRelationship()
                                    fiVNameOfPropectiveRemittence!!.visibility = View.VISIBLE
                                    fiVNameOfPropectiveRemittenceET!!.visibility = View.VISIBLE
                                    fivRelationshipAccounts!!.visibility = View.VISIBLE
                                    fivRelationshipAccountsSP!!.visibility = View.VISIBLE

                                } else {
                                    fiVNameOfPropectiveRemittenceET?.setText("")
                                    fivRelationshipAccountsSP?.setSelection(0)
                                    fiVNameOfPropectiveRemittenceET!!.visibility = View.GONE
                                    fiVNameOfPropectiveRemittence!!.visibility = View.GONE
                                    fivRelationshipAccounts!!.visibility = View.GONE
                                    fivRelationshipAccountsSP!!.visibility = View.GONE
                                }


                                //TODO: WHen ADA and ADRA select initial deposit will not be mantory field
                                if (accountType.equals("7R", true) || accountType.equals(
                                        "7S",
                                        true
                                    )
                                ) {
                                    fiVDepositSource.mandateInput.text = ""
                                } else {
                                    fiVDepositSource.mandateInput.text = "*"
                                }

                                if (fivAccountTypeSP!!.selectedItem.toString()
                                        .contains(
                                            "Nisa",
                                            true
                                        ) && (activity as HawActivity).customerInfo.GENDER == "M"
                                ) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        resources!!.getString(R.string.nisaErrorWhenGenderIsMale),
                                        1
                                    )
                                    fivAccountTypeSP?.setSelection(0)
                                } else {
                                    AccountTypeCode = it.toString()
                                    var verifyAccount = VerifyAccountRequest()
//                                verifyAccount.cnic = "1230756789233"
                                    verifyAccount.cnic =
                                        (activity as HawActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_NO
                                    verifyAccount.ACCT_TYPE = it.toString()
                                    verifyAccount.identifier =
                                        Constants.TYPE_ACCOUNT_VALIDATION_IDENTIFIER_HAW
                                    HBLHRStore.instance?.VerifyAccount(
                                        RetrofitEnums.URL_HBL,
                                        verifyAccount,
                                        object : VerifyAccountCallBack {
                                            override fun VerifyAccountSuccess(response: VerifyAccountResponse) {
                                                if (response.status == "00") {
//                                                    response.data?.getOrNull(0)?.PRIORITY_FLAG.let { check ->
//                                                        (activity as HawActivity).sharedPreferenceManager.branchCode.PRIORITY.let {
//                                                            if (check == true && (it == "1" || it == "2")) {
//                                                                fivPrioritySP?.visibility =
//                                                                    View.VISIBLE
//                                                                fivPriority?.visibility =
//                                                                    View.VISIBLE
//                                                            } else {
//                                                                fivPrioritySP?.visibility =
//                                                                    View.GONE
//                                                                fivPriority?.visibility = View.GONE
//                                                            }
//                                                        }
//                                                    }
                                                    if (position != 0 && (activity as HawActivity).accountTypeList.isNotEmpty()) {
                                                        callCurrency(
                                                            (activity as HawActivity).accountTypeList.get(
                                                                position
                                                            ).ACCT_CCY.toString()
                                                        )
                                                    }
                                                    (activity as HawActivity).globalClass?.setEnabled(
                                                        btNext!!,
                                                        true
                                                    )
                                                } else {
                                                    ToastUtils.normalShowToast(
                                                        context,
                                                        response.message.toString(), 1
                                                    )
                                                    (activity as HawActivity).globalClass?.setDisbaled(
                                                        btNext!!
                                                    )
                                                }
                                            }

                                            override fun VerifyAccountFailure(response: BaseResponse) {
                                                (activity as HawActivity).globalClass?.hideLoader()
                                                //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                                                ToastUtils.normalShowToast(
                                                    context,
                                                    response.message.toString(), 1
                                                )
                                            }

                                        })


                                    if ((AccountTypeCode == "GW" || AccountTypeCode == "EW"
                                                || AccountTypeCode == "IW" || AccountTypeCode == "GQ"
                                                || AccountTypeCode == "GR" || AccountTypeCode == "GS") && (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
                                            0
                                        ).COUNTRY_OF_RES != "PK"
                                    ) {
                                        ToastUtils.normalShowToast(
                                            context,
                                            "For Nisa Country should be pakistan", 1
                                        )
                                    }
                                }
                            }
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        fivInitialSourceSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (pos == 0) {
                        (activity as HawActivity).globalClass?.setDisbaled(fiVDepositSourceET!!)
                    } else {
                        (activity as HawActivity).globalClass?.setEnabled(
                            fiVDepositSourceET!!,
                            true
                        )
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        })
    }

    private fun setLengthAndTpe() {
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fiVAccountTitleET!!,
            15,
            Constants.INPUT_TYPE_ALPHA_BLOCK
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fiVDepositSourceET!!,
            13,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fiVNameOfPropectiveRemittenceET!!,
            35,
            Constants.INPUT_TYPE_ALPHA_BLOCK
        )
    }

    fun callCIF() {
        var cifRequest = CIFRequest()
        cifRequest.payload = Constants.PAYLOAD
        cifRequest.scnic = GlobalClass.id_do
        HBLHRStore.instance?.getCIF(RetrofitEnums.URL_HBL, cifRequest, object : CIFCallBack {
            override fun CIFSuccess(response: AofAccountInfoResponse) {

//
//                SparkDataSetupInSp(activity as HawActivity, response)
//                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo =
//                    response.data.get(0)
                callAccountOperationsType()
//                GlobalClass.FilledEDD = true
//

            }

            override fun CIFFailure(response: BaseResponse) {
                ToastUtils.normalShowToast(
                    context,
                    response.message.toString(), 1
                )
//                Utils.failedAwokeCalls((activity as HawActivity)) { callCIF() }
                (activity as HawActivity).globalClass?.hideLoader()

            }
        })

    }

    fun callAccountOperationsType() {
        try {
            var lovRequest = LovRequest()
            lovRequest.identifier = Constants.ACCOUNT_OPERATIONS_TYPE_IDENTIFIER
            HBLHRStore.instance?.getAccountOperationsType(
                RetrofitEnums.URL_HBL,
                lovRequest,
                object : AccountOperationsTypeCallBack {
                    override fun AccountOperationsTypeSuccess(response: AccountOperationsTypeResponse) {
                        response.data?.let {
                            setAccountOperationsType(it)
//                        var list = ArrayList<AccountOperationsType>()
//                        for (i in 0 until it.size) {
//                            if (it[i].ACCT_OPER_TYPE_DESC == "Individual Account" || it[i].ACCT_OPER_TYPE_DESC == "Joint account" || it[i].ACCT_OPER_TYPE_DESC == "Minor Account") {
//                                list.add(it[i])
//                            }
//                        }
//                        it.removeAt(3)


                        };

                    }

                    override fun AccountOperationsTypeFailure(response: BaseResponse) {
                        //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                        ToastUtils.normalShowToast(activity, response.message, 1)
                        (activity as HawActivity).globalClass?.hideLoader()
                    }

                })
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    fun getAccountType() {
        var isCompanyPage = false
        var lovRequest = LovRequest()
        if (((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_CODE == "01")) {
            lovRequest.condition = true

        } else if (((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_CODE == "02")) {
            lovRequest.condition = false
        }
        lovRequest.conditionname = "PURP_OF_CIF"
        lovRequest.conditionvalue =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_CODE
        lovRequest.BRANCH_CODE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getBRANCHCODE()!!
//        lovRequest.BRANCH_CODE = "5000"
        lovRequest.CUST_SEGMENT =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
//        lovRequest.identifier = Constants.ACCOUNT_TYPE_IDENTIEFIER
//        if(requireArguments()!=null) {
//            isCompanyPage = requireArguments().getBoolean(Constants.ACCOUNT_PAGE)
//        }
//        if(isCompanyPage){
        //If ETB case use diff account type with bulkaccttype
//        lovRequest = LovRequest(lovRequest)
        lovRequest.identifier = Constants.ACCOUNT_TYPE_BULK_ACCOUNT_IDENTIEFIER
//        }
        var gson = Gson().toJson(lovRequest)
        HBLHRStore.instance?.getAccountType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : AccountTypeCallBack {
                override fun AccountTypeSuccess(response: AccountTypeResponse) {
                    response.data?.let {
                        setAccountType(it)
                    };


                }

                override fun AccountTypeFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    Toast.makeText(
                        (activity as HawActivity).globalClass,
                        response.message,
                        Toast.LENGTH_SHORT
                    ).show()
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callCurrency(curreny: String) {
        var lovRequest = CurrenyLovRequest()
        lovRequest.find.CURR = curreny
        lovRequest.identifier = Constants.CURRENCY_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(lovRequest)
        HBLHRStore.instance?.getCurrency(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CurrencyCallBack {
                override fun CurrencySuccess(response: CurrencyResponse) {
                    response.data?.let {

                        setCurrency(it)

                    };


                }

                override fun CurrencyFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }


    fun callDepositType() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.DEPOSIT_TYPE_IDENTIFIER
        HBLHRStore.instance?.getDepositType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : DepositTypeCallBack {
                override fun DepositTypeSuccess(response: DepositTypeResponse) {
                    response.data?.let {
                        setDepositType(it)
                    };

                }

                override fun DepositTypeFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }


    fun loadDummyData() {
//        fiVAccountTitleET?.setText("Syed Muhammad Abdullah")
//        fiVDepositSourceET?.setText("5000")


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btRiskRating -> {
                try {
                    (this.context as HawActivity).globalClass?.showDialog(this.context)
                    Executors.newSingleThreadExecutor().execute(Runnable {
                        setRiskAllModelsInAOFRequestModel()
                    })
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
            R.id.btNext -> {
                try {
                    var accountType = ""
                    (activity as HawActivity).globalClass?.findSpinnerCodeFromAccountType(
                        (activity as HawActivity).accountTypeList,
                        fivAccountTypeSP?.selectedItem.toString()
                    ).let {
                        if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {
                            accountType = it.toString()
                        }
                    }
                    if (validation(accountType)) {
                        if (accountCode(accountType)) {
                            (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                            (this.context as HawActivity).globalClass?.showDialog(this.context)
                            Executors.newSingleThreadExecutor().execute(Runnable {
                                saveAndNext()
                                setAllModelsInAOFRequestModel()
                                (activity as HawActivity).runOnUiThread {
                                    (activity as HawActivity).globalClass?.setEnabled(
                                        btNext!!,
                                        true
                                    )
                                }
                            })
                        }

                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }

            }
            /*    } else {
                ToastUtils.normalShowToast(activity, "Please check the risk rating first.")
            }*/
            R.id.btBack -> accountBackpage()

//            R.id.btBack -> findNavController().popBackStack()
        }

    }

    private fun accountCode(accountType: String): Boolean {
        (activity as HawActivity).globalClass?.findSpinnerCodeFromAccountType(
            (activity as HawActivity).accountTypeList,
            fivAccountTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                AccountTypeCode = it.toString()

            }
        }

        if (AccountTypeCode == "GW") {
            if ((accountType != "7R" && accountType != "7S") && !TextUtils.isEmpty(
                    fiVDepositSourceET?.text.toString()
                ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() >= 1000
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    activity,
                    "Minimum Initial Deposit should be 1,000 ",
                    1
                )
                return false
            }
        } else if (AccountTypeCode == "EW") {
            if ((accountType != "7R" && accountType != "7S") && !TextUtils.isEmpty(
                    fiVDepositSourceET?.text.toString()
                ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() >= 5000
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    activity,
                    "Minimum Initial Deposit should be 5,000 ",
                    1
                )
                return false
            }
        } else if (AccountTypeCode == "IW") {
            if ((accountType != "7R" && accountType != "7S") && !TextUtils.isEmpty(
                    fiVDepositSourceET?.text.toString()
                ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() >= 5000
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    activity,
                    "Minimum Initial Deposit should be 5,000 ",
                    1
                )
                return false
            }
        } else if (AccountTypeCode == "GQ" || AccountTypeCode == "GR" || AccountTypeCode == "GS") {
            if ((accountType != "7R" && accountType != "7S") && !TextUtils.isEmpty(
                    fiVDepositSourceET?.text.toString()
                ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() >= 1000
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    activity,
                    "Minimum Initial Deposit should be 100 ",
                    1
                )
                return false
            }
        } else {
            return true
        }
    }

    private fun validation(accountType: String): Boolean {
        var depositSource = 0
        if (fivAccountOTypeSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select operational type",
                1
            )
            return false
        } else if (TextUtils.isEmpty(fiVAccountTitleET?.text.toString())) {
            ToastUtils.normalShowToast(
                activity,
                "Please enter short title",
                1
            )
            return false
        } else /*if (fivPrioritySP?.selectedItemPosition == 0 && fivPrioritySP?.isVisible == true) {
            ToastUtils.normalShowToast(
                activity,
                "Please Select Priority First",
                1
            )
            return false
        } else*/ if (fivAccountTypeSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select account type",
                1
            )
            return false
        } else if (fiVNameOfPropectiveRemittenceET?.visibility == View.VISIBLE && fiVNameOfPropectiveRemittenceET?.text.isNullOrEmpty()) {
            fiVNameOfPropectiveRemittenceET?.error =
                resources.getString(R.string.fiVNameOfPropectiveRemittenceErr)
            fiVNameOfPropectiveRemittenceET?.requestFocus()
            return false
        } else if (fivRelationshipAccountsSP?.visibility == View.VISIBLE && fivRelationshipAccountsSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select RELATIONSHIP WITH PROSPECTIVE REMITTER",
                1
            )
            return false
        } /*else if (fivInitialSourceSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select initial source type",
                1
            )
            return false
        } */ else if ((accountType != "7R" && accountType != "7S") && TextUtils.isEmpty(
                fiVDepositSourceET?.text.toString()
            )
        ) {
            ToastUtils.normalShowToast(
                activity,
                "Please enter deposit source",
                1
            )
            return false
        } /*else if ((accountType != "7R" || accountType != "7S") && !TextUtils.isEmpty(
                fiVDepositSourceET?.text.toString()
            ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() == 0
        ) {
            ToastUtils.normalShowToast(
                activity,
                "Initial deposit should not be 0",
                1
            )
            return false
        } */ else if ((AccountTypeCode == "GW" || AccountTypeCode == "EW"
                    || AccountTypeCode == "IW" || AccountTypeCode == "GQ"
                    || AccountTypeCode == "GR" || AccountTypeCode == "GS") && (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES != "PK"
        ) {
            ToastUtils.normalShowToast(context, "For NISA country should be pakistan", 1)
            return false
        } else
            return true
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                accountBackpage()
                true
            }
            false
        }
    }

    fun accountBackpage() {
        val bundle = Bundle()
        bundle.putParcelable(Constants.NADRA_RESPONSE_DATA, nadraVerify)
        if ((activity as HawActivity).backInt == 2) {
            if (findNavController().currentDestination?.id == R.id.CIFStep7)
                findNavController().navigate(R.id.action_CIFStep7_to_CIFStep1_5, bundle)
        } else if ((activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(0)?.ETBNTBFLAG == "C") {
            if (findNavController().currentDestination?.id == R.id.CIFStep7)
                findNavController().navigate(R.id.action_CIFStep7_to_Company_page)
        } else if (!(activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .equals(
                    "High",
                    true
                )
        ) {
            if (findNavController().currentDestination?.id == R.id.CIFStep7)
                findNavController().navigate(R.id.action_CIFStep7_to_CIFStep5_7)
        } else {
            if (findNavController().currentDestination?.id == R.id.CIFStep7)
                findNavController().navigate(R.id.action_CIFStep7_to_CIFStep6_11)
        }
    }


    fun saveAndNext() {
        var accountType = ""
        (activity as HawActivity).globalClass?.findSpinnerCodeFromAccountType(
            (activity as HawActivity).accountTypeList,
            fivAccountTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {
                accountType = it.toString()
            }
        }
//        (activity as HawActivity).cnicNumber.let {
//            (activity as HawActivity).customerAccounts.ID_DOC_NO = it!!
//        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromAccountOperationTypeCode(
            (activity as HawActivity).accountOperationTypeList,
            fivAccountOTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select therCe is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.ACCTTYPEDESC =
                    fivAccountOTypeSP?.selectedItem.toString()
                (activity as HawActivity).customerAccounts.ACCT_TYPE = it.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromAccountOperationTypeCode(
            (activity as HawActivity).accountOperationTypeList,
            fivAccountOTypeSP?.selectedItem.toString()
        ).let {

            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select therCe is no item selected "Choose an item"  <--- hint
                //TODO: when Customer Segment Minor then accoint operational type select 3 and Minor Account
                if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
                    (activity as HawActivity).customerAccounts.ACCT_SNG_JNT_DESC =
                        "Minor Account"
                    (activity as HawActivity).customerAccounts.ACCT_SNG_JNT = "3"
                } else {
                    (activity as HawActivity).customerAccounts.ACCT_SNG_JNT_DESC =
                        fivAccountOTypeSP?.selectedItem.toString()
                    (activity as HawActivity).customerAccounts.ACCT_SNG_JNT = it.toString()
                }
                (activity as HawActivity).customerAccounts.ACCTTYPEDESC = it.toString()

//                (activity as HawActivity).customerAccounts.ACCT_SNG_JNT_DESC = fivAccountOTypeSP?.selectedItem.toString()
//                (activity as HawActivity).customerAccounts.ACCT_SNG_JNT = it.toString()

                (activity as HawActivity).customerAccounts.ACCT_OPER_TYPE_CODE = it.toString()
                (activity as HawActivity).customerAccounts.ACCT_OPER_TYPE_DESC =
                    fivAccountOTypeSP?.selectedItem.toString()
            }
        }


//for oper code
//        (activity as HawActivity).globalClass?.findSpinnerPositionFromAccountOperationTypeCode(
//            (activity as HawActivity).accountOperationTypeList,
//            fivAccountOTypeSP?.selectedItem.toString()
//        ).let {
//            if (it != resources!!.getString(R.string.select)) {// when select therCe is no item selected "Choose an item"  <--- hint
//                (activity as HawActivity).customerAccounts.ACCT_OPER_TYPE_CODE =
//                    it.toString()
//                Log.i("abcde", it.toString())
//            }
//        }

        fiVAccountTitleET?.text.toString()
            .let { (activity as HawActivity).customerAccounts.TITLE_OF_ACCT = it }


        (activity as HawActivity).globalClass?.findSpinnerPositionFromAccountTypeCode(
            (activity as HawActivity).accountTypeList,
            fivAccountTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.ACCT_TYPE = it.toString()
                (activity as HawActivity).customerAccounts.ACCTTYPEDESC =
                    fivAccountTypeSP?.selectedItem.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromRelationshipCode(
            (activity as HawActivity).relationshipList,
            fivRelationshipAccountsSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.RELATIONSHIP_CODE = it.toString()
                (activity as HawActivity).customerAccounts.RELATIONSHIP_DESC =
                    fivRelationshipAccountsSP?.selectedItem.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromPriorityCode(
            (activity as HawActivity).priorityList,
            fivPrioritySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select therCe is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.PRITY_DESC =
                    fivPrioritySP?.selectedItem.toString()
                (activity as HawActivity).customerAccounts.PRITY_CODE = it.toString()
                if (it == "P" || it == "V") {
                    (activity as HawActivity).customerAccounts.IS_PRIORITY = "1"
                } else {
                    (activity as HawActivity).customerAccounts.IS_PRIORITY = "0"
                }
            } else {
                (activity as HawActivity).customerAccounts.IS_PRIORITY = "0"
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromAccountIslamic(
            (activity as HawActivity).accountTypeList,
            fivAccountTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.CONVENTIONAL_ISLAMIC =
                    it.toString()
                if (it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as HawActivity).customerAccounts.CONVENTIONAL_ISLAMIC =
                        it.toString()

                }
            }
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCurrencyOfFundCode(
            (activity as HawActivity).currencyList,
            fivCurrencySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.CURRENCY = it.toString()


            }
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCurrencyOfCode(
            (activity as HawActivity).currencyList,
            fivCurrencySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerAccounts.CURRENCY_DESC = it.toString()
                (activity as HawActivity).customerAccounts.ACCT_CCY = it.toString()

            }
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromDepositTypeCode(
            (activity as HawActivity).depositTypeList,
            fivInitialSourceSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint

                (activity as HawActivity).customerAccounts.INIT_DEP_SOURCE = it.toString()
                (activity as HawActivity).customerAccounts.INITDEPSOURCEDESC = fivInitialSourceSP?.selectedItem.toString()

            }
        }

        (activity as HawActivity).customerInfo.ID_DOCUMENT_NO.let {
            (activity as HawActivity).customerAccounts.ID_DOC_NO
        }

        fiVDepositSourceET?.text.toString()
            .let {
                if ((it == "" || it == "0") && (accountType == "7R" || accountType == "7S")) {
                    //TODO: If user do not add the initials deposit in 7R or 7S case then value remain insert as "0"
                    (activity as HawActivity).customerAccounts.INIT_DEPOSIT = "0"
                    Log.i("CheckValuessss", "$accountType $it")
                } else {
                    Log.i("CheckValuessss", "other $it")
                    (activity as HawActivity).customerAccounts.INIT_DEPOSIT = it
                }

            }
        fiVNameOfPropectiveRemittenceET?.text.toString()
            .let {
                (activity as HawActivity).customerAccounts.NAME_PROSPECTIVE_REMITTER = it
            }
        /*riskStatusET?.text.toString().let { }*/
        (activity as HawActivity).sharedPreferenceManager.customerAccount =
            (activity as HawActivity).customerAccounts
    }

    fun setRiskAllModelsInAOFRequestModel() {

        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }

        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        aofRiskDataAlign()
    }

    fun aofRiskDataAlign() {
        (activity as HawActivity).aofAccountInfoRequest.CHANNEL = "2"
        (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
            ?.get(0)?.itemDescription.let {
                (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it!!
            }
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).etbntbFLAG
        } else {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).REMEDIATE
        } else {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as HawActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID()
                .toString()
        (activity as HawActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE()
                .toString()
        //TODO: set aofAccountInfo.NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).fullName
        } else {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }//        aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
//        aofAccountInfo.RISK_RATING_TOTAL = (activity as HawActivity).riskRatingTotal!!
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            //TODO: set BRANCH_CODE
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it!!
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        callToRiskRatingResponse((activity as HawActivity).aofAccountInfoRequest)
    }

    fun callToRiskRatingResponse(aofAccountInfo: Data) {
        var aofAccountInfoRequest = AofAccountInfoRequest()
        aofAccountInfoRequest.identifier = Constants.AOF_SUBMIT_IDENTIFIER
        aofAccountInfoRequest.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(aofAccountInfoRequest)
        HBLHRStore.instance?.postRiskRating(
            RetrofitEnums.URL_HBL,
            aofAccountInfoRequest, object : CustomerRiskRatingCallBack {
                @SuppressLint("WrongConstant")
                override fun CustomerRiskRatingSuccess(response: RiskRatingResponse) {
                    try {
                        ToastUtils.normalShowToast(activity, response.message, 2)
                        (activity as HawActivity).globalClass?.hideLoader()
                        response.data?.get(0)?.RISK_RATING?.let {
                            (activity as HawActivity).riskRating = it
                            riskCheck(it)
                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun CustomerRiskRatingFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun riskCheck(risk: String?) {
        riskStatusET?.isEnabled = false
        if (risk.equals("Medium", true)) {
            riskStatusET?.setText("Medium")
            riskStatusET?.setTextColor(Color.parseColor("#008577"));
        } else {
            riskStatusET?.setText("High")
            riskStatusET?.setTextColor(Color.parseColor("#D53F45"));
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        aofDataAlign()
    }

    fun aofDataAlign() {
        (activity as HawActivity).aofAccountInfoRequest.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                ?.get(0)
                ?.itemDescription.let {
                    (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).etbntbFLAG
        } else {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).REMEDIATE
        } else {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as HawActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID()
                .toString()
        (activity as HawActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE()
                .toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).fullName
        } else {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).riskRating.toString()
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }
        if ((activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(0)?.ETBNTBFLAG == "C") {
            (activity as HawActivity).sharedPreferenceManager.appStatus.Acct.let {
                (activity as HawActivity).globalClass?.appStatus = it.toString()
            }
        } else if ((activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(0)?.ETBNTBFLAG == "N") {
            (activity as HawActivity).sharedPreferenceManager.appStatus.CIFAcct.let {
                (activity as HawActivity).globalClass?.appStatus = it.toString()
            }
        }
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF((activity as HawActivity).aofAccountInfoRequest)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        if (findNavController().currentDestination?.id == R.id.CIFStep7)
                            findNavController().navigate(R.id.action_CIFStep7_to_CIFStep7_2)
                        (activity as HawActivity).recyclerViewSetup()
                        (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
//                    callRM()
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
//                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}