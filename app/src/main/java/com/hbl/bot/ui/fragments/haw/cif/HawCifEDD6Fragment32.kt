package com.hbl.bot.ui.fragments.cif


import com.hbl.bot.ui.activities.HawActivity
import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.GenerateCIFCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.GenerateCIFResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep6_9.*
import kotlinx.android.synthetic.main.fragment_cifstep6_9.btBack
import kotlinx.android.synthetic.main.fragment_cifstep6_9.btNext
import kotlinx.android.synthetic.main.fragment_cifstep6_9.fivOtherSource
import kotlinx.android.synthetic.main.fragment_cifstep6_9.formSectionHeader
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class HawCifEDD6Fragment32 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivSalaryIncomeET: EditText? = null
    var fivRentalIncomeET: EditText? = null
    var fivBusinessIncomeET: EditText? = null
    var fivRetirementFundET: EditText? = null
    var fivPensionET: EditText? = null
    var fivInheritanceET: EditText? = null
    var fivOtherET: EditText? = null
    var fivOtherSourceEt: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep6_9, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }


    }

    fun header() {
        var custSegType =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        val txt = resources.getString(R.string.edd_financial_supporter_source_of_income)
        if (custSegType == "A0" || /*STUDENT*/ custSegType == "A2" || /*HOUSE WIFE*/ custSegType == "A4" /*UNEMPLOYED*/) {
            viewModel.totalSteps.setValue(7)
            viewModel.currentFragmentIndex.setValue(5)
            val txt1 = " (6/7 Page)"
            val txt2 = txt + txt1
            formSectionHeader.header_tv_title.text =
                GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity)
                .registerReceiver(mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST)
                );
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            init()
            load()
            setLengthAndType()
           /* if (GlobalClass.isDummyDataTrue) {
                loadDummyData()
            }*/
            onBackPress(view)
            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun load() {
        (activity as HawActivity).customerEdd.sAL_OF_FIN_SUPPORTER_EXP_AMT
            .let { fivSalaryIncomeET?.setText(it) }
        (activity as HawActivity).customerEdd.rENTAL_INCOME_FIN_SUPPORTER_EXP_AMT
            .let {
                fivRentalIncomeET?.setText(it)
            }
        (activity as HawActivity).customerEdd.bUSS_INCOME_FIN_SUPPORTER_EXP_AMT
            .let {
                fivBusinessIncomeET?.setText(it)
            }
        (activity as HawActivity).customerEdd.rETIREMENT_FUND_EXP_AMT
            .let { fivRetirementFundET?.setText(it) }
        (activity as HawActivity).customerEdd.pENSION_EXP_AMT
            .let { fivPensionET?.setText(it) }
        (activity as HawActivity).customerEdd.iNHERITANCE_EXP_AMT
            .let { fivInheritanceET?.setText(it) }
        (activity as HawActivity).customerEdd.oTHER_EXP_AMT
            .let { fivOtherET?.setText(it) }
        (activity as HawActivity).customerEdd.oTHER_SOURCE_NRP
            .let { fivOtherSourceEt?.setText(it) }
    }

    fun init() {
        fivSalaryIncomeET = fivSalaryIncome.getTextFromEditText(R.id.fivSalaryIncome)
        fivRentalIncomeET = fivRentalIncome.getTextFromEditText(R.id.fivRentalIncome)
        fivBusinessIncomeET = fivBusinessIncome.getTextFromEditText(R.id.fivBusinessIncome)
        fivRetirementFundET = fivRetirementFund.getTextFromEditText(R.id.fivRetirementFund)
        fivPensionET = fivPension.getTextFromEditText(R.id.fivPension)
        fivInheritanceET = fivInheritance.getTextFromEditText(R.id.fivInheritance)
        fivOtherET = fivOther.getTextFromEditText(R.id.otherID)
        fivOtherSourceEt = fivOtherSource.getTextFromEditText(R.id.otherResource)
        addTextWatcher(fivSalaryIncomeET!!)
        addTextWatcher(fivRentalIncomeET!!)
        addTextWatcher(fivBusinessIncomeET!!)
        addTextWatcher(fivRetirementFundET!!)
        addTextWatcher(fivPensionET!!)
        addTextWatcher(fivInheritanceET!!)
    }
    private fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
            (activity as HawActivity).globalClass?.setDisbaled(fivSalaryIncomeET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivRentalIncomeET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivBusinessIncomeET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivRetirementFundET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivPensionET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivInheritanceET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivOtherET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivOtherSourceEt!!, false)
        }
    }
    fun setLengthAndType() {
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivSalaryIncomeET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivRentalIncomeET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivBusinessIncomeET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivRetirementFundET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivPensionET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivInheritanceET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivOtherET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivOtherSourceEt!!,
            25,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
    }

    fun loadDummyData() {
        fivSalaryIncomeET?.setText("75000")
        fivRentalIncomeET?.setText("12000")
        fivBusinessIncomeET?.setText("120000")
        fivRetirementFundET?.setText("500000")
        fivPensionET?.setText("2500")
        fivInheritanceET?.setText("2000")
        fivOtherET?.setText("1500")
        fivOtherSourceEt?.setText("50000")
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).globalClass?.setDisbaled(btNext, false)
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            activity?.runOnUiThread {
                                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep6_9)
                    findNavController().navigate(R.id.action_CIFStep6_9_to_CIFStep6_7)
        }

    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep6_9)
                    findNavController().navigate(R.id.action_CIFStep6_9_to_CIFStep6_7)
                true
            }
            false
        }
    }

    private fun addTextWatcher(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (editText?.text.isNotEmpty()) {
                        fivSalaryIncome.mandateInput.text = ""
                        fivBusinessIncome.mandateInput.text = ""
                        fivRentalIncome.mandateInput.text = ""
                        fivRetirementFund.mandateInput.text = ""
                        fivPension.mandateInput.text = ""
                        fivInheritance.mandateInput.text = ""
                    } else {
                        fivSalaryIncome.mandateInput.text = "*"
                        fivRentalIncome.mandateInput.text = "*"
                        fivBusinessIncome.mandateInput.text = "*"
                        fivRetirementFund.mandateInput.text = "*"
                        fivPension.mandateInput.text = "*"
                        fivInheritance.mandateInput.text = "*"
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

    }

    fun saveAndNext() {
        fivSalaryIncomeET?.text.toString()
            .let { (activity as HawActivity).customerEdd.sAL_OF_FIN_SUPPORTER_EXP_AMT = it }
        fivRentalIncomeET?.text.toString()
            .let {
                (activity as HawActivity).customerEdd.rENTAL_INCOME_FIN_SUPPORTER_EXP_AMT = it
            }
        fivBusinessIncomeET?.text.toString()
            .let {
                (activity as HawActivity).customerEdd.bUSS_INCOME_FIN_SUPPORTER_EXP_AMT = it
            }
        fivRetirementFundET?.text.toString()
            .let { (activity as HawActivity).customerEdd.rETIREMENT_FUND_EXP_AMT = it }
        fivPensionET?.text.toString()
            .let { (activity as HawActivity).customerEdd.pENSION_EXP_AMT = it }
        fivInheritanceET?.text.toString()
            .let { (activity as HawActivity).customerEdd.iNHERITANCE_EXP_AMT = it }
        fivOtherET?.text.toString()
            .let { (activity as HawActivity).customerEdd.oTHER_EXP_AMT = it }
        fivOtherSourceEt?.text.toString()
            .let { (activity as HawActivity).customerEdd.oTHER_SOURCE_NRP = it }
        (activity as HawActivity).sharedPreferenceManager.setCustomerEDD((activity as HawActivity).customerEdd)
    }

    fun validation(): Boolean {
        var value1 = fivSalaryIncomeET?.text.toString().trim()
        var value2 = fivRentalIncomeET?.text.toString().trim()
        var value3 = fivBusinessIncomeET?.text.toString().trim()
        var value4 = fivRetirementFundET?.text.toString().trim()
        var value5 = fivPensionET?.text.toString().trim()
        var value6 = fivInheritanceET?.text.toString().trim()
        var value7 = fivOtherET?.text.toString().trim()
        var value8 = fivOtherSourceEt?.text.toString().trim()
        if (value1.length == 0 &&
            value2.length == 0 &&
            value3.length == 0 &&
            value4.length == 0 &&
            value5.length == 0 &&
            value6.length == 0 &&
            value7.length == 0 &&
            value8.length == 0
        ) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_specify_any_amount), 1
            )
            return false
        } else {
            return true
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as HawActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as HawActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as HawActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as HawActivity).aofAccountInfoRequest = it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as HawActivity).globalClass?.hideLoader()
                        if (findNavController().currentDestination?.id == R.id.CIFStep6_9)
                            findNavController().navigate(R.id.action_CIFStep6_9_to_CIFStep6_11)
                        (activity as HawActivity).recyclerViewSetup()
                        (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
