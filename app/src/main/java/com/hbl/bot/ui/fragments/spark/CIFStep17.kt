package com.hbl.bot.ui.fragments.spark


import android.content.ActivityNotFoundException
import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.navigation.fragment.findNavController

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.BoolCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.IndustryMainCategoryBack
import com.hbl.bot.network.ResponseHandlers.callbacks.IndustrySubCategoryBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.BoolResponse
import com.hbl.bot.network.models.response.base.IndustryMainCategoryResponse
import com.hbl.bot.network.models.response.base.IndustrySubCategoryResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import kotlinx.android.synthetic.main.fragment_cifstep1.btNext
import kotlinx.android.synthetic.main.fragment_cifstep17.*
import kotlinx.android.synthetic.main.fragment_cifstep1_3.btBack
import kotlinx.android.synthetic.main.fragment_cifstep7.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class CIFStep17 : Fragment(), View.OnClickListener {
    var hblhrStore: HBLHRStore = HBLHRStore()
    var myView: View? = null
    var riskStatusET: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep17, container, false)
                industryMainCategory()
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }finally {
            return myView
        }


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btNext.setOnClickListener(this)
        btBack.setOnClickListener(this)

    }

    fun industryMainCategory() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.INDUSTRY_MAIN_CATEGORY_IDENTIFIER
        hblhrStore?.getIndustryMainCategory(
            RetrofitEnums.URL_HBL,
            lovRequest, object : IndustryMainCategoryBack {
                override fun IndustryMainCategorySuccess(response: IndustryMainCategoryResponse) {
                    response.data?.let { fivCustomerMain.setItemForIndustryMainCategory(it) };
                    industrySubCategory()
                }

                override fun IndustryMainCategoryFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun industrySubCategory() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.INDUSTRY_SUB_CATEGORY_IDENTIFIER
        hblhrStore?.getIndustrySubCategory(
            RetrofitEnums.URL_HBL,
            lovRequest, object : IndustrySubCategoryBack {
                override fun IndustrySubCategorySuccess(response: IndustrySubCategoryResponse) {
                    response.data?.let { fivCustomerSub.setItemForIndustrySubCategory(it) };
                    callBool()
                }

                override fun IndustrySubCategoryyFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        hblhrStore.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let { fivMailInstruction.setItemForBools(it) };
                    init()
                    (activity as CIFRootActivity).globalClass?.hideLoader()

                }

                override fun BoolFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
//            R.id.btNext -> findNavController().navigate(R.id.action_CIFStep17_to_CIFStep18)
            R.id.btBack -> findNavController().popBackStack()
        }


    }

    fun init() {
        riskStatusET = fiVRiskRating.getTextFromEditText(R.id.riskStatusID)
        riskStatusET?.isEnabled = false
        if ((activity as CIFRootActivity).cnicNumber.equals("4210119523995")) {
            riskStatusET?.setText("Medium")
            riskStatusET?.setTextColor(Color.parseColor("#008577"));
        } else {
            riskStatusET?.setText("High")
            riskStatusET?.setTextColor(Color.parseColor("#D53F45"));
        }
    }
}

