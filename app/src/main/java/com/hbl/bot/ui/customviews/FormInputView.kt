package com.hbl.bot.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.KeyEvent
import android.widget.EditText
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.doOnTextChanged
import com.hbl.bot.R
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import kotlinx.android.synthetic.main.view_form_input.view.*


class FormInputView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    init {
        inflate(context, R.layout.view_form_input, this)
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.FormInputView)
        tv_title.text = attributes.getString(R.styleable.FormInputView_titleTextInput)
        tvTitleUrdu.text = attributes.getString(R.styleable.FormInputView_titleTextInputUrdu)
        mandateInput.text = attributes.getString(R.styleable.FormInputView_mandatoryInput)
        attributes.recycle()
        tv.doOnTextChanged { text: CharSequence?, start, count, after ->
            val length = text.toString().length
            if (length==1 && text!!.startsWith(" ")) {
                tv?.setText("")
            }
        }
    }

    override fun dispatchKeyEvent(event: KeyEvent?): Boolean {
        try {
            if (event!!.keyCode == KeyEvent.KEYCODE_BACK) {
                (context as CIFRootActivity).hideNavigationBar()
                (context as CIFRootActivity).hideKeyboard()
                return true;
            }
        }catch (e: java.lang.ClassCastException){

        }
//        if (event!!.keyCode == KeyEvent.KEYCODE_ENTER) {
//            (context as CIFRootActivity).hideNavigationBar()
//            (context as CIFRootActivity).hideKeyboard()
//            return true;
//        }
        return super.dispatchKeyEvent(event)
    }

    fun getTextFromEditText(id: Int): EditText {
        tv.id = id
        return tv
    }
    fun removeFirstChar(s: String): String? {
        return s.substring(1)
    }
    /*fun getTextFromEditText(): EditText {
        return tv
    }*/
    fun getTextFromTextView(): TextView {
        return tv
    }

}
