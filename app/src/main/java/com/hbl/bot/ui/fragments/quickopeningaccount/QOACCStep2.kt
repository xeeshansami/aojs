package com.hbl.bot.ui.fragments.quickopeningaccount
import android.content.ActivityNotFoundException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.hbl.bot.R
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.ui.activities.OpenQuickAccountActivity
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import kotlinx.android.synthetic.main.fragment_cifstep15.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

class QOACCStep2 : Fragment(), View.OnClickListener {
    var myView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try{
        if (myView == null) {
            // Inflate the layout for this fragment
            myView = inflater.inflate(R.layout.fragment_qoaccstep2, container, false)
        }
        } catch (e: UnsatisfiedLinkError) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        } finally {
            return myView
        }


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btGenerteAccount.setOnClickListener(this)

        (activity as OpenQuickAccountActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
            fivTrackingNumber.setText(
                it
            )
        }
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btGenerteAccount -> findNavController().navigate(R.id.action_QOACCStep2_QOACCStep3)
        }

    }

}