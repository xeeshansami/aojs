package com.hbl.bot.ui.adapter

import android.content.ActivityNotFoundException
import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.SpinnerAdapter
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.hbl.bot.R
import com.hbl.bot.network.models.response.baseRM.DocumentType
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import kotlinx.android.synthetic.main.view_spinner_upload.view.*
import java.io.FileNotFoundException
import java.io.IOException


class CustomDocumentUploadSpinnerAdapter(
    var context: Context,
    documentUpload: ArrayList<DocumentType>,
) : BaseAdapter(), SpinnerAdapter {
    var documentUpload = ArrayList<DocumentType>()
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var view:View?=null
        try {
            view = View.inflate(context, R.layout.view_spinner_upload, null)
            val textView = view!!.findViewById<TextView>(R.id.text1)
            textView.text = this.documentUpload[position].DOCDS
//        if (MainActivity.selectedCar == position) {
//            textView.setTextColor(Color.parseColor("#bdbdbd"))
//        }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(context, context.getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((context as CIFRootActivity), (e as Exception))
        } finally {
            return view!!
        }
    }

    init {
        this.documentUpload = documentUpload
    }

    //
    override fun isEnabled(position: Int): Boolean {


        return position == GlobalClass.selectedItem

        return false
    }


    override fun getItem(position: Int): Any {
        return documentUpload[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()

    }

    override fun getCount(): Int {
        return documentUpload.size
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        var globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass

        var view: View = View.inflate(context, R.layout.view_spinner_dropped_upload, null)
        val textView = view.findViewById<TextView>(R.id.text1)
        val tvCompleted = view.findViewById<TextView>(R.id.text2)
        tvCompleted.text = ""
        textView.text = documentUpload[position].DOCDS
//        var size = this.documentUpload.size
//        for (i in 0 until size) {
//            if (documentUpload[i].PARTIAL_KEY == true) {
//
//
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//                textView.isEnabled = true
//
//            } else {
//                textView.isEnabled = false
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }
//        if (position == 0) {
//            if (GlobalClass.IDDOC == 1) {
//
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//
//            }
//
//        }


        if (position == GlobalClass.selectedItem) {

            view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
        } else {
            textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))

        }


//        if (position == 1) {
//            if (GlobalClass.NADRA == 1) {
//
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }
//        if (position == 2) {
//            if (GlobalClass.ADDR == 1) {
//
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//
//            }
//        }
//        if (position == 3) {
//            if (GlobalClass.UNSC == 1) {
//
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }
//        if (position == 4) {
//            if (GlobalClass.AOFCIF == 1) {
//
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }
//        if (position == 5) {
//            if (GlobalClass.TnC == 1) {
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }
//        if (position == 6) {
//            if (GlobalClass.POI == 1) {
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }
//        if (position == 7) {
//            if (GlobalClass.EDDFILL == 1) {
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }
//        if (position == 8) {
//            if (GlobalClass.SSCARD == 1) {
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }
//        if (position == 9) {
//            if (GlobalClass.KFS == 1) {
//                view.text1.setTextColor(ContextCompat.getColor(context, R.color.colorBlack))
//            } else {
//                textView.setTextColor(ContextCompat.getColor(context, R.color.colorGrayText))
//            }
//        }

//        when(documentUpload[position]) {
//            DocumentCategoryState.OptionalCompleted -> {
//                tvCompleted.text = "Completed"
//            }
//            DocumentCategoryState.MandatoryCompleted -> {
//                tvCompleted.text = "Completed"
//                textView.setTextColor(Color.RED)
//            }
//            DocumentCategoryState.Mandatory -> {
//                textView.setTextColor(Color.RED)
//            }
//            DocumentCategoryState.Optional -> {
//                textView.setTextColor(Color.BLACK)
//
//            }
        return view
    }
//
}


