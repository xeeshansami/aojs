package com.hbl.bot.ui.adapter

import android.annotation.SuppressLint
import android.graphics.Paint
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hbl.bot.R
import com.hbl.bot.model.DrawerItem
import com.hbl.bot.model.DrawerModel
import kotlinx.android.synthetic.main.view_drawer_item.view.*

class DrawerAdapter(private val mList: MutableList<DrawerModel>) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    /*   open inner class HeaderViewHolder(view: View) : RecyclerView.ViewHolder(view) {
           val tvTitle = view.tvTitle!!
           val statusImg = view.statusImg2!!
       }*/


    open inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvTitle = view.tvItemTitle!!
        val statusImg = view.statusImg!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            /* 0, 1 -> {
                 val layout = LayoutInflater.from(parent.context)
                     .inflate(R.layout.view_drawer_header, parent, false)
 //                HeaderViewHolder(layout)
             }*/
            else -> {
                val layout = LayoutInflater.from(parent.context)
                    .inflate(R.layout.view_drawer_item, parent, false)
                ItemViewHolder(layout)
            }
        }
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun getItemViewType(position: Int): Int {
        return mList[position].item!!.ordinal
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (mList[position].item) {
            DrawerItem.HEADER_DISABLED -> {
                val headerHolder = holder as ItemViewHolder
                headerHolder.statusImg.setBackgroundResource(R.mipmap.ic_lineup)
                headerHolder.tvTitle.text = mList[position].title
//                headerHolder.tvTitle.paintFlags = Paint.UNDERLINE_TEXT_FLAG
                headerHolder.tvTitle.setTextColor(R.color.red)
            }
            DrawerItem.HEADER_PROCEEDING -> {
                val headerHolder = holder as ItemViewHolder
                headerHolder.statusImg.setBackgroundResource(R.mipmap.ic_processing)
                headerHolder.tvTitle.text = Html.fromHtml(mList[position].title)
                headerHolder.tvTitle.paintFlags =
                    headerHolder.tvTitle.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            }
            DrawerItem.ITEM_DONE -> {
                val itemViewHolder = holder as ItemViewHolder
                itemViewHolder.statusImg.setBackgroundResource(R.mipmap.ic_done1)
                itemViewHolder.tvTitle.text = Html.fromHtml(mList[position].title)
                itemViewHolder.tvTitle.paintFlags =
                    itemViewHolder.tvTitle.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            }
            DrawerItem.ITEM_LINEUP -> {
                val itemViewHolder = holder as ItemViewHolder
                itemViewHolder.tvTitle.text = Html.fromHtml(mList[position].title)
//                itemViewHolder.tvTitle.text =mList[position].title
                itemViewHolder.statusImg.setBackgroundResource(R.mipmap.ic_lineup)
                itemViewHolder.tvTitle.setTextColor(R.color.colorLightGray)
//                itemViewHolder.tvTitle.paintFlags =itemViewHolder.tvTitle.paintFlags or Paint.UNDERLINE_TEXT_FLAG
            }
        }
    }

}