package com.hbl.bot.ui.fragments.spark.accounts


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep7_2.*
import kotlinx.android.synthetic.main.fragment_cifstep7_2.btBack
import kotlinx.android.synthetic.main.fragment_cifstep7_2.btNext
import kotlinx.android.synthetic.main.fragment_cifstep7_2.fivRMCode
import kotlinx.android.synthetic.main.fragment_cifstep7_2.fivSundryAnalysis
import kotlinx.android.synthetic.main.fragment_cifstep7_2.formSectionHeader
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException
import java.util.concurrent.Executors
import kotlin.collections.ArrayList

class CIFStep7_2 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivRMCodeSP: Spinner? = null
    var sundrySP: Spinner? = null
    var fivPurposeOfAccountSP: Spinner? = null
    var fivNoOfChequeSP: Spinner? = null
    var fivModeOfDeliverySP: Spinner? = null
    var fivOtherPurposeOfAccountET: EditText? = null
    var fivSMSSP: Spinner? = null
    var fivPhoneBankSP: Spinner? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep7_2, container, false)


            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(5)
        viewModel.currentFragmentIndex.setValue(1)
        val txt = resources.getString(R.string.account)
        val txt1 = " (2/5 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as CIFRootActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            header()
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            init()
            setLengthAndTpe()
            load()
            /*  if (GlobalClass.isDummyDataTrue) {
                  loadDummyData()
              }*/
            setConditions()
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setConditions() {
        fivPhoneBankSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                try {
                    if (fivPhoneBankSP?.selectedItem.toString().equals("No", true)) {
                        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "1") {
                            ToastUtils.normalShowToast(context,
                                "Phone banking should be YES as per CIF details",
                                1)
                            fivPhoneBankSP?.setSelection(1)
                        }
                    } else if (fivPhoneBankSP?.selectedItem.toString().equals("Yes", true)) {
                        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "0") {
                            ToastUtils.normalShowToast(context,
                                "Phone banking should be NO as per CIF details",
                                1)
                            fivPhoneBankSP?.setSelection(0)
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
        })
        fivPurposeOfAccountSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (fivPurposeOfAccountSP?.selectedItem.toString() != "Other") {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(
                            fivOtherPurposeOfAccountET!!
                        )
                        fivOtherPurposeOfAccountET?.setText("")
                        fivOtherPurposeOfAccount.mandateInput.text = ""
                    } else if (fivPurposeOfAccountSP?.selectedItem.toString() == "Other") {
                        fivOtherPurposeOfAccount.mandateInput.text = "*"
                        (activity as CIFRootActivity).globalClass?.setEnabled(
                            fivOtherPurposeOfAccountET!!,
                            true
                        )

                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        })
    }

    private fun setLengthAndTpe() {
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivOtherPurposeOfAccountET!!,
            25,
            Constants.INPUT_TYPE_ALPHA
        )
    }

    fun init() {
        fivRMCodeSP = fivRMCode.getSpinner(R.id.rmCodeID)
        sundrySP = fivSundryAnalysis.getSpinner(R.id.sundryID)
        fivNoOfChequeSP = fivNoOfCheque.getSpinner(R.id.fivNoOfCheque)
        fivModeOfDeliverySP = fivModeOfDelivery.getSpinner(R.id.fivModeOfDelivery)
        fivPurposeOfAccountSP = fivPurposeOfAccount.getSpinner(R.id.purposeOfAccountID)
        fivOtherPurposeOfAccountET =
            fivOtherPurposeOfAccount.getTextFromEditText(R.id.fivOtherPurposeOfAccount)
        fivSMSSP = fivSMS.getSpinner(R.id.SMSID)
        fivPhoneBankSP = fivPhoneBank.getSpinner(R.id.phoneBankID)
    }

    fun loadDummyData() {


//        fivOtherPurposeOfAccountET?.setText("For just saving ")
    }

    fun load() {
        (activity as CIFRootActivity).customerAccounts.OTHER_PURPOSE_OF_ACCT_DESC.let { t ->
            fivOtherPurposeOfAccountET?.setText(
                t
            )
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.lovRMCodes.isNullOrEmpty()) {
            callRM()

        } else {
            setRM((activity as CIFRootActivity).sharedPreferenceManager.lovRMCodes)
        }

//        (activity as CIFRootActivity).rmCodesList.let { fivRMCode.setItemForRMCodes(it) };
//        (activity as CIFRootActivity).boolList.let {
//            fivSMS.setItemForfivSMS(it)
//            fivSMSSP?.setSelection(0)
//        }
//        (activity as CIFRootActivity).boolList.let {
//            fivPhoneBank.setItemForfivPhoneBank(it)
//
//        }
    }

    private fun setRM(it: ArrayList<RMCodes>) {
        try {
            (activity as CIFRootActivity).rmCodesList = it
            fivRMCode.setItemForRMCodes(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovRMCodes = it
            (activity as CIFRootActivity).customerInfo.RM_DESC.let { rm ->
                (activity as CIFRootActivity).globalClass?.findSpinnerIndexFromRMCode(
                    it,
                    rm
                ).let {
                    fivRMCodeSP?.setSelection(it!!)
                }
            }

            if ((activity as CIFRootActivity).sharedPreferenceManager.lovSundry.isNullOrEmpty()) {
                callSundary()
            } else {
                setSundryCode((activity as CIFRootActivity).sharedPreferenceManager.lovSundry)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun callSundary() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.SUNDRY_IDENTIFIER
        HBLHRStore.instance?.getSundry(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : SundryCallBack {
                override fun SundrySuccess(response: SundryResponse) {
                    response.data?.let {
                        setSundryCode(it)
                    };
                }

                override fun SundryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setSundryCode(it: ArrayList<Sundry>) {
        try {
            (activity as CIFRootActivity).sundryList = it
            fivSundryAnalysis.setItemForSundry(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovSundry = it
            (activity as CIFRootActivity).customerAccounts.SUNDRY_DESC.let { item ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromSundryIndex(
                    it,
                    item
                ).let {
                    sundrySP?.setSelection(it!!)
                }
            }
            fivSundryAnalysis.remainSelection(it.size)
            if ((activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfAcct.isNullOrEmpty()) {
                callPurposeOfAccount()

            } else {
                setPurposeOfAccount((activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfAcct)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {

        }
    }

    private fun setPurposeOfAccount(it: ArrayList<PurposeOfAccount>) {

        try {
            (activity as CIFRootActivity).puposeOfAccountList = it
            fivPurposeOfAccount.setItemForPurposeOfAccount(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfAcct = it



            (activity as CIFRootActivity).customerAccounts.PURPOSEOFACCTDESC.let { POAcct ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPuposeOfAccountIndex(
                    it,
                    POAcct
                ).let {
                    fivPurposeOfAccountSP?.setSelection(it!!)
                }

            }


            if ((activity as CIFRootActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
                callBool()

            } else {
                setBool((activity as CIFRootActivity).sharedPreferenceManager.lovBool)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }

    }

    private fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let {
                        setBool(it)
                    };


                }

                override fun BoolFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setBool(it: ArrayList<Bool>) {
        try {
            (activity as CIFRootActivity).boolList = it
            fivSMS.setItemForfivSMS(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovBool = it
            fivPhoneBank.setItemForfivPhoneBank(it)

            if ((activity as CIFRootActivity).customerAccounts.SMS_ALERT.isNullOrEmpty()) {
                fivSMSSP?.setSelection(0)
            } else {
                (activity as CIFRootActivity).customerAccounts.SMS_ALERT.let { sms ->
                    (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromSMSIndex(
                        it,
                        sms
                    ).let {

                        fivSMSSP?.setSelection(it!!)

                    }

                }
            }


            if ((activity as CIFRootActivity).customerAccounts.PHONE_BANK.isNullOrEmpty()) {
                fivPhoneBankSP?.setSelection(1)
            } else {
                (activity as CIFRootActivity).customerAccounts.PHONE_BANK.let { phoneBank ->
                    (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPBIndex(
                        it,
                        phoneBank
                    ).let {
                        fivPhoneBankSP?.setSelection(it!!)
                    }

                }
            }
            if ((activity as CIFRootActivity).sharedPreferenceManager.lovChequeBookLeaves.isNullOrEmpty()) {
                callChequeBookLeave()
            } else {
                setChequeBookLeaves((activity as CIFRootActivity).sharedPreferenceManager.lovChequeBookLeaves)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun callChequeBookLeave() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.CHEQUE_BOOK_LEAVES_IDENTIFIER
        HBLHRStore.instance?.getChequeBookLeaves(
            RetrofitEnums.URL_HBL,
            lovRequest, object : ChequeBookLeavesCallBack {
                override fun ChequeBookLeavesSuccess(response: ChequeBookLeavesResponse) {
                    response.data?.let {
                        setChequeBookLeaves(it)
                    };
                }

                override fun ChequeBookLeavesFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setChequeBookLeaves(it: ArrayList<ChequeBookLeaves>) {
        try {
            fivNoOfCheque.setItemForChequeBookLeaves(it)
            (activity as CIFRootActivity).chequeBookLeavesList = it
            (activity as CIFRootActivity).sharedPreferenceManager.lovChequeBookLeaves = it
            (activity as CIFRootActivity).customerAccounts.CHEQUEBOOK_LEAVES_DESC.let { CBL ->
                (activity as CIFRootActivity).globalClass?.findSpinnerIndexFromChequeBookLeaves(
                    it,
                    CBL
                ).let {
                    fivNoOfChequeSP?.setSelection(it!!)
                }
            }
            if ((activity as CIFRootActivity).sharedPreferenceManager.lovModeOfDelivery.isNullOrEmpty()) {
                callModeOfDelivery()
            } else {
                setModeOfDelivery((activity as CIFRootActivity).sharedPreferenceManager.lovModeOfDelivery)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun callModeOfDelivery() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.MODE_OF_DELIVERY_IDENTIFIER
        HBLHRStore.instance?.getMOD(
            RetrofitEnums.URL_HBL,
            lovRequest, object : ModeOfDeliveryCallBack {
                override fun ModeOfDeliverySuccess(response: MODResponse) {
                    response.data?.let {
                        setModeOfDelivery(it)
                    };
                }

                override fun ModeOfDeliveryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setModeOfDelivery(it: ArrayList<MODelivery>) {
        try {
            fivModeOfDelivery.setItemForModeOfDelivery(it)
            (activity as CIFRootActivity).modeOfDeliveryList = it
            (activity as CIFRootActivity).sharedPreferenceManager.lovModeOfDelivery = it
            (activity as CIFRootActivity).customerAccounts.MODE_DESC.let { mod ->
                (activity as CIFRootActivity).globalClass?.findSpinnerIndexFromModeOfDelivery(
                    it,
                    mod
                ).let {
                    fivModeOfDeliverySP?.setSelection(it!!)
                }

            }
            (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }


    fun callRM() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.RM_CODES_IDENTIFIER
        HBLHRStore.instance?.getRMCodes(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : RMCodesCallBack {
                override fun RMCodesSuccess(response: RMCodesResponse) {
                    response.data?.let {
                        setRM(it)
                    };
//                    (activity as CIFRootActivity).globalClass?.hideLoader()


                }

                override fun RMCodesFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    fun callPurposeOfAccount() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.PURPOSE_OF_ACCOUNT_IDENTIFIER
        HBLHRStore.instance?.getPurposeOfAccount(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : PurposeOfAccountCallBack {
                override fun PurposeOfAccountSuccess(response: PurposeOfAccountResponse) {
                    response.data?.let {
                        setPurposeOfAccount(it)
//                        fivPurposeOfAccountSP?.setSelection(1)
                    };
//                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

                override fun PurposeOfAccountFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                        Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            (activity as CIFRootActivity).runOnUiThread {
                                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }

            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep7_2)
                    findNavController().navigate(R.id.action_CIFStep7_2_to_CIFStep7)
        }

    }

    private fun validation(): Boolean {
        if (fivNoOfChequeSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select no of cheque book leaves",
                1
            )
            return false
        } else if (fivModeOfDeliverySP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please Select mode of delivery",
                1
            )
            return false
        } else if (fivRMCodeSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select RM Code",
                1
            )
            return false
        } else if (sundrySP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select Sundry Analysis",
                1
            )
            return false
        } else if (fivPurposeOfAccountSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select purpose of account",
                1
            )
            return false
        } else if (fivPurposeOfAccountSP?.selectedItem.toString() == "Other") {
            if (TextUtils.isEmpty(fivOtherPurposeOfAccountET?.text.toString())) {
                ToastUtils.normalShowToast(
                    activity,
                    "Please other purpose of account",
                    1
                )
                return false
            } else {
                return true
            }
        } else if (fivPhoneBankSP?.selectedItem.toString()
                .equals("No", true)
        ) {//Internet should be YES as per CIF detailsFrag
            if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "1") {
                ToastUtils.normalShowToast(context,
                    "Phone banking should be YES as per CIF details",
                    1)
                return false
            } else {
                return true
            }
        } else if (fivPhoneBankSP?.selectedItem.toString().equals("Yes", true)) {
            if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "0") {
                ToastUtils.normalShowToast(context,
                    "Phone banking should be NO as per CIF details",
                    1)
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep7_2)
                    findNavController().navigate(R.id.action_CIFStep7_2_to_CIFStep7)
            }
            true
        }
        false
    }

    fun saveAndNext() {
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromModeOfDelivery(
            (activity as CIFRootActivity).modeOfDeliveryList,
            fivModeOfDeliverySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.MODE = it.toString()
                (activity as CIFRootActivity).customerAccounts.MODE_DESC =
                    fivModeOfDeliverySP?.selectedItem.toString()
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromChequeBookLeaves(
            (activity as CIFRootActivity).chequeBookLeavesList,
            fivNoOfChequeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.CHEQUEBOOK_LEAVES = it.toString()
                (activity as CIFRootActivity).customerAccounts.CHEQUEBOOK_LEAVES_DESC =
                    fivNoOfChequeSP?.selectedItem.toString()
            }
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromRMCode(
            (activity as CIFRootActivity).rmCodesList,
            fivRMCodeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.RM_CODE =
                    fivRMCodeSP?.selectedItem.toString() // code
                (activity as CIFRootActivity).customerAccounts.RM_DESC = it.toString() //Desc
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromSundryCode(
            (activity as CIFRootActivity).sundryList,
            sundrySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                var splitArray= sundrySP?.selectedItem.toString().split(",")
                (activity as CIFRootActivity).customerAccounts.SUNDRY_ANALYSIS =it
                (activity as CIFRootActivity).customerAccounts.SUNDRY_DESC  = splitArray[0]
            }
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfAccountCode(
            (activity as CIFRootActivity).puposeOfAccountList,
            fivPurposeOfAccountSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.PURPOSE_OF_ACCT = it.toString()
                (activity as CIFRootActivity).customerAccounts.PURPOSEOFACCTDESC =
                    fivPurposeOfAccountSP?.selectedItem.toString()
            }
        }
        fivOtherPurposeOfAccountET?.text.toString()
            .let { (activity as CIFRootActivity).customerAccounts.OTHER_PURPOSE_OF_ACCT_DESC = it }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivSMSSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerAccounts.SMS_ALERT = it!!
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivPhoneBankSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerAccounts.PHONE_BANK = it!!
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount =
            (activity as CIFRootActivity).customerAccounts
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as CIFRootActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as CIFRootActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as CIFRootActivity).aofAccountInfoRequest = it
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                        if (findNavController().currentDestination?.id == R.id.CIFStep7_2)
                            findNavController().navigate(R.id.action_CIFStep7_2_to_CIFStep7_3)
                        (activity as CIFRootActivity).recyclerViewSetup()
                        (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
