package com.hbl.bot.ui.fragments.spark.cif


import com.hbl.bot.ui.activities.spark.CIFRootActivity
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import android.widget.AdapterView.OnItemSelectedListener
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.BoolCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.CountryCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.GenerateCIFCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.BoolResponse
import com.hbl.bot.network.models.response.base.CountryResponse
import com.hbl.bot.network.models.response.base.GenerateCIFResponse
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.Country
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep5_3.*
import kotlinx.android.synthetic.main.fragment_cifstep5_3.btBack
import kotlinx.android.synthetic.main.fragment_cifstep5_3.btNext
import kotlinx.android.synthetic.main.fragment_cifstep5_3.formSectionHeader
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.lang.Exception
import android.content.ActivityNotFoundException
import com.hbl.bot.utils.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class CIFStep5_3 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null

    /*inward*/
    var inwardFirstMonthlyAmountET1: EditText? = null
    var inwardMailInstructionSP1: Spinner? = null
    var inwardPurposeDetailET1: EditText? = null

    var inwardFirstMonthlyAmountET2: EditText? = null
    var inwardMailInstructionSP2: Spinner? = null
    var inwardPurposeDetailET2: EditText? = null

    var inwardFirstMonthlyAmountET3: EditText? = null
    var inwardMailInstructionSP3: Spinner? = null
    var inwardPurposeDetailET3: EditText? = null

    /*outward*/
    var outwardFirstMonthlyAmountET1: EditText? = null
    var outwardMailInstructionSP1: Spinner? = null
    var outwardPurposeDetailET1: EditText? = null

    var outwardFirstMonthlyAmountET2: EditText? = null
    var outwardMailInstructionSP2: Spinner? = null
    var outwardPurposeDetailET2: EditText? = null

    var outwardFirstMonthlyAmountET3: EditText? = null
    var outwardMailInstructionSP3: Spinner? = null
    var outwardPurposeDetailET3: EditText? = null

    var inwardYesNoSelectedSPIDSP: Spinner? = null
    var outwardYesNoSelectedSPIDSP: Spinner? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep5_3, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }finally {
            return myView
        }

    }

    fun header() {
        viewModel.totalSteps.setValue(4)
        viewModel.currentFragmentIndex.setValue(1)
        val txt = resources.getString(R.string.foreign_inward_outward_remittance_details)
        val txt1 = " (2/4 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            init()
            load()
            setLengthAndType()
           /* if (GlobalClass.isDummyDataTrue) {
                loadDummyData()
            }*/
            inwardConditions()
            outwardConditions()
            inwardOutwardPurposeOfCIF()
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun inwardOutwardPurposeOfCIF() {
        try {
            var purposeOfCIF =
                (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_CODE
            /*Foreign Inward/Outward remittance details will be freeze. And first expected monthly amount will be 9999,
            country of inward and outward will be “Pakistan” and purpose will be “NA”  */
            if (purposeOfCIF == "03" /*Director*/ || purposeOfCIF == "06" /*Beneficial Owner*/ || purposeOfCIF == "08" /*Authorized Signatory*/) {
                (activity as CIFRootActivity).globalClass?.setDisbaled(inwardYesNoSelectedSPIDSP!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(outwardYesNoSelectedSPIDSP!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(inwardFirstMonthlyAmountET1!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(outwardFirstMonthlyAmountET1!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(inwardPurposeDetailET1!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(outwardPurposeDetailET1!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(inwardMailInstructionSP1!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(outwardMailInstructionSP1!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(inwardCheckbox2!!,false)
                (activity as CIFRootActivity).globalClass?.setDisbaled(outwardCheckbox1!!,false)
                inwardYesNoSelectedSPIDSP?.setSelection(1)
                outwardYesNoSelectedSPIDSP?.setSelection(1)
                /*set Defualt Value*/
                inwardFirstMonthlyAmountET1?.setText("9999")
                inwardPurposeDetailET1?.setText("NA")
                outwardFirstMonthlyAmountET1?.setText("9999")
                outwardPurposeDetailET1?.setText("NA")

                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryNameSetCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                    "PK"
                ).let {
                    if (it != 0) {// when select there is no item selected "Choose an item"  <--- hint
                        inwardMailInstructionSP1?.setSelection(it!!)
                    }
                }
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryNameSetCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                    "PK"
                ).let {
                    if (it != 0) {// when select there is no item selected "Choose an item"  <--- hint
                        outwardMailInstructionSP1?.setSelection(it!!)
                    }
                }
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setLengthAndType() {
        /*inward 1*/
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            inwardFirstMonthlyAmountET1!!,
            15,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            inwardPurposeDetailET1!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        /*inward 2*/
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            inwardFirstMonthlyAmountET2!!,
            15,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            inwardPurposeDetailET2!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        /*inward 3*/
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            inwardFirstMonthlyAmountET3!!,
            15,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            inwardPurposeDetailET3!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        /*outward1*/
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            outwardFirstMonthlyAmountET1!!,
            15,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            outwardPurposeDetailET1!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )

        /*outward2*/
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            outwardFirstMonthlyAmountET2!!,
            15,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            outwardPurposeDetailET2!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        /*outward3*/
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            outwardFirstMonthlyAmountET3!!,
            15,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            outwardPurposeDetailET3!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
    }

    private fun inwardConditions() {
        inwardYesNoSelectedSPIDSP!!.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                i: Int,
                l: Long,
            ) {
                try {
                    if (i == 0) {
                        llIInwardLayoutnfo.visibility = View.GONE
                    } else {
                        llIInwardLayoutnfo.visibility = View.VISIBLE
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
//                llIInwardLayoutnfo.visibility = View.GONE
            }
        })
        inwardCheckbox2.setOnCheckedChangeListener { compoundButton, check ->
            try {
                if (check) {
                    inward2Layout.visibility = View.VISIBLE
                } else {
                    inward2Layout.visibility = View.GONE
                    inward3Layout.visibility = View.GONE
                    inwardCheckbox3.isChecked = false
                }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
            }
        }
        inwardCheckbox3.setOnCheckedChangeListener { compoundButton, check ->
            try {
                if (check) {
                    inward3Layout.visibility = View.VISIBLE
                } else {
                    inward3Layout.visibility = View.GONE
                }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
            }
        }
    }

    fun outwardConditions() {
        outwardYesNoSelectedSPIDSP!!.setOnItemSelectedListener(object : OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                i: Int,
                l: Long,
            ) {
                try{
                if (i == 0) {
                    llIOutwardLayoutnfo.visibility = View.GONE
                } else {
                    llIOutwardLayoutnfo.visibility = View.VISIBLE
                }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
        }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
//                llIOutwardLayoutnfo.visibility = View.GONE
            }
        })
        outwardCheckbox1.setOnCheckedChangeListener { compoundButton, check ->
            try{
            if (check) {
                outwardlayout2.visibility = View.VISIBLE
            } else {
                outwardlayout2.visibility = View.GONE
                outwardlayout3.visibility = View.GONE
                outwardCheckbox2.isChecked = false
            }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
            }
        }
        outwardCheckbox2.setOnCheckedChangeListener { compoundButton, check ->
            try{
            if (check) {
                outwardlayout3.visibility = View.VISIBLE
            } else {
                outwardlayout3.visibility = View.GONE
            }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
            }
    }
    }

    fun init() {
        inwardFirstMonthlyAmountET1 =
            fivinwardFirstMonthlyAmount1.getTextFromEditText(R.id.fivFirstMonthlyAmount)
        inwardMailInstructionSP1 = fivinwardMailInstruction1.getSpinner(R.id.fivMailInstructionID)
        inwardPurposeDetailET1 =
            fivinwardPurposeDetail1.getTextFromEditText(R.id.purposeOfAccountID)
        inwardFirstMonthlyAmountET2 =
            fivinwardFirstMonthlyAmount2.getTextFromEditText(R.id.fivFirstMonthlyAmount22)
        inwardMailInstructionSP2 = fivinwardMailInstruction2.getSpinner(R.id.fivMailInstructionID22)
        inwardPurposeDetailET2 =
            fivinwardPurposeDetail2.getTextFromEditText(R.id.purposeOfAccountID22)
        inwardFirstMonthlyAmountET3 =
            fivinwardFirstMonthlyAmount3.getTextFromEditText(R.id.fivFirstMonthlyAmount33)
        inwardMailInstructionSP3 = fivinwardMailInstruction3.getSpinner(R.id.fivMailInstructionID33)
        inwardPurposeDetailET3 =
            fivinwardPurposeDetail3.getTextFromEditText(R.id.purposeOfAccountID33)
        /*outward*/
        outwardFirstMonthlyAmountET1 =
            fivoutwardFirstMonthlyAmount1.getTextFromEditText(R.id.fivoutwardFirstMonthlyAmount11)
        outwardMailInstructionSP1 =
            fivoutwardMailInstruction1.getSpinner(R.id.fivoutwardMailInstruction1ID)
        outwardPurposeDetailET1 =
            fivoutwardPurposeDetail1.getTextFromEditText(R.id.outwardpurposeOfAccountID1)

        outwardFirstMonthlyAmountET2 =
            fivoutwardFirstMonthlyAmount2.getTextFromEditText(R.id.fivoutwardFirstMonthlyAmount22)
        outwardMailInstructionSP2 =
            fivoutwardMailInstruction2.getSpinner(R.id.fivoutwardMailInstruction2ID)
        outwardPurposeDetailET2 =
            fivoutwardPurposeDetail2.getTextFromEditText(R.id.outwardpurposeOfAccountID22)

        outwardFirstMonthlyAmountET3 =
            fivoutwardFirstMonthlyAmount3.getTextFromEditText(R.id.fivoutwardFirstMonthlyAmount33)
        outwardMailInstructionSP3 =
            fivoutwardMailInstruction3.getSpinner(R.id.fivoutwardMailInstruction3ID)
        outwardPurposeDetailET3 =
            fivoutwardPurposeDetail3.getTextFromEditText(R.id.outwardpurposeOfAccountID3)

        inwardYesNoSelectedSPIDSP =
            inwardYesNoSelectedSPID.getSpinner(R.id.inwardYesNoSelectedSPID1)
        outwardYesNoSelectedSPIDSP =
            outwardYesNoSelectedSPID.getSpinner(R.id.outwardYesNoSelectedSPID2)
    }

    fun loadDummyData() {
        inwardFirstMonthlyAmountET1?.setText("40000")
        inwardPurposeDetailET1?.setText("There is a pupose of this details, i have to work as a financial supporter and  et cetera.")
        outwardFirstMonthlyAmountET1?.setText("50000")
        outwardPurposeDetailET1?.setText("i have to work as a financial supporter and  et cetera.")
    }

    fun load() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()
        } else {
            setBool((activity as CIFRootActivity).sharedPreferenceManager.lovBool)
        }
    }

    fun callCountries() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    response.data?.let { setCountries(it) };
                }

                override fun CountryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setCountries(it: java.util.ArrayList<Country>) {
        try{
        setTextLoad()
        fivinwardMailInstruction1.setItemForCountries(it)
        fivinwardMailInstruction2.setItemForCountries(it)
        fivinwardMailInstruction3.setItemForCountries(it)
        fivoutwardMailInstruction1.setItemForfivCountry(it)
        fivoutwardMailInstruction2.setItemForfivCountry(it)
        fivoutwardMailInstruction3.setItemForfivCountry(it)
        (activity as CIFRootActivity).sharedPreferenceManager.lovCountries = it
        /*Inward COUNTRIES*/
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_1
        ).let {
            if (it != 0) {
                inwardMailInstructionSP1?.setSelection(it!!)
                inwardYesNoSelectedSPIDSP?.setSelection(1)
                llIInwardLayoutnfo.visibility = View.VISIBLE
                /*For Drawer update*/
                (activity as CIFRootActivity).aofbModels.inward = 1
                (activity as CIFRootActivity).sharedPreferenceManager.aofbModels =
                    (activity as CIFRootActivity).aofbModels
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_2
        ).let {
            if (it != 0) {
                inwardMailInstructionSP2?.setSelection(it!!)
                inwardYesNoSelectedSPIDSP?.setSelection(1)
                inwardCheckbox2.isChecked = true
                inward2Layout.isVisible = true
                llIInwardLayoutnfo.visibility = View.VISIBLE
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_3
        ).let {
            if (it != 0) {
                inwardMailInstructionSP3?.setSelection(it!!)
                inwardYesNoSelectedSPIDSP?.setSelection(1)
                inwardCheckbox3.isChecked = true
                inward3Layout.isVisible = true
                llIInwardLayoutnfo.visibility = View.VISIBLE
            }
        }
        fivinwardMailInstruction1.remainSelection(it.size)
        fivinwardMailInstruction2.remainSelection(it.size)
        fivinwardMailInstruction3.remainSelection(it.size)
        fivoutwardMailInstruction1.remainSelection(it.size)
        fivoutwardMailInstruction2.remainSelection(it.size)
        fivoutwardMailInstruction3.remainSelection(it.size)
        /*Outward Countries*/
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_1
        ).let {
            if (it != 0) {
                outwardMailInstructionSP1?.setSelection(it!!)
                outwardYesNoSelectedSPIDSP?.setSelection(1)
                llIOutwardLayoutnfo.visibility = View.VISIBLE
                /*For Drawer update*/
                (activity as CIFRootActivity).aofbModels.outward = 1
                (activity as CIFRootActivity).sharedPreferenceManager.aofbModels =
                    (activity as CIFRootActivity).aofbModels
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_2
        ).let {
            if (it != 0) {
                outwardMailInstructionSP2?.setSelection(it!!)
                outwardYesNoSelectedSPIDSP?.setSelection(1)
                outwardCheckbox1.isChecked = true
                llIOutwardLayoutnfo.visibility = View.VISIBLE
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_3
        ).let {
            if (it != 0) {
                outwardMailInstructionSP3?.setSelection(it!!)
                outwardYesNoSelectedSPIDSP?.setSelection(1)
                outwardCheckbox2.isChecked = true
                llIOutwardLayoutnfo.visibility = View.VISIBLE
            }
        }
        (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setTextLoad() {
        /*-------inward values loaded*/
        /*inward 1*/
        (activity as CIFRootActivity).customerCdd.iS_FOREGIN_FUND_OW.let {
            if (it == "Y") {
                outwardYesNoSelectedSPIDSP?.setSelection(1)
            } else {
                outwardYesNoSelectedSPIDSP?.setSelection(0)
            }
        }
        (activity as CIFRootActivity).customerCdd.iS_FOREIGN_FUND_IW.let {
            if (it == "Y") {
                inwardYesNoSelectedSPIDSP?.setSelection(1)
            } else {
                inwardYesNoSelectedSPIDSP?.setSelection(0)
            }
        }

        (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_1.let {
            if (!it.toString().isNullOrEmpty()) {
                inwardFirstMonthlyAmountET1?.setText(it.toString())
                inwardYesNoSelectedSPIDSP?.setSelection(1)
                llIInwardLayoutnfo.visibility = View.VISIBLE
                /*For Drawer update*/
                (activity as CIFRootActivity).aofbModels.inward = 1
                (activity as CIFRootActivity).sharedPreferenceManager.aofbModels =
                    (activity as CIFRootActivity).aofbModels
            }
        }
        (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_1
            .let {
                if (!it.isNullOrEmpty()) {
                    inwardPurposeDetailET1?.setText(it)
                    inwardYesNoSelectedSPIDSP?.setSelection(1)
                    llIInwardLayoutnfo.visibility = View.VISIBLE
                }
            }
        /*inward 2*/
        (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_2.let {
            if (!it.toString().isNullOrEmpty()) {
                inwardFirstMonthlyAmountET2?.setText(it.toString())
                inwardYesNoSelectedSPIDSP?.setSelection(1)
                llIInwardLayoutnfo.visibility = View.VISIBLE
                inwardCheckbox2.isChecked = true
                inward2Layout.isVisible = true
            }
        }
        (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_2
            .let {
                if (!it.isNullOrEmpty()) {
                    inwardPurposeDetailET2?.setText(it)
                    inwardYesNoSelectedSPIDSP?.setSelection(1)
                    llIInwardLayoutnfo.visibility = View.VISIBLE
                    inwardCheckbox2.isChecked = true
                    inward2Layout.isVisible = true
                }
            }
        /*inward 3*/
        (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_3.let {
            if (!it.toString().isNullOrEmpty()) {
                inwardFirstMonthlyAmountET3?.setText(it.toString())
                inwardYesNoSelectedSPIDSP?.setSelection(1)
                llIInwardLayoutnfo.visibility = View.VISIBLE
                inwardCheckbox3.isChecked = true
                inward3Layout.isVisible = true
            }
        }
        (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_3
            .let {
                if (!it.isNullOrEmpty()) {
                    inwardPurposeDetailET3?.setText(it)
                    inwardYesNoSelectedSPIDSP?.setSelection(1)
                    inwardCheckbox3.isChecked = true
                    inward3Layout.isVisible = true
                }
            }

        /*-------outward values loaded*/
        /*outward 1*/
        (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_1.let {
            if (!it.toString().toString().isNullOrEmpty()) {
                outwardFirstMonthlyAmountET1?.setText(it.toString())
                outwardYesNoSelectedSPIDSP?.setSelection(1)
                llIOutwardLayoutnfo.visibility = View.VISIBLE
                /*For Drawer update*/
                (activity as CIFRootActivity).aofbModels.outward = 1
                (activity as CIFRootActivity).sharedPreferenceManager.aofbModels =
                    (activity as CIFRootActivity).aofbModels
            }
        }
        (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_1
            .let {
                if (!it.isNullOrEmpty()) {
                    outwardPurposeDetailET1?.setText(it)
                    outwardYesNoSelectedSPIDSP?.setSelection(1)
                    llIOutwardLayoutnfo.visibility = View.VISIBLE
                }
            }
        /*outward 2*/
        (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_2.let {
            if (!it.toString().isNullOrEmpty()) {
                outwardFirstMonthlyAmountET2?.setText(it.toString())
                outwardYesNoSelectedSPIDSP?.setSelection(1)
                llIOutwardLayoutnfo.visibility = View.VISIBLE
                outwardCheckbox1.isChecked = true
                outwardlayout2.isVisible = true
            }
        }
        (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_2
            .let {
                if (!it.isNullOrEmpty()) {
                    outwardPurposeDetailET2?.setText(it)
                    outwardYesNoSelectedSPIDSP?.setSelection(1)
                    llIOutwardLayoutnfo.visibility = View.VISIBLE
                    outwardCheckbox1.isChecked = true
                    outwardlayout2.isVisible = true
                }
            }

        /*outward 3*/
        (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_3.let {
            if (!it.toString().isNullOrEmpty()) {
                outwardFirstMonthlyAmountET3?.setText(it.toString())
                outwardYesNoSelectedSPIDSP?.setSelection(1)
                llIOutwardLayoutnfo.visibility = View.VISIBLE
                outwardCheckbox2.isChecked = true
                outwardlayout3.isVisible = true

            }
        }
        (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_3
            .let {
                if (!it.isNullOrEmpty()) {
                    outwardPurposeDetailET3?.setText(it)
                    outwardYesNoSelectedSPIDSP?.setSelection(1)
                    llIOutwardLayoutnfo.visibility = View.VISIBLE
                    outwardCheckbox2.isChecked = true
                    outwardlayout3.isVisible = true
                }
            }
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore?.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let { setBool(it) };
                }

                override fun BoolFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setBool(it: ArrayList<Bool>) {
        try{
        setItemForBools(it)
        (activity as CIFRootActivity).sharedPreferenceManager.lovBool = it
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovCountries.isNullOrEmpty()) {
            callCountries()
        } else {
            setCountries((activity as CIFRootActivity).sharedPreferenceManager.lovCountries)
        }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
}

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try{
                    if (validation()) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(btNext,false)
                        ( this.context as CIFRootActivity).globalClass?.showDialog( this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            activity?.runOnUiThread {
                                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.btBack -> if (findNavController().currentDestination?.id == R.id.CIFStep5_3)
                findNavController().navigate(R.id.action_CIFStep5_3_to_CIFStep5)
        }

    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep5_3)
                    findNavController().navigate(R.id.action_CIFStep5_3_to_CIFStep5)
                true
            }
            false
        }
    }

    fun validation(): Boolean {
        /*inward first without checked*/
        if (llIInwardLayoutnfo.isVisible && inwardFirstMonthlyAmountET1?.text?.toString()!!
                .isEmpty()
        ) {
            inwardFirstMonthlyAmountET1?.error =
                resources!!.getString(R.string.pleaseSelectInwardExpectedMonthlyAmount1)
            inwardFirstMonthlyAmountET1?.requestFocus()
            return false
        } else if (llIInwardLayoutnfo.isVisible && inwardMailInstructionSP1?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.pleaseSelectCountryOfInwardRemittance1),
                1
            )
            return false
        } else if (llIInwardLayoutnfo.isVisible && inwardPurposeDetailET1?.text?.toString()!!
                .isEmpty()
        ) {
            /*outward first without checked*/
            inwardPurposeDetailET1?.error =
                resources!!.getString(R.string.pleaseEnterInwardPurposeOfDetails1)
            inwardPurposeDetailET1?.requestFocus()
            return false
        } else if (llIOutwardLayoutnfo.isVisible && outwardFirstMonthlyAmountET1?.text?.toString()!!
                .isEmpty()
        ) {
            outwardFirstMonthlyAmountET1?.setError(resources!!.getString(R.string.pleaseSelectOutwardExpectedMonthlyAmount1))
            outwardFirstMonthlyAmountET1?.requestFocus()
            return false
        } else if (llIOutwardLayoutnfo.isVisible && outwardMailInstructionSP1?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.pleaseSelectCountryOfOutwardRemittance1),
                1
            )
            return false
        } else if (llIOutwardLayoutnfo.isVisible && outwardPurposeDetailET1?.text?.toString()!!
                .isEmpty()
        ) {
            outwardPurposeDetailET1?.error =
                resources!!.getString(R.string.pleaseEnterOutwardPurposeOfDetails1)
            outwardPurposeDetailET1?.requestFocus()
            return false
        } else /*inward 2nd*/ if (llIInwardLayoutnfo.isVisible && inwardCheckbox2.isChecked && inwardFirstMonthlyAmountET2?.text?.toString()!!
                .isEmpty()
        ) {
            inwardFirstMonthlyAmountET2?.error =
                resources!!.getString(R.string.pleaseSelectInwardExpectedMonthlyAmount2)
            inwardFirstMonthlyAmountET2?.requestFocus()
            return false
        } else if (llIInwardLayoutnfo.isVisible && inwardCheckbox2.isChecked && inwardMailInstructionSP2?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.pleaseSelectCountryOfInwardRemittance2),
                1
            )
            return false
        } else if (llIInwardLayoutnfo.isVisible && inwardCheckbox2.isChecked && inwardPurposeDetailET2?.text?.toString()!!
                .isEmpty()
        ) {
            inwardPurposeDetailET2?.error =
                resources!!.getString(R.string.pleaseEnterInwardPurposeOfDetails2)
            inwardPurposeDetailET2?.requestFocus()
            return false
        } else /*inward 3rd*/ if (llIInwardLayoutnfo.isVisible && inwardCheckbox3.isChecked && inwardFirstMonthlyAmountET3?.text?.toString()!!
                .isEmpty()
        ) {
            inwardFirstMonthlyAmountET3?.error =
                resources!!.getString(R.string.pleaseSelectInwardExpectedMonthlyAmount3)
            inwardFirstMonthlyAmountET3?.requestFocus()
            return false
        } else if (llIInwardLayoutnfo.isVisible && inwardCheckbox3.isChecked && inwardMailInstructionSP3?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.pleaseSelectCountryOfInwardRemittance3),
                1
            )
            return false
        } else if (llIInwardLayoutnfo.isVisible && inwardCheckbox3.isChecked && inwardPurposeDetailET3?.text?.toString()!!
                .isEmpty()
        ) {
            inwardPurposeDetailET3?.error =
                resources!!.getString(R.string.pleaseEnterInwardPurposeOfDetails3)
            inwardPurposeDetailET3?.requestFocus()
            return false
        } else  /*outward 2nd*/ if (llIOutwardLayoutnfo.isVisible && outwardCheckbox1.isChecked && outwardFirstMonthlyAmountET2?.text?.toString()!!
                .isEmpty()
        ) {
            outwardFirstMonthlyAmountET2?.error =
                resources!!.getString(R.string.pleaseSelectOutwardExpectedMonthlyAmount2)
            outwardFirstMonthlyAmountET2?.requestFocus()
            return false
        } else if (llIOutwardLayoutnfo.isVisible && outwardCheckbox1.isChecked && outwardMailInstructionSP2?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.pleaseSelectCountryOfOutwardRemittance2),
                1
            )
            return false
        } else if (llIOutwardLayoutnfo.isVisible && outwardCheckbox1.isChecked && outwardPurposeDetailET2?.text?.toString()!!
                .isEmpty()
        ) {
            outwardPurposeDetailET2?.error =
                resources!!.getString(R.string.pleaseEnterOutwardPurposeOfDetails2)
            outwardPurposeDetailET2?.requestFocus()
            return false
        } else /*outward 3rd*/ if (llIOutwardLayoutnfo.isVisible && outwardCheckbox2.isChecked && outwardFirstMonthlyAmountET3?.text?.toString()!!
                .isEmpty()
        ) {
            outwardFirstMonthlyAmountET3?.error =
                resources!!.getString(R.string.pleaseSelectOutwardExpectedMonthlyAmount3)
            outwardFirstMonthlyAmountET3?.requestFocus()
            return false
        } else if (llIOutwardLayoutnfo.isVisible && outwardCheckbox2.isChecked && outwardMailInstructionSP3?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.pleaseSelectCountryOfOutwardRemittance3),
                1
            )
            return false
        } else if (llIOutwardLayoutnfo.isVisible && outwardCheckbox2.isChecked && outwardPurposeDetailET3?.text?.toString()!!
                .isEmpty()
        ) {
            outwardPurposeDetailET3?.error =
                resources!!.getString(R.string.pleaseEnterOutwardPurposeOfDetails3)
            outwardPurposeDetailET3?.requestFocus()
            return false
        } else {
            return true
        }
    }

    fun setItemForBools(item: ArrayList<Bool>) {
        try{
        val adapter =
            ArrayAdapter(
                activity as CIFRootActivity,
                R.layout.view_spinner_item,
                R.id.text1,
                item
            )
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        inwardYesNoSelectedSPIDSP?.adapter = adapter
        outwardYesNoSelectedSPIDSP?.adapter = adapter
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
}

    fun saveAndNext() {
        /*inward*/
        if (inwardYesNoSelectedSPIDSP?.selectedItemPosition == 1) {
            (activity as CIFRootActivity).customerCdd.iS_FOREIGN_FUND_IW = "Y"
            inwardFirstMonthlyAmountET1?.text.toString().let {
                (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_1 =
                    it
            }
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                inwardMailInstructionSP1?.selectedItem.toString()
            ).let {
                if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as CIFRootActivity).customerCdd.cOUNTRY_INWARD_REMITT_1 =
                        it.toString()
                    (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_1 =
                        inwardMailInstructionSP1?.selectedItem.toString()
                }
            }
            inwardPurposeDetailET1?.text.toString()
                .let { (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_1 = it }
            /*inward2*/
            if (inwardCheckbox2.isChecked) {
                inwardFirstMonthlyAmountET2?.text.toString().let {
                    (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_2 =
                        it
                }
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                    inwardMailInstructionSP2?.selectedItem.toString()
                ).let {
                    if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                        (activity as CIFRootActivity).customerCdd.cOUNTRY_INWARD_REMITT_2 =
                            it.toString()
                        (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_2 =
                            inwardMailInstructionSP2?.selectedItem.toString()
                    }
                }
                inwardPurposeDetailET2?.text.toString()
                    .let { (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_2 = it }
            } else {
                (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_2 = ""
                (activity as CIFRootActivity).customerCdd.cOUNTRY_INWARD_REMITT_2 = ""
                (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_2 = ""
                (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_2 = ""
            }
            /*inward3*/
            if (inwardCheckbox3.isChecked) {
                inwardFirstMonthlyAmountET3?.text.toString().let {
                    (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_3 =
                        it
                }
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                    inwardMailInstructionSP3?.selectedItem.toString()
                ).let {
                    if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                        (activity as CIFRootActivity).customerCdd.cOUNTRY_INWARD_REMITT_3 =
                            it.toString()
                        (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_3 =
                            inwardMailInstructionSP3?.selectedItem.toString()
                    }
                }
                inwardPurposeDetailET3?.text.toString()
                    .let { (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_3 = it }
            } else {
                (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_3 = ""
                (activity as CIFRootActivity).customerCdd.cOUNTRY_INWARD_REMITT_3 = ""
                (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_3 = ""
                (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_3 = ""
            }
        } else {
            (activity as CIFRootActivity).customerCdd.iS_FOREIGN_FUND_IW = "N"
            (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_1 = ""
            (activity as CIFRootActivity).customerCdd.cOUNTRY_INWARD_REMITT_1 = ""
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_1 = ""
            (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_1 = ""
            (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_2 = ""
            (activity as CIFRootActivity).customerCdd.cOUNTRY_INWARD_REMITT_2 = ""
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_2 = ""
            (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_2 = ""
            (activity as CIFRootActivity).customerCdd.eXPECTED_INWARD_MTHLY_INCOME_PKR_3 = ""
            (activity as CIFRootActivity).customerCdd.cOUNTRY_INWARD_REMITT_3 = ""
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_INWARD_REMITT_3 = ""
            (activity as CIFRootActivity).customerCdd.pURPOSE_INWARD_3 = ""
        }
        /*outward*/
        if (outwardYesNoSelectedSPIDSP?.selectedItemPosition == 1) {
            (activity as CIFRootActivity).customerCdd.iS_FOREGIN_FUND_OW = "Y"

            outwardFirstMonthlyAmountET1?.text.toString().let {
                (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_1 =
                    it
            }
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                outwardMailInstructionSP1?.selectedItem.toString()
            ).let {
                if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as CIFRootActivity).customerCdd.cOUNTRY_OUTWARD_REMITT_1 =
                        it.toString()
                    (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_1 =
                        outwardMailInstructionSP1?.selectedItem.toString()
                }
            }
            outwardPurposeDetailET1?.text.toString()
                .let { (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_1 = it }
            /*outward2*/
            if (outwardCheckbox1.isChecked) {
                outwardFirstMonthlyAmountET2?.text.toString().let {
                    (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_2 =
                        it
                }
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                    outwardMailInstructionSP2?.selectedItem.toString()
                ).let {
                    if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                        (activity as CIFRootActivity).customerCdd.cOUNTRY_OUTWARD_REMITT_2 =
                            it.toString()
                        (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_2 =
                            outwardMailInstructionSP2?.selectedItem.toString()
                    }
                }
                outwardPurposeDetailET2?.text.toString()
                    .let { (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_2 = it }
            } else {
                (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_2 = ""
                (activity as CIFRootActivity).customerCdd.cOUNTRY_OUTWARD_REMITT_2 = ""
                (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_2 = ""
                (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_2 = ""
            }
            /*outward3*/
            if (outwardCheckbox2.isChecked) {
                outwardFirstMonthlyAmountET3?.text.toString().let {
                    (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_3 =
                        it
                }
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                    outwardMailInstructionSP3?.selectedItem.toString()
                ).let {
                    if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                        (activity as CIFRootActivity).customerCdd.cOUNTRY_OUTWARD_REMITT_3 =
                            it.toString()
                        (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_3 =
                            outwardMailInstructionSP3?.selectedItem.toString()
                    }
                }
                outwardPurposeDetailET3?.text.toString()
                    .let { (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_3 = it }
            } else {
                (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_3 = ""
                (activity as CIFRootActivity).customerCdd.cOUNTRY_OUTWARD_REMITT_3 = ""
                (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_3 = ""
                (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_3 = ""
            }
        } else {
            (activity as CIFRootActivity).customerCdd.iS_FOREGIN_FUND_OW = "N"
            (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_1 = ""
            (activity as CIFRootActivity).customerCdd.cOUNTRY_OUTWARD_REMITT_1 = ""
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_1 = ""
            (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_1 = ""
            (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_1 = ""
            (activity as CIFRootActivity).customerCdd.cOUNTRY_OUTWARD_REMITT_1 = ""
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_1 = ""
            (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_1 = ""
            (activity as CIFRootActivity).customerCdd.eXPECTED_OUTWARD_MTHLY_INCOME_PKR_1 = ""
            (activity as CIFRootActivity).customerCdd.cOUNTRY_OUTWARD_REMITT_1 = ""
            (activity as CIFRootActivity).customerCdd.dESC_COUNTRY_OUTWARD_REMITT_1 = ""
            (activity as CIFRootActivity).customerCdd.pURPOSE_OUTWARD_1 = ""
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD =
            (activity as CIFRootActivity).customerCdd
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as CIFRootActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as CIFRootActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID()
                .toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as CIFRootActivity).aofAccountInfoRequest = it
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try{
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                    if (findNavController().currentDestination?.id == R.id.CIFStep5_3)
                        findNavController().navigate(R.id.action_CIFStep5_3_to_CIFStep5_5)
                    (activity as CIFRootActivity).recyclerViewSetup()
                    (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
