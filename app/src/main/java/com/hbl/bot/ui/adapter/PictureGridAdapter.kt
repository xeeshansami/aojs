package com.hbl.bot.ui.adapter

import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hbl.bot.R
import kotlinx.android.synthetic.main.row_picture_grid.view.*

class PictureGridAdapter(private val mList: MutableList<CIF14Model>) :
    RecyclerView.Adapter<PictureGridAdapter.MyViewHolder>() {

    open inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val imgGrid = view.imageView!!
        val ivSelected = view.ivSelected!!
        val ivUploaded = view.ivUploaded!!
        val ivSelected2 = view.ivSelected2!!

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_picture_grid, parent, false)
        return MyViewHolder(layout)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val list = mList[position]
        holder.imgGrid.setImageURI(list.imageURI)
        if (list.isSelected) {
            holder.ivSelected.visibility = View.VISIBLE
            holder.ivSelected2.visibility = View.VISIBLE
        } else {
            holder.ivSelected.visibility = View.GONE
            holder.ivSelected2.visibility = View.GONE

        }
        if (list.isUploaded) {
            holder.ivUploaded.visibility = View.VISIBLE
        } else {
            holder.ivUploaded.visibility = View.GONE
        }
    }

}

data class CIF14Model(var imageURI: Uri, var isSelected: Boolean, var isUploaded: Boolean)