package com.hbl.bot.ui.fragments.spark

import android.content.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.CompoundButton
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.GenDataCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.baseRM.DataRequest
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.CIFResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_cifstep6.btNext
import kotlinx.android.synthetic.main.fragment_proceed_to_summary.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

class ProceedToSummary : Fragment(), View.OnClickListener {
    var myView: View? = null
    var isDislaimer = false
    lateinit var resp: CIFResponse
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_proceed_to_summary, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            (activity as CIFRootActivity).toolbar.visibility = View.GONE
            (activity as CIFRootActivity).siIndicator!!.visibility = View.GONE
            btnCompleteProcess.setOnClickListener(this)
            disclaimerYesNoButtonConditions()
            callGenerateCifAccount()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    private fun disclaimerYesNoButtonConditions() {
        yes_action.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (compoundButton.isChecked) {
                yes_action.setTextColor(resources!!.getColor(R.color.colorWhite))
                no_action.setTextColor(resources!!.getColor(R.color.green))
                yes_action.setBackgroundResource(R.drawable.bg_border_green);
                no_action.background = resources!!.getDrawable(R.drawable.bg_border);
            }
        })
        no_action.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (compoundButton.isChecked) {
                no_action.setTextColor(resources!!.getColor(R.color.colorWhite))
                yes_action.setTextColor(resources!!.getColor(R.color.green))
                no_action.setBackgroundResource(R.drawable.bg_border_green);
                yes_action.background = resources!!.getDrawable(R.drawable.bg_border);
            }
        })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btnCompleteProcess ->
                try {
                    if (validation()) {
//                callGenerateCifAccount()

                        findNavController().navigate(R.id.action_CIFStep15_to_summary)
//                (activity as CIFRootActivity).globalClass?.hideLoader()
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
                }
        }
    }

    private fun validation(): Boolean {
        return if (!yes_action!!.isChecked && !no_action!!.isChecked) {
            ToastUtils.normalShowToast(activity, "Please select yes/no option from disclaimer", 1)
            false
        } else {
            true
        }
    }

    private fun disclaimerDialog(isCifOrAccount: Int) {
        var dialogBuilder = AlertDialog.Builder(activity as CIFRootActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.popup_disclaimer, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()
        var toolbar = layoutView.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        var yes = layoutView.findViewById<Button>(R.id.yes_action)
        var no = layoutView.findViewById<Button>(R.id.no_action)
        toolbar.title = "DISCLAIMER"
        yes.setOnClickListener(View.OnClickListener {
            (activity as CIFRootActivity).finish()
            /* if(isCifOrAccount==1) {
                 toolbar.subtitle = "Generate Account"
                 callGenerateCifAccount()
             }else{
                 toolbar.subtitle = "Generate CIF"
                 callGenerateCif()
             }*/
            isDislaimer = true
            alertDialog.dismiss()
        })
        no.setOnClickListener(View.OnClickListener {
            /*if(isCifOrAccount==1) {
                toolbar.subtitle = "Generate Account"
                callGenerateCifAccount()
            }else{
                toolbar.subtitle = "Generate CIF"
                callGenerateCif()
            }*/
            (activity as CIFRootActivity).finish()
            isDislaimer = false
            alertDialog.dismiss()
        })
        alertDialog.show()
    }

    private fun callGenerateCifAccount() {
        var custSegType =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var dataRequest = DataRequest()
        (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.let {
            dataRequest.data = it
        }
        dataRequest.data?.TRACKING_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID
        if ((activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
            dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ACCT_SNG_JNT = "3"
            dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ACCT_SNG_JNT_DESC = "Minor Account"
        } else {
            dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ACCT_SNG_JNT = "1"
            dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ACCT_SNG_JNT_DESC = "Individual Account"
        }
        dataRequest.data?.CUST_ACCOUNTS?.get(0)?.BRANCHNAME =
            GlobalClass.sharedPreferenceManager!!.getLoginData()
                .getBRANCHNAME().toString()

        dataRequest.data?.CUST_ACCOUNTS?.get(0)?.BRANCH_CODE =
            GlobalClass.sharedPreferenceManager!!.getLoginData()
                .getBRANCHCODE().toString()
        dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ID_DOC_NO =
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ID_DOC_NO
        (activity as CIFRootActivity).aofAccountInfoRequest.isDisclaimer = isDislaimer
        var newName = (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        if (newName.length > 35) {
            newName = newName.substring(0, 35)
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME = newName
            (activity as CIFRootActivity).aofAccountInfoRequest.let {
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
            }
        }
        dataRequest.payload = Constants.PAYLOAD
        var json = Gson().toJson(dataRequest)
        HBLHRStore.instance?.genCIFAndAccount(
            RetrofitEnums.URL_HBL,
            dataRequest,
            object : GenDataCallBack {
                override fun GenCifSuccess(response: CIFResponse) {
                    try{
                    ToastUtils.normalShowToast(context, response.message,2)
                    resp = response
                    (activity as CIFRootActivity).globalClass?.hideLoader()

//                    var bundle = Bundle()
//                    response?.data?.get(0)
//                        ?.ACCOUNTNO.let {
//                            bundle.putString(
//                                Constants.ACCOUNT_NUMBER_KEY,
//                                it
//                            )
//                        }
//
//                    response?.data?.get(0)?.CIFNO.let {
//                        bundle.putString(
//                            Constants.CIF_NUMBER_KEY,
//                            it
//                        )
//                    }
//                    response?.data?.get(0)?.IBAN.let {
//                        bundle.putString(
//                            Constants.IBAN_NUMBER_KEY,
//                            it
//                        )
//                    }
//                    response.message.let {
//                        bundle.putString(Constants.ACCOUNT_NUMBER_MESSAGE_KEY, it)
//                    }
//                    findNavController().navigate(R.id.action_CIFStep15_to_summary, bundle)
//                    (activity as CIFRootActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
                    }
                }

                override fun GenCifFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun callGenerateCif() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var dataRequest = DataRequest()
        var newName = (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        (activity as CIFRootActivity).aofAccountInfoRequest.isDisclaimer = isDislaimer
        if (newName.length > 35) {
            newName = newName.substring(0, 35)
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME = newName
            (activity as CIFRootActivity).aofAccountInfoRequest.let {
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
            }
        }
        dataRequest.payload = Constants.PAYLOAD
        HBLHRStore.instance?.genCIF(
            RetrofitEnums.URL_HBL,
            dataRequest,
            object : GenDataCallBack {
                override fun GenCifSuccess(response: CIFResponse) {
                    ToastUtils.normalShowToast(context, response.message,2)
                    var bundle = Bundle()
                    response?.data?.get(0)
                        ?.ACCOUNTNO.let {
                            bundle.putString(
                                Constants.ACCOUNT_NUMBER_KEY,
                                it
                            )
                        }
                    response?.data?.get(0)?.CIFNO.let {
                        bundle.putString(
                            Constants.CIF_NUMBER_KEY,
                            it
                        )
                    }
                    response?.data?.get(0)?.IBAN.let {
                        bundle.putString(
                            Constants.IBAN_NUMBER_KEY,
                            it
                        )
                    }
                    response.message.let {
                        bundle.putString(Constants.ACCOUNT_NUMBER_MESSAGE_KEY, it)
                    }
                    findNavController().navigate(R.id.action_CIFStep15_to_CIFStep16, bundle)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

                override fun GenCifFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })

    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}