package com.hbl.bot.ui.fragments.system

import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.hbl.bot.ui.adapter.HomeGridAdapter
import com.hbl.bot.model.HomeGridModel
import com.hbl.bot.R
import com.hbl.bot.helper.PaddingDecoration
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.ui.activities.SplashActivity
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import kotlinx.android.synthetic.main.fragment_home.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException
import java.util.concurrent.Executors

class HomeFragment : Fragment(), View.OnClickListener {
    var sp: SharedPreferences? = null
    val USER_PREF = "USER_PREF"
    var root: View? = null

    @JvmField
    var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()
    val mList = mutableListOf<HomeGridModel>()
    private lateinit var homeGridAdapter: HomeGridAdapter
    var bundle = Bundle()
    lateinit var downloadController: DownloadController
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            root = inflater.inflate(R.layout.fragment_home, container, false)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        } finally {
            return root
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            init()
            recyclerViewSetup()
            sp = activity?.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE)
            getApkVer()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    private fun getApkVer() {
        val versionName =
            (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                (activity as NavigationDrawerActivity).packageName,
                0
            ).versionName
        val versionCode =
            (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                (activity as NavigationDrawerActivity).packageName,
                0
            ).versionCode
        apkcheck(versionCode, versionName)
    }

    private fun downloadAndInstall(apkUrl: String?, apkName: String?, msg: String) {
        try {
            DownloadController(requireContext(), apkUrl!!, apkName!!, msg)
//        checkInstallCompleted()
        } catch (e: UnsatisfiedLinkError) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            (activity as NavigationDrawerActivity).finish()
            (activity as NavigationDrawerActivity).startActivity(
                Intent(
                    activity as NavigationDrawerActivity,
                    SplashActivity::class.java
                )
            )
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    private fun apkcheck(versionCode: Int?, versionName: String?) {
        /* val base64EncryptionPass = sp!!.getString("password", "")
         val base64EncryptionIV = sp!!.getString("encryptedIV", "")
         val encryptionIv = Base64.decode(
             base64EncryptionIV,
             Base64.DEFAULT
         )
         val encryptedPassword = Base64.decode(
             base64EncryptionPass,
             Base64.DEFAULT
         )
         var keyStore: KeyStore? = null
         var token: String? = null
         try {
             keyStore = KeyStore.getInstance("AndroidKeyStore")
         } catch (e: KeyStoreException) {
             e.printStackTrace()
         }
         try {
             keyStore!!.load(null)
         } catch (e: CertificateException) {
             e.printStackTrace()
         } catch (e: IOException) {
             e.printStackTrace()
         } catch (e: NoSuchAlgorithmException) {
             e.printStackTrace()
         }
         var secretKey: SecretKey? = null
         try {
             secretKey = keyStore!!.getKey("Key", null) as SecretKey
         } catch (e: KeyStoreException) {
             e.printStackTrace()
         } catch (e: NoSuchAlgorithmException) {
             e.printStackTrace()
         } catch (e: UnrecoverableKeyException) {
             e.printStackTrace()
         }
         var cipher: Cipher? = null
         try {
             cipher =
                 Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7)
         } catch (e: NoSuchAlgorithmException) {
             e.printStackTrace()
         } catch (e: NoSuchPaddingException) {
             e.printStackTrace()
         }
         try {
             cipher!!.init(
                 Cipher.DECRYPT_MODE,
                 secretKey,
                 IvParameterSpec(encryptionIv)
             )
         } catch (e: InvalidAlgorithmParameterException) {
             e.printStackTrace()
         } catch (e: InvalidKeyException) {
             e.printStackTrace()
         }
         var passwordbyte: ByteArray? = ByteArray(0)
         try {
             passwordbyte = cipher!!.doFinal(encryptedPassword)
         } catch (e: BadPaddingException) {
             e.printStackTrace()
         } catch (e: IllegalBlockSizeException) {
             e.printStackTrace()
         }
         try {
             token = String(passwordbyte!!, Charset.forName("UTF-8"))
         } catch (e: UnsupportedEncodingException) {
             e.printStackTrace()
         }*/
        var request = APKCheckRequest()
        versionCode.let { request.aof_app_ver_code = it.toString() }
        versionName.let { request.aof_app_ver = it.toString() }
//        request.aof_app_ver = "1.1.0"
//        token.let { request.device_register_token = it }
        request.device_register_token = GlobalClass.DRToken
        HBLHRStore?.instance?.APKCheck(RetrofitEnums.URL_HBL, request, object : APKCheckCallBack {
            override fun APKCheckSuccess(response: APKCheckResponse) {
                try {
                    sharedPreferenceManager.downloadAPK = response
                    GlobalClass.checksum = response.data?.get(0)?.checksum.toString()
                    if (response.status.equals("02")) {
                        showUpdateDialog(response, versionName, versionCode)
                        Log.d("text", response.toString())
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as UnsatisfiedLinkError)
                    )
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NullPointerException)
                    )
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NumberFormatException)
                    )
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as InterruptedException)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as FileNotFoundException)
                    )
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassCastException)
                    )
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as TypeCastException)
                    )
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as SecurityException)
                    )
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalStateException)
                    )
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as OutOfMemoryError)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
                }
            }

            override fun APKCheckFailure(response: BaseResponse) {
                Log.d("text", response.toString())

            }
        })

    }

    private fun showUpdateDialog(
        response: APKCheckResponse,
        versionName: String?,
        versionCode: Int?,
    ) {
        var title = ""
        var msg = ""
        var isMDMChange = false
        if (response!!.data?.get(0)!!.versionName != versionName.toString()) {
            isMDMChange = true
            title = "Alert"
        } else if (response!!.data?.get(0)!!.packages!!.versionCode != versionCode) {
            title = "Update Required"
        }
        msg =
            "Note: ${response.message}\nVersion Name: ${response.data?.get(0)?.versionName}\nVersion Code: ${
                response.data?.get(0)?.packages?.versionCode
            }\n\nWhats New:\n${response.data?.get(0)?.packages?.changelog}"
        val builder = AlertDialog.Builder((activity as NavigationDrawerActivity))
        builder.setTitle(title)
        builder.setCancelable(false)
        builder.setMessage(msg)
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            if (isMDMChange) {
                (activity as NavigationDrawerActivity).finish()
                (activity as NavigationDrawerActivity).startActivity(
                    Intent(
                        activity as NavigationDrawerActivity,
                        SplashActivity::class.java
                    )
                )
            } else {
                downloadAndInstall(
                    response.data?.get(0)?.packages?.downloadUrl_1,
                    response.data?.get(0)?.packages?.name_1,
                    msg
                )
            }
        }
        builder.show()
    }

    private fun init() {
        aboutUs.setOnClickListener(this)
        sharedPreferenceManager.getInstance(activity)
        logout.setOnClickListener(View.OnClickListener {
//            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
            ToastUtils.normalShowToast(activity, "Logging out...", 1)
            Handler().postDelayed({
                (activity as NavigationDrawerActivity).finish()
                (activity as NavigationDrawerActivity).startActivity(
                    Intent(
                        activity as NavigationDrawerActivity,
                        SplashActivity::class.java
                    )
                )
                (activity as NavigationDrawerActivity).globalClass?.hideLoader()
            }, 2000)
        })
        (activity as NavigationDrawerActivity).sharedPreferenceManager.loginData.getUSERID().let {
            tvName.text = it
        }
    }

    private fun recyclerViewSetup() {
        mList.clear()
        mList.add(
            HomeGridModel(
                "Account Opening",
                R.drawable.account_opening_icon
            )
        )
        mList.add(
            HomeGridModel(
                "Biometrics",
                R.drawable.biometrics_icon
            )
        )

        homeGridAdapter =
            HomeGridAdapter(mList)
        rvHome.apply {
            layoutManager = GridLayoutManager(requireContext(), 2)
            adapter = homeGridAdapter

        }

        rvHome.addOnItemTouchListener(
            RecyclerTouchListener(
                requireContext(),
                rvHome,
                object :
                    RecyclerTouchListener.ClickListener {
                    override fun onClick(view: View, position: Int) {
                        try {
                            when (position) {
                                /*Account Opening*/
                                0 -> {
                                    bundle.putInt(Constants.ACTIVITY_KEY, Constants.SPARK)
                                    findNavController().navigate(
                                        R.id.action_nav_home_to_nav_dashboard,
                                        bundle
                                    )
                                }
                                /*Biometric*/
                                1 -> {
                                    /*Biometric*/
                                    bundle.putInt(Constants.ACTIVITY_KEY, Constants.CIF_1_4_SCREEN)
                                    findNavController().navigate(
                                        R.id.action_home_to_biometric,
                                        bundle
                                    )
                                }
                            }
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as UnsatisfiedLinkError)
                            )
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as NullPointerException)
                            )
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as IllegalArgumentException)
                            )
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as NumberFormatException)
                            )
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as InterruptedException)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as RuntimeException)
                            )
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as IOException)
                            )
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as FileNotFoundException)
                            )
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as ClassNotFoundException)
                            )
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as ActivityNotFoundException)
                            )
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as IndexOutOfBoundsException)
                            )
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as ArrayIndexOutOfBoundsException)
                            )
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as ClassCastException)
                            )
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as TypeCastException)
                            )
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as SecurityException)
                            )
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as IllegalStateException)
                            )
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as OutOfMemoryError)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as RuntimeException)
                            )
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as Exception)
                            )
                        }
                    }

                    override fun onLongClick(view: View?, position: Int) {

                    }
                })
        )

        rvHome.addItemDecoration(
            PaddingDecoration(resources.getInteger(R.integer.dashboard_spacing))
        )


    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }



    @SuppressLint("WrongConstant")
    private fun about() {
        try {
            var dialogBuilder = AlertDialog.Builder(activity as NavigationDrawerActivity)
            val layoutView: View = layoutInflater.inflate(R.layout.about_app_popup, null)
            dialogBuilder.setView(layoutView)
            var alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()
            var shareBtn = layoutView.findViewById<ImageView>(R.id.shareBtn)
            var ok_action = layoutView.findViewById<Button>(R.id.ok_action)
            var toolbar = layoutView.findViewById<Toolbar>(R.id.toolbar)
            toolbar.title = "About"
            var appname = layoutView.findViewById<TextView>(R.id.appname)
            appname.text = resources.getString(R.string.app_name)
            var UserID = layoutView.findViewById<TextView>(R.id.UserID)
            UserID.text = sharedPreferenceManager.loginData.getUSERID()
            var applicationID = layoutView.findViewById<TextView>(R.id.applicationID)
            var version = layoutView.findViewById<TextView>(R.id.version)
            var versionCod = layoutView.findViewById<TextView>(R.id.versionCode)
            var apkSize = layoutView.findViewById<TextView>(R.id.apkSize)
            var createDateOfFile = layoutView.findViewById<TextView>(R.id.createDateOfFile)
            var updaDateOfFile = layoutView.findViewById<TextView>(R.id.updaDateOfFile)
            var packageNameText = layoutView.findViewById<TextView>(R.id.packageName)
            packageNameText.text =
                (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                    (activity as NavigationDrawerActivity).packageName,
                    0
                ).packageName
            try {
                val versionName =
                    (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                        (activity as NavigationDrawerActivity).packageName,
                        0
                    ).versionName
                val versionCode =
                    (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                        (activity as NavigationDrawerActivity).packageName,
                        0
                    ).versionCode
                version.text = "$versionName"
                versionCod.text = "$versionCode"
                applicationID.text = "${GlobalClass.deviceID()}"
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            if (GlobalClass.getFileAPK().exists()) {
                apkSize.text = GlobalClass.getFolderSizeLabel(GlobalClass.getFileAPK())
                createDateOfFile.text = GlobalClass.firstModifiedDate()
                updaDateOfFile.text = GlobalClass.lastModifiedDate()
            } else {
                apkSize.text = "N/A"
                createDateOfFile.text = "N/A"
                updaDateOfFile.text = "N/A"
            }
            ok_action.setOnClickListener(View.OnClickListener {
                alertDialog.dismiss()
            })
            shareBtn.setOnClickListener(View.OnClickListener {
                (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
                var request = AppDetailsRequest()
                request.identifier = Constants.APP_DETAILS_IDENTIFIER
                request.apkSize = apkSize.text.toString()
                request.applicationID = applicationID.text.toString()
                request.applicationName = appname.text.toString()
                request.createdApkDate = createDateOfFile.text.toString()
                request.updateApkDate = updaDateOfFile.text.toString()
                request.userID =
                    (activity as NavigationDrawerActivity).sharedPreferenceManager.loginData.getUSERID()
                request.versionCode = versionCod.text.toString()
                request.versionName = version.text.toString()
                request.IMEI =
                    "${GlobalClass.getIMEIDeviceId(activity as NavigationDrawerActivity)}"
                request.packageName =
                    (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                        (activity as NavigationDrawerActivity).packageName,
                        0
                    ).packageName
                request.androidApiVersion =
                    (activity as NavigationDrawerActivity).globalClass!!.getAndroidVersion(android.os.Build.VERSION.SDK_INT)
                        .toString()
                HBLHRStore?.instance?.sendAppDetails(RetrofitEnums.URL_HBL, request, object :
                    AppStatusCallBack {
                    override fun AppStatusSuccess(response: AppStatusResponse) {
                        try {
                            ToastUtils.normalShowToast(activity, response.message, 2)
                            alertDialog.dismiss()
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as UnsatisfiedLinkError)
                            )
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as NullPointerException)
                            )
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as IllegalArgumentException)
                            )
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as NumberFormatException)
                            )
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as InterruptedException)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as RuntimeException)
                            )
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as IOException)
                            )
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as FileNotFoundException)
                            )
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as ClassNotFoundException)
                            )
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as ActivityNotFoundException)
                            )
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as IndexOutOfBoundsException)
                            )
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as ArrayIndexOutOfBoundsException)
                            )
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as ClassCastException)
                            )
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as TypeCastException)
                            )
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as SecurityException)
                            )
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as IllegalStateException)
                            )
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as OutOfMemoryError)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as RuntimeException)
                            )
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as NavigationDrawerActivity),
                                (e as Exception)
                            )
                        }
                        (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                    }

                    override fun AppStatusFailure(response: BaseResponse) {
                        Log.d("text", response.toString())
                        (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                    }
                })
            })
            alertDialog.show()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.aboutUs -> {
                about()
            }
        }
    }
}
