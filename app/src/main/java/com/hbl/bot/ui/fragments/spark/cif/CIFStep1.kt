package com.hbl.bot.ui.fragments.spark.cif


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.DocLovRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.ETBNTBRequest
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.ui.adapter.DocumentUploadAdapter
import com.hbl.bot.ui.customviews.DialogCallback
import com.hbl.bot.ui.customviews.FormSelectionView
import com.hbl.bot.utils.*
import com.hbl.bot.utils.Constants.INPUT_TYPE_ALPHANUMERIC
import com.hbl.bot.utils.Constants.INPUT_TYPE_NUMBER
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep1.*
import kotlinx.android.synthetic.main.fragment_cifstep1.btBack
import kotlinx.android.synthetic.main.fragment_cifstep1.btNext
import kotlinx.android.synthetic.main.fragment_cifstep1.formSectionHeader
import kotlinx.android.synthetic.main.view_form_input.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import kotlinx.android.synthetic.main.view_form_selection.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException
import java.util.concurrent.Executors


class CIFStep1 : Fragment(), View.OnClickListener {
    var csFirstCheck = false
    var dualNationalityCountriesLayout: ConstraintLayout? = null
    var pOMStatus: POMStatus? = null
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var selectCompanyNameSp: Spinner? = null
    var docTypeSpinner: Spinner? = null
    var customerSegmentSpinner: Spinner? = null
    var documentNumber: EditText? = null
    var nationalitySpinner: Spinner? = null
    var secondNationalitySpinner: Spinner? = null
    var dualNationalitySpinner: Spinner? = null
    var statusCode = "02"
    var ETBNTB = ""
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            /**
             * if workflow code 1 or 3 so CIF Screen display
             * else workflow code 2 so Account screen display
             * */
            if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.COM_PRINT_DOC == "1") {
                showSkipDialog()
            } else if (findNavController().currentDestination?.id == R.id.CIFStep1 && checkAllPagesModelsFilled()) {
                findNavController().navigate(R.id.action_CIFStep1_to_CIFStep16)
            } else if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep1, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.value = 2
        viewModel.currentFragmentIndex.value = 0
        var txt = resources!!.getString(R.string.customer_id_info) + " (1/2 Page)"
        formSectionHeader.header_tv_title.setText(
            GlobalClass.textColor(
                txt,
                24,
                txt.length,
                Color.RED
            )
        )
    }


    fun checkAllPagesModelsFilled(): Boolean {
        pOMStatus = POMStatus(activity as CIFRootActivity)
        //:TODO NOK PAGE CHECK
        return pOMStatus?.NextOfKin_Page_1_2_3() == 0
    }

    fun checkAccount1stPageFields(): Boolean {
        pOMStatus = POMStatus(activity as CIFRootActivity)
        //:TODO ACCOUNT 1st PAGES FIELDS CHECK
        return pOMStatus?.Account_Page_1() == 0
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            LocalBroadcastManager.getInstance(activity as CIFRootActivity)
                .registerReceiver(mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST)
                );
            btNext.setOnClickListener(this)
            btCheck.setOnClickListener(this)
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            header()
            init()
            load()
            spinnerConditions()
            onBackPressed(view)
            customerSegmentCheck()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }finally {

        }
    }

    private fun showSkipDialog() {
        val builder = AlertDialog.Builder((activity as CIFRootActivity))
        builder.setTitle("Information")
        builder.setCancelable(false)
        builder.setMessage((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.COM_PRINT_DOC_DESC)
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            try {
                if (findNavController().currentDestination?.id == R.id.CIFStep1) {
                    findNavController().navigate(R.id.action_CIFStep1_to_CIFStep14)
                }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
            } finally {

            }
        }
        builder.show()
    }

    private fun customerSegmentCheck() {
        customerSegmentSpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                try {
                    (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCustomerSegmentCode(
                        (activity as CIFRootActivity).sharedPreferenceManager.lovCustSegment,
                        customerSegmentSpinner?.selectedItem.toString()
                    ).let {
                        if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {
                            //:TODO if customer segment already exist and account 1st page fields have filled then show alert for the user
                            if (csFirstCheck&&checkAccount1stPageFields()
                                && (!(activity as CIFRootActivity).customerDemoGraphicx.CUST_TYPE.isNullOrEmpty() ||
                                        !(activity as CIFRootActivity).customerDemoGraphicx.CUSTTYPEDESC.isNullOrEmpty())
                            ) {
                                customerSegmentForcelyChangedAlert("WARNING","Are you sure want to change this customer segment?\n\nNOTE: If you will change the existing customer segment then you will not go over the any other pages from side menu","YES","NO")
                            }else{
                                csFirstCheck=true
                            }
                            // when select there is no item selected "Choose an item"  <--- hint
                            (activity as CIFRootActivity).customerDemoGraphicx.CUST_TYPE = it.toString()
                            (activity as CIFRootActivity).customerDemoGraphicx.CUSTTYPEDESC = customerSegmentSpinner?.selectedItem.toString()
                        }
                    }

//                    viewCheckListTV.isVisible = position != 0
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }finally {

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        viewCheckListTV.isClickable = true
        viewCheckListTV.movementMethod = LinkMovementMethod.getInstance()
        viewCheckListTV.text = Html.fromHtml("CHECK LIST")
        viewCheckListTV?.setOnClickListener {
            callDocumentType()
        }
    }

    private fun customerSegmentForcelyChangedAlert(
        title: String,
        msg: String,
        button1: String,
        button2: String

    ) {
        (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(
            context,
            title,
            msg,
            button1,
            button2,
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
                    (activity as CIFRootActivity).aofAccountInfoRequest.isForcelyChangedCustomerSegment=false
                    (activity as CIFRootActivity).aofAccountInfoRequest.let { (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo=it }
                }

                override fun onPositiveClicked() {
                    super.onPositiveClicked()
                    (activity as CIFRootActivity).aofAccountInfoRequest.isForcelyChangedCustomerSegment=true
                    (activity as CIFRootActivity).aofAccountInfoRequest.let { (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo=it }
                }
            })
    }


    private fun resetValuesBaseOnSegmet(it: String?) {
        if (it != "A2" || it != "A0" || it != "A4") {//housewife, student, unemployed
            (activity as CIFRootActivity).globalClass?.clearModel(
                SharedPreferenceManager.CUSTOMER_FIN_KEY
            )
        }
        if (it != "A5") {//landlord
            (activity as CIFRootActivity).customerCdd.nATR_EXP_MTH_HIGH_TURN_OVER =
                ""
            (activity as CIFRootActivity).customerCdd.dATE_INCP_BUSS_PROF =
                ""
        }
        if (it != "A3") {//self employees
            (activity as CIFRootActivity).customerCdd.aREA_OF_OPERATION_SUPP =
                ""
            (activity as CIFRootActivity).customerCdd.dESC_BUSS_ACTY_PRD_SERV =
                ""
            (activity as CIFRootActivity).customerCdd.dESC_BUSS_GEO_AREA_OPR_SUPP =
                ""
            (activity as CIFRootActivity).customerCdd.nAME_COUNTER_PARTY_1 =
                ""
            (activity as CIFRootActivity).customerCdd.nAME_COUNTER_PARTY_2 =
                ""
            (activity as CIFRootActivity).customerCdd.nAME_COUNTER_PARTY_3 =
                ""
        }
        if (it != "A6") {//hbl staff
            (activity as CIFRootActivity).customerDemoGraphicx.P_NUMBER = ""
        }

        if (it == "A1") {
            (activity as CIFRootActivity).customerDemoGraphicx.NATUREBUSINESSDESC = ""
            (activity as CIFRootActivity).customerDemoGraphicx.NATURE_BUSINESS = ""
            (activity as CIFRootActivity).customerDemoGraphicx.DESIGNATION = ""
            (activity as CIFRootActivity).customerDemoGraphicx.PROFESSION = ""
            (activity as CIFRootActivity).customerDemoGraphicx.PROFESSIONDESC = ""
            (activity as CIFRootActivity).customerDemoGraphicx.NAME_OF_COMPANY = ""
        }

        (activity as CIFRootActivity).customerDemoGraphicx.let {
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics = it
        }
        (activity as CIFRootActivity).customerCdd.let {
            (activity as CIFRootActivity).sharedPreferenceManager.customerCDD = it
        }

        //for CDD
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }


        //for DemoGraphics
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }

        //for FIN
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }

        //for main sp
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }

    }

    private fun callDocumentType() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = DocLovRequest()
        lovRequest.identifier = Constants.DOCUMENT_TYPE_INDENTIFIER
        lovRequest._SEGCD = (activity as CIFRootActivity).customerDemoGraphicx.CUST_TYPE
        lovRequest._SEGTY = (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE
        lovRequest._WORKFLOW = (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE
        lovRequest._TYPE = ""
        HBLHRStore.instance?.getDocumentType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : DocumentTypeCallBack {
                override fun DocumentTypeSuccess(response: DocTypeResponse) {
                    try {
                        response.data?.get(0)?.data.let {
                            avatarDialog(activity as CIFRootActivity, it!!)
                        }
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    } finally {

                    }
                }


                override fun DocumentTypeFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) {callDocumentType() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    fun avatarDialog(context: Context, it: ArrayList<DocumentType>) {
        var dialogBuilder = AlertDialog.Builder(activity as CIFRootActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.customer_popup, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        var cancel_action = layoutView.findViewById<Button>(R.id.cancel_action)
        var checListRV = layoutView.findViewById<RecyclerView>(R.id.checListRV)
        var documentUploadAdapter = DocumentUploadAdapter(
            it
        )
        checListRV?.apply {
            layoutManager = LinearLayoutManager(context as CIFRootActivity)
            adapter = documentUploadAdapter
            documentUploadAdapter.notifyDataSetChanged()
        }
        cancel_action.setOnClickListener(View.OnClickListener {
            alertDialog.dismiss()
        })
        alertDialog.show()
    }

    fun load() {
        try {
            (activity as CIFRootActivity).customerInfo.ID_DOCUMENT_NO.let { t ->
                documentNumber?.setText(t)
            }
//            if ((activity as CIFRootActivity).sharedPreferenceManager.lovDoctype.isNullOrEmpty()) {
            callDocs()
//            } else {
//                docTypeData((activity as CIFRootActivity).sharedPreferenceManager.lovDoctype)
//            }
            if (!(activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
                statusCode = "00"
                (activity as CIFRootActivity).globalClass?.setDisbaled(documentNumber!!)
                (activity as CIFRootActivity).globalClass?.setDisbaled(docTypeSpinner!!)
                (activity as CIFRootActivity).globalClass?.setDisbaled(btCheck!!)
                ETBNTB = (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.toString()
            } else {
                statusCode = "02"
                (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
                (activity as CIFRootActivity).globalClass?.setEnabled(docTypeSpinner!!, true)
                (activity as CIFRootActivity).globalClass?.setEnabled(btCheck!!, true)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {

        }
    }

    fun init() {
        btBack.setOnClickListener(this)
//        (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
        docTypeSpinner = fsvDocumentType_spinner.getSpinner(R.id.doctypeSpinnerId)
        customerSegmentSpinner = customer_segment_spinner.getSpinner(R.id.customer_segment_spinner)
        documentNumber = documentNumber_et.getTextFromEditText(R.id.documentNumber_et)
        nationalitySpinner = fivNationality_spinner.getSpinner(R.id.nationality_id)
        dualNationalitySpinner = fivDualNationality_spinner.getSpinner(R.id.dual_nationality_id)
        secondNationalitySpinner =
            fivSecondNationality_spinner.getSpinner(R.id.secondNationalitySpinner)
        dualNationalityCountriesLayout =
            fivSecondNationality_spinner.getLayout(R.id.secondNationalitySpinnerLayout)
        documentNumber?.requestFocus()
    }

    private fun spinnerConditions() {
        docTypeSpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                i: Int,
                l: Long,
            ) {
                try {
                    (activity as CIFRootActivity).globalClass?.showDialog(activity)
                    documentNumberCustomConditions(i)
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
        /*  customerSegmentSpinner?.onItemSelectedListener = object :
              AdapterView.OnItemSelectedListener {
              @SuppressLint("Range")
              override fun onItemSelected(
                  adapterView: AdapterView<*>?,
                  view: View?,
                  pos: Int,
                  l: Long,
              ) {
                  try {
                      if (csFirstCheck) {
                          if (!(activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE.isNullOrEmpty()) {
                              (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(
                                  activity,
                                  "Alert\n",
                                  "Are you sure want to change existing CUSTOMER SEGMENT?\n\nNOTE: If you will change the existing CUSTOMER SEGMENT then all drafted data and conditions will be cleared",
                                  "YES",
                                  "NO",
                                  object : DialogCallback {
                                      override fun onNegativeClicked() {
                                          super.onNegativeClicked()
                                          (activity as CIFRootActivity).globalClass?.findSpinnerIndexFromCustomerSegmentCode(
                                              (activity as CIFRootActivity).sharedPreferenceManager.lovCustSegment,
                                              (activity as CIFRootActivity).customerDemoGraphicx.CUSTTYPEDESC
                                          ).let {
                                              customerSegmentSpinner?.setSelection(it!!)
                                          }
                                          csFirstCheck = false
                                      }

                                      override fun onPositiveClicked() {
                                          super.onPositiveClicked()
                                          (activity as CIFRootActivity).globalClass?.clearSubmitAOF()
                                          (activity as CIFRootActivity).init()
                                          (activity as CIFRootActivity).recyclerViewSetup()
                                      }
                                  })
                          }
                      } else {
                          csFirstCheck = true
                      }

                  } catch (e: UnsatisfiedLinkError) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: NullPointerException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: IllegalArgumentException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: NumberFormatException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: InterruptedException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: RuntimeException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: IOException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: FileNotFoundException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: ClassNotFoundException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: ActivityNotFoundException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: IndexOutOfBoundsException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: SecurityException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: IllegalStateException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: OutOfMemoryError) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )

                  } catch (e: RuntimeException) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: Exception) {
                      SendEmail.SendEmail(
                          activity as CIFRootActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } finally {

                  }
              }

              override fun onNothingSelected(adapterView: AdapterView<*>?) {
              }
          }*/
        dualNationalitySpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (pos == 0) {
                        var item = ArrayList<Country>()
                        fivSecondNationality_spinner?.setItemForfivSecondNationality(item)
                        (activity as CIFRootActivity).globalClass?.setDisbaled(
                            secondNationalitySpinner!!)
                        fivSecondNationality_spinner?.mandateSpinner?.text = ""
                    } else {
                        fivSecondNationality_spinner?.mandateSpinner?.text = "*"
                        (activity as CIFRootActivity).globalClass?.setEnabled(
                            secondNationalitySpinner!!,
                            true
                        )
                        if ((activity as CIFRootActivity).sharedPreferenceManager.lovCountries.isNullOrEmpty()) {
                            callAllCountries()
                        } else {
                            setAllCountries((activity as CIFRootActivity).sharedPreferenceManager.lovCountries)

                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
        secondNationalitySpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    /*  In case of dual nationality Pakistan cannot be selected in second nationality.*/
                    if (secondNationalitySpinner?.isEnabled == true) {
                        if (nationalitySpinner?.selectedItem.toString() != "Pakistan" && secondNationalitySpinner?.selectedItem.toString() == "Pakistan") {
//                        secondNationalitySpinner?.
                            secondNationalitySpinner?.setSelection(0)
                            ToastUtils.normalShowToast(
                                activity,
                                resources!!.getString(R.string.second_nationality_error), 1
                            )
                        } else if (secondNationalitySpinner?.selectedItemPosition != 0 && nationalitySpinner?.selectedItem.toString() == secondNationalitySpinner?.selectedItem.toString()) {
                            secondNationalitySpinner?.setSelection(0)
                            ToastUtils.normalShowToast(
                                activity,
                                resources!!.getString(R.string.second_2_nationality_error), 1
                            )
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }

        nationalitySpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    /*  In case of dual nationality Pakistan cannot be selected in second nationality.*/
                    if (docTypeSpinner?.selectedItem.toString() == "PASSPORT" && nationalitySpinner?.selectedItem.toString() == "Pakistan") {
                        nationalitySpinner?.setSelection(0)
                        ToastUtils.normalShowToast(
                            activity,
                            resources!!.getString(R.string.nationality_password_err),1
                        )
                    } else if (docTypeSpinner?.selectedItem.toString() == "ARC" && nationalitySpinner?.selectedItem.toString() == "Pakistan") {
                        nationalitySpinner?.setSelection(0)
                        ToastUtils.normalShowToast(
                            activity,
                            resources!!.getString(R.string.arc_err),1
                        )
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
    }

    fun documentNumberCustomConditions(position: Int) {
        /*call CustomerSegment when doctype select condition name and value*/
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromDocTypeCode(
            (activity as CIFRootActivity).docTypeList,
            docTypeSpinner?.selectedItem.toString()
        ).let {
            callCustomerSegment(docTypeSpinner?.selectedItem.toString(), it.toString())
        }
        var doctypeIndex =
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromDocTypeIndexValue(
                (activity as CIFRootActivity).sharedPreferenceManager.lovDoctype,
                (activity as CIFRootActivity).customerInfo.ID_DOCUMENT_TYPE_DESC
            )
        /*Condition when click doctype list auto generate document number pattern according to doctype*/
        if (doctypeIndex != position) {
            documentNumber?.setText("")
        }
        if (position == 0 && !(activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as CIFRootActivity).globalClass?.setDisbaled(documentNumber!!)
        } else if (position == 1 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            /*CNIC 13 Numeric*/
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "CNIC")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber!!,
                13,
                INPUT_TYPE_NUMBER
            )
        } else /*if (position == 2 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            *//*PASSPORT 20 AlphaNumeric*//*
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "06")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber,
                19,
                INPUT_TYPE_ALPHANUMERIC
            )
        } else */ if (position == 2 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            /*SNIC 13 Numeric*/
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "SNIC")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber!!,
                13,
                INPUT_TYPE_NUMBER
            )
        } else if (position == 4 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            CRC 20 AlphaNumeric
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "CRC")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber!!,
                13,
                INPUT_TYPE_NUMBER
            )
        } else if (position == 3 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            /*NICOP 13 Numeric*/
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "NICOP")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber!!,
                13,
                INPUT_TYPE_NUMBER
            )
        } else if (position == 6 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            /*CRC 13 Numeric*/
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "CRC")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber!!,
                13,
                INPUT_TYPE_NUMBER
            )
        } else if (position == 7 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            /*POC 13 AlphaNumeric*/
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "POC")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber!!,
                13,
                INPUT_TYPE_ALPHANUMERIC
            )
        } else if (position == 8 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            /*BirthCertificate 18 Numeric*/
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "08")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber!!,
                18,
                INPUT_TYPE_NUMBER
            )
        } else if (position == 9 && (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
            /*POR Afghan Refugee Card 13 AlphaNumeric*/
            (activity as CIFRootActivity).globalClass?.setEnabled(documentNumber!!, true)
            documentSetPrefix(position, "POR")
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                documentNumber!!,
                13,
                INPUT_TYPE_ALPHANUMERIC
            )
        }
    }

    fun callDocs() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.DOCS_IDENTIFIER
        Gson().toJson(lovRequest)
        HBLHRStore.instance?.getDocs(RetrofitEnums.URL_HBL, lovRequest, object : DocsCallBack {
            override fun DocsSuccess(response: DocsResponse) {
                try {
                    response.data?.let {
                        docTypeData(it)
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                } finally {

                }
            }

            override fun DocsFailure(response: BaseResponse) {
//                Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                ToastUtils.normalShowToast(activity, response.message, 1)
                (activity as CIFRootActivity).globalClass?.hideLoader()
            }
        })
    }

    fun hawComapnyData(it: ArrayList<DocsData>,spinner:FormSelectionView) {
        spinner.setItemForDocs(it)
        (activity as CIFRootActivity).docTypeList = it
        (activity as CIFRootActivity).sharedPreferenceManager.lovDoctype = it
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromDocTypeIndexValue(
            it,
            (activity as CIFRootActivity).customerInfo.ID_DOCUMENT_TYPE_DESC
        ).let {
            docTypeSpinner?.setSelection(it!!)
        }
        fsvDocumentType_spinner.remainSelection(it.size)
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()
        } else {
            setBool((activity as CIFRootActivity).sharedPreferenceManager.lovBool)
        }
    }

    fun docTypeData(it: ArrayList<DocsData>) {
        fsvDocumentType_spinner.setItemForDocs(it)
        (activity as CIFRootActivity).docTypeList = it
        (activity as CIFRootActivity).sharedPreferenceManager.lovDoctype = it
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromDocTypeIndexValue(
            it,
            (activity as CIFRootActivity).customerInfo.ID_DOCUMENT_TYPE_DESC
        ).let {
            docTypeSpinner?.setSelection(it!!)
        }
        fsvDocumentType_spinner.remainSelection(it.size)
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()
        } else {
            setBool((activity as CIFRootActivity).sharedPreferenceManager.lovBool)
        }
    }


    fun callCustomerSegment(conditionName: String, conditionValue: String) {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.CUSTOMER_SEGMENT_IDENTIFIER
        lovRequest.condition = true
        lovRequest.conditionname = Constants.ID_DOC_TYPE
        lovRequest.conditionvalue = conditionValue
        Gson().toJson(lovRequest)
        HBLHRStore.instance?.getCustomerSegment(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CustomerSegmentCallBack {
                override fun CustomerSegmentSuccess(response: CustomerSegmentResponse) {
                    response.data?.let {
                        try {
                            setCustSeg(it, conditionName, conditionValue)
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                        } finally {

                        }
                    }
                }

                override fun CustomerSegmentFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { callCustomerSegment(conditionName, conditionValue) }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun setCustSeg(it: ArrayList<CustomerSegment>, conditionName: String, conditionValue: String) {
        (activity as CIFRootActivity).customerSegmentList = it
        customer_segment_spinner.setItemForCustomerSegment(it)
        (activity as CIFRootActivity).sharedPreferenceManager.lovCustSegment = it

        (activity as CIFRootActivity).globalClass?.findSpinnerIndexFromCustomerSegmentCode(
            it,
            (activity as CIFRootActivity).customerDemoGraphicx.CUSTTYPEDESC
        ).let {
            customerSegmentSpinner?.setSelection(it!!)
        }
        customer_segment_spinner.remainSelection(it.size)
        callSpecificCountries(conditionName, conditionValue)
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        Gson().toJson(lovRequest)
        HBLHRStore.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    try {
                        response.data?.let {
                            setBool(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun BoolFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun setBool(it: ArrayList<Bool>) {
        (activity as CIFRootActivity).boolList = it
        fivDualNationality_spinner?.setItemForBools(it)
        (activity as CIFRootActivity).sharedPreferenceManager.lovBool = it
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolIndex(
            it,
            (activity as CIFRootActivity).customerInfo.dualnationality
        ).let {
            dualNationalitySpinner?.setSelection(it!!)
        }
//        (activity as CIFRootActivity).globalClass?.hideLoader()
    }

    fun callSpecificCountries(conditionName: String, conditionValue: String) {
        var lovRequest = LovRequest()
        lovRequest.condition = true
        lovRequest.conditionname = Constants.ID_DOC_TYPE
        lovRequest.conditionvalue = conditionValue
        lovRequest.conditionmatrix = Constants.COUNTRY_MATRIX
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(lovRequest)
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    try {
                        response.data?.let {
                            setSpecificCountry(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun CountryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) {callSpecificCountries(conditionName, conditionValue) }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    fun setSpecificCountry(it: ArrayList<Country>) {
        (activity as CIFRootActivity).specificCountryList = it
        fivNationality_spinner?.setItemForfivNationality(it)
        (activity as CIFRootActivity).sharedPreferenceManager.lovSpecificCountry = it
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as CIFRootActivity).customerInfo.nationalitydesc
        ).let {
            nationalitySpinner?.setSelection(it!!)
        }
        fivNationality_spinner.remainSelection(it.size)
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovCountries.isNullOrEmpty()) {
            callAllCountries()
        } else {
            setAllCountries((activity as CIFRootActivity).sharedPreferenceManager.lovCountries)
        }
    }

    fun callAllCountries() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(lovRequest)
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    try {
                        response.data?.let {
                            setAllCountries(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }finally {

                    }
                }

                override fun CountryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    fun setAllCountries(it: ArrayList<Country>) {
        fivSecondNationality_spinner?.setItemForfivSecondNationality(it)
        (activity as CIFRootActivity).sharedPreferenceManager.lovCountries = it
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as CIFRootActivity).customerInfo.secondnationalitydesc
        ).let {
            secondNationalitySpinner?.setSelection(it!!)
        }
        fivSecondNationality_spinner.remainSelection(it.size)
        (activity as CIFRootActivity).globalClass?.hideLoader()
    }

    fun subETBNTB() {
        var etbntbRequest = ETBNTBRequest()
        etbntbRequest.CHANNELLID = "asd"
        etbntbRequest.TELLERID = "asd"
        etbntbRequest.CHANNELDATE = "asd"
        etbntbRequest.CHANNELTIME = "asd"
        etbntbRequest.CHANNELSRLNO = "asd"
        etbntbRequest.SERVICENAME = "C"
        etbntbRequest.REMARK = "asd"
        documentNumber?.text.toString().trim().let {
            etbntbRequest.CNIC = it
        }
        etbntbRequest.payload = "U2FsdGVkX1+eMelgQeJlI51PUXnmebgWMVjt8eGRb7kTZ14o9CGJ17PkRN/R7Ol/LVVjoXNA3uAtF05FVtvweDF/ZQKbqShdXACJgx0H6tI="
        Gson().toJson(etbntbRequest)
        HBLHRStore.instance?.submitETBNTB(
            RetrofitEnums.URL_HBL,
            etbntbRequest,
            object : ETBNTBCallBack {
                override fun ETBNTBSuccess(response: ETBNTBResponse) {
                    try {
                        statusCode = response.status!!
                        ETBNTB = response?.data?.get(0)?.ETBNTBFLAG.toString()
                        if (ETBNTB != "C" && statusCode != "02") {
                            if (ETBNTB == "N") {
                                response.data?.get(0)?.ETBNTBFLAG.let {
                                    ETBNTB = it!!
                                    (activity as CIFRootActivity).etbntbFLAG = it!!
                                    (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                                        it!!
                                }
                                response.data?.get(0)?.REMEDIATE.let {
                                    (activity as CIFRootActivity).REMEDIATE = it!!
                                }
                                (activity as CIFRootActivity).globalClass?.setDisbaled(documentNumber!!)
                                (activity as CIFRootActivity).globalClass?.setDisbaled(docTypeSpinner!!)
                                (activity as CIFRootActivity).globalClass?.setDisbaled(btCheck!!)
                                ToastUtils.normalShowToast(activity, response.message,2)
                            } else {
                                ToastUtils.normalShowToast(activity, response.message, 1)
                            }

                        } else {
                            ToastUtils.normalShowToast(activity, response.message, 1)
                        }

                        (activity as CIFRootActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun ETBNTBFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) {subETBNTB() }
                    statusCode = "02"
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btCheck -> {
                try {
                    if (etbntbValidation()) {
                        (activity as CIFRootActivity).globalClass?.showDialog(activity)
                        subETBNTB()
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                } finally {

                }
            }
            R.id.btNext -> {
                try {
                    Log.i("Clicks", "a")
//                findNavController().navigate(R.id.temp)
                    if (validation()) {
                        /*E, N move farward to next screen else C stop here */
                        if (statusCode == "00" && ETBNTB == "N") {
                            (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!, false)
                            (activity as CIFRootActivity).globalClass?.showDialog(activity)
                            Executors.newSingleThreadExecutor().execute(Runnable {
                                saveAndNext()
                                (activity as CIFRootActivity).init()
                                (activity as CIFRootActivity).runOnUiThread {
                                    (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!,
                                        true)
                                    (activity as CIFRootActivity).recyclerViewSetup()
                                    if (findNavController().currentDestination?.id == R.id.CIFStep1) {
                                        findNavController().navigate(R.id.action_CIFStep1_to_CIFStep1_3)
                                    }
                                    (activity as CIFRootActivity).globalClass?.hideLoader()
                                }
                            })

                        } else if (statusCode == "02" && ETBNTB == "C") {
                            ToastUtils.normalShowToast(
                                activity,
                                activity?.getString(R.string.etbntb_err), 1
                            )
                        } else if (ETBNTB == "E") {
                            ToastUtils.normalShowToast(
                                activity,
                                activity?.getString(R.string.etbntb_err2), 1
                            )
                        } else {
                            ToastUtils.normalShowToast(
                                activity,
                                activity?.getString(R.string.cnic_err_str), 1
                            )
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }finally {

                }
            }
            R.id.btBack -> {
                (activity as CIFRootActivity).finish()
            }
        }
    }

    fun saveAndNext() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in CIFRootActivity*/
        /*set the values from widgets and another models*/
        val startPrefix = startPrefixOfDocument.text.toString().trim()
        documentNumber?.text.toString().trim().let {
            (activity as CIFRootActivity).cnicNumber = startPrefix + it
            //TODO: insert document number
            ((activity as CIFRootActivity).aofAccountInfoRequest).ID_DOC_NO = startPrefix + it
            (activity as CIFRootActivity).customerInfo.ID_DOCUMENT_NO = startPrefix + it
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromDocTypeCode(
            (activity as CIFRootActivity).docTypeList,
            docTypeSpinner?.selectedItem.toString()
        ).let {
            //TODO: set iDDOCUMENTNO
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                ((activity as CIFRootActivity).aofAccountInfoRequest).ID_DOCUMENT_TYPE =
                    it.toString()
                (activity as CIFRootActivity).customerInfo.ID_DOCUMENT_TYPE = it.toString()
                (activity as CIFRootActivity).customerInfo.ID_DOCUMENT_TYPE_DESC =
                    docTypeSpinner?.selectedItem.toString()
            }
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCustomerSegmentCode(
            (activity as CIFRootActivity).customerSegmentList,
            customerSegmentSpinner?.selectedItem.toString()
        ).let {
            //TODO: set customerSegment
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerDemoGraphicx.CUST_TYPE = it.toString()
                (activity as CIFRootActivity).customerDemoGraphicx.CUSTTYPEDESC =
                    customerSegmentSpinner?.selectedItem.toString()
//                resetValuesBaseOnSegmet(it.toString())
            }
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
            (activity as CIFRootActivity).specificCountryList,
            nationalitySpinner?.selectedItem.toString()
        ).let {
            //TODO: set nATIONALITY
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerInfo.NATIONALITY = it.toString()
                (activity as CIFRootActivity).customerInfo.NATIONALITYDESC =
                    nationalitySpinner?.selectedItem.toString()
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            dualNationalitySpinner?.selectedItem.toString()
        ).let {
            //TODO: set dUALNATIONALITY
            (activity as CIFRootActivity).customerInfo.DUAL_NATIONALITY = it
        }

        /*If secondnationailty is enabeld then set the value of second nationality*/
        if (secondNationalitySpinner?.isEnabled == true && dualNationalitySpinner?.selectedItemPosition == 1) {
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                secondNationalitySpinner?.selectedItem.toString()
            ).let {
                //TODO: set sECONDNATIONALITY
                if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as CIFRootActivity).customerInfo.SECOND_NATIONALITY = it.toString()
                    (activity as CIFRootActivity).customerInfo.SECONDNATIONALITYDESC =
                        secondNationalitySpinner?.selectedItem.toString()
                }
            }
            Log.i(
                "SecondNationiltyValue",
                (activity as CIFRootActivity).customerInfo.SECOND_NATIONALITY.toString()
            )
        } else {
            (activity as CIFRootActivity).customerInfo.SECOND_NATIONALITY = ""
            (activity as CIFRootActivity).customerInfo.SECONDNATIONALITYDESC = ""
        }
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_CODE.let {
            //TODO: set BRANCH_CODE
            (activity as CIFRootActivity).customerAccounts.BRANCH_CODE = it!!
        }
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            //TODO: set BRANCHNAME
            (activity as CIFRootActivity).customerAccounts.BRANCHNAME = it!!
        }
        ETBNTB.let {
            if (!it.isNullOrEmpty()) {
                (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG = it
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo =
                    (activity as CIFRootActivity).aofAccountInfoRequest
            }
        }

//        (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
//            ?.get(0)?.itemDescription.let {
//                (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
//            }
        /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in CIFRootActivity*/
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount =
            ((activity as CIFRootActivity).customerAccounts)
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo =
            ((activity as CIFRootActivity).customerInfo)
        (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo =
            ((activity as CIFRootActivity).aofAccountInfoRequest)
        (activity as CIFRootActivity).sharedPreferenceManager.setCustomerDemoGraphics((activity as CIFRootActivity).customerDemoGraphicx)
        /*Cnic verification set and value reinitialise*/
    }

    fun validation(): Boolean {
        return documentNumberValidation()
    }

    fun etbntbValidation(): Boolean {
        val position = docTypeSpinner?.selectedItemPosition
        val length = documentNumber?.text.toString().length
        if (position == 0) {
            ToastUtils.normalShowToast(activity, resources!!.getString(R.string.errDOCTTypeETBN), 1);
            return false
        } else if (length == 0) {
            documentNumber?.setError(resources!!.getString(R.string.errCNICETBN));
            documentNumber?.requestFocus();
            return false
        } else if (position == 1 && length!! < 13) {
            /*CNIC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorCNIC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 2 && length!! < 1) {
            /*PASSPORT 20 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorPassport));
            documentNumber?.requestFocus();
            return false
        } else if (position == 3 && length!! < 13) {
            /*SNIC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorSNIC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 4 && length!! < 13) {
            /*CRC 20 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorARC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 5 && length!! < 13) {
            /*NICOP 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorNICOP));
            documentNumber?.requestFocus();
            return false
        } else if (position == 6 && length!! < 13) {
            /*CRC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorCRC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 7 && length!! < 13) {
            /*POC 13 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorPOC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 8 && length!! < 18) {
            /*BirthCertificate 18 Number*/
            documentNumber?.setError(resources!!.getString(R.string.errorBirthCertificate));
            documentNumber?.requestFocus();
            return false
        } else if (position == 9 && length!! < 13) {
            /*POR Afghan Refugee Card 13 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorAfghanRefugeeCard));
            documentNumber?.requestFocus();
            return false
        } else {
            return true
        }
    }

    fun documentNumberValidation(): Boolean {
        val position = docTypeSpinner?.selectedItemPosition
        val length = documentNumber?.text.toString().length
        if (position == 1 && length!! < 13) {
            /*CNIC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorCNIC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 2 && length!! < 1) {
            /*PASSPORT 20 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorPassport));
            documentNumber?.requestFocus();
            return false
        } else if (position == 3 && length!! < 13) {
            /*SNIC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorSNIC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 4 && length!! < 13) {
            /*CRC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorARC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 5 && length!! < 13) {
            /*NICOP 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorNICOP));
            documentNumber?.requestFocus();
            return false
        } else if (position == 6 && length!! < 13) {
            /*CRC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorCRC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 7 && length!! < 13) {
            /*POC 13 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorPOC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 8 && length!! < 18) {
            /*BirthCertificate 18 Number*/
            documentNumber?.setError(resources!!.getString(R.string.errorBirthCertificate));
            documentNumber?.requestFocus();
            return false
        } else if (position == 9 && length!! < 13) {
            /*POR Afghan Refugee Card 13 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorAfghanRefugeeCard));
            documentNumber?.requestFocus();
            return false
        } else if (docTypeSpinner?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(activity, resources!!.getString(R.string.doctypeErr), 1)
            return false
        } else if (customerSegmentSpinner?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.customerSegmentErr),
                1
            )
            return false
        } else if (nationalitySpinner?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity, resources!!.getString(R.string.nationalityErr), 1
            )

            return false
        } else if (dualNationalitySpinner?.selectedItemPosition == 1) {
            if (secondNationalitySpinner?.selectedItemPosition == 0) {
                ToastUtils.normalShowToast(
                    activity, resources!!.getString(R.string.please_select_second_nationlity), 1
                )

                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    fun documentSetPrefix(position: Int, label: String) {
        documentNumber?.requestFocus()
        if (position == 2 && label.equals("06") && !(activity as CIFRootActivity).customerInfo.iddocumenttype.equals(
                "06"
            )
        ) {
            documentNumber?.setPadding(22, 0, 0, 0)
            startPrefixOfDocument.visibility = View.VISIBLE
            startPrefixOfDocument?.setText("P")
        } else if (position == 8 && label.equals("08") && !(activity as CIFRootActivity).customerInfo.iddocumenttype.equals(
                "08"
            )
        ) {
            documentNumber?.setPadding(22, 0, 0, 0)
            startPrefixOfDocument.visibility = View.VISIBLE
            startPrefixOfDocument?.setText("B")
        } else {
            documentNumber?.setPadding(0, 0, 0, 0)
            startPrefixOfDocument.visibility = View.GONE
            startPrefixOfDocument?.setText("")
        }
    }

    private fun onBackPressed(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                val navController =
                    (activity as CIFRootActivity).findNavController(R.id.cifHostFragment)
                if (navController.currentDestination?.id == R.id.CIFStep1) {
                    Log.i("onBackPress", "Not Up Finish All Fragment")
                    (activity as CIFRootActivity).finish()
                } else {
                    Log.i("onBackPress", "Up")
                    navController.popBackStack()
                }
                true
            } else {
                false
            }
        }
    }

}
