package com.hbl.bot.ui.fragments.cif


import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.BoolCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.GenerateCIFCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.PepCategoriesCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.PepSubCategoriesCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.base.PepSubCategoriesRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.PepCategories
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep1_13.formSectionHeader
import kotlinx.android.synthetic.main.fragment_cifstep1_13.*
import kotlinx.android.synthetic.main.fragment_cifstep1_13.btBack
import kotlinx.android.synthetic.main.fragment_cifstep1_13.btNext
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.util.ArrayList
import java.io.IOException
import java.lang.ClassCastException

/**import java.io.FileNotFoundException
 * A simple [Fragment] subclass.
 */
class HawCifPEP3Fragment : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var foriegnPepSp: Spinner? = null
    var domesticPepSp: Spinner? = null
    var fivPepCategoriesSP: Spinner? = null
    var fivPepSubCategoriesSP: Spinner? = null
    var analysisPep: EditText? = null
    var fivDetailOfAdverseMediaET: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try{
        if (myView == null) {
            // Inflate the layout for this fragment
            myView = inflater.inflate(R.layout.fragment_cifstep1_13, container, false)
        }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(3)
        viewModel.currentFragmentIndex.setValue(2)
        val txt = resources.getString(R.string.pep_details)
        val txt1 = " (3/3 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try{
        super.onViewCreated(view, savedInstanceState)
        btNext.setOnClickListener(this)
        btBack.setOnClickListener(this)
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
        header()
        init()
        setLengthAndType()
        load()
//        if (GlobalClass.isDummyDataTrue) {
//            loadDummyData()
//        }
        setConditions()
        onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    fun setLengthAndType() {
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivDetailOfAdverseMediaET!!,
            50,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            analysisPep!!,
            50,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
    }

    fun setConditions() {
        fivPepCategoriesSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                i: Int,
                l: Long
            ) {
                try{
                (activity as HawActivity).globalClass?.findSpinnerPositionFromPepCategoriesCode(
                    (activity as HawActivity).pepCategoriesList,
                    fivPepCategoriesSP?.selectedItem.toString()
                ).let {
                    callPepSubCategories(it!!)
                }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
        }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        })
        if ((activity as HawActivity).sharedPreferenceManager.customerPep.ADVERSE_MEDIA_CUSTOMER == "1") {
            fivDetailOfAdverseMedia.mandateInput.text = "*"
        } else {
            fivDetailOfAdverseMedia.mandateInput.text = ""
        }
        if ((activity as HawActivity).customerPep.DETAIL_ADVERSE_MEDIA_CUSTOMER.isNullOrEmpty()) {
            fivDetailOfAdverseMediaET?.setText("")
        }
    }

    fun init() {
        fivDetailOfAdverseMediaET =
            fivDetailOfAdverseMedia.getTextFromEditText(R.id.fivDetailOfAdverseMedia)
        foriegnPepSp = fivForeignPep.getSpinner(R.id.foreignPepID)
        domesticPepSp = fivDomesticPep.getSpinner(R.id.domesticPepID)
        fivPepCategoriesSP = fivPepCategories.getSpinner(R.id.fivPepCategories)
        fivPepSubCategoriesSP = fivPepSubCategories.getSpinner(R.id.fivPepSubCategories)
        analysisPep = fivAnalysisPep.getTextFromEditText(R.id.fivAnalysisPep)
    }

    fun loadDummyData() {
        fivDetailOfAdverseMediaET?.setText("All of Social Media")
        analysisPep?.setText("This is a analysis of pep")
    }

    fun load() {
        if ((activity as HawActivity).sharedPreferenceManager.lovPepCategories.isNullOrEmpty()) {
            callPepCategories()
        } else {
            setPepCategories((activity as HawActivity).sharedPreferenceManager.lovPepCategories)
        }
    }

    private fun setPepCategories(it: ArrayList<PepCategories>) {
        try{
        setTextLoad()
        fivPepCategories.setitemForPepCategories(it)
        (activity as HawActivity).pepCategoriesList = it
        (activity as HawActivity).sharedPreferenceManager.lovPepCategories = it
        (activity as HawActivity).globalClass?.findSpinnerPositionFromPepCateIndex(
            it,
            (activity as HawActivity).customerPep.PEP_CATG_DESC
        )
            .let {
                fivPepCategoriesSP?.setSelection(it!!)
            }
        fivPepCategories.remainSelection(it.size)
        if ((activity as HawActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()
        } else {
            setBool((activity as HawActivity).sharedPreferenceManager.lovBool)
        }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let {
                        setBool(it)
                    };

                }

                override fun BoolFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })

    }

    fun setBool(it: ArrayList<Bool>) {
        try{
        (activity as HawActivity).boolList = it
        (activity as HawActivity).sharedPreferenceManager.lovBool = it
        fivForeignPep.setItemForfivForeignPep(it)
        fivDomesticPep.setItemForfivDomesticPep(it)
        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
            it,
            (activity as HawActivity).customerPep.DOMESTIC_PEP
        ).let {
            domesticPepSp?.setSelection(it!!)
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
            it,
            (activity as HawActivity).customerPep.FOREIGN_PEP
        ).let {
            foriegnPepSp?.setSelection(it!!)
        }

        (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
}

    private fun setTextLoad() {
        (activity as HawActivity).customerPep.ANALYSIS_OF_PEP
            .let { analysisPep?.setText(it) }
        (activity as HawActivity).customerPep.DETAIL_ADVERSE_MEDIA_CUSTOMER
            .let { fivDetailOfAdverseMediaET?.setText(it) }
    }

    fun callPepCategories() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.PEP_CATEGORIES_IDENTIFIER
        HBLHRStore.instance?.getPepCategories(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : PepCategoriesCallBack {

                override fun PepCategoriesuccess(response: PepCategoriesResponse) {
                    response.data?.let {
                        setPepCategories(it)
                    };

                }

                override fun PepCategoriesFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callPepSubCategories(pepCategoryCode: String) {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var request = PepSubCategoriesRequest()
        request.identifier = Constants.PEP_SUB_CATEGORIES_IDENTIFIER
        request.find?.PEP_CATG_CODE = pepCategoryCode
        HBLHRStore.instance?.getPepSubCategories(
            RetrofitEnums.URL_HBL,
            request,
            object : PepSubCategoriesCallBack {
                override fun PepSubCategoriesSuccess(response: PepSubCategoriesResponse) {
                    response.data?.let {
                        fivPepSubCategories.setitemForPepSubCategories(it)
                        (activity as HawActivity).pepSubCategoriesList = it
                        (activity as HawActivity).customerPep.PEP_SUB_CATG_DESC.let { item ->
                            (activity as HawActivity).globalClass?.findSpinnerPositionFromPepSubCateIndex(
                                it,
                                item
                            ).let {
                                fivPepSubCategoriesSP?.setSelection(it!!)
                            }
                        }
                        fivPepSubCategories.remainSelection(it.size)
                    };
                    (activity as HawActivity).globalClass?.hideLoader()
                }

                override fun PepSubCategoriesFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                if (validation()) {
                    (activity as HawActivity).globalClass?.setDisbaled(btNext!!,false)
                    ( this.context as HawActivity).globalClass?.showDialog( this.context)
                    java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                        saveAndNext()
                        setAllModelsInAOFRequestModel()
                        activity?.runOnUiThread {
                            (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                        }
                    })
                }
            }
            R.id.btBack -> if (findNavController().currentDestination?.id == R.id.CIFStep1_13) {
                findNavController().navigate(R.id.action_CIFStep1_13_to_CIFStep1_12)
            }
        }

    }
    fun onBackPress(view:View){
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
              if (keyCode == KeyEvent.KEYCODE_BACK&&keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep1_13) {
                    findNavController().navigate(R.id.action_CIFStep1_13_to_CIFStep1_12)
                }
                 true
            }
            false
        }
    }
    private fun validation(): Boolean {
        var adverMedia =
            (activity as HawActivity).sharedPreferenceManager.customerPep.ADVERSE_MEDIA_CUSTOMER
        return when {
            analysisPep?.text.toString().isNullOrEmpty() -> {
                analysisPep?.error = resources!!.getString(R.string.pleaseEnterAnalysicPepErr)
                analysisPep?.requestFocus()
                false
            }
            fivPepCategoriesSP?.selectedItemPosition == 0 -> {
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString(R.string.pleaseSelectPepCatErr),
                    1
                )
                false
            }
            fivPepSubCategoriesSP?.selectedItemPosition == 0 -> {
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString(R.string.pleaseSelectPepSubCatErr),
                    1
                )
                false
            }
            adverMedia == "1" && fivDetailOfAdverseMediaET?.text.toString().isNullOrEmpty() -> {
                fivDetailOfAdverseMediaET?.error =
                    resources!!.getString(R.string.pleaseEnterDetailsOFAdversMediaErr)
                fivDetailOfAdverseMediaET?.requestFocus()
                false
            }
            foriegnPepSp?.selectedItemPosition == 0 && domesticPepSp?.selectedItemPosition == 0 -> {
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString(R.string.foriegnDomesticErr),
                    1
                )
                false
            }
            else -> {
                true
            }
        }
    }


    fun saveAndNext() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in HawActivity*/
        /*set the values from widgets and another models*/
        analysisPep?.text.toString()
            .let { (activity as HawActivity).customerPep.ANALYSIS_OF_PEP = it }
        fivDetailOfAdverseMediaET?.text.toString()
            .let { (activity as HawActivity).customerPep.DETAIL_ADVERSE_MEDIA_CUSTOMER = it }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as HawActivity).boolList, foriegnPepSp?.selectedItem.toString()
        ).let {
            (activity as HawActivity).customerPep.FOREIGN_PEP = it
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as HawActivity).boolList,
            domesticPepSp?.selectedItem.toString()
        ).let {
            (activity as HawActivity).customerPep.DOMESTIC_PEP = it
        }


        (activity as HawActivity).globalClass?.findSpinnerPositionFromPepCateCode(
            (activity as HawActivity).pepCategoriesList,
            fivPepCategoriesSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {
                (activity as HawActivity).customerPep.PEP_CATG_CODE = it
                (activity as HawActivity).customerPep.PEP_CATG_DESC =
                    fivPepCategoriesSP?.selectedItem.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromPepSubCateCode(
            (activity as HawActivity).pepSubCategoriesList,
            fivPepSubCategoriesSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {
                (activity as HawActivity).customerPep.PEP_SUB_CATG_CODE = it
                (activity as HawActivity).customerPep.PEP_SUB_CATG_DESC =
                    fivPepSubCategoriesSP?.selectedItem.toString()
            }
        }
        /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in HawActivity*/
        (activity as HawActivity).sharedPreferenceManager.customerPep =
            ((activity as HawActivity).customerPep)
        (activity as HawActivity).customerPepList.add(((activity as HawActivity).customerPep))
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as HawActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as HawActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as HawActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as HawActivity).aofAccountInfoRequest=it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    (activity as HawActivity).globalClass?.hideLoader()
                    if (findNavController().currentDestination?.id == R.id.CIFStep1_13) {
                        findNavController().navigate(R.id.action_CIFStep1_14_to_CIFStep2)
                    }
                    (activity as HawActivity).recyclerViewSetup()
                    (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }
    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            }catch (ex:Exception){
            }
        }
    }
    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity).unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
