package com.hbl.bot.ui.activities

import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity

import com.hbl.bot.R
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

class SplashActivity : AppCompatActivity() {
    var globalClass: GlobalClass? = null
    fun hideNavigationBar() {
        try {
            window.decorView.apply {
                // Hide both the navigation bar and the status bar.
                // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
                // a general rule, you should design your app to hide the status bar whenever you
                // hide the navigation bar.
                systemUiVisibility =
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(this,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this), (e as Exception))
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        hideNavigationBar()
        setContentView(R.layout.fragment_splash)
        globalClass = this.applicationContext as GlobalClass
        globalClass?.clearAllLovs()
        globalClass?.clearSubmitAOF()
        init()
    }

    private fun init() {

        Handler().postDelayed({
            var intent = Intent(this, NavigationDrawerActivity::class.java)
            startActivity(intent)
            finish()
        }, 1500)
    }

}