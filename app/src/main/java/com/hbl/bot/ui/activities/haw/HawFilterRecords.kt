package com.hbl.bot.ui.activities

import android.content.ActivityNotFoundException
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.hbl.bot.R
import com.hbl.bot.utils.*
import kotlinx.android.synthetic.main.activity_filter_records.*
import java.io.FileNotFoundException
import java.io.IOException

class HawFilterRecords : AppCompatActivity(), View.OnClickListener {


    val globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
    override fun onResume() {
        super.onResume()
//        hideNavigationBar()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
            Thread.setDefaultUncaughtExceptionHandler(LoggingExceptionHandler(globalClass));
            setContentView(R.layout.activity_filter_records)
//        hideNavigationBar()
            backBtn.setOnClickListener(this)
            submit.setOnClickListener(View.OnClickListener {
                try {
                    val trakingid = edTrackingId.text.toString()
                    GlobalClass.isfromFilter = true
                    val docid = edIdDocNo.text.toString()
                    val intent = Intent(this, HawShowRecords::class.java)
                    if (!docid.isNullOrEmpty() && !trakingid.isNullOrEmpty()) {
                        intent.putExtra(Constants.FILTER_TRACKING_ID, trakingid)
                        intent.putExtra(Constants.FILTER_DOCUMENT_ID, docid)
                        startActivity(intent)
                        finish()
                    } else if (!docid.isNullOrEmpty()) {
                        intent.putExtra(Constants.FILTER_DOCUMENT_ID, docid)
                        startActivity(intent)
                        finish()
                    } else if (!trakingid.isNullOrEmpty()) {
                        intent.putExtra(Constants.FILTER_TRACKING_ID, trakingid)
                        startActivity(intent)
                        finish()
                    } else {
                        ToastUtils.normalShowToast(this,
                            "Please enter TRACKING ID or DOCUMENT NUMBER",
                            1)
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(this@HawFilterRecords,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((this@HawFilterRecords), (e as Exception))
                }
            })
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as Exception))
        }
    }

    fun hideNavigationBar() {
        try {
            window.decorView.apply {
                // Hide both the navigation bar and the status bar.
                // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
                // a general rule, you should design your app to hide the status bar whenever you
                // hide the navigation bar.
                systemUiVisibility =
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(this@HawFilterRecords,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@HawFilterRecords), (e as Exception))
        }
    }

    override fun onBackPressed() {
        finish()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.backBtn -> {
                finish()
            }
        }
    }
}
