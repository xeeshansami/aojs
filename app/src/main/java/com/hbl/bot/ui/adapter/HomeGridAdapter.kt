package com.hbl.bot.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.hbl.bot.model.HomeGridModel
import com.hbl.bot.R
import kotlinx.android.synthetic.main.row_home_grid.view.*

class HomeGridAdapter(private val mList: MutableList<HomeGridModel>) :
    RecyclerView.Adapter<HomeGridAdapter.MyViewHolder>() {

    open inner class MyViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvGridTitle = view.tvGridTitle!!
        val imgGrid = view.imgGrid!!
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layout = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_home_grid, parent, false)
        return MyViewHolder(layout)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val list = mList[position]
        holder.tvGridTitle.text = list.title
        holder.imgGrid.setBackgroundResource(list.img!!)
    }

}