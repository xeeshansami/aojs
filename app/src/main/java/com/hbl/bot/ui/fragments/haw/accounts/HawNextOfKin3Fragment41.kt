package com.hbl.bot.ui.fragments.accounts


import android.annotation.SuppressLint

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.AofAccountInfoRequest
import com.hbl.bot.network.models.request.base.CIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep12_2.*
import kotlinx.android.synthetic.main.fragment_cifstep12_2.btBack
import kotlinx.android.synthetic.main.fragment_cifstep12_2.btNext
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import android.content.*
import android.os.Build
import androidx.annotation.RequiresApi
import kotlinx.android.synthetic.main.view_form_bool_selection.view.*
import kotlinx.android.synthetic.main.view_form_input.view.*
import java.lang.ClassCastException
import java.util.concurrent.Executors


/**
 * A simple [Fragment] subclass.
 */
class HawNextOfKin3Fragment41 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var riskStatusET: EditText? = null
    var fivIdDocumentNoET: EditText? = null
    var fivIdDocumentTypeSP: Spinner? = null
    var fivMandateNameET: EditText? = null
    var fivCifNoET: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep12_2, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(3)
        viewModel.currentFragmentIndex.setValue(2)
        val txt = resources.getString(R.string.next_of_kin)
        val txt1 = " (3/3 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            header()
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            btBack.setOnClickListener(this)
            submit_btn.setOnClickListener(this)
            fetch_btn.setOnClickListener(this)
            back_to_btn.setOnClickListener(this)
            risk_btn.setOnClickListener(this)
            init()
            load()
            callDocs()

            /* if (GlobalClass.isDummyDataTrue) {
                 loadDummyData()
             }*/
            setConditions()
            setLengthAndType()
            onBackPress(view)
//            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    fun load() {

        (activity as HawActivity).customerNextOfKin.ID_DOCUMENT_NO.let { t ->
            fivIdDocumentNoET?.setText(
                t
            )
        }
        (activity as HawActivity).customerNextOfKin.NAME_OF_MANDATE_HOLDER.let { t ->
            fivMandateNameET?.setText(
                t
            )
        }
        (activity as HawActivity).customerNextOfKin.CIF_NO.let { t ->
            fivCifNoET?.setText(
                t
            )
        }
//         (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.docTypeList.let {
//             fivIdDocument.setItemForDocs(it)

//         }
    }

    private fun setLengthAndType() {
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivIdDocumentNoET!!,
            13,
            Constants.INPUT_TYPE_NUMBER
        )
    }

    private fun callDocs() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.HAW_DOCS_IDENTIFIER
        HBLHRStore.instance?.getDocs(RetrofitEnums.URL_HBL, lovRequest, object : DocsCallBack {
            override fun DocsSuccess(response: DocsResponse) {
                (activity as HawActivity).globalClass?.hideLoader()
                response.data?.let {
                    (activity as HawActivity).docTypeList = it
                    fivIdDocument.setItemForDocs(it)
                }
            }

            override fun DocsFailure(response: BaseResponse) {
//                Utils.failedAwokeCalls((activity as HawActivity)) { callDocs() }
                ToastUtils.normalShowToast(activity, response.message, 1)
                (activity as HawActivity).globalClass?.hideLoader()
            }
        })
    }

    private fun setConditions() {
        (activity as HawActivity).globalClass?.setDisbaled(fivCifNoET!!,false)
        (activity as HawActivity).globalClass?.setDisbaled(riskStatusET!!,false)
        (activity as HawActivity).globalClass?.setDisbaled(fivIdDocumentTypeSP!!, false)
        (activity as HawActivity).globalClass?.setDisbaled(fivMandateNameET!!, false)
        fivIdDocumentNoET!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            @RequiresApi(Build.VERSION_CODES.O)
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (fivIdDocumentNoET?.text.toString()==(activity as HawActivity).customerInfo.ID_DOCUMENT_NO) {
                        fivIdDocumentNoET?.focusable
                        fivIdDocumentNoET?.error = "Customer next of kin Document Number is same as Account Document Id"
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }
        })
    }

    fun init() {
        (activity as HawActivity).globalClass?.setEnabled(submit_btn!!, true)
        fivIdDocumentTypeSP = fivIdDocument.getSpinner(R.id.documentTypeID)
        fivMandateNameET = fivMandateName.getTextFromEditText(R.id.fivMandateNameID)
        fivCifNoET = fivCifNo.getTextFromEditText(R.id.fivCifNo)
        riskStatusET = fivCifRating.getTextFromEditText(R.id.riskStatus)
        fivIdDocumentNoET = fivIdDocumentNo.getTextFromEditText(R.id.fivIdDocumentNo)
        if ((activity as HawActivity).customerNextOfKin.POWDER_OF_ATTORNEY == "Yes") {
            fivIdDocumentNo.mandateInput.text="*"
            fivCifNo.mandateInput.text="*"
            fivCifRating.mandateInput.text="*"
            fivIdDocument.mandateSpinner.text="*"
            fivMandateName.mandateInput.text="*"
            (activity as HawActivity).globalClass?.setEnabled(fetch_btn!!,true)
            (activity as HawActivity).globalClass?.setEnabled(risk_btn!!,true)
            (activity as HawActivity).globalClass?.setEnabled(fivIdDocumentNoET!!, true)
        } else {
            fivIdDocumentNo.mandateInput.text=""
            fivIdDocument.mandateSpinner.text=""
            fivCifNo.mandateInput.text=""
            fivCifRating.mandateInput.text=""
            fivMandateName.mandateInput.text=""
            (activity as HawActivity).globalClass?.setDisbaled(fetch_btn!!,false)
            (activity as HawActivity).globalClass?.setDisbaled(risk_btn!!,false)
            (activity as HawActivity).globalClass?.setDisbaled(fivMandateNameET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivIdDocumentNoET!!, false)

        }
        submit_btn.visibility = View.VISIBLE
    }

    private fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
            (activity as HawActivity).globalClass?.setDisbaled(fivIdDocumentNoET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivIdDocumentTypeSP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivMandateNameET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivCifNoET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(riskStatusET!!, false)
        }
    }

    fun loadDummyData() {
//        fivIdDocumentNoET?.setText("6549873216546")
//        fivMandateNameET?.setText("Mohammad Zeeshan")

    }

    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("NewApi")
    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.submit_btn -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        Executors.newSingleThreadExecutor().execute(Runnable {
                            send()
                            (activity as HawActivity).runOnUiThread {
                                (activity as HawActivity).globalClass?.setEnabled(
                                    btNext!!,
                                    true
                                )
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }

            }
            R.id.risk_btn -> {
                try {
                    callToRiskRatingResponse()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
            R.id.fetch_btn -> {
                try {
                    if (TextUtils.isEmpty(fivIdDocumentNoET?.text.toString())) {
                        ToastUtils.normalShowToast(activity, "Please enter document number", 1)
                    }  else {
                        (activity as HawActivity).globalClass?.showDialog(activity)
                        callKinCif()
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep12_2)
                    findNavController().navigate(R.id.action_CIFStep12_2_to_CIFStep11)
            R.id.back_to_btn ->
                if (findNavController().currentDestination?.id == R.id.CIFStep12_2)
                    findNavController().navigate(R.id.action_CIFStep12_2_to_CIFStep6)
        }
    }

    @SuppressLint("NewApi")
    @RequiresApi(Build.VERSION_CODES.O)
    private fun validation(): Boolean {
        return if ((activity as HawActivity).sharedPreferenceManager.customerNextOfKin.POWDER_OF_ATTORNEY == "Yes") {
            if (TextUtils.isEmpty(fivMandateNameET?.text.toString()) || TextUtils.isEmpty(fivCifNoET?.text.toString()) || fivIdDocumentTypeSP?.selectedItemPosition==0) {
                ToastUtils.normalShowToast(activity, "Please fetch the cif first.", 1)
                false
            } else if (TextUtils.isEmpty(riskStatusET?.text.toString())) {
                ToastUtils.normalShowToast(activity, "Please check risk rating first", 1)
                false
            } else  if (fivIdDocumentNoET?.text.toString()==(activity as HawActivity).customerInfo.ID_DOCUMENT_NO) {
                fivIdDocumentNoET?.focusable
                fivIdDocumentNoET?.error = "Customer next of kin Document Number is same as Account Document Id"
                false
            } else {
                true
            }
        } else if ((activity as HawActivity).sharedPreferenceManager.customerNextOfKin.POWDER_OF_ATTORNEY == "No") {
            true
        } else {
            true
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep12_2)
                    findNavController().navigate(R.id.action_CIFStep12_2_to_CIFStep6)
            }
            true
        }
        false
    }

    private fun callKinCif() {
        var cifRequest = CIFRequest()
        cifRequest.payload = Constants.PAYLOAD
        cifRequest.scnic = fivIdDocumentNoET?.text.toString()
        HBLHRStore.instance?.getCIF(RetrofitEnums.URL_HBL, cifRequest, object : CIFCallBack {
            override fun CIFSuccess(response: AofAccountInfoResponse) {
                try {
                    if (response.status == "00") {
                        fivMandateNameET?.setText(response.data.get(0).NAME)
                        fivCifNoET?.setText(response.data.get(0).CUST_INFO.get(0).CIF_NO.toString())
                        ToastUtils.normalShowToast(
                            activity,
                            "Risk Rating is"+response.data.get(0).RISK_RATING.toString(),
                            2
                        )
                        fivIdDocumentTypeSP?.setSelection(1)
                    }
                    (activity as HawActivity).globalClass?.hideLoader()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }

            }

            override fun CIFFailure(response: BaseResponse) {
                ToastUtils.normalShowToast(activity, response.message, 1)
//                Utils.failedAwokeCalls((activity as HawActivity)) { callKinCif() }
                (activity as HawActivity).globalClass?.hideLoader()

            }
        })


    }

    private fun callToRiskRatingResponse() {
        var aofAccountInfoRequest = AofAccountInfoRequest()
        aofAccountInfoRequest.identifier = Constants.AOF_SUBMIT_IDENTIFIER
        aofAccountInfoRequest.data = (activity as HawActivity).sharedPreferenceManager.aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(aofAccountInfoRequest)
        HBLHRStore.instance?.postRiskRating(
            RetrofitEnums.URL_HBL,
            aofAccountInfoRequest, object : CustomerRiskRatingCallBack {
                @SuppressLint("WrongConstant")
                override fun CustomerRiskRatingSuccess(response: RiskRatingResponse) {
                    try {
                        ToastUtils.normalShowToast(activity, response.message, 2)
                        response.data?.get(0)?.let {
                            (activity as HawActivity).riskRating = it?.RISK_RATING!!
                            Executors.newSingleThreadExecutor().execute(Runnable {
                                (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                                    it.RISK_RATING.toString()
                                (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                                    it.RISK_RATING_TOTAL.toString()!!
                                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo =
                                    (activity as HawActivity).aofAccountInfoRequest
                                activity?.runOnUiThread {
                                    riskCheck(it)
                                }
                            })
                        }
                        response.data?.get(0)?.RISK_RATING_TOTAL?.let {
                            (activity as HawActivity).riskRatingTotal = it.toString()
                        }
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun CustomerRiskRatingFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) { callToRiskRatingResponse() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun riskCheck(risk: RiskRating) {
        riskStatusET?.isEnabled = false
        if (risk.RISK_RATING.equals("Medium", true)) {
            riskStatusET?.setText("Medium")
            riskStatusET?.setTextColor(Color.parseColor("#008577"));
        } else {
            riskStatusET?.setText("High")
            riskStatusET?.setTextColor(Color.parseColor("#D53F45"));
        }
    }

    fun send() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in HawActivity*/
        /*set the values from widgets and another models*/
        saveAndNext()
        setFinalAllModelsInAOFRequestModel()
        var testModel = Gson().toJson((activity as HawActivity).aofAccountInfoRequest)
    }

    fun saveAndNext() {
        fivIdDocumentNoET?.text.toString()
            .let { (activity as HawActivity).customerNextOfKin.ID_DOCUMENT_NO = it }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeCode(
            (activity as HawActivity).docTypeList,
            fivIdDocumentTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerNextOfKin.ID_DOCUMENT_TYPE = it.toString()
                (activity as HawActivity).customerNextOfKin.ID_DOCUMENT_TYPE_DESC =
                    fivIdDocumentTypeSP?.selectedItem.toString()
            }
        }
        fivMandateNameET?.text.toString()
            .let { (activity as HawActivity).customerNextOfKin.NAME_OF_MANDATE_HOLDER = it }
        fivCifNoET?.text.toString()
            .let { (activity as HawActivity).customerNextOfKin.CIF_NO = it }

        if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
            (activity as HawActivity).customerAccounts.ACCT_SNG_JNT = "3"
            (activity as HawActivity).customerAccounts.ACCT_SNG_JNT_DESC = "Minor Account"
        } else {
            (activity as HawActivity).customerAccounts.ACCT_SNG_JNT = "1"
            (activity as HawActivity).customerAccounts.ACCT_SNG_JNT_DESC =
                "Individual Account"
        }
        (activity as HawActivity).customerAccounts.BRANCHNAME =
            GlobalClass.sharedPreferenceManager!!.getLoginData()
                .getBRANCHNAME().toString()

        (activity as HawActivity).customerAccounts.BRANCH_CODE =
            GlobalClass.sharedPreferenceManager!!.getLoginData()
                .getBRANCHCODE().toString()
        (activity as HawActivity).customerAccounts.ID_DOC_NO =
            (activity as HawActivity).sharedPreferenceManager.getStringFromSharedPreferences(
                Constants.ACCOUNT_CNIC
            )
        (activity as HawActivity).sharedPreferenceManager.customerAccount =
            (activity as HawActivity).customerAccounts
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin =
            (activity as HawActivity).customerNextOfKin
    }

    fun setFinalAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }

        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofFinalDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofFinalDataAlign(aofAccountInfo: Data) {
//        aofAccountInfoRequest.data?.TRACKING_ID =
//            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID
        aofAccountInfo.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as HawActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as HawActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as HawActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .isNullOrEmpty()
        ) {
            aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as HawActivity).aofAccountInfoRequest = it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postFinalGenerateCIF(aofAccountInfo)
    }

    fun postFinalGenerateCIF(aofAccountInfo: Data) {
        var aofAccountInfoRequest = AofAccountInfoRequest()
        aofAccountInfoRequest.identifier = Constants.FINAL_DRAFT_IDENTIFIER
        aofAccountInfoRequest.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(aofAccountInfoRequest)
        LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(
            mStatusCodeResponse, IntentFilter(
                Constants.STATUS_BROADCAST
            )
        );
        HBLHRStore.instance?.postAofaccountInfo(
            RetrofitEnums.URL_HBL,
            aofAccountInfoRequest, object : CustomerAofAccountInfoCallBack {
                @SuppressLint("WrongConstant")
                override fun CustomerAofAccountInfoSuccess(response: AllSubmitResponse) {
                    try {

                        Handler().postDelayed({
                            response.message?.let { msg ->
                                updateAccountAndCif(
                                    msg, response.data!![0].CIF_NO!!,
                                    response.data!![0].ACCT_NO!!,
                                    response.data!![0].IBAN!!
                                )
                            }
                        }, 500)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun CustomerAofAccountInfoFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun updateAccountAndCif(msg: String, cifNo: String, accNo: String, iBanNo: String) {
        try {
            (activity as HawActivity).customerInfo.CIF_NO = cifNo
            (activity as HawActivity).customerAccounts.CIF_NO = cifNo
            (activity as HawActivity).customerAccounts.ACCT_NO = accNo
            (activity as HawActivity).customerAccounts.IBAN = iBanNo
            (activity as HawActivity).customerAccounts.let {
                (activity as HawActivity).sharedPreferenceManager.customerAccount = it
            }
            (activity as HawActivity).customerInfo.let {
                (activity as HawActivity).sharedPreferenceManager.customerInfo = it
            }
            (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
                if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                    (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                        0,
                        it
                    )
                } else {
                    (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                        it
                    )
                }
            }
            (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
                if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                    (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                        0,
                        it
                    )
                } else {
                    (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                        it
                    )
                }
            }
            (activity as HawActivity).aofAccountInfoRequest.let {
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
            }
            (activity as HawActivity).globalClass?.hideLoader()
            confirmCompletedUpload(msg)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(submit_btn, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }

    private fun confirmCompletedUpload(msg: String) {
        val builder = AlertDialog.Builder((activity as HawActivity))
        builder.setTitle(msg)
        builder.setCancelable(false)
        builder.setMessage((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
            "Your tracking Number is $it"
        })
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            if (findNavController().currentDestination?.id == R.id.CIFStep10)
                findNavController().navigate(R.id.action_CIFStep12_2_to_CIFStep16)
            (activity as HawActivity).recyclerViewSetup()
            (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
        }
        builder.show()
    }
}
