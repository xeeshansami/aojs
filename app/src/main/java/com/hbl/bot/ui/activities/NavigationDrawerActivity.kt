package com.hbl.bot.ui.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.text.format.DateFormat
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.Toolbar
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.AppStatusCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.AppDetailsRequest
import com.hbl.bot.network.models.response.base.AppStatusResponse
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.baseRM.DocsData
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.services.Networkservice
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.app_bar_navigation_drawer.ivAvatar
import kotlinx.android.synthetic.main.app_bar_navigation_drawer.nextWorkSpeed
import java.io.*
import java.lang.ClassCastException
import java.util.*
import kotlin.collections.ArrayList


class NavigationDrawerActivity : AppCompatActivity() {
    private lateinit var appBarConfiguration: AppBarConfiguration

    @JvmField
    var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()
    var intentFilter = IntentFilter()
    var permissions = arrayOf(
        Manifest.permission.ACCESS_NETWORK_STATE,
        Manifest.permission.CALL_PHONE,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.INTERNET,
        Manifest.permission.CHANGE_WIFI_STATE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    @JvmField
    var globalClass: GlobalClass? = null
    public var ivRefresh: AppCompatImageView? = null
    public var ivHome: AppCompatImageView? = null
    public var ivSSSendEmal: AppCompatImageView? = null

    fun hideNavigationBar() {
        try {
            window.decorView.apply {
                // Hide both the navigation bar and the status bar.
                // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
                // a general rule, you should design your app to hide the status bar whenever you
                // hide the navigation bar.
                systemUiVisibility =
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as Exception))
        }
    }

    @JvmField
    var docTypeList: ArrayList<DocsData> = ArrayList<DocsData>()
    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
            hideNavigationBar()
            setContentView(R.layout.activity_navigation_drawer)
            init()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as Exception))
        }

    }

    @SuppressLint("RestrictedApi")
    private fun init() {
        sharedPreferenceManager.getInstance(this)
        globalClass = this.applicationContext as GlobalClass
        Thread.setDefaultUncaughtExceptionHandler(LoggingExceptionHandler(globalClass as GlobalClass));
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        ivRefresh = findViewById(R.id.ivRefresh)
        ivHome = findViewById(R.id.ivHome)
        ivSSSendEmal = findViewById(R.id.ivSSSendEmal)
        ivAvatar!!.setOnClickListener {
            // Check for current fragment
            about()
        }
        ivSSSendEmal!!.setOnClickListener {
            // Check for current fragment
            globalClass?.showDialog(this)
            permission()
        }
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home
            )
        )
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        setupActionBarWithNavController(navController, appBarConfiguration)
        updateAppData()
        getNetworkSpeed()
//        switchFragment(R.id.nav_home)
    }

    fun getNetworkSpeed() {
        if (Networkservice.isOnline(applicationContext)) {
            startService()
        } else Toast.makeText(applicationContext,
            "Please check your network connection",
            Toast.LENGTH_SHORT).show()
    }
    private fun switchFragment(startDestId: Int) {
//        val fragmentContainer = view?.findViewById<View>(R.id.nav_host)
//        val navController = Navigation.findNavController(fragmentContainer!!)
            val navController = findNavController(R.id.nav_host_fragment)
            val inflater = navController.navInflater
            val graph = navController.graph
            graph.startDestination = startDestId
            navController.graph = graph
    }
    fun startService() {
        intentFilter.addAction(Constants.BROADCAST)
        val serviceIntent = Intent(this, Networkservice::class.java)
        startService(serviceIntent)
    }

    private fun updateAppData() {
        packageManager.getPackageInfo(packageName,
            0).versionName.let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.VERSION_NAME,
                it)
        }
        packageManager.getPackageInfo(packageName,
            0).versionCode.let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.VERSION_CODE,
                it.toString())
        }
        GlobalClass.deviceID().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.TABLET_ID,
                it)
        }
        GlobalClass.getFolderSizeLabel(GlobalClass.getFileAPK()).let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.TABLET_APP_SIZE,
                it)
        }
        GlobalClass.firstModifiedDate().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.APP_CREATED_DATE,
                it)
        }
        GlobalClass.lastModifiedDate().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.APP_UPDATE_DATE,
                it)
        }
        sharedPreferenceManager.loginData.getUSERID().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.MYSISID,
                it)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    @SuppressLint("WrongConstant")
    private fun about() {
        try {
            var dialogBuilder = AlertDialog.Builder(this)
            val layoutView: View = layoutInflater.inflate(R.layout.about_app_popup, null)
            dialogBuilder.setView(layoutView)
            var alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()
            var shareBtn = layoutView.findViewById<ImageView>(R.id.shareBtn)
            var ok_action = layoutView.findViewById<Button>(R.id.ok_action)
            var toolbar = layoutView.findViewById<Toolbar>(R.id.toolbar)
            toolbar.title = "About"
            var appname = layoutView.findViewById<TextView>(R.id.appname)
            appname.text = resources.getString(R.string.app_name)
            var UserID = layoutView.findViewById<TextView>(R.id.UserID)
            UserID.text = sharedPreferenceManager.loginData.getUSERID()
            var androidVersion = layoutView.findViewById<TextView>(R.id.androidVersion)
            var applicationID = layoutView.findViewById<TextView>(R.id.applicationID)
            var version = layoutView.findViewById<TextView>(R.id.version)
            var versionCod = layoutView.findViewById<TextView>(R.id.versionCode)
            var apkSize = layoutView.findViewById<TextView>(R.id.apkSize)
            var createDateOfFile = layoutView.findViewById<TextView>(R.id.createDateOfFile)
            var updaDateOfFile = layoutView.findViewById<TextView>(R.id.updaDateOfFile)
            var packageNameText = layoutView.findViewById<TextView>(R.id.packageName)
            packageNameText.text = packageManager.getPackageInfo(packageName, 0).packageName
            try {
                val versionName =
                    (this@NavigationDrawerActivity).packageManager.getPackageInfo(
                        packageName,
                        0
                    ).versionName
                val versionCode = packageManager.getPackageInfo(
                        packageName,
                        0
                    ).versionCode
                version.text = "$versionName"
                versionCod.text = "$versionCode"
                applicationID.text = "${GlobalClass.deviceID()}"
                androidVersion.text = "${globalClass!!.getAndroidVersion(android.os.Build.VERSION.SDK_INT)}"
            } catch (e: PackageManager.NameNotFoundException) {
                e.printStackTrace()
            }
            if (GlobalClass.getFileAPK().exists()) {
                apkSize.text = GlobalClass.getFolderSizeLabel(GlobalClass.getFileAPK())
                createDateOfFile.text = GlobalClass.firstModifiedDate()
                updaDateOfFile.text = GlobalClass.lastModifiedDate()
            } else {
                apkSize.text = "N/A"
                createDateOfFile.text = "N/A"
                updaDateOfFile.text = "N/A"
            }
            ok_action.setOnClickListener(View.OnClickListener {
                alertDialog.dismiss()
            })
            shareBtn.setOnClickListener(View.OnClickListener {
                globalClass?.showDialog(this)
                var request = AppDetailsRequest()
                request.identifier = Constants.APP_DETAILS_IDENTIFIER
                request.apkSize = apkSize.text.toString()
                request.applicationID = applicationID.text.toString()
                request.applicationName = appname.text.toString()
                request.createdApkDate = createDateOfFile.text.toString()
                request.updateApkDate = updaDateOfFile.text.toString()
                request.userID = sharedPreferenceManager.loginData.getUSERID()
                request.versionCode = versionCod.text.toString()
                request.versionName = version.text.toString()
                request.IMEI = "${GlobalClass.getIMEIDeviceId(this@NavigationDrawerActivity)}"
                request.packageName =(this@NavigationDrawerActivity).packageManager.getPackageInfo(
                    (this@NavigationDrawerActivity).packageName,
                    0
                ).packageName
                request.androidApiVersion = globalClass!!.getAndroidVersion(android.os.Build.VERSION.SDK_INT).toString()
                HBLHRStore?.instance?.sendAppDetails(RetrofitEnums.URL_HBL, request, object :
                    AppStatusCallBack {
                    override fun AppStatusSuccess(response: AppStatusResponse) {
                        try {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                response.message,
                                2)
                            alertDialog.dismiss()
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as UnsatisfiedLinkError))
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as NullPointerException))
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as IllegalArgumentException))
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as NumberFormatException))
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as InterruptedException))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as RuntimeException))
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as FileNotFoundException))
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as ClassNotFoundException))
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as ActivityNotFoundException))
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as IndexOutOfBoundsException))
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as ArrayIndexOutOfBoundsException))
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as ClassCastException))
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as TypeCastException))
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as SecurityException))
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as IllegalStateException))
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as OutOfMemoryError))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity),
                                (e as RuntimeException))
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                                getString(R.string.something_went_wrong),
                                3)
                            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as Exception))
                        }
                        this@NavigationDrawerActivity.globalClass?.hideLoader()
                    }

                    override fun AppStatusFailure(response: BaseResponse) {
                        Log.d("text", response.toString())
                        this@NavigationDrawerActivity.globalClass?.hideLoader()
                    }
                })
            })
            alertDialog.show()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as Exception))
        }
    }


    protected fun screenshot(view: View, filename: String): File? {
        val date = Date()

        // Here we are initialising the format of our image name
        val format = DateFormat.format("yyyy-MM-dd_hh:mm:ss", date)
        try {
            // Initialising the directory of storage
            val dirpath: String = Environment.getExternalStorageDirectory().toString() + ""
            val file = File(dirpath)
            if (!file.exists()) {
                val mkdir = file.mkdir()
            }

            // File name
            val path = "$dirpath/$filename-$format.jpeg"
            view.isDrawingCacheEnabled = true
            val bitmap: Bitmap = Bitmap.createBitmap(view.drawingCache)
            view.isDrawingCacheEnabled = false
            val imageurl = File(path)
            val outputStream = FileOutputStream(imageurl)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 50, outputStream)
            outputStream.flush()
            outputStream.close()
            globalClass?.hideLoader()
            viewAndSendScreenshortAlert(imageurl)
            return imageurl;
        } catch (io: FileNotFoundException) {
            io.printStackTrace()
        } catch (e: IOException) {
            e.printStackTrace()
        }
        globalClass?.hideLoader()
        return null
    }

    fun viewAndSendScreenshortAlert(file: File) {
        try {
            var dialogBuilder = AlertDialog.Builder(this@NavigationDrawerActivity)
            val layoutView: View = layoutInflater.inflate(R.layout.snap_popup, null)
            dialogBuilder.setView(layoutView)
            var alertDialog = dialogBuilder.create()
            alertDialog.show()
            alertDialog.setCancelable(false)
            var subjectBody = layoutView.findViewById<EditText>(R.id.subjectBody)
            var messageBody = layoutView.findViewById<EditText>(R.id.messageBody)
            var snapImage = layoutView.findViewById<ImageView>(R.id.snapImage)
            var sendAction = layoutView.findViewById<Button>(R.id.sendAction)
            snapImage.setImageURI(Uri.parse(file.path))
            var cancel_action = layoutView.findViewById<Button>(R.id.cancel_action)
            sendAction.setOnClickListener(View.OnClickListener {
                if (!subjectBody.text.toString().trim().isNullOrEmpty()) {
                    globalClass?.showDialog(this)
                    if (file.exists() && file.length() > 0) {
                        val bm = BitmapFactory.decodeFile(file.path)
                        val bOut = ByteArrayOutputStream()
                        bm.compress(Bitmap.CompressFormat.JPEG, 100, bOut)
                    }
                    alertDialog.dismiss()
                    SendEmail.sendAttachmentEmail(this,
                        file,
                        subjectBody.text.toString().trim(),
                        messageBody.text.toString().trim(),
                        this.javaClass.simpleName)
                } else {
                    ToastUtils.normalShowToast(this, "Please insert a subject line", 3)
                }
            })
            cancel_action.setOnClickListener(View.OnClickListener {
                GlobalClass.delFile(file.path)
                globalClass?.hideLoader()
                alertDialog.dismiss()
            })
            alertDialog.show()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                getString(R.string.something_went_wrong),
                3)
            SendEmail.sendEmail((this@NavigationDrawerActivity), (e as Exception))
        }
    }

    var broadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            if (intent.action.equals(Constants.BROADCAST)) {
                if (intent.getStringExtra("online_status").equals("true")) {
                    nextWorkSpeed.setText(intent.getStringExtra("networkSpeed") + "\nkB\\s")
//                    Log.d("data", "true")
                } else {
//                    Toast.makeText(applicationContext, "false", Toast.LENGTH_SHORT).show()
//                    Log.d("data", "false")
                }
            }
        }
    }


    override fun onStart() {
        super.onStart()
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onPause() {
        super.onPause()
        unregisterReceiver(broadcastReceiver)
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(broadcastReceiver, intentFilter)
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this)
            .unregisterReceiver(broadcastReceiver)
        super.onDestroy()
    }

    fun permission() {
        Permissions.check(
            this@NavigationDrawerActivity,
            permissions,
            null,
            null,
            object : PermissionHandler() {
                override fun onGranted() {
                    try {// do your task.
                        screenshot(window.decorView.rootView, "snap");
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail(this@NavigationDrawerActivity,
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail(this@NavigationDrawerActivity as NavigationDrawerActivity,
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail(this@NavigationDrawerActivity,
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail(this@NavigationDrawerActivity as NavigationDrawerActivity,
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity),
                            (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(this@NavigationDrawerActivity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((this@NavigationDrawerActivity), (e as Exception))
                    }
                }

                @SuppressLint("WrongConstant")
                override fun onDenied(
                    context: Context,
                    deniedPermissions: java.util.ArrayList<String>,
                ) {
                    ToastUtils.normalShowToast(
                        this@NavigationDrawerActivity,
                        "Permission denied, please enabled the permissions from setting", 1)
                }
            })
    }
}
