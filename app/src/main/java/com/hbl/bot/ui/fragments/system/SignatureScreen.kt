package com.hbl.bot.ui.fragments.system

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.graphics.*
import android.os.*
import android.util.Base64
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.github.gcacace.signaturepad.views.SignaturePad
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.ui.customviews.Dialog2Callback
import com.hbl.bot.ui.customviews.DialogCallback
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import com.hbl.bot.viewModels.SharedCIFViewModel
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_signature_screen.*
import kotlinx.android.synthetic.main.fragment_signature_screen.btClearSignature
import kotlinx.android.synthetic.main.fragment_signature_screen.signaturePad
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

class SignatureScreen : Fragment(), View.OnClickListener {
    var globalClass: GlobalClass = GlobalClass()
    val viewModel: SharedCIFViewModel by activityViewModels()
    var root: File? = null
    var myView: View? = null
    var permissions = arrayOf(
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )
    var count = 1
    var bitmapList = ArrayList<String>()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_signature_screen, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    override fun onResume() {
        super.onResume()
        viewModel.showAvatar.value = false
    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.nextBtn -> {
                try {
                    uploadSignatureAlert(
                        "ALERT!",
                        "Are you sure this is your final signature",
                        "YES",
                        "NO"
                    )
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as CIFRootActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as CIFRootActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as CIFRootActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as CIFRootActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as CIFRootActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            R.id.btClearSignature -> {
                signaturePad.clear()
            }
        }
        viewModel.indicator.value = false
    }

    private fun uploadSignatureAlert(
        title: String,
        msg: String,
        button1: String,
        button2: String,
    ) {
        (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(requireContext(),
            title,
            msg,
            button1,
            button2,
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
                }

                override fun onPositiveClicked() {
                    try {
                        super.onPositiveClicked()
//                        if (android.os.Build.VERSION.SDK_INT <= android.os.Build.VERSION_CODES.Q) {
                            var bitmap = signaturePad.getTransparentSignatureBitmap(true)
                            if (bitmap != null) {
                                if (count != 0 && count > 0 && (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
                                    count--
                                    bitmapList.add(
                                        convertToBase64(
                                            signaturePad.getTransparentSignatureBitmap(
                                                true
                                            )
                                        )
                                    )
                                    continueFlow(
                                        "Child Signature Required!",
                                        "In Minor/Guardian case Please SIGNATURE here of your child",
                                        "Continue", false, false
                                    )
                                } else {
                                    bitmapList.add(
                                        convertToBase64(
                                            signaturePad.getTransparentSignatureBitmap(
                                                true
                                            )
                                        )
                                    )
                                    continueFlow(
                                        "Congratulations!!!",
                                        "You have applied signatured successfully",
                                        "Continue", true, false
                                    )
                                }
                            } else {
                                ToastUtils.normalShowToast(
                                    activity,
                                    "Please write the signature first",
                                    1
                                )
                            }
//                        } else {
//                            continueFlow(
//                                "Alert!!!",
//                                "Your ANDROID DEVICE VERSION is not compatible please coordinate with administrator and try again, Thanks",
//                                "Ok", true, true
//                            )
//                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }
            })
    }


    private fun continueFlow(
        title: String,
        msg: String,
        button1: String,
        isCountinue: Boolean,
        isError: Boolean
    ) {
        if (isError) {
            (GlobalClass.applicationContext as GlobalClass).showSingleBtnConfirmationDialog(
                requireContext(),
                title,
                msg,
                button1,
                object : Dialog2Callback, com.hbl.bot.utils.DialogCallback {
                    override fun onNegativeClicked() {

                    }

                    override fun onPositiveClicked() {
                        try {
                            var data =
                                "Apk Size: " + GlobalClass!!.getFolderSizeLabel(GlobalClass.getFileAPK()) + "\n"
                            "Device ID: " + GlobalClass.deviceID() + "\n"
                            "AppName: " + (activity as CIFRootActivity).resources.getString(R.string.app_name) + "\n"
                            "Build Installed Time: " + GlobalClass.firstModifiedDate() + "\n"
                            "Build Update Time: " + GlobalClass.lastModifiedDate() + "\n"
                            "UserID: " + (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID() + "\n"
                            "Version Name: " + (activity as CIFRootActivity).packageManager.getPackageInfo(
                                (activity as CIFRootActivity).packageName,
                                0
                            ).versionCode.toString() + "\n"
                            "Version Code: " + (activity as CIFRootActivity).packageManager.getPackageInfo(
                                (activity as CIFRootActivity).packageName,
                                0
                            ).versionCode.toString() + "\n"
                            "Android Version: " + android.os.Build.VERSION.SDK_INT + "\n"
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                data
                            )
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as UnsatisfiedLinkError)
                            )
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as NullPointerException)
                            )
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as IllegalArgumentException)
                            )
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as NumberFormatException)
                            )
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as InterruptedException)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as RuntimeException)
                            )
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as IOException)
                            )
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as FileNotFoundException)
                            )
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as ClassNotFoundException)
                            )
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as ActivityNotFoundException)
                            )
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as IndexOutOfBoundsException)
                            )
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as ArrayIndexOutOfBoundsException)
                            )
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as ClassCastException)
                            )
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as TypeCastException)
                            )
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as SecurityException)
                            )
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as IllegalStateException)
                            )
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as OutOfMemoryError)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as RuntimeException)
                            )
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as Exception)
                            )
                        }
                    }
                })
        } else {
            (GlobalClass.applicationContext as GlobalClass).showSingleBtnConfirmationDialog(
                requireContext(),
                title,
                msg,
                button1,
                object : Dialog2Callback, com.hbl.bot.utils.DialogCallback {
                    override fun onNegativeClicked() {

                    }

                    override fun onPositiveClicked() {
                        try {
                            signaturePad.clear()
                            if (isCountinue) {
                                postSignature(
                                    bitmapList
                                )

                            }
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as UnsatisfiedLinkError)
                            )
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as NullPointerException)
                            )
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as IllegalArgumentException)
                            )
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as NumberFormatException)
                            )
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as InterruptedException)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as RuntimeException)
                            )
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as FileNotFoundException)
                            )
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as ClassNotFoundException)
                            )
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as ActivityNotFoundException)
                            )
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as IndexOutOfBoundsException)
                            )
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as ArrayIndexOutOfBoundsException)
                            )
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as ClassCastException)
                            )
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as TypeCastException)
                            )
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as SecurityException)
                            )
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as IllegalStateException)
                            )
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as OutOfMemoryError)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as CIFRootActivity),
                                (e as RuntimeException)
                            )
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                        }
                    }
                })
        }

    }

    fun permission() {
        Permissions.check(
            activity as CIFRootActivity,
            permissions,
            null,
            null,
            object : PermissionHandler() {
                override fun onGranted() {
                    // do your task.
                    deltAllFiles()
                }

                @SuppressLint("WrongConstant")
                override fun onDenied(
                    context: Context,
                    deniedPermissions: ArrayList<String>,
                ) {
                    ToastUtils.normalShowToast(
                        activity as CIFRootActivity,
                        "Permission denied, please enabled the permissions from setting", 1
                    )
                }
            })
    }

    private fun deltAllFiles() {
        File(
            Environment.getExternalStorageDirectory().toString(), ".AOF"
        ).deleteDirectoryFiles()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            init()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun init() {
        (activity as CIFRootActivity).siIndicator!!.visibility = View.INVISIBLE
        (activity as CIFRootActivity).tvTitle.text = "Page: Signature"
        btClearSignature.setOnClickListener(this)
        nextBtn.setOnClickListener(this)
        viewModel.totalSteps.value = 2
        viewModel.currentFragmentIndex.value = 0
        permission()
        signaturePad.setOnSignedListener(object : SignaturePad.OnSignedListener {
            override fun onClear() {
                Log.i("Clear", "Sign")
            }

            override fun onSigned() {
                Log.i("Done", "Signing")
            }

            override fun onStartSigning() {
                Log.i("Start", "Signing")
            }
        })
    }

    fun File.deleteDirectoryFiles() {
        if (this.listFiles() != null) {
            this.listFiles().forEach {
                if (it.isDirectory) {
                    it.deleteDirectoryFiles()
                } else {
                    it.delete()
                }
            }
        }

        this.delete()
    }

    fun postSignature(bitmapList: ArrayList<String>) {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var request = SignatureRequest()
        request.tRACKINGID =
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID
        if (bitmapList.size == 2) {
            request.minorSign = bitmapList[0]
            request.gaurdianSign = bitmapList[1]
        } else {
            request.gaurdianSign = ""
            request.minorSign = bitmapList[0]
        }
        bitmapList.clear()
        HBLHRStore.instance?.postSignature(
            RetrofitEnums.URL_HBL,
            request, object : SignatureCallBack {
                override fun Success(response: SignatureResponse) {
                    try {
                        if (findNavController().currentDestination?.id == R.id.SignatureScreen)
                            findNavController().navigate(R.id.action_signatureScreen_to_13)
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as CIFRootActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun Failure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun convertToBase64(bitmap: Bitmap): String {
        val byteArrayOutputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        return Base64.encodeToString(byteArray, Base64.NO_WRAP)
    }
}
