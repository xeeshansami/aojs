package com.hbl.bot.ui.fragments.spark.cif

import com.hbl.bot.ui.activities.spark.CIFRootActivity
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.CIFCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.CIFRequest
import com.hbl.bot.network.models.request.baseRM.CUSTADDRRESS
import com.hbl.bot.network.models.response.base.AofAccountInfoResponse
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep1.btNext
import kotlinx.android.synthetic.main.fragment_cifstep1_5.*
import kotlinx.android.synthetic.main.fragment_cifstep1_5.btBack
import kotlinx.android.synthetic.main.fragment_cifstep1_5.formSectionHeader
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

class CIFStep1_5 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep1_5, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }finally {
            return myView
        }

    }

    fun loadDummyData() {

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        val view: TextView = formSectionHeader.getTextView()
        viewModel.totalSteps.value = 2
        viewModel.currentFragmentIndex.value = 1
        val txt = resources.getString(R.string.bio_metric_Information)
        val txt1 = " (2/2 Page)"
        val txt2 = txt + txt1
        view.text = GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            btNext.setOnClickListener(this)
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            if ((activity as CIFRootActivity).backInt == 1) {
                btNext.text = "Done"
                (activity as CIFRootActivity).siIndicator!!.visibility = View.GONE
            }

            btBack.setOnClickListener(this)
            header()
            (activity as CIFRootActivity).sharedPreferenceManager.nadraResponseData.let { nadraVerify ->
                (activity as CIFRootActivity).cnicNumber.let { citizenID.text = it }
                nadraVerify?.run {
                    RESPONSE_PHOTOGRAPH?.let { bitmap ->
                        Glide.with(activity as CIFRootActivity)
                            .load(GlobalClass.decodeBase64AndSetImage(bitmap))
                            .apply(RequestOptions.circleCropTransform())
                            .into(ivProfile)
                    }
                    responseCode.text = responsecode
                    sessionID.text = sessionid
                    citizennumber.let { citizenID.text = it }
                    responseName.text = responsename + "\n\n" + responsE_NAME_ENG
                    responseFatherName.text =
                        responsefatherhusbandname + "\n\n" + responsE_FATHER_HUSBAND_NAME_ENG
                    presentAddress.text =
                        responsepresentaddress + "\n\n" + responsE_PRESENT_ADDRESS_ENG
                    permanentAddress.text =
                        responsepermanentaddress + "\n\n" + responsE_PERMANENT_ADDRESS_ENG
                    date_of_birth.text = responsedateofbirth
                    birth_place.text =
                        responsebirthplace + "\n\n" + responsE_BIRTH_PLACE_ENG
                    expiry_date.text = responseexpirydate
                    card_type.text = responsecardtype
                    responseMessage.text = responsemessage
                    verisys_permormed_on.text = datetime
                    verisys_permormed_by.text = userid
                    verisys_permormed_from.text = branchcode
                    if (!(activity as CIFRootActivity).aofAccountInfoRequest.isBioModelNull) {
                        if (!custInfo.FIRST_NAME.isNullOrEmpty()) {
                            custInfo.FIRST_NAME.let {
                                (activity as CIFRootActivity).customerInfo.FIRST_NAME = it
                            }
                        }
                        if (!custInfo.MIDDLE_NAME.isNullOrEmpty()) {
                            custInfo.MIDDLE_NAME.let {
                                (activity as CIFRootActivity).customerInfo.MIDDLE_NAME = it
                            }
                        }
                        if (!custInfo.LAST_NAME.isNullOrEmpty()) {
                            custInfo.LAST_NAME.let {
                                (activity as CIFRootActivity).customerInfo.LAST_NAME = it
                            }
                        }
                        if (!custInfo.FATHER_NAME.isNullOrEmpty()) {
                            custInfo.FATHER_NAME.let {
                                (activity as CIFRootActivity).customerInfo.FATHER_NAME = it
                            }
                        }
                        responseexpirydate.let {
                            (activity as CIFRootActivity).customerInfo.EXPIRY_DATE = it
                        }
                        responsedateofbirth.let {
                            (activity as CIFRootActivity).customerInfo.DATE_OF_BIRTH = it
                        }
                        if ((activity as CIFRootActivity).customerAddressList.size >= 1) {
                            (activity as CIFRootActivity).customerAddressList.getOrNull(0).let {
                                var customerAddress: CUSTADDRRESS = it!!
                                if (!custAddrResidential.ADDRESS_LINE1.isNullOrEmpty()) {
                                    customerAddress.ADDRESS_LINE1
                                }
                                if (!custAddrResidential.ADDRESS_LINE2.isNullOrEmpty()) {
                                    customerAddress.ADDRESS_LINE2
                                }
                                if (!custAddrResidential.ADDRESS_LINE3.isNullOrEmpty()) {
                                    customerAddress.ADDRESS_LINE3
                                }
                                if ((activity as CIFRootActivity).customerAddressList.size == 0) {
                                    (activity as CIFRootActivity).customerAddressList.add(
                                        0,
                                        customerAddress
                                    )
                                } else if ((activity as CIFRootActivity).customerAddressList.size >= 1) {
                                    (activity as CIFRootActivity).customerAddressList.set(
                                        0,
                                        customerAddress
                                    )
                                }
                            }
                            (activity as CIFRootActivity).customerAddressList?.getOrNull(1).let {
                                var customerAddress2: CUSTADDRRESS = it!!
                                if (!custAddrPermanent.ADDRESS_LINE1.isNullOrEmpty()) {
                                    customerAddress2.ADDRESS_LINE1
                                }
                                if (!custAddrPermanent.ADDRESS_LINE2.isNullOrEmpty()) {
                                    customerAddress2.ADDRESS_LINE2
                                }
                                if (!custAddrPermanent.ADDRESS_LINE3.isNullOrEmpty()) {
                                    customerAddress2.ADDRESS_LINE3
                                }
                                if ((activity as CIFRootActivity).customerAddressList.size == 1) {
                                    (activity as CIFRootActivity).customerAddressList.add(
                                        1,
                                        customerAddress2
                                    )
                                } else if ((activity as CIFRootActivity).customerAddressList.size >= 2) {
                                    (activity as CIFRootActivity).customerAddressList[1] =
                                        customerAddress2
                                }
                            }
                        } else {
                            var customerAddress: CUSTADDRRESS = CUSTADDRRESS()
                            if (!custAddrResidential.ADDRESS_LINE1.isNullOrEmpty()) {
                                customerAddress.ADDRESS_LINE1
                            }
                            if (!custAddrResidential.ADDRESS_LINE2.isNullOrEmpty()) {
                                customerAddress.ADDRESS_LINE2
                            }
                            if (!custAddrResidential.ADDRESS_LINE3.isNullOrEmpty()) {
                                customerAddress.ADDRESS_LINE3
                            }
                            if ((activity as CIFRootActivity).customerAddressList.size == 0) {
                                (activity as CIFRootActivity).customerAddressList.add(
                                    0,
                                    customerAddress
                                )
                            } else if ((activity as CIFRootActivity).customerAddressList.size >= 1) {
                                (activity as CIFRootActivity).customerAddressList.set(
                                    0,
                                    customerAddress
                                )
                            }
                            var customerAddress2: CUSTADDRRESS = CUSTADDRRESS()
                            if (!custAddrPermanent.ADDRESS_LINE1.isNullOrEmpty()) {
                                customerAddress2.ADDRESS_LINE1
                            }
                            if (!custAddrPermanent.ADDRESS_LINE2.isNullOrEmpty()) {
                                customerAddress2.ADDRESS_LINE2
                            }
                            if (!custAddrPermanent.ADDRESS_LINE3.isNullOrEmpty()) {
                                customerAddress2.ADDRESS_LINE3
                            }
                            if ((activity as CIFRootActivity).customerAddressList.size == 1) {
                                (activity as CIFRootActivity).customerAddressList.add(
                                    1,
                                    customerAddress2
                                )
                            } else if ((activity as CIFRootActivity).customerAddressList.size >= 2) {
                                (activity as CIFRootActivity).customerAddressList[1] =
                                    customerAddress2
                            }
                        }
                        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo =
                            (activity as CIFRootActivity).customerInfo
                        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress =
                            (activity as CIFRootActivity).customerAddressList
                        (activity as CIFRootActivity).init()
                        (activity as CIFRootActivity).aofAccountInfoRequest.isBioModelNull = true
                        (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo =
                            (activity as CIFRootActivity).aofAccountInfoRequest
                    }
                }
            }
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {

        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {

            }
            R.id.btNext -> {
                try {
                    if ((activity as CIFRootActivity).backInt == 2) {
                        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_NO.isNullOrEmpty()) {
                            callCIF()
                        } else {
                            if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                                findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep7)
                            }
                        }

                    } else if ((activity as CIFRootActivity).backInt == 0) {
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep1_6)
                        }
                    } else {
                        (activity as CIFRootActivity).finish()
                    }
                    (activity as CIFRootActivity).init()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                } finally {

                }
            }
            R.id.btBack -> {
                if ((activity as CIFRootActivity).customerBiometric.responsecode == "100") {
                    if ((activity as CIFRootActivity).backInt == 0) {
                        /*cif condition backward*/
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep1_3)
                        }
                    } else if ((activity as CIFRootActivity).backInt == 2) {
                        /*account condition backward*/
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_accountPage)
                        }
                    } else if ((activity as CIFRootActivity).backInt == 1) {
                        /*biometric condition backward*/
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep1_4)
                        }
                    }else{
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep1_3)
                        }
                    }
                } else {
                    if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                        findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep1_4)
                    }
                }
            }
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if ((activity as CIFRootActivity).customerBiometric.responsecode == "100") {
                    if ((activity as CIFRootActivity).backInt == 0) {
                        /*cif condition backward*/
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep1_3)
                        }
                    } else if ((activity as CIFRootActivity).backInt == 2) {
                        /*account condition backward*/
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_accountPage)
                        }
                    } else if ((activity as CIFRootActivity).backInt == 1) {
                        /*biometric condition backward*/
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep1_4)
                        }
                    }
                } else {
                    if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                        findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep1_4)
                    }
                }
                true
            }
            false
        }
    }

    fun callCIF() {
        var cifRequest = CIFRequest()
        cifRequest.payload = Constants.PAYLOAD
        cifRequest.scnic =
            (activity as CIFRootActivity).sharedPreferenceManager.getStringFromSharedPreferences(
                Constants.ACCOUNT_CNIC
            )
        HBLHRStore.instance?.getCIF(RetrofitEnums.URL_HBL, cifRequest, object : CIFCallBack {
            override fun CIFSuccess(response: AofAccountInfoResponse) {
                try {
                    if (response.status == "00") {
                        SparkDataSetupInSp(activity as CIFRootActivity, response)
                        (activity as CIFRootActivity).init()


                        (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                            ?.get(0)?.itemDescription.let {
                                (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID =
                                    it.toString()
                                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo =
                                    (activity as CIFRootActivity).aofAccountInfoRequest
                            }
                        if (findNavController().currentDestination?.id == R.id.CIFStep1_5) {
                            findNavController().navigate(R.id.action_CIFStep1_5_to_CIFStep7)
                        }
                    } else {
                        ToastUtils.normalShowToast(context, response.message,2)
                    }
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                } finally {

                }
            }

            override fun CIFFailure(response: BaseResponse) {
//                Utils.failedAwokeCalls((activity as CIFRootActivity)) {callCIF() }
                ToastUtils.normalShowToast(activity, response.message, 1)
                (activity as CIFRootActivity).globalClass?.hideLoader()

            }
        })

    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
