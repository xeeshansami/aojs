package com.hbl.bot.ui.adapter

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.Context
import android.content.Intent
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.TextView
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.ui.activities.spark.CallsForMeetingActivity

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.BusinessAreaRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.base.TrackingIDRequest
import com.hbl.bot.network.models.request.base.UpdateCallRequest
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Onboarding
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import java.text.SimpleDateFormat
import java.util.*

class CallingAdapter(var mCtx: Context, var resources: Int, var items: List<Onboarding>?) :
    ArrayAdapter<Onboarding>(
        mCtx, resources,
        items!!
    ) {


    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()
        sharedPreferenceManager.getInstance(mCtx)

        val layoutInflater: LayoutInflater = LayoutInflater.from(mCtx)
        val view: View = layoutInflater.inflate(resources, null)
        val NameTextView: TextView = view.findViewById(R.id.txtName)
        val ContactTextView: TextView = view.findViewById(R.id.txtContactNo)
        val TrackingNoTextView: TextView = view.findViewById(R.id.txtTrackingNo)
        val DateTextView: TextView = view.findViewById(R.id.txtDate)
        val AddressTextView: TextView = view.findViewById(R.id.txt_Address)
        val AppliedForTextView: TextView = view.findViewById(R.id.txtLocation)
        val btnCall: Button = view.findViewById(R.id.btnCall)
        val btnCallFoward: Button = view.findViewById(R.id.btnCallFoward)
        val imgCallWaiting: Button = view.findViewById(R.id.imgCallWaiting)
        val imgLocation: Button = view.findViewById(R.id.imgLocation)
        var mItem: Onboarding = this.items!![position]
        var globalClass: GlobalClass? = null
        var date = null;




        btnCallFoward.setOnClickListener(View.OnClickListener {

            globalClass?.showDialog(mCtx)
            callTrackingID(0)
            val intent = Intent(mCtx, CIFRootActivity::class.java)
            intent.putExtra(Constants.ACTIVITY_KEY, Constants.CIF_1_SCREEN)
            GlobalClass.CINC = ""
            mCtx.startActivity(intent)
//            view.findNavController().navigate()

        })



        btnCall.setOnClickListener(View.OnClickListener {

            Utils.makeCall(ContactTextView.text.toString(), mCtx)
        })



        imgLocation.setOnClickListener(View.OnClickListener {
            Utils.openMap(AddressTextView.text.toString(), mCtx)
        })

        imgCallWaiting.setOnClickListener {
            val cal = Calendar.getInstance()
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                cal.set(Calendar.HOUR_OF_DAY, hour)
                cal.set(Calendar.MINUTE, minute)
                GlobalClass.CallTime = SimpleDateFormat("hh:mm").format(cal.time)
//                ToastUtils.normalShowToast(mCtx, GlobalClass.MeetTime)
                GlobalClass.fullCallDateTime = GlobalClass.Calldate + " " + GlobalClass.CallTime

                if (!TextUtils.isEmpty(GlobalClass.Calldate) && !TextUtils.isEmpty(GlobalClass.CallTime)) {

                    var request = UpdateCallRequest()
                    request.tracking_id = mItem.TRACKING_ID
                    request.CALL_DATE_TIME = GlobalClass.fullCallDateTime
                    request.MEET_DATE_TIME = ""
                    request.identifier = "updatelead"
                    HBLHRStore.instance?.callUpdate(
                        RetrofitEnums.URL_HBL,
                        request,
                        object : callUpdateCallBack {
                            override fun callUpdateSuccess(response: UpdateCallResponse) {
                                ToastUtils.normalShowToast(mCtx, response.message.toString(),2)
                                (mCtx as CallsForMeetingActivity).finish()
                            }

                            override fun callUpdateFailure(response: BaseResponse) {
                                ToastUtils.normalShowToast(mCtx, response.message.toString(),1)


                            }
                        })
                }

            }
            TimePickerDialog(
                mCtx,
                R.style.DialogTheme,
                timeSetListener,
                cal.get(Calendar.HOUR_OF_DAY),
                cal.get(Calendar.MINUTE),
                false
            ).show()
            val dpd = DatePickerDialog(
                mCtx,
                R.style.DialogTheme,
                DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth ->
                    // Display Selected date in TextView
                    cal.set(Calendar.YEAR, year)
                    cal.set(Calendar.MONTH, month)
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                    GlobalClass.Calldate = "$month/$dayOfMonth/$year"
//                    ToastUtils.normalShowToast(mCtx, GlobalClass.Meetdate)
//                textView.setText("" + dayOfMonth + " " + month + ", " + year)
                },
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)
            )
            dpd.show()


        }


        NameTextView.text = mItem.NAME
        ContactTextView.text = mItem.CONTACT_NO
        TrackingNoTextView.text = mItem.TRACKING_ID
        DateTextView.text = mItem.CALL_DATE_TIME
        AddressTextView.text = mItem.ADDRESS
        when {
            mItem.APPLIED_FOR?.get(0)?.ACCOUNT.equals("Y") -> {
                AppliedForTextView.text = mItem.APPLIED_FOR?.get(0)?.DESC_ACCOUNT
            }
            mItem.APPLIED_FOR?.get(0)?.CREDIT_CARD.equals("Y") -> {
                AppliedForTextView.text = mItem.APPLIED_FOR?.get(0)?.DESC_CREDIT_CARD
            }
            mItem.APPLIED_FOR?.get(0)?.LOAN.equals("Y") -> {
                AppliedForTextView.text = mItem.APPLIED_FOR?.get(0)?.DESC_LOAN
            }


        }



        return view


    }

    fun callTrackingID(activityNumber: Int) {
        var trackingIDRequest = TrackingIDRequest()
        trackingIDRequest?.brcode =
            GlobalClass.sharedPreferenceManager!!.getLoginData()
                .getBRANCHCODE()
        HBLHRStore.instance?.getTrackingID(
            RetrofitEnums.URL_HBL,
            trackingIDRequest,
            object : TrackingIDCallBack {
                override fun TrackingIDSuccess(response: TrackingIDResponse) {
                    GlobalClass.sharedPreferenceManager!!.setTrackingID(
                        response?.data?.get(0)
                    )
                    callMySisRef(activityNumber)
                }

                override fun TrackingIDFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(mCtx, response.message,1)
                }
            })
    }

    fun callMySisRef(activityNumber: Int) {
        var lov = LovRequest()
        lov.identifier = Constants.WORKFLOW_IDENTIFIER
        HBLHRStore.instance?.getWorkFlow(
            RetrofitEnums.URL_HBL,
            lov,
            object : WorkFlowCallBack {
                override fun WorkFlowSuccess(response: WorkFlowResponse) {
                    GlobalClass.sharedPreferenceManager!!.setWorkFlow(
                        response
                    )
                    callWorkFlow(activityNumber)
                }

                override fun WorkFlowFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(mCtx, response.message,1)
//                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callWorkFlow(activityNumber: Int) {
        var lov = LovRequest()
        lov.identifier = Constants.MYSISREF_IDENTIFIER
        HBLHRStore.instance?.getMySisRef(
            RetrofitEnums.URL_HBL,
            lov,
            object : MySisRefCallBack {
                override fun MySisRefSuccess(response: MYSISREFResponse) {
                    GlobalClass.sharedPreferenceManager!!.setMySisRef(
                        response
                    )
                    callBusinessArea(activityNumber)
                }

                override fun MySisRefFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(mCtx, response.message,1)
//                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callBusinessArea(activityNumber: Int) {
        var lov = BusinessAreaRequest()
        lov.identifier = Constants.BUSINESSAREA_IDENTIFIER
        lov.find.BR_CODE =
            GlobalClass.sharedPreferenceManager!!.getLoginData().getBRANCHCODE().toString()
        HBLHRStore.instance?.getBusinessArea(
            RetrofitEnums.URL_HBL,
            lov,
            object : BusinessAreaCallBack {
                override fun BusinessAreaSuccess(response: BusinessAreaResponse) {
                    GlobalClass.sharedPreferenceManager!!.setBusinessArea(
                        response.data.get(0)
                    )
                    callToActivity(activityNumber)
                }

                override fun BusinessAreaFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(mCtx, response.message, 1)
//                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callToActivity(activityNumber: Int) {
//        (activity as NavigationDrawerActivity).globalClass?.hideLoader()
//        if (activityNumber == 0) {
//            findNavController().navigate(R.id.action_nav_dashboard_to_CIFRootActivity)
//        } else if (activityNumber == 2) {
//            findNavController().navigate(R.id.action_nav_dashboard_to_Open_quick_account)
//        }
    }

    private fun encodeValue(value: String): String {
        return URLEncoder.encode(value, StandardCharsets.UTF_8.toString())

    }


}