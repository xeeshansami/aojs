package com.hbl.bot.ui.fragments.spark.cif;

import android.Manifest;

import com.hbl.bot.network.ResponseHandlers.callbacks.CheckNumberNadraCallBack;
import com.hbl.bot.network.models.request.base.CheckNumberNadraRequest;
import com.hbl.bot.network.models.response.base.CheckNumberNadraResponse;
import com.hbl.bot.ui.activities.spark.CIFRootActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;

import com.google.gson.Gson;
import com.hbl.bot.R;
import com.hbl.bot.network.ResponseHandlers.callbacks.BioAccountTypeCallBack;
import com.hbl.bot.network.ResponseHandlers.callbacks.NadraVerifyCallBack;
import com.hbl.bot.network.ResponseHandlers.callbacks.ProvinceCallBack;
import com.hbl.bot.network.enums.RetrofitEnums;
import com.hbl.bot.network.models.request.base.GetVerysisRequest;
import com.hbl.bot.network.models.request.base.LovRequest;
import com.hbl.bot.network.models.request.base.ManualVersysisRequest;
import com.hbl.bot.network.models.response.base.BaseResponse;
import com.hbl.bot.network.models.response.base.BioAccountTypeResponse;
import com.hbl.bot.network.models.response.base.NadraVerifyResponse;
import com.hbl.bot.network.models.response.base.ProvinceResponse;
import com.hbl.bot.network.models.response.baseRM.BioAccountType;
import com.hbl.bot.network.models.response.baseRM.Province;
import com.hbl.bot.network.models.response.baseRM.RESPONSEFINGERINDEX;
import com.hbl.bot.network.store.HBLHRStore;
import com.hbl.bot.ui.customviews.FormBoolSelectionView;
import com.hbl.bot.ui.customviews.FormInputView;
import com.hbl.bot.ui.customviews.FormSectionHeader;
import com.hbl.bot.ui.customviews.FormSelectionView;
import com.hbl.bot.utils.Constants;
import com.hbl.bot.utils.FingerImpression;
import com.hbl.bot.utils.GlobalClass;
import com.hbl.bot.utils.SendEmail;
import com.hbl.bot.utils.ToastUtils;
import com.hbl.bot.utils.managers.SharedPreferenceManager;
import com.hbl.bot.utils.morpho.CaptureInfo;
import com.hbl.bot.utils.morpho.Uitls;
import com.hbl.bot.viewModels.SharedCIFViewModel;
//import com.idemia.peripherals.PeripheralsPowerInterface;
import com.morpho.android.usb.USBManager;
import com.morpho.morphosmart.sdk.CallbackMessage;
import com.morpho.morphosmart.sdk.Coder;
import com.morpho.morphosmart.sdk.CompressionAlgorithm;
import com.morpho.morphosmart.sdk.CustomInteger;
import com.morpho.morphosmart.sdk.EnrollmentType;
import com.morpho.morphosmart.sdk.ErrorCodes;
import com.morpho.morphosmart.sdk.LatentDetection;
import com.morpho.morphosmart.sdk.MorphoDevice;
import com.morpho.morphosmart.sdk.MorphoImage;
import com.morpho.morphosmart.sdk.MorphoImageHeader;
import com.morpho.morphosmart.sdk.SecurityLevel;
import com.morpho.morphosmart.sdk.StrategyAcquisitionMode;
import com.morpho.morphosmart.sdk.TemplateFVPType;
import com.morpho.morphosmart.sdk.TemplateList;
import com.morpho.morphosmart.sdk.TemplateType;

import org.jetbrains.annotations.NotNull;

import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Locale;
import java.util.Observable;
import java.util.Observer;

import pl.droidsonroids.gif.GifImageView;

@SuppressLint({"SdCardPath", "HandlerLeak"})
public class CIFStep1_4_Morpho_fingerprint_6_34 extends Fragment implements View.OnClickListener, Observer {
    /*Morpho------------------------------*/
    private String TAG = "MorphoSample";
    private String sensorName = "";
    private Handler mHandler = new Handler();
    private DeviceBroadcastReceiver mDeviceReceiver;
    //    private PermissionBroadcastReceiver permissionBroadcastReceiver;
    private MorphoDevice morphoDevice = null;
    String tem_type = "";
    Bitmap imageBmp = null;
    byte[] image = null;
    /*Morpho------------------------------*/
    int countR_Thumb = 0, countR_Index = 0, countR_Middle = 0, countR_Ring = 0, countR_Little = 0;
    int countL_Thumb = 0, countL_Index = 0, countL_Middle = 0, countL_Ring = 0, countL_Little = 0;
    int finger1 = 0, finger2 = 0, finger3 = 0, finger4 = 0, finger5 = 0;
    int finger6 = 0, finger7 = 0, finger8 = 0, finger9 = 0, finger10 = 0;
    FormBoolSelectionView nadra_fingers;
    String fingers[] = {
            "Select an finger",
            "Right Thumb",
            "Right Index Finger",
            "Right Middle Finger",
            "Right Ring Finger",
            "Right Little Finger",
            "Left Thumb",
            "Left Index Finger",
            "Left Middle Finger",
            "Left Ring Finger",
            "Left Little Finger"};
    String indexOfNadra[] = {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10"};
    String test[] = {
            "4",
            "1",
            "5",
            "9"};
    ArrayList<String> fingersList;
    int nadraRequestCount = 9;
    FormSectionHeader formSectionHeader;
    FormSelectionView nadra_provice, nadra_ac_type;
    String sessiondID = "";
    boolean fingerRetry = false;
    Spinner nadra_provice_spinner, nadra_ac_type_spinner, nadra_fingers_spinner;
    FormInputView fivMobileNumber, fivCnic;
    View fragmentView;
    public Bitmap bitmapFingerPrint;
    ImageView iv_left, iv_Right;
    ArrayList<FingerImpression> fingerImpressions;
    Button btVerify, btBack, btGetVerysis, btCheckMobile;
    SharedCIFViewModel viewModel;
    //Finger Print Declarations
    ProgressDialog progressDialog;
    GlobalClass globalClass;
    EditText numberET, cnicEt;
    TextView fivIssueDateTV, attempsError;
    ArrayList<Province> provincesList;
    ArrayList<BioAccountType> accountTypeList;
    LovRequest lovRequest;
    HBLHRStore hblhrStore;
    String indexOfFinger = "0";
    View view;
    Calendar myCalendar = Calendar.getInstance();
    public AlertDialog.Builder alertDialogBuilder, alertDialogBuilderMV;
    public AlertDialog dialog, dialogMV;
    public AlertDialog verifyDialog;
    Toolbar toolbar;
    public TextView error_box1, error_box2, error_box3, error_box4, verification_place_txt;
    public Button cancel_action, done_action;
    ImageView fingerPrintImg;
    GifImageView scanner;
    Button done;
    boolean isWorking = false;
    /*MorphoDevice_Android Device 10, 6.34*/
    private View rootView;
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    private String nadraErrorCode;
    private boolean deviceIsSet;
    private boolean capturing;
    private ImageView mFingerprintImage;
    private boolean grantPermissions;
    private boolean nadraActive;
    private String nadraErrorMessage;
    boolean verifyPermission = false;

    /**
     * To know status of sensor Working/ideal
     */
    public void setButtonEnabled(boolean enabled) {
        try {
            isWorking = !enabled;
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }


    public void errorDialog(String ex, String title) {
        try {
            if (dialog != null && dialog.isShowing()) {
                dialog.dismiss();
            }
            alertDialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.popup_error_box, null);
            alertDialogBuilder.setView(view);
            alertDialogBuilder.setCancelable(false);
            dialog = alertDialogBuilder.create();
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            toolbar.setTitle(title);
            toolbar.setTitleTextColor(getActivity().getResources().getColor(R.color.colorWhite));
            toolbar.setSubtitleTextColor(getActivity().getResources().getColor(R.color.colorWhite));
            error_box1 = view.findViewById(R.id.error1);
            error_box1.setText(title);
            error_box2 = view.findViewById(R.id.error2);
            error_box2.setText(ex);
            error_box3 = view.findViewById(R.id.error3);
            error_box4 = view.findViewById(R.id.error4);
            cancel_action = view.findViewById(R.id.cancel_action);
            cancel_action.setOnClickListener(view1 -> {
                try {
                    dialog.dismiss();
                    /*((CIFRootActivity) getActivity()).init();
                    if (Navigation.findNavController(fragmentView).popBackStack()) {
                        Navigation.findNavController(fragmentView).popBackStack();
                    } else {
                        getActivity().finish();
                    }*/
                } catch (NoClassDefFoundError e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                } catch (UnsatisfiedLinkError e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                } catch (NullPointerException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                } catch (IllegalArgumentException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                } catch (ActivityNotFoundException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                } catch (ClassCastException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                } catch (ArrayIndexOutOfBoundsException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                } catch (IndexOutOfBoundsException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                } catch (SecurityException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                } catch (IllegalStateException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                } catch (OutOfMemoryError e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                } catch (RuntimeException e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                } catch (Exception e) {
                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                }
            });
            if (!dialog.isShowing()) {
                dialog.show();
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void verifyDialog() {
        try {
            alertDialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.popup_verify_dialog, null);
            alertDialogBuilder.setView(view);
            alertDialogBuilder.setCancelable(false);
            verifyDialog = alertDialogBuilder.create();
            toolbar = view.findViewById(R.id.toolbar);
            verification_place_txt = view.findViewById(R.id.verification_place_txt);
            fingerPrintImg = view.findViewById(R.id.fingerPrintImg);
            scanner = view.findViewById(R.id.scanner);
            done = view.findViewById(R.id.done);
            toolbar.setTitle("Biometric Verification");
            toolbar.setTitleTextColor(getActivity().getResources().getColor(R.color.green));
            toolbar.setSubtitleTextColor(getActivity().getResources().getColor(R.color.green));
            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        verifyDialog.dismiss();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void hideLoader() {
        try {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.cancel();
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        try {
            fragmentView = inflater.inflate(R.layout.fragment_cifstep1_4, container, false);
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        } finally {
            return fragmentView;
        }
    }

    public void loadData() {
        try {
//            numberET.setText("03412030258");
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        try {
            super.onViewCreated(view, savedInstanceState);
            verifyStoragePermisssions(getActivity());
            if (((CIFRootActivity) getActivity()).getBackInt() == 1) {
                ((CIFRootActivity) getActivity()).getSiIndicator().setVisibility(View.GONE);
            }
            header();
            onBackPress(view);
            provincesList = new ArrayList<>();
            accountTypeList = new ArrayList<>();
            fingerImpressions = new ArrayList<>();
            fingersList = new ArrayList<>();
            fingersList.addAll(Arrays.asList(fingers));
            Collections.addAll(fingerImpressions, FingerImpression.values());
            globalClass = new GlobalClass();
            hblhrStore = new HBLHRStore();
            progressDialog = new ProgressDialog(getActivity());
            // Initialize UI
            rootView = getActivity().findViewById(android.R.id.content).getRootView();
            iv_left = fragmentView.findViewById(R.id.iv_left);
            iv_Right = fragmentView.findViewById(R.id.iv_Right);
            btVerify = fragmentView.findViewById(R.id.btVerify);
            btBack = fragmentView.findViewById(R.id.btBack);
            btCheckMobile = fragmentView.findViewById(R.id.btCheckMobile);
            btGetVerysis = fragmentView.findViewById(R.id.btGetVerysis);
            nadra_provice = fragmentView.findViewById(R.id.fsvProvince);
            nadra_ac_type = fragmentView.findViewById(R.id.fsvAccountType);
            nadra_fingers = fragmentView.findViewById(R.id.nadra_fingers);
            formSectionHeader = fragmentView.findViewById(R.id.formSectionHeader);
            fivMobileNumber = fragmentView.findViewById(R.id.fivMobileNumber);
            attempsError = fragmentView.findViewById(R.id.attempsError);
            fivCnic = fragmentView.findViewById(R.id.fivCnic);
            init();
            setFingerForNadra();
            ((CIFRootActivity) getActivity()).getIvRefresh().setOnClickListener(this);
            btVerify.setOnClickListener(this);
            btBack.setOnClickListener(this);
            btGetVerysis.setOnClickListener(this);
            btCheckMobile.setOnClickListener(this);
            mFingerprintImage = (ImageView) view.findViewById(R.id.fingerPrintImg);
            capturing = false;
            deviceIsSet = false;
            // Initialize the Observer object for the capture's callback
            verifyDialog();
            load();
//            if (GlobalClass.isDummyDataTrue) {
//                loadData();
//            }

            morphoInit();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void load() {
        try {
            reset();
            if (((CIFRootActivity) getActivity()).getBackInt() == 1) {
                globalClass.clearSubmitAOF();
            }
            if (((CIFRootActivity) getActivity()).sharedPreferenceManager.getProvince() == null) {
                getProvince();
            } else {
                setProvince(((CIFRootActivity) getActivity()).sharedPreferenceManager.getProvince());
            }
            //TODO: when minor case || ncr case occurred check number button will not be shown and verify button enabled
            if((((CIFRootActivity) getActivity()).getBackInt() == 0 && ((CIFRootActivity) getActivity()).sharedPreferenceManager.getCustomerDemoGraphics().getCUST_TYPE().equalsIgnoreCase("A7"))||
                    (((CIFRootActivity) getActivity()).getBackInt() == 1 )){
                btCheckMobile.setVisibility(View.GONE);
                globalClass.setEnabled(btVerify, true);
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }


    public void setProvince(ArrayList<Province> response) {
        try {
            nadra_provice.setItemForProvince(response);
            ((CIFRootActivity) getActivity()).sharedPreferenceManager.setLovProvince(response);
            nadra_provice_spinner.setSelection(((CIFRootActivity) getActivity()).globalClass.
                    findSpinnerPositionFromProvinceIndex(response,
                            ((CIFRootActivity) getActivity()).customerBiometric.getPROVINCEDESC()));
            provincesList = response;
            if (((CIFRootActivity) getActivity()).sharedPreferenceManager.getBioAccountType() == null) {
                getAccountType();
            } else {
                setAccountType(((CIFRootActivity) getActivity()).sharedPreferenceManager.getBioAccountType());
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void getProvince() {
        try {
            globalClass.showDialog(getActivity());
            lovRequest = new LovRequest();
            lovRequest.setIdentifier(Constants.PROVINCE_IDENTIFIER);
            String json=new Gson().toJson(lovRequest);
            hblhrStore.getProvince(RetrofitEnums.URL_HBL, lovRequest, new ProvinceCallBack() {
                @Override
                public void ProvinceSuccess(@NotNull ProvinceResponse response) {
                    try {
                        setProvince(response.getData());
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }

                @Override
                public void ProvinceFailure(@NotNull BaseResponse response) {
                    try {
                        ToastUtils.normalShowToast(globalClass, response.getMessage(), 1);
                        globalClass.hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void setAccountType(ArrayList<BioAccountType> response) {
        try {
            nadra_ac_type.setItemForBioAccountType(response);
            accountTypeList = response;
            ((CIFRootActivity) getActivity()).sharedPreferenceManager.setLovBioAccountType(response);
            nadra_ac_type_spinner.setSelection(((CIFRootActivity) getActivity()).globalClass.findSpinnerPositionFromBioAccountTypeIndex(response,
                    ((CIFRootActivity) getActivity()).customerBiometric.getACCOUNTBATNAME()));
            globalClass.hideLoader();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void getAccountType() {
        try {
            lovRequest = new LovRequest();
            lovRequest.setIdentifier(Constants.BIO_ACCOUNT_TYPE_IDENTIEFIER);
            String json=new Gson().toJson(lovRequest);
            hblhrStore.getBioAccountType(RetrofitEnums.URL_HBL, lovRequest, new BioAccountTypeCallBack() {
                @Override
                public void AccountTypSuccess(@NotNull BioAccountTypeResponse response) {
                    try {
                        setAccountType(response.getData());
                        globalClass.hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }

                @Override
                public void AccountTypFailure(@NotNull BaseResponse response) {
                    try {
                        ToastUtils.normalShowToast(globalClass, response.getMessage(), 1);
                        globalClass.hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }


    @SuppressLint("SetTextI18n")
    public void header() {
        try {
            formSectionHeader = fragmentView.findViewById(R.id.formSectionHeader);
            TextView view = formSectionHeader.getTextView();
            viewModel = new ViewModelProvider(requireActivity()).get(SharedCIFViewModel.class);
            viewModel.getTotalSteps().setValue(2);
            viewModel.getCurrentFragmentIndex().setValue(0);
            String txt = getResources().getString(R.string.bio_metric_Information);
            String txt1 = " (1/2 Page)";
            String txt2 = txt + txt1;
            view.setText(GlobalClass.textColor(txt2, txt.length(), txt2.length(), Color.RED));
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void verifyStoragePermisssions(Activity activity) {
        try {
            // Check if we have write permission
            int permission = ActivityCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE);

            if (permission != PackageManager.PERMISSION_GRANTED) {
                // We don't have permission so prompt the user
                ActivityCompat.requestPermissions(
                        activity,
                        PERMISSIONS_STORAGE,
                        REQUEST_EXTERNAL_STORAGE
                );
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void grantPermission() {
        try {
            USBManager.getInstance().initialize(getActivity(), "com.morpho.morphosample.USB_ACTION", true);
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void init() {
        try {
//            grantPermission();
            nadra_ac_type_spinner = nadra_ac_type.getSpinner(R.id.nadraTypeAccountID);
            nadra_provice_spinner = nadra_provice.getSpinner(R.id.nadra_provinceID);
            nadra_fingers_spinner = nadra_fingers.getSpinner(R.id.nadra_fingers);
            numberET = fivMobileNumber.getTextFromEditText(R.id.fivMobileNumber);
            cnicEt = fivCnic.getTextFromEditText(R.id.fivCnic);
            globalClass.edittextTypeCount(numberET, 11, Constants.INPUT_TYPE_NUMBER);
            globalClass.edittextTypeCount(cnicEt, 13, Constants.INPUT_TYPE_NUMBER);
            if (((CIFRootActivity) getActivity()).getBackInt() == 1) {
                fivCnic.setVisibility(View.VISIBLE);
                btGetVerysis.setVisibility(View.GONE);
            } else {
                fivCnic.setVisibility(View.GONE);
                btGetVerysis.setVisibility(View.VISIBLE);
            }
            if (!((CIFRootActivity) getActivity()).customerBiometric.getCONTACTNUMBER().isEmpty()) {
                numberET.setText(((CIFRootActivity) getActivity()).customerBiometric.getCONTACTNUMBER());
            }
            globalClass.setDisbaled(btVerify, false);
            globalClass.setDisbaled(btGetVerysis, false);
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void setFingerForNadra() {
        try {
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    getActivity(), R.layout.view_spinner_item, R.id.text1, fingersList) {
                @Override
                public boolean isEnabled(int position) {
                    if (position == 0) {
                        return false;
                    } else if (countR_Thumb == 2 && position == 1) {
                        return false;
                    } else if (countR_Index == 2 && position == 2) {
                        return false;
                    } else if (countR_Middle == 2 && position == 3) {
                        return false;
                    } else if (countR_Ring == 2 && position == 4) {
                        return false;
                    } else if (countR_Little == 2 && position == 5) {
                        return false;
                    } else if (countL_Thumb == 2 && position == 6) {
                        return false;
                    } else if (countL_Index == 2 && position == 7) {
                        return false;
                    } else if (countL_Middle == 2 && position == 8) {
                        return false;
                    } else if (countL_Ring == 2 && position == 9) {
                        return false;
                    } else if (countL_Little == 2 && position == 10) {
                        return false;
                    } else {
                        return true;
                    }
                }

                @Override
                public View getDropDownView(int position, View convertView,
                                            ViewGroup parent) {
                    View view = super.getDropDownView(position, convertView, parent);
                    TextView tv = (TextView) view.findViewById(R.id.text1);
                    if (position == 0) {
                        tv.setTextColor(Color.GRAY);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countR_Thumb == 2 && position == 1) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countR_Index == 2 && position == 2) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countR_Middle == 2 && position == 3) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countR_Ring == 2 && position == 4) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countR_Little == 2 && position == 5) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countL_Thumb == 2 && position == 6) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countL_Index == 2 && position == 7) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countL_Middle == 2 && position == 8) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countL_Ring == 2 && position == 9) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else if (countL_Little == 2 && position == 10) {
                        tv.setTextColor(Color.GRAY);
                        tv.setPaintFlags(tv.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        view.setBackgroundColor(getResources().getColor(R.color.disabled));
                    } else {
                        tv.setTextColor(getResources().getColor(R.color.green));
                    }

                    return view;
                }
            };
            adapter.setDropDownViewResource(R.layout.view_spinner_finger_list);
            nadra_fingers_spinner.setAdapter(adapter);
            adapter.notifyDataSetChanged();
            nadra_fingers_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                    try {
                        setImageFor(fingerImpressions.get(i));
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> adapterView) {

                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }


    @Override
    public void onClick(View view) {
        try {
            switch (view.getId()) {
                case R.id.ivRefresh: {
                    load();
                }
                case R.id.btVerify:
                    if (validation()) {
                        try {
                            verifyPermission = false;
                            if (nadraRequestCount != 0 && nadraRequestCount >= 0) {
                                if (USBManager.getInstance().scanDevice() > 0 &&
                                        USBManager.getInstance().isDevicesHasPermission()) {
                                    tem_type = "WSQ";
                                    CaptureInfo.getInstance().setCompressionAlgorithm(CompressionAlgorithm.MORPHO_COMPRESS_WSQ);
                                    CaptureInfo.getInstance().setTemplateType(TemplateType.MORPHO_PK_ISO_FMR);
                                    mHandler.postDelayed(new Runnable() {
                                        @Override
                                        public synchronized void run() {
                                            try {
                                                morphoDeviceCapture_wsq(CIFStep1_4_Morpho_fingerprint_6_34.this);
                                            } catch (NoClassDefFoundError e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                                            } catch (UnsatisfiedLinkError e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                                            } catch (NullPointerException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                                            } catch (IllegalArgumentException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                                            } catch (ActivityNotFoundException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                                            } catch (ClassCastException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                                            } catch (ArrayIndexOutOfBoundsException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                                            } catch (IndexOutOfBoundsException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                                            } catch (SecurityException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                                            } catch (IllegalStateException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                                            } catch (OutOfMemoryError e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                                            } catch (RuntimeException e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                                            } catch (Exception e) {
                                                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                                            }
                                        }
                                    }, 500);
                                } else {
                                    if (USBManager.getInstance().scanDevice() == 0)
                                        errorDialog(getString(R.string.biometric_device_connection_error), "Biometric Device Unplug Connection Error");
                                    else {
                                        verifyPermission = true;
                                        deviceInitialize();
                                    }
                                }
                            } else {
                                alertBox("Biometric Response", "8 attempts complete", "All attempts Completed. Verify New CNIC", "133");
                            }
                        } catch (NoClassDefFoundError e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                        } catch (UnsatisfiedLinkError e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                        } catch (NullPointerException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                        } catch (IllegalArgumentException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                        } catch (ActivityNotFoundException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                        } catch (ClassCastException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                        } catch (ArrayIndexOutOfBoundsException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                        } catch (IndexOutOfBoundsException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                        } catch (SecurityException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                        } catch (IllegalStateException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                        } catch (OutOfMemoryError e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                        } catch (RuntimeException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                        } catch (Exception e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                        }
                    }
                    break;
                case R.id.btBack:
                    try {
                        if (((CIFRootActivity) getActivity()).getBackInt() == 2) {
                            if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                                Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_openAccountStep1);
                            }
                        } else if (((CIFRootActivity) getActivity()).getBackInt() == 0) {
                            if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                                Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_CIFStep1_3);
                            }
                        } else {
                            getActivity().finish();
                        }
                        ((CIFRootActivity) getActivity()).init();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                    break;
                case R.id.btCheckMobile: {
                    if (checkNumberValidation()) {
                        checkNumberNadra();
                    }
                    break;
                }
                case R.id.btGetVerysis:
                    try {
                        String docType = ((CIFRootActivity) getActivity()).sharedPreferenceManager.getCustomerInfo().getIDDOCUMENTTYPE();
                        if (docType.equals("01") || docType.equals("02") || docType.equals("04") || docType.equals("09")) {
                            getVerysis();
                        } else {
                            ToastUtils.normalShowToast(getActivity(), "Biometric cannot be proceed with this document type, please select different document type", 1);
                        }
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                    break;
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    private void checkNumberNadra() {
        try {
            globalClass.showDialog(getActivity());
            CheckNumberNadraRequest request = new CheckNumberNadraRequest();
            request.setCHANNELID("2");
            request.setCONTACTNUMBER(numberET.getText().toString().trim());
            request.setIDDOCUMENTNO(((CIFRootActivity) getActivity()).sharedPreferenceManager.getCustomerInfo().getIDDOCUMENTNO());
            request.setTRACKINGID(((CIFRootActivity) getActivity()).aofAccountInfoRequest.getTRACKING_ID());
            request.setUSERID(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getUSERID());
            String json=new Gson().toJson(request);
            hblhrStore.checkNumberNadra(RetrofitEnums.URL_HBL, request, new CheckNumberNadraCallBack() {
                @Override
                public void CheckNumberNadraSuccess(CheckNumberNadraResponse response) {
                    try {
                        globalClass.setEnabled(btVerify, true);
                        globalClass.setEnabled(btGetVerysis, true);
                        ToastUtils.normalShowToast(getActivity(), response.getMessage(), 2);
                        globalClass.hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }

                @Override
                public void CheckNumberNadraFailure(BaseResponse response) {
                    try {
                        ToastUtils.normalShowToast(getActivity(), response.getMessage(), 1);
                        globalClass.hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    private void getVerysis() {
        try {
            globalClass.showDialog(getActivity());
            GetVerysisRequest request = new GetVerysisRequest();
            request.setAddress(Constants.getIP());
            request.setCITIZEN_NUMBER(((CIFRootActivity) getActivity()).sharedPreferenceManager.getCustomerInfo().ID_DOCUMENT_NO);
            request.setMethod(Constants.POST);
            request.setPort(Constants.PORT_NUMBER);
            request.setUSER_ID(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getUSERID()));
            if (((CIFRootActivity) getActivity()).getBackInt() == 2) {
                request.setWORK_FLOW_CODE("2");
                request.setETBNTBFLAG(((CIFRootActivity) getActivity()).aofAccountInfoRequest.getETBNTBFLAG());
            } else {
                request.setWORK_FLOW_CODE("1");
                request.setETBNTBFLAG(((CIFRootActivity) getActivity()).aofAccountInfoRequest.getETBNTBFLAG());
            }
            String json=new Gson().toJson(request);
            hblhrStore.getVerysis(RetrofitEnums.URL_HBL, request, new NadraVerifyCallBack() {
                @Override
                public void NadraVerifySuccess(NadraVerifyResponse response) {
                    try {
                        saveAndNext(response, true);
                        ToastUtils.normalShowToast(getActivity(), getResources().getString(R.string.request_submited), 2);
                        capturing = false;
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }

                @Override
                public void NadraVerifyFailure(BaseResponse response) {
                    try {
                        ToastUtils.normalShowToast(getActivity(), response.getMessage(), 1);
                        globalClass.hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void onBackPress(View view) {
        try {
            view.setFocusableInTouchMode(true);
            view.requestFocus();
            view.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    try {
                        if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                            // handle back button's click listener
                            if (((CIFRootActivity) getActivity()).getBackInt() == 2) {
                                if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                                    Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_openAccountStep1);
                                }
                            } else if (((CIFRootActivity) getActivity()).getBackInt() == 0) {
                                if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                                    Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_CIFStep1_3);
                                }
                            } else {
                                getActivity().finish();
                            }
                            ((CIFRootActivity) getActivity()).init();
                            return true;
                        }
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    } finally {
                        return false;
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void setImageFor(FingerImpression fingerImpression) {
        switch (fingerImpression) {
            case RightThumb: {
                indexOfFinger = "1";
                iv_left.setImageResource(R.drawable.left_hand_thumb);
                iv_Right.setImageResource(R.drawable.right_hand);
                break;
            }
            case RightIndex: {
                indexOfFinger = "2";
                iv_left.setImageResource(R.drawable.left_hand_index_finger);
                iv_Right.setImageResource(R.drawable.right_hand);
                break;
            }
            case RightMiddle: {
                indexOfFinger = "3";
                iv_left.setImageResource(R.drawable.left_hand_middle_finger);
                iv_Right.setImageResource(R.drawable.right_hand);
                break;
            }
            case RightRing: {
                indexOfFinger = "4";
                iv_left.setImageResource(R.drawable.left_hand_ring_finger);
                iv_Right.setImageResource(R.drawable.right_hand);
                break;
            }
            case RightLittle: {
                indexOfFinger = "5";
                iv_left.setImageResource(R.drawable.left_hand_pinky_finger);
                iv_Right.setImageResource(R.drawable.right_hand);
                break;
            }
            case LeftThumb: {
                indexOfFinger = "6";
                iv_left.setImageResource(R.drawable.left_hand);
                iv_Right.setImageResource(R.drawable.right_hand_thumb);
                break;
            }
            case LeftIndex: {
                indexOfFinger = "7";
                iv_left.setImageResource(R.drawable.left_hand);
                iv_Right.setImageResource(R.drawable.right_hand_index_finger);
                break;
            }
            case LeftMiddle: {
                indexOfFinger = "8";
                iv_left.setImageResource(R.drawable.left_hand);
                iv_Right.setImageResource(R.drawable.right_hand_middle_finger);
                break;
            }
            case LeftRing: {
                indexOfFinger = "9";
                iv_left.setImageResource(R.drawable.left_hand);
                iv_Right.setImageResource(R.drawable.right_hand_ring_finger);
                break;
            }
            case LeftLittle: {
                indexOfFinger = "10";
                iv_left.setImageResource(R.drawable.right_hand);
                iv_Right.setImageResource(R.drawable.left_hand_pinky_finger);
                break;
            }
        }
    }

    public void postManualVerysis(String date) {
        try {
            globalClass.showDialog(getActivity());
            ManualVersysisRequest request = new ManualVersysisRequest();
            request.setUSER_ID(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getUSERID()));
            request.setDATETIME(GlobalClass.getCurrentTimeAndDate(Constants.fomratDateTime));
            request.setBRANCH_CODE(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getBRANCHCODE()));
            request.setBRANCH_NAME(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getBRANCHNAME()));
            request.setBRANCH_ADDRESS(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getBRANCHADDRESS()));
            request.setBRANCH_TYPE(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getBRANCHTYPE()));
            request.setROLE(((GlobalClass) getActivity().getApplicationContext()).sharedPreferenceManager.getLoginData().getROLE());
            request.setURLTYPE("");
            if (((CIFRootActivity) getActivity()).getBackInt() == 1) {
                request.setID_DOCUMENT_NO(cnicEt.getText().toString());
            } else {
                request.setID_DOCUMENT_NO(((CIFRootActivity) getActivity()).sharedPreferenceManager.getCustomerInfo().ID_DOCUMENT_NO);
            }
            request.setSERVICE_TYPE("MOBILE");
            request.setSESSION_ID("");
            request.setCONTACT_NUMBER(numberET.getText().toString());
            request.setISSUE_DATE(date);
            request.setBIOTRACKING_ID(Constants.MANUAL_VERYSIS_BIO_TRACKING_ID);
            request.setOLDER_DAYS("0");
            request.setNADRA_VERISYS_CHECK("Y");
            if (nadra_provice_spinner.getSelectedItemPosition() != 0) {// when select there is no item selected "Choose an item"  <--- hint
                request.setPROVINCE_CODE(globalClass.findSpinnerPositionFromProvinceCode(((CIFRootActivity) getActivity()).sharedPreferenceManager.getProvince(), nadra_provice_spinner.getSelectedItem().toString()));
                request.setPROVINCE_DESC(nadra_provice_spinner.getSelectedItem().toString());
            }
            if (TextUtils.isEmpty(((CIFRootActivity) getActivity()).aofAccountInfoRequest.getTRACKING_ID())) {
                request.setTRACKING_ID(String.valueOf(((GlobalClass) getActivity().getApplicationContext()).sharedPreferenceManager.getTrackingID().getBatch().get(0).getItemDescription()));
            } else {
                request.setTRACKING_ID(String.valueOf((((CIFRootActivity) getActivity()).aofAccountInfoRequest.getTRACKING_ID())));
            }
            String json = new Gson().toJson(request);
            hblhrStore.postManualVerysis(RetrofitEnums.URL_HBL, request, new NadraVerifyCallBack() {
                @Override
                public void NadraVerifySuccess(NadraVerifyResponse response) {
                    try {
                        saveAndNext(response, false);
                        ToastUtils.normalShowToast(getActivity(), getResources().getString(R.string.request_submited), 2);
                        capturing = false;
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }

                @Override
                public void NadraVerifyFailure(BaseResponse response) {
                    try {
                        ToastUtils.normalShowToast(getActivity(), response.getMessage(), 1);
                        globalClass.hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void postRequestToNadra(String base64FingerPrint) {
        try {
            if (nadra_ac_type_spinner.getSelectedItemPosition() != 0) {// when select there is no item selected "Choose an item"  <--- hint
                ((CIFRootActivity) getActivity()).customerBiometric.setACCOUNTBATCODE(globalClass.findSpinnerPositionFromBioAccountTypeCode(((CIFRootActivity) getActivity()).sharedPreferenceManager.getBioAccountType(), nadra_ac_type_spinner.getSelectedItem().toString()));
                ((CIFRootActivity) getActivity()).customerBiometric.setACCOUNTBATNAME(nadra_ac_type_spinner.getSelectedItem().toString());
            }
            if (nadra_provice_spinner.getSelectedItemPosition() != 0) {// when select there is no item selected "Choose an item"  <--- hint
                ((CIFRootActivity) getActivity()).customerBiometric.setPROVINCECODE(globalClass.findSpinnerPositionFromProvinceCode(((CIFRootActivity) getActivity()).sharedPreferenceManager.getProvince(), nadra_provice_spinner.getSelectedItem().toString()));
                ((CIFRootActivity) getActivity()).customerBiometric.setPROVINCEDESC(nadra_provice_spinner.getSelectedItem().toString());
            }
            if (nadra_fingers_spinner.getSelectedItemPosition() != 0) {// when select there is no item selected "Choose an item"  <--- hint
                ((CIFRootActivity) getActivity()).customerBiometric.setFINGERINDEX(String.valueOf(indexOfFinger));
            }

            if (((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getApp() == null||TextUtils.isEmpty(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getApp())) {
                if (TextUtils.isEmpty(((CIFRootActivity) getActivity()).aofAccountInfoRequest.getTRACKING_ID())) {
                    ((CIFRootActivity) getActivity()).customerBiometric.setTRACKINGID(String.valueOf(((GlobalClass) getActivity().getApplicationContext()).sharedPreferenceManager.getTrackingID().getBatch().get(0).getItemDescription()));
                } else {
                    ((CIFRootActivity) getActivity()).customerBiometric.setTRACKINGID(String.valueOf((((CIFRootActivity) getActivity()).aofAccountInfoRequest.getTRACKING_ID())));
                }
                if (((CIFRootActivity) getActivity()).getBackInt() == 1) {
                    ((CIFRootActivity) getActivity()).customerBiometric.setCITIZENNUMBER(cnicEt.getText().toString());
                    ((CIFRootActivity) getActivity()).customerBiometric.setID_DOCUMENT_NO(cnicEt.getText().toString());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECITIZENNUMBER(cnicEt.getText().toString());
                    ((CIFRootActivity) getActivity()).customerBiometric.setBIOTRACKINGID(Constants.BIOMETRIC_BIO_BM);
                } else {
                    ((CIFRootActivity) getActivity()).customerBiometric.setCITIZENNUMBER(((CIFRootActivity) getActivity()).sharedPreferenceManager.getCustomerInfo().ID_DOCUMENT_NO);
                    ((CIFRootActivity) getActivity()).customerBiometric.setID_DOCUMENT_NO(((CIFRootActivity) getActivity()).sharedPreferenceManager.getCustomerInfo().ID_DOCUMENT_NO);
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECITIZENNUMBER(((CIFRootActivity) getActivity()).sharedPreferenceManager.getCustomerInfo().ID_DOCUMENT_NO);
                    ((CIFRootActivity) getActivity()).customerBiometric.setBIOTRACKINGID(Constants.BIOMETRIC_BIO_TRACKING_ID);
                }
                ((CIFRootActivity) getActivity()).customerBiometric.setSERVICETYPE("");
            } else {
                ((CIFRootActivity) getActivity()).customerBiometric.setTRACKINGID(Constants.CONSTANT_TRACKINGID);
                ((CIFRootActivity) getActivity()).customerBiometric.setSERVICETYPE("NCR");
                ((CIFRootActivity) getActivity()).customerBiometric.setCITIZENNUMBER(cnicEt.getText().toString());
                ((CIFRootActivity) getActivity()).customerBiometric.setID_DOCUMENT_NO(cnicEt.getText().toString());
                ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECITIZENNUMBER(cnicEt.getText().toString());
                ((CIFRootActivity) getActivity()).customerBiometric.setBIOTRACKINGID(Constants.BIOMETRIC_BIO_BM);
            }
            ((CIFRootActivity) getActivity()).customerBiometric.setSESSIONID(sessiondID);
            ((CIFRootActivity) getActivity()).customerBiometric.setCHANNELID("2");
            ((CIFRootActivity) getActivity()).customerBiometric.setTEMPLATETYPE("WSQ");
            ((CIFRootActivity) getActivity()).customerBiometric.setNADRAVERISYSCHECK("Y");
            ((CIFRootActivity) getActivity()).customerBiometric.setOLDERDAYS("0");
            ((CIFRootActivity) getActivity()).customerBiometric.setMOBILENO(numberET.getText().toString());
            ((CIFRootActivity) getActivity()).customerBiometric.setCONTACTNUMBER(numberET.getText().toString());
            ((CIFRootActivity) getActivity()).customerBiometric.setDATETIME(GlobalClass.getCurrentTimeAndDate(Constants.fomratDateTime));
            ((CIFRootActivity) getActivity()).customerBiometric.setUser_login(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getStringFromSharedPreferences(SharedPreferenceManager.USER_LOGIN)));
            ((CIFRootActivity) getActivity()).customerBiometric.setMOBILEOPERATORCODE(numberET.getText().toString().substring(0, 4));
            if(((GlobalClass) getActivity().getApplicationContext()).sharedPreferenceManager.getLoginData().getROLE()==0) {
                ((CIFRootActivity) getActivity()).customerBiometric.setROLE("");
            }else{
                ((CIFRootActivity) getActivity()).customerBiometric.setROLE(String.valueOf(((GlobalClass) getActivity().getApplicationContext()).sharedPreferenceManager.getLoginData().getROLE()));
            }
            ((CIFRootActivity) getActivity()).customerBiometric.setFINGER_TEMPLATE(base64FingerPrint);
            ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSETRANSACTIONID(((CIFRootActivity) getActivity()).customerBiometric.getTRANSACTIONID());
            ((CIFRootActivity) getActivity()).customerBiometric.setUSERID(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getUSERID()));
            ((CIFRootActivity) getActivity()).customerBiometric.setBRANCHCODE(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getBRANCHCODE()));
            ((CIFRootActivity) getActivity()).customerBiometric.setBRANCHNAME(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getBRANCHNAME()));
            ((CIFRootActivity) getActivity()).customerBiometric.setBRANCHADDRESS(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getBRANCHADDRESS()));
            ((CIFRootActivity) getActivity()).customerBiometric.setBRANCHTYPE(String.valueOf(((CIFRootActivity) getActivity()).sharedPreferenceManager.getLoginData().getBRANCHTYPE()));
            ((CIFRootActivity) getActivity()).customerBiometric.setBIOPURPOSE(new ArrayList<String>());
//        ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEFINGERINDEX();
            /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in CIFRootActivity*/
            ((CIFRootActivity) getActivity()).sharedPreferenceManager.setCustomerBiomatric(((CIFRootActivity) getActivity()).customerBiometric);
            ((CIFRootActivity) getActivity()).sharedPreferenceManager.setAofAccountInfo(((CIFRootActivity) getActivity()).aofAccountInfoRequest);
            String json = new Gson().toJson(((CIFRootActivity) getActivity()).customerBiometric);
            globalClass.showDialog(getActivity());
            Log.i("biometric",json);
            hblhrStore.postNadraVerfication(RetrofitEnums.URL_HBL, ((CIFRootActivity) getActivity()).customerBiometric, new NadraVerifyCallBack() {
                @Override
                public void NadraVerifySuccess(NadraVerifyResponse response) {
                    try {
                        if (indexOfFinger.equals("1")) {
                            countR_Thumb++;
                        } else if (indexOfFinger.equals("2")) {
                            countR_Index++;
                        } else if (indexOfFinger.equals("3")) {
                            countR_Middle++;
                        } else if (indexOfFinger.equals("4")) {
                            countR_Ring++;
                        } else if (indexOfFinger.equals("5")) {
                            countR_Little++;
                        } else if (indexOfFinger.equals("6")) {
                            countL_Thumb++;
                        } else if (indexOfFinger.equals("7")) {
                            countL_Index++;
                        } else if (indexOfFinger.equals("8")) {
                            countL_Middle++;
                        } else if (indexOfFinger.equals("9")) {
                            countL_Ring++;
                        } else if (indexOfFinger.equals("10")) {
                            countL_Little++;
                        }
                        saveAndNext(response, false);
                        ToastUtils.normalShowToast(getActivity(), getResources().getString(R.string.request_submited), 2);
                        capturing = false;
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }

                @Override
                public void NadraVerifyFailure(BaseResponse response) {
                    try {
                        ToastUtils.normalShowToast(getActivity(), response.getMessage(), 1);
                        globalClass.hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void saveAndNext(NadraVerifyResponse response, boolean isGetVerysis) {
        try {
            /*Save some fields of aofAccountInfoRequest in its object, who initiate in CIFRootActivity*/
            ((CIFRootActivity) getActivity()).customerBiometric.setBIOTRACKINGID(response.getData().get(0).getBIOTRACKINGID());
            ((CIFRootActivity) getActivity()).customerBiometric.setFINGER_TEMPLATE("");
//        response.getData().get(0).setRESPONSECODE("121");
            if (response.getData().get(0).getRESPONSECODE().equals("100")) {
                try {
                    attempsError.setVisibility(View.GONE);
                    globalClass.setDisbaled(btVerify, false);
                    globalClass.setDisbaled(nadra_fingers_spinner, false);
                    sessiondID = "";
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSETRANSACTIONID("");
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEDATEOFBIRTH(response.getData().get(0).getRESPONSEDATEOFBIRTH());
                    ((CIFRootActivity) getActivity()).customerBiometric.setDATETIME(response.getData().get(0).getDATETIME());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECARDTYPE(response.getData().get(0).getRESPONSECARDTYPE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECITIZENNUMBER(response.getData().get(0).getCITIZENNUMBER());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEDATEOFBIRTH(response.getData().get(0).getRESPONSEDATEOFBIRTH());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECODE(response.getData().get(0).getRESPONSECODE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEBIRTHPLACE(response.getData().get(0).getRESPONSEBIRTHPLACE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEEXPIRYDATE(response.getData().get(0).getRESPONSEEXPIRYDATE().equalsIgnoreCase("Lifetime") ? "2099-01-01" : response.getData().get(0).getRESPONSEEXPIRYDATE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEFATHERHUSBANDNAME(response.getData().get(0).getRESPONSEFATHERHUSBANDNAME());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEFINGERINDEX(response.getData().get(0).getRESPONSEFINGERINDEX());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEMESSAGE(response.getData().get(0).getRESPONSEMESSAGE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSENAME(response.getData().get(0).getRESPONSENAME());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEPERMANENTADDRESS(response.getData().get(0).getRESPONSEPERMANENTADDRESS());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEPHOTOGRAPH(response.getData().get(0).getRESPONSEPHOTOGRAPH());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEPRESENTADDRESS(response.getData().get(0).getRESPONSEPRESENTADDRESS());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSESESSIONID(response.getData().get(0).getRESPONSESESSIONID());
                    if (isGetVerysis) {
                        ((CIFRootActivity) getActivity()).customerBiometric.setNADRAVERISYSCHECK("N");
                        ((CIFRootActivity) getActivity()).customerBiometric.setOLDERDAYS("");
                    } else {
                        ((CIFRootActivity) getActivity()).customerBiometric.setNADRAVERISYSCHECK("Y");
                        ((CIFRootActivity) getActivity()).customerBiometric.setOLDERDAYS("0");
                    }
                    ((CIFRootActivity) getActivity()).customerBiometric.setFINGERINDEX(response.getData().get(0).getFINGERINDEX());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSE_FATHER_HUSBAND_NAME_ENG(response.getData().get(0).getRESPONSE_FATHER_HUSBAND_NAME_ENG());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSE_PRESENT_ADDRESS_ENG(response.getData().get(0).getRESPONSE_PRESENT_ADDRESS_ENG());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSE_PERMANENT_ADDRESS_ENG(response.getData().get(0).getRESPONSE_PERMANENT_ADDRESS_ENG());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSE_NAME_ENG(response.getData().get(0).getRESPONSE_NAME_ENG());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSE_BIRTH_PLACE_ENG(response.getData().get(0).getRESPONSE_BIRTH_PLACE_ENG());
                    if (!TextUtils.isEmpty(response.getData().get(0).getCONTACTNUMBER())) {
                        numberET.setText(response.getData().get(0).getCONTACTNUMBER());
                        ((CIFRootActivity) getActivity()).customerBiometric.setCONTACTNUMBER(response.getData().get(0).getCONTACTNUMBER());
                    }
                    if (!TextUtils.isEmpty(response.getData().get(0).getACCOUNTBATNAME())) {
                        nadra_ac_type_spinner.setSelection(((CIFRootActivity) getActivity()).globalClass.findSpinnerPositionFromBioAccountTypeIndex(((CIFRootActivity) getActivity()).sharedPreferenceManager.getBioAccountType(),
                                response.getData().get(0).getACCOUNTBATNAME()));
                        ((CIFRootActivity) getActivity()).customerBiometric.setACCOUNTBATNAME(response.getData().get(0).getACCOUNTBATNAME());
                        ((CIFRootActivity) getActivity()).customerBiometric.setACCOUNTBATCODE(response.getData().get(0).getACCOUNTBATCODE());
                    }
                    if (!TextUtils.isEmpty(response.getData().get(0).getPROVINCEDESC())) {
                        nadra_provice_spinner.setSelection(((CIFRootActivity) getActivity()).globalClass.findSpinnerPositionFromProvinceIndex(((CIFRootActivity) getActivity()).sharedPreferenceManager.getProvince(),
                                response.getData().get(0).getPROVINCEDESC()));
                        ((CIFRootActivity) getActivity()).customerBiometric.setPROVINCEDESC(response.getData().get(0).getPROVINCEDESC());
                        ((CIFRootActivity) getActivity()).customerBiometric.setPROVINCECODE(response.getData().get(0).getPROVINCECODE());
                    }
                    /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in CIFRootActivity*/
                    ((CIFRootActivity) getActivity()).sharedPreferenceManager.setCustomerBiomatric(((CIFRootActivity) getActivity()).customerBiometric);
                    ((CIFRootActivity) getActivity()).sharedPreferenceManager.setAofAccountInfo(((CIFRootActivity) getActivity()).aofAccountInfoRequest);
                    ((CIFRootActivity) getActivity()).sharedPreferenceManager.setNadraResponseData(((CIFRootActivity) getActivity()).customerBiometric);
                    /*setViewModel Data*/
//                cifViewModel.setMobileNumber(number.getText().toString().trim());

                    nadra_provice_spinner.setSelection(((CIFRootActivity) getActivity()).globalClass.
                            findSpinnerPositionFromProvinceIndex(((CIFRootActivity) getActivity()).sharedPreferenceManager.getProvince(),
                                    response.getData().get(0).getPROVINCEDESC()));
                    nadra_ac_type_spinner.setSelection(((CIFRootActivity) getActivity()).globalClass.
                            findSpinnerPositionFromBioAccountTypeIndex(((CIFRootActivity) getActivity()).sharedPreferenceManager.getBioAccountType(),
                                    response.getData().get(0).getACCOUNTBATNAME()));
                    if (!response.getData().get(0).MOBILE_NO.isEmpty() || response.getData().get(0).MOBILE_NO != null) {
                        numberET.setText(response.getData().get(0).MOBILE_NO);
                    }
                    /*Next page to set nadra biometric response data and screen changed.*/
                    if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                        Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_CIFStep1_5);
                    }
                    ((CIFRootActivity) getActivity()).init();
                    ((CIFRootActivity) getActivity()).recyclerViewSetup();
               /* globalClass.alertBox(getActivity(), "Congratulation!!!", response.getMessage(), "Ok", new setOnitemClickListner() {
                    @Override
                    public void onClick(DialogInterface view, int i) {

                        view.dismiss();
                    }
                });*/

                } catch (Exception ex) {
                    reset();
                }
            } else if (response.getData().get(0).getRESPONSECODE().equals("122") || response.getData().get(0).getRESPONSECODE().equals("121") || response.getData().get(0).getRESPONSECODE().equals("118")) {
                try {
                    ((CIFRootActivity) getActivity()).customerBiometric.setNADRAVERISYSCHECK("");
                    ((CIFRootActivity) getActivity()).customerBiometric.setOLDERDAYS("");
                    if (!fingerRetry) {
                        reset();
                        sessiondID = response.getData().get(0).getRESPONSESESSIONID();
                        ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSETRANSACTIONID(response.getData().get(0).getTRANSACTIONID());
//                    ((CIFRootActivity) getActivity()).customerBiometric.setTRANSACTIONID(response.getData().get(0).getTRANSACTIONID());
                    }
                    ((CIFRootActivity) getActivity()).customerBiometric.setTRANSACTIONID(response.getData().get(0).getTRANSACTIONID());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSESESSIONID(response.getData().get(0).getRESPONSESESSIONID());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECODE(response.getData().get(0).getRESPONSECODE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEMESSAGE(response.getData().get(0).getRESPONSEMESSAGE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECITIZENNUMBER(response.getData().get(0).getRESPONSECITIZENNUMBER());
                    fingerAdjustment(response.getData().get(0).getRESPONSEFINGERINDEX());
                    fingerRetry = true;
                    if (nadraRequestCount <= 9 && nadraRequestCount >= 0) {
                        nadraRequestCount--;
                        attempsError.setVisibility(View.VISIBLE);
                        attempsError.setText(nadraRequestCount + "/8 Attempts remaining for biometric");
                    } else {
                        attempsError.setVisibility(View.GONE);
                    }
                    if (nadraRequestCount == 0) {//manual verisys disbaled
                        alertBox("Biometric Response", "8 attempts have completed", "All attempts Completed. Verify New CNIC", response.getData().get(0).getRESPONSECODE());
                    /*AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setCancelable(false);
                    builder.setTitle("Biometric Verification Failed");
                    builder.setMessage("Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\nYou have attempts 8 times finger print & All attempts completed.\nDo you want to use manual verysis?");
                    builder.setPositiveButton("YES", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            manualVerysisAlert("MANUAL VERYSIS", "", "");
                            dialogInterface.dismiss();
                        }
                    });
                    builder.setNegativeButton("NO", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            if (((CIFRootActivity) getActivity()).getBackInt() == 2) {
                                if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                                    Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_openAccountStep1);
                                }
                            } else if (((CIFRootActivity) getActivity()).getBackInt() == 0) {
                                if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                                    Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_CIFStep1_3);
                                }
                            } else {
                                getActivity().finish();
                            }
                            dialogInterface.dismiss();
                        }
                    });
                    AlertDialog alert = builder.create();
                    alert.show();*/
                    } else {
                        if (countR_Thumb == 2 && indexOfFinger.equals("1")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Right Thumb Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countR_Index == 2 && indexOfFinger.equals("2")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Right Index Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countR_Middle == 2 && indexOfFinger.equals("3")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Right Middle Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countR_Ring == 2 && indexOfFinger.equals("4")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Right Ring Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countR_Little == 2 && indexOfFinger.equals("5")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Right Little Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countL_Thumb == 2 && indexOfFinger.equals("6")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Left Thumb exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countL_Index == 2 && indexOfFinger.equals("7")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Left Index Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countL_Middle == 2 && indexOfFinger.equals("8")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Left Middle Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countL_Ring == 2 && indexOfFinger.equals("9")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Left Ring Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else if (countL_Little == 2 && indexOfFinger.equals("10")) {
                            alertBox("Biometric Response", "SELECTED FINGER BLOCKED", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nFinger Index : Left Little Finger exceed retry other fingers..." +
                                    "\nYou cannot attempt selected finger more than twice.." + "\n\n Total attempts remaining " + nadraRequestCount, "121");
                        } else {
                            alertBox("Biometric Response", "", "Message: " + response.getData().get(0).getRESPONSEMESSAGE() + "\n\nTotal attempts remaining " + nadraRequestCount, "121");
                        }
                    }
                    String json = new Gson().toJson(((CIFRootActivity) getActivity()).customerBiometric);
                } catch (Exception ex) {
                    reset();
                }
            } else {
                try {
                    attempsError.setVisibility(View.GONE);
                    ((CIFRootActivity) getActivity()).customerBiometric.setNADRAVERISYSCHECK("");
                    ((CIFRootActivity) getActivity()).customerBiometric.setOLDERDAYS("");
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECODE(response.getData().get(0).getRESPONSECODE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSEMESSAGE(response.getData().get(0).getRESPONSEMESSAGE());
                    ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSECITIZENNUMBER(response.getData().get(0).getRESPONSECITIZENNUMBER());
                    nadraErrorCode = response.getData().get(0).getRESPONSECODE();
                    nadraErrorMessage = response.getData().get(0).getRESPONSEMESSAGE();
                    nadraActive = true;
                    alertBox("Biometric Response", "Unsuccessful " + "Code: " + response.getData().get(0).getRESPONSECODE(), "Message: " + response.getData().get(0).getRESPONSEMESSAGE(), response.getData().get(0).getRESPONSECODE());
                } catch (Exception ex) {
                    reset();
                }
            }
            setFingerForNadra();
            globalClass.hideLoader();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    private void fingerAdjustment(RESPONSEFINGERINDEX test) {
        try {
            for (int j = 1; j <= indexOfNadra.length; j++) {
                for (String x : test.getFINGER()) {
                    if (j == 1 && j == Integer.parseInt(x)) {
                        finger1 = 1;
                    } else if (j == 2 && j == Integer.parseInt(x)) {
                        finger2 = 2;
                    } else if (j == 3 && j == Integer.parseInt(x)) {
                        finger3 = 3;
                    } else if (j == 4 && j == Integer.parseInt(x)) {
                        finger4 = 4;
                    } else if (j == 5 && j == Integer.parseInt(x)) {
                        finger5 = 5;
                    } else if (j == 6 && j == Integer.parseInt(x)) {
                        finger6 = 6;
                    } else if (j == 7 && j == Integer.parseInt(x)) {
                        finger7 = 7;
                    } else if (j == 8 && j == Integer.parseInt(x)) {
                        finger8 = 8;
                    } else if (j == 9 && j == Integer.parseInt(x)) {
                        finger9 = 9;
                    } else if (j == 10 && j == Integer.parseInt(x)) {
                        finger10 = 10;
                    }
                }
            }
            if (finger1 != 1) {
                countR_Thumb = 2;
            }
            if (finger2 != 2) {
                countR_Index = 2;
            }
            if (finger3 != 3) {
                countR_Middle = 2;
            }
            if (finger4 != 4) {
                countR_Ring = 2;
            }
            if (finger5 != 5) {
                countR_Little = 2;
            }
            if (finger6 != 6) {
                countL_Thumb = 2;
            }
            if (finger7 != 7) {
                countL_Index = 2;
            }
            if (finger8 != 8) {
                countL_Middle = 2;
            }
            if (finger9 != 9) {
                countL_Ring = 2;
            }
            if (finger10 != 10) {
                countL_Little = 2;
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void reset() {
        try {
            fingerRetry = false;
            indexOfFinger = "0";
            nadraRequestCount = 9;
            countR_Thumb = 0;
            countR_Index = 0;
            countR_Middle = 0;
            countR_Ring = 0;
            countR_Little = 0;
            countL_Thumb = 0;
            countL_Index = 0;
            countL_Middle = 0;
            countL_Ring = 0;
            countL_Little = 0;
            finger1 = 0;
            finger2 = 0;
            finger3 = 0;
            finger4 = 0;
            finger5 = 0;
            finger6 = 0;
            finger7 = 0;
            finger8 = 0;
            finger9 = 0;
            finger10 = 0;
            sessiondID = "";
            ((CIFRootActivity) getActivity()).customerBiometric.setTRANSACTIONID("");
            ((CIFRootActivity) getActivity()).customerBiometric.setRESPONSETRANSACTIONID("");
            ((CIFRootActivity) getActivity()).customerBiometric.setSESSIONID("");
            ((CIFRootActivity) getActivity()).sharedPreferenceManager.setCustomerBiomatric(((CIFRootActivity) getActivity()).customerBiometric);
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public boolean checkNumberValidation() {
        String number = numberET.getText().toString().trim();
        boolean check = false;
        if (TextUtils.isEmpty(number)) {
            numberET.setError(getResources().getString(R.string.please_enter_mobile_number));
            numberET.requestFocus();
            check = false;
        } else if (!number.startsWith("03")) {
            numberET.setError(getResources().getString(R.string.please_enter_valid_phone_number_2));
            numberET.requestFocus();
            check = false;
        } else if (number.length() < 11) {
            numberET.setError(getResources().getString(R.string.please_enter_valid_phone_number_3));
            numberET.requestFocus();
            check = false;
        }else{
            check=true;
        }
        return check;
    }

    public boolean validation() {
        boolean check = false;
        try {
            String number = numberET.getText().toString().trim();
            String cnic = cnicEt.getText().toString().trim();
            ((CIFRootActivity) getActivity()).mobileNumber = number;
            if (((CIFRootActivity) getActivity()).getBackInt() == 1 && TextUtils.isEmpty(cnic)) {
                cnicEt.setError(getResources().getString(R.string.pleaseEnterCnicNumber));
                cnicEt.requestFocus();
                check = false;
            } else if (((CIFRootActivity) getActivity()).getBackInt() == 1 && cnic.length() < 13) {
                cnicEt.setError(getResources().getString(R.string.pleaseEnterValidCNICNumberErr));
                cnicEt.requestFocus();
                check = false;
            } else if (TextUtils.isEmpty(number)) {
                numberET.setError(getResources().getString(R.string.please_enter_mobile_number));
                numberET.requestFocus();
                check = false;
            } else if (!number.startsWith("03")) {
                numberET.setError(getResources().getString(R.string.please_enter_valid_phone_number_2));
                numberET.requestFocus();
                check = false;
            } else if (number.length() < 11) {
                numberET.setError(getResources().getString(R.string.please_enter_valid_phone_number_3));
                numberET.requestFocus();
                check = false;
            } else if (nadra_ac_type_spinner.getSelectedItemPosition() == 0) {
                ToastUtils.normalShowToast(getActivity(), getResources().getString(R.string.please_your_select_account_type), 1);
                check = false;
            } else if (nadra_provice_spinner.getSelectedItemPosition() == 0) {
                ToastUtils.normalShowToast(getActivity(), getResources().getString(R.string.please_select_province), 1);
                check = false;
            } else if (nadra_fingers_spinner.getSelectedItemPosition() == 0) {
                ToastUtils.normalShowToast(getActivity(), getResources().getString(R.string.please_select_an_finger), 1);
                check = false;
            } else {
                check = true;
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        } finally {
            return check;
        }
    }

    public void alertBox(String title, String subTitle, String msg, String code) {
        try {
            alertDialogBuilder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.popup_error_box, null);
            alertDialogBuilder.setView(view);
            alertDialogBuilder.setCancelable(false);
            dialog = alertDialogBuilder.create();
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            toolbar.setTitle(title);
            toolbar.setSubtitle(subTitle);
            toolbar.setTitleTextColor(getActivity().getResources().getColor(R.color.colorWhite));
            toolbar.setSubtitleTextColor(getActivity().getResources().getColor(R.color.colorWhite));
            error_box1 = view.findViewById(R.id.error1);
            error_box1.setText(msg);
            error_box2 = view.findViewById(R.id.error2);
            error_box2.setVisibility(View.GONE);
            error_box3 = view.findViewById(R.id.error3);
            error_box4 = view.findViewById(R.id.error4);
            cancel_action = view.findViewById(R.id.cancel_action);
            cancel_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        dialog.dismiss();
                        if (!code.equals("121")) {
                            if (((CIFRootActivity) getActivity()).getBackInt() == 2) {
                                if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                                    Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_openAccountStep1);
                                }
                            } else if (((CIFRootActivity) getActivity()).getBackInt() == 0) {
                                if (Navigation.findNavController(fragmentView).getCurrentDestination().getId() == R.id.CIFStep1_4) {
                                    Navigation.findNavController(fragmentView).navigate(R.id.action_CIFStep1_4_to_CIFStep1_3);
                                }
                            } else {
                                getActivity().finish();
                            }
                        }
                        hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
            dialog.show();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void manualVerysisAlert(String title, String subTitle, String msg) {
        try {
            alertDialogBuilderMV = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.popup_manual_verysis, null);
            alertDialogBuilderMV.setView(view);
            alertDialogBuilderMV.setCancelable(false);
            dialogMV = alertDialogBuilderMV.create();
            fivIssueDateTV = view.findViewById(R.id.fivIssueDateVerysis);
            Toolbar toolbar = view.findViewById(R.id.toolbar);
            toolbar.setTitle(title);
            toolbar.setSubtitle(subTitle);
            toolbar.setTitleTextColor(getActivity().getResources().getColor(R.color.colorWhite));
            toolbar.setSubtitleTextColor(getActivity().getResources().getColor(R.color.colorWhite));
            done_action = view.findViewById(R.id.done_action);
            cancel_action = view.findViewById(R.id.cancel_action);
            fivIssueDateTV.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.DialogTheme,
                                datep, myCalendar
                                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                                myCalendar.get(Calendar.DAY_OF_MONTH));
                        datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                        datePickerDialog.show();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
            cancel_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        dialogMV.dismiss();
                        hideLoader();
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
            done_action.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    try {
                        if (!fivIssueDateTV.getText().toString().equals(getResources().getString(R.string.date_picker))) {
                            postManualVerysis(fivIssueDateTV.getText().toString());
                            dialogMV.dismiss();
                        } else {
                            ToastUtils.normalShowToast(getActivity(), "Please select issue date", 1);
                        }
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });

            dialogMV.show();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    DatePickerDialog.OnDateSetListener datep = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear,
                              int dayOfMonth) {
            try {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = Constants.fomratDate2;
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                fivIssueDateTV.setText(sdf.format(myCalendar.getTime()));
            } catch (NoClassDefFoundError e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
            } catch (UnsatisfiedLinkError e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
            } catch (NullPointerException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
            } catch (IllegalArgumentException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
            } catch (ActivityNotFoundException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
            } catch (ClassCastException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
            } catch (ArrayIndexOutOfBoundsException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
            } catch (IndexOutOfBoundsException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
            } catch (SecurityException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
            } catch (IllegalStateException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
            } catch (OutOfMemoryError e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
            } catch (RuntimeException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
            } catch (Exception e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
            }
        }
    };

    @Override
    public void onPause() {
        try {
            super.onPause();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    @Override
    public void onResume() {
        try {
            super.onResume();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void initSecurityLevel() {
        try {
//        CaptureInfo.getInstance().setSecurityLevel(SecurityLevel.FFD_SECURITY_LEVEL_CRITICAL_HOST);
//            morphoDevice.setSecurityLevel(SecurityLevel.FFD_SECURITY_LEVEL_CRITICAL_HOST);
            morphoDevice.setSecurityLevel(SecurityLevel.FFD_SECURITY_LEVEL_HIGH_HOST);
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void getAndWriteFFDLogs() {
        try {
            String ffdLogs = morphoDevice.getFFDLogs();
            if (ffdLogs.contains("Fake finger")) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            ToastUtils.normalShowToast(getActivity(), "Fake Finger", 1);
                            verifyDialog.dismiss();
                        } catch (NoClassDefFoundError e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                        } catch (UnsatisfiedLinkError e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                        } catch (NullPointerException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                        } catch (IllegalArgumentException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                        } catch (ActivityNotFoundException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                        } catch (ClassCastException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                        } catch (ArrayIndexOutOfBoundsException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                        } catch (IndexOutOfBoundsException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                        } catch (SecurityException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                        } catch (IllegalStateException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                        } catch (OutOfMemoryError e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                        } catch (RuntimeException e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                        } catch (Exception e) {
                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                        }
                    }
                });
            } else {
                verifyDialog.dismiss();
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    //TODO New Morpho
    private void morphoInit() {
        try {
//            registerRequestListener();
            requestPermission();
            powerOnorOff(getActivity(), true);
            registerListener();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    private void requestPermission() {
        try {
            if (Build.VERSION.SDK_INT >= 23) {
                int checkpermission = ((CIFRootActivity) getActivity()).checkSelfPermission(Manifest.permission.CAMERA);
                if (checkpermission != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]{
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE
                    }, 1);
                } else {
                    grantPermissions = true;
                    deviceInitialize();
                }
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        try {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
                grantPermissions = true;
                deviceInitialize();
            } else {
                ((CIFRootActivity) getActivity()).finish();
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void morphoDeviceCapture_wsq(final Observer observer) {
        try {
            Thread commandThread = (new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        final TemplateList templateList = new TemplateList();
                        int timeout = 0;
                        int acquisitionThreshold = 30;
                        int advancedSecurityLevelsRequired = 4;
                        int nbFinger = 1;
                        TemplateType templateType;
                        TemplateFVPType templateFVPType = TemplateFVPType.MORPHO_NO_PK_FVP;
                        int maxSizeTemplate = 255;
                        EnrollmentType enrollType = EnrollmentType.ONE_ACQUISITIONS;
                        LatentDetection latentDetection = LatentDetection.LATENT_DETECT_ENABLE;
                        Coder coderChoice = Coder.fromString("Default");
                        CompressionAlgorithm compressAlgo;
                        int compressRate = 0;
                        int detectModeChoice = 18;
                        int callbackCmd = 199;
                        int ret = morphoDevice.setStrategyAcquisitionMode(StrategyAcquisitionMode.fromString("Expert(Default)"));
                        Log.d(TAG, "ret setStrategyAcquisitionMode:" + ret);
                        templateType = CaptureInfo.getInstance().getTemplateType();
                        if (templateType != TemplateType.MORPHO_NO_PK_FP) {
                            if (templateType == TemplateType.MORPHO_PK_MAT || templateType == TemplateType.MORPHO_PK_MAT_NORM || templateType == TemplateType.MORPHO_PK_PKLITE) {
                                maxSizeTemplate = 1;
                            } else {
                                maxSizeTemplate = 255;
                            }
                        } else {
                            templateType = TemplateType.MORPHO_PK_COMP;
                            maxSizeTemplate = 255;
                        }

                        compressAlgo = CaptureInfo.getInstance().getCompressionAlgorithm();
                        if (!compressAlgo.equals(CompressionAlgorithm.NO_IMAGE)) {
                            templateList.setActivateFullImageRetrieving(true);
                            if (compressAlgo.equals(CompressionAlgorithm.MORPHO_COMPRESS_WSQ)) {
                                compressRate = 15;
                            }
                        }

                        if (ret == ErrorCodes.MORPHO_OK) {
                            mHandler.post(new Runnable() {
                                @Override
                                public synchronized void run() {
                                    try {

                                        openFingerDialog();
                                    } catch (NoClassDefFoundError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                                    } catch (UnsatisfiedLinkError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                                    } catch (NullPointerException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                                    } catch (IllegalArgumentException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                                    } catch (ActivityNotFoundException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                                    } catch (ClassCastException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                                    } catch (ArrayIndexOutOfBoundsException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                                    } catch (IndexOutOfBoundsException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                                    } catch (SecurityException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                                    } catch (IllegalStateException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                                    } catch (OutOfMemoryError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                                    } catch (RuntimeException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                                    } catch (Exception e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                                    }
                                }
                            });

                            ret = morphoDevice.capture(timeout,
                                    acquisitionThreshold,
                                    advancedSecurityLevelsRequired,
                                    nbFinger,
                                    templateType,
                                    templateFVPType,
                                    maxSizeTemplate,
                                    enrollType,
                                    latentDetection,
                                    coderChoice,
                                    detectModeChoice,
                                    compressAlgo,
                                    compressRate,
                                    templateList,
                                    callbackCmd,
                                    observer);

                            int finalRet1 = ret;
                            mHandler.post(new Runnable() {
                                @Override
                                public synchronized void run() {
                                    try {
                                        Log.d("EveryTime", "  1");
                                        if (finalRet1 != 0) {
                                            getAndWriteFFDLogs();
                                        }
                                    } catch (NoClassDefFoundError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                                    } catch (UnsatisfiedLinkError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                                    } catch (NullPointerException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                                    } catch (IllegalArgumentException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                                    } catch (ActivityNotFoundException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                                    } catch (ClassCastException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                                    } catch (ArrayIndexOutOfBoundsException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                                    } catch (IndexOutOfBoundsException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                                    } catch (SecurityException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                                    } catch (IllegalStateException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                                    } catch (OutOfMemoryError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                                    } catch (RuntimeException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                                    } catch (Exception e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                                    }
                                }
                            });
                            Log.d(TAG, "retCapture:" + ret);
                        }

                        if (ErrorCodes.MORPHO_OK == ret) {
                            MorphoImage morphoImage = templateList.getImage(0);
                            if (morphoImage != null) {
                                MorphoImageHeader mImageHeader = morphoImage.getMorphoImageHeader();
                                int imageColumnNumber = mImageHeader.getNbColumn();
                                int imageRowNumber = mImageHeader.getNbRow();

                                Log.d(TAG, "CompressionAlgorithm:" + morphoImage.getCompressionAlgorithm() + " mImageHeader: " + imageColumnNumber + "  " + imageRowNumber + " " + mImageHeader.getCompressionRatio()
                                        + " " + mImageHeader.getNbBitsPerPixel());
                            }

                            mHandler.post(new Runnable() {
                                @Override
                                public synchronized void run() {
                                    try {
                                        Log.d("EveryTime", "  3");
                                        int count = Uitls.GetWSQMinutiaeCount(templateList);

                                    } catch (NoClassDefFoundError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                                    } catch (UnsatisfiedLinkError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                                    } catch (NullPointerException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                                    } catch (IllegalArgumentException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                                    } catch (ActivityNotFoundException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                                    } catch (ClassCastException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                                    } catch (ArrayIndexOutOfBoundsException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                                    } catch (IndexOutOfBoundsException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                                    } catch (SecurityException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                                    } catch (IllegalStateException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                                    } catch (OutOfMemoryError e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                                    } catch (RuntimeException e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                                    } catch (Exception e) {
                                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                                    }
                                }
                            });
                            // Image extraction
                            try {
                                if (templateList.isActivateFullImageRetrieving()) {
                                    byte[] data = null;

                                    if (compressAlgo.equals(CompressionAlgorithm.MORPHO_COMPRESS_WSQ)) {
                                        //Case of WSQ
                                        data = templateList.getImage(0).getCompressedImage();
                                        String base64format = Base64.encodeToString(data, Base64.DEFAULT);
                                        Log.e(TAG, base64format);
                                        ((CIFRootActivity) getActivity()).runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {

                                                try {
                                                    Log.d("EveryTime", "  4");
                                                    if (base64format != null) {
                                                        postRequestToNadra(base64format);
                                                        globalClass.setDisbaled(cnicEt, false);
                                                        globalClass.setDisbaled(numberET, false);
                                                        globalClass.setDisbaled(nadra_provice_spinner, false);
                                                        globalClass.setDisbaled(nadra_ac_type_spinner, false);
                                                        verifyDialog.dismiss();
                                                    }
                                                } catch (NoClassDefFoundError e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                                                } catch (UnsatisfiedLinkError e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                                                } catch (NullPointerException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                                                } catch (IllegalArgumentException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                                                } catch (ActivityNotFoundException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                                                } catch (ClassCastException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                                                } catch (ArrayIndexOutOfBoundsException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                                                } catch (IndexOutOfBoundsException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                                                } catch (SecurityException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                                                } catch (IllegalStateException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                                                } catch (OutOfMemoryError e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                                                } catch (RuntimeException e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                                                } catch (Exception e) {
                                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                                                }
                                            }
                                        });
                                    } else {
                                        //Case of RAW Image
                                        data = templateList.getImage(0).getImage();
                                        String base64format = Base64.encodeToString(data, Base64.DEFAULT);
                                        Log.e("RAW Data", base64format);

                                    }

                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            try {

                                int NFIQvalue = -1;
                                if (image != null) {
                                    NFIQvalue = Uitls.GetWSQNFIQCount(templateList);
                                }
                                final int finalNFIQvalue = NFIQvalue;
                                Log.e("NFIQ level:", finalNFIQvalue + "");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } else {
                            if (ret != ErrorCodes.MORPHOERR_CMDE_ABORTED) {
                                final String alertMessage = "";
                                final int finalRet = ret;
                                mHandler.post(new Runnable() {
                                    @Override
                                    public synchronized void run() {
                                        try {
                                            alert(finalRet, 0, "Capture", alertMessage, false);
                                        } catch (NoClassDefFoundError e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                                        } catch (UnsatisfiedLinkError e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                                        } catch (NullPointerException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                                        } catch (IllegalArgumentException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                                        } catch (ActivityNotFoundException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                                        } catch (ClassCastException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                                        } catch (ArrayIndexOutOfBoundsException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                                        } catch (IndexOutOfBoundsException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                                        } catch (SecurityException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                                        } catch (IllegalStateException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                                        } catch (OutOfMemoryError e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                                        } catch (RuntimeException e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                                        } catch (Exception e) {
                                            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                                        }
                                    }
                                });
                            }
                        }
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }

            }));

            commandThread.start();
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    private void showMinutiaCount(TemplateList templateList) {
        byte[] template_data = null;
        template_data = templateList.getTemplate(0).getData();
        String base64format = Base64.encodeToString(template_data, Base64.DEFAULT);
//        tv_templete_data.setText(tem_type + " ::" + base64format);
        int minutiaCount = Uitls.GetTemplateMinutiaeNumBer(templateList);
//        tv_show.setText("MinutiaCount is : " + minutiaCount);
    }

    public void powerOnorOff(@NotNull Context context, boolean on) {
        try {
            Intent intent = new Intent();
            intent.setAction("mtk.intent.ACTION_FINGER_CONFIG");
            intent.putExtra("enable", on);
            context.sendBroadcast(intent);
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        try {
            CallbackMessage message = (CallbackMessage) arg;
            int type = message.getMessageType();
            switch (type) {
                case 1:
                    break;
                case 2:
                    try {
                        image = (byte[]) message.getMessage();

                        MorphoImage morphoImage = MorphoImage.getMorphoImageFromLive(image);

                        int imageRowNumber = morphoImage.getMorphoImageHeader().getNbRow();
                        int imageColumnNumber = morphoImage.getMorphoImageHeader().getNbColumn();
                        imageBmp = Bitmap.createBitmap(imageColumnNumber, imageRowNumber, Bitmap.Config.ALPHA_8);

                        imageBmp.copyPixelsFromBuffer(ByteBuffer.wrap(morphoImage.getImage(), 0, morphoImage.getImage().length));

                        Matrix matrix = new Matrix();

                        matrix.postRotate(180);

                        Bitmap scaledBitmap = Bitmap.createScaledBitmap(imageBmp, imageColumnNumber, imageRowNumber, true);

                        Bitmap rotatedBitmap = Bitmap.createBitmap(scaledBitmap, 0, 0, scaledBitmap.getWidth(), scaledBitmap.getHeight(), matrix, true);


                        mHandler.post(new Runnable() {
                            @Override
                            public synchronized void run() {
                                try {
                                    updateImage(rotatedBitmap);
                                } catch (NoClassDefFoundError e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                                } catch (UnsatisfiedLinkError e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                                } catch (NullPointerException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                                } catch (IllegalArgumentException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                                } catch (ActivityNotFoundException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                                } catch (ClassCastException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                                } catch (ArrayIndexOutOfBoundsException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                                } catch (IndexOutOfBoundsException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                                } catch (SecurityException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                                } catch (IllegalStateException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                                } catch (OutOfMemoryError e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                                } catch (RuntimeException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                                } catch (Exception e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                                }
                            }
                        });
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                case 3:
                    try {
//                        final Integer quality = (Integer) message.getMessage();
                        mHandler.post(new Runnable() {
                            @Override
                            public synchronized void run() {
                                try {
                                    updateImageBackground(R.id.imageView, 0);
                                } catch (NoClassDefFoundError e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                                } catch (UnsatisfiedLinkError e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                                } catch (NullPointerException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                                } catch (IllegalArgumentException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                                } catch (ActivityNotFoundException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                                } catch (ClassCastException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                                } catch (ArrayIndexOutOfBoundsException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                                } catch (IndexOutOfBoundsException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                                } catch (SecurityException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                                } catch (IllegalStateException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                                } catch (OutOfMemoryError e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                                } catch (RuntimeException e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                                } catch (Exception e) {
                                    ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                                    SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                                }
                            }
                        });
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                    break;
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    @SuppressLint("NewApi")
    private void updateImageBackground(int id, int level) {
//        ImageView iv = (ImageView) findViewById(id);
//        Drawable drawable = null;
//        if (level <= 25) {
//            drawable = getResources().getDrawable(R.drawable.red_border);
//        } else if (level <= 75) {
//            drawable = getResources().getDrawable(R.drawable.yellow_border);
//        } else {
//            drawable = getResources().getDrawable(R.drawable.green_border);
//        }
//        if (iv != null) {
//            iv.setBackground(drawable);
//        }
    }

    private void updateImage(Bitmap bitmap) {
        try {
            bitmapFingerPrint = bitmap;
            fingerPrintImg.setVisibility(View.VISIBLE);
            fingerPrintImg.setImageBitmap(bitmap);
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    private class DeviceBroadcastReceiver extends BroadcastReceiver {
        private String action = null;

        @SuppressLint("LongLogTag")
        @Override
        public void onReceive(Context context, Intent intent) {
            try {
                action = intent.getAction();
                if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {
                    UsbDevice newDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (newDevice != null && isFingerDevice(newDevice)) {
                        deviceInitialize();
                    }
                } else if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {
                    UsbDevice oldDevice = (UsbDevice) intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (oldDevice != null && isFingerDevice(oldDevice)) {

                    }
                } else if (USBManager.ACTION_USB_PERMISSION.equals(action)) {
                    if (USBManager.getInstance().isDevicesHasPermission()) {
                        Log.e("PermissionBroadcastReceiver", "Permission Granted");
                        openDevice();
                        if (verifyPermission) {
                            btVerify.callOnClick();
                        }
                    } else {
                        verifyPermission = false;
                        Log.e("PermissionBroadcastReceiver", "Permission Denied");
                        if (USBManager.getInstance().scanDevice() == 0)
                            errorDialog(getString(R.string.biometric_device_connection_error), "Biometric Device Unplug Connection Error");
                        else
                            errorDialog(getString(R.string.biometric_device_permission_error), "Biometric Device Permission Required");

                    }
                }
            } catch (NoClassDefFoundError e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
            } catch (UnsatisfiedLinkError e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
            } catch (NullPointerException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
            } catch (IllegalArgumentException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
            } catch (ActivityNotFoundException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
            } catch (ClassCastException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
            } catch (ArrayIndexOutOfBoundsException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
            } catch (IndexOutOfBoundsException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
            } catch (SecurityException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
            } catch (IllegalStateException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
            } catch (OutOfMemoryError e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
            } catch (RuntimeException e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
            } catch (Exception e) {
                ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
            }
        }

    }

    private boolean isFingerDevice(UsbDevice device) {
        boolean check = false;
        try {
            int vid = device.getVendorId();
            Log.d(TAG, "vid :" + vid);//8797
            if (vid == 8797) {
                check = true;
            } else {
                check = false;
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        } finally {
            return check;
        }
    }

    private void registerListener() {
        try {
            IntentFilter filter = new IntentFilter();
            filter.addAction(USBManager.ACTION_USB_PERMISSION);
            filter.addAction(UsbManager.ACTION_USB_DEVICE_ATTACHED);
            filter.addAction(UsbManager.ACTION_USB_DEVICE_DETACHED);
            mDeviceReceiver = new DeviceBroadcastReceiver();
            ((CIFRootActivity) getActivity()).registerReceiver(mDeviceReceiver, filter);

        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    //wait for power on success and delay device Initialize
    private void deviceInitialize() {
        try {
            morphoDevice = new MorphoDevice();
            /*To set security level of scanner*/
            initSecurityLevel();
            /* initialize usb manager for scanner connection*/
            if (!USBManager.getInstance().isDevicesHasPermission()) {
                USBManager.getInstance().initialize(getActivity(), "com.hbl.bot.USB_ACTION", true);
            } else {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
                openDevice();
//                    }
//                }, 2000);
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void openDevice() {
        try {
            CustomInteger cUsbDevice = new CustomInteger();
            int ret = morphoDevice.initUsbDevicesNameEnum(cUsbDevice);
            Integer nbUsbDevice = new Integer(cUsbDevice.getValueOf());
            Log.d(TAG, "nbUsbDevice :" + nbUsbDevice.intValue());
            if (ret == ErrorCodes.MORPHO_OK) {
                if (nbUsbDevice > 0) {
                    sensorName = morphoDevice.getUsbDeviceName(0);
                    int retOpenDevice = morphoDevice.openUsbDevice(sensorName, 0);
                    Log.d(TAG, "retOpenDevice:" + retOpenDevice);
                    if (retOpenDevice == ErrorCodes.MORPHO_OK) {
                    }
                } /*else {
                    errorDialog(getString(R.string.biometric_device_connection_error), "Biometric Device Unplug Connection Error");
                }*/
            } else {
                errorDialog(getString(R.string.biometric_device_connection_error), "Biometric Device Unplug Connection Error");
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void openFingerDialog() {
        try {
            ((CIFRootActivity) getActivity()).runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        if (!verifyDialog.isShowing()) {
                            toolbar.setSubtitle("Finger: " + fingerImpressions.get(Integer.parseInt(indexOfFinger)));
                            verification_place_txt.setText("Please clear the biometric lense first and then place your"
                                    + fingerImpressions.get(Integer.parseInt(indexOfFinger)) + "finger properly on Biometric Sensor for the fine finger print"
                                    + "\n\nTotal Attempts Left: " + nadraRequestCount);
                            verifyDialog.show();
                        }
                    } catch (NoClassDefFoundError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
                    } catch (UnsatisfiedLinkError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
                    } catch (NullPointerException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
                    } catch (IllegalArgumentException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
                    } catch (ActivityNotFoundException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
                    } catch (ClassCastException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
                    } catch (ArrayIndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
                    } catch (IndexOutOfBoundsException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
                    } catch (SecurityException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
                    } catch (IllegalStateException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
                    } catch (OutOfMemoryError e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
                    } catch (RuntimeException e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
                    } catch (Exception e) {
                        ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
                        SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
                    }
                }
            });
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public void alert(int codeError, int internalError, String title, String message, boolean isMatchingOperation) {
        try {
            String msg;
            if (codeError == 0) {
                msg = getString(R.string.OP_SUCCESS);
            } else {
                String errorInternationalization = convertToInternationalMessage(codeError, internalError, isMatchingOperation);
                msg = getString(R.string.OP_FAILED) + "\n" + errorInternationalization;
                msg += ((message.equalsIgnoreCase("")) ? "" : "\n" + message);
                if (errorInternationalization.contains("Fake")) {
                    ToastUtils.normalShowToast(getActivity(), msg, 1);
                } else {
                    errorDialog(getString(R.string.biometric_device_connection_error), getString(R.string.OP_FAILED));
//                    ToastUtils.normalShowToast(getActivity(), getString(R.string.OP_FAILED) + getString(R.string.biometric_device_connection_error), 1);
                }
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

    public String convertToInternationalMessage(int iCodeError, int iInternalError, boolean isMatchingOperation) {
        switch (iCodeError) {
            case ErrorCodes.MORPHO_OK:
                return getString(R.string.MORPHO_OK);
            case ErrorCodes.MORPHOERR_FFD:
            case ErrorCodes.MORPHOERR_MOIST_FINGER:
            case ErrorCodes.MORPHOERR_MOVED_FINGER:
            case ErrorCodes.MORPHOERR_SATURATED_FINGER:
            case ErrorCodes.MORPHOERR_INVALID_FINGER:
                return isMatchingOperation ? getString(R.string.MORPHOERR_NO_HIT) : getString(R.string.MORPHOERR_LOW_QUALITY_FINGER);
            case ErrorCodes.MORPHOERR_CAPTURE_FAILED:
                return getString(R.string.MORPHOERR_CAPTURE_FAILED);
            default:
                return String.format("Unknown error %d, Internal Error = %d", iCodeError, iInternalError);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (morphoDevice != null) {
                morphoDevice.cancelLiveAcquisition();
                morphoDevice.closeDevice();
                powerOnorOff(((CIFRootActivity) getActivity()), false);
            }
            if (mDeviceReceiver != null) {
                ((CIFRootActivity) getActivity()).unregisterReceiver(mDeviceReceiver);
            }
        } catch (NoClassDefFoundError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NoClassDefFoundError) e));
        } catch (UnsatisfiedLinkError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((UnsatisfiedLinkError) e));
        } catch (NullPointerException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((NullPointerException) e));
        } catch (IllegalArgumentException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalArgumentException) e));
        } catch (ActivityNotFoundException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ActivityNotFoundException) e));
        } catch (ClassCastException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ClassCastException) e));
        } catch (ArrayIndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((ArrayIndexOutOfBoundsException) e));
        } catch (IndexOutOfBoundsException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IndexOutOfBoundsException) e));
        } catch (SecurityException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((SecurityException) e));
        } catch (IllegalStateException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((IllegalStateException) e));
        } catch (OutOfMemoryError e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((OutOfMemoryError) e));
        } catch (RuntimeException e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((RuntimeException) e));
        } catch (Exception e) {
            ToastUtils.normalShowToast(getActivity(), getString(R.string.something_went_wrong), 3);
            SendEmail.sendEmail(((CIFRootActivity) getActivity()), ((Exception) e));
        }
    }

}



