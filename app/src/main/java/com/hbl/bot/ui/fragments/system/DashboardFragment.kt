package com.hbl.bot.ui.fragments.system


import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.Constants
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import kotlinx.android.synthetic.main.dashboard_new.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException


/**
 * A simple [Fragment] subclass.
 */
class DashboardFragment : Fragment(), OnClickListener {
    var cDisparent: Int? = null
    var cInprogress: Int? = null
    var cSuccessfull: Int? = null
    var cDocUpload: Int? = null
    var bundle = Bundle()
    var myView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_dashboard_2, container, false)
            myView = inflater.inflate(R.layout.dashboard_new, container, false)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            (activity as NavigationDrawerActivity).sharedPreferenceManager.getInstance(activity)
            backBtn.setOnClickListener(this)
            btnCIF.setOnClickListener(this)
            btnAccount.setOnClickListener(this)
            when {
                arguments?.getInt(Constants.ACTIVITY_KEY) == Constants.HAW -> {
                    (activity as NavigationDrawerActivity).globalClass?.setDisbaled(btnAccount,false)
                    buttonAccountLayout.visibility = View.GONE
                    OpenCiftxt.setText("Open Account")
                    btnAccount.isEnabled = false
                }
                arguments?.getInt(Constants.ACTIVITY_KEY) == Constants.SPARK -> {
                    buttonAccountLayout.visibility = View.VISIBLE
                    OpenCiftxt.setText("Open CIF")
                    (activity as NavigationDrawerActivity).globalClass?.setDisbaled(btnAccount,false)
                    btnAccount.isEnabled = false
                }
            }

            btnOpenQuickAccount.setOnClickListener(this)
            viewbox2.setOnClickListener(this)
            viewbox1.setOnClickListener(this)
            showRecords.setOnClickListener(this)
            callCountApi()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    private fun callCountApi() {
        (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
        val request = CountRequest()
        request.BRANCH_CODE = GlobalClass.sharedPreferenceManager!!.loginData.getBRANCHCODE()
        request.USER_ID = GlobalClass.sharedPreferenceManager!!.loginData.getUSERID()
        request.USER_ROLE = GlobalClass.sharedPreferenceManager!!.loginData.getROLE()?.toString()
        request.identifier = "getchartdata"
        HBLHRStore.instance?.getCount(RetrofitEnums.URL_HBL, request, object : CountCallBack {
            override fun CountSuccess(response: CountResponse) {
                try {
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                    if (response.data?.get(2)?.get(0)?.value == null) {
                        NoOfCustomer.text = "0"
                    } else {
                        NoOfCustomer.text = response.data?.get(2)?.get(0)?.value.toString()
                    }
                    if (response.data?.get(2)?.get(1)?.value == null) {
                        NoOfMeeting.text = "0"
                    } else {
                        NoOfMeeting.text = response.data?.get(2)?.get(1)?.value.toString()
                    }
                    NoOfdiscrepent.text = response.data?.get(0)?.get(2)?.value.toString()
                    cDisparent = response.data?.get(0)?.get(2)?.value
                    NoOfPending.text = response.data?.get(0)?.get(1)?.value.toString()
                    cInprogress = response.data?.get(0)?.get(1)?.value
                    NoOfDoc.text = response.data?.get(0)?.get(3)?.value.toString()
                    cDocUpload = response.data?.get(0)?.get(3)?.value
                    NoOfSuccessful.text = response.data?.get(1)?.get(0)?.value.toString()
                    cSuccessfull = response.data?.get(1)?.get(0)?.value
                    var TotalCount = response.data?.get(0)?.get(4)?.value
                    counterRading.text = TotalCount.toString()
                    var progress = TotalCount!!.toFloat() / 100.0;
                    counter.progress = progress.toFloat()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as UnsatisfiedLinkError)
                    )
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NullPointerException)
                    )
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NumberFormatException)
                    )
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as InterruptedException)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as FileNotFoundException)
                    )
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassCastException)
                    )
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as TypeCastException)
                    )
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as SecurityException)
                    )
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalStateException)
                    )
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as OutOfMemoryError)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
                }
            }

            override fun CountFailure(response: BaseResponse) {
                Log.d("fail", response.message.toString())
                (activity as NavigationDrawerActivity).globalClass?.hideLoader()

            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.backBtn -> {
                findNavController().navigate(R.id.back_to_welcome_screen)
            }
            R.id.btnCIF -> {
                try {
//                forcely()
                    (activity as NavigationDrawerActivity).globalClass?.clearAllLovs()
                    (activity as NavigationDrawerActivity).globalClass?.clearSubmitAOF()
                    (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
                    GlobalClass.CINC = ""
                    when {
                        arguments?.getInt(Constants.ACTIVITY_KEY) == Constants.SPARK -> {
                            callTrackingID()
                        }
                        arguments?.getInt(Constants.ACTIVITY_KEY) == Constants.HAW -> {
                            callWorkFlow( )
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as UnsatisfiedLinkError)
                    )
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NullPointerException)
                    )
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NumberFormatException)
                    )
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as InterruptedException)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as FileNotFoundException)
                    )
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassCastException)
                    )
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as TypeCastException)
                    )
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as SecurityException)
                    )
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalStateException)
                    )
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as OutOfMemoryError)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
                }
            }
            R.id.btnAccount -> {
//                (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
//                callCIF()
                (activity as NavigationDrawerActivity).globalClass?.clearAllLovs()
                (activity as NavigationDrawerActivity).globalClass?.clearSubmitAOF()
                bundle.putInt(Constants.ACTIVITY_KEY, Constants.ACCOUNT_OPENING_SCREEN)
                findNavController().navigate(R.id.action_nav_dashboard_to_open_account, bundle)
            }
            R.id.btnOpenQuickAccount -> {
                (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
                callTrackingID()
            }
            R.id.viewbox2 -> {
                GlobalClass.MYMEET = 1;
                findNavController().navigate(R.id.action_nav_dashboard_to_callsForMeetingActivity)
            }
            R.id.viewbox1 -> {
                GlobalClass.NEWCUST = 1;
                findNavController().navigate(R.id.action_nav_dashboard_to_callsForMeetingActivity)
            }
            R.id.showRecords -> {
                when {
                    arguments?.getInt(Constants.ACTIVITY_KEY) == Constants.SPARK -> {
                        var bundle=Bundle()
                        bundle.putInt(Constants.ACTIVITY_KEY,Constants.SPARK)
                        findNavController().navigate(R.id.action_nav_dashboard_to_show_records)
                    }
                    arguments?.getInt(Constants.ACTIVITY_KEY) == Constants.HAW -> {
                        var bundle=Bundle()
                        bundle.putInt(Constants.ACTIVITY_KEY,Constants.HAW)
                        findNavController().navigate(R.id.action_nav_dashboard_to_show_haw_records,bundle)
                    }
                }
            }
        }
    }

    private fun forcely() {
        throw RuntimeException("test error")
    }

    fun callTrackingID() {
        var trackingIDRequest = TrackingIDRequest()
        trackingIDRequest?.brcode =
            (activity as NavigationDrawerActivity).sharedPreferenceManager.getLoginData()
                .getBRANCHCODE()
        trackingIDRequest?.brcode =
            (activity as NavigationDrawerActivity).sharedPreferenceManager.getLoginData()
                .getBRANCHCODE()
        var baseResponse = Gson().toJson(trackingIDRequest)
        HBLHRStore.instance?.getTrackingID(
            RetrofitEnums.URL_HBL,
            trackingIDRequest,
            object : TrackingIDCallBack {
                override fun TrackingIDSuccess(response: TrackingIDResponse) {
                    try {
                        (activity as NavigationDrawerActivity).sharedPreferenceManager.setTrackingID(
                            response?.data?.get(0)
                        )
                        callWorkFlow()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as Exception)
                        )
                    }
                }

                override fun TrackingIDFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callWorkFlow() {
        var lov = LovRequest()
        lov.identifier = Constants.WORKFLOW_IDENTIFIER
        HBLHRStore.instance?.getWorkFlow(RetrofitEnums.URL_HBL, lov, object : WorkFlowCallBack {
            override fun WorkFlowSuccess(response: WorkFlowResponse) {
                try {
                    (activity as NavigationDrawerActivity).sharedPreferenceManager.setWorkFlow(
                        response
                    )
                    /*GlobalClass.workFlowCode = response.data[0].WORK_FLOW_CODE
                    GlobalClass.workFlowDesc = response.data[0].PUR_DESC*/
                    callMYSIS()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as UnsatisfiedLinkError)
                    )
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NullPointerException)
                    )
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NumberFormatException)
                    )
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as InterruptedException)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as FileNotFoundException)
                    )
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassCastException)
                    )
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as TypeCastException)
                    )
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as SecurityException)
                    )
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalStateException)
                    )
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as OutOfMemoryError)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
                }
            }

            override fun WorkFlowFailure(response: BaseResponse) {
                ToastUtils.normalShowToast(activity, response.message, 1)
                (activity as NavigationDrawerActivity).globalClass?.hideLoader()
            }
        })
    }

    fun callMYSIS() {
        var lov = LovRequest()
        lov.identifier = Constants.MYSISREF_IDENTIFIER
        HBLHRStore.instance?.getMySisRef(
            RetrofitEnums.URL_HBL,
            lov,
            object : MySisRefCallBack {
                override fun MySisRefSuccess(response: MYSISREFResponse) {
                    try {
                        (activity as NavigationDrawerActivity).sharedPreferenceManager.setMySisRef(
                            response
                        )
                        callAppStatus()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as Exception)
                        )
                    }
                }

                override fun MySisRefFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
    }


    fun callAppStatus() {
        var lov = LovRequest()
        lov.identifier = Constants.APPSTATUS_IDENTIFIER
        HBLHRStore.instance?.getAppStatus(
            RetrofitEnums.URL_HBL,
            lov,
            object : AppStatusCallBack {
                override fun AppStatusSuccess(response: AppStatusResponse) {
                    try {
                        (activity as NavigationDrawerActivity).sharedPreferenceManager.setAppStatus(
                            response.data?.get(0)
                        )
                        callToActivity()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as Exception)
                        )
                    }
                }

                override fun AppStatusFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callToActivity() {
        setAppStatusValue()
        when {
            arguments?.getInt(Constants.ACTIVITY_KEY) == Constants.SPARK -> {
                findNavController().navigate(R.id.action_nav_dashboard_to_CIFRootActivity)
            }
            arguments?.getInt(Constants.ACTIVITY_KEY) == Constants.HAW -> {
                findNavController().navigate(R.id.action_nav_dashboard_to_HawActivity)
            }
        }

        (activity as NavigationDrawerActivity).globalClass?.hideLoader()
    }

    fun setAppStatusValue() {
        (activity as NavigationDrawerActivity).sharedPreferenceManager.appStatus.CIF.let {
            (activity as NavigationDrawerActivity).globalClass?.appStatus = it!!
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
    }
}
