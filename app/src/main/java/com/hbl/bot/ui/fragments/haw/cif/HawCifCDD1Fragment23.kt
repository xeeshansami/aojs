package com.hbl.bot.ui.fragments.cif


import com.hbl.bot.ui.activities.HawActivity
import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.AofAccountInfoRequest
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.IndustryMainCategory
import com.hbl.bot.network.models.response.baseRM.IndustrySubCategory
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep5.*
import kotlinx.android.synthetic.main.fragment_cifstep5.btBack
import kotlinx.android.synthetic.main.fragment_cifstep5.btNext
import kotlinx.android.synthetic.main.fragment_cifstep5.formSectionHeader
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class HawCifCDD1Fragment23 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var hblhrStore: HBLHRStore = HBLHRStore()
    var myView: View? = null
    var riskStatusET: EditText? = null
    var fivIndustryMainCategorySP: Spinner? = null
    var fivIndustrySubCategorySP: Spinner? = null
    var fivMailInstructionSP: Spinner? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep5, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    fun header() {
        viewModel.totalSteps.setValue(4)
        viewModel.currentFragmentIndex.setValue(0)
        val txt = resources.getString(R.string.cdd_details)
        val txt1 = " (1/4 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity)
                .registerReceiver(
                    mStatusCodeResponse, IntentFilter(
                        Constants.STATUS_BROADCAST
                    )
                );
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            btRiskRating.setOnClickListener(this)
            header()
            init()
            load()
            setConditions()
            riskRatingChangeCheck()
            onBackPress(view)
            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setConditions() {
        fivIndustryMainCategorySP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                try {
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromIndustryMainCategoryCode(
                        (activity as HawActivity).industryMainCategoryList,
                        fivIndustryMainCategorySP?.selectedItem.toString()
                    ).let {
                        if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                            industrySubCategory(it!!)
                        }
                    }
                    var varIndustryMainCategory =
                        (activity as HawActivity).sharedPreferenceManager.customerCDD.dESC_CUST_SRC_OF_INDUS_TYPE
                    if (!fivIndustryMainCategorySP?.selectedItem.toString()
                            .equals(varIndustryMainCategory, true)
                    ) {
                        /*user force fully change IndustryMainCategory so then risk rating again call*/
                        if (!(activity as HawActivity).globalClass?.isAllFieldsDisbaled!!) {
                            (activity as HawActivity).globalClass?.setDisbaled(btNext!!)
                            (activity as HawActivity).globalClass?.setEnabled(
                                btRiskRating!!,
                                true
                            )
                        }
//                        saveAndNext()
                        Log.i(
                            "CDDDETAILS",
                            "$varIndustryMainCategory," + fivIndustryMainCategorySP?.selectedItem.toString()
                        )
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
        })
        fivIndustrySubCategorySP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                try {
                    var varIndustrySubCategorySP =
                        (activity as HawActivity).sharedPreferenceManager.customerCDD.dESC_CUST_SRC_OF_INCOME_INDUS
                    if (!fivIndustrySubCategorySP?.selectedItem.toString()
                            .equals(varIndustrySubCategorySP, true)
                    ) {
                        /*user force fully change IndustryMainCategory so then risk rating again call*/
                        if (!(activity as HawActivity).globalClass?.isAllFieldsDisbaled!!) {
                            (activity as HawActivity).globalClass?.setDisbaled(btNext!!)
                            (activity as HawActivity).globalClass?.setEnabled(btRiskRating!!, true)
                        }
//                        saveAndNext()
                        Log.i(
                            "CDDDETAILS",
                            "$varIndustrySubCategorySP," + fivIndustrySubCategorySP?.selectedItem.toString()
                        )
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
        })
        fivMailInstructionSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                try {
                    var varMailInstructionSP =
                        (activity as HawActivity).sharedPreferenceManager.customerCDD.hOLD_MAIL_INSTR
                    if (!getBoolIndex().equals(varMailInstructionSP) && !varMailInstructionSP.isNullOrEmpty()) {
//                        saveAndNext()
//                    user force fully change IndustryMainCategory so then risk rating again call
                        if (!(activity as HawActivity).globalClass?.isAllFieldsDisbaled!!) {
                            (activity as HawActivity).globalClass?.setDisbaled(btNext!!)
                            (activity as HawActivity).globalClass?.setEnabled(
                                btRiskRating!!,
                                true
                            )
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
        })
    }

    fun getBoolIndex(): String {
        for (x in (activity as HawActivity).boolList.indices) {
            if ((activity as HawActivity).boolList.get(x).LB_NAME.equals(
                    fivMailInstructionSP?.selectedItem.toString()
                )
            ) {
                return (activity as HawActivity).boolList.get(x).LB_CODE.toString()
            }
        }
        return ""
    }

    fun load() {
        (activity as HawActivity).globalClass?.showDialog(activity)
//        if ((activity as HawActivity).sharedPreferenceManager.lovIndustryMainCategory.isNullOrEmpty()) {
        industryMainCategory()
//        } else {
//            setindustryMainCategory((activity as HawActivity).sharedPreferenceManager.lovIndustryMainCategory)
//        }
    }

    private fun setindustryMainCategory(it: ArrayList<IndustryMainCategory>) {
        try {
            fivIndustryMainCategory.setItemForIndustryMainCategory(it)
            (activity as HawActivity).industryMainCategoryList = it
            (activity as HawActivity).sharedPreferenceManager.lovIndustryMainCategory = it
            (activity as HawActivity).globalClass?.findSpinnerPositionFromIndustryHawMainCategoryIndex(
                it,
                (activity as HawActivity).customerCdd.cUST_SRC_OF_INDUS_TYPE
            ).let {
                fivIndustryMainCategorySP?.setSelection(it!!)
            }
            fivIndustryMainCategory.remainSelection(it.size)
            if ((activity as HawActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
                callBool()
            } else {
                setBool((activity as HawActivity).sharedPreferenceManager.lovBool)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let {
                        setBool(it)
                    };
                }

                override fun BoolFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun setBool(it: ArrayList<Bool>) {
        try {
            setTextLoad()
            (activity as HawActivity).boolList = it
            fivInwardRemittance.setItemForfivInwardRemittance(it)
            fivOutwardRemittance.setItemForfivOutwardRemittance(it)
            fivBankInfo.setItemForfivBankInfo(it)
            fivMailInstruction.setItemForBools(it)
            (activity as HawActivity).sharedPreferenceManager.lovBool = it
//            (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
//                it,
//                (activity as HawActivity).customerCdd.HOLD_MAIL_INSTR
//            ).let {
//                fivMailInstructionSP?.setSelection(it!!)
//            }

            (activity as HawActivity).customerCdd.hOLD_MAIL_INSTR.let { HOLD_MAIL_INSTR ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromHoldMailBoolIndex(
                    it,
                    HOLD_MAIL_INSTR
                ).let {
                    if (it != 0) {
                        fivMailInstructionSP?.setSelection(it!!)
                    } else {
                        var list = (activity as HawActivity).sharedPreferenceManager.hawKeys
                        for (x in 0 until list.size) {
                            if (list[x].IDENTIFIER == Constants.BOOL_IDENTIFIER && list[x].NAME == Constants.BOOL_HOLD_MAIL_INSTRUCTION_IDENTIFIER) {
                                (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
                                    (activity as HawActivity).boolList,
                                    list[x].LOV_CODE!!
                                ).let {
                                    fivMailInstructionSP?.setSelection(it!!)
                                }
                            }
                        }
                    }
                }
            }
            (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setTextLoad() {
        if (!(activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.isNullOrEmpty())
            riskCheck((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING)
        else if (!(activity as HawActivity).riskRating.isNullOrEmpty())
            riskCheck((activity as HawActivity).riskRating)
//        else
//            riskCheck("Medium")
//        riskCheck("HIGH")
    }

    fun init() {
        riskStatusET = fiVRiskRating.getTextFromEditText(R.id.riskStatusID)
        riskStatusET?.hint = "Any:-> Low/Medium/High"
        fivIndustryMainCategorySP =
            fivIndustryMainCategory.getSpinner(R.id.fivIndustryMainCategoryID)
        fivIndustrySubCategorySP =
            fivIndustrySubCategory.getSpinner(R.id.fivIndustrySubCategoryID)
        fivMailInstructionSP = fivMailInstruction.getSpinner(R.id.fivMailInstructionID)
        (activity as HawActivity).globalClass?.setDisbaled(riskStatusET!!)
        (activity as HawActivity).globalClass?.setEnabled(btRiskRating!!, true)
    }

    private fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
            /*ETB CASE*/
            (activity as HawActivity).globalClass?.setDisbaled(riskStatusET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivIndustryMainCategorySP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivIndustrySubCategorySP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivMailInstructionSP!!, false)
        } else {
            /*NTB CSAE*/
            (activity as HawActivity).globalClass?.setDisbaled(fivMailInstructionSP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivIndustryMainCategorySP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivIndustrySubCategorySP!!, false)
        }
    }

    fun riskRatingChangeCheck() {
        var purposeOfCIF =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_CODE
        /*In case of Purpose of CIF as director, beneficial owner,
        authorized signatory than hold mail instructions will be freeze with pre-defined value as “NO” */
        if (purposeOfCIF == "03" /*Director*/ || purposeOfCIF == "06" /*Beneficial Owner*/ || purposeOfCIF == "08" /*Authorized Signatory*/) {
            (activity as HawActivity).globalClass?.setDisbaled(fivMailInstructionSP!!)
            fivMailInstructionSP?.setSelection(0)
        }
    }

    fun riskCheck(risk: String?) {
        if (risk.equals("High", true)) {
            riskStatusET?.setText(risk)
            riskStatusET?.setTextColor(Color.parseColor("#D53F45"));
            (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
            (activity as HawActivity).visitBranchPopup("RISK RATING Customer is HIGH so please visit the nearest branch of HBL")
        } else {
            riskStatusET?.setText(risk)
            riskStatusET?.setTextColor(Color.parseColor("#008577"));
            (activity as HawActivity).globalClass?.setDisbaled(btRiskRating!!)
            (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
        }
        (activity as HawActivity).riskRating = risk.toString()
        (activity as HawActivity).aofAccountInfoRequest.RISK_RATING = risk.toString()
        (activity as HawActivity).globalClass?.setDisbaled(riskStatusET!!)


    }

    fun setAllModelsInAOFRiskRequestModel() {

        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }

        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofRiskDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofRiskDataAlign(aofAccountInfo: Data) {
        (activity as HawActivity).aofAccountInfoRequest.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let {
                    (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).etbntbFLAG
        } else {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).REMEDIATE
        } else {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as HawActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        (activity as HawActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).fullName
        } else {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if (!(activity as HawActivity).riskRating.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).riskRating.toString()
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).aofAccountInfoRequest = it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        callToRiskRatingResponse(aofAccountInfo)
    }

    fun callToRiskRatingResponse(aofAccountInfo: Data) {
        var aofAccountInfoRequest = AofAccountInfoRequest()
        aofAccountInfoRequest.identifier = Constants.AOF_SUBMIT_IDENTIFIER
        aofAccountInfoRequest.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(aofAccountInfoRequest)
        HBLHRStore.instance?.postRiskRating(
            RetrofitEnums.URL_HBL,
            aofAccountInfoRequest, object : CustomerRiskRatingCallBack {
                @SuppressLint("WrongConstant")
                override fun CustomerRiskRatingSuccess(response: RiskRatingResponse) {
                    try {
                        ToastUtils.normalShowToast(activity, response.message, 2)
                        response.data?.get(0)?.RISK_RATING?.let {
                            (activity as HawActivity).riskRating = it
                            riskCheck(it)
                        }
                        response.data?.get(0)?.RISK_RATING_TOTAL?.let {
                            (activity as HawActivity).riskRatingTotal = it.toString()
                        }
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as Exception)
                        )
                    }
                }

                override fun CustomerRiskRatingFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) {
//                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
//                            setAllModelsInAOFRiskRequestModel()
//                        })
//                    }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun industryMainCategory() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.INDUSTRY_MAIN_CATEGORY_IDENTIFIER
        hblhrStore?.getIndustryMainCategory(
            RetrofitEnums.URL_HBL,
            lovRequest, object : IndustryMainCategoryBack {
                override fun IndustryMainCategorySuccess(response: IndustryMainCategoryResponse) {
                    response.data?.let {
                        setindustryMainCategory(it)
                    };
                }

                override fun IndustryMainCategoryFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun industrySubCategory(industryCat: String) {
        var lovRequest = LovRequest()
        (activity as HawActivity).globalClass?.showDialog(activity)
        lovRequest.identifier = Constants.INDUSTRY_SUB_CATEGORY_IDENTIFIER
        lovRequest.condition = true
        lovRequest.conditionname = Constants.CCODE
        lovRequest.conditionvalue = industryCat
        Log.i("testIndustrySubCat", industryCat)
        hblhrStore?.getIndustrySubCategory(
            RetrofitEnums.URL_HBL,
            lovRequest, object : IndustrySubCategoryBack {
                override fun IndustrySubCategorySuccess(response: IndustrySubCategoryResponse) {
                    response.data?.let {
                        setindustryMainSubCategory(it)
                    };
                }

                override fun IndustrySubCategoryyFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) {
//                        industrySubCategory(industryCat)
//                    }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setindustryMainSubCategory(it: ArrayList<IndustrySubCategory>) {
        try {
            (activity as HawActivity).industrySubCategoryList = it
            fivIndustrySubCategory.setItemForIndustrySubCategory(it)
            (activity as HawActivity).globalClass?.findSpinnerPositionFromHawIndustrySubCategoryIndex(
                it,
                (activity as HawActivity).customerCdd.cUST_SRC_OF_INCOME_INDUS
            ).let {
                fivIndustrySubCategorySP?.setSelection(it!!)
            }
            fivIndustrySubCategory.remainSelection(it.size)
            (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btRiskRating -> {
                try {
                    if (validation()) {
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            setAllModelsInAOFRiskRequestModel()
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as UnsatisfiedLinkError)
                    )
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NullPointerException)
                    )
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NumberFormatException)
                    )
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as InterruptedException)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as FileNotFoundException)
                    )
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassCastException)
                    )
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as TypeCastException)
                    )
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as SecurityException)
                    )
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalStateException)
                    )
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as OutOfMemoryError)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
                }
            }
            R.id.btNext -> {
                try {
//                if ((activity as HawActivity).cnicNumber.equals("4210119523995")) {
//                    findNavController().navigate(R.id.action_CIFStep5_to_CIFStep7)
//                } else {
//                    if (validation()) {
                    if (!TextUtils.isEmpty(riskStatusET?.text.toString())) {
                        (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            activity?.runOnUiThread {
                                (activity as HawActivity).globalClass?.setEnabled(
                                    btNext,
                                    true
                                )
                            }
                        })
//                        } else {
//                            ToastUtils.normalShowToast(
//                                activity,
//                                "Please check the risk rating first.", 1
//                            )
//                        }
//                }AMOUNT_OF_FOREIGN_TRANSACTION
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as UnsatisfiedLinkError)
                    )
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NullPointerException)
                    )
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NumberFormatException)
                    )
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as InterruptedException)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as FileNotFoundException)
                    )
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassCastException)
                    )
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as TypeCastException)
                    )
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as SecurityException)
                    )
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalStateException)
                    )
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as OutOfMemoryError)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
                }
            }
            R.id.btBack -> isFinancialSupportPage()

        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                isFinancialSupportPage()
                true
            }
            false
        }
    }

    fun isFinancialSupportPage() {
        var custSegType =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        if (custSegType == "A0" || /*STUDENT*/ custSegType == "A2" || /*HOUSE WIFE*/ custSegType == "A4" /*UNEMPLOYED*/ || custSegType == "A7" /*MINOR/GARDIAN*/
        ) {
            if (findNavController().currentDestination?.id == R.id.CIFStep5)
                findNavController().navigate(R.id.action_CIFStep5_to_CIFStep4_8)
        } else if (custSegType == "A3" /*SELF-EMPLOYED*/ || custSegType == "B0" /*BB AGENT*/ || custSegType == "CD" /*SOLE PROPRIETORSHIP*/) {
            if (findNavController().currentDestination?.id == R.id.CIFStep5)
                findNavController().navigate(R.id.action_CIFStep5_to_CIFStep4_6)
        } else if (custSegType == "A5" /*LANDLORD*/) {
            if (findNavController().currentDestination?.id == R.id.CIFStep5)
                findNavController().navigate(R.id.action_CIFStep5_to_CIFStep4_7)
        } else {
            if (findNavController().currentDestination?.id == R.id.CIFStep5)
                findNavController().navigate(R.id.action_CIFStep5_to_CIFStep4_4)
        }
        (activity as HawActivity).recyclerViewSetup()
    }

    private fun validation(): Boolean {
        return when {
            fivIndustryMainCategorySP?.selectedItemPosition == 0 -> {
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString(R.string.pleaseSelectIndustryMainCateErr),
                    1
                )
                false
            }
            fivIndustrySubCategorySP?.selectedItemPosition == 0 -> {
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString(R.string.pleaseSelectIndustrySubCateErr),
                    1
                )
                false
            }
            else -> {
                true
            }
        }
    }

    fun saveAndNext() {
        riskStatusET?.text.toString()
            .let {
                (activity as HawActivity).aofAccountInfoRequest.RISK_RATING = it
                (activity as HawActivity).riskRating = it
            }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromIndustryMainCategoryCode(
            (activity as HawActivity).industryMainCategoryList,
            fivIndustryMainCategorySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerCdd.cUST_SRC_OF_INDUS_TYPE =
                    it.toString()
                (activity as HawActivity).customerCdd.dESC_CUST_SRC_OF_INDUS_TYPE =
                    fivIndustryMainCategorySP?.selectedItem.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromIndustrySubCategoryCode(
            (activity as HawActivity).industrySubCategoryList,
            fivIndustrySubCategorySP?.selectedItem.toString()
        ).let {
            if (it !== "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerCdd.cUST_SRC_OF_INCOME_INDUS = it.toString()
                (activity as HawActivity).customerCdd.dESC_CUST_SRC_OF_INCOME_INDUS =
                    fivIndustrySubCategorySP?.selectedItem.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolHoldMailInstructionCode(
            (activity as HawActivity).boolList,
            fivMailInstructionSP?.selectedItem.toString()
        ).let {
            Log.i("HOLD_MAIL_INSTR", it.toString())
            (activity as HawActivity).customerCdd.hOLD_MAIL_INSTR = it.toString()
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD =
            (activity as HawActivity).customerCdd
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        (activity as HawActivity).aofAccountInfoRequest.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let {
                    (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).etbntbFLAG
        } else {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).REMEDIATE
        } else {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as HawActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        (activity as HawActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).fullName
        } else {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if (!(activity as HawActivity).riskRating.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).riskRating.toString()
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).aofAccountInfoRequest = it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        if (findNavController().currentDestination?.id == R.id.CIFStep5)
                            findNavController().navigate(R.id.action_CIFStep5_to_CIFStep5_3)
                        (activity as HawActivity).recyclerViewSetup()
                        (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
