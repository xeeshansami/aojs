package com.hbl.bot.ui.fragments.spark.cif


import com.hbl.bot.ui.activities.spark.CIFRootActivity
import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.GenerateCIFCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.GenerateCIFResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep6_4.*
import kotlinx.android.synthetic.main.fragment_cifstep6_4.btBack
import kotlinx.android.synthetic.main.fragment_cifstep6_4.btNext
import kotlinx.android.synthetic.main.fragment_cifstep6_4.fivOtherSource
import kotlinx.android.synthetic.main.fragment_cifstep6_4.formSectionHeader
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class CIFStep6_4 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivExpAMTET: EditText? = null
    var fivAgrRentalAMTET: EditText? = null
    var fivBusinessAMTET: EditText? = null
    var fivResidentialRentalAMTET: EditText? = null
    var fivRentalAMTET: EditText? = null
    var fivOtherSourceET: EditText? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep6_4, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }


    }

    fun header() {
        var custSegType =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        val txt = resources.getString(R.string.edd_additional_source_of_income)
        if (custSegType == "A0" || /*STUDENT*/ custSegType == "A2" || /*HOUSE WIFE*/ custSegType == "A4" /*UNEMPLOYED*/) {
            viewModel.totalSteps.setValue(7)
            viewModel.currentFragmentIndex.setValue(2)
            val txt1 = " (3/7 Page)"
            val txt2 = txt + txt1
            formSectionHeader.header_tv_title.text =
                GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
        } else {
            viewModel.totalSteps.setValue(5)
            viewModel.currentFragmentIndex.setValue(2)
            val txt1 = " (3/5 Page)"
            val txt2 = txt + txt1
            formSectionHeader.header_tv_title.text =
                GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as CIFRootActivity)
                .registerReceiver(mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST)
                );
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            init()
            load()
            setLengthAndType()
            /*if (GlobalClass.isDummyDataTrue) {
                loadDummyData()
            }*/
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun load() {
        (activity as CIFRootActivity).customerEdd.aGRI_INCOME_EXP_AMT
            .let { fivExpAMTET?.setText(it) }
        (activity as CIFRootActivity).customerEdd.aGRI_RENTAL_INCOME_EXP_AMT
            .let { fivAgrRentalAMTET?.setText(it.toString()) }
        (activity as CIFRootActivity).customerEdd.bUSINESS_INCOME_EXP_AMT
            .let { fivBusinessAMTET?.setText(it.toString()) }
        (activity as CIFRootActivity).customerEdd.rENTAL_INCOME_EXP_AMT
            .let { fivResidentialRentalAMTET?.setText(it.toString()) }
        (activity as CIFRootActivity).customerEdd.rENTAL_INCOME_RES_PROJ_EXP_AMT
            .let { fivRentalAMTET?.setText(it.toString()) }
        (activity as CIFRootActivity).customerEdd.oTHER_SOURCE_2
            .let { fivOtherSourceET?.setText(it) }
    }

    fun init() {
        fivExpAMTET = fivExpAMT.getTextFromEditText(R.id.ExpAMTID)
        fivAgrRentalAMTET = fivAgrRentalAMT.getTextFromEditText(R.id.AgeRentalAmt)
        fivBusinessAMTET = fivBusinessAMT.getTextFromEditText(R.id.BusinessAMT)
        fivResidentialRentalAMTET =
            fivResidentialRentalAMT.getTextFromEditText(R.id.ResidentialRentalAmt)
        fivRentalAMTET = fivRentalAMT.getTextFromEditText(R.id.RentalAMT)
        fivOtherSourceET = fivOtherSource.getTextFromEditText(R.id.otherSourceID)
        addTextWatcher(fivExpAMTET!!)
        addTextWatcher(fivAgrRentalAMTET!!)
        addTextWatcher(fivBusinessAMTET!!)
        addTextWatcher(fivResidentialRentalAMTET!!)
        addTextWatcher(fivRentalAMTET!!)
        addTextWatcher(fivOtherSourceET!!)
    }

    private fun addTextWatcher(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (editText?.text.isNotEmpty()) {
                        fivExpAMT.mandateInput.text = ""
                        fivAgrRentalAMT.mandateInput.text = ""
                        fivBusinessAMT.mandateInput.text = ""
                        fivResidentialRentalAMT.mandateInput.text = ""
                        fivRentalAMT.mandateInput.text = ""
                        fivOtherSource.mandateInput.text = ""
                    } else {
                        fivExpAMT.mandateInput.text = "*"
                        fivAgrRentalAMT.mandateInput.text = "*"
                        fivBusinessAMT.mandateInput.text = "*"
                        fivResidentialRentalAMT.mandateInput.text = "*"
                        fivRentalAMT.mandateInput.text = "*"
                        fivOtherSource.mandateInput.text = "*"
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

    }

    fun setLengthAndType() {
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivExpAMTET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivAgrRentalAMTET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivBusinessAMTET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivResidentialRentalAMTET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivRentalAMTET!!,
            11,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivOtherSourceET!!,
            25,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )

    }


    fun loadDummyData() {
        fivExpAMTET?.setText("In this study we aimed at characterizing AMT-mediated ammonium")
        fivAgrRentalAMTET?.setText("More farmers are affected by the AMT tha")
        fivBusinessAMTET?.setText("The corporate alternative minimum tax (“AMT”) is perhaps best understood")
        fivResidentialRentalAMTET?.setText("Tax-free exchange of rental property occasionally used for personal purposes")
        fivRentalAMTET?.setText("AMT offers affordable Personal and Business")
        fivOtherSourceET?.setText("No")
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            activity?.runOnUiThread {
                                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep6_4)
                    findNavController().navigate(R.id.action_CIFStep6_4_to_CIFStep6_1)
        }

    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep6_4)
                    findNavController().navigate(R.id.action_CIFStep6_4_to_CIFStep6_1)
                true
            }
            false
        }
    }

    fun saveAndNext() {
        fivExpAMTET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.aGRI_INCOME_EXP_AMT = it }
        fivAgrRentalAMTET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.aGRI_RENTAL_INCOME_EXP_AMT = it }
        fivBusinessAMTET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.bUSINESS_INCOME_EXP_AMT = it }
        fivResidentialRentalAMTET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.rENTAL_INCOME_EXP_AMT = it }
        fivRentalAMTET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.rENTAL_INCOME_RES_PROJ_EXP_AMT = it }
        fivOtherSourceET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.oTHER_SOURCE_2 = it }

        (activity as CIFRootActivity).sharedPreferenceManager.setCustomerEDD((activity as CIFRootActivity).customerEdd)
    }

    fun validation(): Boolean {
        var value1 = fivExpAMTET?.text.toString().trim()
        var value2 = fivAgrRentalAMTET?.text.toString().trim()
        var value3 = fivBusinessAMTET?.text.toString().trim()
        var value4 = fivResidentialRentalAMTET?.text.toString().trim()
        var value5 = fivRentalAMTET?.text.toString().trim()
        var value6 = fivOtherSourceET?.text.toString().trim()
        return if (value1.isEmpty() &&
            value2.isEmpty() &&
            value3.isEmpty() &&
            value4.isEmpty() &&
            value5.isEmpty() &&
            value6.isEmpty()
        ) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_specify_any_amt), 1
            )
            false
        } else {
            true
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as CIFRootActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as CIFRootActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as CIFRootActivity).aofAccountInfoRequest = it
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        if (findNavController().currentDestination?.id == R.id.CIFStep6_4)
                            findNavController().navigate(R.id.action_CIFStep6_4_to_CIFStep6_5)
                        (activity as CIFRootActivity).recyclerViewSetup()
                        (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
