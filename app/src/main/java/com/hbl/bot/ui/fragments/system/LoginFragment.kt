package com.hbl.bot.ui.fragments.system

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ApplicationInfo
import android.content.pm.PackageInfo
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.os.Handler
import android.provider.Settings
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties
import android.text.TextUtils
import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.util.Base64
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.bidfeed.utils.NetworkUtils
import com.google.gson.Gson
import com.hbl.bot.BuildConfig
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.BranchCode
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.ui.activities.SplashActivity
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.nabinbhandari.android.permissions.PermissionHandler
import com.nabinbhandari.android.permissions.Permissions
import kotlinx.android.synthetic.main.fragment_cifstep13.*
import kotlinx.android.synthetic.main.fragment_login.*
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.io.UnsupportedEncodingException
import java.nio.charset.Charset
import java.security.*
import java.security.cert.CertificateException
import java.text.SimpleDateFormat
import java.util.*
import javax.crypto.*
import javax.crypto.spec.IvParameterSpec


class LoginFragment : Fragment(), View.OnClickListener {
    var hblhrStore: HBLHRStore = HBLHRStore()
    var myView: View? = null
    var textDialog: TextDialog? = null
    var twotextDialog: TwoButtonsDialogNew? = null
    var resendcounter = 0
    var installDateTime: String? = null
    var sp: SharedPreferences? = null
    var check = false
    var payload =
        "U2FsdGVkX1/Ghu6gSVZaut0HfH9PLWqebgDStXYbpIOWVrP9UxnnBZ2ZECLXkgHcQVAnONIl+UaeF6QRzmxWKg=="
    val USER_PREF = "USER_PREF"
    var permissions = arrayOf(
        Manifest.permission.CALL_PHONE,
        Manifest.permission.READ_PHONE_STATE,
        Manifest.permission.INTERNET,
        Manifest.permission.CHANGE_WIFI_STATE,
        Manifest.permission.WRITE_EXTERNAL_STORAGE,
        Manifest.permission.CAMERA,
        Manifest.permission.ACCESS_FINE_LOCATION,
        Manifest.permission.ACCESS_COARSE_LOCATION,
        Manifest.permission.READ_EXTERNAL_STORAGE
    )

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            // Inflate the layout for this fragment
//        view.findViewById<ToastButton>(R.id.signup_btn).setOnClickListener {
//            findNavController().navigate(R.id.action_register_to_registered)
//        }
            myView = inflater.inflate(R.layout.fragment_login, container, false)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            Log.i("TestBuild", "2")
            super.onViewCreated(view, savedInstanceState)
            show_pass_btn.setOnClickListener(this)
            bt_login.setOnClickListener(this)
            init()
//            sp = activity?.getSharedPreferences(USER_PREF, Context.MODE_PRIVATE)
//            if (!sp!!.contains(GlobalClass.KEY_TOKEN)) {
//                checkDeviceVerification()
//            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    fun rememberMe() {
        if (remeberCheckBox.isChecked) {
            (activity as NavigationDrawerActivity).sharedPreferenceManager.storeBooleanInSharedPreferences(
                SharedPreferenceManager.REMEMBER,
                true
            )
        } else {
            (activity as NavigationDrawerActivity).sharedPreferenceManager.storeBooleanInSharedPreferences(
                SharedPreferenceManager.REMEMBER,
                false
            )
        }
    }

    private fun rememberMeLoad() {
        var saveLogin =
            (activity as NavigationDrawerActivity).sharedPreferenceManager.getBooleanFromSharedPreferences(
                SharedPreferenceManager.REMEMBER
            )
        if (saveLogin) {
            etEmail.setText(
                (activity as NavigationDrawerActivity).sharedPreferenceManager.getStringFromSharedPreferences(
                    SharedPreferenceManager.USER_LOGIN
                )
            );
            remeberCheckBox.isChecked = true;
        } else {
//            (activity as NavigationDrawerActivity).sharedPreferenceManager.storeBooleanInSharedPreferences(SharedPreferenceManager.REMEMBER,false)
            remeberCheckBox.isChecked = false;
        }
    }

    private fun updateAppData() {
        (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
            (activity as NavigationDrawerActivity).packageName,
            0
        ).versionName.let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.VERSION_NAME,
                it
            )
        }
        (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
            (activity as NavigationDrawerActivity).packageName,
            0
        ).versionCode.let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.VERSION_CODE,
                it.toString()
            )
        }
        GlobalClass.deviceID().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.TABLET_ID,
                it
            )
        }
        "2".let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.CHANNEL_ID,
                it
            )
        }
        GlobalClass.getFolderSizeLabel(GlobalClass.getFileAPK()).let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.TABLET_APP_SIZE,
                it
            )
        }
        GlobalClass.firstModifiedDate().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.APP_CREATED_DATE,
                it
            )
        }
        GlobalClass.lastModifiedDate().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.APP_UPDATE_DATE,
                it
            )
        }
        GlobalClass.sharedPreferenceManager!!.loginData.getUSERID().let {
            GlobalClass.sharedPreferenceManager!!.storeStringInSharedPreferences(
                SharedPreferenceManager.MYSISID,
                it
            )
        }
    }

    private fun init() {
        showHide()
        prefilled()
        updateAppData()
    }

    private fun prefilled() {
        etEmail.setTextColor(
            ContextCompat.getColor(
                activity as NavigationDrawerActivity,
                R.color.colorWhite
            )
        );
        etPassword.setTextColor(
            ContextCompat.getColor(
                activity as NavigationDrawerActivity,
                R.color.colorWhite
            )
        );
        if (Config.BASE_URL_HBL.contains(Config.LIVE)) {
            etEmail.setText("")
            etPassword.setText("")
        } else if (Config.BASE_URL_HBL.contains(Config.RP_TEST) || Config.BASE_URL_HBL.contains(
                Config.HOSTPOT
            )
        ) {
            etEmail.setText(resources.getString(R.string.loginUserID1))
            etPassword.setText(resources.getString(R.string.loginUserPwd4))
        }
        rememberMeLoad()
    }

    fun permission() {
        Permissions.check(
            activity as NavigationDrawerActivity,
            permissions,
            null,
            null,
            object : PermissionHandler() {
                override fun onGranted() {
                    try {// do your task.
                        var allow = false
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            allow = (activity as NavigationDrawerActivity).getPackageManager()
                                .canRequestPackageInstalls()
                        } else {
                            try {
                                allow = Settings.Secure.getInt(
                                    (activity as NavigationDrawerActivity).getContentResolver(),
                                    Settings.Secure.INSTALL_NON_MARKET_APPS
                                ) == 1
                            } catch (e: Settings.SettingNotFoundException) {
                                e.printStackTrace()
                            }
                        }
                        if (!allow) {
                            startActivity(
                                Intent(
                                    Settings.ACTION_MANAGE_UNKNOWN_APP_SOURCES,
                                    Uri.parse("package:" + BuildConfig.APPLICATION_ID)
                                )
                            )
                        } else {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                                if (!Environment.isExternalStorageManager()) {
                                    val intent =
                                        Intent(Settings.ACTION_MANAGE_APP_ALL_FILES_ACCESS_PERMISSION)
                                    val uri =
                                        Uri.fromParts("package", BuildConfig.APPLICATION_ID, null)
                                    intent.data = uri
                                    startActivity(intent)
                                } else {
                                    login()
                                }
                            } else {
                                login()
                            }
                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as Exception)
                        )
                    }
                }

                @SuppressLint("WrongConstant")
                override fun onDenied(
                    context: Context,
                    deniedPermissions: ArrayList<String>,
                ) {
                    ToastUtils.normalShowToast(
                        activity as NavigationDrawerActivity,
                        "Permission denied, please enabled the permissions from setting", 1
                    )
                }
            })
    }

    fun checkDeviceVerification() {
        textDialog = TextDialog(requireContext(), object : DialogCallback {
            override fun onNegativeClicked() {}
            override fun onPositiveClicked() {
                try {
                    if (!TextUtils.isEmpty(GlobalClass.verifyEmpEmail) || GlobalClass.verifyEmpEmail != "") {
                        if (GlobalClass.verifyEmpEmail.contains("@")) {
                            (activity as NavigationDrawerActivity).globalClass?.showDialog(
                                activity
                            )
                            var request = VerifyEmailRequest()
                            request.empmail = GlobalClass.verifyEmpEmail
                            request.resend_count = resendcounter
                            request.identifier = "sendotp"
                            HBLHRStore.instance?.getVerifyEmail(RetrofitEnums.URL_HBL,
                                request,
                                object :
                                    VerifyEmailCallBack {
                                    override fun VerifyEmailSuccess(response: VerifyEmailResponse) {
                                        emailVerifyFunc(response)
                                    }

                                    override fun VerifyEmailFailure(response: BaseResponse) {
                                        ToastUtils.normalShowToast(
                                            activity,
                                            response.message,
                                            1
                                        )
                                        (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                                    }

                                });

                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as UnsatisfiedLinkError)
                    )
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NullPointerException)
                    )
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NumberFormatException)
                    )
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as InterruptedException)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IOException)
                    )
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as FileNotFoundException)
                    )
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassCastException)
                    )
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as TypeCastException)
                    )
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as SecurityException)
                    )
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalStateException)
                    )
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as OutOfMemoryError)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as Exception)
                    )
                }

            }
        })
        textDialog!!.setCancelable(false)
        textDialog!!.show()
    }

    private fun emailVerifyFunc(response: VerifyEmailResponse) {
        try {
            var otp = ""
            textDialog?.dismiss()
            if (Config.BASE_URL_HBL.contains(Config.RP_TEST)) {
                otp = response.message!!.substring(response.message!!.lastIndexOf(",") + 1)
            }
            GlobalClass.resendTime =
                response.data?.get(0)?.RESEND_TIME!!
            twotextDialog =
                TwoButtonsDialogNew(context, otp,
                    object : DialogCallback {
                        override fun onNegativeClicked() {
                            resendcounter++
                            resendOTP(resendcounter)
                        }

                        override fun onPositiveClicked() {
                            try {
                                if (GlobalClass.otp.length === 8) {
                                    (activity as NavigationDrawerActivity).globalClass?.showDialog(
                                        activity
                                    )
                                    var request = VerifyOTPRequest()
                                    getinstallTime()
                                    request.mail =
                                        GlobalClass.verifyEmpEmail
                                    request.app_install_datetime =
                                        installDateTime
                                    request.otp_code =
                                        GlobalClass.otp
                                    request.identifier =
                                        "registerdevice"
                                    HBLHRStore.instance?.getVerifyOTP(
                                        RetrofitEnums.URL_HBL,
                                        request,
                                        object : VerifyOTPCallBack {
                                            override fun VerifyOTPSuccess(
                                                response: VerifyOTPResponse,
                                            ) {
                                                otpVerifyFunc(response)
                                            }

                                            override fun VerifyOTPFailure(response: BaseResponse) {
                                                ToastUtils.normalShowToast(
                                                    activity,
                                                    response.message,
                                                    1
                                                )
                                                (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                                            }
                                        })
                                }
                            } catch (e: UnsatisfiedLinkError) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as UnsatisfiedLinkError)
                                )
                            } catch (e: NullPointerException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as NullPointerException)
                                )
                            } catch (e: IllegalArgumentException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as IllegalArgumentException)
                                )
                            } catch (e: NumberFormatException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as NumberFormatException)
                                )
                            } catch (e: InterruptedException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as InterruptedException)
                                )
                            } catch (e: RuntimeException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as RuntimeException)
                                )
                            } catch (e: IOException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as IOException)
                                )
                            } catch (e: FileNotFoundException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as FileNotFoundException)
                                )
                            } catch (e: ClassNotFoundException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as ClassNotFoundException)
                                )
                            } catch (e: ActivityNotFoundException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as ActivityNotFoundException)
                                )
                            } catch (e: IndexOutOfBoundsException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as IndexOutOfBoundsException)
                                )
                            } catch (e: ArrayIndexOutOfBoundsException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as ArrayIndexOutOfBoundsException)
                                )
                            } catch (e: ClassCastException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as ClassCastException)
                                )
                            } catch (e: TypeCastException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as TypeCastException)
                                )
                            } catch (e: SecurityException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as SecurityException)
                                )
                            } catch (e: IllegalStateException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as IllegalStateException)
                                )
                            } catch (e: OutOfMemoryError) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as OutOfMemoryError)
                                )
                            } catch (e: RuntimeException) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as RuntimeException)
                                )
                            } catch (e: Exception) {
                                ToastUtils.normalShowToast(
                                    activity,
                                    getString(R.string.something_went_wrong),
                                    3
                                )
                                SendEmail.sendEmail(
                                    (activity as NavigationDrawerActivity),
                                    (e as Exception)
                                )
                            }
                        }

                    })
            twotextDialog!!.setCancelable(false)
            twotextDialog!!.show()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as UnsatisfiedLinkError)
            )
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NullPointerException)
            )
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as InterruptedException)
            )
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassCastException)
            )
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as TypeCastException)
            )
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as SecurityException)
            )
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    private fun otpVerifyFunc(response: VerifyOTPResponse) {
        try {
            if (response.status.equals("00")) {
                twotextDialog?.otpText!!.setText("")
                twotextDialog?.otpText!!.alpha = 1f
                twotextDialog?.dismiss()
                val token: String =
                    response.data?.get(
                        0
                    )?.DEVICE_REGISTER_TOKEN.toString()

                val secretKey =
                    createKey()
                var cipher: Cipher? =
                    null
                try {
                    cipher =
                        Cipher.getInstance(
                            KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7
                        )
                } catch (e: NoSuchAlgorithmException) {
                    e.printStackTrace()
                } catch (e: NoSuchPaddingException) {
                    e.printStackTrace()
                }
                try {
                    cipher!!.init(
                        Cipher.ENCRYPT_MODE,
                        secretKey
                    )
                } catch (e: InvalidKeyException) {
                    e.printStackTrace()
                }

                val encriptionIV =
                    cipher!!.iv
                var passwordbyte =
                    ByteArray(
                        0
                    )
                try {
                    passwordbyte =
                        token.toByteArray(
                            charset(
                                "UTF-8"
                            )
                        )
                } catch (e: UnsupportedEncodingException) {
                    e.printStackTrace()
                }
                var encryptedpasswordbyte: ByteArray? =
                    ByteArray(
                        0
                    )
                try {
                    encryptedpasswordbyte =
                        cipher.doFinal(
                            passwordbyte
                        )
                } catch (e: BadPaddingException) {
                    e.printStackTrace()
                } catch (e: IllegalBlockSizeException) {
                    e.printStackTrace()
                }
                val encryptedpassowrd =
                    Base64.encodeToString(
                        encryptedpasswordbyte,
                        Base64.DEFAULT
                    )


                val editor =
                    sp!!.edit()
                editor.putString(
                    "password",
                    encryptedpassowrd
                )
                editor.putString(
                    "encryptedIV",
                    Base64.encodeToString(
                        encriptionIV,
                        Base64.DEFAULT
                    )
                )

                editor.putString(
                    GlobalClass.KEY_TOKEN!!,
                    response.data?.get(
                        0
                    )?.DEVICE_REGISTER_TOKEN.toString()
                )
                editor.apply()


            }
            (activity as NavigationDrawerActivity).globalClass?.hideLoader()
            ToastUtils.normalShowToast(
                context,
                response.message, 2
            )
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as UnsatisfiedLinkError)
            )
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NullPointerException)
            )
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as InterruptedException)
            )
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassCastException)
            )
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as TypeCastException)
            )
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as SecurityException)
            )
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    private fun createKey(): SecretKey {
        var keyGenerator: KeyGenerator? = null
        try {
            keyGenerator = KeyGenerator.getInstance(
                KeyProperties.KEY_ALGORITHM_AES,
                "AndroidKeyStore"
            )
        } catch (e: NoSuchAlgorithmException) {
            e.printStackTrace()
        } catch (e: NoSuchProviderException) {
            e.printStackTrace()
        }
        try {
            keyGenerator!!.init(
                KeyGenParameterSpec.Builder(
                    "Key",
                    KeyProperties.PURPOSE_ENCRYPT or KeyProperties.PURPOSE_DECRYPT
                )
                    .setBlockModes(KeyProperties.BLOCK_MODE_CBC)
                    .setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    .build()
            )
        } catch (e: InvalidAlgorithmParameterException) {
            e.printStackTrace()
        }
        return keyGenerator!!.generateKey()
    }

    private fun getinstallTime() {
        val packageManager: PackageManager = requireActivity().packageManager
        val installTimeInMilliseconds: Long // install time is conveniently provided in milliseconds
        var installDate: Date? = null
        try {
            val packageInfo: PackageInfo =
                packageManager.getPackageInfo(requireActivity().packageName, 0)
            installTimeInMilliseconds = packageInfo.firstInstallTime
            installDateTime =
                GlobalClass.getDate(installTimeInMilliseconds, "MM/dd/yyyy hh:mm:ss")
        } catch (e: PackageManager.NameNotFoundException) {
            // an error occurred, so display the Unix epoch
            installDate = Date(0)
            installDateTime = installDate.toString()
        }
    }

    private fun resendOTP(resendcounter: Int) {
        try {
            var request = VerifyEmailRequest()
            request.empmail = GlobalClass.verifyEmpEmail
            request.resend_count = resendcounter
            request.identifier = "sendotp"
            twotextDialog?.otpText!!.setText("")
            HBLHRStore.instance?.getVerifyEmail(RetrofitEnums.URL_HBL, request, object :
                VerifyEmailCallBack {
                override fun VerifyEmailSuccess(response: VerifyEmailResponse) {
                    if (Config.BASE_URL_HBL.contains(Config.RP_TEST)) {
                        var otp =
                            response.message!!.substring(response.message!!.lastIndexOf(",") + 1)
                        twotextDialog?.otpText!!.setText(otp.trim())
                    }
                    GlobalClass.resendTime = response.data?.get(0)?.RESEND_TIME!!
                }

                override fun VerifyEmailFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }

            });
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as UnsatisfiedLinkError)
            )
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NullPointerException)
            )
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as InterruptedException)
            )
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassCastException)
            )
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as TypeCastException)
            )
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as SecurityException)
            )
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    fun callBranchCode(msg: String) {
        var branchCodeRequest = BranchCodeRequest()
        GlobalClass.sharedPreferenceManager!!.getLoginData().getBRANCHCODE()
            .let { branchCodeRequest.find.BR_CODE = it }
        branchCodeRequest?.identifier = Constants.BRANCH_CODE_INDENTIFIER
        val gson = Gson()
        val json = gson.toJson(branchCodeRequest)
        hblhrStore!!.getBranchCode(
            RetrofitEnums.URL_HBL,
            branchCodeRequest,
            object : BranchCodeCallBack {
                override fun BranchCodeSuccess(response: BranchCodeResponse) {
                    GlobalClass.sharedPreferenceManager!!.setBranchCode(response?.data?.get(0))
                    gotoHome(msg)
                }

                override fun BranchCodeFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
    }

    fun gotoHome(msg: String) {
        var request = AppDetailsRequest()
        request.identifier = Constants.APP_DETAILS_IDENTIFIER
        request.apkSize = getFolderSizeLabel(getFileAPK())
        request.applicationID = "${GlobalClass.deviceID()}"
        request.packageName = (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
            (activity as NavigationDrawerActivity).packageName,
            0
        ).packageName
        request.applicationName = resources.getString(R.string.app_name)
        request.createdApkDate = firstModifiedDate()
        request.updateApkDate = lastModifiedDate()
        request.userID =
            (activity as NavigationDrawerActivity).sharedPreferenceManager.loginData.getUSERID()
        request.IMEI = "${GlobalClass.getIMEIDeviceId(activity as NavigationDrawerActivity)}"
        request.androidApiVersion =
            (activity as NavigationDrawerActivity).globalClass?.getAndroidVersion(android.os.Build.VERSION.SDK_INT)
                .toString()
        Log.i("IMEI", "IMEI=> ${GlobalClass.getIMEIDeviceId(activity as NavigationDrawerActivity)}")
        val versionName =
            (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                (activity as NavigationDrawerActivity).packageName,
                0
            ).versionName
        val verCode = (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
            (activity as NavigationDrawerActivity).packageName,
            0
        ).versionCode
        request.versionCode = verCode.toString()
        request.versionName = versionName
        request.HAVE_GOOGLE_SERVICES=GlobalClass.isGooglePlayServicesAvailable(activity as NavigationDrawerActivity).toString()
        request.IMEI = "${GlobalClass.getIMEIDeviceId(activity as NavigationDrawerActivity)}"
        request.packageName = (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
            (activity as NavigationDrawerActivity).packageName,
            0
        ).packageName
        request.androidApiVersion =
            (activity as NavigationDrawerActivity).globalClass!!.getAndroidVersion(android.os.Build.VERSION.SDK_INT)
                .toString()
        HBLHRStore?.instance?.sendAppDetails(RetrofitEnums.URL_HBL, request, object :
            AppStatusCallBack {
            override fun AppStatusSuccess(response: AppStatusResponse) {
                try {
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                    ToastUtils.normalShowToast(activity, msg, 2)
                    findNavController().navigate(R.id.action_loginFragment_to_nav_home2)
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as UnsatisfiedLinkError)
                    )
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NullPointerException)
                    )
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalArgumentException)
                    )
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as NumberFormatException)
                    )
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as InterruptedException)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IOException)
                    )
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as FileNotFoundException)
                    )
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ClassNotFoundException)
                    )
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ActivityNotFoundException)
                    )
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IndexOutOfBoundsException)
                    )
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: java.lang.ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as java.lang.ClassCastException)
                    )
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as TypeCastException)
                    )
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as SecurityException)
                    )
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as IllegalStateException)
                    )
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as OutOfMemoryError)
                    )
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as RuntimeException)
                    )
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as NavigationDrawerActivity),
                        (e as Exception)
                    )
                }
                (activity as NavigationDrawerActivity).globalClass?.hideLoader()
            }

            override fun AppStatusFailure(response: BaseResponse) {
                Log.d("text", response.toString())
                (activity as NavigationDrawerActivity).globalClass?.hideLoader()
            }
        })
    }

    fun callBranchType(msg: String) {
        var branchCodeRequest = BranchCodeRequest()
        GlobalClass.sharedPreferenceManager!!.getLoginData().getBRANCHCODE()
            .let { branchCodeRequest.find.BR_CODE = it }
        branchCodeRequest?.identifier = Constants.BRANCH_TYPE_INDENTIFIER
        val gson = Gson()
        val json = gson.toJson(branchCodeRequest)
        hblhrStore!!.getBranchType(
            RetrofitEnums.URL_HBL,
            branchCodeRequest,
            object : BranchTypeCallBack {
                override fun BranchTypeSuccess(response: BranchTypeResponse) {
                    try {
                        GlobalClass.sharedPreferenceManager!!.setBranchType(
                            response?.data?.get(
                                0
                            )
                        )
                        findNavController().navigate(R.id.action_loginFragment_to_nav_home2)
                        (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                        ToastUtils.normalShowToast(activity, msg, 2)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as Exception)
                        )
                    }
                }

                override fun BranchTypeFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
    }

    fun isEmailValid(email: String): Boolean {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun validation(): Boolean {
        val email: String = etEmail.getText().toString().trim()
        val pwd: String = etPassword.getText().toString().trim()
        return if (TextUtils.isEmpty(email)) {
            ToastUtils.normalShowToast(activity, getString(R.string.username_err), 1)
            false
        }/* else if (!isEmailValid(email)) {
            etEmail.setError("Please enter valid UserID")
            etEmail.setFocusable(true)
            ToastUtils.normalShowToast(activity, "Please enter valid UserID")
            return false;
        }*/ else if (TextUtils.isEmpty(pwd)) {
            ToastUtils.normalShowToast(activity, getString(R.string.pwd_err), 1)
            false
        } else {
            true
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.bt_login -> {
//                val pm: PowerManager? = (activity as NavigationDrawerActivity).getSystemService(Context.POWER_SERVICE) as PowerManager?
//                pm!!.reboot(null)
                if (NetworkUtils.isInternetAvailable()) {
                    permission()
                } else {
                    ToastUtils.normalShowToast(activity, getString(R.string.not_network), 0)
                }
            }
            R.id.show_pass_btn -> {
                showHide()
            }
        }
    }

    fun gotoTheHome(){
        (activity as NavigationDrawerActivity).globalClass?.hideLoader()
        ToastUtils.normalShowToast(activity, "Login Successfully", 2)
        findNavController().navigate(R.id.action_loginFragment_to_nav_home2)
    }

    private fun showHide() {
        if (check) {
            check = false
            etPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
            show_pass_btn.setImageResource(R.drawable.ic_baseline_remove_red_eye_24)
        } else {
            etPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
            show_pass_btn.setImageResource(R.drawable.ic_baseline_visibility_off_24)
            check = true
        }
    }


    fun login() {
        try {
            /**
             * Checks if the device is rooted.
             * @return <code>true</code> if the device is rooted, <code>false</code>MH otherwise.
             */
            var root = RootUtil(activity as NavigationDrawerActivity)
            var isEmulator = Build.FINGERPRINT.contains("generic")
            if (isEmulator || !root.isDeviceRooted) {
                if (validation()) /*{
                    val base64EncryptionPass = sp!!.getString("password", "")
                    val base64EncryptionIV = sp!!.getString("encryptedIV", "")

                    val encryptionIv = Base64.decode(
                        base64EncryptionIV,
                        Base64.DEFAULT
                    )
                    val encryptedPassword = Base64.decode(
                        base64EncryptionPass,
                        Base64.DEFAULT
                    )

                    var keyStore: KeyStore? = null
                    var token: String? = null
                    try {
                        keyStore = KeyStore.getInstance("AndroidKeyStore")
                    } catch (e: KeyStoreException) {
                        e.printStackTrace()
                    }
                    try {
                        keyStore!!.load(null)
                    } catch (e: CertificateException) {
                        e.printStackTrace()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    } catch (e: NoSuchAlgorithmException) {
                        e.printStackTrace()
                    }
                    var secretKey: SecretKey? = null
                    try {
                        secretKey = keyStore!!.getKey("Key", null) as SecretKey
                    } catch (e: KeyStoreException) {
                        e.printStackTrace()
                    } catch (e: NoSuchAlgorithmException) {
                        e.printStackTrace()
                    } catch (e: UnrecoverableKeyException) {
                        e.printStackTrace()
                    }

                    var cipher: Cipher? = null
                    try {
                        cipher =
                            Cipher.getInstance(KeyProperties.KEY_ALGORITHM_AES + "/" + KeyProperties.BLOCK_MODE_CBC + "/" + KeyProperties.ENCRYPTION_PADDING_PKCS7)
                    } catch (e: NoSuchAlgorithmException) {
                        e.printStackTrace()
                    } catch (e: NoSuchPaddingException) {
                        e.printStackTrace()
                    }
                    try {
                        cipher!!.init(
                            Cipher.DECRYPT_MODE,
                            secretKey,
                            IvParameterSpec(encryptionIv)
                        )
                    } catch (e: InvalidAlgorithmParameterException) {
                        e.printStackTrace()
                    } catch (e: InvalidKeyException) {
                        e.printStackTrace()
                    }
                    var passwordbyte: ByteArray? = ByteArray(0)
                    try {
                        passwordbyte = cipher!!.doFinal(encryptedPassword)
                    } catch (e: BadPaddingException) {
                        e.printStackTrace()
                    } catch (e: IllegalBlockSizeException) {
                        e.printStackTrace()
                    }
                    try {
                        token = String(passwordbyte!!, Charset.forName("UTF-8"))
                        GlobalClass.DRToken = token
                        (activity as NavigationDrawerActivity).sharedPreferenceManager.storeStringInSharedPreferences(
                            SharedPreferenceManager.D_TOKEN,
                            token
                        )
                        Log.i("TOKEN", token)
                    } catch (e: UnsupportedEncodingException) {
                        e.printStackTrace()
                    }

                    val loginRequest: LoginRequest? = LoginRequest(
                        payload,
                        token.toString(),
                        etEmail.text.toString(),
                        etPassword.text.toString()
                    )
                    rememberMe()
                    (activity as NavigationDrawerActivity).sharedPreferenceManager.storeStringInSharedPreferences(
                        SharedPreferenceManager.USER_LOGIN,
                        etEmail?.text.toString().trim()
                    )
                    (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
                    hblhrStore!!.getLogin(
                        RetrofitEnums.URL_HBL,
                        loginRequest,
                        object : LoginCallBack {
                            override fun LoginSuccess(response: LoginResponse) {
                                try {
                                    if (response.status.equals("01") && response.message.equals("This device is not registered, please re-install the application")) {
                                        clearCacheAlert()
                                    } else if (response.data[0].app.isNullOrEmpty()) {
                                        GlobalClass.sharedPreferenceManager!!.setLoginData(
                                            response.data.get(
                                                0
                                            )
                                        )
                                        //Normal Flow
                                        if (response.data[0].USER_BRANCH_SELECTION == "1") {
                                            GlobalClass.sharedPreferenceManager!!.setBranchCode(
                                                BranchCode()
                                            )
                                            gotoHome(response?.message)
                                        } else {
                                            callBranchCode(response?.message)
                                        }
                                    } else {
                                        GlobalClass.sharedPreferenceManager!!.setLoginData(
                                            response.data.get(
                                                0
                                            )
                                        )
                                        //NCR Condition
                                        gotoHome(response?.message)
                                    }
                                } catch (e: UnsatisfiedLinkError) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as UnsatisfiedLinkError)
                                    )
                                } catch (e: NullPointerException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as NullPointerException)
                                    )
                                } catch (e: IllegalArgumentException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as IllegalArgumentException)
                                    )
                                } catch (e: NumberFormatException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as NumberFormatException)
                                    )
                                } catch (e: InterruptedException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as InterruptedException)
                                    )
                                } catch (e: RuntimeException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as RuntimeException)
                                    )
                                } catch (e: IOException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as IOException)
                                    )
                                } catch (e: FileNotFoundException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as FileNotFoundException)
                                    )
                                } catch (e: ClassNotFoundException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as ClassNotFoundException)
                                    )
                                } catch (e: ActivityNotFoundException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as ActivityNotFoundException)
                                    )
                                } catch (e: IndexOutOfBoundsException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as IndexOutOfBoundsException)
                                    )
                                } catch (e: ArrayIndexOutOfBoundsException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as ArrayIndexOutOfBoundsException)
                                    )
                                } catch (e: ClassCastException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as ClassCastException)
                                    )
                                } catch (e: TypeCastException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as TypeCastException)
                                    )
                                } catch (e: SecurityException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as SecurityException)
                                    )
                                } catch (e: IllegalStateException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as IllegalStateException)
                                    )
                                } catch (e: OutOfMemoryError) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as OutOfMemoryError)
                                    )
                                } catch (e: RuntimeException) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as RuntimeException)
                                    )
                                } catch (e: Exception) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        getString(R.string.something_went_wrong),
                                        3
                                    )
                                    SendEmail.sendEmail(
                                        (activity as NavigationDrawerActivity),
                                        (e as Exception)
                                    )
                                }
                            }

                            override fun LoginFailure(response: BaseResponse) {
                                ToastUtils.normalShowToast(activity, response.message, 1)
                                (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                            }
                        })
                }*/
                {
                    (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
                    Handler().postDelayed({
                        gotoTheHome()
                    }, 1500)
                }
            } else {
                warning("Your device is rooted and you can't be run further any process", "Warning")
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as UnsatisfiedLinkError)
            )
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NullPointerException)
            )
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalArgumentException)
            )
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as NumberFormatException)
            )
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as InterruptedException)
            )
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as FileNotFoundException)
            )
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassNotFoundException)
            )
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ActivityNotFoundException)
            )
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IndexOutOfBoundsException)
            )
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ArrayIndexOutOfBoundsException)
            )
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as ClassCastException)
            )
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as TypeCastException)
            )
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as SecurityException)
            )
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail(
                (activity as NavigationDrawerActivity),
                (e as IllegalStateException)
            )
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }
    }

    fun clearCacheAlert() {
        var dialogBuilder = AlertDialog.Builder(activity as NavigationDrawerActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.popup_cache, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        alertDialog.setCancelable(false)
        var yes = layoutView.findViewById<Button>(R.id.yes_action)
        var cancel = layoutView.findViewById<Button>(R.id.cancel_action)
        var progress = layoutView.findViewById<ProgressBar>(R.id.progressBar)
        var toolbar = layoutView.findViewById<Toolbar>(R.id.toolbar)
        var desctext1 = layoutView.findViewById<TextView>(R.id.desctext1)
        progress.max=100
        desctext1.setText("Re-Installation required for registration, press \"Continue\" to perform installation and register this application with your domain HBL email address")
        toolbar.setTitle("ALERT")
        toolbar.setSubtitle("Application registration failed")
        yes.setOnClickListener(View.OnClickListener {
            try {
                Handler().postDelayed({
                    GlobalClass.clearAppData(activity as NavigationDrawerActivity)
                    (activity as NavigationDrawerActivity).finish()
                    (activity as NavigationDrawerActivity).startActivity(
                        Intent(
                            activity as NavigationDrawerActivity,
                            SplashActivity::class.java
                        )
                    )
                }, 3000)

            } catch (e: UnsatisfiedLinkError) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: NullPointerException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.NullPointerException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IllegalArgumentException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IllegalArgumentException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: NumberFormatException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.NumberFormatException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: InterruptedException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IOException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: FileNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ClassNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ActivityNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ArrayIndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.ClassCastException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: TypeCastException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: SecurityException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IllegalStateException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IllegalStateException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: OutOfMemoryError) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.Exception) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: Exception) {
                Log.i("SystemLogs", e.message.toString())
            }
            alertDialog.dismiss()
        })
        cancel.setOnClickListener(View.OnClickListener {
            try {
                alertDialog.dismiss()
            } catch (e: UnsatisfiedLinkError) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: NullPointerException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.NullPointerException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IllegalArgumentException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IllegalArgumentException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: NumberFormatException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.NumberFormatException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: InterruptedException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IOException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: FileNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ClassNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ActivityNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ArrayIndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.ClassCastException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: TypeCastException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: SecurityException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IllegalStateException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IllegalStateException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: OutOfMemoryError) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.Exception) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: Exception) {
                Log.i("SystemLogs", e.message.toString())
            }
            alertDialog.dismiss()
        })
        alertDialog.show()
    }

    private fun warning(message: String, header: String) {
        var dialogBuilder = AlertDialog.Builder(activity as NavigationDrawerActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.synce_to_server_survey_dialog, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        alertDialog.setCancelable(false)
        var cancel = layoutView.findViewById<Button>(R.id.cancel)
        var done = layoutView.findViewById<Button>(R.id.done)
        var edit = layoutView.findViewById<Button>(R.id.edit)
        edit.visibility = View.GONE
        done.visibility = View.GONE
        var textDialog = layoutView.findViewById<TextView>(R.id.textDialog)
        var header_title = layoutView.findViewById<TextView>(R.id.header_title)
        header_title.text = header
        textDialog.text = message
        cancel.text = "OK"
        cancel.setOnClickListener(View.OnClickListener {
            try {
//                (activity as NavigationDrawerActivity).finish()
            } catch (e: UnsatisfiedLinkError) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: NullPointerException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.NullPointerException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IllegalArgumentException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IllegalArgumentException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: NumberFormatException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.NumberFormatException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: InterruptedException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IOException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: FileNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ClassNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ActivityNotFoundException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: ArrayIndexOutOfBoundsException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.ClassCastException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: TypeCastException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: SecurityException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: IllegalStateException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.IllegalStateException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: OutOfMemoryError) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: RuntimeException) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: java.lang.Exception) {
                Log.i("SystemLogs", e.message.toString())
            } catch (e: Exception) {
                Log.i("SystemLogs", e.message.toString())
            }
            alertDialog.dismiss()
        })
        alertDialog.show()
    }


    @SuppressLint("WrongConstant")
    private fun about() {
        var dialogBuilder = AlertDialog.Builder(activity as NavigationDrawerActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.about_app_popup, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.show()
        var shareBtn = layoutView.findViewById<ImageView>(R.id.shareBtn)
        var ok_action = layoutView.findViewById<Button>(R.id.ok_action)
        var toolbar = layoutView.findViewById<Toolbar>(R.id.toolbar)
        toolbar.title = "About"
        var appname = layoutView.findViewById<TextView>(R.id.appname)
        appname.text = resources.getString(R.string.app_name)
        var packageName = layoutView.findViewById<TextView>(R.id.packageName)
        packageName.text = (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
            (activity as NavigationDrawerActivity).packageName,
            0
        ).packageName
        var UserID = layoutView.findViewById<TextView>(R.id.UserID)
        var versionCode = layoutView.findViewById<TextView>(R.id.versionCode)
        UserID.text =
            (activity as NavigationDrawerActivity).sharedPreferenceManager.loginData.getUSERID()
        var applicationID = layoutView.findViewById<TextView>(R.id.applicationID)
        var version = layoutView.findViewById<TextView>(R.id.version)
        var apkSize = layoutView.findViewById<TextView>(R.id.apkSize)
        var createDateOfFile = layoutView.findViewById<TextView>(R.id.createDateOfFile)
        var updaDateOfFile = layoutView.findViewById<TextView>(R.id.updaDateOfFile)
        try {
            val versionName =
                (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                    (activity as NavigationDrawerActivity).packageName,
                    0
                ).versionName
            val verCode = (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                (activity as NavigationDrawerActivity).packageName,
                0
            ).versionCode
            version.text = "$versionName"
            versionCode.text = "$verCode"
            applicationID.text = "${GlobalClass.deviceID()}"
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }
        if (getFileAPK().exists()) {
            apkSize.text = getFolderSizeLabel(getFileAPK())
            createDateOfFile.text = firstModifiedDate()
            updaDateOfFile.text = lastModifiedDate()
        } else {
            apkSize.text = "N/A"
            createDateOfFile.text = "N/A"
            updaDateOfFile.text = "N/A"
        }
        ok_action.setOnClickListener(View.OnClickListener {
            alertDialog.dismiss()
        })
        shareBtn.setOnClickListener(View.OnClickListener {
            (activity as NavigationDrawerActivity).globalClass?.showDialog(activity)
            var request = AppDetailsRequest()
            request.identifier = Constants.APP_DETAILS_IDENTIFIER
            request.apkSize = apkSize.text.toString()
            request.applicationID = applicationID.text.toString()
            request.applicationName = appname.text.toString()
            request.createdApkDate = createDateOfFile.text.toString()
            request.updateApkDate = updaDateOfFile.text.toString()
            request.userID =
                (activity as NavigationDrawerActivity).sharedPreferenceManager.loginData.getUSERID()
            request.IMEI = "${GlobalClass.getIMEIDeviceId(activity as NavigationDrawerActivity)}"
            request.packageName =
                (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                    (activity as NavigationDrawerActivity).packageName,
                    0
                ).packageName
            request.versionCode = versionCode.text.toString()
            request.versionName = version.text.toString()
            request.androidApiVersion =
                (activity as NavigationDrawerActivity).globalClass!!.getAndroidVersion(android.os.Build.VERSION.SDK_INT)
                    .toString()
            request.IMEI = "${GlobalClass.getIMEIDeviceId(activity as NavigationDrawerActivity)}"
            request.packageName =
                (activity as NavigationDrawerActivity).packageManager.getPackageInfo(
                    (activity as NavigationDrawerActivity).packageName,
                    0
                ).packageName
            HBLHRStore?.instance?.sendAppDetails(RetrofitEnums.URL_HBL, request, object :
                AppStatusCallBack {
                override fun AppStatusSuccess(response: AppStatusResponse) {
                    try {
                        ToastUtils.normalShowToast(activity, response.message, 2)
                        alertDialog.dismiss()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as UnsatisfiedLinkError)
                        )
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NullPointerException)
                        )
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as NumberFormatException)
                        )
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as InterruptedException)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IOException)
                        )
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as FileNotFoundException)
                        )
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as ClassCastException)
                        )
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as TypeCastException)
                        )
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as SecurityException)
                        )
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as IllegalStateException)
                        )
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as OutOfMemoryError)
                        )
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as RuntimeException)
                        )
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as NavigationDrawerActivity),
                            (e as Exception)
                        )
                    }
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }

                override fun AppStatusFailure(response: BaseResponse) {
                    Log.d("text", response.toString())
                    (activity as NavigationDrawerActivity).globalClass?.hideLoader()
                }
            })
        })
        alertDialog.show()
    }

    private fun getFileAPK(): File {
        val pm = (activity as NavigationDrawerActivity).packageManager
        val applicationInfo: ApplicationInfo =
            pm.getApplicationInfo((activity as NavigationDrawerActivity).packageName, 0)
        val file: File = File(applicationInfo.publicSourceDir)
        return file
    }

    private fun lastModifiedDate(): String {
        val pm = (activity as NavigationDrawerActivity).packageManager
        val packageInfo: PackageInfo =
            pm.getPackageInfo(
                (activity as NavigationDrawerActivity).packageName,
                PackageManager.GET_PERMISSIONS
            )
        val updateTime = Date(packageInfo.lastUpdateTime)
        val sdf = SimpleDateFormat("dd-MMM-yyyy hh:mm a")
        return sdf.format(updateTime)
    }

    private fun firstModifiedDate(): String {
        val pm = (activity as NavigationDrawerActivity).packageManager
        val packageInfo: PackageInfo =
            pm.getPackageInfo(
                (activity as NavigationDrawerActivity).packageName,
                PackageManager.GET_PERMISSIONS
            )
        val installTime = Date(packageInfo.firstInstallTime)
        val sdf = SimpleDateFormat("dd-MMM-yyyy hh:mm a")
        return sdf.format(installTime)
    }

    private fun getFolderSizeLabel(file: File): String? {
        val size =
            getFolderSize(file).toDouble() / 1000.0 // Get size and convert bytes into KB.
        return if (size >= 1024) {
            String.format("%.2f MB", (size / 1024))
        } else {
            String.format("%.2f KB", size)
        }
    }

    fun getFolderSize(file: File): Long {
        var size: Long = 0
        if (file.isDirectory) {
            for (child in file.listFiles()) {
                size += getFolderSize(child)
            }
        } else {
            size = file.length()
        }
        return size
    }


}