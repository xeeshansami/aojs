package com.hbl.bot.ui.fragments.cif


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.GetCountryCityCodeRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.NadraVerify
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep2.*
import kotlinx.android.synthetic.main.fragment_cifstep2.btBack
import kotlinx.android.synthetic.main.fragment_cifstep2.btNext
import kotlinx.android.synthetic.main.fragment_cifstep2.formSectionHeader
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.text.SimpleDateFormat
import java.util.*
import com.hbl.bot.ui.customviews.DialogCallback
import com.hbl.bot.utils.*
import kotlinx.android.synthetic.main.view_form_input.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class HawCifContactFragment8 : Fragment(), View.OnClickListener {
    var fCountryCode = ""
    var FATCAFlag = 0
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivPreferredMailingAddressSp: Spinner? = null
    var fivResidentialLandlineET: EditText? = null
    var fivCountryCodeET: EditText? = null
    var fivMobileNoET: EditText? = null
    var fivOfficeNoET: EditText? = null
    var fivEmailAddressET: EditText? = null
    var fivInternetBankingSP: Spinner? = null
    var customerBiometric = NadraVerify()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep2, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(4)
        viewModel.currentFragmentIndex.setValue(0)
        val txt = resources.getString(R.string.contact_information)
        val txt1 = " (1/4 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            init()
//        if (GlobalClass.isDummyDataTrue) {
//            loadDummyData()
//        }
            load()
            setLengthAndType()
            textWatcher()
            setConditions()
            onBackPress(view)
            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    fun callPreferenceMailing() {
        var lovRequest = LovRequest()
        lovRequest.condition = true
        (activity as HawActivity).customerDemoGraphicx.CUST_TYPE.let {
            lovRequest.conditionvalue = it
        }
        lovRequest.conditionname = Constants.CUST_SEGMENT
        lovRequest.identifier = Constants.PREFERENCES_MAILING_IDENTIFIER
        HBLHRStore.instance?.getPreferenceMailing(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : PreferencesMailingCallBack {
                override fun PreferencesMailingSuccess(response: PreferencesMailingResponse) {
                    response.data?.let {
                        setPreferenceMailing(it)
                    };
                }

                override fun PreferencesMailingFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setPreferenceMailing(it: ArrayList<PreferencesMailing>) {
        try {
            fivPreferredMailingAddress.setItemForPreferencesMailing(it)
            (activity as HawActivity).preferencesList = it
            (activity as HawActivity).sharedPreferenceManager.lovPreferencesMailing = it
            (activity as HawActivity).customerContacts.PREFERREDMAILADDRDESC.let { item ->
                (activity as HawActivity).globalClass?.findSpinnerPositionFromPreferenceMailingIndex(
                    it,
                    item
                ).let {
                    if (it != 0) {
                        fivPreferredMailingAddressSp?.setSelection(it!!)
                    } else {
                        var list = (activity as HawActivity).sharedPreferenceManager.hawKeys
                        for (x in 0 until list.size) {
                            if (list[x].IDENTIFIER == Constants.PREFERENCES_MAILING_IDENTIFIER) {
                                (activity as HawActivity).globalClass?.findSpinnerPositionFromPreferenceMailingIndex(
                                    (activity as HawActivity).sharedPreferenceManager.lovPreferencesMailing,
                                    list[x].LOV_DESC!!
                                ).let {
                                    fivPreferredMailingAddressSp?.setSelection(it!!)
                                }
                            }
                        }
                    }
                }
            }
            fivPreferredMailingAddress.remainSelection(it.size)
            (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    fun setConditions() {
        fivCountryCodeET?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.startsWith("001")) {
                    (activity as HawActivity).isCheckedUSDialCode = 1
                    isThisUSCodeCBID.visibility = View.VISIBLE
                } else {
                    (activity as HawActivity).isCheckedUSDialCode = 0
                    isThisUSCodeCBID.visibility = View.GONE
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        fivResidentialLandlineET?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (s!!.length == 6 && s!!.startsWith("001")) {
                        Log.i("Codes", s!!.substring(0, 3) + "," + s!!.substring(4, 6))
                        callCountryAndCityCode(s!!.substring(0, 3), s!!.substring(4, 6), 0, 1)
                    } else {
                        (activity as HawActivity).customerInfo.FATCA = 0
                        (activity as HawActivity).sharedPreferenceManager.customerInfo =
                            (activity as HawActivity).customerInfo
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        fivOfficeNoET?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.length == 6 && s!!.startsWith("001")) {
                    Log.i("Codes", s!!.substring(0, 3) + "," + s!!.substring(4, 6))
                    callCountryAndCityCode(s!!.substring(0, 3), s!!.substring(4, 6), 1, 0)
                } else {
                    (activity as HawActivity).sharedPreferenceManager.customerInfo.FATCA = 0
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        fivInternetBankingSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                if((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE=="A7"){
//                    fivInternetBankingSP?.setSelection(0)
//                    ToastUtils.normalShowToast(
//                        activity,
//                        resources!!.getString(R.string.minor_internet_banking_err),
//                        1
//                    )
//                }else {
                if (p2 == 1 && (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE != "A7") {
                    val c = Calendar.getInstance().time
                    val sdf = SimpleDateFormat("yyyy-MM-dd")
                    val currentDate: String = sdf.format(c)
                    if (!(activity as HawActivity).customerBiometric.datetime.isNullOrEmpty()) {
                        val biometricDate: String = Utils.getDateSplited(
                            (activity as HawActivity).customerBiometric.datetime,
                            0
                        )!!
                        if (biometricDate != currentDate) {
                            biometricDateAlertForInternetBanking()
                        }
                    } else {
                        fivInternetBankingSP?.setSelection(0)
                        ToastUtils.normalShowToast(
                            activity,
                            resources!!.getString(R.string.something_went_wrong) + " with biometric date",
                            1
                        )
                    }
                }
//                }
            }
        })
        fivEmailAddressET?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (GlobalClass.isValidEmail(s.toString())) {
                    fivEmailAddressET?.setError(
                        "Valid Email ",
                        resources!!.getDrawable(R.drawable.ic_done_white_18dp)
                    )
                    fivEmailAddressET?.requestFocus()
                } else {
                    fivEmailAddressET?.setError("Invalid Email")
                    fivEmailAddressET?.requestFocus()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) { // other stuffs
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) { // other stuffs
            }
        })
    }

    private fun biometricDateAlertForInternetBanking() {
        (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(requireContext(),
            resources!!.getString(R.string.biomatricHeaderAlertStr),
            resources!!.getString(R.string.biomatricAgainErr),
            resources!!.getString(R.string.biomatricPageStr),
            resources!!.getString(R.string.cancelStr),
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
                    fivInternetBankingSP?.setSelection(0)
                }

                override fun onPositiveClicked() {
                    super.onPositiveClicked()
                    if (findNavController().currentDestination?.id == R.id.CIFStep2) {
                        findNavController().navigate(R.id.action_goto_biometricPage)
                    }
                }
            })
    }

    fun init() {
        (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
        fivPreferredMailingAddressSp =
            fivPreferredMailingAddress.getSpinner(R.id.fivPreferredMailingAddress)
        fivResidentialLandlineET =
            fivResidentialLandline.getTextFromEditText(R.id.fivResidentialLandline)
        fivCountryCodeET = fivCountryCode.getTextFromEditText(R.id.fivCountryCode)
        fivMobileNoET = fivMobileNo.getTextFromEditText(R.id.fivMobileNo2)
        fivOfficeNoET = fivOfficeNo.getTextFromEditText(R.id.fivOfficeNo2)
        fivEmailAddressET = fivEmailAddress.getTextFromEditText(R.id.fivEmailAddress)
        fivInternetBankingSP = fivInternetBanking.getSpinner(R.id.internetBankingID)
        fivResidentialLandlineET?.requestFocus()
        fivOfficeNo.mandateInput.text = ""
        fivResidentialLandline.mandateInput.text = ""
        (activity as HawActivity).globalClass?.setDisbaled(fivCountryCodeET!!, false)
    }

    private fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
            /*ETB CASE*/
            (activity as HawActivity).globalClass?.setDisbaled(
                fivPreferredMailingAddressSp!!,
                false
            )
            (activity as HawActivity).globalClass?.setDisbaled(fivResidentialLandlineET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivCountryCodeET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivMobileNoET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivOfficeNoET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivEmailAddressET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivInternetBankingSP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivResidentialLandlineET!!, false)
        } else {
            /*NTB CASE*/
            (activity as HawActivity).globalClass?.setDisbaled(
                fivPreferredMailingAddressSp!!,
                false
            )
        }
    }

    fun load() {
        (activity as HawActivity).customerContacts.RES_LANDLINE
            .let { fivResidentialLandlineET?.setText(it) }
        (activity as HawActivity).customerContacts.COUNTRY_DIAL_CODE
            .let {

                fivCountryCodeET?.setText(it)

                (activity as HawActivity).customerContacts.COUNTRY_DIAL_CODE.let {
                    if (!it.isNullOrEmpty()) {
                        fivCountryCodeET?.setText(it)
                    } else {
                        var list =
                            (activity as HawActivity).sharedPreferenceManager.hawKeys
                        for (x in 0 until list.size) {
                            if (list[x].IDENTIFIER == Constants.INPUT_VALUES && list[x].NAME == Constants.COUNTRY_CODE_KEY) {
                                fivCountryCodeET?.setText(list[x].LOV_DESC)
                            }
                        }
                    }
                }
            }
        if (!(activity as HawActivity).customerContacts.MOBILE_NO.isNullOrEmpty()) {
            fivMobileNoET?.setText((activity as HawActivity).customerContacts.MOBILE_NO)
        } else if (!(activity as HawActivity).customerBiometric.MOBILE_NO.isNullOrEmpty()) {
            fivMobileNoET?.setText((activity as HawActivity).customerBiometric.MOBILE_NO)
        }
        (activity as HawActivity).customerContacts.OFFICE_NO
            .let { fivOfficeNoET?.setText(it) }
        (activity as HawActivity).customerContacts.EMAIL_ADDRESS
            .let { fivEmailAddressET?.setText(it) }
        (activity as HawActivity).globalClass?.showDialog(activity)
        if ((activity as HawActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()
        } else {
            setBool((activity as HawActivity).sharedPreferenceManager.lovBool)
        }
        if (isThisUSCodeCBID.isChecked && isThisUSCodeCBID.visibility == View.VISIBLE) {
            (activity as HawActivity).customerInfo.FATCA = 1
        } else if (fCountryCode.startsWith("001")) {
            (activity as HawActivity).customerInfo.FATCA = 1
        }
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore?.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let { setBool(it) };
                }

                override fun BoolFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setBool(it: ArrayList<Bool>) {
        fivInternetBanking.setItemForBools(it)
        (activity as HawActivity).boolList = it
        (activity as HawActivity).customerInfo.INTERNET_MOB_BNK_REQ.let { item ->
            (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
                it,
                item
            ).let {
                if (!(activity as HawActivity).customerInfo.INTERNET_MOB_BNK_REQ.isNullOrEmpty()) {
//                    if (!(activity as HawActivity).customerInfo.CUSTOMER_VISUALLY_IMPAIRED.isNullOrEmpty()) {
//                        fivInternetBankingSP?.setSelection(0)
//                    } else {
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
                        (activity as HawActivity).boolList,
                        (activity as HawActivity).customerInfo.INTERNET_MOB_BNK_REQ
                    ).let { fivInternetBankingSP?.setSelection(it!!) }
//                    }
                } else {
                    var list = (activity as HawActivity).sharedPreferenceManager.hawKeys
                    for (x in 0 until list.size) {
                        if (list[x].IDENTIFIER == Constants.BOOL_IDENTIFIER && list[x].NAME == Constants.BOOL_PHONE_BANKING_IDENTIFIER) {
                            (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
                                (activity as HawActivity).boolList,
                                list[x].LOV_CODE!!
                            ).let {
                                fivInternetBankingSP?.setSelection(it!!)
                            }
                        }
                    }
                }
            }
        }


//        if ((activity as HawActivity).sharedPreferenceManager.lovPreferencesMailing.isNullOrEmpty()) {
        callPreferenceMailing()
//        } else {
//            setPreferenceMailing((activity as HawActivity).sharedPreferenceManager.lovPreferencesMailing)
//        }
    }

    fun setLengthAndType() {
        fivResidentialLandlineET!!.setHint("Start with 0")
        fivOfficeNoET!!.setHint("Start with 0")
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivResidentialLandlineET!!,
            12,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivCountryCodeET!!,
            4,
            Constants.INPUT_TYPE_NUMBER
        )
        if (fivCountryCodeET?.text.toString().trim() == "0092" || fivCountryCodeET?.text.toString()
                .trim() == "092"
        ) {
            (activity as HawActivity).globalClass?.edittextTypeCount(
                fivMobileNoET!!,
                11,
                Constants.INPUT_TYPE_NUMBER
            )
        } else {
            (activity as HawActivity).globalClass?.edittextTypeCount(
                fivMobileNoET!!,
                16,
                Constants.INPUT_TYPE_NUMBER
            )
        }
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivOfficeNoET!!,
            12,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivEmailAddressET!!,
            60,
            Constants.INPUT_TYPE_CUSTOM_EMAIL_ALPHA
        )
    }

    fun loadDummyData() {
//        fivResidentialLandlineET?.setText("0213412030258")
//        fivCountryCodeET?.setText("92")
//        fivMobileNoET?.setText("03412030258")
//        fivOfficeNoET?.setText("03453164365")
//        fivEmailAddressET?.setText("info.xeeshan@gmail.com")
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                if (validation()) {
                    (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                    (this.context as HawActivity).globalClass?.showDialog(this.context)
                    java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                        saveAndNext()
                        setAllModelsInAOFRequestModel()
                        activity?.runOnUiThread {
                            (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                        }
                    })
                }
            }
            R.id.btBack -> {
//                if ((activity as HawActivity).customerInfo.PEP == "0" ||
//                    (activity as HawActivity).customerInfo.PEP == ""
//                ) {
                if (findNavController().currentDestination?.id == R.id.CIFStep2) {
                    findNavController().navigate(R.id.action_CIFStep2_to_CIFStep1_9)
                }
//                } else {
//                    if (findNavController().currentDestination?.id == R.id.CIFStep2)
//                        findNavController().navigate(R.id.action_CIFStep2_to_CIFStep1_13)
//                }
            }
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep2)
                    findNavController().navigate(R.id.action_CIFStep2_to_CIFStep1_13)
                true
            }
            false
        }
    }

    fun saveAndNext() {
        fivResidentialLandlineET?.text.toString()
            .let { (activity as HawActivity).customerContacts.RES_LANDLINE = it }
        fivCountryCodeET?.text.toString()
            .let { (activity as HawActivity).customerContacts.COUNTRY_DIAL_CODE = it }
        fivMobileNoET?.text.toString()
            .let { (activity as HawActivity).customerContacts.MOBILE_NO = it }
        fivOfficeNoET?.text.toString()
            .let { (activity as HawActivity).customerContacts.OFFICE_NO = it }
        fivEmailAddressET?.text.toString()
            .let { (activity as HawActivity).customerContacts.EMAIL_ADDRESS = it }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromNISABoolCode(
            (activity as HawActivity).boolList,
            fivInternetBankingSP?.selectedItem.toString()
        ).let {
            (activity as HawActivity).customerInfo.INTERNET_MOB_BNK_REQ = it
            (activity as HawActivity).customerAccounts.PHONE_BANK = it!!

        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromPreferenceMailingCode(
            (activity as HawActivity).preferencesList,
            fivPreferredMailingAddressSp?.selectedItem.toString()
        ).let {
            //TODO: set preferenceMailing
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerContacts.PREFERRED_MAIL_ADDR = it!!
                (activity as HawActivity).customerContacts.PREFERREDMAILADDRDESC =
                    fivPreferredMailingAddressSp?.selectedItem.toString()
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ
        if (isThisUSCodeCBID.isChecked && isThisUSCodeCBID.visibility == View.VISIBLE) {
            (activity as HawActivity).customerInfo.FATCA = 1
        } else if (fCountryCode.startsWith("001")) {
            (activity as HawActivity).customerInfo.FATCA = 1
        }
        (activity as HawActivity).sharedPreferenceManager.setCustomerContacts((activity as HawActivity).customerContacts)
        (activity as HawActivity).sharedPreferenceManager.customerInfo =
            (activity as HawActivity).customerInfo
        (activity as HawActivity).sharedPreferenceManager.customerAccount =
            (activity as HawActivity).customerAccounts
        (activity as HawActivity).customerContactList.add((activity as HawActivity).customerContacts)
        (activity as HawActivity).customerInfoList.add((activity as HawActivity).customerInfo)
        (activity as HawActivity).sharedPreferenceManager.setCustomerContacts((activity as HawActivity).customerContacts)
    }
    private fun textWatcher() {
        fivMobileNoET?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.contains("-")) {
                    fivMobileNoET?.error=getString(R.string.please_enter_valid_phone_number_4)
                    fivMobileNoET?.requestFocus()
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
    }
    fun validation(): Boolean {
        val email: String = fivEmailAddressET?.getText().toString().trim()
        val countryCode: String = fivCountryCodeET?.getText().toString().trim()
        val mobileNumber: String = fivMobileNoET?.getText().toString().trim()
        val fivResidentialLandlineNumber: String =
            fivResidentialLandlineET?.getText().toString().trim()
        val fivOfficeNo: String = fivOfficeNoET?.getText().toString().trim()
        /*if (fivResidentialLandlineNumber!!.isEmpty()) {
            fivResidentialLandlineET?.setError(resources!!.getString(R.string.please_enter_resindent_landline_number))
            fivResidentialLandlineET?.requestFocus()
            return false;
        } else*/ if (fivResidentialLandlineNumber.length != 0 && !fivResidentialLandlineNumber.startsWith("0")) {
            fivResidentialLandlineET?.setError(resources!!.getString(R.string.please_enter_resindent_landline_number4))
            fivResidentialLandlineET?.requestFocus()
            return false;
        } else if (fivResidentialLandlineNumber.length != 0 && fivResidentialLandlineNumber.length < 11 ) {
            fivResidentialLandlineET?.setError(resources!!.getString(R.string.please_enter_resindent_landline_number3))
            fivResidentialLandlineET?.requestFocus()
            return false;
        }  else if (countryCode.isEmpty() || countryCode.length < 3) {
            fivCountryCodeET?.setError(resources!!.getString(R.string.countryDialingCodeErr))
            fivCountryCodeET?.requestFocus()
            return false;
        } else if (fivPreferredMailingAddressSp?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_pre_enter_email),
                1
            )
            return false;
        } else if (mobileNumber.isEmpty()) {
            fivMobileNoET?.setError(resources!!.getString(R.string.please_enter_mobile_number))
            fivMobileNoET?.requestFocus()
            return false;
        } else if (!mobileNumber.startsWith("03") && (countryCode == "0092" || countryCode == "092")) {
            fivMobileNoET?.setError(resources!!.getString(R.string.please_enter_valid_phone_number_2))
            fivMobileNoET?.requestFocus()
            return false;
        } else if (mobileNumber.length < 11 && (countryCode == "0092" || countryCode == "092")) {
            fivMobileNoET?.setError(resources!!.getString(R.string.please_enter_valid_phone_number_3))
            fivMobileNoET?.requestFocus()
            return false;
        }else if (mobileNumber.contains("-")) {
            fivMobileNoET?.setError(resources!!.getString(R.string.please_enter_valid_phone_number_4))
            fivMobileNoET?.requestFocus()
            return false;
        } else if (email.length != 0 && !GlobalClass.isValidEmail(email)) {
            fivEmailAddressET?.setError(resources!!.getString(R.string.please_enter_valid_email))
            fivEmailAddressET?.requestFocus()
            return false;
        } else if (fivOfficeNo.length != 0 && !fivOfficeNo.startsWith("0") ) {
            fivOfficeNoET?.setError(resources!!.getString(R.string.please_enter_office_number4))
            fivOfficeNoET?.requestFocus()
            return false;
        } else if (fivOfficeNo.length != 0 && fivOfficeNo.length < 11 ) {
            fivOfficeNoET?.setError(resources!!.getString(R.string.please_enter_office_number3))
            fivOfficeNoET?.requestFocus()
            return false;
        }   else {
            return true
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as HawActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as HawActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as HawActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .isNullOrEmpty()
        ) {
            aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as HawActivity).aofAccountInfoRequest = it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled!!) {
            //TODO: set WORK_FLOW_CIF_AND_ACCOUNT
            (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "2"
            (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC =
                "ACCOUNT"
            request.identifier = Constants.SUBMIT_DRAFT_IDENTIFIER
        } else {
            request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
            //TODO: set WORK_FLOW_CIF_AND_ACCOUNT
            (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "3"
            (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC =
                "CIF AND ACCOUNT"
        }
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    (activity as HawActivity).globalClass?.hideLoader()
                    if (findNavController().currentDestination?.id == R.id.CIFStep2)
                        findNavController().navigate(R.id.action_CIFStep2_to_CIFStep2_3)
                    (activity as HawActivity).recyclerViewSetup()
                    (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callCountryAndCityCode(
        countryCode: String,
        cityCode: String,
        isOffice: Int,
        isResidence: Int
    ) {
        fCountryCode = countryCode
        var request = GetCountryCityCodeRequest()
        request.find.COUNTRY_DIAL_CODE = countryCode
        request.find.CITY_DIAL_CODE = cityCode
        request.identifier = Constants.COUNTRY_CANADA_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(request)
        Log.i("jsonResponse", json)
        (activity as HawActivity).globalClass?.showDialog(activity)
        HBLHRStore.instance?.getCountryCityCode(
            RetrofitEnums.URL_HBL,
            request,
            object : GetCountryCityCodeCallBack {
                override fun GetCountryCityCodeSuccess(response: CountryCityCodeResponse) {
                    try {
                        Log.i("isSuccessResponse", "True")
                        if (response.data.isNullOrEmpty() && isResidence == 1) {
                            (activity as HawActivity).residenceNumberFATCA = 1
                            Log.i(
                                "isOfficeOrResidence",
                                response.data?.toString() + " = " + (activity as HawActivity).residenceNumberFATCA
                            )
                        } else if (response.data.isNullOrEmpty() && isOffice == 1) {
                            (activity as HawActivity).officeNumberFATCA = 1
                            Log.i(
                                "isOfficeOrResidence",
                                response.data?.toString() + " = " + (activity as HawActivity).officeNumberFATCA
                            )
                        } else {
                            Log.i(
                                "isOfficeOrResidence",
                                response.data?.toString() + " = " + (activity as HawActivity).residenceNumberFATCA + ", " + (activity as HawActivity).officeNumberFATCA
                            )
                            (activity as HawActivity).residenceNumberFATCA = 0
                            (activity as HawActivity).officeNumberFATCA = 0
                        }
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun GetCountryCityCodeFailure(response: BaseResponse) {
                    Log.i("isSuccessResponse", "False")
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                    if (isOffice == 1) {
                        (activity as HawActivity).residenceNumberFATCA = 0
                    } else if (isResidence == 1) {
                        (activity as HawActivity).officeNumberFATCA = 0
                    }
//                    Utils.failedAwokeCalls((activity as HawActivity)) {
//                        callCountryAndCityCode(
//                            countryCode,
//                            cityCode,
//                            isOffice,
//                            isResidence
//                        )
//                    }
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
