package com.hbl.bot.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.hbl.bot.R
import kotlinx.android.synthetic.main.view_form_textview.view.*
import kotlinx.android.synthetic.main.view_form_textview.view.tv
import kotlinx.android.synthetic.main.view_form_textview.view.tvTitleUrdu
import kotlinx.android.synthetic.main.view_form_textview.view.tv_title

class FormTextView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {

    init {
        inflate(context, R.layout.view_form_textview, this)
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.FormTextView)
        tv_title.text = attributes.getString(R.styleable.FormTextView_titleTextViewInput)
        tvTitleUrdu.text = attributes.getString(R.styleable.FormTextView_titleTextViewInputUrdu)
        tv.text = attributes.getString(R.styleable.FormTextView_titleTextViewDisplay)
        mandateTextView.text = attributes.getString(R.styleable.FormTextView_mandatoryInputView)
        attributes.recycle()
        clearDate()
    }

    fun getTextFromTextView(id: Int): TextView {
        tv.id = id
        return tv
    }

    private fun clearDate() {
        clearDate.setOnClickListener {
            tv.text = resources!!.getString(R.string.date_picker)
        }
    }
}
