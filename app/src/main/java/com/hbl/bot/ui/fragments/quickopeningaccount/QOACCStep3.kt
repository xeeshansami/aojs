package com.hbl.bot.ui.fragments.quickopeningaccount

import android.content.ActivityNotFoundException
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import com.hbl.bot.R
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import kotlinx.android.synthetic.main.fragment_cifstep16.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class QOACCStep3 : Fragment(), View.OnClickListener {
    var myView: View? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try{
        if (myView == null) {
            // Inflate the layout for this fragment
            myView = inflater.inflate(R.layout.fragment_qoaccstep3, container, false)
        }
        } catch (e: UnsatisfiedLinkError) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        }finally {
            return myView
        }


    }

    fun showAccountNumber() {
        val number = Math.floor(Math.random() * 9000000000L).toLong() + 1000000000L
        fivAccountNumber.setText("0786" + number)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showAccountNumber()
        back_to_dashboard_btn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
//            R.id.btNext -> findNavController().navigate(R.id.action_CIFStep12_to_CIFStep13)
            R.id.back_to_dashboard_btn -> {
                activity?.finish()
            }
        }

    }
}
