package com.hbl.bot.ui.fragments.system

import android.content.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.GenDataCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.baseRM.DataRequest
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.CIFResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import kotlinx.android.synthetic.main.fragment_cifstep15.*
import kotlinx.android.synthetic.main.fragment_cifstep15.customerNameTv
import kotlinx.android.synthetic.main.fragment_cifstep15.customerSegmentTv
import kotlinx.android.synthetic.main.fragment_cifstep15.fivTrackingID_txtV
import kotlinx.android.synthetic.main.fragment_cifstep15.mysisID
import kotlinx.android.synthetic.main.fragment_cifstep15.purpOfCifTv
import kotlinx.android.synthetic.main.fragment_cifstep15.requestInitiateDateTimeTv
import kotlinx.android.synthetic.main.fragment_cifstep15.riskRatingTv
import kotlinx.android.synthetic.main.fragment_cifstep6.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

class HawFinalSummaryFragment47 : Fragment(), View.OnClickListener {
    var myView: View? = null
    var isDislaimer = false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep15, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity)
                .registerReceiver(mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST)
                );
            btGenerteAccount.setOnClickListener(this)

            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            setDataOnLoad()
//        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
//            fivTrackingNumber.setText(
//                it
//            )
//        }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun setDataOnLoad() {
        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
            fivTrackingID_txtV.setText(
                it
            )
        }
        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
            mysisID.setText(
                it.toString()
            )
        }

        (activity as HawActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC.let {
            accountType.setText(
                it.toString()
            )
        }
        (activity as HawActivity).riskRating.let { riskRatingTv.setText(it) }
        (activity as HawActivity).globalClass?.currentDate.let {
            requestInitiateDateTimeTv.setText(
                it
            )
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUSTTYPEDESC.let {
            customerSegmentTv.setText(
                it
            )
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.TITLE_OF_ACCT.let {
            customerNameTv.setText(
                it
            )
        }

        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_DESC.let {
            purpOfCifTv.setText(
                it
            )
        }

    }

    fun cifAndAccountConditions() {
        var cifPlusAccount = (activity as HawActivity).sharedPreferenceManager.appStatus.CIFAcct
        var cif = (activity as HawActivity).sharedPreferenceManager.appStatus.CIF
        var status =
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.WORK_FLOW_CODE
        if (status == "3" || status == "2") {
            btGenerteAccount.visibility = View.VISIBLE
//            btGenerteCif.visibility = View.GONE
        } else if (status == "1") {
            btGenerteAccount.visibility = View.GONE
//            btGenerteCif.visibility = View.VISIBLE
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btGenerteAccount -> activity?.finish()
        }
    }

    private fun disclaimerDialog(isCifOrAccount: Int) {
        try {
            var dialogBuilder = AlertDialog.Builder(activity as HawActivity)
            val layoutView: View = layoutInflater.inflate(R.layout.popup_disclaimer, null)
            dialogBuilder.setView(layoutView)
            var alertDialog = dialogBuilder.create()
            alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            alertDialog.show()
            var toolbar = layoutView.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
            var yes = layoutView.findViewById<Button>(R.id.yes_action)
            var no = layoutView.findViewById<Button>(R.id.no_action)
            toolbar.title = "DISCLAIMER"
            yes.setOnClickListener(View.OnClickListener {
                (activity as HawActivity).finish()
                /* if(isCifOrAccount==1) {
                     toolbar.subtitle = "Generate Account"
                     callGenerateCifAccount()
                 }else{
                     toolbar.subtitle = "Generate CIF"
                     callGenerateCif()
                 }*/
                isDislaimer = true
                alertDialog.dismiss()
            })
            no.setOnClickListener(View.OnClickListener {
                /*if(isCifOrAccount==1) {
                    toolbar.subtitle = "Generate Account"
                    callGenerateCifAccount()
                }else{
                    toolbar.subtitle = "Generate CIF"
                    callGenerateCif()
                }*/
                (activity as HawActivity).finish()
                isDislaimer = false
                alertDialog.dismiss()
            })
            alertDialog.show()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun callGenerateCifAccount() {
        var custSegType =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        (activity as HawActivity).globalClass?.showDialog(activity)
        var dataRequest = DataRequest()
        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.let {
            dataRequest.data = it
        }
        dataRequest.data?.TRACKING_ID =
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID
        if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
            dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ACCT_SNG_JNT = "3"
            dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ACCT_SNG_JNT_DESC = "Minor Account"
        } else {
            dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ACCT_SNG_JNT = "1"
            dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ACCT_SNG_JNT_DESC = "Individual Account"
        }
        dataRequest.data?.CUST_ACCOUNTS?.get(0)?.BRANCHNAME =
            GlobalClass.sharedPreferenceManager!!.getLoginData()
                .getBRANCHNAME().toString()

        dataRequest.data?.CUST_ACCOUNTS?.get(0)?.BRANCH_CODE =
            GlobalClass.sharedPreferenceManager!!.getLoginData()
                .getBRANCHCODE().toString()
        dataRequest.data?.CUST_ACCOUNTS?.get(0)?.ID_DOC_NO =
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ID_DOC_NO
        (activity as HawActivity).aofAccountInfoRequest.isDisclaimer = isDislaimer
        var newName = (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        if (newName.length > 35) {
            newName = newName.substring(0, 35)
            (activity as HawActivity).aofAccountInfoRequest.NAME = newName
            (activity as HawActivity).aofAccountInfoRequest.let {
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
            }
        }
        dataRequest.payload = Constants.PAYLOAD
        HBLHRStore.instance?.genCIFAndAccount(
            RetrofitEnums.URL_HBL,
            dataRequest,
            object : GenDataCallBack {
                override fun GenCifSuccess(response: CIFResponse) {
                    try {
                        ToastUtils.normalShowToast(context, response.message,2)
                        var bundle = Bundle()
                        response?.data?.get(0)
                            ?.ACCOUNTNO.let {
                                bundle.putString(
                                    Constants.ACCOUNT_NUMBER_KEY,
                                    it
                                )
                            }

                        response?.data?.get(0)?.CIFNO.let {
                            bundle.putString(
                                Constants.CIF_NUMBER_KEY,
                                it
                            )
                        }
                        response?.data?.get(0)?.IBAN.let {
                            bundle.putString(
                                Constants.IBAN_NUMBER_KEY,
                                it
                            )
                        }

                        /*  if (custSegType == "A6") {

                              response?.data?.getOrNull(0)
                                  ?.ACCOUNTNO.let {
                                      val strs = it?.split(",")?.toTypedArray()
                                      bundle.putString(Constants.ACCOUNT_NUMBER_KEY, strs?.get(1))
                                  }
                          } else {*/
//                        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.WORK_FLOW_CODE == "3" || (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.WORK_FLOW_CODE == "1") {
//
//                        } else if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.WORK_FLOW_CODE == "2") {
//                            response?.data?.get(0)
//                                ?.ACCOUNTNO.let {
//                                    bundle.putString(
//                                        Constants.ACCOUNT_NUMBER_KEY,
//                                        it
//                                    )
//                                }
//                        }


//                    }
                        response.message.let {
                            bundle.putString(Constants.ACCOUNT_NUMBER_MESSAGE_KEY, it)
                        }
                        findNavController().navigate(R.id.action_CIFStep15_to_CIFStep16, bundle)
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun GenCifFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun callGenerateCif() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var dataRequest = DataRequest()
        var newName = (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        (activity as HawActivity).aofAccountInfoRequest.isDisclaimer = isDislaimer
        if (newName.length > 35) {
            newName = newName.substring(0, 35)
            (activity as HawActivity).aofAccountInfoRequest.NAME = newName
            (activity as HawActivity).aofAccountInfoRequest.let {
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
            }
        }
        dataRequest.payload = Constants.PAYLOAD
        HBLHRStore.instance?.genCIF(
            RetrofitEnums.URL_HBL,
            dataRequest,
            object : GenDataCallBack {
                override fun GenCifSuccess(response: CIFResponse) {
                    try {
                        ToastUtils.normalShowToast(context, response.message,2)
                        var bundle = Bundle()
                        response?.data?.get(0)
                            ?.ACCOUNTNO.let {
                                bundle.putString(
                                    Constants.ACCOUNT_NUMBER_KEY,
                                    it
                                )
                            }
                        response?.data?.get(0)?.CIFNO.let {
                            bundle.putString(
                                Constants.CIF_NUMBER_KEY,
                                it
                            )
                        }
                        response?.data?.get(0)?.IBAN.let {
                            bundle.putString(
                                Constants.IBAN_NUMBER_KEY,
                                it
                            )
                        }
                        response.message.let {
                            bundle.putString(Constants.ACCOUNT_NUMBER_MESSAGE_KEY, it)
                        }
                        findNavController().navigate(R.id.action_CIFStep15_to_CIFStep16, bundle)
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun GenCifFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })

    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}