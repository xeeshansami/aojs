package com.hbl.bot.ui.fragments.spark.accounts


import android.content.*
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController

import com.hbl.bot.R
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep9.*
import kotlinx.android.synthetic.main.fragment_cifstep9.btBack
import kotlinx.android.synthetic.main.fragment_cifstep9.btNext
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class CIFStep9 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels(null)
    var myView: View? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try{
        if (myView == null) {
            // Inflate the layout for this fragment
            myView = inflater.inflate(R.layout.fragment_cifstep9, container, false)
        }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try{
        super.onViewCreated(view, savedInstanceState)
        LocalBroadcastManager.getInstance(activity as CIFRootActivity).registerReceiver(mStatusCodeResponse, IntentFilter(
            Constants.STATUS_BROADCAST)
        );
        viewModel.indicator
//        viewModel.currentFragmentIndex.value = 0
//        cifRootFragment.svpFIndicator.visibility=View.GONE
//        (activity as CIFRootActivity).tvTitle.visibility=View.GONE
        btNext.setOnClickListener(this)
        btBack.setOnClickListener(this)
        (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
            fivTrackingID_txtV.setText(
                it
            )
        }
        (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
            mysisID.setText(
                it.toString()
            )
        }

        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC.let {
            accountType.setText(
                it.toString()
            )
        }
        (activity as CIFRootActivity).riskRating.let { riskRatingTv.setText(it) }
        (activity as CIFRootActivity).globalClass?.currentDate.let {
            requestInitiateDateTimeTv.setText(
                it
            )
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUSTTYPEDESC.let {
            customerSegmentTv.setText(
                it
            )
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.FULL_NAME.let {
            customerNameTv.setText(
                it
            )
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.CIF_NO.let {
            cifNumberTv.setText(
                it
            )
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_DESC.let {
            purpOfCifTv.setText(
                it
            )
        }
        (activity as CIFRootActivity).sharedPreferenceManager.getStringFromSharedPreferences(
            SharedPreferenceManager.ACCOUNT_NUMBER_GENERATED
        ).let { AccountNumberID.setText(it) }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btNext -> findNavController().navigate(R.id.action_CIFStep9_to_CIFStep15)

//            R.id.btBack -> {
//                if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.COM_UPLOAD_DOC == "1") {
//                    findNavController().navigate(R.id.action_CIFStep9_to_CIFStep12_2)
//                }
//                else
//                {
//
//                }
//            }
        }
    }
    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            }catch (ex:Exception){
            }
        }
    }
    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity).unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
