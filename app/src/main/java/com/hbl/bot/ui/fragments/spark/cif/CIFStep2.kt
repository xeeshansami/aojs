package com.hbl.bot.ui.fragments.spark.cif


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.GetCountryCityCodeRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.NadraVerify
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep2.*
import kotlinx.android.synthetic.main.fragment_cifstep2.btBack
import kotlinx.android.synthetic.main.fragment_cifstep2.btNext
import kotlinx.android.synthetic.main.fragment_cifstep2.formSectionHeader
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.text.SimpleDateFormat
import java.util.*
import com.hbl.bot.ui.customviews.DialogCallback
import com.hbl.bot.utils.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class CIFStep2 : Fragment(), View.OnClickListener {
    var fCountryCode = ""
    var FATCAFlag = 0
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivPreferredMailingAddressSp: Spinner? = null
    var fivResidentialLandlineET: EditText? = null
    var fivCountryCodeET: EditText? = null
    var fivMobileNoET: EditText? = null
    var fivOfficeNoET: EditText? = null
    var fivEmailAddressET: EditText? = null
    var fivInternetBankingSP: Spinner? = null
    var customerBiometric = NadraVerify()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep2, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(4)
        viewModel.currentFragmentIndex.setValue(0)
        val txt = resources.getString(R.string.contact_information)
        val txt1 = " (1/4 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try{
        super.onViewCreated(view, savedInstanceState)
        LocalBroadcastManager.getInstance(activity as CIFRootActivity).registerReceiver(
            mStatusCodeResponse, IntentFilter(
                Constants.STATUS_BROADCAST
            )
        );
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
        btNext.setOnClickListener(this)
        btBack.setOnClickListener(this)
        header()
        init()
//        if (GlobalClass.isDummyDataTrue) {
//            loadDummyData()
//        }
        load()
        setLengthAndType()
        setConditions()
        onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }finally {

        }
    }

    fun callPreferenceMailing() {
        var lovRequest = LovRequest()
        lovRequest.condition = true
        (activity as CIFRootActivity).customerDemoGraphicx.CUST_TYPE.let {
            lovRequest.conditionvalue = it
        }
        lovRequest.conditionname = Constants.CUST_SEGMENT
        lovRequest.identifier = Constants.PREFERENCES_MAILING_IDENTIFIER
        Gson().toJson(lovRequest)
        HBLHRStore.instance?.getPreferenceMailing(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : PreferencesMailingCallBack {
                override fun PreferencesMailingSuccess(response: PreferencesMailingResponse) {
                    response.data?.let {
                        setPreferenceMailing(it)
                    };
                }

                override fun PreferencesMailingFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setPreferenceMailing(it: ArrayList<PreferencesMailing>) {
        try{
        fivPreferredMailingAddress.setItemForPreferencesMailing(it)
        (activity as CIFRootActivity).preferencesList = it
        (activity as CIFRootActivity).sharedPreferenceManager.lovPreferencesMailing = it
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPreferenceMailingIndex(
            it,
            (activity as CIFRootActivity).customerContacts.PREFERREDMAILADDRDESC
        ).let {
            fivPreferredMailingAddressSp?.setSelection(it!!)
        }
        fivPreferredMailingAddress.remainSelection(it.size)
        (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }finally {

        }
    }

    fun setConditions() {
        fivCountryCodeET?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.startsWith("001")) {
                    (activity as CIFRootActivity).isCheckedUSDialCode = 1
                    isThisUSCodeCBID.visibility = View.VISIBLE
                } else {
                    (activity as CIFRootActivity).isCheckedUSDialCode = 0
                    isThisUSCodeCBID.visibility = View.GONE
                }
                if (fivCountryCodeET?.text.toString().trim() == "0092" || fivCountryCodeET?.text.toString()
                        .trim() == "092"
                ) {
                    (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                        fivMobileNoET!!,
                        11,
                        Constants.INPUT_TYPE_NUMBER
                    )
                } else {
                    (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                        fivMobileNoET!!,
                        16,
                        Constants.INPUT_TYPE_NUMBER
                    )
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        fivResidentialLandlineET?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try{
                if (s!!.length == 6 && s!!.startsWith("001")) {
                    Log.i("Codes", s!!.substring(0, 3) + "," + s!!.substring(4, 6))
                    callCountryAndCityCode(s!!.substring(0, 3), s!!.substring(4, 6), 0, 1)
                } else {
                    (activity as CIFRootActivity).customerInfo.FATCA = 0
                    (activity as CIFRootActivity).sharedPreferenceManager.customerInfo =
                        (activity as CIFRootActivity).customerInfo
                }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                } finally {

                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        fivOfficeNoET?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s!!.length == 6 && s!!.startsWith("001")) {
                    Log.i("Codes", s!!.substring(0, 3) + "," + s!!.substring(4, 6))
                    callCountryAndCityCode(s!!.substring(0, 3), s!!.substring(4, 6), 1, 0)
                } else {
                    (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.FATCA = 0
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })
        fivInternetBankingSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
//                if((activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE=="A7"){
//                    fivInternetBankingSP?.setSelection(0)
//                    ToastUtils.normalShowToast(
//                        activity,
//                        resources!!.getString(R.string.minor_internet_banking_err),
//                        1
//                    )
//                }else {
                    if (p2 == 1 && (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE!="A7") {
                        val c = Calendar.getInstance().time
                        val sdf = SimpleDateFormat("yyyy-MM-dd")
                        val currentDate: String = sdf.format(c)
                        if (!(activity as CIFRootActivity).customerBiometric.datetime.isNullOrEmpty()) {
                            val biometricDate: String = Utils.getDateSplited(
                                (activity as CIFRootActivity).customerBiometric.datetime,
                                0
                            )!!
                            if (biometricDate != currentDate) {
                                biometricDateAlertForInternetBanking()
                            }
                        } else {
                            fivInternetBankingSP?.setSelection(0)
                            ToastUtils.normalShowToast(
                                activity,
                                resources!!.getString(R.string.something_went_wrong) + " with biometric date",
                                1
                            )
                        }
                    }
//                }
            }
        })
        fivEmailAddressET?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                if (GlobalClass.isValidEmail(s.toString())) {
                    fivEmailAddressET?.setError(
                        "Valid Email ",
                        resources!!.getDrawable(R.drawable.ic_done_white_18dp)
                    )
                    fivEmailAddressET?.requestFocus()
                } else {
                    fivEmailAddressET?.setError("Invalid Email")
                    fivEmailAddressET?.requestFocus()
                }
            }

            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) { // other stuffs
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) { // other stuffs
            }
        })
    }

    private fun biometricDateAlertForInternetBanking() {
        (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(requireContext(),
            resources!!.getString(R.string.biomatricHeaderAlertStr),
            resources!!.getString(R.string.biomatricAgainErr),
            resources!!.getString(R.string.biomatricPageStr),
            resources!!.getString(R.string.cancelStr),
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
                    fivInternetBankingSP?.setSelection(0)
                }

                override fun onPositiveClicked() {
                    super.onPositiveClicked()
                    if (findNavController().currentDestination?.id == R.id.CIFStep2) {
                        findNavController().navigate(R.id.action_goto_biometricPage)
                    }
                }
            })
    }

    fun init() {
        (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
        fivPreferredMailingAddressSp =
            fivPreferredMailingAddress.getSpinner(R.id.fivPreferredMailingAddress)
        fivResidentialLandlineET =
            fivResidentialLandline.getTextFromEditText(R.id.fivResidentialLandline)
        fivCountryCodeET = fivCountryCode.getTextFromEditText(R.id.fivCountryCode)
        fivMobileNoET = fivMobileNo.getTextFromEditText(R.id.fivMobileNo2)
        fivOfficeNoET = fivOfficeNo.getTextFromEditText(R.id.fivOfficeNo2)
        fivEmailAddressET = fivEmailAddress.getTextFromEditText(R.id.fivEmailAddress)
        fivInternetBankingSP = fivInternetBanking.getSpinner(R.id.internetBankingID)
        fivResidentialLandlineET?.requestFocus()
    }

    fun load() {
        (activity as CIFRootActivity).customerContacts.RES_LANDLINE
            .let { fivResidentialLandlineET?.setText(it) }
        (activity as CIFRootActivity).customerContacts.COUNTRY_DIAL_CODE
            .let { fivCountryCodeET?.setText(it) }
        if ((activity as CIFRootActivity).customerContacts.MOBILE_NO.isNullOrEmpty()) {
            (activity as CIFRootActivity).customerBiometric.MOBILE_NO
                .let { fivMobileNoET?.setText(it) }
        } else {
            (activity as CIFRootActivity).customerContacts.MOBILE_NO
                .let { fivMobileNoET?.setText(it) }
        }
        (activity as CIFRootActivity).customerContacts.OFFICE_NO
            .let { fivOfficeNoET?.setText(it) }
        (activity as CIFRootActivity).customerContacts.EMAIL_ADDRESS
            .let { fivEmailAddressET?.setText(it) }
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()
        } else {
            setBool((activity as CIFRootActivity).sharedPreferenceManager.lovBool)
        }
        if (isThisUSCodeCBID.isChecked && isThisUSCodeCBID.visibility == View.VISIBLE) {
            (activity as CIFRootActivity).customerInfo.FATCA = 1
        } else if (fCountryCode.startsWith("001")) {
            (activity as CIFRootActivity).customerInfo.FATCA = 1
        }
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore?.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let { setBool(it) };
                }

                override fun BoolFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setBool(it: ArrayList<Bool>) {
        fivInternetBanking.setItemForBools(it)
        if (!(activity as CIFRootActivity).customerInfo.CUSTOMER_VISUALLY_IMPAIRED.isNullOrEmpty()) {
            fivInternetBankingSP?.setSelection(0)
        } else {
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolIndex(
                it,
                (activity as CIFRootActivity).customerInfo.INTERNET_MOB_BNK_REQ
            ).let { fivInternetBankingSP?.setSelection(it!!) }
        }
//        if ((activity as CIFRootActivity).sharedPreferenceManager.lovPreferencesMailing.isNullOrEmpty()) {
            callPreferenceMailing()
//        } else {
//            setPreferenceMailing((activity as CIFRootActivity).sharedPreferenceManager.lovPreferencesMailing)
//        }
    }

    fun setLengthAndType() {
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivResidentialLandlineET!!,
            20,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivCountryCodeET!!,
            4,
            Constants.INPUT_TYPE_NUMBER
        )
        if (fivCountryCodeET?.text.toString().trim() == "0092" || fivCountryCodeET?.text.toString()
                .trim() == "092"
        ) {
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                fivMobileNoET!!,
                11,
                Constants.INPUT_TYPE_NUMBER
            )
        } else {
            (activity as CIFRootActivity).globalClass?.edittextTypeCount(
                fivMobileNoET!!,
                16,
                Constants.INPUT_TYPE_NUMBER
            )
        }
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivOfficeNoET!!,
            20,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivEmailAddressET!!,
            60,
            Constants.INPUT_TYPE_CUSTOM_EMAIL_ALPHA
        )
    }

    fun loadDummyData() {
        fivResidentialLandlineET?.setText("0213412030258")
        fivCountryCodeET?.setText("92")
        fivMobileNoET?.setText("03412030258")
        fivOfficeNoET?.setText("03453164365")
        fivEmailAddressET?.setText("info.xeeshan@gmail.com")
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                if (validation()) {
                    (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!, false)
                    (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                    java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                        saveAndNext()
                        setAllModelsInAOFRequestModel()
                        activity?.runOnUiThread {
                            (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                        }
                    })
                }
            }
            R.id.btBack -> {
                if ((activity as CIFRootActivity).customerInfo.PEP == "0" ||
                    (activity as CIFRootActivity).customerInfo.PEP == ""
                ) {
                    if (findNavController().currentDestination?.id == R.id.CIFStep2) {
                        findNavController().navigate(R.id.action_CIFStep2_to_CIFStep1_9)
                    }
                } else {
                    if (findNavController().currentDestination?.id == R.id.CIFStep2)
                        findNavController().navigate(R.id.action_CIFStep2_to_CIFStep1_13)
                }
            }
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep2)
                    findNavController().navigate(R.id.action_CIFStep2_to_CIFStep1_13)
                true
            }
            false
        }
    }

    fun saveAndNext() {
        fivResidentialLandlineET?.text.toString()
            .let { (activity as CIFRootActivity).customerContacts.RES_LANDLINE = it }
        fivCountryCodeET?.text.toString()
            .let { (activity as CIFRootActivity).customerContacts.COUNTRY_DIAL_CODE = it }
        fivMobileNoET?.text.toString()
            .let { (activity as CIFRootActivity).customerContacts.MOBILE_NO = it }
        fivOfficeNoET?.text.toString()
            .let { (activity as CIFRootActivity).customerContacts.OFFICE_NO = it }
        fivEmailAddressET?.text.toString()
            .let { (activity as CIFRootActivity).customerContacts.EMAIL_ADDRESS = it }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivInternetBankingSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerInfo.INTERNET_MOB_BNK_REQ = it
            (activity as CIFRootActivity).customerAccounts.PHONE_BANK = it!!

        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPreferenceMailingCode(
            (activity as CIFRootActivity).preferencesList,
            fivPreferredMailingAddressSp?.selectedItem.toString()
        ).let {
            //TODO: set preferenceMailing
            if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerContacts.PREFERRED_MAIL_ADDR = it!!
                (activity as CIFRootActivity).customerContacts.PREFERREDMAILADDRDESC =
                    fivPreferredMailingAddressSp?.selectedItem.toString()
            }
        }

        if (isThisUSCodeCBID.isChecked && isThisUSCodeCBID.visibility == View.VISIBLE) {
            (activity as CIFRootActivity).customerInfo.FATCA = 1
        } else if (fCountryCode.startsWith("001")) {
            (activity as CIFRootActivity).customerInfo.FATCA = 1
        }
        (activity as CIFRootActivity).sharedPreferenceManager.setCustomerContacts((activity as CIFRootActivity).customerContacts)
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo = (activity as CIFRootActivity).customerInfo
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount = (activity as CIFRootActivity).customerAccounts
        (activity as CIFRootActivity).customerContactList.add((activity as CIFRootActivity).customerContacts)
        (activity as CIFRootActivity).customerInfoList.add((activity as CIFRootActivity).customerInfo)
        (activity as CIFRootActivity).sharedPreferenceManager.setCustomerContacts((activity as CIFRootActivity).customerContacts)
    }

    fun validation(): Boolean {
        val email: String = fivEmailAddressET?.getText().toString().trim()
        val countryCode: String = fivCountryCodeET?.getText().toString().trim()
        val mobileNumber: String = fivMobileNoET?.getText().toString().trim()
        if (fivPreferredMailingAddressSp?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_pre_enter_email),
                1
            )
            return false;
        } /*else if (email.length == 0) {
            fivEmailAddressET?.setError(resources!!.getString(R.string.please_enter_email))
            fivEmailAddressET?.requestFocus()
            return false;
        } */ else if (email.length != 0 && !GlobalClass.isValidEmail(email)) {
            fivEmailAddressET?.setError(resources!!.getString(R.string.please_enter_valid_email))
            fivEmailAddressET?.requestFocus()
            return false;
        } else if (countryCode.isEmpty() || countryCode.length < 3) {
            fivCountryCodeET?.setError(resources!!.getString(R.string.countryDialingCodeErr))
            fivCountryCodeET?.requestFocus()
            return false;
        } else if (mobileNumber.isEmpty()) {
            fivMobileNoET?.setError(resources!!.getString(R.string.please_enter_mobile_number))
            fivMobileNoET?.requestFocus()
            return false;
        } else if (!mobileNumber.startsWith("03") && (countryCode == "0092" || countryCode == "092")) {
            fivMobileNoET?.setError(resources!!.getString(R.string.please_enter_valid_phone_number_2))
            fivMobileNoET?.requestFocus()
            return false;
        } else if (mobileNumber.length != 11 && (countryCode == "0092" || countryCode == "092")) {
            fivMobileNoET?.setError(resources!!.getString(R.string.please_enter_valid_phone_number_3))
            fivMobileNoET?.requestFocus()
            return false;
        } else {
            return true
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as CIFRootActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as CIFRootActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as CIFRootActivity).aofAccountInfoRequest = it
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                    if (findNavController().currentDestination?.id == R.id.CIFStep2)
                        findNavController().navigate(R.id.action_CIFStep2_to_CIFStep2_3)
                    (activity as CIFRootActivity).recyclerViewSetup()
                    (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callCountryAndCityCode(
        countryCode: String,
        cityCode: String,
        isOffice: Int,
        isResidence: Int
    ) {
        fCountryCode = countryCode
        var request = GetCountryCityCodeRequest()
        request.find.COUNTRY_DIAL_CODE = countryCode
        request.find.CITY_DIAL_CODE = cityCode
        request.identifier = Constants.COUNTRY_CANADA_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(request)
        Log.i("jsonResponse", json)
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        HBLHRStore.instance?.getCountryCityCode(
            RetrofitEnums.URL_HBL,
            request,
            object : GetCountryCityCodeCallBack {
                override fun GetCountryCityCodeSuccess(response: CountryCityCodeResponse) {
                    try{
                    Log.i("isSuccessResponse", "True")
                    if (response.data.isNullOrEmpty() && isResidence == 1) {
                        (activity as CIFRootActivity).residenceNumberFATCA = 1
                        Log.i(
                            "isOfficeOrResidence",
                            response.data?.toString() + " = " + (activity as CIFRootActivity).residenceNumberFATCA
                        )
                    } else if (response.data.isNullOrEmpty() && isOffice == 1) {
                        (activity as CIFRootActivity).officeNumberFATCA = 1
                        Log.i(
                            "isOfficeOrResidence",
                            response.data?.toString() + " = " + (activity as CIFRootActivity).officeNumberFATCA
                        )
                    } else {
                        Log.i(
                            "isOfficeOrResidence",
                            response.data?.toString() + " = " + (activity as CIFRootActivity).residenceNumberFATCA + ", " + (activity as CIFRootActivity).officeNumberFATCA
                        )
                        (activity as CIFRootActivity).residenceNumberFATCA = 0
                        (activity as CIFRootActivity).officeNumberFATCA = 0
                    }
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun GetCountryCityCodeFailure(response: BaseResponse) {
                    Log.i("isSuccessResponse", "False")
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                    if (isOffice == 1) {
                        (activity as CIFRootActivity).residenceNumberFATCA = 0
                    } else if (isResidence == 1) {
                        (activity as CIFRootActivity).officeNumberFATCA = 0
                    }
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) {callCountryAndCityCode(countryCode,cityCode,isOffice,isResidence) }
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
