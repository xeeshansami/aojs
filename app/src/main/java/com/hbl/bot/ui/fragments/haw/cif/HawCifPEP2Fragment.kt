package com.hbl.bot.ui.fragments.cif


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.SourceOfWealth
import com.hbl.bot.network.models.response.baseRM.SourceOffund
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep1_12.*
import kotlinx.android.synthetic.main.fragment_cifstep1_12.btBack
import kotlinx.android.synthetic.main.fragment_cifstep1_12.btNext
import kotlinx.android.synthetic.main.fragment_cifstep1_12.formSectionHeader
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class HawCifPEP2Fragment : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var sourceOfFundSp: Spinner? = null
    var otherSourceOfFundET: EditText? = null
    var sourceOfWealthSp: Spinner? = null
    var otherSourceOfWealthET: EditText? = null
    var isAdversMediaSp: Spinner? = null
    var additionalInfoET: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try{
        if (myView == null) {
            // Inflate the layout for this fragment
            myView = inflater.inflate(R.layout.fragment_cifstep1_12, container, false)

        }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(3)
        viewModel.currentFragmentIndex.setValue(1)
        val txt = resources.getString(R.string.pep_details)
        val txt1 = " (2/3 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(mStatusCodeResponse, IntentFilter(
            Constants.STATUS_BROADCAST)
        );
        (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
        btNext.setOnClickListener(this)
        btBack.setOnClickListener(this)
        header()
        init()
        load()
        setLengthAndType()
//        if (GlobalClass.isDummyDataTrue) {
//            loadDummyData()
//        }
        setConditions()
        onBackPress(view)
    }


    private fun setConditions() {
        sourceOfFundSp?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if ((activity as HawActivity).sourceOfFundList.get(position).SOF_CODE.equals("06")) {
                    fivOtherSourceOfIncome.mandateInput.text = "*"
                    (activity as HawActivity).globalClass?.setEnabled(otherSourceOfFundET!!, true)
                } else {
                    fivOtherSourceOfIncome.mandateInput.text = ""
                    (activity as HawActivity).globalClass?.setDisbaled(otherSourceOfFundET!!)
                }
                if ((activity as HawActivity).customerPep.OTHER_SOURCE_OF_FUND.isNullOrEmpty()) {
                    otherSourceOfFundET?.setText("")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
        sourceOfWealthSp?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                if ((activity as HawActivity).sourceOfWealthList.get(position).SOW_CODE.equals("05")) {
                    fivOtherSourceOfIncome2.mandateInput.text = "*"
                    (activity as HawActivity).globalClass?.setEnabled(
                        otherSourceOfWealthET!!,
                        true
                    )
                } else {
                    fivOtherSourceOfIncome2.mandateInput.text = ""
                    (activity as HawActivity).globalClass?.setDisbaled(otherSourceOfWealthET!!)
                }
                if ((activity as HawActivity).customerPep.OTHER_SOURCE_OF_WEALTH.isNullOrEmpty()) {
                    otherSourceOfWealthET?.setText("")
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }
        }
    }

    fun init() {
        sourceOfFundSp = fivSourceOfIncome.getSpinner(R.id.fivSourceOfIncome1)
        otherSourceOfFundET = fivOtherSourceOfIncome.getTextFromEditText(R.id.otherSourceOfFundID1)
        sourceOfWealthSp = fivSourceOfWealth.getSpinner(R.id.sourceOfWealthID)
        otherSourceOfWealthET =
            fivOtherSourceOfIncome2.getTextFromEditText(R.id.otherSourceOfWealthID)
        isAdversMediaSp = fivAdverseMedia.getSpinner(R.id.adversMediaID)
        additionalInfoET = fivAdditionalInfo.getTextFromEditText(R.id.fivAdditionalInfo)
    }

    fun setLengthAndType() {
        (activity as HawActivity).globalClass?.edittextTypeCount(
            otherSourceOfFundET!!,
            50,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            otherSourceOfWealthET!!,
            50,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            additionalInfoET!!,
            50,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
    }

    fun loadDummyData() {
        additionalInfoET?.setText("There is a other resource of income as freelance")
    }

    fun load() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        if ((activity as HawActivity).sharedPreferenceManager.lovSourceOffundIncome.isNullOrEmpty()) {
            callSourceOffund()
        } else {
            setSourceOffund((activity as HawActivity).sharedPreferenceManager.lovSourceOffundIncome)
        }
    }

    private fun setSourceOffund(it: ArrayList<SourceOffund>) {
        (activity as HawActivity).sourceOfFundList = it
        (activity as HawActivity).sharedPreferenceManager.lovSourceOffundIncome = it
        fivSourceOfIncome.setItemForSourceOffund(it)
        (activity as HawActivity).globalClass?.findSpinnerPositionFromSourceOfFundIndex(
            it,
            (activity as HawActivity).customerPep.DESC_SOURCE_OF_FUND
        ).let {
            sourceOfFundSp?.setSelection(it!!)
        }
        fivSourceOfIncome.remainSelection(it.size)
        if ((activity as HawActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()
        } else {
            setBool((activity as HawActivity).sharedPreferenceManager.lovBool)
        }
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let {
                        setBool(it)
                    };

                }

                override fun BoolFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun setBool(it: ArrayList<Bool>) {
        (activity as HawActivity).boolList = it
        fivAdverseMedia.setItemForBools(it)
        (activity as HawActivity).sharedPreferenceManager.lovBool = it
        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
            it,
            (activity as HawActivity).customerPep.ADVERSE_MEDIA_CUSTOMER
        )
            .let {
                isAdversMediaSp?.setSelection(it!!)
            }
        if ((activity as HawActivity).sharedPreferenceManager.lovSourceOfWealth.isNullOrEmpty()) {
            callSourceOfWealth()
        } else {
            setSourceOfWealth((activity as HawActivity).sharedPreferenceManager.lovSourceOfWealth)
        }
    }


    private fun setSourceOfWealth(it: java.util.ArrayList<SourceOfWealth>) {
        setTextLoad()
        (activity as HawActivity).sharedPreferenceManager.lovSourceOfWealth = it
        (activity as HawActivity).sourceOfWealthList = it
        fivSourceOfWealth.setItemForSourceOfWealth(it)
        (activity as HawActivity).globalClass?.findSpinnerPositionFromSourceOfWealthIndex(
            it,
            (activity as HawActivity).customerPep.DESC_SOURCE_OF_WEALTH
        ).let {
            sourceOfWealthSp?.setSelection(it!!)
        }
        fivSourceOfWealth.remainSelection(it.size)
        (activity as HawActivity).globalClass?.hideLoader()
    }

    private fun setTextLoad() {
        (activity as HawActivity).customerPep.othersourceoffund.let {
            (activity as HawActivity).globalClass?.setEnabled(otherSourceOfFundET!!, true)
            otherSourceOfFundET?.setText(it)
        }
        (activity as HawActivity).customerPep.othersourceofwealth.let {
            (activity as HawActivity).globalClass?.setEnabled(otherSourceOfFundET!!, true)
            otherSourceOfWealthET?.setText(it)
        }
        (activity as HawActivity).customerPep.ADDITIONAL_INFO.let {
            additionalInfoET?.setText(it)
        }
    }

    fun callSourceOffund() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.SOURCE_OFFUND_IDENTIFIER
        HBLHRStore.instance?.getSourceOffund(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : SourceOffundCallBack {
                override fun SourceOffundSuccess(response: SourceOffundResponse) {
                    response.data?.let {
                        setSourceOffund(it)
                    };
                }

                override fun SourceOffundFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callSourceOfWealth() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.SOURCE_OFWEALTH_IDENTIFIER
        HBLHRStore.instance?.getSourceOfWealth(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : SourceOfWealthCallBack {
                override fun SourceOfWealthSuccess(response: SourceOfWealthResponse) {
                    response.data?.let {
                        setSourceOfWealth(it)
                    };
                }

                override fun SourceOfWealthFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                if (validation()) {
                    (activity as HawActivity).globalClass?.setDisbaled(btNext,false)
                    ( this.context as HawActivity).globalClass?.showDialog( this.context)
                    java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                        saveAndNext()
                        setAllModelsInAOFRequestModel()
                        activity?.runOnUiThread {
                            (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                        }
                    })
                }
            }
            R.id.btBack -> if (findNavController().currentDestination?.id == R.id.CIFStep1_12) {
                findNavController().navigate(R.id.action_CIFStep1_12_to_CIFStep1_11)
            }
        }
    }


    fun onBackPress(view:View){
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
              if (keyCode == KeyEvent.KEYCODE_BACK&&keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep1_12) {
                    findNavController().navigate(R.id.action_CIFStep1_12_to_CIFStep1_11)
                }
                 true
            }
            false
        }
    }
    private fun validation(): Boolean {
        return when {
            sourceOfFundSp?.selectedItemPosition == 0 -> {
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString((R.string.please_select_other_source_of_funds))
                ,1)
                false
            }
            otherSourceOfFundET?.isEnabled == true && otherSourceOfFundET?.text.toString()
                .isNullOrEmpty() -> {
                otherSourceOfFundET?.error = resources!!.getString(R.string.pleaseEnterOtherSOIErr)
                otherSourceOfFundET?.requestFocus()
                false
            }
            sourceOfWealthSp?.selectedItemPosition == 0 -> {
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString((R.string.please_select_other_source_of_wealthsErr)),1
                )
                false
            }
            otherSourceOfWealthET?.isEnabled == true && otherSourceOfWealthET?.text.toString()
                .isNullOrEmpty() -> {
                otherSourceOfWealthET?.error =
                    resources!!.getString(R.string.pleaseEnterOtherSOWErr)
                otherSourceOfWealthET?.requestFocus()
                false
            }
            else -> {
                true
            }
        }
    }

    fun saveAndNext() {
        /*set the values from widgets and another models*/
        (activity as HawActivity).globalClass?.findSpinnerPositionFromSourceOfFundCode(
            (activity as HawActivity).sourceOfFundList,
            sourceOfFundSp?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerPep.SOURCE_OF_FUND = it.toString()
                (activity as HawActivity).customerPep.DESC_SOURCE_OF_FUND =
                    sourceOfFundSp?.selectedItem.toString()
            }
        }

        otherSourceOfFundET?.text.toString().let {
            (activity as HawActivity).customerPep.OTHER_SOURCE_OF_FUND = it
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromSourceOfWealthCode(
            (activity as HawActivity).sourceOfWealthList,
            sourceOfWealthSp?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerPep.SOURCE_OF_WEALTH = it.toString()
                (activity as HawActivity).customerPep.DESC_SOURCE_OF_WEALTH =
                    sourceOfWealthSp?.selectedItem.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as HawActivity).boolList,
            isAdversMediaSp?.selectedItem.toString()
        ).let {
            (activity as HawActivity).customerPep.ADVERSE_MEDIA_CUSTOMER = it.toString()
        }

        otherSourceOfWealthET?.text.toString().let {
            (activity as HawActivity).customerPep.OTHER_SOURCE_OF_WEALTH = it.toString()
        }
        additionalInfoET?.text.toString()
            .let { (activity as HawActivity).customerPep.ADDITIONAL_INFO = it.toString() }
        /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in HawActivity*/
        (activity as HawActivity).sharedPreferenceManager.customerPep =
            ((activity as HawActivity).customerPep)
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as HawActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as HawActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as HawActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as HawActivity).aofAccountInfoRequest=it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    (activity as HawActivity).globalClass?.hideLoader()
                    if (findNavController().currentDestination?.id == R.id.CIFStep1_12) {
                        findNavController().navigate(R.id.action_CIFStep1_12_to_CIFStep1_13)
                    }
                    (activity as HawActivity).recyclerViewSetup()
                    (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }
    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            }catch (ex:Exception){
            }
        }
    }
    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity).unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
