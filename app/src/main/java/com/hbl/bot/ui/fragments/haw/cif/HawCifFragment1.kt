package com.hbl.bot.ui.fragments.cif


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.DocLovRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.ETBNTBRequest
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.ui.adapter.DocumentUploadAdapter
import com.hbl.bot.ui.customviews.DialogCallback
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_cifstep1.*
import kotlinx.android.synthetic.main.fragment_cifstep1.btBack
import kotlinx.android.synthetic.main.fragment_cifstep1.btNext
import kotlinx.android.synthetic.main.fragment_cifstep1.formSectionHeader
import kotlinx.android.synthetic.main.header.view.*
import kotlinx.android.synthetic.main.view_form_input.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import kotlinx.android.synthetic.main.view_form_selection.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException
import java.util.concurrent.Executors


class HawCifFragment1 : Fragment(), View.OnClickListener {
    var csFirstCheck = false
    var dualNationalityCountriesLayout: ConstraintLayout? = null
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var docTypeSpinner: Spinner? = null
    var customerSegmentSpinner: Spinner? = null
    var documentNumber: EditText? = null
    var nationalitySpinner: Spinner? = null
    var secondNationalitySpinner: Spinner? = null
    var dualNationalitySpinner: Spinner? = null
    var statusCode = "02"
    var ETBNTB = ""
    var pOMStatus: HawPOMStatus? = null
    private fun showSkipDialog() {
        val builder = AlertDialog.Builder((activity as HawActivity))
        builder.setTitle("Information")
        builder.setCancelable(false)
        builder.setMessage((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.COM_PRINT_DOC_DESC)
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            try {
                if (findNavController().currentDestination?.id == R.id.CIFStep1) {
                    findNavController().navigate(R.id.action_CIFStep1_to_CIFStep14)
                }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail(
                    (activity as HawActivity),
                    (e as ArrayIndexOutOfBoundsException)
                )
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
            } catch (e: java.lang.Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as HawActivity), (e as java.lang.Exception))
            } finally {

            }
        }
        builder.show()
    }

    fun checkAllPagesModelsFilled(): Boolean {
        pOMStatus = HawPOMStatus(activity as HawActivity)
        //:TODO NOK PAGE CHECK
        return pOMStatus?.NextOfKin_Page_1_2_3() == 0
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            /**
             * if workflow code 1 or 3 so CIF Screen display
             * else workflow code 2 so Account screen display
             * */
            if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.COM_PRINT_DOC == "1") {
                showSkipDialog()
            } else if (findNavController().currentDestination?.id == R.id.CIFStep1 && checkAllPagesModelsFilled()) {
                findNavController().navigate(R.id.action_CIFStep1_to_CIFStep16)
            } else if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep1, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.value = 2
        viewModel.currentFragmentIndex.value = 0
        var txt = resources!!.getString(R.string.customer_id_info) + " (1/2 Page)"
        formSectionHeader.header_tv_title.setText(
            GlobalClass.textColor(
                txt,
                24,
                txt.length,
                Color.RED
            )
        )
    }


    fun checkAccount1stPageFields(): Boolean {
        pOMStatus = HawPOMStatus(activity as HawActivity)
        //:TODO ACCOUNT 1st PAGES FIELDS CHECK
        return pOMStatus?.Account_Page_1() == 0
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        try {
            LocalBroadcastManager.getInstance(activity as HawActivity)
                .registerReceiver(
                    mStatusCodeResponse, IntentFilter(
                        Constants.STATUS_BROADCAST
                    )
                );
            btNext.setOnClickListener(this)
            btCheck.setOnClickListener(this)
            btBack.setOnClickListener(this)
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            header()
            init()
            load()
            spinnerConditions()
            onBackPressed(view)
            customerSegmentCheck()
            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }


    private fun customerSegmentCheck() {
        customerSegmentSpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                try {
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromCustomerSegmentCode(
                        (activity as HawActivity).sharedPreferenceManager.lovCustSegment,
                        customerSegmentSpinner?.selectedItem.toString()
                    ).let {
                        if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {
                            //:TODO if customer segment already exist and account 1st page fields have filled then show alert for the user
                            if (csFirstCheck && checkAccount1stPageFields()
                                && (!(activity as HawActivity).customerDemoGraphicx.CUST_TYPE.isNullOrEmpty() ||
                                        !(activity as HawActivity).customerDemoGraphicx.CUSTTYPEDESC.isNullOrEmpty())
                            ) {
                                customerSegmentForcelyChangedAlert(
                                    "WARNING",
                                    "Are you sure want to change this customer segment?\n\nNOTE: If you will change the existing customer segment then you will not go over the any other pages from side menu",
                                    "YES",
                                    "NO"
                                )
                            } else {
                                csFirstCheck = true
                            }
                            // when select there is no item selected "Choose an item"  <--- hint
                            (activity as HawActivity).customerDemoGraphicx.CUST_TYPE = it.toString()
                            (activity as HawActivity).customerDemoGraphicx.CUSTTYPEDESC =
                                customerSegmentSpinner?.selectedItem.toString()
                        }
                    }

//                    viewCheckListTV.isVisible = position != 0
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        viewCheckListTV.isClickable = true
        viewCheckListTV.movementMethod = LinkMovementMethod.getInstance()
        viewCheckListTV.text = Html.fromHtml("CHECK LIST")
        viewCheckListTV?.setOnClickListener {
            callDocumentType()
        }
    }

    private fun customerSegmentForcelyChangedAlert(
        title: String,
        msg: String,
        button1: String,
        button2: String,

        ) {
        (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(
            context,
            title,
            msg,
            button1,
            button2,
            object : DialogCallback {
                override fun onNegativeClicked() {
                    super.onNegativeClicked()
                    (activity as HawActivity).aofAccountInfoRequest.isForcelyChangedCustomerSegment =
                        false
                    (activity as HawActivity).aofAccountInfoRequest.let {
                        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
                    }
                }

                override fun onPositiveClicked() {
                    super.onPositiveClicked()
                    (activity as HawActivity).aofAccountInfoRequest.isForcelyChangedCustomerSegment =
                        true
                    (activity as HawActivity).aofAccountInfoRequest.let {
                        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
                    }
                }
            })
    }


    private fun resetValuesBaseOnSegmet(it: String?) {
        if (it != "A2" || it != "A0" || it != "A4") {//housewife, student, unemployed
            (activity as HawActivity).globalClass?.clearModel(
                SharedPreferenceManager.CUSTOMER_FIN_KEY
            )
        }
        if (it != "A5") {//landlord
            (activity as HawActivity).customerCdd.nATR_EXP_MTH_HIGH_TURN_OVER =
                ""
            (activity as HawActivity).customerCdd.dATE_INCP_BUSS_PROF =
                ""
        }
        if (it != "A3") {//self employees
            (activity as HawActivity).customerCdd.aREA_OF_OPERATION_SUPP =
                ""
            (activity as HawActivity).customerCdd.dESC_BUSS_ACTY_PRD_SERV =
                ""
            (activity as HawActivity).customerCdd.dESC_BUSS_GEO_AREA_OPR_SUPP =
                ""
            (activity as HawActivity).customerCdd.nAME_COUNTER_PARTY_1 =
                ""
            (activity as HawActivity).customerCdd.nAME_COUNTER_PARTY_2 =
                ""
            (activity as HawActivity).customerCdd.nAME_COUNTER_PARTY_3 =
                ""
        }
        if (it != "A6") {//hbl staff
            (activity as HawActivity).customerDemoGraphicx.P_NUMBER = ""
        }

        if (it == "A1") {
            (activity as HawActivity).customerDemoGraphicx.NATUREBUSINESSDESC = ""
            (activity as HawActivity).customerDemoGraphicx.NATURE_BUSINESS = ""
            (activity as HawActivity).customerDemoGraphicx.DESIGNATION = ""
            (activity as HawActivity).customerDemoGraphicx.PROFESSION = ""
            (activity as HawActivity).customerDemoGraphicx.PROFESSIONDESC = ""
            (activity as HawActivity).customerDemoGraphicx.NAME_OF_COMPANY = ""
        }

        (activity as HawActivity).customerDemoGraphicx.let {
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics = it
        }
        (activity as HawActivity).customerCdd.let {
            (activity as HawActivity).sharedPreferenceManager.customerCDD = it
        }

        //for CDD
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }


        //for DemoGraphics
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }

        //for FIN
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }

        //for main sp
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }

    }

    private fun callDocumentType() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = DocLovRequest()
        lovRequest.identifier = Constants.DOCUMENT_TYPE_INDENTIFIER
        lovRequest._SEGCD = (activity as HawActivity).customerDemoGraphicx.CUST_TYPE
        lovRequest._SEGTY = (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE
        lovRequest._WORKFLOW = (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE
        lovRequest._TYPE = ""
        HBLHRStore.instance?.getDocumentType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : DocumentTypeCallBack {
                override fun DocumentTypeSuccess(response: DocTypeResponse) {
                    try {
                        response.data?.get(0)?.data.let {
                            avatarDialog(activity as HawActivity, it!!)
                        }
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }


                override fun DocumentTypeFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) { callDocumentType() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    fun avatarDialog(context: Context, it: ArrayList<DocumentType>) {
        var dialogBuilder = AlertDialog.Builder(activity as HawActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.customer_popup, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        var cancel_action = layoutView.findViewById<Button>(R.id.cancel_action)
        var checListRV = layoutView.findViewById<RecyclerView>(R.id.checListRV)
        var documentUploadAdapter = DocumentUploadAdapter(
            it
        )
        checListRV?.apply {
            layoutManager = LinearLayoutManager(context as HawActivity)
            adapter = documentUploadAdapter
            documentUploadAdapter.notifyDataSetChanged()
        }
        cancel_action.setOnClickListener(View.OnClickListener {
            alertDialog.dismiss()
        })
        alertDialog.show()
    }

    fun load() {
        try {
            (activity as HawActivity).customerInfo.ID_DOCUMENT_NO.let { t ->
                documentNumber?.setText(t)
            }
//            if ((activity as HawActivity).sharedPreferenceManager.lovDoctype.isNullOrEmpty()) {
            callHawKeys()

//            } else {
//                docTypeData((activity as HawActivity).sharedPreferenceManager.lovDoctype)
//            }
            if (!(activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(0)?.ETBNTBFLAG.isNullOrEmpty() || !(activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
                statusCode = "00"
                (activity as HawActivity).globalClass?.setDisbaled(documentNumber!!)
                (activity as HawActivity).globalClass?.setDisbaled(docTypeSpinner!!)
                (activity as HawActivity).globalClass?.setDisbaled(btCheck!!)
                ETBNTB =
                    (activity as HawActivity).sharedPreferenceManager.etbntb.getOrNull(0)?.ETBNTBFLAG.toString()
            } else {
                statusCode = "02"
                (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
                (activity as HawActivity).globalClass?.setEnabled(docTypeSpinner!!, true)
                (activity as HawActivity).globalClass?.setEnabled(btCheck!!, true)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    fun init() {
//        (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
        if (!(activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)?.itemDescription.isNullOrEmpty()) {
            val trackingID = (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)?.itemDescription
            //TrackingID
            (activity as HawActivity).nav_view!!.getHeaderView(0).trackingID.text = Html.fromHtml("Tracking ID: $trackingID")
            (activity as HawActivity).nav_view!!.getHeaderView(0).trackingID.paintFlags = (activity as HawActivity).nav_view!!.getHeaderView(0).trackingID.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        } else if (!(activity as HawActivity).sharedPreferenceManager!!.aofAccountInfo!!.TRACKING_ID!!.isNullOrEmpty()) {
            //TrackingID
            (activity as HawActivity).nav_view!!.getHeaderView(0).trackingID.text = Html.fromHtml("Tracking ID: ${(activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID}")
            (activity as HawActivity).nav_view!!.getHeaderView(0).trackingID.paintFlags = (activity as HawActivity).nav_view!!.getHeaderView(0).trackingID.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        }
        docTypeSpinner = fsvDocumentType_spinner.getSpinner(R.id.doctypeSpinnerId)
        customerSegmentSpinner = customer_segment_spinner.getSpinner(R.id.customer_segment_spinner)
        documentNumber = documentNumber_et.getTextFromEditText(R.id.documentNumber_et)
        nationalitySpinner = fivNationality_spinner.getSpinner(R.id.nationality_id)
        dualNationalitySpinner = fivDualNationality_spinner.getSpinner(R.id.dual_nationality_id)
        secondNationalitySpinner =
            fivSecondNationality_spinner.getSpinner(R.id.secondNationalitySpinner)
        dualNationalityCountriesLayout =
            fivSecondNationality_spinner.getLayout(R.id.secondNationalitySpinnerLayout)
        /*Old changes for haw*/
//        (activity as HawActivity).globalClass?.setDisbaled(dualNationalitySpinner!!,false)
//        dualNationalitySpinner?.setSelection(0)
        /*New chances for haw Dual Nationality to be autopopuated as No but should be editable */
        dualNationalitySpinner?.setSelection(0)
        documentNumber?.requestFocus()
    }

    fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
            (activity as HawActivity).globalClass?.setDisbaled(docTypeSpinner!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(customerSegmentSpinner!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(documentNumber!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(nationalitySpinner!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(dualNationalitySpinner!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(secondNationalitySpinner!!, false)
        }
    }

    private fun spinnerConditions() {
        docTypeSpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                i: Int,
                l: Long,
            ) {
                try {
                    (activity as HawActivity).globalClass?.showDialog(activity)
                    documentNumberCustomConditions(i)
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
        /*  customerSegmentSpinner?.onItemSelectedListener = object :
              AdapterView.OnItemSelectedListener {
              @SuppressLint("Range")
              override fun onItemSelected(
                  adapterView: AdapterView<*>?,
                  view: View?,
                  pos: Int,
                  l: Long,
              ) {
                  try {
                      if (csFirstCheck) {
                          if (!(activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE.isNullOrEmpty()) {
                              (GlobalClass.applicationContext as GlobalClass).showConfirmationDialog(
                                  activity,
                                  "Alert\n",
                                  "Are you sure want to change existing CUSTOMER SEGMENT?\n\nNOTE: If you will change the existing CUSTOMER SEGMENT then all drafted data and conditions will be cleared",
                                  "YES",
                                  "NO",
                                  object : DialogCallback {
                                      override fun onNegativeClicked() {
                                          super.onNegativeClicked()
                                          (activity as HawActivity).globalClass?.findSpinnerIndexFromCustomerSegmentCode(
                                              (activity as HawActivity).sharedPreferenceManager.lovCustSegment,
                                              (activity as HawActivity).customerDemoGraphicx.CUSTTYPEDESC
                                          ).let {
                                              customerSegmentSpinner?.setSelection(it!!)
                                          }
                                          csFirstCheck = false
                                      }

                                      override fun onPositiveClicked() {
                                          super.onPositiveClicked()
                                          (activity as HawActivity).globalClass?.clearSubmitAOF()
                                          (activity as HawActivity).init()
                                          (activity as HawActivity).recyclerViewSetup()
                                      }
                                  })
                          }
                      } else {
                          csFirstCheck = true
                      }

                  } catch (e: UnsatisfiedLinkError) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: NullPointerException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: IllegalArgumentException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: NumberFormatException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: InterruptedException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: RuntimeException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: IOException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: FileNotFoundException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: ClassNotFoundException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: ActivityNotFoundException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: IndexOutOfBoundsException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: SecurityException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: IllegalStateException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: OutOfMemoryError) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )

                  } catch (e: RuntimeException) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } catch (e: Exception) {
                      SendEmail.SendEmail(
                          activity as HawActivity, javaClass.simpleName,
                          "StackTrace: " + e.stackTrace,
                          "Message: " + e.message,
                          "LocalizedMessage: " + e.localizedMessage,
                          "Suppressed: " + e.suppressed,
                          "ClassSimpleName: " + e.javaClass.simpleName,
                          "Cuase: " + e.cause, Throwable().stackTrace[0].methodName
                      )
                  } finally {

                  }
              }

              override fun onNothingSelected(adapterView: AdapterView<*>?) {
              }
          }*/
        dualNationalitySpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (pos == 0) {
                        var item = ArrayList<Country>()
                        fivSecondNationality_spinner?.setItemForfivSecondNationality(item)
                        (activity as HawActivity).globalClass?.setDisbaled(
                            secondNationalitySpinner!!
                        )
                        fivSecondNationality_spinner?.mandateSpinner?.text = ""
                    } else {
                        /*Old changes for haw*/
                        /*fivSecondNationality_spinner?.mandateSpinner?.text = "*"
                        *//*If haw disabled true case when it will be run*//*
                        if (!(activity as HawActivity).globalClass?.isAllFieldsDisbaled!!) {
                            (activity as HawActivity).globalClass?.setEnabled(
                                secondNationalitySpinner!!,
                                true
                            )
                        }
                        if ((activity as HawActivity).sharedPreferenceManager.lovCountries.isNullOrEmpty()) {
                            callAllCountries()
                        } else {
                            setAllCountries((activity as HawActivity).sharedPreferenceManager.lovCountries)
                        }*/
                        /*New changes for haw Dual Nationality to be autopopuated as No but should be editable */
                        (activity as HawActivity).visitBranchPopup("Please visit nearest branch to open account for dual nationality.")
                        dualNationalitySpinner?.setSelection(0)
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
        secondNationalitySpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    /*  In case of dual nationality Pakistan cannot be selected in second nationality.*/
                    if (secondNationalitySpinner?.isEnabled == true) {
                        if (nationalitySpinner?.selectedItem.toString() != "Pakistan" && secondNationalitySpinner?.selectedItem.toString() == "Pakistan") {
//                        secondNationalitySpinner?.
                            secondNationalitySpinner?.setSelection(0)
                            ToastUtils.normalShowToast(
                                activity,
                                resources!!.getString(R.string.second_nationality_error), 1
                            )
                        } else if (secondNationalitySpinner?.selectedItemPosition != 0 && nationalitySpinner?.selectedItem.toString() == secondNationalitySpinner?.selectedItem.toString()) {
                            secondNationalitySpinner?.setSelection(0)
                            ToastUtils.normalShowToast(
                                activity,
                                resources!!.getString(R.string.second_2_nationality_error), 1
                            )
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }

        nationalitySpinner?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    /*  In case of dual nationality Pakistan cannot be selected in second nationality.*/
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCode(
                        (activity as HawActivity).sharedPreferenceManager.lovCountries,
                        nationalitySpinner?.selectedItem.toString()
                    ).let {
                        if (!it!!.equals("PK",true)) {
                            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeIndex((activity as HawActivity).sharedPreferenceManager.lovCountries,
                                "PK").let {
                                nationalitySpinner?.setSelection(it!!)
                            }
                            (activity as HawActivity).visitBranchPopup("Please visit the nearest branch to open account for other than country of PAKISTAN")
                        }
                    }
                  /*  if (docTypeSpinner?.selectedItem.toString() == "PASSPORT" && nationalitySpinner?.selectedItem.toString() == "Pakistan") {
                        nationalitySpinner?.setSelection(0)
                        ToastUtils.normalShowToast(
                            activity,
                            resources!!.getString(R.string.nationality_password_err), 1
                        )
                    } else if (docTypeSpinner?.selectedItem.toString() == "ARC" && nationalitySpinner?.selectedItem.toString() == "Pakistan") {
                        nationalitySpinner?.setSelection(0)
                        ToastUtils.normalShowToast(
                            activity,
                            resources!!.getString(R.string.arc_err), 1
                        )
                    }*/
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
    }

    fun documentNumberCustomConditions(position: Int) {
        /*call CustomerSegment when doctype select condition name and value*/
        (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeCode(
            (activity as HawActivity).docTypeList,
            docTypeSpinner?.selectedItem.toString()
        ).let {
            callCustomerSegment(docTypeSpinner?.selectedItem.toString(), it.toString())
        }
//        var doctypeIndex =
//            (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeIndexValue(
//                (activity as HawActivity).sharedPreferenceManager.lovDoctype,
//                (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE_DESC
//            )
        /*Condition when click doctype list auto generate document number pattern according to doctype*/
//        if (doctypeIndex != position) {
//            documentNumber?.setText("")
//        }
//        if (position == 0 && !(activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            (activity as HawActivity).globalClass?.setDisbaled(documentNumber!!)
//        } else if (position == 1 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            /*CNIC 13 Numeric*/
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "CNIC")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber!!,
//                13,
//                INPUT_TYPE_NUMBER
//            )
//        } else /*if (position == 2 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            *//*PASSPORT 20 AlphaNumeric*//*
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "06")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber,
//                19,
//                INPUT_TYPE_ALPHANUMERIC
//            )
//        } else */ if (position == 2 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            /*SNIC 13 Numeric*/
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "SNIC")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber!!,
//                13,
//                INPUT_TYPE_NUMBER
//            )
//        } else if (position == 4 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
////            CRC 20 AlphaNumeric
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "CRC")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber!!,
//                13,
//                INPUT_TYPE_NUMBER
//            )
//        } else if (position == 3 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            /*NICOP 13 Numeric*/
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "NICOP")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber!!,
//                13,
//                INPUT_TYPE_NUMBER
//            )
//        } else if (position == 6 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            /*CRC 13 Numeric*/
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "CRC")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber!!,
//                13,
//                INPUT_TYPE_NUMBER
//            )
//        } else if (position == 7 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            /*POC 13 AlphaNumeric*/
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "POC")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber!!,
//                13,
//                INPUT_TYPE_ALPHANUMERIC
//            )
//        } else if (position == 8 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            /*BirthCertificate 18 Numeric*/
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "08")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber!!,
//                18,
//                INPUT_TYPE_NUMBER
//            )
//        } else if (position == 9 && (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG.isNullOrEmpty()) {
//            /*POR Afghan Refugee Card 13 AlphaNumeric*/
//            (activity as HawActivity).globalClass?.setEnabled(documentNumber!!, true)
//            documentSetPrefix(position, "POR")
//            (activity as HawActivity).globalClass?.edittextTypeCount(
//                documentNumber!!,
//                13,
//                INPUT_TYPE_ALPHANUMERIC
//            )
//        }
    }

    private fun callHawKeys() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.HAW_KEYS_IDENTIFIER
        HBLHRStore.instance?.getHawKeys(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : HawKeysCallBack {
                override fun Success(response: HawKeysResponse) {
                    try {
                        response.data?.let {
                            (activity as HawActivity).sharedPreferenceManager.hawKeys = it
                        }
                        callDocs()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun Failure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callDocs() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.HAW_DOCS_IDENTIFIER
        HBLHRStore.instance?.getDocs(RetrofitEnums.URL_HBL, lovRequest, object : DocsCallBack {
            override fun DocsSuccess(response: DocsResponse) {
                try {
                    response.data?.let {
                        docTypeData(it)
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun DocsFailure(response: BaseResponse) {
                //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                ToastUtils.normalShowToast(activity, response.message, 1)
                (activity as HawActivity).globalClass?.hideLoader()
            }
        })
    }


    fun docTypeData(it: ArrayList<DocsData>) {
        fsvDocumentType_spinner.setItemForDocs(it)
        (activity as HawActivity).docTypeList = it
        (activity as HawActivity).sharedPreferenceManager.lovDoctype = it
        (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeIndexValue(
            it,
            (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE_DESC
        ).let {
            docTypeSpinner?.setSelection(it!!)
        }
        fsvDocumentType_spinner.remainSelection(it.size)
        if ((activity as HawActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()
        } else {
            setBool((activity as HawActivity).sharedPreferenceManager.lovBool)
        }
    }

    fun callCustomerSegment(conditionName: String, conditionValue: String) {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.HAW_SEGMENT_IDENTIFIER
        lovRequest.condition = true
        lovRequest.conditionname = Constants.ID_DOC_TYPE
        lovRequest.conditionvalue = conditionValue
        HBLHRStore.instance?.getCustomerSegment(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CustomerSegmentCallBack {
                override fun CustomerSegmentSuccess(response: CustomerSegmentResponse) {
                    response.data?.let {
                        try {
                            setCustSeg(it, conditionName, conditionValue)
                        } catch (e: UnsatisfiedLinkError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as UnsatisfiedLinkError)
                            )
                        } catch (e: NullPointerException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as NullPointerException)
                            )
                        } catch (e: IllegalArgumentException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as IllegalArgumentException)
                            )
                        } catch (e: NumberFormatException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as NumberFormatException)
                            )
                        } catch (e: InterruptedException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as InterruptedException)
                            )
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                        } catch (e: IOException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                        } catch (e: FileNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as FileNotFoundException)
                            )
                        } catch (e: ClassNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as ClassNotFoundException)
                            )
                        } catch (e: ActivityNotFoundException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as ActivityNotFoundException)
                            )
                        } catch (e: IndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as IndexOutOfBoundsException)
                            )
                        } catch (e: ArrayIndexOutOfBoundsException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as ArrayIndexOutOfBoundsException)
                            )
                        } catch (e: ClassCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as ClassCastException)
                            )
                        } catch (e: TypeCastException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                        } catch (e: SecurityException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                        } catch (e: IllegalStateException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail(
                                (activity as HawActivity),
                                (e as IllegalStateException)
                            )
                        } catch (e: OutOfMemoryError) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                        } catch (e: RuntimeException) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                        } catch (e: Exception) {
                            ToastUtils.normalShowToast(
                                activity,
                                getString(R.string.something_went_wrong),
                                3
                            )
                            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                        } finally {

                        }
                    }
                }

                override fun CustomerSegmentFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) {
//                        callCustomerSegment(
//                            conditionName,
//                            conditionValue
//                        )
//                    }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun setCustSeg(it: ArrayList<CustomerSegment>, conditionName: String, conditionValue: String) {
        (activity as HawActivity).customerSegmentList = it
        customer_segment_spinner.setItemForCustomerSegment(it)
        (activity as HawActivity).sharedPreferenceManager.lovCustSegment = it

        (activity as HawActivity).globalClass?.findSpinnerIndexFromCustomerSegmentCode(
            it,
            (activity as HawActivity).customerDemoGraphicx.CUSTTYPEDESC
        ).let {
            customerSegmentSpinner?.setSelection(it!!)
        }
        customer_segment_spinner.remainSelection(it.size)
//        callSpecificCountries(conditionName, conditionValue)
        if ((activity as HawActivity).sharedPreferenceManager.lovCountries.isNullOrEmpty()) {
            callAllCountries()
        } else {
            setSpecificCountry((activity as HawActivity).sharedPreferenceManager.lovCountries)
            setAllCountries((activity as HawActivity).sharedPreferenceManager.lovCountries)
        }
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    try {
                        response.data?.let {
                            setBool(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun BoolFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun setBool(it: ArrayList<Bool>) {
        (activity as HawActivity).boolList = it
        fivDualNationality_spinner?.setItemForBools(it)
        (activity as HawActivity).sharedPreferenceManager.lovBool = it
        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolIndex(
            it,
            (activity as HawActivity).customerInfo.dualnationality
        ).let {
//            dualNationalitySpinner?.setSelection(it!!)
            nationalitySpinner?.setSelection(1)
        }
//        (activity as HawActivity).globalClass?.hideLoader()
    }

    fun callSpecificCountries(conditionName: String, conditionValue: String) {
        var lovRequest = LovRequest()
        lovRequest.condition = true
        lovRequest.conditionname = Constants.ID_DOC_TYPE
        lovRequest.conditionvalue = conditionValue
        lovRequest.conditionmatrix = Constants.COUNTRY_MATRIX
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(lovRequest)
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    try {
                        response.data?.let {
                            setSpecificCountry(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun CountryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) {
//                        callSpecificCountries(
//                            conditionName,
//                            conditionValue
//                        )
//                    }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    fun setSpecificCountry(it: ArrayList<Country>) {
        (activity as HawActivity).specificCountryList = it
        fivNationality_spinner?.setItemForfivNationality(it)
        (activity as HawActivity).sharedPreferenceManager.lovSpecificCountry = it
        if(!(activity as HawActivity).customerInfo.nationalitydesc.isNullOrEmpty()) {
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                (activity as HawActivity).customerInfo.nationalitydesc
            ).let {
                    nationalitySpinner?.setSelection(it!!)
            }
        }else{
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                "Pakistan"
            ).let {
//            (activity as HawActivity).globalClass?.setDisbaled(countryOfBirthSP!!,false)
                nationalitySpinner?.setSelection(it!!)
            }
        }
        fivNationality_spinner.remainSelection(it.size)
    }

    fun callAllCountries() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(lovRequest)
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    try {
                        response.data?.let {
                            setSpecificCountry(it)
                            setAllCountries(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun CountryFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    fun setAllCountries(it: ArrayList<Country>) {
        fivSecondNationality_spinner?.setItemForfivSecondNationality(it)
        (activity as HawActivity).sharedPreferenceManager.lovCountries = it
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            (activity as HawActivity).customerInfo.secondnationalitydesc
        ).let {
            if (it != 0) {
                secondNationalitySpinner?.setSelection(it!!)
                dualNationalitySpinner?.setSelection(1)
            }
        }
        fivSecondNationality_spinner.remainSelection(it.size)
        (activity as HawActivity).globalClass?.hideLoader()
    }

    fun subETBNTB() {
        var etbntbRequest = ETBNTBRequest()
        etbntbRequest.CHANNELLID = "asd"
        etbntbRequest.TELLERID = "asd"
        etbntbRequest.CHANNELDATE = "asd"
        etbntbRequest.CHANNELTIME = "asd"
        etbntbRequest.CHANNELSRLNO = "asd"
        etbntbRequest.SERVICENAME = "C"
        etbntbRequest.REMARK = "asd"
        documentNumber?.text.toString().trim().let {
            etbntbRequest.CNIC = it
        }
        etbntbRequest.payload =
            "U2FsdGVkX1+eMelgQeJlI51PUXnmebgWMVjt8eGRb7kTZ14o9CGJ17PkRN/R7Ol/LVVjoXNA3uAtF05FVtvweDF/ZQKbqShdXACJgx0H6tI="

        HBLHRStore.instance?.submitETBNTB(
            RetrofitEnums.URL_HBL,
            etbntbRequest,
            object : ETBNTBCallBack {
                override fun ETBNTBSuccess(response: ETBNTBResponse) {
                    try {
                        statusCode = response.status!!
                        ETBNTB = response?.data?.get(0)?.ETBNTBFLAG.toString()
                        if (ETBNTB != "C" && statusCode != "02") {
                            if (ETBNTB == "N") {
                                response.data?.get(0)?.ETBNTBFLAG.let {
                                    ETBNTB = it!!
                                    (activity as HawActivity).etbntbFLAG = it!!
                                    (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                                        it!!
                                }
                                response.data?.get(0)?.REMEDIATE.let {
                                    (activity as HawActivity).REMEDIATE = it!!
                                }
                                (activity as HawActivity).globalClass?.setDisbaled(documentNumber!!)
                                (activity as HawActivity).globalClass?.setDisbaled(docTypeSpinner!!)
                                (activity as HawActivity).globalClass?.setDisbaled(btCheck!!)
                                ToastUtils.normalShowToast(activity, response.message, 2)
                            } else {
                                ToastUtils.normalShowToast(activity, response.message, 1)
                            }
                        } else {
                            ToastUtils.normalShowToast(activity, response.message, 1)
                        }
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun ETBNTBFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) { subETBNTB() }
                    statusCode = "02"
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btCheck -> {
                try {
                    if (etbntbValidation()) {
                        (activity as HawActivity).globalClass?.showDialog(activity)
                        subETBNTB()
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                        (activity as HawActivity).globalClass?.showDialog(activity)
                        Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            (activity as HawActivity).init()
                            (activity as HawActivity).runOnUiThread {
                                (activity as HawActivity).globalClass?.setEnabled(
                                    btNext!!,
                                    true
                                )
                                (activity as HawActivity).recyclerViewSetup()
                                if (findNavController().currentDestination?.id == R.id.CIFStep1) {
                                    findNavController().navigate(R.id.action_CIFStep1_to_CIFStep1_3)
                                }
                                (activity as HawActivity).globalClass?.hideLoader()
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }
            R.id.btBack -> {
                back()

            }
        }
    }

    fun back() {
        if (findNavController().currentDestination?.id == R.id.CIFStep1) {
            if ((activity as HawActivity).intent.extras!!.getInt(
                    Constants.ACTIVITY_KEY
                ) == Constants.CIF_1_SHOW_RECORD
            ) {
                activity?.finish()
            } else {
                findNavController().navigate(R.id.action_back_to_company_name)
            }
        }
    }

    fun saveAndNext() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in HawActivity*/
        /*set the values from widgets and another models*/
        val startPrefix = startPrefixOfDocument.text.toString().trim()
        documentNumber?.text.toString().trim().let {
            (activity as HawActivity).cnicNumber = startPrefix + it
            //TODO: insert document number
            ((activity as HawActivity).aofAccountInfoRequest).ID_DOC_NO = startPrefix + it
            (activity as HawActivity).customerInfo.ID_DOCUMENT_NO = startPrefix + it
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeCode(
            (activity as HawActivity).docTypeList,
            docTypeSpinner?.selectedItem.toString()
        ).let {
            //TODO: set iDDOCUMENTNO
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                ((activity as HawActivity).aofAccountInfoRequest).ID_DOCUMENT_TYPE =
                    it.toString()
                (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE = it.toString()
                (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE_DESC =
                    docTypeSpinner?.selectedItem.toString()
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromCustomerSegmentCode(
            (activity as HawActivity).customerSegmentList,
            customerSegmentSpinner?.selectedItem.toString()
        ).let {
            //TODO: set customerSegment
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerDemoGraphicx.CUST_TYPE = it.toString()
                (activity as HawActivity).customerDemoGraphicx.CUSTTYPEDESC =
                    customerSegmentSpinner?.selectedItem.toString()
//                resetValuesBaseOnSegmet(it.toString())
            }
        }

        (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCode(
            (activity as HawActivity).specificCountryList,
            nationalitySpinner?.selectedItem.toString()
        ).let {
            //TODO: set nATIONALITY
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerInfo.NATIONALITY = it.toString()
                (activity as HawActivity).customerInfo.NATIONALITYDESC =
                    nationalitySpinner?.selectedItem.toString()
            }
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as HawActivity).boolList,
            dualNationalitySpinner?.selectedItem.toString()
        ).let {
            //TODO: set dUALNATIONALITY
            (activity as HawActivity).customerInfo.DUAL_NATIONALITY = it
        }

        /*If secondnationailty is enabeld then set the value of second nationality*/
        if (secondNationalitySpinner?.isEnabled == true && dualNationalitySpinner?.selectedItemPosition == 1) {
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCode(
                (activity as HawActivity).sharedPreferenceManager.lovCountries,
                secondNationalitySpinner?.selectedItem.toString()
            ).let {
                //TODO: set sECONDNATIONALITY
                if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as HawActivity).customerInfo.SECOND_NATIONALITY = it.toString()
                    (activity as HawActivity).customerInfo.SECONDNATIONALITYDESC =
                        secondNationalitySpinner?.selectedItem.toString()
                }
            }
            Log.i(
                "SecondNationiltyValue",
                (activity as HawActivity).customerInfo.SECOND_NATIONALITY.toString()
            )
        } else {
            (activity as HawActivity).customerInfo.SECOND_NATIONALITY = ""
            (activity as HawActivity).customerInfo.SECONDNATIONALITYDESC = ""

        }
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_CODE.let {
            //TODO: set BRANCH_CODE
            (activity as HawActivity).customerAccounts.BRANCH_CODE = it!!
        }
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            //TODO: set BRANCHNAME
            (activity as HawActivity).customerAccounts.BRANCHNAME = it!!
        }
        ETBNTB.let {
            if (!it.isNullOrEmpty()) {
                (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG = it
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo =
                    (activity as HawActivity).aofAccountInfoRequest
            }
        }
        /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in HawActivity*/
        (activity as HawActivity).sharedPreferenceManager.customerAccount =
            ((activity as HawActivity).customerAccounts)
        (activity as HawActivity).sharedPreferenceManager.customerInfo =
            ((activity as HawActivity).customerInfo)
        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo =
            ((activity as HawActivity).aofAccountInfoRequest)
        (activity as HawActivity).sharedPreferenceManager.setCustomerDemoGraphics((activity as HawActivity).customerDemoGraphicx)
        /*Cnic verification set and value reinitialise*/
    }

    fun validation(): Boolean {
        return documentNumberValidation()
    }

    fun etbntbValidation(): Boolean {
        val position = docTypeSpinner?.selectedItemPosition
        val length = documentNumber?.text.toString().length
        if (position == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.errDOCTTypeETBN),
                1
            );
            return false
        } else if (length == 0) {
            documentNumber?.setError(resources!!.getString(R.string.errCNICETBN));
            documentNumber?.requestFocus();
            return false
        } else if (position == 1 && length!! < 13) {
            /*CNIC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorCNIC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 2 && length!! < 1) {
            /*PASSPORT 20 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorPassport));
            documentNumber?.requestFocus();
            return false
        } else if (position == 3 && length!! < 13) {
            /*SNIC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorSNIC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 4 && length!! < 13) {
            /*CRC 20 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorARC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 5 && length!! < 13) {
            /*NICOP 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorNICOP));
            documentNumber?.requestFocus();
            return false
        } else if (position == 6 && length!! < 13) {
            /*CRC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorCRC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 7 && length!! < 13) {
            /*POC 13 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorPOC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 8 && length!! < 18) {
            /*BirthCertificate 18 Number*/
            documentNumber?.setError(resources!!.getString(R.string.errorBirthCertificate));
            documentNumber?.requestFocus();
            return false
        } else if (position == 9 && length!! < 13) {
            /*POR Afghan Refugee Card 13 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorAfghanRefugeeCard));
            documentNumber?.requestFocus();
            return false
        } else {
            return true
        }
    }

    fun documentNumberValidation(): Boolean {
        val position = docTypeSpinner?.selectedItemPosition
        val length = documentNumber?.text.toString().length
        if (position == 1 && length!! < 13) {
            /*CNIC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorCNIC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 2 && length!! < 1) {
            /*PASSPORT 20 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorPassport));
            documentNumber?.requestFocus();
            return false
        } else if (position == 3 && length!! < 13) {
            /*SNIC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorSNIC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 4 && length!! < 13) {
            /*CRC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorARC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 5 && length!! < 13) {
            /*NICOP 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorNICOP));
            documentNumber?.requestFocus();
            return false
        } else if (position == 6 && length!! < 13) {
            /*CRC 13 Numeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorCRC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 7 && length!! < 13) {
            /*POC 13 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorPOC));
            documentNumber?.requestFocus();
            return false
        } else if (position == 8 && length!! < 18) {
            /*BirthCertificate 18 Number*/
            documentNumber?.setError(resources!!.getString(R.string.errorBirthCertificate));
            documentNumber?.requestFocus();
            return false
        } else if (position == 9 && length!! < 13) {
            /*POR Afghan Refugee Card 13 AlphaNumeric*/
            documentNumber?.setError(resources!!.getString(R.string.errorAfghanRefugeeCard));
            documentNumber?.requestFocus();
            return false
        } else if (docTypeSpinner?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(activity, resources!!.getString(R.string.doctypeErr), 1)
            return false
        } else if (customerSegmentSpinner?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.customerSegmentErr),
                1
            )
            return false
        } else if (nationalitySpinner?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity, resources!!.getString(R.string.nationalityErr), 1
            )

            return false
        } else if (dualNationalitySpinner?.selectedItemPosition == 1) {
            if (secondNationalitySpinner?.selectedItemPosition == 0) {
                ToastUtils.normalShowToast(
                    activity, resources!!.getString(R.string.please_select_second_nationlity), 1
                )
                return false
            } else {
                return true
            }
        } else {
            return true
        }
    }

    fun documentSetPrefix(position: Int, label: String) {
        documentNumber?.requestFocus()
        if (position == 2 && label.equals("06") && !(activity as HawActivity).customerInfo.iddocumenttype.equals(
                "06"
            )
        ) {
            documentNumber?.setPadding(22, 0, 0, 0)
            startPrefixOfDocument.visibility = View.VISIBLE
            startPrefixOfDocument?.setText("P")
        } else if (position == 8 && label.equals("08") && !(activity as HawActivity).customerInfo.iddocumenttype.equals(
                "08"
            )
        ) {
            documentNumber?.setPadding(22, 0, 0, 0)
            startPrefixOfDocument.visibility = View.VISIBLE
            startPrefixOfDocument?.setText("B")
        } else {
            documentNumber?.setPadding(0, 0, 0, 0)
            startPrefixOfDocument.visibility = View.GONE
            startPrefixOfDocument?.setText("")
        }
    }

    private fun onBackPressed(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                val navController =
                    (activity as HawActivity).findNavController(R.id.cifHostFragment)
                if (navController.currentDestination?.id == R.id.CIFStep1) {
                    Log.i("onBackPress", "Not Up Finish All Fragment")
                    (activity as HawActivity).finish()
                } else {
                    Log.i("onBackPress", "Up")
                    navController.popBackStack()
                }
                true
            } else {
                false
            }
        }
    }

}
