package com.hbl.bot.ui.fragments.spark.cif


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.ModeOfTans
import com.hbl.bot.network.models.response.baseRM.SourceOfWealth
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep4_4.*
import kotlinx.android.synthetic.main.fragment_cifstep4_4.btBack
import kotlinx.android.synthetic.main.fragment_cifstep4_4.btNext
import kotlinx.android.synthetic.main.fragment_cifstep4_4.formSectionHeader
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.util.ArrayList
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class CIFStep4_4 : Fragment(), View.OnClickListener {
    private var list = ArrayList<String>()
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivBeneficiarOwnerSP: Spinner? = null
    var fivSourceOfwealthSP: Spinner? = null
    var fivOtherSourceWealthET: EditText? = null
    var fivNECTSP: Spinner? = null
    var fivONECTET: EditText? = null
    var fivIBOET: EditText? = null
    var fivEmployeePNumberIDET: EditText? = null
    var constraintLayout: ConstraintLayout? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep4_4, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    fun header() {
        viewModel.totalSteps.setValue(4)
        viewModel.currentFragmentIndex.setValue(2)
        val txt = resources.getString(R.string.customer_demographic)
        val txt1 = " (3/4 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as CIFRootActivity)
                .registerReceiver(mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST)
                );
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            init()
            load()
            setLengthAndType()
//            if (GlobalClass.isDummyDataTrue) {
//                loadDummyData()
//            }
            setCondition()
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun load() {
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovSourceOfWealth.isNullOrEmpty()) {
            callSourceOfWealth()
        } else {
            setSourceOfWealth((activity as CIFRootActivity).sharedPreferenceManager.lovSourceOfWealth)
        }
    }
    private fun addHardCordBenificaryOwner() {
        val questions =
            (activity as CIFRootActivity).resources!!.getStringArray(R.array.benificiaryOwner)
        list.clear()
        for (element in questions) {
            list.add(element)
        }
        val adapter =
            ArrayAdapter(activity as CIFRootActivity, R.layout.view_spinner_item, R.id.text1, list)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        (activity as CIFRootActivity).customerDemoGraphicx.BENEFICIAL_OWNER_M_CODE.let {
            if (it == "O") {
                fivBeneficiarOwnerSP?.setSelection(0)
            } else {
                fivBeneficiarOwnerSP?.setSelection(1)
            }
        }
        fivBeneficiarOwnerSP?.adapter = adapter
        fivBeneficiarOwnerSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                var fivBeneficiarOwnerValue = fivBeneficiarOwnerSP?.selectedItem.toString()
                if (fivBeneficiarOwnerValue.equals("Others", true)) {
                    fivBeneficiarOwnerSP?.setSelection(0)
                    ToastUtils.normalShowToast(activity, getString(R.string.others_err), 1)
                    (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!,false)
                } else {
                    (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                }
            }
        })
    }
    private fun setSourceOfWealth(it: ArrayList<SourceOfWealth>) {
        try {
            (activity as CIFRootActivity).sharedPreferenceManager.lovSourceOfWealth = it
            fivSourceOfwealth.setItemForSourceOfWealth(it)
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromSourceOfWealthIndex(
                it,
                (activity as CIFRootActivity).customerDemoGraphicx.SOURCEOFWEALTHDESC
            ).let {
                fivSourceOfwealthSP?.setSelection(it!!)
            }
            fivSourceOfwealth.remainSelection(it.size)
            if ((activity as CIFRootActivity).sharedPreferenceManager.lovModeOfTans.isNullOrEmpty()) {
                callModeOfTrans()
            } else {
                setModeOfTrans((activity as CIFRootActivity).sharedPreferenceManager.lovModeOfTans)
            }

        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun setLengthAndType() {
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivEmployeePNumberIDET!!,
            6,
            Constants.INPUT_TYPE_NUMBER
        )
    }

    fun setCondition() {
        var custSegType =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        fivNECTSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {

            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                try {
                    var natureOfBusiness = fivNECTSP?.selectedItem.toString()
                    if (natureOfBusiness.equals("OTHER", true)) {
                        fivONECT.mandateInput.text = "*"
                        (activity as CIFRootActivity).globalClass?.setEnabled(fivONECTET!!, true)
                    } else {
                        if ((activity as CIFRootActivity).customerDemoGraphicx.OTHEXPMODECRTRANSDESC.isNullOrEmpty()) {
                            fivONECTET?.setText("")
                        }
                        fivONECT.mandateInput.text = ""
                        (activity as CIFRootActivity).globalClass?.setDisbaled(fivONECTET!!,false)
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
        })
        if (custSegType == "A6"/*HBL STAFF*/) {
            fivEmployeePNumberID?.visibility = View.VISIBLE
            constraintLayout?.visibility = View.VISIBLE
        } else {
            fivEmployeePNumberID?.visibility = View.GONE
            constraintLayout?.visibility = View.GONE
        }
    }

    fun init() {
        fivBeneficiarOwnerSP = fivBeneficiarOwner.getSpinner(R.id.fivBeneficiarOwner)
        fivSourceOfwealthSP = fivSourceOfwealth.getSpinner(R.id.sourceOfWealthID)
        fivOtherSourceWealthET =
            fivOtherSourceWealth.getTextFromEditText(R.id.otherSourceOfWealthID)
        fivEmployeePNumberIDET =
            fivEmployeePNumberID.getTextFromEditText(R.id.fivEmployeePNumberID1)
        constraintLayout = fivEmployeePNumberID.edittextLayout
        fivNECTSP = fivNECT.getSpinner(R.id.NECTID)
        fivONECTET = fivONECT.getTextFromEditText(R.id.ONECTET)
        fivIBOET = fivIBO.getTextFromEditText(R.id.IBOET)
        (activity as CIFRootActivity).globalClass?.setDisbaled(fivONECTET!!,false)
    }

    fun loadDummyData() {
        fivOtherSourceWealthET?.setText("Other Source Of Wealth")
//        fivONECTET?.setText("EXPECTED MODE OF")
        fivIBOET?.setText("FINANCIAL INFO OF BENEFICIARY")
    }

    fun callSourceOfWealth() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.SOURCE_OFWEALTH_IDENTIFIER
        HBLHRStore.instance?.getSourceOfWealth(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : SourceOfWealthCallBack {
                override fun SourceOfWealthSuccess(response: SourceOfWealthResponse) {
                    response.data?.let { setSourceOfWealth(it) };
                }

                override fun SourceOfWealthFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callModeOfTrans() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.MODE_OFTRANS_INDENTIFIER
        HBLHRStore.instance?.getModeOfTrans(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : ModeOfTransCallBack {
                override fun ModeOfTransSuccess(response: ModeOfTransResponse) {
                    response.data?.let {
                        setModeOfTrans(it)
                    }
                }

                override fun ModeOfTransFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setModeOfTrans(it: ArrayList<ModeOfTans>) {
        try {
            (activity as CIFRootActivity).modeOfTansList = it
            fivNECT.setItemForModeOfTrans(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovModeOfTans = it
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromModeOfTansIndex(
                (activity as CIFRootActivity).modeOfTansList,
                (activity as CIFRootActivity).customerDemoGraphicx.EXPMODECRTRANSDESC
            ).let {
                fivNECTSP?.setSelection(it!!)
            }
            (activity as CIFRootActivity).customerDemoGraphicx.OTHEXPMODECRTRANSDESC
                .let {
                    fivONECTET?.setText(it.toString())
                }
            (activity as CIFRootActivity).customerDemoGraphicx.OTHSRCOFWEALTHDESC
                .let {
                    fivOtherSourceWealthET?.setText(it.toString())
                }
            (activity as CIFRootActivity).customerDemoGraphicx.FIN_INFO_BENF_OWNER
                .let { fivIBOET?.setText(it) }
            (activity as CIFRootActivity).customerDemoGraphicx.P_NUMBER
                .let { fivEmployeePNumberIDET?.setText(it) }
            addHardCordBenificaryOwner()
            (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun disclaimerDialog() {
        var dialogBuilder = AlertDialog.Builder(activity as CIFRootActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.popup_disclaimer, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.setCancelable(false)
        alertDialog.show()
        var toolbar = layoutView.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        var desctext1 = layoutView.findViewById<TextView>(R.id.desctext1)
        var desctext2 = layoutView.findViewById<TextView>(R.id.desctext2)
        var yes = layoutView.findViewById<Button>(R.id.yes_action)
        var no = layoutView.findViewById<Button>(R.id.no_action)
        toolbar.title = "DISCLAIMER"
        desctext1.text = resources.getString(R.string.benificiaryOwnerDisclaimerStr)
        yes.text = "OK"
        desctext2.visibility = View.GONE
        no.visibility = View.GONE
        yes.setOnClickListener(View.OnClickListener {
            isFinancialSupportPage()
            (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            alertDialog.dismiss()
        })

        alertDialog.show()
    }
//    fun callBool() {
//        (activity as CIFRootActivity).globalClass?.showDialog(activity)
//        var lovRequest = LovRequest()
//        lovRequest.identifier = Constants.BOOL_IDENTIFIER
//        HBLHRStore?.instance?.getBools(
//            RetrofitEnums.URL_HBL,
//            lovRequest, object : BoolCallBack {
//                override fun BoolSuccess(response: BoolResponse) {
//                    response.data?.let { setBool(it) };
//                }
//
//                override fun BoolFailure(response: BaseResponse) {
//                    ToastUtils.normalShowToast(activity, response.message,"")
//                    (activity as CIFRootActivity).globalClass?.hideLoader()
//                }
//            })
//    }
//
//    private fun setBool(it: ArrayList<Bool>) {
//        (activity as CIFRootActivity).boolList=it
//        fivPowerOfattorney.setItemForConsumerProductBool(it)
//        (activity as CIFRootActivity).customerDemoGraphicx.CUSTTYPEDESC.let { item ->
//            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolIndex(
//                (activity as CIFRootActivity).boolList,
//                item
//            ).let {
//                fivPowerOfattorneySP?.setSelection(it!!)
//            }
//        }
//        (activity as CIFRootActivity).globalClass?.hideLoader()
//    }


    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            activity?.runOnUiThread {
                                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep4_4)
                    findNavController().navigate(R.id.action_CIFStep4_4_to_CIFStep4_2)
        }

    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep4_4)
                    findNavController().navigate(R.id.action_CIFStep4_4_to_CIFStep4_2)
                true
            }
            false
        }
    }

    fun saveAndNext() {
        if (fivBeneficiarOwnerSP?.selectedItemPosition == 0) {
            (activity as CIFRootActivity).customerDemoGraphicx.BENEFICIAL_OWNER_M_CODE = "S"
            (activity as CIFRootActivity).customerDemoGraphicx.BENEFICIAL_OWNER_M_DESC =
                fivBeneficiarOwnerSP?.selectedItem.toString()
        } else {
            (activity as CIFRootActivity).customerDemoGraphicx.BENEFICIAL_OWNER_M_CODE = "O"
            (activity as CIFRootActivity).customerDemoGraphicx.BENEFICIAL_OWNER_M_DESC =
                fivBeneficiarOwnerSP?.selectedItem.toString()
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromSourceOfWealthCode(
            (activity as CIFRootActivity).sourceOfWealthList,
            fivSourceOfwealthSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerDemoGraphicx.SOURCE_OF_WEALTH = it.toString()
                (activity as CIFRootActivity).customerDemoGraphicx.SOURCEOFWEALTHDESC =
                    fivSourceOfwealthSP?.selectedItem.toString()
            }
        }
        fivOtherSourceWealthET?.text.toString()
            .let {
                (activity as CIFRootActivity).customerDemoGraphicx.OTHSRCOFWEALTHDESC = it
            }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromModeOfTansCode(
            (activity as CIFRootActivity).modeOfTansList,
            fivNECTSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty()&&it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerDemoGraphicx.EXP_MODE_CR_TRANS = it.toString()
                (activity as CIFRootActivity).customerDemoGraphicx.EXPMODECRTRANSDESC =
                    fivNECTSP?.selectedItem.toString()
            }
        }
        fivOtherSourceWealthET?.text.toString()
            .let {  /* (activity as CIFRootActivity).customerDemoGraphicx.OTHER = it*/ }
        if (fivONECTET?.isEnabled == true) {
            fivONECTET?.text.toString()
                .let {
                    (activity as CIFRootActivity).customerDemoGraphicx.OTHEXPMODECRTRANSDESC = it
                }
        }
        fivIBOET?.text.toString()
            .let { (activity as CIFRootActivity).customerDemoGraphicx.FIN_INFO_BENF_OWNER = it }

        if (constraintLayout?.isVisible == true) {
            fivEmployeePNumberIDET?.text.toString()
                .let { (activity as CIFRootActivity).customerDemoGraphicx.P_NUMBER = it }
        }

//        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
//            (activity as CIFRootActivity).boolList, fivPowerOfattorneySP?.selectedItem.toString()
//        ).let {
//            (activity as CIFRootActivity).customerDemoGraphicx.CONSUMER_PRODUCT = it
//        }
        (activity as CIFRootActivity).sharedPreferenceManager.setCustomerDemoGraphics((activity as CIFRootActivity).customerDemoGraphicx)
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as CIFRootActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as CIFRootActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as CIFRootActivity).aofAccountInfoRequest = it
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                        (activity as CIFRootActivity).runOnUiThread {
                            disclaimerDialog()
                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun isFinancialSupportPage() {
        var custSegType =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        if (custSegType == "A0" || /*STUDENT*/ custSegType == "A2" || /*HOUSE WIFE*/ custSegType == "A4" /*UNEMPLOYED*/ || custSegType == "A7" /*MINOR/GAURDIAN*/
        ) {
            if (findNavController().currentDestination?.id == R.id.CIFStep4_4)
                findNavController().navigate(R.id.action_CIFStep4_4_to_financialSupporter)
        } else if (custSegType == "A3" /*SELF-EMPLOYED*/ || custSegType == "B0" /*BB AGENT*/ || custSegType == "CD" /*SOLE PROPRIETORSHIP*/) {
            if (findNavController().currentDestination?.id == R.id.CIFStep4_4)
                findNavController().navigate(R.id.action_CIFStep4_4_to_self_employeed)
        } else if (custSegType == "A5" /*LANDLORD*/) {
            if (findNavController().currentDestination?.id == R.id.CIFStep4_4)
                findNavController().navigate(R.id.action_CIFStep4_4_to_landlord)
        } else {
            if (findNavController().currentDestination?.id == R.id.CIFStep4_4)
                findNavController().navigate(R.id.action_CIFStep4_4_to_all_skip_normal_flow)
        }
        (activity as CIFRootActivity).recyclerViewSetup()
    }

    fun validation(): Boolean {
        var fivEmployeePNumber = fivEmployeePNumberIDET?.text.toString().trim()
        var otherNormalExpectedCreditTransations = fivONECTET?.text.toString().trim()
        var IBO = fivIBOET?.text.toString().trim()
        if (otherNormalExpectedCreditTransations.length == 0 && fivONECTET?.isEnabled == true) {
            fivONECTET?.setError(resources!!.getString(R.string.pleaseEnterotherNormalExpectedCreditTransationsErr))
            fivONECTET?.requestFocus()
            return false
        } else if (fivNECTSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.pleaseSelectNEMTErr),
                1
            )
            return false
        } /*else if (IBO.isEmpty()) {
            fivIBOET?.setError(resources!!.getString(R.string.pleaseEnterFinancialInfoBinErr))
            fivIBOET?.requestFocus()
            return false
        }*/ else if (fivEmployeePNumber.length == 0 && constraintLayout?.isVisible == true) {
            fivEmployeePNumberIDET?.setError(resources!!.getString(R.string.fivEmployeePNumberIDETErr))
            fivEmployeePNumberIDET?.requestFocus()
            return false
        } else if (fivEmployeePNumber.length < 6 && constraintLayout?.isVisible == true) {
            fivEmployeePNumberIDET?.setError(resources!!.getString(R.string.fivEmployeePNumberID2ETErr))
            fivEmployeePNumberIDET?.requestFocus()
            return false
        } else {
            return true
        }
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
