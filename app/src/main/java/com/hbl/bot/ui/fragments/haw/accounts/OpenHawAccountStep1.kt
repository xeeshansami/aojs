package com.hbl.bot.ui.fragments.accounts

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.BusinessAreaRequest
import com.hbl.bot.network.models.request.base.CIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.base.TrackingIDRequest
import com.hbl.bot.network.models.request.baseRM.ETBNTBRequest
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.utils.*
import kotlinx.android.synthetic.main.fragment_open_account_1.*
import kotlinx.android.synthetic.main.fragment_open_account_1.btNext

class OpenHawAccountStep1 : Fragment(), View.OnClickListener {
    var documentNumber: EditText? = null
    var docID = null
    var fsvDocumentTypeSP: Spinner? = null
    var statusCode = "02"
    var msg = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_open_account_1, container, false)

    }

    private fun callDocs() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.DOCS_IDENTIFIER
        HBLHRStore.instance?.getDocs(RetrofitEnums.URL_HBL, lovRequest, object : DocsCallBack {
            override fun DocsSuccess(response: DocsResponse) {
                response.data?.let {
                    fsvDocumentType.setItemForDocs(it)
                    (activity as HawActivity).docTypeList = it
                }
                fsvDocumentTypeSP?.setSelection(1)
                (activity as HawActivity).globalClass?.hideLoader()

            }

            override fun DocsFailure(response: BaseResponse) {
                ToastUtils.normalShowToast(activity, response.message, 1)
                (activity as HawActivity).globalClass?.hideLoader()
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btNext.setOnClickListener(this)

        documentNumber = fivDocNo.getTextFromEditText(R.id.fivDocNo)
        if (!(activity as HawActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_NO.isNullOrEmpty()) {
            documentNumber?.setText((activity as HawActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_NO.toString())

        }
//        documentNumber?.setText("6549873216546")
        fsvDocumentTypeSP = fsvDocumentType.getSpinner()
        setLengthAndTpe()


        callDocs()

//        fsvDocumentType.setItem(resources!!.getStringArray(R.array.documetType));


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btNext -> {
                if (validation()) {
                    if ((activity as HawActivity).backInt == 2) {
                        (activity as HawActivity).globalClass?.showDialog(activity)
                        documentNumber?.text.toString().let {
                            (activity as HawActivity).customerInfo.ID_DOCUMENT_NO = it
                            (activity as HawActivity).sharedPreferenceManager.customerInfo =
                                (activity as HawActivity).customerInfo
                        }
                        subETBNTB()
                    } else {

                        documentNumber?.text.toString().let {
                            (activity as HawActivity).customerInfo.ID_DOCUMENT_NO = it
                            (activity as HawActivity).sharedPreferenceManager.customerInfo =
                                (activity as HawActivity).customerInfo
                        }
                        findNavController().navigate(R.id.action_open_account_to_biometric_page)
                    }
                }


            }
        }
    }

    private fun validation(): Boolean {
        (activity as HawActivity).sharedPreferenceManager.storeStringInSharedPreferences(
            Constants.ACCOUNT_CNIC,
            documentNumber?.text.toString()
        )
        if (documentNumber?.text.isNullOrEmpty()) {
            ToastUtils.normalShowToast(context, "Kindly Fill CNIC",1)
            return false
        } else {
            return true
        }
    }

    private fun setLengthAndTpe() {

        (activity as HawActivity).globalClass?.edittextTypeCount(
            documentNumber!!,
            13,
            Constants.INPUT_TYPE_NUMBER
        )
    }


    fun subETBNTB() {
        var etbntbRequest = ETBNTBRequest()
        etbntbRequest.CHANNELLID = "asd"
        etbntbRequest.TELLERID = "asd"
        etbntbRequest.CHANNELDATE = "asd"
        etbntbRequest.CHANNELTIME = "asd"
        etbntbRequest.CHANNELSRLNO = "asd"
        etbntbRequest.SERVICENAME = "A"
        etbntbRequest.REMARK = "asd"
        documentNumber?.text.toString().trim().let {
            etbntbRequest.CNIC = it
        }
        etbntbRequest.payload = "U2FsdGVkX1+eMelgQeJlI51PUXnmebgWMVjt8eGRb7kTZ14o9CGJ17PkRN/R7Ol/LVVjoXNA3uAtF05FVtvweDF/ZQKbqShdXACJgx0H6tI="
        var json= Gson().toJson(etbntbRequest)
        HBLHRStore.instance?.submitETBNTB(
            RetrofitEnums.URL_HBL,
            etbntbRequest,
            object : ETBNTBCallBack {
                override fun ETBNTBSuccess(response: ETBNTBResponse) {
                    statusCode = response.status!!
                    msg = response.message!!
                    if (response.status == "00") {
//                        callCIF(response.data[0].)
//                        callTrackingID(0)
                    } else {
                        ToastUtils.normalShowToast(activity, response.message,1)
                        (activity as HawActivity).globalClass?.hideLoader()

                    }
                }

                override fun ETBNTBFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }



    private fun callTrackingID(activityNumber: Int) {

        var trackingIDRequest = TrackingIDRequest()
        trackingIDRequest?.brcode =
            (activity as HawActivity).sharedPreferenceManager.getLoginData()
                .getBRANCHCODE()
        HBLHRStore.instance?.getTrackingID(
            RetrofitEnums.URL_HBL,
            trackingIDRequest,
            object : TrackingIDCallBack {
                override fun TrackingIDSuccess(response: TrackingIDResponse) {
                    (activity as HawActivity).sharedPreferenceManager.setTrackingID(
                        response.data?.get(0)
                    )
                    response.data?.get(0)?.getBatch()?.get(0)?.itemDescription.toString().let {
                        (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it
                        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo =
                            (activity as HawActivity).aofAccountInfoRequest
                    }



                    callMYSIS(activityNumber)


                }

                override fun TrackingIDFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message,1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })


    }

    fun callWorkFlow(activityNumber: Int) {
        var lov = LovRequest()
        lov.identifier = Constants.WORKFLOW_IDENTIFIER
        HBLHRStore.instance?.getWorkFlow(RetrofitEnums.URL_HBL, lov, object : WorkFlowCallBack {
            override fun WorkFlowSuccess(response: WorkFlowResponse) {
                (activity as HawActivity).sharedPreferenceManager.setWorkFlow(
                    response
                )

//                GlobalClass.sharedPreferenceManager!!.workFlowCode = response.data[1].WORK_FLOW_CODE
//                workFlowDesc = response.data[1].PUR_DESC


            }

            override fun WorkFlowFailure(response: BaseResponse) {
                ToastUtils.normalShowToast(activity, response.message,1)
                (activity as HawActivity).globalClass?.hideLoader()
            }
        })
    }

    fun callMYSIS(activityNumber: Int) {
        var lov = LovRequest()
        lov.identifier = Constants.MYSISREF_IDENTIFIER
        HBLHRStore.instance?.getMySisRef(
            RetrofitEnums.URL_HBL,
            lov,
            object : MySisRefCallBack {
                override fun MySisRefSuccess(response: MYSISREFResponse) {
                    (activity as HawActivity).sharedPreferenceManager.setMySisRef(
                        response
                    )
                    (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF =
                        response.data?.get(0)?.MYSIS_REF.toString()
                    callBusinessArea(activityNumber)
                }

                override fun MySisRefFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message,1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callBusinessArea(activityNumber: Int) {
        var lov = BusinessAreaRequest()
        lov.identifier = Constants.BUSINESSAREA_IDENTIFIER
        lov.find.BR_CODE =
            GlobalClass.sharedPreferenceManager!!.getLoginData().getBRANCHCODE().toString()
        HBLHRStore.instance?.getBusinessArea(
            RetrofitEnums.URL_HBL,
            lov,
            object : BusinessAreaCallBack {
                override fun BusinessAreaSuccess(response: BusinessAreaResponse) {
//                    var bundle = Bundle()
                    (activity as HawActivity).sharedPreferenceManager.setBusinessArea(
                        response.data.get(0)
                    )
                    (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.WORK_FLOW_CODE_DESC =
                        response.data?.get(0)?.BUSINESS_AREA.toString()
//                    callAppStatus(activityNumber)
                    (activity as HawActivity).globalClass?.hideLoader()


                    if (statusCode == "00") {
                        callCIF("")

                    } else {
                        ToastUtils.normalShowToast(context, msg,1)
                    }
                }

                override fun BusinessAreaFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message,1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }


    fun callCIF(cnic:String) {
        var cifRequest = CIFRequest()
        cifRequest.payload = Constants.PAYLOAD
        cifRequest.scnic =cnic
//            (activity as HawActivity).sharedPreferenceManager.getStringFromSharedPreferences(
//                Constants.ACCOUNT_CNIC
//            )
        HBLHRStore.instance?.getCIF(RetrofitEnums.URL_HBL, cifRequest, object : CIFCallBack {
            override fun CIFSuccess(response: AofAccountInfoResponse) {
                if (response.status == "00") {

                    findNavController().navigate(R.id.action_open_account_to_biometric_page)
                } else {
                    ToastUtils.normalShowToast(context, response.message,2)
                }
            }

            override fun CIFFailure(response: BaseResponse) {
                (activity as NavigationDrawerActivity).globalClass?.hideLoader()

            }
        })

    }

    fun callAppStatus(activityNumber: Int) {
        var lov = LovRequest()
        lov.identifier = Constants.APPSTATUS_IDENTIFIER
        HBLHRStore.instance?.getAppStatus(
            RetrofitEnums.URL_HBL,
            lov,
            object : AppStatusCallBack {
                override fun AppStatusSuccess(response: AppStatusResponse) {
                    (activity as HawActivity).sharedPreferenceManager.setAppStatus(
                        response.data?.get(0)
                    )
                }

                override fun AppStatusFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message,1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }


}
