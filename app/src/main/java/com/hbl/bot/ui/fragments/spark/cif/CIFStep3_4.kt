package com.hbl.bot.ui.fragments.spark.cif


import com.hbl.bot.ui.activities.spark.CIFRootActivity
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.BoolCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.CountryCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.GenerateCIFCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.PurposeOfTinCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.Country
import com.hbl.bot.network.models.response.baseRM.PurposeOfTin
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep3_4.*
import kotlinx.android.synthetic.main.fragment_cifstep3_4.btBack
import kotlinx.android.synthetic.main.fragment_cifstep3_4.btNext
import kotlinx.android.synthetic.main.fragment_cifstep3_4.fivReasonTin1
import kotlinx.android.synthetic.main.fragment_cifstep3_4.fivTin1
import kotlinx.android.synthetic.main.fragment_cifstep3_4.formSectionHeader
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import kotlinx.android.synthetic.main.view_form_selection.view.*
import kotlinx.android.synthetic.main.view_form_textview.view.*
import java.io.FileNotFoundException
import java.text.SimpleDateFormat
import java.util.*
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class CIFStep3_4 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var expireDate: TextView? = null
    val myCalendar = Calendar.getInstance()
    var myView: View? = null
    var fivOCNET: EditText? = null
    var fivCOOCSP: Spinner? = null
    var fivNOCountrySP1: Spinner? = null
    var fivTin1ET: EditText? = null
    var fivReasonTin1SP: Spinner? = null
    var commentBox1ET: EditText? = null

    var fivNOCountrySP2: Spinner? = null
    var fivTin2ET: EditText? = null
    var fivReasonTin2SP: Spinner? = null
    var commentBox2ET: EditText? = null

    var fivNOCountrySP3: Spinner? = null
    var fivTin3ET: EditText? = null
    var fivReasonTin3SP: Spinner? = null
    var commentBox3ET: EditText? = null

    var fivTaxResidenceySP: Spinner? = null
    var defaultIsForceCRS = false

    var rmvspModels: RMVSPModels? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep3_4, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    fun header() {
        val view: TextView = formSectionHeader.getTextView()
        viewModel.totalSteps.setValue(2)
        viewModel.currentFragmentIndex.setValue(0)
        val txt = resources.getString(R.string.other_country_tax_payer)
        val txt1 = " (1/2 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as CIFRootActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            init()
            setLengthAndType()
            crsCondition()
            taxResidenceConditions()
            load()
            openDateDialog()
            USCardConditions()
            tin2And3Conditions()
            reasonOfTin()
            tinNumber()
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun crsCondition() {
        var cor = (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES
        if (cor != "PK" && cor != "US") {
            fivTaxResidenceySP?.setSelection(1)
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivTaxResidenceySP!!)
        }
    }

    private fun reasonOfTin() {
        fivReasonTin1SP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                reasonOfTin1()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        fivReasonTin2SP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                reasonOfTin2()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        fivReasonTin3SP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                reasonOfTin3()
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
    }

    fun tinNumbersEditedCheck() {
        if ((activity as CIFRootActivity).customerInfo.TIN1.isNullOrEmpty()) {
            (activity as CIFRootActivity).globalClass?.setEnabled(fivReasonTin1SP!!, true)
            fivReasonTin1.mandateSpinner.text = "*"
        } else {
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivReasonTin1SP!!)
            fivReasonTin1.mandateSpinner.text = ""
        }
        if ((activity as CIFRootActivity).customerInfo.TIN2.isNullOrEmpty()) {
            (activity as CIFRootActivity).globalClass?.setEnabled(fivReasonTin2SP!!, true)
            fivReasonTin2.mandateSpinner.text = "*"
        } else {
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivReasonTin2SP!!)
            fivReasonTin2.mandateSpinner.text = ""
        }
        if ((activity as CIFRootActivity).customerInfo.TIN3.isNullOrEmpty()) {
            (activity as CIFRootActivity).globalClass?.setEnabled(fivReasonTin3SP!!, true)
            fivReasonTin3.mandateSpinner.text = "*"
        } else {
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivReasonTin3SP!!)
            fivReasonTin3.mandateSpinner.text = ""
        }
    }

    fun reasonOfTin1() {
        try {
            var REASON_IN_CASE_OF_NO_TIN1 =
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin,
                    fivReasonTin1SP?.selectedItem.toString()
                )
            if (fivReasonTin1SP?.selectedItemPosition == 0) {
                (activity as CIFRootActivity).globalClass?.setEnabled(fivTin1ET!!, true)
                fivTin1.mandateInput.text = "*"
            } else {
                fivTin1ET?.setText("")
                (activity as CIFRootActivity).globalClass?.setDisbaled(fivTin1ET!!)
                fivTin1.mandateInput.text = ""
            }
            if (REASON_IN_CASE_OF_NO_TIN1 == "B") {
                commentBox1.mandateInput.text = "*"
                commentBox1.isVisible = true
            } else {
                commentBox1.mandateInput.text = ""
                commentBox1.isVisible = false
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun reasonOfTin2() {
        try {
            var REASON_IN_CASE_OF_NO_TIN2 =
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin,
                    fivReasonTin2SP?.selectedItem.toString()
                )
            if (fivReasonTin2SP?.selectedItemPosition == 0) {
                (activity as CIFRootActivity).globalClass?.setEnabled(fivTin2ET!!, true)
                fivTin2.mandateInput.text = "*"
            } else {
                fivTin2ET?.setText("")
                (activity as CIFRootActivity).globalClass?.setDisbaled(fivTin2ET!!)
                fivTin2.mandateInput.text = ""
            }
            if (REASON_IN_CASE_OF_NO_TIN2 == "B") {
                commentBox2.mandateInput.text = "*"
                commentBox2.isVisible = true
            } else {
                commentBox2.mandateInput.text = ""
                commentBox2.isVisible = false
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun reasonOfTin3() {
        try {
            var REASON_IN_CASE_OF_NO_TIN3 =
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin,
                    fivReasonTin3SP?.selectedItem.toString()
                )
            if (fivReasonTin3SP?.selectedItemPosition == 0) {
                (activity as CIFRootActivity).globalClass?.setEnabled(fivTin3ET!!, true)
                fivTin3.mandateInput.text = "*"
            } else {
                fivTin3ET?.setText("")
                (activity as CIFRootActivity).globalClass?.setDisbaled(fivTin3ET!!)
                fivTin3.mandateInput.text = ""
            }
            if (REASON_IN_CASE_OF_NO_TIN3 == "B") {
                commentBox3.mandateInput.text = "*"
                commentBox3.isVisible = true
            } else {
                commentBox3.mandateInput.text = ""
                commentBox3.isVisible = false

            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun tinNumber() {
        addTextWatcher(fivTin1ET!!)
        addTextWatcher(fivTin2ET!!)
        addTextWatcher(fivTin3ET!!)
    }

    private fun addTextWatcher(editText: EditText) {
        editText.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (fivTin1ET?.text?.toString().isNullOrEmpty()) {
                        (activity as CIFRootActivity).globalClass?.setEnabled(fivReasonTin1SP!!,
                            true)
                        fivReasonTin1.mandateSpinner.text = "*"
                        commentBox1.mandateInput.text = "*"
                    } else {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(fivReasonTin1SP!!)
                        fivReasonTin1.mandateSpinner.text = ""
                        commentBox1.mandateInput.text = ""
                    }
                    if (fivTin2ET?.text?.toString().isNullOrEmpty()) {
                        (activity as CIFRootActivity).globalClass?.setEnabled(fivReasonTin2SP!!,
                            true)
                        fivReasonTin2.mandateSpinner.text = "*"
                        commentBox2.mandateInput.text = "*"
                    } else {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(fivReasonTin2SP!!)
                        fivReasonTin2.mandateSpinner.text = ""
                        commentBox2.mandateInput.text = ""
                    }
                    if (fivTin3ET?.text?.toString().isNullOrEmpty()) {
                        (activity as CIFRootActivity).globalClass?.setEnabled(fivReasonTin3SP!!,
                            true)
                        fivReasonTin3.mandateSpinner.text = "*"
                        commentBox3.mandateInput.text = "*"
                    } else {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(fivReasonTin3SP!!)
                        fivReasonTin3.mandateSpinner.text = ""
                        commentBox3.mandateInput.text = ""
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })

    }

    fun USCardConditions() {
        fivOCNET?.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                try {
                    if (fivOCNET?.text?.length != 0) {
                        fivCOOC.mandateSpinner.text = "*"
                        fivEDOC.mandateTextView.text = "*"
                    } else {
                        fivCOOC.mandateSpinner.text = ""
                        fivEDOC.mandateTextView.text = ""
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun afterTextChanged(s: Editable?) {
            }
        })
    }

    private fun setLengthAndType() {
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivOCNET!!,
            35,
            Constants.INPUT_TYPE_ALPHANUMERIC
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            commentBox1ET!!,
            35,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            commentBox2ET!!,
            35,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivTin1ET!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivTin2ET!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivTin3ET!!,
            20,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
    }

    private fun tin2And3Conditions() {
        tin2IDCheckBox.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (compoundButton.isChecked) {
                tin2Layout.visibility = View.VISIBLE
            } else {
                tin3IDCheckBox.isChecked = false
                tin2Layout.visibility = View.GONE
                tin3Layout.visibility = View.GONE
            }
        })
        tin3IDCheckBox.setOnCheckedChangeListener(CompoundButton.OnCheckedChangeListener { compoundButton, b ->
            if (compoundButton.isChecked) {
                tin3Layout.visibility = View.VISIBLE
            } else {
                tin3Layout.visibility = View.GONE
            }
        })
    }

    fun init() {
        rmvspModels = RMVSPModels(activity as CIFRootActivity)
        expireDate = fivEDOC.getTextFromTextView(R.id.expiry_date)
        fivOCNET = fivOCN.getTextFromEditText(R.id.COOCID)
        fivCOOCSP = fivCOOC.getSpinner(R.id.COOCID)
        fivNOCountrySP1 = fivNOCountry1.getSpinner(R.id.NOCountryID)
        fivTin1ET = fivTin1.getTextFromEditText(R.id.tin1ID)
        fivReasonTin1SP = fivReasonTin1.getSpinner(R.id.reasonTIn1ID)
        commentBox1ET = commentBox1.getTextFromEditText(R.id.commentBox1)
        fivTaxResidenceySP = fivTaxResidencey.getSpinner(R.id.TaxResidenceyID)
        fivNOCountrySP2 = fivNOCountry2.getSpinner(R.id.NOCountry2ID)
        fivTin2ET = fivTin2.getTextFromEditText(R.id.tin2ID)
        fivReasonTin2SP = fivReasonTin2.getSpinner(R.id.reasonTin2ID)
        commentBox2ET = commentBox2.getTextFromEditText(R.id.commentBox2)
        fivNOCountrySP3 = fivNOCountry3.getSpinner(R.id.NoCountry3ID)
        fivTin3ET = fivTin3.getTextFromEditText(R.id.tin3ID)
        fivReasonTin3SP = fivReasonTin3.getSpinner(R.id.reasonTin3ID)
        commentBox3ET = commentBox3.getTextFromEditText(R.id.commentBox3)
    }

    fun openDateDialog() {
        expireDate!!.setOnClickListener {
            try {
                var datePicker =
                    activity?.let {
                        DatePickerDialog(
                            it,
                            R.style.DialogTheme,
                            date,
                            myCalendar[Calendar.YEAR],
                            myCalendar[Calendar.MONTH],
                            myCalendar[Calendar.DAY_OF_MONTH]
                        )
                    }
                datePicker?.datePicker?.minDate = System.currentTimeMillis() - 1000
                datePicker?.show()
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity),
                    (e as ArrayIndexOutOfBoundsException))
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
            }
        }
        fivEDOC!!.setOnClickListener {
            try {
                var datePicker =
                    activity?.let {
                        DatePickerDialog(
                            it,
                            R.style.DialogTheme,
                            date,
                            myCalendar[Calendar.YEAR],
                            myCalendar[Calendar.MONTH],
                            myCalendar[Calendar.DAY_OF_MONTH]
                        )
                    }
                datePicker?.datePicker?.maxDate = System.currentTimeMillis()-1000
                datePicker?.show()
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity),
                    (e as ArrayIndexOutOfBoundsException))
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
                SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
            }
        }
    }

    var date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        myCalendar.add(Calendar.DATE, 0)
        // Set the Calendar new date as minimum date of date picker
        updateLabel()
    }

    private fun updateLabel() {
        try {
//        val myFormat = "dd-MMM-yyyy" //In which you need put here
            val myFormat = "dd-MM-yyyy"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            expireDate?.text = sdf.format(myCalendar.time)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun callPurposeOfTin() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.PURPOSE_OFTIN_IDENTIFIER
        HBLHRStore.instance?.getPurposeOfTin(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : PurposeOfTinCallBack {
                override fun PurposeOfTinSuccess(response: PurposeOfTinResponse) {
                    try {
                        response.data?.let {
                            setPurposeOfTin(it)
                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun PurposeOfTinFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setPurposeOfTin(it: ArrayList<PurposeOfTin>) {
        try {
            (activity as CIFRootActivity).purposeOfTinList = it
            fivReasonTin1.setItemForPurposeOfTin1(it)
            fivReasonTin2.setItemForPurposeOfTin2(it)
            fivReasonTin3.setItemForPurposeOfTin3(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin = it
            /*Comments*/
            (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN1?.let {
                if (!it.isNullOrEmpty()) {
                    commentBox1ET?.setText(it)
                    fivTaxResidenceySP?.setSelection(1)
                }
            }
            (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN2?.let {
                if (!it.isNullOrEmpty()) {
                    commentBox2ET?.setText(it)
                    fivTaxResidenceySP?.setSelection(1)
                    tin2IDCheckBox.isChecked = true
                    tin2Layout.isVisible = true
                }
            }
            (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN3?.let {
                if (!it.isNullOrEmpty()) {
                    commentBox3ET?.setText(it)
                    fivTaxResidenceySP?.setSelection(1)
                    tin3IDCheckBox.isChecked = true
                    tin3Layout.isVisible = true
                }
            }
            fivReasonTin1.remainSelection(it.size)
            fivReasonTin2.remainSelection(it.size)
            fivReasonTin3.remainSelection(it.size)
            /*Reson in Case of no tin1*/
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinIndex(
                it,
                (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN1
            )?.let {
                if (it != 0) {
                    fivReasonTin1SP?.setSelection(it!!)
                    fivTaxResidenceySP?.setSelection(1)
                }
            }
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinIndex(
                it,
                (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN2
            )?.let {
                if (it != 0) {
                    fivReasonTin2SP?.setSelection(it!!)
                    fivTaxResidenceySP?.setSelection(1)
                    tin2IDCheckBox.isChecked = true
                    tin2Layout.isVisible = true
                }
            }
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinIndex(
                it,
                (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN3
            )?.let {
                if (it != 0) {
                    fivReasonTin3SP?.setSelection(it!!)
                    fivTaxResidenceySP?.setSelection(1)
                    tin3IDCheckBox.isChecked = true
                    tin3Layout.isVisible = true
                }
            }
            if ((activity as CIFRootActivity).sharedPreferenceManager.lovCountries.isNullOrEmpty()) {
                callCountries()
            } else {
                setCountries((activity as CIFRootActivity).sharedPreferenceManager.lovCountries)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun callCountries() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    response.data?.let { setCountries(it) };
                }

                override fun CountryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setCountries(it: ArrayList<Country>) {
        try {
            fivCOOC.setItemForfivCOOC(it)
            fivNOCountry1.setItemForfivNOCountry(it)
            fivNOCountry2.setItemForfivNOCountry(it)
            fivNOCountry3.setItemForfivNOCountry(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovCountries = it
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                (activity as CIFRootActivity).customerInfo.TAX_RES_COUNTRY_NAME
            )?.let {
                if (it != 0) {
                    fivCOOCSP?.setSelection(it!!)
                    fivTaxResidenceySP?.setSelection(1)
                }
            }
            /*Name Of Country 1*/
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC1
            )?.let {
                if (it != 0) {
                    fivNOCountrySP1?.setSelection(it!!)
                    fivTaxResidenceySP?.setSelection(1)
                }
            }

            /*Name Of Country 2*/
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC2
            )?.let {
                if (it != 0) {
                    fivNOCountrySP2?.setSelection(it!!)
                    fivTaxResidenceySP?.setSelection(1)
                    tin2IDCheckBox.isChecked = true
                    tin2Layout.isVisible = true
                }
            }

            /*Name Of Country 3*/
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC3
            )?.let {
                if (it != 0) {
                    fivNOCountrySP3?.setSelection(it!!)
                    fivTaxResidenceySP?.setSelection(1)
                    tin3IDCheckBox.isChecked = true
                    tin3Layout.isVisible = true
                }
            }
            fivNOCountry1.remainSelection(it.size)
            fivNOCountry2.remainSelection(it.size)
            fivNOCountry3.remainSelection(it.size)
            if ((activity as CIFRootActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
                callBool()
            } else {
                setBool((activity as CIFRootActivity).sharedPreferenceManager.lovBool)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun callBool() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore?.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let { setBool(it) };
                }

                override fun BoolFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setBool(it: ArrayList<Bool>) {
        try {
            (activity as CIFRootActivity).sharedPreferenceManager.lovBool = it
            fivTaxResidencey.setItemForfivTaxResidencey(it)
            (activity as CIFRootActivity).customerInfo.TIN1?.let {
                if (!it.isNullOrEmpty()) {
                    fivTin1ET?.setText(it)
                    fivTaxResidenceySP?.setSelection(1)
                }
            }
            (activity as CIFRootActivity).customerInfo.TIN2?.let {
                if (!it.isNullOrEmpty()) {
                    fivTin2ET?.setText(it)
                    fivTaxResidenceySP?.setSelection(1)
                    tin2IDCheckBox.isChecked = true
                    tin2Layout.isVisible = true
                }
            }
            (activity as CIFRootActivity).customerInfo.TIN3?.let {
                if (!it.isNullOrEmpty()) {
                    fivTin3ET?.setText(it)
                    fivTaxResidenceySP?.setSelection(1)
                    tin3IDCheckBox.isChecked = true
                    tin3Layout.isVisible = true
                }
            }
            if (isCRSPageOpenCheck()) {
                val is_Force_Crs =
                    (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.IS_FORCE_CRS
                if (is_Force_Crs == "" || is_Force_Crs == "1") {
                    fivTaxResidenceySP?.setSelection(1)
                    defaultIsForceCRS = true
                    crsLayout.visibility = View.VISIBLE
                    isCRSPageEnabled(true)
                } else {
                    fivTaxResidenceySP?.setSelection(0)
                    crsLayout.visibility = View.GONE
                    isCRSPageEnabled(false)
                }
            } else {

            }
            (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun load() {
        tinNumbersEditedCheck()
        reasonOfTin1()
        reasonOfTin2()
        reasonOfTin3()
        (activity as CIFRootActivity).customerInfo.EXPIRY_DATE_OF_OVERSEAS_CARD?.let {
            if (!it.isNullOrEmpty()) {
                expireDate?.setText(it)
                fivTaxResidenceySP?.setSelection(1)
            }
        }
        (activity as CIFRootActivity).customerInfo.OVERSEAS_CARD_NUMBER?.let {
            if (!it.isNullOrEmpty()) {
                fivOCNET?.setText(it)
                fivTaxResidenceySP?.setSelection(1)
            }
        }
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin.isNullOrEmpty()) {
            callPurposeOfTin()
        } else {
            setPurposeOfTin((activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin)
        }
    }

    fun taxResidenceConditions() {
        fivTaxResidenceySP?.onItemSelectedListener =
            object : AdapterView.OnItemSelectedListener {
                override fun onNothingSelected(p0: AdapterView<*>?) {

                }

                override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                    try {
                        if (defaultIsForceCRS) {
                            defaultIsForceCRS = false
                        } else if (p2 == 1) {
                            crsLayout.visibility = View.VISIBLE
                            isCRSPageEnabled(true)
                            Log.i("USFORCECRS", "TRUE")
                            (activity as CIFRootActivity).customerInfo.IS_FORCE_CRS = "1"
                        } else {
                            rmvspModels?.rmvCRS()
                            isCRSPageEnabled(false)
                            crsLayout.visibility = View.GONE
                            Log.i("USFORCECRS", "FALSE")
                            (activity as CIFRootActivity).customerInfo.IS_FORCE_CRS = "0"
                        }
                        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo =
                            (activity as CIFRootActivity).customerInfo
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }
            }

    }

    fun isCRSPageEnabled(isCRSPageOpen: Boolean) {
        if (isCRSPageOpen) {
            (activity as CIFRootActivity).customerInfo.TAX_PAYER_OTHER_COUNTRY = "1"
            (activity as CIFRootActivity).customerInfo.TAX_PAYER_BOOl = "1"
        } else {
            (activity as CIFRootActivity).customerInfo.TAX_PAYER_OTHER_COUNTRY = "0"
            (activity as CIFRootActivity).customerInfo.TAX_PAYER_BOOl = "0"
        }
    }

    fun isCRSPageOpenCheck(): Boolean {
        val nationalityCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY
        val secondNationalityCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.SECOND_NATIONALITY
        val countryOfBirth =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_BIRTH
        val currentAddressCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.getOrNull(0)?.COUNTRY
        val permanentAddressCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.getOrNull(1)?.COUNTRY
        val officeAddressCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.getOrNull(2)?.COUNTRY
        val residenceCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES
        val pkCountryCode =
            (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.COUNTRY_DIAL_CODE
        if (nationalityCountry != "PK" ||
            nationalityCountry == "US" ||
            countryOfBirth != "PK" ||
            secondNationalityCountry == "US" ||
            secondNationalityCountry != "" ||
            officeAddressCountry != "PK" ||
            currentAddressCountry != "PK" ||
            permanentAddressCountry != "PK" ||
            residenceCountry != "PK" ||
            (pkCountryCode != "0092" &&
                    pkCountryCode != "092")
        ) {
            return true
        } else {
            return false
        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(btNext, false)
                        (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            activity?.runOnUiThread {
                                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!,
                                    true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.btBack -> {
                isFATCAPageOpen()
            }
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                isFATCAPageOpen()
                true
            }
            false
        }
    }

    fun saveAndNext() {
        if (fivTaxResidenceySP?.selectedItemPosition == 1) {
            expireDate?.text.toString()
                .let {
                    if (it != resources!!.getString(R.string.date_picker))
                        (activity as CIFRootActivity).customerInfo.EXPIRY_DATE_OF_OVERSEAS_CARD = it
                }
            fivOCNET?.text.toString()
                .let { (activity as CIFRootActivity).customerInfo.OVERSEAS_CARD_NUMBER = it }

            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                fivCOOCSP?.selectedItem.toString()
            ).let {
                if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as CIFRootActivity).customerInfo.TAX_RES_COUNTRY = it.toString()
                    (activity as CIFRootActivity).customerInfo.TAX_RES_COUNTRY_NAME =
                        fivCOOCSP?.selectedItem.toString()
                }
            }
            /*Name Of Country 1*/
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                fivNOCountrySP1?.selectedItem.toString()
            ).let {
                if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as CIFRootActivity).customerInfo.NAME_OF_COUNTRY1 = it.toString()
                    (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC1 =
                        fivNOCountrySP1?.selectedItem.toString()
                }
            }

            fivTin1ET?.text.toString()
                .let {
                    if (fivTin1ET?.isEnabled == true) {
                        (activity as CIFRootActivity).customerInfo.TIN1 = it
                    } else {
                        (activity as CIFRootActivity).customerInfo.TIN1 = ""
                    }
                }
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                (activity as CIFRootActivity).purposeOfTinList,
                fivReasonTin1SP?.selectedItem.toString()
            ).let {
                if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0" && fivReasonTin1SP?.isEnabled == true) {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN1 =
                        it.toString()
                } else {
                    (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN1 =
                        ""
                }
            }

            commentBox1ET?.text.toString()
                .let { (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN1 = it }


            /*Name Of tin Country 2*/
            if (tin2IDCheckBox.isChecked) {
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                    fivNOCountrySP2?.selectedItem.toString()
                ).let {
                    if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                        (activity as CIFRootActivity).customerInfo.NAME_OF_COUNTRY2 = it.toString()
                        (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC2 =
                            fivNOCountrySP2?.selectedItem.toString()
                    }
                }

                fivTin2ET?.text.toString().let {
                    if (fivTin2ET?.isEnabled == true) {
                        (activity as CIFRootActivity).customerInfo.TIN2 = it
                    } else {
                        (activity as CIFRootActivity).customerInfo.TIN2 = ""
                    }
                }
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                    (activity as CIFRootActivity).purposeOfTinList,
                    fivReasonTin2SP?.selectedItem.toString()
                ).let {
                    if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0" && fivReasonTin2SP?.isEnabled == true) {// when select there is no item selected "Choose an item"  <--- hint
                        (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN2 =
                            it.toString()
                    } else {
                        (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN2 = ""
                    }
                }
                commentBox2ET?.text.toString()
                    .let {
                        (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN2 = it
                    }
            } else {
                (activity as CIFRootActivity).customerInfo.NAME_OF_COUNTRY2 = ""
                (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC2 = ""
                (activity as CIFRootActivity).customerInfo.TIN2 = ""
                (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN2 = ""
                (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN2 = ""
            }
            /*Name Of tin Country 3*/
            if (tin2IDCheckBox.isChecked) {
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCountryCode(
                    (activity as CIFRootActivity).sharedPreferenceManager.lovCountries,
                    fivNOCountrySP3?.selectedItem.toString()
                ).let {
                    if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                        (activity as CIFRootActivity).customerInfo.NAME_OF_COUNTRY3 = it.toString()
                        (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC3 =
                            fivNOCountrySP3?.selectedItem.toString()
                    }
                }

                fivTin3ET?.text.toString().let {
                    if (fivTin3ET?.isEnabled == true) {
                        (activity as CIFRootActivity).customerInfo.TIN3 = it
                    } else {
                        (activity as CIFRootActivity).customerInfo.TIN3 = ""
                    }
                }
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                    (activity as CIFRootActivity).purposeOfTinList,
                    fivReasonTin3SP?.selectedItem.toString()
                ).let {
                    if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0" && fivReasonTin3SP?.isEnabled == true) {// when select there is no item selected "Choose an item"  <--- hint
                        (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN3 =
                            it.toString()
                    } else {
                        (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN3 = ""
                    }
                }

                commentBox3ET?.text.toString()
                    .let {
                        (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN3 = it
                    }
            } else {
                (activity as CIFRootActivity).customerInfo.NAME_OF_COUNTRY3 = ""
                (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC3 = ""
                (activity as CIFRootActivity).customerInfo.TIN3 = ""
                (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN3 = ""
                (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN3 = ""
            }
        } else {
            (activity as CIFRootActivity).customerInfo.EXPIRY_DATE_OF_OVERSEAS_CARD = ""
            (activity as CIFRootActivity).customerInfo.OVERSEAS_CARD_NUMBER = ""
            (activity as CIFRootActivity).customerInfo.TAX_RES_COUNTRY = ""
            (activity as CIFRootActivity).customerInfo.TAX_RES_COUNTRY_NAME = ""
            (activity as CIFRootActivity).customerInfo.NAME_OF_COUNTRY1 = ""
            (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC1 = ""
            (activity as CIFRootActivity).customerInfo.TIN1 = ""
            (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN1 = ""
            (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN1 = ""
            (activity as CIFRootActivity).customerInfo.NAME_OF_COUNTRY2 = ""
            (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC2 = ""
            (activity as CIFRootActivity).customerInfo.TIN2 = ""
            (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN2 = ""
            (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN2 = ""
            (activity as CIFRootActivity).customerInfo.NAME_OF_COUNTRY3 = ""
            (activity as CIFRootActivity).customerInfo.NAMEOFCOUNTRYDESC3 = ""
            (activity as CIFRootActivity).customerInfo.TIN3 = ""
            (activity as CIFRootActivity).customerInfo.REASON_IN_CASE_OF_NO_TIN3 = ""
            (activity as CIFRootActivity).customerInfo.COMMENT_IN_CASE_OF_NO_TIN3 = ""
        }
        if (!isFATCAPageOpenCheck()) {
            rmvspModels?.rmvFATCA()
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo =
            (activity as CIFRootActivity).customerInfo
    }

    fun validation(): Boolean {
        var overseasCardNo = fivOCNET?.text.toString().trim()
        var expireDate = expireDate?.text.toString().trim()
        var REASON_IN_CASE_OF_NO_TIN1 =
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                (activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin,
                fivReasonTin1SP?.selectedItem.toString()
            )
        var REASON_IN_CASE_OF_NO_TIN2 =
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                (activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin,
                fivReasonTin2SP?.selectedItem.toString()
            )
        var REASON_IN_CASE_OF_NO_TIN3 =
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPurposeOfTinCode(
                (activity as CIFRootActivity).sharedPreferenceManager.lovPurposeOfTin,
                fivReasonTin3SP?.selectedItem.toString()
            )
        if (fivCOOCSP?.selectedItemPosition == 0 && overseasCardNo.length != 0 && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*country will not be empty*/
            ToastUtils.normalShowToast(
                activity,
                resources.getString(R.string.please_select_overseas_country), 1
            )
            return false
        } else if (expireDate.equals(resources.getString(R.string.date_picker)) && overseasCardNo.length != 0 && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*expiry date of overseas must be select*/
            ToastUtils.normalShowToast(
                activity,
                resources.getString(R.string.please_select_overSeasCountryAndExpiryDateStr),
                1
            )
            return false
        } else if (fivNOCountrySP1?.selectedItemPosition == 0 && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*tin 1 name of country 1 if not select*/
            ToastUtils.normalShowToast(
                activity,
                resources.getString(R.string.please_select_country1), 1
            )
            return false
        } else if (fivTin1ET?.text?.length == 0 && fivTin1ET?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin is yes then reason of tin must be select*/
            fivTin1ET?.setError(resources!!.getString(R.string.please_select_tin1_number))
            fivTin1ET?.requestFocus()
            return false
        } else if (fivReasonTin1SP?.selectedItemPosition == 0 && fivReasonTin1SP?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin is yes then reason of tin must be select*/
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_select_reason_tin1),
                1
            )
            return false
        } else if (REASON_IN_CASE_OF_NO_TIN1 == "B" && commentBox1ET?.text?.length == 0 && fivReasonTin1SP?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin is yes then reason of tin must be select*/
            commentBox1ET?.setError(resources!!.getString(R.string.please_enter_comment_box_1_err))
            commentBox1ET?.requestFocus()
            return false
        } else if (tin2IDCheckBox.isChecked && fivNOCountrySP2?.selectedItemPosition == 0 && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin 2 is checked so name of country 2 must be select*/
            ToastUtils.normalShowToast(
                activity,
                resources.getString(R.string.please_select_country2), 1
            )
            return false
        } else if (tin2IDCheckBox.isChecked && fivTin2ET?.text?.length == 0 && fivTin2ET?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin 2 and tin2 is set to yes then reason of tin 2 must be select*/
            fivTin2ET?.setError(resources!!.getString(R.string.please_select_tin2_number))
            fivTin2ET?.requestFocus()
            return false
        } else if (tin2IDCheckBox.isChecked && fivReasonTin2SP?.selectedItemPosition == 0 && fivReasonTin2SP?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin is yes then reason of tin must be select*/
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_select_reason_tin2),
                1
            )
            return false
        } else if (tin2IDCheckBox.isChecked && REASON_IN_CASE_OF_NO_TIN2 == "B" && commentBox2ET?.text?.length == 0 && fivReasonTin2SP?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin is yes then reason of tin must be select*/
            commentBox2ET?.setError(resources!!.getString(R.string.please_enter_comment_box_2_err))
            commentBox2ET?.requestFocus()
            return false
        } else if (tin3IDCheckBox.isChecked && fivNOCountrySP3?.selectedItemPosition == 0 && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin3 is checked then name of country must be select*/
            ToastUtils.normalShowToast(
                activity,
                resources.getString(R.string.please_select_country3), 1
            )
            return false
        } else if (tin3IDCheckBox.isChecked && fivTin3ET?.text?.length == 0 && fivTin3ET?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin 3 is checked so tin 3 is set to yes and id yes then reason of tin 3 must be select*/
            fivTin3ET?.setError(resources!!.getString(R.string.please_select_tin3_number))
            fivTin3ET?.requestFocus()
            return false
        } else if (tin3IDCheckBox.isChecked && fivReasonTin3SP?.selectedItemPosition == 0 && fivReasonTin3SP?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin is yes then reason of tin must be select*/
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_select_reason_tin3),
                1
            )
            return false
        } else if (tin3IDCheckBox.isChecked && REASON_IN_CASE_OF_NO_TIN3 == "B" && commentBox3ET?.text?.length == 0 && fivReasonTin3SP?.isEnabled == true && fivTaxResidenceySP?.selectedItemPosition == 1) {
            /*if tin is yes then reason of tin must be select*/
            commentBox3ET?.setError(resources!!.getString(R.string.please_enter_comment_box_3_err))
            commentBox3ET?.requestFocus()
            return false
        } else {
            return true
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as CIFRootActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as CIFRootActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID()
                .toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as CIFRootActivity).aofAccountInfoRequest = it
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        isCRSEnabled()
                        (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun isCRSEnabled() {
        if (fivTaxResidenceySP?.selectedItemPosition == 1) {
            if (findNavController().currentDestination?.id == R.id.CIFStep3_4)
                findNavController().navigate(R.id.action_CIFStep3_4_to_CIFStep3_5)
        } else {
            if (findNavController().currentDestination?.id == R.id.CIFStep3_4)
                findNavController().navigate(R.id.action_CIFStep3_4_to_customer_demographics_page)
        }
        (activity as CIFRootActivity).recyclerViewSetup()
    }


    fun isFATCAPageOpen() {
        var custSegType =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        if (isFATCAPageOpenCheck()) {
            if (findNavController().currentDestination?.id == R.id.CIFStep3_4)
                findNavController().navigate(R.id.action_CIFStep3_4_to_CIFStep3_2)
        } else if (custSegType == "A0" || custSegType == "A2" || custSegType == "A4") {
            if (findNavController().currentDestination?.id == R.id.CIFStep3_4)
                findNavController().navigate(R.id.action_CIFStep3_4_to_CIFStep2_5)
        } else /*if (isCRSPageOpenCheck()) */ {
            if (findNavController().currentDestination?.id == R.id.CIFStep3_4)
                findNavController().navigate(R.id.action_CIFStep3_4_to_CIFStep2_7)
        }/* else {
            findNavController().navigate(R.id.action_goto_CDD)
        }*/
        activity?.runOnUiThread {
            (activity as CIFRootActivity).recyclerViewSetup()
        }
    }

    fun isFATCAPageOpenCheck(): Boolean {
        val nationalityCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY
        val secondNationalityCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.SECOND_NATIONALITY
        val residentOfCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES
        val birthOfCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_BIRTH
        val officeAddressCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.getOrNull(0)?.COUNTRY
        val currentAddressCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.getOrNull(1)?.COUNTRY
        val permanentAddressCountry =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.getOrNull(2)?.COUNTRY
        val residenceNumber = (activity as CIFRootActivity).residenceNumberFATCA
        val officeNumber = (activity as CIFRootActivity).officeNumberFATCA
        val checkedUSDialCode = (activity as CIFRootActivity).isCheckedUSDialCode
        return if (nationalityCountry == "US" ||
            secondNationalityCountry == "US" ||
            residentOfCountry == "US" ||
            birthOfCountry == "US" ||
            officeAddressCountry == "US" ||
            currentAddressCountry == "US" ||
            permanentAddressCountry == "US" ||
            residenceNumber == 1 ||
            officeNumber == 1 ||
            checkedUSDialCode == 1
        ) {
            (activity as CIFRootActivity).customerInfo.FATCA = 1
            (this.context as CIFRootActivity).sharedPreferenceManager.customerInfo =
                (this.context as CIFRootActivity).customerInfo
            true
        } else {
            (activity as CIFRootActivity).customerInfo.FATCA = 0
            (this.context as CIFRootActivity).sharedPreferenceManager.customerInfo =
                (this.context as CIFRootActivity).customerInfo
            false
        }
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
