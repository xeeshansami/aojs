package com.hbl.bot.ui.fragments.quickopeningaccount
import android.content.ActivityNotFoundException
import java.io.FileNotFoundException
import java.io.IOException
import com.hbl.bot.ui.activities.OpenQuickAccountActivity
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.NavigationDrawerActivity
import com.hbl.bot.utils.Constants
import com.hbl.bot.utils.SendEmail
import com.hbl.bot.utils.ToastUtils
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep1.btCheck
import kotlinx.android.synthetic.main.fragment_qoaccstep1.*
import kotlinx.android.synthetic.main.quick_acc_open_content_main.*
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class QOACCStep1 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var docTypeSpinner: Spinner? = null
    var customerSegmentSpinner: Spinner? = null
    var documentNumber: EditText? = null
    var nationalitySpinner: Spinner? = null
    var secondNationalitySpinner: Spinner? = null
    var dualNationalitySpinner: Spinner? = null
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        try{
        if (myView == null) {
            // Inflate the layout for this fragment
            myView = inflater.inflate(R.layout.fragment_qoaccstep1, container, false)
            callDocs()
        }
        } catch (e: UnsatisfiedLinkError) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: IOException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as RuntimeException))
        } catch (e: Exception) {
            activity?.findNavController(R.id.nav_host_fragment)!!.navigate(R.id.logout)
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as NavigationDrawerActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        (activity as OpenQuickAccountActivity).siIndicator.visibility = View.GONE
        btn_quick_open_account_submit.setOnClickListener(this)
        btCheck.setOnClickListener(this)
        init()
    }

    fun init() {
    }

    fun callDocs() {
        (activity as OpenQuickAccountActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.DOCS_IDENTIFIER
        HBLHRStore.instance?.getDocs(RetrofitEnums.URL_HBL, lovRequest, object : DocsCallBack {
            override fun DocsSuccess(response: DocsResponse) {
                response.data?.let {
                    documentType_qoacc_spn.setItemForDocs(it)
//                    (activity as OpenQuickAccountActivity).docsList = it
                }
                callCountries()
            }

            override fun DocsFailure(response: BaseResponse) {
                ToastUtils.normalShowToast(activity, response.message,1)
                (activity as OpenQuickAccountActivity).globalClass?.hideLoader()
            }
        })
    }

    fun callCountries() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    response.data?.let {
//                        (activity as OpenQuickAccountActivity).countryList=it
                        countryOfBirth_qoacc_spn?.setItemForfivNationality(it)
                        countryOfResidence_qoacc_spn?.setItemForfivSecondNationality(it)
                    };
                    getAccountType()

                }

                override fun CountryFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message,1)
                    (activity as OpenQuickAccountActivity).globalClass?.hideLoader()
                }

            })
    }

    fun getAccountType() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.ACCOUNT_TYPE_IDENTIEFIER
        HBLHRStore.instance?.getAccountType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : AccountTypeCallBack {
                override fun AccountTypeSuccess(response: AccountTypeResponse) {
                    response.data?.let {
                        accountType_qoacc_spn.setItemForAccountType(it)
                    };
                    (activity as OpenQuickAccountActivity).globalClass?.hideLoader()
                }

                override fun AccountTypeFailure(response: BaseResponse) {
                    Toast.makeText(
                        (activity as OpenQuickAccountActivity).globalClass,
                        response.message,
                        Toast.LENGTH_SHORT
                    ).show()
                    (activity as OpenQuickAccountActivity).globalClass?.hideLoader()
                }
            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.btCheck -> {
                (activity as OpenQuickAccountActivity).globalClass?.showDialog(activity)
                Handler().postDelayed({
                    (activity as OpenQuickAccountActivity).globalClass?.hideLoader()
                }, 2000)
            }
            R.id.btn_quick_open_account_submit -> {
                /*    val index = viewModel.mList.indexOfFirst {
                        it.title == getString(R.string.customer_id_info)
                    }
                    viewModel.mList[index].item = DrawerItem.ITEM_DONE*/
                /*        saveDataAndNext()
                        findNavController().navigate(R.id.action_CIFStep1_to_CIFStep1_2)*/
                (activity as OpenQuickAccountActivity).globalClass?.showDialog(activity)
                Handler().postDelayed({
                    ToastUtils.normalShowToast(activity, "Request submit successfully",2)
                    (activity as OpenQuickAccountActivity).globalClass?.hideLoader()
                    findNavController().navigate(R.id.action_QOACCStep1_QOACCStep2)
                }, 2000)
            }

        }
    }

}
