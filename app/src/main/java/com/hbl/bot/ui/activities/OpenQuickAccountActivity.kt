package com.hbl.bot.ui.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.hbl.bot.R
import com.hbl.bot.utils.GlobalClass
import com.hbl.bot.utils.LoggingExceptionHandler
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_cifroot.*

class OpenQuickAccountActivity : AppCompatActivity() {
    val viewModel: SharedCIFViewModel by viewModels()

    @JvmField
    var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()

    @JvmField
    var globalClass: GlobalClass? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        globalClass = this.applicationContext as GlobalClass
        sharedPreferenceManager.getInstance(this)
        Thread.setDefaultUncaughtExceptionHandler(LoggingExceptionHandler(globalClass as GlobalClass));
        setContentView(R.layout.activity_open_quick_account)
        val toolbar: Toolbar = findViewById(R.id.toolbar)
        setSupportActionBar(toolbar)
        tvTitle.setText("QUICK OPEN ACCOUNT")
        viewModel.showAvatar.observe(this, androidx.lifecycle.Observer {
            if (it)
                ivAvatar.visibility = View.VISIBLE
            else ivAvatar.visibility = View.GONE

        })

        viewModel.currentFragmentIndex.observe(this, androidx.lifecycle.Observer {
            siIndicator.currentStep = it
            // it is your current fragment index

        })

        viewModel.totalSteps.observe(this, androidx.lifecycle.Observer {
            siIndicator.stepCount = it

        })

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        val host: NavHostFragment = supportFragmentManager
            .findFragmentById(R.id.cifHostFragment) as NavHostFragment? ?: return
        val fragmen = host.childFragmentManager.fragments.get(0)
        fragmen.onActivityResult(requestCode, resultCode, intent)
        host.onActivityResult(requestCode, resultCode, intent)
        viewModel.isPhotoTaken.value = true

    }


    override fun onBackPressed() {
        if (findNavController(R.id.cifHostFragment).popBackStack()) {
            findNavController(R.id.cifHostFragment).popBackStack()
        } else {
            finish()
        }
    }
}
