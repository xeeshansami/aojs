package com.hbl.bot.ui.customviews

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.hbl.bot.R

class TwoButtonsDialog(
    c: Context,
    private var headerTitle: String,
    message: String,
    positiveTitle: String,
    negativeTitle: String,
    callback: DialogCallback

) :
    Dialog(c), View.OnClickListener {
    var d: Dialog? = null
    var yes: Button? = null
    var no: Button? = null
    var message: String
    var positiveTitle: String
    var negativeTitle: String
    private var callback: DialogCallback
    var tvMessage: TextView? = null
    var tvHeader: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.popup_two_buttons_dialog)
        yes = findViewById(R.id.bt_confirm)
        no = findViewById(R.id.bt_cancel)
        tvMessage = findViewById(R.id.tv_message)
        tvHeader = findViewById(R.id.tv_header)
        tvHeader?.text = headerTitle
        tvMessage?.text = message
        yes?.text = positiveTitle
        no?.text = negativeTitle
        yes?.setOnClickListener(this)
        no?.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.bt_confirm -> callback.onPositiveClicked()
            R.id.bt_cancel -> callback.onNegativeClicked()
            else -> {
            }
        }
        dismiss()
    }

    init {
        this.callback = callback
        this.message = message
        this.positiveTitle = positiveTitle
        this.negativeTitle = negativeTitle
    }
}

interface DialogCallback {
    fun onPositiveClicked() {}
    fun onNegativeClicked() {}
}