package com.hbl.bot.ui.fragments.cif


import com.hbl.bot.ui.activities.HawActivity
import android.annotation.SuppressLint
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.request.baseRM.*
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep5_7.*
import kotlinx.android.synthetic.main.fragment_cifstep5_7.btBack
import kotlinx.android.synthetic.main.fragment_cifstep5_7.openaccount_btn
import kotlinx.android.synthetic.main.fragment_cifstep5_7.formSectionHeader
import kotlinx.android.synthetic.main.fragment_cifstep5_7.opencif_btn
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class HawCifCDD4Fragment26 : Fragment(), View.OnClickListener, GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    var currentAddress = ""
    var TAG="GeoLocationGenerateACC_CIF"
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocation: Location? = null
    private var locationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    var rmvspModels: HawRMVSPModels? = null
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivBreifRemarkET: EditText? = null
    var fivInputtedByET: EditText? = null
    var fivSupervisedByET: EditText? = null
    var fivApprovedByET: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {

            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep5_7, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    fun header() {
        viewModel.totalSteps.setValue(4)
        viewModel.currentFragmentIndex.setValue(3)
        val txt = resources.getString(R.string.cdd_verification)
        val txt1 = " (4/4 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            openaccount_btn.setOnClickListener(this)
            btBack.setOnClickListener(this)
            opencif_btn.setOnClickListener(this)
            header()
            locationGet()
            init()
            load()
            setLengthAndType()
            /* if (GlobalClass.isDummyDataTrue) {
                 loadDummyData()
             }*/
            onBackPress(view)
            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun load() {
        (activity as HawActivity).customerCdd.dESC_BREIF_REMARKS
            .let { fivBreifRemarkET?.setText(it) }
        (activity as HawActivity).customerCdd.fILLED_BY
            .let { fivInputtedByET?.setText(it) }
        (activity as HawActivity).customerCdd.aPPROVED_BY_AOF
            .let { fivSupervisedByET?.setText(it) }
        (activity as HawActivity).customerCdd.aPPROVED_BY_BR_MGR
            .let { fivApprovedByET?.setText(it) }
    }

    private fun setLengthAndType() {
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivBreifRemarkET!!,
            77,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivInputtedByET!!,
            25,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivSupervisedByET!!,
            25,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivApprovedByET!!,
            25,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
    }

    fun init() {
        rmvspModels = HawRMVSPModels(activity as HawActivity)
        fivBreifRemarkET = fivBreifRemark.getTextFromEditText(R.id.breadRemarksID)
        fivInputtedByET = fivInputtedBy.getTextFromEditText(R.id.inputtedID)
        fivSupervisedByET = fivSupervisedBy.getTextFromEditText(R.id.fivSupervisedBy)
        fivApprovedByET = fivApprovedBy.getTextFromEditText(R.id.approvedByID)

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.equals( "HIGH",true) ||
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING.equals( "HIGH",true)
        ) {
//            Risk Status is high
            openaccount_btn?.setText(activity?.resources?.getString(R.string.next))
            opencif_btn?.visibility = View.GONE
        }
//        //TODO: IF CRC and CUSTOMER SEGMENT MINOR/GUARDIAN then show only open account button will be shown
        if ((activity as HawActivity).sharedPreferenceManager.customerInfo.iddocumenttype == "07" &&
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7"
        ) {
            opencif_btn?.visibility = View.GONE
            openaccount_btn?.visibility = View.VISIBLE
        }
    }

    private fun locationGet() {
        mGoogleApiClient = GoogleApiClient.Builder(activity as HawActivity)
            .addConnectionCallbacks(this@HawCifCDD4Fragment26)
            .addOnConnectionFailedListener(this@HawCifCDD4Fragment26)
            .addApi(LocationServices.API)
            .build()
        locationManager = (activity as HawActivity).getSystemService(Context.LOCATION_SERVICE) as LocationManager?
    }
    fun getAddress(lat: Double, lng: Double) {
        val gCoder = Geocoder(activity as HawActivity)
        var addresses: List<Address>? = null
        try {
            addresses = gCoder.getFromLocation(lat, lng, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (addresses != null && addresses.isNotEmpty()) {
            currentAddress= addresses[0].getAddressLine(0)
            Log.i(TAG, addresses[0].getAddressLine(0))
        }
    }
    override fun onConnected(bundle: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                activity as HawActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity as HawActivity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        startLocationUpdates()
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (mLocation == null) {
            startLocationUpdates()
        }
        if (mLocation != null) {
            val latitude = mLocation!!.getLatitude()
            val longitude = mLocation!!.getLongitude()
        } else {
            // Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }
    private fun startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000)        // 10 seconds, in milliseconds
            .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                activity as HawActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity as HawActivity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.i(
                TAG,
                "Location Allowed"
            )
            return
        }else{
            Log.i(
                TAG,
                "Location Allowed"
            )
        }
        val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient!!,
                mLocationRequest!!,
                object :com.google.android.gms.location.LocationListener{
                    override fun onLocationChanged(p0: Location) {
                        Log.i(
                            TAG,
                            "${p0!!.latitude},${p0!!.longitude}"
                        )
                        currentLatitude = p0!!.latitude
                        currentLongitude = p0!!.longitude
                    }

                })
            Log.i(
                TAG,
                "$currentLatitude WORKS $currentLongitude"
            )
        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.latitude
            currentLongitude = location.longitude
            //If everything went fine lets get latitude and longitude
            getAddress(currentLatitude,currentLongitude)
            Log.i(
                TAG,
                "$currentLatitude WORKS $currentLongitude"
            )
        }
        Log.d("reque", "--->>>>")
    }
    override fun onConnectionSuspended(i: Int) {
        Log.i(TAG, "Connection Suspended")
        mGoogleApiClient!!.connect()
    }
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.errorCode)
    }
    override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }
    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.disconnect()
        }
    }

    private fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
//            openaccount_btn.setText("CIF VERIFIED")
            (activity as HawActivity).globalClass?.setDisbaled(fivBreifRemarkET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivInputtedByET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivSupervisedByET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivApprovedByET!!, false)
        }
        opencif_btn.visibility = View.GONE
    }

    fun loadDummyData() {
        fivBreifRemarkET?.setText("HBL team loves to listen to & answer your queries and comments")
        fivInputtedByET?.setText("Sheikh Sabir Zayyan")
        fivSupervisedByET?.setText("Abdul Wajid Ali Khan")
        fivApprovedByET?.setText("Ahmed Zubair Kazmi")
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.opencif_btn -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).sharedPreferenceManager.appStatus.CIF.let {
                            (activity as HawActivity).globalClass?.appStatus = it.toString()
                        }
                        (activity as HawActivity).globalClass?.setDisbaled(opencif_btn!!, false)
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            cif()
                            activity?.runOnUiThread {
                                (activity as HawActivity).globalClass?.setEnabled(
                                    opencif_btn,
                                    true
                                )
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                }
            }
            R.id.openaccount_btn -> {
                if (validation()) {
                    (activity as HawActivity).globalClass?.setDisbaled(openaccount_btn, false)
                    (this.context as HawActivity).globalClass?.showDialog(this.context)
                    verifyAccountApi()
                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep5_7)
                    findNavController().navigate(R.id.action_CIFStep5_7_to_CIFStep5_5)
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep5_7)
                    findNavController().navigate(R.id.action_CIFStep5_7_to_CIFStep5_5)
                true
            }
            false
        }
    }

    private fun verifyAccountApi() {
        var verifyAccount = VerifyAccountRequest()
        verifyAccount.cnic =
            (activity as HawActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_NO
        verifyAccount.ACCT_TYPE = ""
        verifyAccount.identifier =Constants.TYPE_ACCOUNT_VALIDATION_IDENTIFIER_HAW
        HBLHRStore.instance?.VerifyAccount(
            RetrofitEnums.URL_HBL,
            verifyAccount,
            object : VerifyAccountCallBack {
                override fun VerifyAccountSuccess(response: VerifyAccountResponse) {
                    try {
                        if (response.status == "00") {
                            java.util.concurrent.Executors.newSingleThreadExecutor()
                                .execute(Runnable {
                                    saveAndNext()
                                    setAllModelsInAOFRequestModel()
                                })
                            (activity as HawActivity).recyclerViewSetup()
                        } else if (response.status == "01") {
                            callIfAccountAlreadyExist((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ID_DOC_NO)
                        } else {
                            ToastUtils.normalShowToast(context, response.message.toString(), 1)
                        }
                        (activity as HawActivity).globalClass?.setEnabled(openaccount_btn, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun VerifyAccountFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) { verifyAccountApi() }
                    ToastUtils.normalShowToast(context, response.message.toString(), 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun callIfAccountAlreadyExist(cNIC: String) {
        var request = DraftMaintainanceRequest()
        request.DOC_ID = cNIC
        request.identifier = Constants.CNIC_MAINTANACE_INDENTIFIER
        var baseResponse = Gson().toJson(request)
        Log.i("RequestJson",baseResponse)
        HBLHRStore.instance?.callDraftMaintanance(
            RetrofitEnums.URL_HBL,
            request,
            object : DraftCallBack {
                override fun DraftSuccess(response: AofAccountInfoResponse) {
                    HawDataSetupInSp(activity as HawActivity, response!!, false)
                    if (response.data.getOrNull(0)?.ACCT_STATUS == "1") {
                        if (findNavController().currentDestination?.id == R.id.CIFStep5_7)
                            findNavController().navigate(R.id.action_CIFStep5_7_to_summary_page)
                    } else if (response.data.getOrNull(0)?.ACCT_STATUS == "2") {
                        if (findNavController().currentDestination?.id == R.id.CIFStep5_7)
                            findNavController().navigate(R.id.action_CIFStep5_7_to_document_upload)
                    }else if (response.data.getOrNull(0)?.ACCT_STATUS == "3") {
                        stopAlert()
                    }
                    (activity as HawActivity).globalClass?.hideLoader()
                }

                override fun DraftFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun stopAlert() {
        var dialogBuilder = AlertDialog.Builder(activity as HawActivity)
        val layoutView: View = layoutInflater.inflate(R.layout.popup_etbntb, null)
        dialogBuilder.setView(layoutView)
        var alertDialog = dialogBuilder.create()
        alertDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        alertDialog.setCancelable(false)
        alertDialog.show()
        var toolbar = layoutView.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar)
        var desctext1 = layoutView.findViewById<TextView>(R.id.desctext1)
        var yes = layoutView.findViewById<Button>(R.id.yes_action)
        toolbar.title ="Alert"
        desctext1.text = getString(R.string.alreadyAccountAlertStr)
        yes.text = "OK"
        yes.setOnClickListener(View.OnClickListener {
            alertDialog.dismiss()
        })
        alertDialog.show()
    }

    private fun validation(): Boolean {
        var fivBreifRemark = fivBreifRemarkET?.text?.toString()
        var fivInputtedBy = fivInputtedByET?.text?.toString()
        var fivSupervisedBy = fivSupervisedByET?.text?.toString()
        var fivApprovedBy = fivApprovedByET?.text?.toString()
        when {
            fivBreifRemark.isNullOrEmpty() -> {
                fivBreifRemarkET?.error = resources!!.getString(R.string.pleaseEnterBreidRemarksErr)
                fivBreifRemarkET?.requestFocus()
                return false
            }
            fivInputtedBy.isNullOrEmpty() -> {
                fivInputtedByET?.error = resources!!.getString(R.string.pleaseEnterInputtedByErr)
                fivInputtedByET?.requestFocus()
                return false
            }
            fivSupervisedBy.isNullOrEmpty() -> {
                fivSupervisedByET?.error = resources!!.getString(R.string.pleaseEnterSupervisedErr)
                fivSupervisedByET?.requestFocus()
                return false
            }
            fivApprovedBy.isNullOrEmpty() -> {
                fivApprovedByET?.error = resources!!.getString(R.string.pleaseEnterApprovedByErr)
                fivApprovedByET?.requestFocus()
                return false
            }
            else -> {
                return true
            }
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }

        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled!!) {
            aofDataAlign(
                "1",
                Constants.SUBMIT_DRAFT_IDENTIFIER
            )
        } else {
            aofDataAlign(
                "1",
                Constants.UPDATE_DRAFT_IDENTIFIER
            )
        }
    }

    fun aofDataAlign(
        doc_inserted: String,
        identifier: String,
    ) {
        (activity as HawActivity).aofAccountInfoRequest.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let {
                    (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).etbntbFLAG
        } else {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).REMEDIATE
        } else {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as HawActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        (activity as HawActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).fullName
        } else {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).riskRating.toString()
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .equals(
                    "High",
                    true
                )
        ) {
            /*Risk status is high behave normal Move to EDD Page*/
            (activity as HawActivity).sharedPreferenceManager.appStatus.CIF.let {
                (activity as HawActivity).globalClass?.appStatus = it.toString()
            }
            //TODO: set WORK_FLOW_CIF
            (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "1"
            (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC = "CIF"
        } else {
            (activity as HawActivity).sharedPreferenceManager.appStatus.CIFAcct.let {
                (activity as HawActivity).globalClass?.appStatus = it.toString()
            }
            //TODO: set WORK_FLOW_CIF_AND_ACCOUNT
            (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "3"
            (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC =
                "CIF AND ACCOUNT"
        }
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as HawActivity).aofAccountInfoRequest.isForcelyChangedCustomerSegment = false
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF((activity as HawActivity).aofAccountInfoRequest, identifier)
    }

    fun postGenerateCIF(aofAccountInfo: Data, identifier: String) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = identifier
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled!!) {
            //TODO: set WORK_FLOW_ACCOUNT
            aofAccountInfo.WORK_FLOW_CODE = "2"
            aofAccountInfo.WORK_FLOW_CODE_DESC = "ACCOUNT"
        } else {
            //TODO: set WORK_FLOW_ACCOUNT
            aofAccountInfo.WORK_FLOW_CODE = "3"
            aofAccountInfo.WORK_FLOW_CODE_DESC = "CID AND ACCOUNT"
        }
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        Log.i("RequestJson",json)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        if (response.status.equals("00") && (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().equals("High", true)) {
//                            if (findNavController().currentDestination?.id == R.id.CIFStep5_7)
//                                findNavController().navigate(R.id.action_CIFStep5_7_to_CIFStep6)
                            (activity as HawActivity).visitBranchPopup("Customer RISK RATING is HIGH so please visit the nearest branch of HBL")
                        } else {
                            rmvspModels?.rmvEDD()
                            if (response.status.equals("01") &&
                                aofAccountInfo.DOC_INSERTED.equals("1") &&
                                response.message.equals("This tracking id already found in db, please create new record again.")
                            ) {
                                /*When submit identifier api response error then move to update api response call*/
                                aofDataAlign("1", Constants.UPDATE_DRAFT_IDENTIFIER)
                            } else {
                                if (findNavController().currentDestination?.id == R.id.CIFStep5_7)
                                    findNavController().navigate(R.id.route_to_account_page)
                            }
                        }
                        (activity as HawActivity).recyclerViewSetup()
                        (activity as HawActivity).globalClass?.hideLoader()
                        (activity as HawActivity).globalClass?.setEnabled(openaccount_btn, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }


    fun cif() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }

        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }

        saveAndGenerateCIF()
    }

    fun saveAndGenerateCIF() {
        (activity as HawActivity).aofAccountInfoRequest.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let {
                    (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).etbntbFLAG
        } else {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).REMEDIATE
        } else {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as HawActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        (activity as HawActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).fullName
        } else {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).riskRating.toString()
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }

        (activity as HawActivity).sharedPreferenceManager.appStatus.CIF.let {
            (activity as HawActivity).globalClass?.appStatus = it.toString()
        }
        //TODO: set WORK_FLOW_CIF
        (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "1"
        (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC = "CIF"

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as HawActivity).aofAccountInfoRequest.isForcelyChangedCustomerSegment = false
        removeObjects()
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }

        postFinalGenerateCIF((activity as HawActivity).aofAccountInfoRequest)
    }

    fun removeObjects() {
        (activity as HawActivity).globalClass?.clearModel(SharedPreferenceManager.CUSTOMER_EDD_KEY)
        (activity as HawActivity).globalClass?.clearModel(SharedPreferenceManager.CUSTOMER_ACCOUNT)
        (activity as HawActivity).globalClass?.clearModel(SharedPreferenceManager.CUSTOMER_NEXTOFKIN)
        var customerEdd: CUSTEDD = CUSTEDD()
        var customerNextOfKin: CUSTNEXTOFKIN = CUSTNEXTOFKIN()
        var customerAccounts: CUST_ACCOUNTS = CUST_ACCOUNTS()

        (activity as HawActivity).customerAccounts = customerAccounts
        (activity as HawActivity).customerEdd = customerEdd
        (activity as HawActivity).customerNextOfKin = customerNextOfKin

        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    customerEdd
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    customerEdd
                )
            }
        }

        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    customerAccounts
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    customerAccounts
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    customerNextOfKin
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    customerNextOfKin
                )
            }
        }
        activity?.runOnUiThread {
            (activity as HawActivity).recyclerViewSetup()
        }

    }

    fun postFinalGenerateCIF(aofAccountInfo: Data) {
        aofAccountInfo.GEOLOCATION.LATITUDE=currentLatitude.toString()
        aofAccountInfo.GEOLOCATION.LONGITUDE=currentLongitude.toString()
        aofAccountInfo.GEOLOCATION.GEO_LOCATION_ADDRESS=currentAddress.toString()
        var aofAccountInfoRequest = AofAccountInfoRequest()
        aofAccountInfoRequest.identifier = Constants.FINAL_DRAFT_IDENTIFIER
        aofAccountInfoRequest.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(aofAccountInfoRequest)
        Log.i("RequestJson",json)
        HBLHRStore.instance?.postAofaccountInfo(
            RetrofitEnums.URL_HBL,
            aofAccountInfoRequest, object : CustomerAofAccountInfoCallBack {
                @SuppressLint("WrongConstant")
                override fun CustomerAofAccountInfoSuccess(response: AllSubmitResponse) {
                    try {
                        (activity as HawActivity).globalClass?.hideLoader()
                        response.data?.get(0)?.CIF_NO.let { saveCIFNoInSP(it!!) }
                        Handler().postDelayed({
                            response.message?.let {
//                            (activity as HawActivity).globalClass?.clearSubmitAOF()
                                confirmCompletedUpload(it)
                            }
                        }, 500)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun CustomerAofAccountInfoFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun saveCIFNoInSP(cifNo: String) {
        try {
            (activity as HawActivity).customerInfo.CIF_NO = cifNo
            (activity as HawActivity).customerInfo.let {
                (activity as HawActivity).sharedPreferenceManager.customerInfo = it
            }
            (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
                if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                    (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                        0,
                        it
                    )
                } else {
                    (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                        it
                    )
                }
            }
            (activity as HawActivity).aofAccountInfoRequest.let {
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }
    }

    private fun confirmCompletedUpload(msg: String) {
        val builder = AlertDialog.Builder((activity as HawActivity))
        builder.setTitle(msg)
        builder.setCancelable(false)
        builder.setMessage((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let { "Your tracking number is " + it })
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            if (findNavController().currentDestination?.id == R.id.CIFStep5_7)
                findNavController().navigate(R.id.route_to_document_page)
            (activity as HawActivity).recyclerViewSetup()
            (activity as HawActivity).globalClass?.setEnabled(openaccount_btn, true)
        }
        builder.show()
    }

    fun saveAndNext() {
        fivBreifRemarkET?.text.toString()
            .let { (activity as HawActivity).customerCdd.dESC_BREIF_REMARKS = it }
        fivInputtedByET?.text.toString()
            .let { (activity as HawActivity).customerCdd.fILLED_BY = it }
        fivSupervisedByET?.text.toString()
            .let { (activity as HawActivity).customerCdd.aPPROVED_BY_AOF = it }
        fivApprovedByET?.text.toString()
            .let { (activity as HawActivity).customerCdd.aPPROVED_BY_BR_MGR = it }
        (activity as HawActivity).sharedPreferenceManager.customerCDD =
            (activity as HawActivity).customerCdd
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(openaccount_btn, true)
                (activity as HawActivity).globalClass?.setEnabled(opencif_btn, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
