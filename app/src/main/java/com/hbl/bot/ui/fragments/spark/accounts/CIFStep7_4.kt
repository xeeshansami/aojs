package com.hbl.bot.ui.fragments.spark.accounts

import java.io.FileNotFoundException
import java.io.IOException
import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.DebitCardLovRequest
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.CardProduct
import com.hbl.bot.network.models.response.baseRM.PrefComMode
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep7_4.*
import kotlinx.android.synthetic.main.fragment_cifstep7_4.btBack
import kotlinx.android.synthetic.main.fragment_cifstep7_4.btNext
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import kotlinx.android.synthetic.main.view_form_selection.view.*
import java.lang.ClassCastException
import java.util.concurrent.Executors

/**
 * A simple [Fragment] subclass.
 */
class CIFStep7_4 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivHBLNisaSP: Spinner? = null
    var fivCardRequirdSP: Spinner? = null
    var fivCardTypeSP: Spinner? = null
    var NameOfCardET: EditText? = null
    var fivModeSP: Spinner? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep7_4, container, false)
                (activity as CIFRootActivity).globalClass?.showDialog(activity)

            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(5)
        viewModel.currentFragmentIndex.setValue(3)
        val txt = resources.getString(R.string.account)
        val txt1 = " (4/5 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as CIFRootActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            header()
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            init()
            load()
            /* if (GlobalClass.isDummyDataTrue) {
                 loadDummyData()
             }*/
            setLengthAndTpe()
            setConditions()
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setConditions() {
        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.gender == "F"
            && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK" &&
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
        ) {
            fivHBLNisaSP?.setSelection(1)
        } else if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.gender == "M"
        ) {
            fivHBLNisaSP?.setSelection(0)
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivHBLNisaSP!!)
        } else {
            fivHBLNisaSP?.setSelection(0)
            (activity as CIFRootActivity).globalClass?.setEnabled(fivHBLNisaSP!!, true)
        }

        fivCardRequirdSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (fivCardRequirdSP?.selectedItem.toString() == "No") {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(fivCardTypeSP!!)
                        fivCardTypeSP?.setSelection(0)
                        (activity as CIFRootActivity).globalClass?.setDisbaled(NameOfCardET!!)
                        NameOfCardET?.setText("")
                        fivCardType.mandateSpinner.text = ""
                        NameOfCard.mandateInput.text = ""
                        fivMode.mandateSpinner.text = ""
                    } else if (fivCardRequirdSP?.selectedItem.toString() == "Yes") {
                        (activity as CIFRootActivity).globalClass?.setEnabled(fivCardTypeSP!!, true)
                        (activity as CIFRootActivity).globalClass?.setEnabled(NameOfCardET!!, true)
                        fivCardType.mandateSpinner.text = "*"
                        NameOfCard.mandateInput.text = "*"
                        fivMode.mandateSpinner.text = "*"
                        callCardProducts()
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }


        fivHBLNisaSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                if ((activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCT_SNG_JNT != "02" &&
                    ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
                            || (activity as CIFRootActivity).customerAccounts.ACCT_CCY == "PKR")
                ) {

                    if (fivHBLNisaSP?.selectedItem.toString() == "Yes") {
                        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.gender != "F"
                            && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY != "PK"
                            && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES != "PK"
                        ) {

                            ToastUtils.normalShowToast(
                                context,
                                "For Nisa Your Gender Should be Female or Nationality should be Pakistan",
                                1
                            )
                        } else {
                            callCardProducts()


                        }

                    } else if (fivHBLNisaSP?.selectedItem.toString() == "No") {
                        callCardProducts()
                    }
                }


            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
    }


    fun setItemForCardProducts(item: ArrayList<CardProduct>) {
        try {
            var obj = CardProduct()
            resources.getString(R.string.select).let {
                obj.PRD_CODE = "0"
                obj.PRD_NAME = it
            }
            if (item.size != 0 && !item[0].PRD_NAME.equals(resources.getString(R.string.select))) {
                item.add(0, obj)
            }
            val adapter: ArrayAdapter<CardProduct> = object : ArrayAdapter<CardProduct>(
                activity as CIFRootActivity, R.layout.view_spinner_item, R.id.text1, item
            ) {
                override fun isEnabled(position: Int): Boolean {
                    if ((activity as CIFRootActivity).customerAccounts.PRITY_CODE == "N" && !(activity as CIFRootActivity).customerAccounts.PRITY_CODE.isNullOrEmpty()) {
                        if ((item[position].PRD_CODE == "D9" || item[position].PRD_CODE == "H1")) {
                            return false
                        } else {
                            return true
                        }
                    } else {
                        return true
                    }
                }

                override fun getDropDownView(
                    position: Int, convertView: View?,
                    parent: ViewGroup,
                ): View {
                    val view = super.getDropDownView(position, convertView, parent)
                    val tv = view.findViewById<View>(R.id.text1) as TextView
                    if ((activity as CIFRootActivity).customerAccounts.PRITY_CODE == "N" && !(activity as CIFRootActivity).customerAccounts.PRITY_CODE.isNullOrEmpty()) {
                        if ((item[position].PRD_CODE == "D9" || item[position].PRD_CODE == "H1")) {
                            tv.setTextColor(Color.GRAY)
                            view.setBackgroundColor(resources.getColor(R.color.disabled))
                        }
                    } else {
                        tv.setTextColor(resources.getColor(R.color.green))
                    }
                    return view
                }
            }
            adapter.setDropDownViewResource(R.layout.view_spinner_list)
            fivCardTypeSP?.adapter = adapter
//        adapter.notifyDataSetChanged()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setCardProduct(it: ArrayList<CardProduct>) {
        try {
            setItemForCardProducts(it)
            (activity as CIFRootActivity).cardProductList = it
            (activity as CIFRootActivity).sharedPreferenceManager.lovCardType = it
            (activity as CIFRootActivity).customerAccounts.DEBITCARDTYPEDESC.let { cardtype ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromecardTypeIndex(
                    it,
                    cardtype
                ).let {
                    fivCardTypeSP?.setSelection(it!!)
                }
            }
            (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setLengthAndTpe() {
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            NameOfCardET!!,
            19,
            Constants.INPUT_TYPE_ALPHA_BLOCK
        )

    }

    fun init() {
        fivHBLNisaSP = fivHBLNisa.getSpinner(R.id.HBLNisaID)
        fivCardRequirdSP = fivCardRequird.getSpinner(R.id.fivCardRequirdID)
        fivCardTypeSP = fivCardType.getSpinner(R.id.cardTypeID)
        NameOfCardET = NameOfCard.getTextFromEditText(R.id.nameOFCart)
        fivModeSP = fivMode.getSpinner(R.id.fibModeID)
    }

    fun loadDummyData() {
//        NameOfCardET?.setText("Debit Card")

    }

    fun load() {

        (activity as CIFRootActivity).customerAccounts.NAME_ON_CARD.let { t ->
            NameOfCardET?.setText(t)
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()

        } else {
            setBool((activity as CIFRootActivity).sharedPreferenceManager.lovBool)
        }



        (activity as CIFRootActivity).boolList.let {
//            fivHBLNisa.setItemForfivHBLNisa(it)
////            fivHBLNisaSP?.setSelection(0)
//            if ((activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC == "Individual Account") {
//                if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
//                        0
//                    ).gender == "F"
//                    && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
//                    && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
//                ) {
//                    fivHBLNisaSP?.setSelection(1)
//                } else {
//                    fivHBLNisaSP?.setSelection(0)
//
//
//                }
//            } else if ((activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC == "Joint Account") {
//                if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
//                        0
//                    ).gender == "F"
//                    && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
//                    && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
//                ) {
//                    fivHBLNisaSP?.setSelection(1)
//
//                } else {
//                    fivHBLNisaSP?.setSelection(0)
//
//
//                }
//
//            }
//
//
//            fivCardRequird.setItemForfivfivCardRequird(it)

        }
        if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS.get(0).PHONE_BANK == "1") {
            fivCardRequirdSP?.setSelection(1)
        } else if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS.get(0).PHONE_BANK == "0") {
            ToastUtils.normalShowToast(activity,
                "Debit cards fields are disabled due to phone banking is No, please mask as Yes",1)
            fivCardRequirdSP?.setSelection(0)
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivCardRequirdSP!!)
        }
        //TODO: When customer segment and account type will be Freelancer then all of debit card feilds are disbaled except PKR & USD
        var accountType =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCT_TYPE
        var currency =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.CURRENCY_DESC
        if ((accountType == "7T" || accountType == "7W") && (currency != "PKR" && currency != "USD")) {
            ToastUtils.normalShowToast(activity,
                getString(R.string.currency_account_type_error) + " this $currency currency",
                1)
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivCardRequirdSP!!)
            fivCardRequirdSP?.setSelection(0)
        }
    }

    private fun setBool(it: ArrayList<Bool>) {
        try {
            fivHBLNisa.setItemForfivHBLNisa(it)
            (activity as CIFRootActivity).boolList = it
            (activity as CIFRootActivity).sharedPreferenceManager.lovBool = it
            (activity as CIFRootActivity).customerAccounts.HBL_NISA.let { nisa ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromNisaIndex(
                    it,
                    nisa
                ).let {
                    fivHBLNisaSP?.setSelection(it!!)
                }

            }

            if ((activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC == "Individual Account") {
                if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
                        0
                    ).gender == "F"
                    && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
                    && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
                ) {
                    fivHBLNisaSP?.setSelection(1)
                } else {
                    fivHBLNisaSP?.setSelection(0)


                }
            } else if ((activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCTTYPEDESC == "Joint Account") {
                if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
                        0
                    ).gender == "F"
                    && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
                    && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
                ) {
                    fivHBLNisaSP?.setSelection(1)

                } else {
                    fivHBLNisaSP?.setSelection(0)

                }

            }

            fivCardRequird.setItemForfivfivCardRequird(it)

            (activity as CIFRootActivity).customerAccounts.HBL_NISA.let { nisa ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromNisaReqIndex(
                    it,
                    nisa
                ).let {
                    fivCardRequirdSP?.setSelection(it!!)
                }
            }


            if ((activity as CIFRootActivity).sharedPreferenceManager.lovPrefMode.isNullOrEmpty()) {
                callPrefComMode()

            } else {
                setPrefComMode((activity as CIFRootActivity).sharedPreferenceManager.lovPrefMode)
            }

        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setPrefComMode(it: ArrayList<PrefComMode>) {
        try {
            (activity as CIFRootActivity).prefComModeList = it
            (activity as CIFRootActivity).sharedPreferenceManager.lovPrefMode = it
            fivMode.setItemForPrefComMode(it)
            (activity as CIFRootActivity).customerAccounts.PREFERREDCOMMUMODEDESC.let { premode ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromprefIndex(
                    it,
                    premode
                ).let {
                    fivModeSP?.setSelection(it!!)
                }
            }
            (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }


    private fun callBool() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.BOOL_IDENTIFIER
        HBLHRStore.instance?.getBools(
            RetrofitEnums.URL_HBL,
            lovRequest, object : BoolCallBack {
                override fun BoolSuccess(response: BoolResponse) {
                    response.data?.let {
                        setBool(it)

                    }


                }

                override fun BoolFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }


    fun callCardProducts() {
        try {
            (activity as CIFRootActivity).globalClass?.showDialog(activity)
            var lovRequest = DebitCardLovRequest()
            var condition = null
            var iden: String? = null
            if ((activity as CIFRootActivity).customerAccounts.OPER_INSTRUCTION == "02") {//photo
                iden = "cardproductphoto"
            } else if ((activity as CIFRootActivity).customerAccounts.OPER_INSTRUCTION != "02" &&//self
                (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES != "PK" && //country not pkr
                (activity as CIFRootActivity).customerAccounts.ACCT_CCY == "PKR" //currency pkr
            ) {
                iden = "cardproductphotonrp"
            } else if ((activity as CIFRootActivity).customerAccounts.OPER_INSTRUCTION != "02" && //self
                ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK" ||// residence pkr
                        (activity as CIFRootActivity).customerAccounts.ACCT_CCY != "PKR") //currency not pkr
                && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.gender == "F"// gender female
            ) {
                lovRequest.ACCT_TYPE = (activity as CIFRootActivity).customerAccounts.ACCT_TYPE
                iden = "cardproducthblnisa"
            } else {
                iden = "cardproduct"
                lovRequest.find.PRD_ACTP = (activity as CIFRootActivity).customerAccounts.ACCT_TYPE
            }
            lovRequest.CUST_TYPE =
                (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
            lovRequest.ACCT_CCY =
                (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.CURRENCY
            lovRequest.identifier = iden
            lovRequest.ACCT_TYPE = (activity as CIFRootActivity).customerAccounts.ACCT_TYPE
            var gson = Gson().toJson(lovRequest)
            HBLHRStore.instance?.getCardProducts(
                RetrofitEnums.URL_HBL,
                lovRequest, object : CardProductCallBack {
                    override fun CardProductSuccess(response: CardProductResponse) {
                        response.data?.let {
                            setCardProduct(it)
                        }
                    }

                    override fun CardProductFailure(response: BaseResponse) {
//                        Utils.failedAwokeCalls((activity as CIFRootActivity)) { callCardProducts() }
                        ToastUtils.normalShowToast(activity, response.message, 1)
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                    }
                })
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun callPrefComMode() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.PREF_COM_MODE_IDENTIFIER
        HBLHRStore.instance?.getPrefComMode(
            RetrofitEnums.URL_HBL,
            lovRequest, object : PrefComModeCallBack {
                override fun PrefComModeSuccess(response: PrefComModeResponse) {
                    response.data?.let {
                        setPrefComMode(it)
                    }
//                    fivModeSP?.setSelection(1)

//                    load()

                }

                override fun PrefComModeFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                        Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            (activity as CIFRootActivity).runOnUiThread {
                                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep7_4)
                    findNavController().navigate(R.id.action_CIFStep7_4_to_CIFStep7_3)
        }
    }

    private fun validation(): Boolean {

        if (fivHBLNisaSP?.selectedItem.toString() == "Yes") {
            if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.gender == "F"
                && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.NATIONALITY == "PK"
                && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES == "PK"
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    context,
                    "For Nisa Your Gender Should be Female or Nationality should be Pakistan",
                1)
                return false
            }
        } else if (fivCardRequirdSP?.selectedItem.toString() == "Yes") {
            if (fivCardTypeSP?.selectedItemPosition == 0) {
                ToastUtils.normalShowToast(
                    activity,
                    "Please select card type",
                    1
                )
                return false
            } else if (TextUtils.isEmpty(NameOfCardET?.text.toString())) {
                ToastUtils.normalShowToast(
                    activity,
                    "Please select fill name of card",
                    1
                )
                return false
            }else if (fivModeSP?.selectedItemPosition == 0) {
                ToastUtils.normalShowToast(
                    activity,
                    "Please select communication mode",
                    1
                )
                return false
            } else {
                return true
            }
        }  else {
            return true
        }

    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep7_4)
                    findNavController().navigate(R.id.action_CIFStep7_4_to_CIFStep7_3)
            }
            true
        }
        false
    }

    fun send() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in CIFRootActivity*/
        /*set the values from widgets and another models*/
        saveAndNext()
        callAofCustomerAccount()
        var testModel = Gson().toJson((activity as CIFRootActivity).aofAccountInfoRequest)
    }

    fun callAofCustomerAccount() {
        /*  (activity as CIFRootActivity).globalClass?.showDialog(activity)
          var customerAofAccountInfoRequest: CustomerAofAccountInfoResponse =
              CustomerAofAccountInfoResponse()
          customerAofAccountInfoRequest.identifier = Constants.AOF_SUBMIT_IDENTIFIER
          customerAofAccountInfoRequest.data = "{[]}"
          HBLHRStore.instance?.postAofaccountInfo(
              RetrofitEnums.URL_HBL,
              customerAofAccountInfoRequest, object : CustomerAofAccountInfoCallBack {
                  @SuppressLint("WrongConstant")
                  override fun CustomerAofAccountInfoSuccess(response: CustomerAofAccountInfoResponse) {
                      response.data?.let {
                      };
                      Toast.makeText(
                          activity,
                          response.message,
                          5000
                      ).show()
                      activity?.finish()
                      (activity as CIFRootActivity).globalClass?.hideLoader()
                  }

                  override fun CustomerAofAccountInfoFailure(response: BaseResponse) {
                      ToastUtils.normalShowToast(activity, response.message)
                      (activity as CIFRootActivity).globalClass?.hideLoader()
                  }
              })*/
    }

    fun saveAndNext() {
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivHBLNisaSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerAccounts.HBL_NISA = it!!
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivCardRequirdSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerAccounts.DEBIT_CARD_REQ = it!!
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCardProductCode(
            (activity as CIFRootActivity).cardProductList,
            fivCardTypeSP?.selectedItem.toString()
        ).let {
            if (it != resources.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.DEBIT_CARD_TYPE = it.toString()
                (activity as CIFRootActivity).customerAccounts.DEBITCARDTYPEDESC =
                    fivCardTypeSP?.selectedItem.toString()
            }
        }
        NameOfCardET?.text.toString()
            .let { (activity as CIFRootActivity).customerAccounts.NAME_ON_CARD = it }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPrefComModeCode(
            (activity as CIFRootActivity).prefComModeList,
            fivModeSP?.selectedItem.toString()
        ).let {
            if (it != resources.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.PREFERRED_COMMU_MODE = it.toString()
                (activity as CIFRootActivity).customerAccounts.PREFERREDCOMMUMODEDESC =
                    fivModeSP?.selectedItem.toString()
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount =
            (activity as CIFRootActivity).customerAccounts
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as CIFRootActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as CIFRootActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as CIFRootActivity).aofAccountInfoRequest = it
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                        callCountries()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callCountries() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    response.data?.let {
                        (activity as CIFRootActivity).sharedPreferenceManager.lovCountries = it
                    }
                    callCities()
                }

                override fun CountryFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { callCountries() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    fun callCities() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.CITIES_IDENTIFIER
        HBLHRStore.instance?.getCities(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CityCallBack {
                override fun CitySuccess(response: CityResponse) {
                    response.data?.let {
                        (activity as CIFRootActivity).sharedPreferenceManager.lovCities = it
                        conditionalPageGo()
                        (activity as CIFRootActivity).recyclerViewSetup()
                    }
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

                override fun CityFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { callCountries() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun conditionalPageGo() {
        if (findNavController().currentDestination?.id == R.id.CIFStep7_4) {
            if ((activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7"
                && (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCT_SNG_JNT == "3"
            ) {
                findNavController().navigate(R.id.action_CIFStep7_4_to_CIFStep7_5)
            } else {
                findNavController().navigate(R.id.action_CIFStep7_4_to_CIFStep10)
            }
        }
    }


    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
