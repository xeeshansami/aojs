package com.hbl.bot.ui.activities

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import com.hbl.bot.R
import com.hbl.bot.ui.adapter.CallingAdapter
import com.hbl.bot.ui.adapter.MeetingAdapter
import com.hbl.bot.network.ResponseHandlers.callbacks.OnBoardingCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.OnBoardingRequest
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.OnBoardingResponse
import com.hbl.bot.network.models.response.baseRM.Onboarding
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.*


class HawCallsForMeetingActivity : AppCompatActivity() {

    val REQUEST_PHONE_CALL = 1
    var listview: ListView? = null
    var meetingAdapter: MeetingAdapter? = null
    var callingAdapter: CallingAdapter? = null
    val globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
    override fun onResume() {
        super.onResume()
//        hideNavigationBar()
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
//        hideNavigationBar()
        Thread.setDefaultUncaughtExceptionHandler(LoggingExceptionHandler(globalClass));
        setContentView(R.layout.activity_calls_meeting)
        listview = findViewById(R.id.listView)


        if (GlobalClass.MYMEET == 1) {
            globalClass?.showLoader(this)
            meetAPi(Constants.MYMEET_IDENTIFIER)
            GlobalClass.MYMEET = 0

        } else if (GlobalClass.NEWCUST == 1) {
            globalClass?.showLoader(this)

            callAPi(Constants.NEWCUST_IDENTIFIER)
            GlobalClass.NEWCUST = 0
        }

    }
    fun hideNavigationBar() {
        window.decorView.apply {
            // Hide both the navigation bar and the status bar.
            // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
            // a general rule, you should design your app to hide the status bar whenever you
            // hide the navigation bar.
            systemUiVisibility =
                View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
        }
    }

    fun callAPi(identifier: String) {
        var list = mutableListOf<Onboarding>()
        globalClass.showDialog(this@HawCallsForMeetingActivity)
        var onBoardingRequest = OnBoardingRequest()
        onBoardingRequest.identifier = identifier
        HBLHRStore.instance?.OnBoarding(
            RetrofitEnums.URL_HBL,
            onBoardingRequest,
            object : OnBoardingCallBack {
                override fun OnBoardingSuccess(response: OnBoardingResponse) {
                    globalClass?.hideLoader()
                    if (response.status.equals("98")) {
                        ToastUtils.normalShowToast(
                            this@HawCallsForMeetingActivity,
                            response.message.toString(),2
                        )
                    }
                    callingAdapter =
                        CallingAdapter(
                            this@HawCallsForMeetingActivity,
                            R.layout.row_meeting,
                            response.data
                        )

                    listview!!.adapter = callingAdapter
                    globalClass?.hideLoader()
                }

                override fun OnBoardingFailure(response: BaseResponse) {
                    globalClass?.hideLoader()
                    ToastUtils.normalShowToast(this@HawCallsForMeetingActivity, response.message, 1)
                    globalClass?.hideLoader()
                }

            })

    }

    fun meetAPi(identifier: String) {
        globalClass?.showDialog(this@HawCallsForMeetingActivity)
        var list = mutableListOf<Onboarding>()
        var onBoardingRequest = OnBoardingRequest()
        onBoardingRequest.identifier = identifier
        HBLHRStore.instance?.OnBoarding(
            RetrofitEnums.URL_HBL,
            onBoardingRequest,
            object : OnBoardingCallBack {
                override fun OnBoardingSuccess(response: OnBoardingResponse) {
                    globalClass?.hideLoader()
                    meetingAdapter =
                        MeetingAdapter(
                            this@HawCallsForMeetingActivity,
                            R.layout.row_meeting,
                            response.data
                        )

                    listview!!.adapter = meetingAdapter
                    globalClass?.hideLoader()
                }

                override fun OnBoardingFailure(response: BaseResponse) {
                    globalClass?.hideLoader()
                    ToastUtils.normalShowToast(this@HawCallsForMeetingActivity, response.message, 1)
                    globalClass?.hideLoader()
                }

            })

    }


    @SuppressLint("MissingPermission")
    private fun callStart() {
//        btnCall.setOnClickListener {
//            val Callintent = Intent(Intent.ACTION_CALL)
//            Callintent.data = Uri.parse("tel:" + "03001234567")
//            startActivity(Callintent)
//        }

        fun onRequestPermissionsResult(
            requestcode: Int,
            permission: Array<out String>,
            grantResults: IntArray
        ) {
            if (requestcode == REQUEST_PHONE_CALL) callStart()
        }

    }


}



