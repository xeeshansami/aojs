package com.hbl.bot.ui.fragments.cif


import com.hbl.bot.ui.activities.HawActivity
import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CompoundButton
import android.widget.EditText
import android.widget.Spinner
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.AddressTypeCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.CityCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.CountryCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.GenerateCIFCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.CUSTADDRRESS
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.AddressType
import com.hbl.bot.network.models.response.baseRM.City
import com.hbl.bot.network.models.response.baseRM.Country
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep2_5.*
import kotlinx.android.synthetic.main.fragment_cifstep2_5.btBack
import kotlinx.android.synthetic.main.fragment_cifstep2_5.btNext
import kotlinx.android.synthetic.main.fragment_cifstep2_5.fivAddress1
import kotlinx.android.synthetic.main.fragment_cifstep2_5.fivAddress2
import kotlinx.android.synthetic.main.fragment_cifstep2_5.fivAddress3
import kotlinx.android.synthetic.main.fragment_cifstep2_5.fivCity
import kotlinx.android.synthetic.main.fragment_cifstep2_5.fivOfficeNo
import kotlinx.android.synthetic.main.fragment_cifstep2_5.fivTypeOfAddress
import kotlinx.android.synthetic.main.fragment_cifstep2_5.formSectionHeader
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class HawCifAdd2Fragment10 : Fragment(), View.OnClickListener {
    var rmvspModels: HawRMVSPModels? = null
    var customerAddress: CUSTADDRRESS = CUSTADDRRESS()
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivAddress1ET: EditText? = null
    var fivAddress2ET: EditText? = null
    var fivAddress3ET: EditText? = null
    var fivOfficeNoET: EditText? = null
    var fivPostalCodeET: EditText? = null
    var fivCountrySP: Spinner? = null
    var fivCitySP: Spinner? = null
    var fivTypeOfAddressSP: Spinner? = null
    var count=0
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep2_5, container, false)
            }

        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(4)
        viewModel.currentFragmentIndex.setValue(2)
        val txt = resources.getString(R.string.permanent_address)
        val txt1 = " (3/4 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as HawActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            init()
            setHint()
            setLengthAndType()
            load()
            setData()
//            if (GlobalClass.isDummyDataTrue) {
//                loadDummyData()
//            }
            condition()
            onBackPress(view)
            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }finally {

        }
    }

    fun init() {
        rmvspModels = HawRMVSPModels(activity as HawActivity)
        fivAddress1ET = fivAddress1.getTextFromEditText(R.id.fivAddress11)
        fivAddress2ET = fivAddress2.getTextFromEditText(R.id.fivAddress12)
        fivAddress3ET = fivAddress3.getTextFromEditText(R.id.fivAddress13)
        fivOfficeNoET = fivOfficeNo.getTextFromEditText(R.id.fivOfficeNo25)
        fivPostalCodeET = fivPostalCode2.getTextFromEditText(R.id.fivPostalCode2)
        fivCountrySP = fivCountry2.getSpinner(R.id.customerCountryID2)
        fivCitySP = fivCity.getSpinner(R.id.customerCityID2)
        fivTypeOfAddressSP = fivTypeOfAddress.getSpinner(R.id.typeOfAddressID2)
        fivAddress1ET?.requestFocus()
    }

    private fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
            (activity as HawActivity).globalClass?.setDisbaled(fivAddress1ET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivAddress2ET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivAddress3ET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivOfficeNoET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivPostalCodeET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivCountrySP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivCitySP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(fivTypeOfAddressSP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(saveAsResCheckBox!!, false)
        }
    }

    private fun setData() {
        var _35CharactersOfResidentialAddress = (activity as HawActivity).customerBiometric.responsE_PERMANENT_ADDRESS_ENG
        var commas = _35CharactersOfResidentialAddress.split(",")
        if (commas.isNotEmpty() && commas.size == 3) {
            var address1 = commas[0]
            var address2 = commas[1]
            var address3 = commas[2]
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1.isNullOrEmpty()) {
                fivAddress1ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1)
            } else {
                fivAddress1ET?.setText(address1)
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2.isNullOrEmpty()) {
                fivAddress2ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2)
            } else {
                fivAddress2ET?.setText(address2)
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3.isNullOrEmpty()) {
                fivAddress3ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3)
            } else {
                fivAddress3ET?.setText(address3)
            }
        } else if (commas.isNotEmpty() && commas.size > 3) {
            var address1=commas[0]
            var address2=commas[1]
            var address3=""
            for(x in 0 until commas.size){
                if(x!=0||x!=1){
                    address3 += " "+commas[x]
                }
            }
            if(address3.length>=35){
                address2=""
                address3=""
                for(x in 0 until commas.size){
                    if(x==1 || x==2){
                        address2 += commas[x]
                    }else if(x>=3){
                        address3+=commas[x]
                    }
                }
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1.isNullOrEmpty()) {
                fivAddress1ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1)
            } else {
                fivAddress1ET?.setText(address1)
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2.isNullOrEmpty()) {
                fivAddress2ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2)
            } else {
                fivAddress2ET?.setText(address2)
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3.isNullOrEmpty()) {
                fivAddress3ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3)
            } else {
                fivAddress3ET?.setText(address3)
            }
        } else if (commas.isNotEmpty() && commas.size == 2) {
            var address1 = commas[0]
            var address2 = commas[1].substring(commas[1].lastIndexOf(" ")+1)
            var address3 = commas[1].substring(commas[1].lastIndexOf(" ")+2)
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1.isNullOrEmpty()) {
                fivAddress1ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1)
            } else {
                fivAddress1ET?.setText(address1)
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2.isNullOrEmpty()) {
                fivAddress2ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2)
            } else {
                fivAddress2ET?.setText(address2)
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3.isNullOrEmpty()) {
                fivAddress3ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3)
            } else {
                fivAddress3ET?.setText(address3)
            }
        } else if (commas.isNotEmpty() && commas.size == 1) {
            val st = commas[0].split(" ")
            var address1 = ""
            var address2 = ""
            var address3 = ""
            if(st.size>5) {
                for (x in st.indices) {
                    if (x == 0 || x == 1 ||x==2) {
                        address1 +=" "+ st[x]
                    } else if (x == 3 || x == 4 || x==5) {
                        address2 +=" "+ st[x]
                    } else {
                        address3 += " " + st[x]
                    }
                }
            }else{
                for (x in st.indices) {
                    if (x == 0 ) {
                        address1 += st[x]
                    } else if (x == 1 || x == 2) {
                        address2 +=" "+ st[x]
                    } else {
                        address3 += " " + st[x]
                    }
                }
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1.isNullOrEmpty()) {
                fivAddress1ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1)
            } else {
                fivAddress1ET?.setText(address1)
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2.isNullOrEmpty()) {
                fivAddress2ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2)
            } else {
                fivAddress2ET?.setText(address2)
            }
            if (!(activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3.isNullOrEmpty()) {
                fivAddress3ET?.setText((activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3)
            } else {
                fivAddress3ET?.setText(address3)
            }
        }

        (activity as HawActivity).customerAddressList.getOrNull(1)?.NEAREST_LANDMARK
            .let {
                fivOfficeNoET?.setText(it)
            }
        (activity as HawActivity).customerAddressList.getOrNull(1)?.POSTCODE
            .let {
                fivPostalCodeET?.setText (it)
            }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            (activity as HawActivity).sharedPreferenceManager.lovCountries,
            "Pakistan"
        ).let {
            if(it!=0) {
                (activity as HawActivity).globalClass?.setDisbaled(fivCountrySP!!, false)
                fivCountrySP?.setSelection(it!!)
            }
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCityIndex(
            (activity as HawActivity).sharedPreferenceManager.lovCities,
            (activity as HawActivity).customerAddressList.getOrNull(1)?.CITY_NAME
        ).let {
            fivCitySP?.setSelection(it!!)
        }
    }

    fun getAddress(characters: String, startIndex: Int, lastIndex: Int): String {
        var split=characters
        if(characters.length in startIndex..lastIndex) {
            split = characters.substring(startIndex, lastIndex)
        }
        return split
    }


    fun setHint() {
        fivAddress1ET?.setHint(resources!!.getString(R.string.addressLine1_hint_str))
        fivAddress2ET?.setHint(resources!!.getString(R.string.addressLine2_hint_str))
        fivAddress3ET?.setHint(resources!!.getString(R.string.addressLine3_hint_str))
    }

    fun setLengthAndType() {
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivAddress1ET!!,
            35,
            Constants.INPUT_TYPE_CUSTOM_ADDRESS_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivAddress2ET!!,
            35,
            Constants.INPUT_TYPE_CUSTOM_ADDRESS_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivAddress3ET!!,
            35,
            Constants.INPUT_TYPE_CUSTOM_ADDRESS_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivOfficeNoET!!,
            35,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as HawActivity).globalClass?.edittextTypeCount(
            fivPostalCodeET!!,
            15,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
    }

    fun loadDummyData() {
        fivAddress1ET?.setText("Aleem Paradise FlatAb#108")
        fivAddress2ET?.setText("House#B-159, Muraqba Hall Society")
        fivAddress3ET?.setText("Karachi Pakistan")
        fivOfficeNoET?.setText("03453164365")
        fivPostalCodeET?.setText("75850")
    }

    fun address3Error(): Boolean {
        if (fivAddress3ET?.text.toString().length > 35) {
            return false
        } else {
            return true
        }
    }

    fun condition() {
        saveAsResCheckBox.setOnCheckedChangeListener { compoundButton: CompoundButton, check: Boolean ->
            try {
                if (check) {
                    (activity as HawActivity).customerAddressList.getOrNull(0)?.ADDRESS_LINE1
                        .let { fivAddress1ET?.setText(it) }
                    (activity as HawActivity).customerAddressList.getOrNull(0)?.ADDRESS_LINE2
                        .let { fivAddress2ET?.setText(it) }
                    (activity as HawActivity).customerAddressList.getOrNull(0)?.ADDRESS_LINE3
                        .let { fivAddress3ET?.setText(it) }
                    (activity as HawActivity).customerAddressList.getOrNull(0)?.NEAREST_LANDMARK
                        .let { fivOfficeNoET?.setText(it) }
                    (activity as HawActivity).customerAddressList.getOrNull(0)?.POSTCODE
                        .let { fivPostalCodeET?.setText(it) }
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                        (activity as HawActivity).sharedPreferenceManager.lovCountries,
                        "Pakistan"
                    ).let {
                        if(it!=0) {
                            (activity as HawActivity).globalClass?.setDisbaled(fivCountrySP!!, false)
                            fivCountrySP?.setSelection(it!!)
                        }
                    }
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromCityIndex(
                        (activity as HawActivity).sharedPreferenceManager.lovCities,
                        (activity as HawActivity).customerAddressList.getOrNull(0)?.CITY_NAME
                    ).let {
                        fivCitySP?.setSelection(it!!)
                    }
                } else {
                    if ((activity as HawActivity).customerAddressList.getOrNull(1) != null) {
                        (activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1
                            .let { fivAddress1ET?.setText(it) }
                        (activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2
                            .let { fivAddress2ET?.setText(it) }
                        (activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3
                            .let { fivAddress3ET?.setText(it) }
                        (activity as HawActivity).customerAddressList.getOrNull(1)?.NEAREST_LANDMARK
                            .let { fivOfficeNoET?.setText(it) }
                        (activity as HawActivity).customerAddressList.getOrNull(1)?.POSTCODE
                            .let { fivPostalCodeET?.setText(it) }
                        (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                            (activity as HawActivity).sharedPreferenceManager.lovCountries,
                            "Pakistan"
                        ).let {
                            if(it!=0) {
                                (activity as HawActivity).globalClass?.setDisbaled(fivCountrySP!!, false)
                                fivCountrySP?.setSelection(it!!)
                            }
                        }
                        (activity as HawActivity).globalClass?.findSpinnerPositionFromCityIndex(
                            (activity as HawActivity).sharedPreferenceManager.lovCities,
                            (activity as HawActivity).customerAddressList.getOrNull(1)?.CITY_NAME
                        ).let {
                            fivCitySP?.setSelection(it!!)
                        }
                    } else {
                        fivAddress1ET?.setText("")
                        fivAddress2ET?.setText("")
                        fivAddress3ET?.setText("")
                        fivOfficeNoET?.setText("")
                        fivPostalCodeET?.setText("")
//                        fivCountrySP?.setSelection(0)
                        fivCitySP?.setSelection(0)
                    }
                }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
            } catch (e: Exception) {
                ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                SendEmail.sendEmail((activity as HawActivity), (e as Exception))
            } finally {

            }
        }

    }

    fun load() {
        count=0
        (activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE1
            .let {
                if (!it.isNullOrEmpty()) {
                    count++
                }
                fivAddress1ET?.setText(it) }
        (activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE2
            .let {
                if (!it.isNullOrEmpty()) {
                    count++
                }
                fivAddress2ET?.setText(it) }
        (activity as HawActivity).customerAddressList.getOrNull(1)?.ADDRESS_LINE3
            .let {
                if (!it.isNullOrEmpty()) {
                    count++
                }
                fivAddress3ET?.setText(it) }
        (activity as HawActivity).customerAddressList.getOrNull(1)?.NEAREST_LANDMARK
            .let { fivOfficeNoET?.setText(it) }
        (activity as HawActivity).customerAddressList.getOrNull(1)?.POSTCODE
            .let { fivPostalCodeET?.setText(it) }
        if ((activity as HawActivity).sharedPreferenceManager.lovCountries.isNullOrEmpty()) {
            callAllCountries()
        } else {
            setCountries((activity as HawActivity).sharedPreferenceManager.lovCountries)
        }
    }

    fun callCities() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.CITIES_IDENTIFIER
        HBLHRStore.instance?.getCities(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CityCallBack {
                override fun CitySuccess(response: CityResponse) {
                    response.data?.let {
                        setCities(it)
                    };
                }

                override fun CityFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun setCities(it: ArrayList<City>) {
        try {
            (activity as HawActivity).cityList = it
            fivCity.setItemForCities(it)
            (activity as HawActivity).sharedPreferenceManager.lovCities = it
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCityIndex(
                it,
                (activity as HawActivity).customerAddressList.getOrNull(1)?.CITY_NAME
            ).let {
                if(!(activity as HawActivity).customerAddressList.getOrNull(1)?.CITY_NAME.isNullOrEmpty()){
                    count++
                }
                fivCitySP?.setSelection(it!!)
            }
            fivCity.remainSelection(it.size)
            if ((activity as HawActivity).sharedPreferenceManager.lovAddressType.isNullOrEmpty()) {
                callAddressType()
            } else {
                setAddressType((activity as HawActivity).sharedPreferenceManager.lovAddressType)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }finally {

        }
    }

    fun callAllCountries() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(lovRequest)
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    response.data?.let {
                        setCountries(it)
                    };
                }

                override fun CountryFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    fun setCountries(it: ArrayList<Country>) {
        try {
            (activity as HawActivity).countryList = it
            (activity as HawActivity).sharedPreferenceManager.lovCountries = it
            fivCountry2.setItemForfivCountry(it)
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                "Pakistan"
            ).let {
                if(!(activity as HawActivity).customerAddressList.getOrNull(1)?.COUNTRY_NAME.isNullOrEmpty()){
                    count++
                }
                if(it!=0) {
                    (activity as HawActivity).globalClass?.setDisbaled(fivCountrySP!!, false)
                    fivCountrySP?.setSelection(it!!)
                }
            }
            fivCountry2.remainSelection(it.size)
            if ((activity as HawActivity).sharedPreferenceManager.lovCities.isNullOrEmpty()) {
                callCities()
            } else {
                setCities((activity as HawActivity).sharedPreferenceManager.lovCities)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }finally {

        }
    }

    private fun callAddressType() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.ADDRESS_TYPE_IDENTIFIER
        HBLHRStore.instance?.getAddressType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : AddressTypeCallBack {
                override fun AddressTypeSuccess(response: AddressTypeResponse) {
                    response.data?.let {
                        setAddressType(it)
                    }
                }

                override fun AddressTypeFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun setAddressType(it: ArrayList<AddressType>) {
        try {
            (activity as HawActivity).typeOfAddressList = it
            (activity as HawActivity).sharedPreferenceManager.lovAddressType = it
            fivTypeOfAddress.setItemForAddType(it)
            (activity as HawActivity).globalClass?.findSpinnerPositionFromAddressTypeIndex(
                it,
                (activity as HawActivity).customerAddressList.getOrNull(1)?.TYPE_OF_ADDRESS_DESC
            ).let {
                if(!(activity as HawActivity).customerAddressList.getOrNull(1)?.TYPE_OF_ADDRESS_DESC.isNullOrEmpty()){
                    count++
                }
                fivTypeOfAddressSP?.setSelection(it!!)
            }
            if(count==5){
                saveAsResCheckBox.isChecked=true
            }
            fivTypeOfAddress.remainSelection(it.size)
            (activity as HawActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).globalClass?.setDisbaled(btNext, false)
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            activity?.runOnUiThread {
                                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })

                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep2_5)
                    findNavController().navigate(R.id.action_CIFStep2_5_to_CIFStep2_3)
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep2_5)
                    findNavController().navigate(R.id.action_CIFStep2_5_to_CIFStep2_3)
                true
            }
            false
        }
    }

    fun saveAndNext() {
        try {
            customerAddress.ADDRESS_TYPE = "PERMANENT"
            fivAddress1ET?.text.toString()
                .let { customerAddress.ADDRESS_LINE1 = it }
            fivAddress2ET?.text.toString()
                .let { customerAddress.ADDRESS_LINE2 = it }
            fivAddress3ET?.text.toString()
                .let { customerAddress.ADDRESS_LINE3 = it }
            fivPostalCodeET?.text.toString()
                .let { customerAddress.POSTCODE = it }
            fivOfficeNoET?.text.toString()
                .let { customerAddress.NEAREST_LANDMARK = it }
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCode(
                (activity as HawActivity).sharedPreferenceManager.lovCountries,
                fivCountrySP?.selectedItem.toString()
            ).let {
                if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    customerAddress.COUNTRY = it.toString()
                    customerAddress.COUNTRY_NAME =
                        fivCountrySP?.selectedItem.toString()
                }
            }
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCityCode(
                (activity as HawActivity).sharedPreferenceManager.lovCities,
                fivCitySP?.selectedItem.toString()
            ).let {
                if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    customerAddress.CITY = it.toString()
                    customerAddress.CITY_NAME =
                        fivCitySP?.selectedItem.toString()
                }
            }
            /* (activity as HawActivity).globalClass?.findSpinnerPositionFromAddressTypeCode((activity as HawActivity).typeOfAddressList, fivTypeOfAddressSP?.selectedItem.toString()
         ).let {
             customerAddress.TYPE_OF_ADDRESS_CODE = it!!
             customerAddress.TYPE_OF_ADDRESS_DESC =
                 fivTypeOfAddressSP?.selectedItem.toString()
         }*/
            if ((activity as HawActivity).customerAddressList.size == 1) {
                (activity as HawActivity).customerAddressList.add(1, customerAddress)
            } else if ((activity as HawActivity).customerAddressList.size >= 2) {
                (activity as HawActivity).customerAddressList.set(1, customerAddress)
            }
            if (!isFATCAPageOpenCheck()) {
                rmvspModels?.rmvFATCA()
            }
            (activity as HawActivity).sharedPreferenceManager.setCustomerAddress((activity as HawActivity).customerAddressList)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        }finally {

        }
    }

    fun validation(): Boolean {
        var address1 = fivAddress1ET?.text.toString().trim()
        var address2 = fivAddress2ET?.text.toString().trim()
        var address3 = fivAddress3ET?.text.toString().trim()
        if (address1.length == 0) {
            fivAddress1ET?.setError(resources!!.getString(R.string.please_enter_address1))
            fivAddress1ET?.requestFocus()
            return false
        } else if (address2.length == 0) {
            fivAddress2ET?.setError(resources!!.getString(R.string.please_enter_address2))
            fivAddress2ET?.requestFocus()
            return false
        } else if (address3.length == 0) {
            fivAddress3ET?.setError(resources!!.getString(R.string.please_enter_address3))
            fivAddress3ET?.requestFocus()
            return false
        } else if (fivCountrySP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_select_country),
                1
            )
            return false
        } else if (fivCitySP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_select_city),
                1
            )
            return false
        } else if (!address3Error()) {
            fivAddress3ET?.setError(resources!!.getString(R.string.pleaseEnter35Address3CharactersErr))
            fivAddress3ET?.requestFocus()
            return false
        } else {
            return true
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as HawActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "9"
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as HawActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as HawActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as HawActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as HawActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as HawActivity).aofAccountInfoRequest = it
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as HawActivity).globalClass?.hideLoader()
                        isCustSegHouseWifeOrOther()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }finally {

                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun isCustSegHouseWifeOrOther() {
        var custSegType =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        if (custSegType == "A0" || custSegType == "A2" || custSegType == "A4") {
            var customerAddress: CUSTADDRRESS = CUSTADDRRESS()
            customerAddress.ADDRESS_LINE1 = "NA"
            customerAddress.ADDRESS_LINE2 = "NA"
            customerAddress.ADDRESS_LINE3 = "NA"
            customerAddress.CITY = "0021"
            customerAddress.CITY_NAME = "KARACHI"
            customerAddress.COUNTRY = "PK"
            customerAddress.COUNTRY_NAME = "PAKISTAN"
            if ((activity as HawActivity).customerAddressList.size == 2) {
                (activity as HawActivity).customerAddressList.add(2, customerAddress)
            } else if ((activity as HawActivity).customerAddressList.size >= 2) {
                (activity as HawActivity).customerAddressList.set(2, customerAddress)
            }
            (activity as HawActivity).sharedPreferenceManager.setCustomerAddress((activity as HawActivity).customerAddressList)
            isFATCAPageOpen()
        } else {
            if (findNavController().currentDestination?.id == R.id.CIFStep2_5)
                findNavController().navigate(R.id.action_CIFStep2_5_to_CIFStep2_7)
        }
        (activity as HawActivity).recyclerViewSetup()
        (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
    }

    fun isFATCAPageOpen() {
        if (isFATCAPageOpenCheck()) {
            if (findNavController().currentDestination?.id == R.id.CIFStep2_5)
                findNavController().navigate(R.id.action_goto_FATCA)
        } else /*if (isCRSPageOpenCheck()) */ {
            if (findNavController().currentDestination?.id == R.id.CIFStep2_5)
                findNavController().navigate(R.id.action_goto_CRS)
        }/* else {
            findNavController().navigate(R.id.action_goto_CDD)
        }*/
        (activity as HawActivity).recyclerViewSetup()
    }

    fun isFATCAPageOpenCheck(): Boolean {
        val nationalityCountry =
            (activity as HawActivity).sharedPreferenceManager.customerInfo.NATIONALITY
        val residentOfCountry =
            (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES
        val birthOfCountry =
            (activity as HawActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_BIRTH
        val secondNationalityCountry =
            (activity as HawActivity).sharedPreferenceManager.customerInfo.SECOND_NATIONALITY
        val officeAddressCountry =
            (activity as HawActivity).sharedPreferenceManager.customerAddress.getOrNull(0)?.COUNTRY
        val currentAddressCountry =
            (activity as HawActivity).sharedPreferenceManager.customerAddress.getOrNull(1)?.COUNTRY
        val permanentAddressCountry =
            (activity as HawActivity).sharedPreferenceManager.customerAddress.getOrNull(2)?.COUNTRY
        val residenceNumber = (activity as HawActivity).residenceNumberFATCA
        val officeNumber = (activity as HawActivity).officeNumberFATCA
        val checkedUSDialCode = (activity as HawActivity).isCheckedUSDialCode
        return if (nationalityCountry == "US" ||
            residentOfCountry == "US" ||
            birthOfCountry == "US" ||
            secondNationalityCountry == "US" ||
            officeAddressCountry == "US" ||
            currentAddressCountry == "US" ||
            permanentAddressCountry == "US" ||
            residenceNumber == 1 ||
            officeNumber == 1 ||
            checkedUSDialCode == 1
        ) {
            (activity as HawActivity).customerInfo.FATCA = 1
            (this.context as HawActivity).sharedPreferenceManager.customerInfo =
                (this.context as HawActivity).customerInfo
            true
        } else {
            (activity as HawActivity).customerInfo.FATCA = 0
            (this.context as HawActivity).sharedPreferenceManager.customerInfo =
                (this.context as HawActivity).customerInfo
            false
        }
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
