package com.hbl.bot.ui.fragments.spark.accounts


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.AccountOperationsInst
import com.hbl.bot.network.models.response.baseRM.Bool
import com.hbl.bot.network.models.response.baseRM.EstateFrequency
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep7_3.*
import kotlinx.android.synthetic.main.fragment_cifstep7_3.btBack
import kotlinx.android.synthetic.main.fragment_cifstep7_3.btNext
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import kotlinx.android.synthetic.main.view_form_selection.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException
import java.util.concurrent.Executors

/**
 * A simple [Fragment] subclass.
 */
class CIFStep7_3 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivNetBankSP: Spinner? = null
    var fivZakatSP: Spinner? = null
    var fivInsuranceSP: Spinner? = null
    var fivStatmentSP: Spinner? = null
    var fivStatmentFrequencySP: Spinner? = null
    var fivOperationInstructionSP: Spinner? = null
    var firstAttempt=false
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep7_3, container, false)
                (activity as CIFRootActivity).globalClass?.showDialog(activity)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }


    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(5)
        viewModel.currentFragmentIndex.setValue(2)
        val txt = resources.getString(R.string.account)
        val txt1 = " (3/5 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    private fun setBool(it: ArrayList<Bool>) {
        try {
            (activity as CIFRootActivity).boolList = it
            (activity as CIFRootActivity).sharedPreferenceManager.lovBool = it
            fivInsurance.setItemForfivInsurance(it)
            fivStatment.setItemForfivStatment(it)
            fivNetBank.setItemForfivNetBank(it)
            fivZakat.setItemForfivZakat(it)
//            (activity as CIFRootActivity).globalClass?.setDisbaled(fivInsuranceSP!!,false)
            (activity as CIFRootActivity).customerAccounts.ESTATEMENT.let { eFreq ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromestateIndex(
                    it,
                    eFreq
                ).let {
                    fivStatmentSP?.setSelection(it!!)
                }
            }

            (activity as CIFRootActivity).customerAccounts.FREE_INSURANCE.let { free ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromfreeINsIndex(
                    it,
                    free
                ).let {
                        fivInsuranceSP?.setSelection(it!!)
                    }
                }


        (activity as CIFRootActivity).customerAccounts.INTERNET_BANK.let { netBank ->
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromInternetIndex(
                it,
                netBank
            ).let {
                fivNetBankSP?.setSelection(it!!)
            }

        }


        (activity as CIFRootActivity).customerAccounts.ZAKAT_EXEMPT.let { zakat ->
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromZakatIndex(
                it,
                zakat
            ).let {
                fivZakatSP?.setSelection(it!!)
            }
        }



        if ((activity as CIFRootActivity).customerAccounts.ACCT_TYPE == "GM") {
            fivInsuranceSP?.setSelection(1)

        } else {
            fivInsuranceSP?.setSelection(0)

        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "1") {
            fivNetBankSP?.setSelection(1)
        } else {
            fivNetBankSP?.setSelection(0)

        }


//            if ((activity as CIFRootActivity).sharedPreferenceManager.lovOperatioanlInst.isNullOrEmpty()) {
                callOperationalInst()
//            } else {
//                setOperationalInst((activity as CIFRootActivity).sharedPreferenceManager.lovOperatioanlInst)
//            }
    } catch (e: UnsatisfiedLinkError)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
    } catch (e: NullPointerException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
    } catch (e: IllegalArgumentException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
    } catch (e: NumberFormatException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
    } catch (e: InterruptedException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
    } catch (e: RuntimeException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
    } catch (e: IOException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
    } catch (e: FileNotFoundException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
    } catch (e: ClassNotFoundException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
    } catch (e: ActivityNotFoundException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
    } catch (e: IndexOutOfBoundsException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
    } catch (e: ArrayIndexOutOfBoundsException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
    } catch (e: ClassCastException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
    } catch (e: TypeCastException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
    } catch (e: SecurityException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
    } catch (e: IllegalStateException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
    } catch (e: OutOfMemoryError)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
    } catch (e: RuntimeException)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
    } catch (e: Exception)
    {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
    }

}

private fun setOperationalInst(it: ArrayList<AccountOperationsInst>) {
    try {
        (activity as CIFRootActivity).accountOperationInstList = it
        fivOperationInstruction.setItemForAccountOperationsInst(it)
        (activity as CIFRootActivity).sharedPreferenceManager.lovOperatioanlInst = it

        (activity as CIFRootActivity).customerAccounts.OPERINSTDESC.let { OInst ->
            (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromOperInstIndex(
                it,
                OInst
            ).let {
                fivOperationInstructionSP?.setSelection(it!!)
            }
        }
        (activity as CIFRootActivity).globalClass?.hideLoader()
    } catch (e: UnsatisfiedLinkError) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
    } catch (e: NullPointerException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
    } catch (e: IllegalArgumentException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
    } catch (e: NumberFormatException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
    } catch (e: InterruptedException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
    } catch (e: RuntimeException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
    } catch (e: IOException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
    } catch (e: FileNotFoundException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
    } catch (e: ClassNotFoundException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
    } catch (e: ActivityNotFoundException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
    } catch (e: IndexOutOfBoundsException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
    } catch (e: ArrayIndexOutOfBoundsException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
    } catch (e: ClassCastException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
    } catch (e: TypeCastException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
    } catch (e: SecurityException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
    } catch (e: IllegalStateException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
    } catch (e: OutOfMemoryError) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
    } catch (e: RuntimeException) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
    } catch (e: Exception) {
        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
    }

}

private fun callBool() {
    (activity as CIFRootActivity).globalClass?.showDialog(activity)
    var lovRequest = LovRequest()
    lovRequest.identifier = Constants.BOOL_IDENTIFIER
    HBLHRStore.instance?.getBools(
        RetrofitEnums.URL_HBL,
        lovRequest, object : BoolCallBack {
            override fun BoolSuccess(response: BoolResponse) {
                response.data?.let {
                    setBool(it)

                };


            }

                override fun BoolFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun callOperationalInst() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.ACCOUNT_OPERATIONS_INST_IDENTIFIER
        lovRequest.condition = true
        lovRequest.conditionname = "ACCT_OPER_TYPE"
        lovRequest.conditionvalue =
            (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCT_OPER_TYPE_CODE.toString()
        Gson().toJson(lovRequest)
        HBLHRStore.instance?.getAccountOperationsInst(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : AccountOperationsInstCallBack {
                override fun AccountOperationsInstSuccess(response: AccountOperationsInstResponse) {
                    response.data?.let {
                        setOperationalInst(it)
                    };
                }

                override fun AccountOperationsInstFailure(response: BaseResponse) {
//                        Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

        })

}

override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
    try {
        super.onViewCreated(view, savedInstanceState)
        LocalBroadcastManager.getInstance(activity as CIFRootActivity).registerReceiver(
            mStatusCodeResponse, IntentFilter(
                Constants.STATUS_BROADCAST
            )
        );
        header()
        (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
        btNext.setOnClickListener(this)
        btBack.setOnClickListener(this)
        init()
        load()
        setConditions()
//        if (GlobalClass.isDummyDataTrue) {
//            loadDummyData()
//        }
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

private fun setConditions() {
    fivInsuranceSP?.setOnItemSelectedListener(object :
        AdapterView.OnItemSelectedListener {
        override fun onItemSelected(
            adapterView: AdapterView<*>?,
            view: View?,
            pos: Int,
            l: Long,
        ) {
            try {
                if (pos == 0 && (activity as CIFRootActivity).customerAccounts.ACCT_TYPE == "7L") {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.error_nisa_fee_statment),
                        2
                    )
                    fivInsuranceSP?.setSelection(1)
                }
            } catch (e: UnsatisfiedLinkError) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
            } catch (e: NullPointerException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
            } catch (e: IllegalArgumentException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail(
                    (activity as CIFRootActivity),
                    (e as IllegalArgumentException)
                )
            } catch (e: NumberFormatException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
            } catch (e: InterruptedException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: IOException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
            } catch (e: FileNotFoundException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
            } catch (e: ClassNotFoundException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail(
                    (activity as CIFRootActivity),
                    (e as ClassNotFoundException)
                )
            } catch (e: ActivityNotFoundException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail(
                    (activity as CIFRootActivity),
                    (e as ActivityNotFoundException)
                )
            } catch (e: IndexOutOfBoundsException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail(
                    (activity as CIFRootActivity),
                    (e as IndexOutOfBoundsException)
                )
            } catch (e: ArrayIndexOutOfBoundsException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail(
                    (activity as CIFRootActivity),
                    (e as ArrayIndexOutOfBoundsException)
                )
            } catch (e: ClassCastException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
            } catch (e: TypeCastException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
            } catch (e: SecurityException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
            } catch (e: IllegalStateException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
            } catch (e: OutOfMemoryError) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
            } catch (e: RuntimeException) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
            } catch (e: java.lang.Exception) {
                ToastUtils.normalShowToast(
                    activity,
                    getString(R.string.something_went_wrong),
                    3
                )
                SendEmail.sendEmail((activity as CIFRootActivity), (e as java.lang.Exception))
            }
        }

        override fun onNothingSelected(adapterView: AdapterView<*>?) {
        }
    })
    fivStatmentSP?.onItemSelectedListener = object :
        AdapterView.OnItemSelectedListener {
        @SuppressLint("Range")

        override fun onItemSelected(
            adapterView: AdapterView<*>?,
            view: View?,
            pos: Int,
            l: Long,
        ) {
            try {
                if (fivStatmentSP?.selectedItem.toString() == "No") {
                    (activity as CIFRootActivity).globalClass?.setDisbaled(
                        fivStatmentFrequencySP!!,false
                    )
                    fivStatmentFrequencySP?.setSelection(0)
                    fivStatmentFrequency.mandateSpinner.text = ""
                } else if (fivStatmentSP?.selectedItem.toString() == "Yes") {
                    (activity as CIFRootActivity).globalClass?.setEnabled(
                        fivStatmentFrequencySP!!,
                        true
                    )
                    fivStatmentFrequency.mandateSpinner.text = "*"
                    if ((activity as CIFRootActivity).sharedPreferenceManager.lovEstateFrequency.isNullOrEmpty()) {
                        callEstateFrequency()

                        } else {
                            setEstateFreQuency((activity as CIFRootActivity).sharedPreferenceManager.lovEstateFrequency)
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

        override fun onNothingSelected(adapterView: AdapterView<*>?) {
        }
    }



        fivNetBankSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {

            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (fivNetBankSP?.selectedItem.toString().equals("No", true)) {
                        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "1") {
                            ToastUtils.normalShowToast(context,
                                "Internet banking should be YES as per CIF details",
                                1)
                            fivNetBankSP?.setSelection(1)
                        }
                    } else if (fivNetBankSP?.selectedItem.toString().equals("Yes", true)) {
                        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "0") {
                            ToastUtils.normalShowToast(context,
                                "Internet banking should be NO as per CIF details",
                                1)
                            fivNetBankSP?.setSelection(0)
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }


    }

    private fun setEstateFreQuency(it: ArrayList<EstateFrequency>) {
        try {
            (activity as CIFRootActivity).estateFrequencyList = it
            fivStatmentFrequency.setItemForEstateFrequency(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovEstateFrequency = it


            (activity as CIFRootActivity).customerAccounts.ESTATEMENTFREQDESC.let { esfreq ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromEstateFreqIndex(
                    it,
                    esfreq
                ).let {
                    fivStatmentFrequencySP?.setSelection(it!!)
                }

            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }

    }

    fun init() {
        fivNetBankSP = fivNetBank.getSpinner(R.id.netBankID)
        fivZakatSP = fivZakat.getSpinner(R.id.zakatID)
        fivInsuranceSP = fivInsurance.getSpinner(R.id.insuranceID)
        fivStatmentSP = fivStatment.getSpinner(R.id.statementID)
        fivStatmentFrequencySP = fivStatmentFrequency.getSpinner(R.id.statementFrequencyID)
        fivOperationInstructionSP = fivOperationInstruction.getSpinner(R.id.operationInstructionID)
//        fivZakatSP?.setSelection(0)
//        fivStatmentSP?.setSelection(0)
//        (activity as CIFRootActivity).globalClass?.setDisbaled(fivStatmentFrequencySP!!)
}

    fun loadDummyData() {
//        fivOperationInstructionSP?.setSelection(1)

    }

    fun load() {
        if ((activity as CIFRootActivity).sharedPreferenceManager.lovBool.isNullOrEmpty()) {
            callBool()

        } else {
            setBool((activity as CIFRootActivity).sharedPreferenceManager.lovBool)
        }
    }

    fun callEstateFrequency() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.ESTATE_FREQUENCY_IDENTIFIER
        HBLHRStore.instance?.getEstateFrequency(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : EstateFrequencyCallBack {
                override fun EstateFrequencySuccess(response: EstateFrequencyResponse) {
                    response.data?.let {
                        setEstateFreQuency(it)
                    };
//                    load()

                }

                override fun EstateFrequencyFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) {   callEstateFrequency() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                        Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            (activity as CIFRootActivity).runOnUiThread {
                                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                            }
                        })

                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.btBack ->
                if (findNavController().currentDestination?.id == R.id.CIFStep7_3)
                    findNavController().navigate(R.id.action_CIFStep7_3_to_CIFStep7_2)
        }

    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep7_3)
                    findNavController().navigate(R.id.action_CIFStep7_3_to_CIFStep7_2)
            }
            true
        }
        false
    }

    fun saveAndNext() {
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivNetBankSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerAccounts.INTERNET_BANK = it!!
        }



        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivZakatSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerAccounts.ZAKAT_EXEMPT = it!!
        }



        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivInsuranceSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerAccounts.FREE_INSURANCE = it!!
        }



        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromBoolCode(
            (activity as CIFRootActivity).boolList,
            fivStatmentSP?.selectedItem.toString()
        ).let {
            (activity as CIFRootActivity).customerAccounts.ESTATEMENT = it!!
        }


        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromEstateFrequencyCode(
            (activity as CIFRootActivity).estateFrequencyList,
            fivStatmentFrequencySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.ESTATEMENT_FREQ = it.toString()
                (activity as CIFRootActivity).customerAccounts.ESTATEMENTFREQDESC =
                    fivStatmentFrequencySP?.selectedItem.toString()
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromAccountOperationInstCode(
            (activity as CIFRootActivity).accountOperationInstList,
            fivOperationInstructionSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.OPER_INSTRUCTION = it.toString()
                (activity as CIFRootActivity).customerAccounts.OPERINSTDESC =
                    fivOperationInstructionSP?.selectedItem.toString()
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount =
            (activity as CIFRootActivity).customerAccounts
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        aofDataAlign((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo)
    }

    fun aofDataAlign(aofAccountInfo: Data) {
        aofAccountInfo.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                aofAccountInfo.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let { aofAccountInfo.TRACKING_ID = it.toString() }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            aofAccountInfo.ETBNTBFLAG = (activity as CIFRootActivity).etbntbFLAG
        } else {
            aofAccountInfo.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            aofAccountInfo.REMEDIATE = (activity as CIFRootActivity).REMEDIATE
        } else {
            aofAccountInfo.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        aofAccountInfo.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID()
                .toString()
        aofAccountInfo.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        } else {
            aofAccountInfo.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        } else {
            aofAccountInfo.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            aofAccountInfo.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        aofAccountInfo.NAME = (activity as CIFRootActivity).fullName
        aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
        aofAccountInfo.RISK_RATING_TOTAL =
            (activity as CIFRootActivity).riskRatingTotal!!
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            aofAccountInfo.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                aofAccountInfo.MYSIS_REF = it.toString()
            }
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_BRANCH = it.BR_CODE
            }
        } else {
            aofAccountInfo.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.USER_REGION = it.REG_CODE
            }
        } else {
            aofAccountInfo.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                aofAccountInfo.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            aofAccountInfo.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        aofAccountInfo.let {
            (activity as CIFRootActivity).aofAccountInfoRequest = it
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF(aofAccountInfo)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                        if (findNavController().currentDestination?.id == R.id.CIFStep7_3)
                            findNavController().navigate(R.id.action_CIFStep7_3_to_CIFStep7_4)
                        (activity as CIFRootActivity).recyclerViewSetup()
                        (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

            override fun GenerateCIFFailure(response: BaseResponse) {
                ToastUtils.normalShowToast(activity, response.message, 1)
                (activity as CIFRootActivity).globalClass?.hideLoader()
            }
        })
}

fun validation(): Boolean {
    if (fivStatmentFrequencySP?.isEnabled == true && fivStatmentFrequencySP?.selectedItemPosition == 0) {
//            ToastUtils.normalShowToast(activity, resources!!.getString(R.string.err_stateFrequent))
        ToastUtils.normalShowToast(
            activity,
            resources!!.getString(R.string.err_stateFrequent),
            1
        )
        return false
    } else if (fivOperationInstructionSP?.selectedItemPosition == 0) {
        ToastUtils.normalShowToast(
            activity,
            "Please select operational instruction",
            1
        )
        return false
    } else if (fivNetBankSP?.selectedItem.toString() == "No") {//Internet should be YES as per CIF detailsFrag
        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "1") {
            ToastUtils.normalShowToast(
                context,
                "Internet banking should be YES as per CIF details",
                1
            )
            return false
        } else {
            return true
        }
    } else if (fivNetBankSP?.selectedItem.toString() == "Yes") {
        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.INTERNET_MOB_BNK_REQ == "0") {
            ToastUtils.normalShowToast(
                context,
                "Internet banking should be NO as per CIF details",
                1
            )
            return false
        } else {
            return true
        }
    } else if (fivOperationInstructionSP?.selectedItemPosition == 0) {
        ToastUtils.normalShowToast(
            activity,
            "Please select operational instruction",
            1
        )
        return false
    } else if (fivStatmentSP?.selectedItem.toString() == "Yes") {
        if (fivStatmentFrequencySP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select E-Statement frequency",
                1
            )
            return false
        } else {
            return true
        }
    } else {
        return true
    }
}

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
