package com.hbl.bot.ui.fragments.cif


import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.ActivityNotFoundException
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Spinner
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.GenerateCIFCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.GenerateCIFResponse
import com.hbl.bot.network.models.response.baseRM.NadraVerify
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep1_6.*
import kotlinx.android.synthetic.main.fragment_cifstep1_6.btBack
import kotlinx.android.synthetic.main.fragment_cifstep1_6.btNext
import kotlinx.android.synthetic.main.fragment_cifstep1_6.formSectionHeader
import java.text.SimpleDateFormat
import java.util.*
import com.hbl.bot.ui.activities.HawActivity
import com.hbl.bot.utils.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class HawCifDOBFragment5 : Fragment(), View.OnClickListener {
    val myCalendar = Calendar.getInstance()
    val viewModel: SharedCIFViewModel by activityViewModels()
    var documentTypeSp: Spinner? = null
    var trackingIDET: EditText? = null
    var documentIDET: EditText? = null
    var issueDateTV: TextView? = null
    var expireDateTV: TextView? = null
    var dateOfBirth: TextView? = null
    var myView: View? = null
    var bundle = Bundle()
    var nadraVerify = NadraVerify()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep1_6, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    @SuppressLint("SetTextI18n")
    fun header() {
        val view: TextView = formSectionHeader.getTextView()
        viewModel.totalSteps.setValue(3)
        viewModel.currentFragmentIndex.setValue(0)
        val txt = resources.getString(R.string.customer_id_details)
        val txt1 = " (1/3 Page)"
        val txt2 = txt + txt1
        view.text = GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            lifetimeBtn.setOnClickListener(this)
            header()
            init()
            load()
            openDateDialog()
            openDobDateDialog()
            expiryDate()
            onBackPress(view)
            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    fun openDobDateDialog() {
        dateOfBirth!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                try {
                    val myCalendar = Calendar.getInstance()
                    var datePicker =
                        activity?.let {
                            DatePickerDialog(
                                it,
                                R.style.DialogTheme,
                                dobDate,
                                myCalendar[Calendar.YEAR],
                                myCalendar[Calendar.MONTH],
                                myCalendar[Calendar.DAY_OF_MONTH]
                            )
                        }
                    if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE == "A7") {
                        myCalendar.add(Calendar.YEAR, -18)
                        datePicker?.getDatePicker()?.setMinDate(myCalendar.timeInMillis)
                        myCalendar.add(Calendar.YEAR, 18)
                        datePicker?.getDatePicker()?.setMaxDate(myCalendar.timeInMillis)
                    } else if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE == "A0") {
                        myCalendar.add(Calendar.YEAR, -24)
                        datePicker?.getDatePicker()?.setMinDate(myCalendar.timeInMillis)
                        myCalendar.add(Calendar.YEAR, 6)
                        datePicker?.getDatePicker()?.setMaxDate(myCalendar.timeInMillis)
                    } else if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE == "A8") {
                        myCalendar.add(Calendar.YEAR, -55)
                        datePicker?.getDatePicker()?.setMaxDate(myCalendar.timeInMillis)
                    } else {
                        myCalendar.add(Calendar.YEAR, -18)
                        datePicker?.getDatePicker()?.setMaxDate(myCalendar.timeInMillis)
                    }
                    datePicker?.show()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }
        })
    }

    var dobDate = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        myCalendar.add(Calendar.DATE, 0);
        // Set the Calendar new date as minimum date of date picker
        dobUpdateLabel()
    }

    private fun dobUpdateLabel() {
        try {
//        val myFormat = "dd-MMM-yyyy" //In which you need put here
            val myFormat = "dd-MM-yyyy"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            Log.i("test111", "run")
            dateOfBirth?.setText(sdf.format(myCalendar.time))
//        val comingFromNadraDOB = (activity as HawActivity).globalClass?.parseFormate((activity as HawActivity).customerBiometric.responsedateofbirth)
            val currentDate = (activity as HawActivity).globalClass?.currentDate
            val selectedDate = sdf.format(myCalendar.time)
            if ((activity as HawActivity).globalClass?.dobValidityCheck(
                    currentDate,
                    selectedDate
                )!! < 18
                && !(activity as HawActivity).customerDemoGraphicx.CUST_TYPE.equals("A7")
                && !((activity as HawActivity).customerDemoGraphicx.CUST_TYPE.equals("A8"))
                && !((activity as HawActivity).customerDemoGraphicx.CUST_TYPE.equals("A0"))
            ) {
                Log.i("test111", "run")
                dateOfBirth?.setText(resources!!.getString(R.string.date_picker))
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString(R.string.dob_age_error), 1
                )
            } else if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE.equals("A8")) {//SINOR CITIZER
//                  If customer segment = senior citizen than date of birth should be greater than 55 years
                if ((activity as HawActivity).globalClass?.dobValidityCheck(
                        currentDate, selectedDate
                    )!! >= 55
                ) {
                    Log.i("test111", "run")
                    dateOfBirth?.setText(sdf.format(myCalendar.time))
                } else {
                    Log.i("test111", "run")
                    dateOfBirth?.setText(resources!!.getString(R.string.date_picker))
                    ToastUtils.normalShowToast(
                        activity,
                        resources!!.getString(R.string.age_senior_citizen_error), 1
                    )
                }
            } else if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE.equals("A0")) {//STUDENT
//            If customer segment = student than date of birth should be between 18 years to 24 years.
                if ((activity as HawActivity).globalClass?.dobValidityCheck(
                        currentDate, selectedDate
                    )!! in 18..24
                ) {
                    Log.i("test111", "run")
                    dateOfBirth?.setText(sdf.format(myCalendar.time))
                } else {
                    Log.i("test111", "run")
                    dateOfBirth?.setText(resources!!.getString(R.string.date_picker))
                    ToastUtils.normalShowToast(
                        activity,
                        resources!!.getString(R.string.age_student_3_error), 1
                    )
                }
            } else if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE.equals("A7")) {//MINOR
                if ((activity as HawActivity).globalClass?.dobValidityCheck(
                        currentDate,
                        selectedDate
                    )!! <= 18
                ) {
                    /*if minor condition occured then 18 years add in dob date.*/
                    (activity as HawActivity).customerInfo.EXPIRY_DATE =
                        GlobalClass.addYears(sdf.format(myCalendar.time), 18)
                    (activity as HawActivity).sharedPreferenceManager.customerInfo =
                        (activity as HawActivity).customerInfo
                    Log.i("test111", "run")
                    dateOfBirth?.setText(sdf.format(myCalendar.time))
                } else {
                    Log.i("test111", "run")
                    dateOfBirth?.setText(resources!!.getString(R.string.date_picker))
                    ToastUtils.normalShowToast(
                        activity,
                        resources!!.getString(R.string.age_minor_error), 1
                    )
                }
            } else {
                Log.i("test111", "run")
                dateOfBirth?.setText(sdf.format(myCalendar.time))
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    private fun load() {
        try {
            arguments?.getParcelable<NadraVerify>(Constants.NADRA_RESPONSE_DATA)?.let {
                nadraVerify = it
            }
            if ((activity as HawActivity).customerBiometric.responseexpirydate.isNullOrEmpty()) {
                lifetimeBtn.visibility = View.VISIBLE
            } else {
                lifetimeBtn.visibility = View.GONE
            }
            fsvDocumentType.setItemForDocs((activity as HawActivity).docTypeList)
            (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeIndexValue(
                (activity as HawActivity).docTypeList,
                (activity as HawActivity).customerInfo.ID_DOCUMENT_TYPE_DESC
            ).let {
                documentTypeSp?.setSelection(it!!)
            }

            if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE.equals("A7") && !(activity as HawActivity).sharedPreferenceManager.customerInfo.EXPIRY_DATE.isNullOrEmpty()) {
                /*if customer Segment is minor and exipiry date modified 18 years of date*/
                (activity as HawActivity).sharedPreferenceManager.customerInfo.EXPIRY_DATE.let {
                    expireDateTV?.text = it
                }
            }
            if ((activity as HawActivity).customerInfo.issuedate.isNullOrEmpty()) {
                issueDateTV?.setText(resources!!.getString(R.string.date_picker))
            } else {
//                (activity as HawActivity).globalClass?.setDisbaled(issueDateTV!!,false)
                issueDateTV?.setText((activity as HawActivity).globalClass?.formateChange((activity as HawActivity).customerInfo.issuedate))
            }
            if ((activity as HawActivity).customerInfo.expirydate.isNullOrEmpty()) {
                expireDateTV?.setText(resources!!.getString(R.string.date_picker))
            } else {
                (activity as HawActivity).globalClass?.setDisbaled(expireDateTV!!,false)
                expireDateTV?.setText((activity as HawActivity).globalClass?.formateChange((activity as HawActivity).customerInfo.expirydate))
            }
            if ((activity as HawActivity).customerInfo.DATE_OF_BIRTH.isNullOrEmpty()) {
                Log.i("test111", "run")
                dateOfBirth?.text = resources!!.getString(R.string.date_picker)
            } else {
                Log.i("test111", "fixed")
                (activity as HawActivity).globalClass?.setDisbaled(dateOfBirth!!,false)
                dateOfBirth?.setText((activity as HawActivity).globalClass?.formateChange((activity as HawActivity).customerInfo.DATE_OF_BIRTH))
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    fun init() {
        (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
        documentTypeSp = fsvDocumentType.getSpinner(R.id.documentTypeID)
        trackingIDET = fivTrackingID.getTextFromEditText(R.id.txtTracking)
        trackingIDET?.isEnabled = false
        if (!(activity as HawActivity).aofAccountInfoRequest.TRACKING_ID.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                trackingIDET?.setText(it)
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                ?.getOrNull(0)?.itemDescription.let {
                    trackingIDET?.setText(
                        it
                    )
                }
        }
        documentIDET = fivIdDocumentNo.getTextFromEditText(R.id.documentID)
        issueDateTV = fivIssueDate.getTextFromTextView(R.id.fivIssueDate)
        expireDateTV = fivExpiryDate.getTextFromTextView(R.id.expiry_date)
        dateOfBirth = fivDOB.getTextFromTextView(R.id.date_of_birth)
        documentIDET?.isEnabled = false
        (activity as HawActivity).customerInfo.ID_DOCUMENT_NO.let {
            documentIDET?.setText(it)
            documentIDET?.setTextColor(Color.parseColor("#008577"));
        }
        (activity as HawActivity).globalClass?.setDisbaled(documentTypeSp!!)
        (activity as HawActivity).globalClass?.setDisbaled(trackingIDET!!)
        (activity as HawActivity).globalClass?.setDisbaled(documentIDET!!)
    }
    private fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
            (activity as HawActivity).globalClass?.setDisbaled(documentTypeSp!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(trackingIDET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(issueDateTV!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(expireDateTV!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(dateOfBirth!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(documentIDET!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(lifetimeBtn!!, false)
        }
    }
    fun openDateDialog() {
        issueDateTV!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                try {
                    var datePicker =
                        activity?.let {
                            DatePickerDialog(
                                it,
                                R.style.DialogTheme,
                                date,
                                myCalendar[Calendar.YEAR],
                                myCalendar[Calendar.MONTH],
                                myCalendar[Calendar.DAY_OF_MONTH]
                            )
                        }
                    datePicker?.getDatePicker()?.setMaxDate(System.currentTimeMillis())
                    datePicker?.show()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }
        })
    }

    var date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        myCalendar.add(Calendar.DATE, 0);
        // Set the Calendar new date as minimum date of date picker
        updateLabel()
    }

    private fun updateLabel() {
        try {
//        val myFormat = "dd-MMM-yyyy" //In which you need put here
            val myFormat = "dd-MM-yyyy"
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            val comingFromNadraDOB =
                (activity as HawActivity).sharedPreferenceManager.customerInfo.DATE_OF_BIRTH
            if ((activity as HawActivity).globalClass?.yearFromDOBValidate(
                    sdf.format(myCalendar.time), dateOfBirth?.text.toString().trim()
                )!! >= 18 && (activity as HawActivity).customerDemoGraphicx.CUST_TYPE != "A7"
            ) {
                //working is fine in if this case is not  minor then customer will greater then equal to 18
//                (activity as HawActivity).globalClass?.setEnabled(expireDateTV!!, true)
                issueDateTV?.setText(sdf.format(myCalendar.time))
            } else if ((activity as HawActivity).globalClass?.yearFromDOBValidate(
                    sdf.format(myCalendar.time), dateOfBirth?.text.toString().trim()
                )!! >= 18 && (activity as HawActivity).customerDemoGraphicx.CUST_TYPE == "A7"
            ) {
                //Error if this case is minor then customer will less then to 18
                issueDateTV?.setText(resources!!.getString(R.string.date_picker))
                expireDateTV?.setText(resources!!.getString(R.string.date_picker))
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString(R.string.age_minor_2_error), 1
                )
            } else if ((activity as HawActivity).globalClass?.yearFromDOBValidate(
                    sdf.format(myCalendar.time), dateOfBirth?.text.toString().trim()
                )!! < 18 && (activity as HawActivity).customerDemoGraphicx.CUST_TYPE == "A7"
            ) {
                //working is fine in if this case is  minor then customer will less then equal to 18
//                (activity as HawActivity).globalClass?.setEnabled(expireDateTV!!, true)
                issueDateTV?.setText(sdf.format(myCalendar.time))
            } else {
                //Error if this is any case then customer will greater then to 18
                issueDateTV?.setText(resources!!.getString(R.string.date_picker))
                expireDateTV?.setText(resources!!.getString(R.string.date_picker))
                ToastUtils.normalShowToast(
                    activity,
                    resources!!.getString(R.string.age_18_years_error), 1
                )
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    fun expiryDate() {
/*        val comingFromNadraDOB = issueDateTV?.text.toString()
        if (comingFromNadraDOB != null && !TextUtils.isEmpty(comingFromNadraDOB)) {
            (activity as HawActivity).globalClass?.setEnabled(expireDateTV!!, false)
            expireDateTV?.setText(comingFromNadraDOB)
        } else {*/

        expireDateTV!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                try {
                    if (!issueDateTV?.text?.equals(resources!!.getString(R.string.date_picker))!!) {
                        var datePicker =
                            activity?.let {
                                DatePickerDialog(
                                    it, R.style.DialogTheme,
                                    date2,
                                    myCalendar[Calendar.YEAR],
                                    myCalendar[Calendar.MONTH],
                                    myCalendar[Calendar.DAY_OF_MONTH]
                                )
                            }
                        val sdf = SimpleDateFormat("dd-MM-yyyy")
                        val mDate: Date = sdf.parse(issueDateTV?.text.toString())
                        if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE != "A8") {
                            datePicker?.getDatePicker()?.setMinDate(System.currentTimeMillis())
                        }
                        datePicker?.show()
                    } else {
                        ToastUtils.normalShowToast(
                            activity,
                            resources!!.getString(R.string.issue_date_error), 1
                        )
                        issueDateTV?.requestFocus();
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }
        })
//        }
    }

    var date2 = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        myCalendar.add(Calendar.DATE, 0);
        // Set the Calendar new date as minimum date of date picker
        updateLabel2()
    }

    private fun updateLabel2() {
        try {
//        val myFormat = "dd-MMM-yyyy" //In which you need put here
            val myFormat = "dd-MM-yyyy" //In which you need put here
            val sdf = SimpleDateFormat(myFormat, Locale.US)
            expireDateTV?.setText(sdf.format(myCalendar.time))
//        var expiryexpiry = sdf.format(myCalendar.time)
            /*var issueDate = issueDateTV?.text?.trim().toString()
            var expiryDateChange = issueDateTV?.text?.trim().toString()
            Log.i("issueExpiryDate", "$expiryexpiry=$issueDate")
            if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE.equals("A7")) {
                if ((activity as HawActivity).expiryDate.equals(
                        GlobalClass.minusYears(
                            (activity as HawActivity).sharedPreferenceManager.customerInfo.EXPIRY_DATE,
                            18
                        )
                    )
                ) {
        //                (activity as HawActivity).globalClass?.setDisbaled(expireDateTV!!)
                    Log.i(
                        "oldAndNew",
                        (activity as HawActivity).expiryDate + "=" + GlobalClass.minusYears(
                            (activity as HawActivity).sharedPreferenceManager.customerInfo.EXPIRY_DATE,
                            18
                        )
                    )
                    return
                } else {
                    if (issueDate != expiryexpiry) {
                        expireDateTV?.setText(sdf.format(myCalendar.time))
                        (activity as HawActivity).expiryDate = sdf.format(myCalendar.time)
                    } else {
                        ToastUtils.normalShowToast(
                            activity,
                            resources!!.getString(R.string.issue_expiry_date_cannot_be_same_error), 1
                        )
                        expireDateTV?.setText(resources!!.getString(R.string.date_picker))
                        issueDateTV?.requestFocus();
                    }

                }
            } else {
                //MINOR this field is remain freeze
                if (issueDate != expiryexpiry) {
                    expireDateTV?.setText(sdf.format(myCalendar.time))
                    (activity as HawActivity).expiryDate = sdf.format(myCalendar.time)
                } else {
                    ToastUtils.normalShowToast(
                        activity,
                        resources!!.getString(R.string.issue_expiry_date_cannot_be_same_error), 1
                    )
                    expireDateTV?.setText(resources!!.getString(R.string.date_picker))
                    issueDateTV?.requestFocus();
                }
            }*/
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity),
                (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    override fun onClick(v: View?) {
        val bundle = Bundle()
        bundle.putParcelable(Constants.NADRA_RESPONSE_DATA, nadraVerify)
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                        (this.context as HawActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            setAllModelsInAOFRequestModel()
                            activity?.runOnUiThread {
                                (activity as HawActivity).globalClass?.setEnabled(btNext!!,
                                    true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity,
                        getString(R.string.something_went_wrong),
                        3)
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }
            R.id.btBack ->
                if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
                    if (findNavController().currentDestination?.id == R.id.CIFStep1_6) {
                        findNavController().navigate(
                            R.id.action_CIFStep1_6_to_CIFStep1_3,
                            bundle
                        )
                    }
                } else {
                    if (findNavController().currentDestination?.id == R.id.CIFStep1_6) {
                        findNavController().navigate(
                            R.id.action_CIFStep1_6_to_CIFStep1_5,
                            bundle
                        )
                    }
                }
            R.id.lifetimeBtn -> {
                expireDateTV?.setText("31-12-2099")
            }
        }

    }

    fun onBackPress(view: View) {
        this.view?.isFocusableInTouchMode = true
        this.view?.requestFocus()
        this.view?.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep1_6) {
                    findNavController().navigate(
                        R.id.action_CIFStep1_6_to_CIFStep1_5,
                        bundle
                    )
                }
                true
            }
            false
        }
    }

    fun validation(): Boolean {
        val currentDate = (activity as HawActivity).globalClass?.currentDate
        val selectedDate = dateOfBirth?.text.toString()

        if (issueDateTV?.text?.toString()
                .equals(resources!!.getString(R.string.date_picker)) || issueDateTV?.text.toString()
                .isNullOrEmpty()
        ) {
            ToastUtils.normalShowToast(activity,
                resources!!.getString(R.string.issue_date_error),
                1)
            return false;
        } else if (expireDateTV?.text?.toString()
                .equals(resources!!.getString(R.string.date_picker)) || expireDateTV?.text.toString()
                .isNullOrEmpty()
        ) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.expiry_date_error),
                1
            )
            return false;
        } else if (dateOfBirth?.text?.toString()
                .equals(resources!!.getString(R.string.date_picker)) || dateOfBirth?.text.toString()
                .isNullOrEmpty()
        ) {
            ToastUtils.normalShowToast(activity, resources!!.getString(R.string.dob_date_error), 1)
            return false;
        } else if ((activity as HawActivity).globalClass?.dobValidityCheck(
                currentDate,
                selectedDate
            )!! < 18
            && (activity as HawActivity).customerDemoGraphicx.CUST_TYPE != "A7"
        ) {
            //18 year Condition
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.dob_age_error), 1
            )
            return false
        } else if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE == "A8" && (activity as HawActivity).globalClass?.dobValidityCheck(
                currentDate, selectedDate
            )!! < 55
        ) {//SINOR CITIZER
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.age_senior_citizen_error), 1
            )
            return false
        } else if ((activity as HawActivity).customerDemoGraphicx.CUST_TYPE == "A0" && ((activity as HawActivity).globalClass?.dobValidityCheck(
                currentDate, selectedDate
            )!! < 18 || (activity as HawActivity).globalClass?.dobValidityCheck(
                currentDate, selectedDate
            )!! > 24
                    )
        ) {
            //STUDENT
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.age_student_3_error), 1
            )
            return false
        } else if ((activity as HawActivity).globalClass?.yearFromDOBValidate(
                issueDateTV?.text.toString().trim(), dateOfBirth?.text.toString().trim()
            )!! < 18 && ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE != "A7")
        ) {
            //STUDENT
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.age_18_years_error), 1
            )
            return false
        } else if ((activity as HawActivity).globalClass?.compareDateValidate(issueDateTV?.text.toString()
                .trim(), dateOfBirth?.text.toString().trim())!!
        ) {
            //EveryCase
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.age_issue_date_less_error), 1
            )
            return false
        } else if ((activity as HawActivity).globalClass?.compareDateValidate(expireDateTV?.text.toString()
                .trim(), issueDateTV?.text.toString().trim())!!
        ) {
            //EveryCase
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.age_expiry_less_error), 1
            )
            return false
        } else {
            return true;
        }
    }

    fun saveAndNext() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in HawActivity*/
        /*set the values from widgets and another models*/
        /*  issueDateTV?.text.toString().let { (activity as HawActivity).customerAccounts.ID_DOC_NO = it }
          expireDateTV?.text.toString().let { (activity as HawActivity).customerAccounts.ID_DOC_NO = it}*/
        /*  (activity as HawActivity).globalClass?.findSpinnerPositionFromDocTypeCode(
                  (activity as HawActivity).docTypeList, documentTypeSp?.selectedItem.toString()
          ).let {
              if (it != resources!!.getString(R.string.select) && it!="0") {// when select there is no item selected "Choose an item"  <--- hint
                  (activity as HawActivity).customerAccounts.ACCT_TYPE = it.toString()
              }
          }
          documentIDET?.text.toString()
              .let { (activity as HawActivity).customerAccounts.ACCT_TYPE = it }*/
        issueDateTV?.text.toString()
            .let {
                if (!issueDateTV?.text?.toString().equals(
                        resources!!.getString(R.string.date_picker)
                    )
                ) {
                    (activity as HawActivity).customerInfo.ISSUE_DATE = it
                }
            }
        expireDateTV?.text.toString()
            .let {
                if (
                    !issueDateTV?.text?.toString().equals(
                        resources!!.getString(R.string.date_picker)
                    )
                ) {
                    (activity as HawActivity).customerInfo.EXPIRY_DATE = it
                }
            }
        dateOfBirth?.text.toString().let {
            if (!dateOfBirth?.text?.toString()
                    .equals(resources!!.getString(R.string.date_picker))
            ) {
                (activity as HawActivity).customerInfo.DATE_OF_BIRTH = it
            }
        }
        /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in HawActivity*/
        /*  (activity as HawActivity).sharedPreferenceManager.customerAccount =
              ((activity as HawActivity).customerAccounts)*/
        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo =
            ((activity as HawActivity).aofAccountInfoRequest)
        (activity as HawActivity).sharedPreferenceManager.customerInfo =
            ((activity as HawActivity).customerInfo)
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as HawActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerAddress.let {
            (activity as HawActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as HawActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        if ((activity as HawActivity).aofAccountInfoRequest.TRACKING_ID.isNullOrEmpty()) {
            aofDataAlign(
                "1",
                Constants.SUBMIT_DRAFT_IDENTIFIER
            )
        } else {
            aofDataAlign(
                "1",
                Constants.UPDATE_DRAFT_IDENTIFIER
            )
        }
    }

    fun aofDataAlign(
        doc_inserted: String,
        identifier: String
    ) {
        (activity as HawActivity).aofAccountInfoRequest.CHANNEL = "9"
//        (activity as HawActivity).aofAccountInfoRequest.DOC_INSERTED = doc_inserted
        //TODO: set HIGH_RISK_BRANCH
        if (!(activity as HawActivity).sharedPreferenceManager.loginData.HIGH_RISK_BRANCH.isNullOrEmpty()
        ) {
            (activity as HawActivity).sharedPreferenceManager.loginData.HIGH_RISK_BRANCH.let {
                (activity as HawActivity).aofAccountInfoRequest.HIGH_RISK_BRANCH = it.toString()
            }
        }
        //TODO: set trackingID
        if ((activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {1104230008-24
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let {
                    (activity as HawActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).etbntbFLAG
        } else {
            (activity as HawActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).REMEDIATE
        } else {
            (activity as HawActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as HawActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as HawActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        (activity as HawActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as HawActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).fullName
        } else {
            (activity as HawActivity).aofAccountInfoRequest.NAME =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).riskRating.toString()
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).riskRatingTotal!!
        } else {
            (activity as HawActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set BRANCH_NAME
        (activity as HawActivity).sharedPreferenceManager.branchCode.PRIORITY.let {
            (activity as HawActivity).customerAccounts.USER_BRANCH_PRIORITY = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as HawActivity).sharedPreferenceManager.mySisRef.data.size == 0) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as HawActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as HawActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }

        /*
        *
        * */
        (activity as HawActivity).sharedPreferenceManager.appStatus.CIF.let {
            (activity as HawActivity).globalClass?.appStatus = it.toString()
        }
        if (!(activity as HawActivity).sharedPreferenceManager.aofAccountInfo.WORK_FLOW_CODE_DESC.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.WORK_FLOW_CODE_DESC.let {
                if (it == "CIF") {
                    (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC =
                        it.toString()
                }
            }

            //TODO: set WORK_FLOW_CODE
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.WORK_FLOW_CODE.let {
                if (it == "1") {
                    (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE =
                        it.toString()
                }
            }

        } else {
            //TODO: set WORK_FLOW_CODE_DESC
            (activity as HawActivity).sharedPreferenceManager.workFlow.data.get(0)
                .PUR_DESC.let {
                    if (it == "CIF") {
                        (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC =
                            it.toString()
                    }
                }
            //TODO: set WORK_FLOW_CODE
            (activity as HawActivity).sharedPreferenceManager.workFlow.data.get(0)
                .WORK_FLOW_CODE.let {
                    if (it == "1") {
                        (activity as HawActivity).aofAccountInfoRequest.WORK_FLOW_CODE =
                            it.toString()
                    }
                }
        }

        /*
        *
        * */

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.USER_REGION =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as HawActivity).sharedPreferenceManager.businessArea.let {
                (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as HawActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as HawActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as HawActivity).aofAccountInfoRequest.let {
            (activity as HawActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF((activity as HawActivity).aofAccountInfoRequest, identifier)
    }

    fun postGenerateCIF(aofAccountInfo: Data, identifier: String) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        request.identifier = identifier
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        if (response.status.equals("01") &&
                            aofAccountInfo.DOC_INSERTED.equals("1") &&
                            response.message.equals("This tracking id already found in db, please create new record again.")
                        ) {
                            /*When submit identifier api response error then move to update api response call*/
                            aofDataAlign("1", Constants.UPDATE_DRAFT_IDENTIFIER)
                        }else {
                            if (findNavController().currentDestination?.id == R.id.CIFStep1_6)
                                findNavController().navigate(R.id.action_CIFStep1_6_to_CIFStep1_7)
                        }
                        (activity as HawActivity).recyclerViewSetup()
                        (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
                        (activity as HawActivity).globalClass?.hideLoader()
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity),
                            (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity,
                            getString(R.string.something_went_wrong),
                            3)
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as HawActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as HawActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
