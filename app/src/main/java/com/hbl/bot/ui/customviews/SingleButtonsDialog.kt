package com.hbl.bot.ui.customviews

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.TextView
import com.hbl.bot.R

class SingleButtonsDialog(
    c: Context,
    private var headerTitle: String,
    message: String,
    positiveTitle: String,
    callback: Dialog2Callback

) :
    Dialog(c), View.OnClickListener {
    var d: Dialog? = null
    var yes: Button? = null
    var message: String
    var positiveTitle: String
    private var callback: Dialog2Callback
    var tvMessage: TextView? = null
    var tvHeader: TextView? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        window!!.setBackgroundDrawableResource(android.R.color.transparent)
        setContentView(R.layout.popup_single_buttons_dialog)
        yes = findViewById(R.id.bt_confirm)
        tvMessage = findViewById(R.id.tv_message)
        tvHeader = findViewById(R.id.tv_header)
        tvHeader?.text = headerTitle
        tvMessage?.text = message
        yes?.text = positiveTitle
        yes?.setOnClickListener(this)
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.bt_confirm -> callback.onPositiveClicked()
            else -> {
            }
        }
        dismiss()
    }

    init {
        this.callback = callback
        this.message = message
        this.positiveTitle = positiveTitle
    }
}

interface Dialog2Callback {
    fun onPositiveClicked() {}
}