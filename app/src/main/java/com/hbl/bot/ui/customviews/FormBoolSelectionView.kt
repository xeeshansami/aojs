package com.hbl.bot.ui.customviews

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Spinner
import androidx.constraintlayout.widget.ConstraintLayout
import com.hbl.bot.R
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.ui.fragments.spark.cif.CIFStep1_4_Morpho_fingerprint_6_34
import kotlinx.android.synthetic.main.view_form_bool_selection.view.mandateSpinner
import kotlinx.android.synthetic.main.view_form_bool_selection.view.spinner
import kotlinx.android.synthetic.main.view_form_bool_selection.view.spinnerLayout
import kotlinx.android.synthetic.main.view_form_bool_selection.view.tvTitleUrdu
import kotlinx.android.synthetic.main.view_form_bool_selection.view.tv_title


class FormBoolSelectionView(context: Context, attrs: AttributeSet) :
    ConstraintLayout(context, attrs) {
    var countryObj = Country()
    var relationshipObj = Relationship()
    var cityObj = City()
    var addressTypeObj = AddressType()
    var sourceOfFundObj = SourceOffund()
    var purposeOfTinObj = PurposeOfTin()
    var sourceOfWealthObj = SourceOfWealth()
    var fragment =
        CIFStep1_4_Morpho_fingerprint_6_34()
    private var showRemove: Boolean = true

    init {
        inflate(context, R.layout.view_form_bool_selection, this)
        val attributes = context.obtainStyledAttributes(attrs, R.styleable.FormSelectionView)
        tv_title.text = attributes.getString(R.styleable.FormSelectionView_titleText)
        tvTitleUrdu.text = attributes.getString(R.styleable.FormSelectionView_titleTextUrdu)
        mandateSpinner.text = attributes.getString(R.styleable.FormSelectionView_mandatory)
        showRemove = attributes.getBoolean(R.styleable.FormSelectionView_showRemoveIcon, true)
        attributes.recycle()
        resources!!.getString(R.string.select).let {
            countryObj.COUNTRY_CODE = "0"
            countryObj.COUNTRY_NAME = it
        }
        resources!!.getString(R.string.select).let {
            relationshipObj.RELATION_CODE = "0"
            relationshipObj.RELATION_DESC = it
        }
        resources!!.getString(R.string.select).let {
            cityObj.CITY_CODE = "0"
            cityObj.CITY_NAME = it
        }
        resources!!.getString(R.string.select).let {
            addressTypeObj.ADDR_CODE = "0"
            addressTypeObj.ADDR_DESC = it
        }
        resources!!.getString(R.string.select).let {
            sourceOfFundObj.SOF_CODE = "0"
            sourceOfFundObj.SOF_DESC = it
        }
        resources!!.getString(R.string.select).let {
            purposeOfTinObj.PURP_CODE = "0"
            purposeOfTinObj.PURP_DESC = it
        }
        resources!!.getString(R.string.select).let {
            sourceOfWealthObj.SOW_CODE = "0"
            sourceOfWealthObj.SOW_DESC = it
        }
    }

    fun setItemForDocs(item: ArrayList<DocsData>) {
        var obj = DocsData()
        resources!!.getString(R.string.select).let {
            obj.iDDOCCODE = "0"
            obj.iDDOCDESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForModeOfTrans(item: ArrayList<ModeOfTans>) {
        var obj = ModeOfTans()
        resources!!.getString(R.string.select).let {
            obj.TRAN_CODE = "0"
            obj.TRAN_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun getSpinnerValue(): Int {
        return spinner.selectedItemPosition
    }

    fun setItemForCustomerSegment(item: ArrayList<CustomerSegment>) {
        var obj = CustomerSegment()
        resources!!.getString(R.string.select).let {
            obj.cUSTSEGMENT = "0"
            obj.sEGMENTDESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForNatureOfBusiness(item: ArrayList<NatureOfBusiness>) {
        var obj = NatureOfBusiness()
        resources!!.getString(R.string.select).let {
            obj.BUS_CODE = "0"
            obj.BUS_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForProfessions(item: ArrayList<Profession>) {
        var obj = Profession()
        resources!!.getString(R.string.select).let {
            obj.PROF_CODE = "0"
            obj.PROF_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForSourceOfIncome(item: ArrayList<SourceOffund>) {
        var obj = SourceOffund()
        if (item[0].SOF_CODE != "0") {
            item.add(0, sourceOfFundObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForCountries(item: ArrayList<Country>) {
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivCountry(item: ArrayList<Country>) {
        spinner.id = R.id.fivCountry
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForPepReasonCountry(item: ArrayList<Country>) {
        spinner.id = R.id.normalCountry
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivNOCountry(item: ArrayList<Country>) {
        spinner.id = R.id.NOCountry
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivCOOC(item: ArrayList<Country>) {
        spinner.id = R.id.COOC
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun getSpinner(id: Int): Spinner {
        spinner.id = id
        return spinner;
    }

    fun getSpinner(): Spinner {
        return spinner
    }

    fun getLayout(id: Int): ConstraintLayout {
        spinnerLayout.id = id
        return spinnerLayout;
    }

    fun getLayout(): ConstraintLayout {

        return spinnerLayout;
    }

    fun setItemForfivSecondNationality(item: ArrayList<Country>): Spinner {
        spinner.id = R.id.cif1SecondNationality
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
        return spinner
    }

    fun setItemForfivNationality(item: ArrayList<Country>): Spinner {
        spinner.id = R.id.cif1Nationality
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
        return spinner
    }

    fun setItemForfivCOR(item: ArrayList<Country>) {
        spinner.id = R.id.COR
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfsvCountryOfBirth(item: ArrayList<Country>) {
        spinner.id = R.id.CountryOfBirth
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivCountryInward1(item: ArrayList<Country>) {
        spinner.id = R.id.CountryInward1
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivCountryInward2(item: ArrayList<Country>) {
        spinner.id = R.id.CountryInward2
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivCountryInward3(item: ArrayList<Country>) {
        spinner.id = R.id.CountryInward3
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivCountryOutward1(item: ArrayList<Country>) {
        spinner.id = R.id.CountryOutward1
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivCountryOutward2(item: ArrayList<Country>) {
        spinner.id = R.id.CountryOutward2
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForfivCountryOutward3(item: ArrayList<Country>) {
        spinner.id = R.id.CountryOutward3
        if (item[0].COUNTRY_CODE != "0") {
            item.add(0, countryObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        if (item.size == 2) {
            spinner.setSelection(1)
        }
        invalidate();
    }

    fun setItemForAccountOperationsType(item: ArrayList<AccountOperationsInst>) {
        var obj = AccountOperationsInst()
        resources!!.getString(R.string.select).let {
            obj.OPER_CODE = "0"
            obj.OPER_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForEstateFrequency(item: ArrayList<EstateFrequency>) {
        var obj = EstateFrequency()
        resources!!.getString(R.string.select).let {
            obj.E_STATE_FRQ_CODE = "0"
            obj.E_STATE_FRQ = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForCurrency(item: ArrayList<Currency>) {
        var obj = Currency()
        resources!!.getString(R.string.select).let {
            obj.CURR_CODE = "0"
            obj.CURR_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForDepositType(item: ArrayList<DepositType>) {
        var obj = DepositType()
        resources!!.getString(R.string.select).let {
            obj.DEP_CODE = "0"
            obj.DEP_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPurposeOfTin1(item: ArrayList<PurposeOfTin>) {
        spinner.id = R.id.fivReasonTin1
        if (item[0].PURP_CODE != "0") {
            item.add(0, purposeOfTinObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPurposeOfTin2(item: ArrayList<PurposeOfTin>) {
        spinner.id = R.id.fivReasonTin2
        if (item[0].PURP_CODE != "0") {
            item.add(0, purposeOfTinObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPurposeOfTin3(item: ArrayList<PurposeOfTin>) {
        spinner.id = R.id.fivReasonTin3
        if (item[0].PURP_CODE != "0") {
            item.add(0, purposeOfTinObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForCapacity(item: ArrayList<Capacity>) {
        var obj = Capacity()
        resources!!.getString(R.string.select).let {
            obj.CAPACITY_CODE = "0"
            obj.CAPACITY_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPurpOfCif(item: ArrayList<PurposeCIF>) {
        var obj = PurposeCIF()
        resources!!.getString(R.string.select).let {
            obj.PURP_CODE = "0"
            obj.PURP_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForBools(item: ArrayList<Bool>) {
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForBoolRelationShip(item: ArrayList<Bool>) {
        spinner.id = R.id.relationshipBool
        /*       var obj = Bool()
               resources!!.getString(R.string.select).let {
                   obj.LB_CODE = "0"
                   obj.LB_NAME = it
               }
               item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivZakat(item: ArrayList<Bool>) {
        spinner.id = R.id.Zakat
        /*    var obj = Bool()
            resources!!.getString(R.string.select).let {
                obj.LB_CODE = "0"
                obj.LB_NAME = it
            }
            item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivNetBank(item: ArrayList<Bool>) {
        spinner.id = R.id.NetBank
        /*      var obj = Bool()
              resources!!.getString(R.string.select).let {
                  obj.LB_CODE = "0"
                  obj.LB_NAME = it
              }
              item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivPhoneBank(item: ArrayList<Bool>) {
        spinner.id = R.id.PhoneBank
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivSMS(item: ArrayList<Bool>) {
        spinner.id = R.id.SMS
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivBankInfo(item: ArrayList<Bool>) {
        spinner.id = R.id.BankInfo
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivOutwardRemittance(item: ArrayList<Bool>) {
        spinner.id = R.id.OutwardRemittance
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivInwardRemittance(item: ArrayList<Bool>) {
        spinner.id = R.id.InwardRemittance
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivIrisFormW8(item: ArrayList<Bool>) {
        spinner.id = R.id.IrisFormW8
        /*      var obj = Bool()
              resources!!.getString(R.string.select).let {
                  obj.LB_CODE = "0"
                  obj.LB_NAME = it
              }
              item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivIrisFormW9(item: ArrayList<Bool>) {
        spinner.id = R.id.IrisFormW9
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivDomesticPep(item: ArrayList<Bool>) {
        /*      var obj = Bool()
              resources!!.getString(R.string.select).let {
                  obj.LB_CODE = "0"
                  obj.LB_NAME = it
              }
              item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivForeignPep(item: ArrayList<Bool>) {
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivTaxResidencey(item: ArrayList<Bool>) {
        spinner.id = R.id.TaxResidencey
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivCitizenship(item: ArrayList<Bool>) {
        spinner.id = R.id.Citizenship
        /*     var obj = Bool()
             resources!!.getString(R.string.select).let {
                 obj.LB_CODE = "0"
                 obj.LB_NAME = it
             }
             item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivSpendDays(item: ArrayList<Bool>) {
        spinner.id = R.id.SpendDays
        /* var obj = Bool()
         resources!!.getString(R.string.select).let {
             obj.LB_CODE = "0"
             obj.LB_NAME = it
         }
         item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivHBLNisa(item: ArrayList<Bool>) {
        spinner.id = R.id.HBLNisa
        /*    var obj = Bool()
            resources!!.getString(R.string.select).let {
                obj.LB_CODE = "0"
                obj.LB_NAME = it
            }
            item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivfivCardRequird(item: ArrayList<Bool>) {
        spinner.id = R.id.fivCardRequird
        /*   var obj = Bool()
           resources!!.getString(R.string.select).let {
               obj.LB_CODE = "0"
               obj.LB_NAME = it
           }
           item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivStatment(item: ArrayList<Bool>) {
        spinner.id = R.id.Statment
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivInsurance(item: ArrayList<Bool>) {
        spinner.id = R.id.Insurance
/*        var obj = Bool()
        resources!!.getString(R.string.select).let {
            obj.LB_CODE = "0"
            obj.LB_NAME = it
        }
        item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivExistingRelation(item: ArrayList<Bool>) {
        spinner.id = R.id.ExistingRelation
        /* var obj = Bool()
         resources!!.getString(R.string.select).let {
             obj.LB_CODE = "0"
             obj.LB_NAME = it
         }
         item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPartyMandate(item: ArrayList<Bool>) {
        spinner.id = R.id.PartyMandate
        /*  var obj = Bool()
          resources!!.getString(R.string.select).let {
              obj.LB_CODE = "0"
              obj.LB_NAME = it
          }
          item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivReason(item: ArrayList<Bool>) {
        spinner.id = R.id.Reason
        /* var obj = Bool()
         resources!!.getString(R.string.select).let {
             obj.LB_CODE = "0"
             obj.LB_NAME = it
         }
         item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForCardProducts(item: ArrayList<CardProduct>) {
        var obj = CardProduct()
        resources!!.getString(R.string.select).let {
            obj.PRD_CODE = "0"
            obj.PRD_NAME = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPrefComMode(item: ArrayList<PrefComMode>) {
        var obj = PrefComMode()
        resources!!.getString(R.string.select).let {
            obj.COMMU_CODE = "0"
            obj.COMMU_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForIndustryMainCategory(item: ArrayList<IndustryMainCategory>) {
        var obj = IndustryMainCategory()
        resources!!.getString(R.string.select).let {
            obj.SOIIC_CODE = "0"
            obj.SOIIC_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForIndustrySubCategory(item: ArrayList<IndustrySubCategory>) {
        var obj = IndustrySubCategory()
        resources!!.getString(R.string.select).let {
            obj.SOIISC_CODE = "0"
            obj.SOIISC_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForFingers() {
        var adapter = ArrayAdapter(
            context,
            R.layout.view_spinner_item,
            R.id.text1,
            resources!!.getStringArray(R.array.fingers)
        )
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
        spinner?.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(p0: AdapterView<*>?) {
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, position: Int, p3: Long) {

            }
        }
    }

    fun setItemForCities(item: ArrayList<City>) {
        if (item[0].CITY_CODE != "0") {
            item.add(0, cityObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForCity(item: ArrayList<City>) {
        spinner.id = R.id.city
        resources!!.getString(R.string.select).let {
            cityObj.CITY_CODE = "0"
            cityObj.CITY_NAME = it
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForAddressType(item: ArrayList<AddressType>) {
        if (item[0].ADDR_CODE != "0") {
            item.add(0, addressTypeObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForAddType(item: ArrayList<AddressType>) {
        spinner.id = R.id.addType
        if (item[0].ADDR_CODE != "0") {
            item.add(0, addressTypeObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForBioAccountType(item: ArrayList<BioAccountType>) {
        var obj = BioAccountType()
        resources!!.getString(R.string.select).let {
            obj.BAT_CODE = "0"
            obj.BAT_NAME = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForAccountType(item: ArrayList<AccountType>) {
        var obj = AccountType()
        resources!!.getString(R.string.select).let {
            obj.ACCT_TYPE = "0"
            obj.ACCT_TYPE_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForProvince(item: ArrayList<Province>) {
        var obj = Province()
        resources!!.getString(R.string.select).let {
            obj.PR_CODE = "0"
            obj.PR_NAME = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPurposeCIF(item: ArrayList<PurposeCIF>) {
        var obj = PurposeCIF()
        resources!!.getString(R.string.select).let {
            obj.PURP_CODE = "0"
            obj.PURP_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForTitle(item: ArrayList<Title>) {
        /*    var obj = Title()
            resources!!.getString(R.string.select).let {
                obj.TITLE_CODE = "0"
                obj.TITLE_DESC = it
            }
            item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForMarital(item: ArrayList<MaritalStatus>) {
        var obj = MaritalStatus()
        resources!!.getString(R.string.select).let {
            obj.MARITAL_CODE = "0"
            obj.MARITAL_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPep(item: ArrayList<Pep>) {
        var obj = Pep()
        resources!!.getString(R.string.select).let {
            obj.REASON_CODE = "0"
            obj.REASON_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForSourceOffund(item: ArrayList<SourceOffund>) {
        spinner.id = R.id.SourceOffund
        if (item[0].SOF_CODE != "0") {
            item.add(0, sourceOfFundObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivSourceOfIncome(item: ArrayList<SourceOffund>) {
        spinner.id = R.id.SourceOfIncome
        if (item[0].SOF_CODE != "0") {
            item.add(0, sourceOfFundObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForfivSourceFunds(item: ArrayList<SourceOffund>) {
        spinner.id = R.id.OtherSourceOffund
        if (item[0].SOF_CODE != "0") {
            item.add(0, sourceOfFundObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForSourceOfWealth(item: ArrayList<SourceOfWealth>) {
        spinner.id = R.id.SourceOfWealth
        if (item[0].SOW_CODE != "0") {
            item.add(0, sourceOfWealthObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForOtherSourceOfWealth(item: ArrayList<SourceOfWealth>) {
        spinner.id = R.id.otherSourceOfWealth
        if (item[0].SOW_CODE != "0") {
            item.add(0, sourceOfWealthObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForGender(item: ArrayList<Gender>) {
        var obj = Gender()
        resources!!.getString(R.string.select).let {
            obj.GENDER_CODE = "0"
            obj.GENDER_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForSBPCodes(item: ArrayList<SBPCodes>) {
        spinner.id = R.id.SBPCodes
        var obj = SBPCodes()
        resources!!.getString(R.string.select).let {
            obj.SBP_CODE = "0"
            obj.SBP_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForRMCodes(item: ArrayList<RMCodes>) {
        spinner.id = R.id.RMCodes
        var obj = RMCodes()
        resources!!.getString(R.string.select).let {
            obj.C2ACO = "0"
            obj.C2RNM = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForPurposeOfAccount(item: ArrayList<PurposeOfAccount>) {
        var obj = PurposeOfAccount()
        resources!!.getString(R.string.select).let {
            obj.PUR_CODE = "0"
            obj.PUR_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForSundry(item: ArrayList<Sundry>) {
        spinner.id = R.id.Sundry
        var obj = Sundry()
        resources!!.getString(R.string.select).let {
            obj.SUNDRY_CODE = "0"
            obj.SUNDRY_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForConsumerProductBool(item: ArrayList<Bool>) {
        spinner.id = R.id.ConsumerProductBool
        /*   var obj = Bool()
           resources!!.getString(R.string.select).let {
               obj.LB_CODE = "0"
               obj.LB_NAME = it
           }
           item.add(0, obj)*/
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForConsumerProduct(item: ArrayList<ConsumerProduct>) {
        spinner.id = R.id.ConsumerProduct
        var obj = ConsumerProduct()
        resources!!.getString(R.string.select).let {
            obj.PRDT_CODE = "0"
            obj.PRDT_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForRelationship(item: ArrayList<Relationship>) {
        spinner.id = R.id.fivCountryInward1
        if (item[0].RELATION_CODE != "0") {
            item.add(0, relationshipObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForRelationship2(item: ArrayList<Relationship>) {
        spinner.id = R.id.fivCountryInward2
        if (item[0].RELATION_CODE != "0") {
            item.add(0, relationshipObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForRelationship3(item: ArrayList<Relationship>) {
        spinner.id = R.id.fivCountryInward3
        if (item[0].RELATION_CODE != "0") {
            item.add(0, relationshipObj)
        }
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForReasonPrefEDD(item: ArrayList<ReasonPrefEDD>) {
        var obj = ReasonPrefEDD()
        resources!!.getString(R.string.select).let {
            obj.REASON_CODE = "0"
            obj.REASON_DESC = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun setItemForDocType(item: ArrayList<DocumentType>) {
        var obj = DocumentType()
        resources!!.getString(R.string.select).let {
            obj.SEQ_NO = "0"
            obj.DOCID = it
        }
        item.add(0, obj)
        val adapter = ArrayAdapter(context, R.layout.view_spinner_item, R.id.text1, item)
        adapter.setDropDownViewResource(R.layout.view_spinner_list)
        spinner?.adapter = adapter
        invalidate();
    }

    fun remainSelection(size: Int) {
        if (size >= 2) {
//            spinner.setSelection(1)
        }
    }

}
