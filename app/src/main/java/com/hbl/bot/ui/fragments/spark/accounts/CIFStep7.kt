package com.hbl.bot.ui.fragments.spark.accounts


import android.annotation.SuppressLint
import android.content.*
import android.graphics.Color
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.*
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.*
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.activities.spark.CIFRootActivity
import com.hbl.bot.utils.*
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep7.*
import kotlinx.android.synthetic.main.fragment_cifstep7.btBack
import kotlinx.android.synthetic.main.fragment_cifstep7.btNext
import kotlinx.android.synthetic.main.fragment_cifstep7.btRiskRating
import kotlinx.android.synthetic.main.fragment_cifstep7.fiVRiskRating
import kotlinx.android.synthetic.main.fragment_cifstep7.formSectionHeader
import kotlinx.android.synthetic.main.view_form_input.view.*
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.lang.Exception
import java.math.BigInteger
import java.util.concurrent.Executors
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class CIFStep7 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var fivPrioritySP: Spinner? = null
    var AccountTypeCode: String? = null
    var riskStatusET: EditText? = null
    var fivAccountOTypeSP: Spinner? = null
    var fiVAccountTitleET: EditText? = null
    var fiVNameOfPropectiveRemittenceET: EditText? = null
    var fivAccountTypeSP: Spinner? = null
    var fivRelationshipAccountsSP: Spinner? = null
    var fivCurrencySP: Spinner? = null
    var fivInitialSourceSP: Spinner? = null
    var fiVDepositSourceET: EditText? = null
    var nadraVerify = NadraVerify()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep7, container, false)

            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }

    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.setValue(5)
        viewModel.currentFragmentIndex.setValue(0)
        val txt = resources.getString(R.string.account)
        val txt1 = " (1/5 Page)"
        val txt2 = txt + txt1
        formSectionHeader.header_tv_title.text =
            GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as CIFRootActivity).registerReceiver(
                mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST
                )
            );
            header()
            (activity as CIFRootActivity).ivRefresh!!.setOnClickListener(this)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            btRiskRating.setOnClickListener(this)
            header()
            init()
            load()
            /*  if (GlobalClass.isDummyDataTrue) {
                  loadDummyData()
              }*/
            setLengthAndTpe()
            try {
                setConditions()

            } catch (e: Exception) {
            }
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }


    private fun load() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        arguments?.getParcelable<NadraVerify>(Constants.NADRA_RESPONSE_DATA)?.let {
            nadraVerify = it
        }

        (activity as CIFRootActivity).customerInfo.FIRST_NAME.let { t ->
            fiVAccountTitleET?.setText(
                t
            )
        }
        (activity as CIFRootActivity).customerAccounts.INIT_DEPOSIT.let { t ->
//            if (t.isNullOrEmpty()) {
//                fiVDepositSourceET?.setText(
//                    "0"
//                )
//            } else {
            fiVDepositSourceET?.setText(
                t
            )
//            }
        }


        if ((activity as CIFRootActivity).sharedPreferenceManager.lovOperationalType.isNullOrEmpty()) {
            callAccountOperationsType()

        } else {
            setAccountOperationsType((activity as CIFRootActivity).sharedPreferenceManager.lovOperationalType)
        }


    }

    private fun setAccountOperationsType(it: ArrayList<AccountOperationsType>) {
        try {
            (activity as CIFRootActivity).accountOperationTypeList = it
            fivAccountOType.setItemForAccountOperationsType(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovOperationalType = it
//        if (checkMinorAccountCondition()) {
            (activity as CIFRootActivity).customerAccounts.ACCT_OPER_TYPE_DESC.let { operationaltype ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromOpTypeIndex(
                    it,
                    operationaltype
                ).let {
                    fivAccountOTypeSP?.setSelection(it!!)
                    if (it == 0) {
                        checkMinorAccountCondition()
                    }
                }
            }
//        }
            if ((activity as CIFRootActivity).sharedPreferenceManager.lovPriority.isNullOrEmpty()) {
                callPriority()
            } else {
                setPriority((activity as CIFRootActivity).sharedPreferenceManager.lovPriority)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun checkMinorAccountCondition(): Boolean {
        return if ((activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
            fivAccountOTypeSP?.setSelection(3)
            false
        } else if ((activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE != "A7") {
            fivAccountOTypeSP?.setSelection(1)
            false
        } else {
            true
        }
    }

    fun callPriority() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.PRIORITY_IDENTIFIER
        HBLHRStore.instance?.getPriority(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : PriorityCallBack {
                override fun PrioritySuccess(response: PriorityResponse) {
                    response.data?.let {
                        setPriority(it)
                    };
                }

                override fun PriorityFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setPriority(it: ArrayList<Priority>) {
        try {
            (activity as CIFRootActivity).priorityList = it
            fivPriority.setItemForPriority(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovPriority = it
            (activity as CIFRootActivity).customerAccounts.PRITY_DESC.let { currDesc ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPriorityIndex(
                    it,
                    currDesc
                ).let {
                    fivPrioritySP?.setSelection(it!!)
                }

            }

//            if ((activity as CIFRootActivity).sharedPreferenceManager.lovAccountType.isNullOrEmpty()) {
            getAccountType()
/*
            } else {
                setAccountType((activity as CIFRootActivity).sharedPreferenceManager.lovAccountType)
            }
*/
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setAccountType(it: ArrayList<AccountType>) {
        try {
            (activity as CIFRootActivity).accountTypeList = it
            fivAccountType.setItemForAccountType(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovAccountType = it
            (activity as CIFRootActivity).customerAccounts.ACCTTYPEDESC.let { accttype ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromAccTypeIndex(
                    it,
                    accttype
                ).let {
                    fivAccountTypeSP?.setSelection(it!!)
                }

            }
            if ((activity as CIFRootActivity).sharedPreferenceManager.lovCurrency.isNullOrEmpty()) {
                callCurrency("PKR")
            } else {
                setCurrency((activity as CIFRootActivity).sharedPreferenceManager.lovCurrency)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun callRelationship() {
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.RELATIONSHIP_IDENTIFIER
        HBLHRStore.instance?.getRelationship(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : RelationshipCallBack {
                override fun RelationshipSuccess(response: RelationshipResponse) {
                    response.data?.let {
                        setRelationsip(it)
                    };
                }

                override fun RelationshipFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) {   callRelationship() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setRelationsip(it: ArrayList<Relationship>) {
        try {
            (activity as CIFRootActivity).relationshipList = it
            fivRelationshipAccounts.setItemForRelationship(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovRelationship = it
            (activity as CIFRootActivity).customerAccounts.RELATIONSHIP_DESC.let { relation ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromRelationshipByDescIndex(
                    it,
                    relation
                ).let {
                    fivRelationshipAccountsSP?.setSelection(it!!)

                }
            }
            (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.NAME_PROSPECTIVE_REMITTER.let { t ->
                fiVNameOfPropectiveRemittenceET?.setText(
                    t
                )
            }
            (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun setCurrency(it: ArrayList<Currency>) {
        try {
//        fivCurrencySP?.setSelection(1)
            (activity as CIFRootActivity).currencyList = it
            fivCurrency.setItemForCurrency(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovCurrency = it


            (activity as CIFRootActivity).customerAccounts.CURRENCY_DESC.let { currDesc ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCurrencyIndex(
                    it,
                    currDesc
                ).let {
                    fivCurrencySP?.setSelection(it!!)
                    fivCurrencySP?.setSelection(1)
                }

            }


            if ((activity as CIFRootActivity).sharedPreferenceManager.lovDepositType.isNullOrEmpty()) {
                callDepositType()

            } else {
                setDepositType((activity as CIFRootActivity).sharedPreferenceManager.lovDepositType)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }

    }

    private fun setDepositType(it: ArrayList<DepositType>) {
        try {
            (activity as CIFRootActivity).depositTypeList = it
            fivInitialSource.setItemForDepositType(it)
            (activity as CIFRootActivity).sharedPreferenceManager.lovDepositType = it
            (activity as CIFRootActivity).customerAccounts.INITDEPSOURCEDESC.let { depDesc ->
                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromDepTypeIndex(
                    it,
                    depDesc
                ).let {
                    fivInitialSourceSP?.setSelection(it!!)
                }
            }
            (activity as CIFRootActivity).globalClass?.hideLoader()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }


    private fun setConditions() {
        //added by Abdullah on the request of Saqib Bhai (If account no is available so Short Title and Account type will be disable)
        if (!(activity as CIFRootActivity).sharedPreferenceManager.customerAccount.ACCT_NO.isNullOrEmpty()) {
            (activity as CIFRootActivity).globalClass?.setDisbaled(fiVAccountTitleET!!)
            (activity as CIFRootActivity).globalClass?.setDisbaled(fiVAccountTitle!!)
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivAccountTypeSP!!)
            (activity as CIFRootActivity).globalClass?.setDisbaled(fivAccountType!!)
        } else {
            (activity as CIFRootActivity).globalClass?.setEnabled(fiVAccountTitleET!!, true)
            (activity as CIFRootActivity).globalClass?.setEnabled(fiVAccountTitle!!, true)
            (activity as CIFRootActivity).globalClass?.setEnabled(fivAccountTypeSP!!, true)
            (activity as CIFRootActivity).globalClass?.setEnabled(fivAccountType!!, true)
        }


        fivAccountTypeSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long,
            ) {
                try {
                    (activity as CIFRootActivity).globalClass?.findSpinnerCodeFromAccountType(
                        (activity as CIFRootActivity).accountTypeList,
                        fivAccountTypeSP?.selectedItem.toString()
                    ).let {
                        if (it != resources!!.getString(R.string.select) && it != "0") {
                            (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!)
                            // when select there is no item selected "Choose an item"  <--- hint
                            var dualNationality =
                                (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.DUAL_NATIONALITY
                            var docType =
                                (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_TYPE
                            var COUNTRY_OF_RES =
                                (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES
                            var gender =
                                (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.GENDER
                            var accountType =
                                (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromAccountTypeCode(
                                    (activity as CIFRootActivity).sharedPreferenceManager.lovAccountType,
                                    fivAccountTypeSP?.selectedItem.toString()
                                )
                            //TODO: This account type is not allowed for dual national customers
                            if (accountType.equals(
                                    "7L",
                                    true
                                ) && COUNTRY_OF_RES != "PK"
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.error_nisa_res_of_country),
                                    1
                                )

                            }else if (accountType.equals(
                                    "7L",
                                    true
                                )  && gender != "F"
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.error_nisa_gender),
                                    1
                                )

                            }else if (accountType.equals(
                                    "7L",
                                    true
                                )   && (docType != "01" && docType != "02" && docType != "04" && docType != "05")
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.error_nisa_doctype),
                                    1
                                )
                            }  else if ((dualNationality == "1" && COUNTRY_OF_RES != "PK") && (accountType.equals(
                                    "7R",
                                    true
                                ) || accountType.equals("7S", true))
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.dualNationalityCORAccErr),
                                    1
                                )

                            } else if ((dualNationality == "1") && (accountType.equals(
                                    "7R",
                                    true
                                ) || accountType.equals("7S", true))
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.dualNationalityAccErr),
                                    1
                                )

                            } else if ((COUNTRY_OF_RES != "PK") && (accountType.equals(
                                    "7R",
                                    true
                                ) || accountType.equals("7S", true))
                            ) {
                                fivAccountTypeSP?.setSelection(0)
                                ToastUtils.normalShowToast(
                                    activity,
                                    resources.getString(R.string.corAccErr),
                                    1
                                )

                            } else {

                                //TODO: If customer second(Dual)YES nationality yes then Account type will not be selected "7R", "7S"
                                if (accountType.equals("7S", true) || accountType.equals(
                                        "7V",
                                        true
                                    )
                                ) {
                                    callRelationship()
                                    fiVNameOfPropectiveRemittence!!.visibility = View.VISIBLE
                                    fiVNameOfPropectiveRemittenceET!!.visibility = View.VISIBLE
                                    fivRelationshipAccounts!!.visibility = View.VISIBLE
                                    fivRelationshipAccountsSP!!.visibility = View.VISIBLE

                                } else {
                                    fiVNameOfPropectiveRemittenceET?.setText("")
                                    fivRelationshipAccountsSP?.setSelection(0)
                                    fiVNameOfPropectiveRemittenceET!!.visibility = View.GONE
                                    fiVNameOfPropectiveRemittence!!.visibility = View.GONE
                                    fivRelationshipAccounts!!.visibility = View.GONE
                                    fivRelationshipAccountsSP!!.visibility = View.GONE
                                }


                                //TODO: WHen ADA and ADRA select initial deposit will not be mantory field
                                if (accountType.equals("7R", true) || accountType.equals(
                                        "7S",
                                        true
                                    )
                                ) {
                                    fiVDepositSource.mandateInput.text = ""
                                } else {
                                    fiVDepositSource.mandateInput.text = "*"
                                }

                                if (fivAccountTypeSP!!.selectedItem.toString()
                                        .contains(
                                            "Nisa",
                                            true
                                        ) && (activity as CIFRootActivity).customerInfo.GENDER == "M"
                                ) {
                                    ToastUtils.normalShowToast(
                                        activity,
                                        resources!!.getString(R.string.nisaErrorWhenGenderIsMale),
                                        1
                                    )
                                    fivAccountTypeSP?.setSelection(0)
                                } else {
                                    AccountTypeCode = it.toString()
                                    var verifyAccount = VerifyAccountRequest()
//                                verifyAccount.cnic = "1230756789233"
                                    verifyAccount.cnic =
                                        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_NO
                                    verifyAccount.ACCT_TYPE = it.toString()
                                    verifyAccount.identifier =
                                        Constants.TYPE_ACCOUNT_VALIDATION_IDENTIFIER
                                    HBLHRStore.instance?.VerifyAccount(
                                        RetrofitEnums.URL_HBL,
                                        verifyAccount,
                                        object : VerifyAccountCallBack {
                                            override fun VerifyAccountSuccess(response: VerifyAccountResponse) {
                                                if (response.status == "00") {
                                                    response.data?.getOrNull(0)?.PRIORITY_FLAG.let { check ->
                                                        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.PRIORITY.let {
                                                            if (check == true && (it == "1" || it == "2")) {
                                                                fivPrioritySP?.visibility =
                                                                    View.VISIBLE
                                                                fivPriority?.visibility =
                                                                    View.VISIBLE
                                                            } else {
                                                                fivPrioritySP?.visibility =
                                                                    View.GONE
                                                                fivPriority?.visibility = View.GONE
                                                            }
                                                        }
                                                    }
                                                    if (position != 0 && (activity as CIFRootActivity).accountTypeList.isNotEmpty()) {
                                                        callCurrency(
                                                            (activity as CIFRootActivity).accountTypeList.get(
                                                                position
                                                            ).ACCT_CCY.toString()
                                                        )
                                                    }
                                                    (activity as CIFRootActivity).globalClass?.setEnabled(
                                                        btNext!!,
                                                        true
                                                    )
                                                } else {
                                                    ToastUtils.normalShowToast(
                                                        context,
                                                        response.message.toString(), 1
                                                    )
                                                    (activity as CIFRootActivity).globalClass?.setDisbaled(
                                                        btNext!!
                                                    )
                                                }
                                            }

                                            override fun VerifyAccountFailure(response: BaseResponse) {
                                                (activity as CIFRootActivity).globalClass?.hideLoader()
//                                                Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                                                ToastUtils.normalShowToast(
                                                    context,
                                                    response.message.toString(), 1
                                                )
                                            }

                                        })


                                    if ((AccountTypeCode == "GW" || AccountTypeCode == "EW"
                                                || AccountTypeCode == "IW" || AccountTypeCode == "GQ"
                                                || AccountTypeCode == "GR" || AccountTypeCode == "GS") && (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.CUST_INFO.get(
                                            0
                                        ).COUNTRY_OF_RES != "PK"
                                    ) {
                                        fivAccountTypeSP?.setSelection(0)
                                        ToastUtils.normalShowToast(
                                            context,
                                            "For Nisa Country should be pakistan", 1
                                        )
                                    }
                                }
                            }
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
            }

        }
        fivInitialSourceSP?.setOnItemSelectedListener(object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if (pos == 0) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(fiVDepositSourceET!!)
                    } else {
                        (activity as CIFRootActivity).globalClass?.setEnabled(
                            fiVDepositSourceET!!,
                            true
                        )
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        })
    }

    private fun setLengthAndTpe() {
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fiVAccountTitleET!!,
            15,
            Constants.INPUT_TYPE_ALPHA_BLOCK
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fiVDepositSourceET!!,
            13,
            Constants.INPUT_TYPE_NUMBER
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fiVNameOfPropectiveRemittenceET!!,
            35,
            Constants.INPUT_TYPE_ALPHA_BLOCK
        )
    }

    fun callCIF() {
        var cifRequest = CIFRequest()
        cifRequest.payload = Constants.PAYLOAD
        cifRequest.scnic = GlobalClass.id_do
        HBLHRStore.instance?.getCIF(RetrofitEnums.URL_HBL, cifRequest, object : CIFCallBack {
            override fun CIFSuccess(response: AofAccountInfoResponse) {

//
//                SparkDataSetupInSp(activity as CIFRootActivity, response)
//                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo =
//                    response.data.get(0)
                callAccountOperationsType()
//                GlobalClass.FilledEDD = true
//

            }

            override fun CIFFailure(response: BaseResponse) {
                ToastUtils.normalShowToast(
                    context,
                    response.message.toString(), 1
                )
//                Utils.failedAwokeCalls((activity as CIFRootActivity)) { callCIF() }
                (activity as CIFRootActivity).globalClass?.hideLoader()

            }
        })

    }

    fun callAccountOperationsType() {
        try {
            var lovRequest = LovRequest()
            lovRequest.identifier = Constants.ACCOUNT_OPERATIONS_TYPE_IDENTIFIER
            HBLHRStore.instance?.getAccountOperationsType(
                RetrofitEnums.URL_HBL,
                lovRequest,
                object : AccountOperationsTypeCallBack {
                    override fun AccountOperationsTypeSuccess(response: AccountOperationsTypeResponse) {
                        response.data?.let {
                            setAccountOperationsType(it)
//                        var list = ArrayList<AccountOperationsType>()
//                        for (i in 0 until it.size) {
//                            if (it[i].ACCT_OPER_TYPE_DESC == "Individual Account" || it[i].ACCT_OPER_TYPE_DESC == "Joint account" || it[i].ACCT_OPER_TYPE_DESC == "Minor Account") {
//                                list.add(it[i])
//                            }
//                        }
//                        it.removeAt(3)


                        };

                    }

                    override fun AccountOperationsTypeFailure(response: BaseResponse) {
//                        Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                        ToastUtils.normalShowToast(activity, response.message, 1)
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                    }

                })
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    fun getAccountType() {
        var lovRequest = LovRequest()
        if (((activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_CODE == "01")) {
            lovRequest.condition = true
        } else if (((activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_CODE == "02")) {
            lovRequest.condition = false
        }
        lovRequest.conditionname = "PURP_OF_CIF"
        lovRequest.conditionvalue =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.PURP_OF_CIF_CODE
        lovRequest.BRANCH_CODE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getBRANCHCODE()!!
        lovRequest.CUST_SEGMENT =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        lovRequest.identifier = Constants.ACCOUNT_TYPE_IDENTIEFIER
        var gson = Gson().toJson(lovRequest)
        HBLHRStore.instance?.getAccountType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : AccountTypeCallBack {
                override fun AccountTypeSuccess(response: AccountTypeResponse) {
                    response.data?.let {
                        setAccountType(it)
                    };


                }

                override fun AccountTypeFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    Toast.makeText(
                        (activity as CIFRootActivity).globalClass,
                        response.message,
                        Toast.LENGTH_SHORT
                    ).show()
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callCurrency(curreny: String) {
        var lovRequest = CurrenyLovRequest()
        lovRequest.find.CURR = curreny
        lovRequest.identifier = Constants.CURRENCY_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(lovRequest)
        HBLHRStore.instance?.getCurrency(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CurrencyCallBack {
                override fun CurrencySuccess(response: CurrencyResponse) {
                    response.data?.let {

                        setCurrency(it)

                    };


                }

                override fun CurrencyFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }

    fun init() {
        fivAccountOTypeSP = fivAccountOType.getSpinner(R.id.accountOperationOfTypeID)
        fivPrioritySP = fivPriority.getSpinner(R.id.fivPriority)
        (activity as CIFRootActivity).globalClass?.setDisbaled(fivAccountOTypeSP!!)
        riskStatusET = fiVRiskRating.getTextFromEditText(R.id.riskStatusID)
        fiVAccountTitleET = fiVAccountTitle.getTextFromEditText(R.id.fiVAccountTitle1)
        fiVNameOfPropectiveRemittenceET =
            fiVNameOfPropectiveRemittence.getTextFromEditText(R.id.fiVNameOfPropectiveRemittence)
        fivAccountTypeSP = fivAccountType.getSpinner()
        fivRelationshipAccountsSP = fivRelationshipAccounts.getSpinner()
        fivCurrencySP = fivCurrency.getSpinner(R.id.fivCurrencyID)
        fivInitialSourceSP = fivInitialSource.getSpinner()
        fiVDepositSourceET = fiVDepositSource.getTextFromEditText(R.id.fiVDepositSource)
        (activity as CIFRootActivity).globalClass?.setDisbaled(fivCurrencySP!!)
        (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!)
    }

    fun callDepositType() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.DEPOSIT_TYPE_IDENTIFIER
        HBLHRStore.instance?.getDepositType(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : DepositTypeCallBack {
                override fun DepositTypeSuccess(response: DepositTypeResponse) {
                    response.data?.let {
                        setDepositType(it)
                    };

                }

                override fun DepositTypeFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }

            })
    }


    fun loadDummyData() {
//        fiVAccountTitleET?.setText("Syed Muhammad Abdullah")
//        fiVDepositSourceET?.setText("5000")


    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btRiskRating -> {
                try {
                    (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                    Executors.newSingleThreadExecutor().execute(Runnable {
                        setRiskAllModelsInAOFRequestModel()
                    })
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.btNext -> {
                try {
                    var accountType = ""
                    (activity as CIFRootActivity).globalClass?.findSpinnerCodeFromAccountType(
                        (activity as CIFRootActivity).accountTypeList,
                        fivAccountTypeSP?.selectedItem.toString()
                    ).let {
                        if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {
                            accountType = it.toString()
                        }
                    }
                    if (validation(accountType)) {
                        if (accountCode(accountType)) {
                            (activity as CIFRootActivity).globalClass?.setDisbaled(btNext!!, false)
                            (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                            Executors.newSingleThreadExecutor().execute(Runnable {
                                saveAndNext()
                                setAllModelsInAOFRequestModel()
                                (activity as CIFRootActivity).runOnUiThread {
                                    (activity as CIFRootActivity).globalClass?.setEnabled(
                                        btNext!!,
                                        true
                                    )
                                }
                            })
                        }

                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }

            }
            /*    } else {
                ToastUtils.normalShowToast(activity, "Please check the risk rating first.")
            }*/
            R.id.btBack -> accountBackpage()

//            R.id.btBack -> findNavController().popBackStack()
        }

    }

    private fun accountCode(accountType: String): Boolean {
        (activity as CIFRootActivity).globalClass?.findSpinnerCodeFromAccountType(
            (activity as CIFRootActivity).accountTypeList,
            fivAccountTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                AccountTypeCode = it.toString()

            }
        }

        if (AccountTypeCode == "GW") {
            if ((accountType != "7R" && accountType != "7S") && !TextUtils.isEmpty(
                    fiVDepositSourceET?.text.toString()
                ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() >= 1000
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    activity,
                    "Minimum Initial Deposit should be 1,000 ",
                    1
                )
                return false
            }
        } else if (AccountTypeCode == "EW") {

            if ((accountType != "7R" && accountType != "7S") && !TextUtils.isEmpty(
                    fiVDepositSourceET?.text.toString()
                ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() >= 5000
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    activity,
                    "Minimum Initial Deposit should be 5,000 ",
                    1
                )
                return false
            }

        } else if (AccountTypeCode == "IW") {
            if ((accountType != "7R" && accountType != "7S") && !TextUtils.isEmpty(
                    fiVDepositSourceET?.text.toString()
                ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() >= 5000
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    activity,
                    "Minimum Initial Deposit should be 5,000 ",
                    1
                )
                return false
            }

        } else if (AccountTypeCode == "GQ" || AccountTypeCode == "GR" || AccountTypeCode == "GS") {
            if ((accountType != "7R" && accountType != "7S") && !TextUtils.isEmpty(
                    fiVDepositSourceET?.text.toString()
                ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() >= 1000
            ) {
                return true
            } else {
                ToastUtils.normalShowToast(
                    activity,
                    "Minimum Initial Deposit should be 100 ",
                    1
                )
                return false
            }

        } else {
            return true
        }
    }

    private fun validation(accountType: String): Boolean {
        var depositSource = 0
        if (fivAccountOTypeSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select operational type",
                1
            )
            return false
        } else if (TextUtils.isEmpty(fiVAccountTitleET?.text.toString())) {
            ToastUtils.normalShowToast(
                activity,
                "Please fill short title",
                1
            )
            return false
        } else /*if (fivPrioritySP?.selectedItemPosition == 0 && fivPrioritySP?.isVisible == true) {
            ToastUtils.normalShowToast(
                activity,
                "Please Select Priority First",
                1
            )
            return false
        } else*/ if (fivAccountTypeSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select account type",
                1
            )
            return false
        } else if (fiVNameOfPropectiveRemittenceET?.visibility == View.VISIBLE && fiVNameOfPropectiveRemittenceET?.text.isNullOrEmpty()) {
            fiVNameOfPropectiveRemittenceET?.error =
                resources.getString(R.string.fiVNameOfPropectiveRemittenceErr)
            fiVNameOfPropectiveRemittenceET?.requestFocus()
            return false
        } else if (fivRelationshipAccountsSP?.visibility == View.VISIBLE && fivRelationshipAccountsSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select RELATIONSHIP WITH PROSPECTIVE REMITTER",
                1
            )
            return false
        } else if (fivInitialSourceSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                "Please select initial source type",
                1
            )
            return false
        } else if ((accountType != "7R" && accountType != "7S") && TextUtils.isEmpty(
                fiVDepositSourceET?.text.toString()
            )
        ) {
            ToastUtils.normalShowToast(
                activity,
                "Please fill deposit source",
                1
            )
            return false
        } else if ((accountType != "7R" || accountType != "7S") && !TextUtils.isEmpty(
                fiVDepositSourceET?.text.toString()
            ) && BigInteger(fiVDepositSourceET?.text.toString()).toInt() == 0
        ) {
            ToastUtils.normalShowToast(
                activity,
                "Initial deposit should not be 0",
                1
            )
            return false
        } else if ((AccountTypeCode == "GW" || AccountTypeCode == "EW"
                    || AccountTypeCode == "IW" || AccountTypeCode == "GQ"
                    || AccountTypeCode == "GR" || AccountTypeCode == "GS") && (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.COUNTRY_OF_RES != "PK"
        ) {
            ToastUtils.normalShowToast(context, "For NISA country should be pakistan", 1)
            return false
        } else
            return true
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                accountBackpage()
                true
            }
            false
        }
    }

    fun accountBackpage() {
        val bundle = Bundle()
        bundle.putParcelable(Constants.NADRA_RESPONSE_DATA, nadraVerify)
        if ((activity as CIFRootActivity).backInt == 2) {
            if (findNavController().currentDestination?.id == R.id.CIFStep7)
                findNavController().navigate(R.id.action_CIFStep7_to_CIFStep1_5, bundle)
        } else if (!(activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().equals(
                "High",
                true
            )
        ) {
            if (findNavController().currentDestination?.id == R.id.CIFStep7)
                findNavController().navigate(R.id.action_CIFStep7_to_CIFStep5_7)
        } else {
            if (findNavController().currentDestination?.id == R.id.CIFStep7)
                findNavController().navigate(R.id.action_CIFStep7_to_CIFStep6_11)
        }
    }


    fun saveAndNext() {
        var accountType = ""
        (activity as CIFRootActivity).globalClass?.findSpinnerCodeFromAccountType(
            (activity as CIFRootActivity).accountTypeList,
            fivAccountTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {
                accountType = it.toString()
            }
        }
//        (activity as CIFRootActivity).cnicNumber.let {
//            (activity as CIFRootActivity).customerAccounts.ID_DOC_NO = it!!
//        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromAccountOperationTypeCode(
            (activity as CIFRootActivity).accountOperationTypeList,
            fivAccountOTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select therCe is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.ACCTTYPEDESC =
                    fivAccountOTypeSP?.selectedItem.toString()
                (activity as CIFRootActivity).customerAccounts.ACCT_TYPE = it.toString()
            }
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromAccountOperationTypeCode(
            (activity as CIFRootActivity).accountOperationTypeList,
            fivAccountOTypeSP?.selectedItem.toString()
        ).let {

            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select therCe is no item selected "Choose an item"  <--- hint
                //TODO: when Customer Segment Minor then accoint operational type select 3 and Minor Account
                if ((activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
                    (activity as CIFRootActivity).customerAccounts.ACCT_SNG_JNT_DESC =
                        "Minor Account"
                    (activity as CIFRootActivity).customerAccounts.ACCT_SNG_JNT = "3"
                } else {
                    (activity as CIFRootActivity).customerAccounts.ACCT_SNG_JNT_DESC =
                        fivAccountOTypeSP?.selectedItem.toString()
                    (activity as CIFRootActivity).customerAccounts.ACCT_SNG_JNT = it.toString()
                }
                (activity as CIFRootActivity).customerAccounts.ACCTTYPEDESC = it.toString()

//                (activity as CIFRootActivity).customerAccounts.ACCT_SNG_JNT_DESC = fivAccountOTypeSP?.selectedItem.toString()
//                (activity as CIFRootActivity).customerAccounts.ACCT_SNG_JNT = it.toString()

                (activity as CIFRootActivity).customerAccounts.ACCT_OPER_TYPE_CODE = it.toString()
                (activity as CIFRootActivity).customerAccounts.ACCT_OPER_TYPE_DESC =
                    fivAccountOTypeSP?.selectedItem.toString()
            }
        }


//for oper code
//        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromAccountOperationTypeCode(
//            (activity as CIFRootActivity).accountOperationTypeList,
//            fivAccountOTypeSP?.selectedItem.toString()
//        ).let {
//            if (it != resources!!.getString(R.string.select)) {// when select therCe is no item selected "Choose an item"  <--- hint
//                (activity as CIFRootActivity).customerAccounts.ACCT_OPER_TYPE_CODE =
//                    it.toString()
//                Log.i("abcde", it.toString())
//            }
//        }

        fiVAccountTitleET?.text.toString()
            .let { (activity as CIFRootActivity).customerAccounts.TITLE_OF_ACCT = it }


        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromAccountTypeCode(
            (activity as CIFRootActivity).accountTypeList,
            fivAccountTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.ACCT_TYPE = it.toString()
                (activity as CIFRootActivity).customerAccounts.ACCTTYPEDESC =
                    fivAccountTypeSP?.selectedItem.toString()
            }
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromRelationshipCode(
            (activity as CIFRootActivity).relationshipList,
            fivRelationshipAccountsSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.RELATIONSHIP_CODE = it.toString()
                (activity as CIFRootActivity).customerAccounts.RELATIONSHIP_DESC =
                    fivRelationshipAccountsSP?.selectedItem.toString()
            }
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromPriorityCode(
            (activity as CIFRootActivity).priorityList,
            fivPrioritySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select therCe is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.PRITY_DESC =
                    fivPrioritySP?.selectedItem.toString()
                (activity as CIFRootActivity).customerAccounts.PRITY_CODE = it.toString()
                if (it == "P" || it == "V") {
                    (activity as CIFRootActivity).customerAccounts.IS_PRIORITY = "1"
                } else {
                    (activity as CIFRootActivity).customerAccounts.IS_PRIORITY = "0"
                }
            } else {
                (activity as CIFRootActivity).customerAccounts.IS_PRIORITY = "0"
            }
        }

        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromAccountIslamic(
            (activity as CIFRootActivity).accountTypeList,
            fivAccountTypeSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.CONVENTIONAL_ISLAMIC =
                    it.toString()
                if (it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                    (activity as CIFRootActivity).customerAccounts.CONVENTIONAL_ISLAMIC =
                        it.toString()

                }
            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCurrencyOfFundCode(
            (activity as CIFRootActivity).currencyList,
            fivCurrencySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.CURRENCY = it.toString()


            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromCurrencyOfCode(
            (activity as CIFRootActivity).currencyList,
            fivCurrencySP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.CURRENCY_DESC = it.toString()
                (activity as CIFRootActivity).customerAccounts.ACCT_CCY = it.toString()

            }
        }
        (activity as CIFRootActivity).globalClass?.findSpinnerPositionFromDepositTypeCode(
            (activity as CIFRootActivity).depositTypeList,
            fivInitialSourceSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as CIFRootActivity).customerAccounts.INIT_DEP_SOURCE = it.toString()
                (activity as CIFRootActivity).customerAccounts.INITDEPSOURCEDESC = fivInitialSourceSP?.selectedItem.toString()
            }
        }

        (activity as CIFRootActivity).customerInfo.ID_DOCUMENT_NO.let {
            (activity as CIFRootActivity).customerAccounts.ID_DOC_NO
        }

        fiVDepositSourceET?.text.toString()
            .let {
                if ((it == "" || it == "0") && (accountType == "7R" || accountType == "7S")) {
                    //TODO: If user do not add the initials deposit in 7R or 7S case then value remain insert as "0"
                    (activity as CIFRootActivity).customerAccounts.INIT_DEPOSIT = "0"
                    Log.i("CheckValuessss", "$accountType $it")
                } else {
                    Log.i("CheckValuessss", "other $it")
                    (activity as CIFRootActivity).customerAccounts.INIT_DEPOSIT = it
                }

            }
        fiVNameOfPropectiveRemittenceET?.text.toString()
            .let {
                (activity as CIFRootActivity).customerAccounts.NAME_PROSPECTIVE_REMITTER = it
            }
        /*riskStatusET?.text.toString().let { }*/
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount =
            (activity as CIFRootActivity).customerAccounts
    }

    fun setRiskAllModelsInAOFRequestModel() {

        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }

        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }

        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }

        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        aofRiskDataAlign()
    }

    fun aofRiskDataAlign() {
        (activity as CIFRootActivity).aofAccountInfoRequest.CHANNEL = "2"
        (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
            ?.get(0)?.itemDescription.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID = it!!
            }
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as CIFRootActivity).etbntbFLAG
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as CIFRootActivity).REMEDIATE
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID()
                .toString()
        (activity as CIFRootActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE()
                .toString()
        //TODO: set aofAccountInfo.NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME =
                (activity as CIFRootActivity).fullName
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }//        aofAccountInfo.RISK_RATING = (activity as CIFRootActivity).riskRating.toString()
//        aofAccountInfo.RISK_RATING_TOTAL = (activity as CIFRootActivity).riskRatingTotal!!
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            //TODO: set BRANCH_CODE
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH = it!!
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        callToRiskRatingResponse((activity as CIFRootActivity).aofAccountInfoRequest)
    }

    fun callToRiskRatingResponse(aofAccountInfo: Data) {
        var aofAccountInfoRequest = AofAccountInfoRequest()
        aofAccountInfoRequest.identifier = Constants.AOF_SUBMIT_IDENTIFIER
        aofAccountInfoRequest.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(aofAccountInfoRequest)
        HBLHRStore.instance?.postRiskRating(
            RetrofitEnums.URL_HBL,
            aofAccountInfoRequest, object : CustomerRiskRatingCallBack {
                @SuppressLint("WrongConstant")
                override fun CustomerRiskRatingSuccess(response: RiskRatingResponse) {
                    try {
                        ToastUtils.normalShowToast(activity, response.message, 2)
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                        response.data?.get(0)?.RISK_RATING?.let {
                            (activity as CIFRootActivity).riskRating = it
                            riskCheck(it)
                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun CustomerRiskRatingFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun riskCheck(risk: String?) {
        riskStatusET?.isEnabled = false
        if (risk.equals("Medium", true)) {
            riskStatusET?.setText("Medium")
            riskStatusET?.setTextColor(Color.parseColor("#008577"));
        } else {
            riskStatusET?.setText("High")
            riskStatusET?.setTextColor(Color.parseColor("#D53F45"));
        }
    }

    fun setAllModelsInAOFRequestModel() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        aofDataAlign()
    }

    fun aofDataAlign() {
        (activity as CIFRootActivity).aofAccountInfoRequest.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                ?.get(0)
                ?.itemDescription.let {
                    (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as CIFRootActivity).etbntbFLAG
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as CIFRootActivity).REMEDIATE
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID()
                .toString()
        (activity as CIFRootActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE()
                .toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME =
                (activity as CIFRootActivity).fullName
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as CIFRootActivity).riskRating.toString()
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }
        if ((activity as CIFRootActivity).backInt == 2) {
            (activity as CIFRootActivity).sharedPreferenceManager.appStatus.Acct.let {
                (activity as CIFRootActivity).globalClass?.appStatus = it.toString()
            }
            //TODO: set WORK_FLOW_ACCOUNT
            (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "2"
            (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC = "ACCOUNT"
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.appStatus.CIFAcct.let {
                (activity as CIFRootActivity).globalClass?.appStatus = it.toString()
            }
            //TODO: set WORK_FLOW_CIF_AND_ACCOUNT
            (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "3"
            (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC =
                "CIF AND ACCOUNT"
        }
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.REG_NAME
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF((activity as CIFRootActivity).aofAccountInfoRequest)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        request.payload = Constants.PAYLOAD
        if ((activity as CIFRootActivity).backInt == 2) {
            request.identifier = Constants.SUBMIT_DRAFT_IDENTIFIER

        } else {
            request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER

        }
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        if (findNavController().currentDestination?.id == R.id.CIFStep7)
                            findNavController().navigate(R.id.action_CIFStep7_to_CIFStep7_2)
                        (activity as CIFRootActivity).recyclerViewSetup()
                        (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
//                    callRM()
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
//                (activity as CIFRootActivity).globalClass?.setEnabled(btNext!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}