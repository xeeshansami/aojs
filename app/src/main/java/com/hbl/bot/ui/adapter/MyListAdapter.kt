package com.hbl.bot.ui.adapter

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import com.hbl.bot.R
import com.hbl.bot.network.models.response.baseRM.Onboarding

class MyListAdapter(private val context: Activity, private val items: ArrayList<Onboarding>?) :
    ArrayAdapter<Onboarding>(context, R.layout.row_meeting, items!!) {

    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val rowView = inflater.inflate(R.layout.row_meeting, null, true)

        val NameTextView: TextView = view!!.findViewById(R.id.txtName)
        val ContactTextView: TextView = view.findViewById(R.id.txtContactNo)
        val TrackingNoTextView: TextView = view.findViewById(R.id.txtTrackingNo)
        val DateTextView: TextView = view.findViewById(R.id.txtDate)
        val AddressTextView: TextView = view.findViewById(R.id.txt_Address)


        var mItem: Onboarding = this.items!![position]
//        imageView.setImageDrawable(mCtx.resources.getDrawable(mItem.img))
        NameTextView.text = mItem.NAME
        ContactTextView.text = mItem.CONTACT_NO
        TrackingNoTextView.text = mItem.TRACKING_ID
        DateTextView.text = mItem.CALL_DATE_TIME + " " + mItem.MEET_DATE_TIME
        AddressTextView.text = mItem.ADDRESS



        return view
    }
}