package com.hbl.bot.ui.activities

import android.content.ActivityNotFoundException
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.showRecordsCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.showRecordsRequest
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.showRecordsResponse
import com.hbl.bot.network.models.response.baseRM.Rows
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.ui.adapter.ShowHawRecordsAdapter
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import kotlinx.android.synthetic.main.activity_show_records.*
import kotlinx.android.synthetic.main.activity_show_records.backBtn
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

class HawShowRecords : AppCompatActivity(), View.OnClickListener {
    var tid = ""
    var doid = ""
    var rowsList = ArrayList<Rows>()
    var rvRecords: RecyclerView? = null
    var skip = "0"
    var sharedPreferenceManager: SharedPreferenceManager = SharedPreferenceManager()
    private lateinit var showRecordsAdapter: ShowHawRecordsAdapter
    val globalClass = GlobalClass.applicationContext!!.applicationContext as GlobalClass
    fun hideNavigationBar() {
        try {
            window.decorView.apply {
                // Hide both the navigation bar and the status bar.
                // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
                // a general rule, you should design your app to hide the status bar whenever you
                // hide the navigation bar.
                systemUiVisibility =
                    View.SYSTEM_UI_FLAG_HIDE_NAVIGATION or View.SYSTEM_UI_FLAG_FULLSCREEN
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as Exception))
        }
    }

    override fun onResume() {
        super.onResume()
//        hideNavigationBar()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        try {
            super.onCreate(savedInstanceState)
            Thread.setDefaultUncaughtExceptionHandler(LoggingExceptionHandler(globalClass as GlobalClass));
            setContentView(R.layout.activity_show_records)
//            hideNavigationBar()
            backBtn.setOnClickListener(this)
            ivRefresh.setOnClickListener(this)
            rvRecords = findViewById(R.id.rvRecords)
            sharedPreferenceManager.getInstance(this)
            load()
            showRecordsTv.setOnClickListener(View.OnClickListener {
                try {
                    val intent = Intent(this, HawFilterRecords::class.java)
                    startActivity(intent)
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (this@HawShowRecords),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        this@HawShowRecords,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((this@HawShowRecords), (e as Exception))
                }

            })
            rvRecords?.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                    try {
                        super.onScrollStateChanged(recyclerView, newState)
                        val totalItemCount = recyclerView.layoutManager!!.itemCount
                        if (!rvRecords?.canScrollVertically(1)!! && newState == RecyclerView.SCROLL_STATE_IDLE) {
                            var skipcounter = Integer.parseInt(skip) + 10
                            skip = skipcounter.toString()
                            callRecordsApi(skipcounter.toString(), tid, doid)
                        }
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (this@HawShowRecords),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as Exception))
                    }
                }
            })
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(
                this@HawShowRecords,
                getString(R.string.something_went_wrong),
                3
            )
            SendEmail.sendEmail((this@HawShowRecords), (e as Exception))
        }
    }

    override fun onBackPressed() {
        finish()
    }

    fun load() {
        val extras = intent.extras
        rowsList.clear()
        when {
            intent.hasExtra(Constants.FILTER_TRACKING_ID) -> {
                callRecordsApi("0", extras?.getString(Constants.FILTER_TRACKING_ID).toString(), "")
            }
            intent.hasExtra(Constants.FILTER_DOCUMENT_ID) -> {
                callRecordsApi("0", "", extras?.getString(Constants.FILTER_DOCUMENT_ID).toString())
            }
            else -> {
                callRecordsApi("0", "", "")
            }
        }
    }

    private fun callRecordsApi(skip: String, tid: String, doid: String) {
        globalClass.showDialog(this)
        var request = showRecordsRequest()
        request.userId = sharedPreferenceManager.loginData.getUSERID().toString()
        request.tracking_id = tid
        request.ishaw = "1"
        request.id_doc_no = doid
        request.limit = "10"
        request.skip = skip
        request.identifier = "getgriddata"
        var baseResponse = Gson().toJson(request)
        HBLHRStore.instance?.showRecords(
            RetrofitEnums.URL_HBL,
            request,
            object : showRecordsCallBack {
                override fun showRecordsSuccess(response: showRecordsResponse) {
                    try {
                        for (item in response.data?.get(0)?.rows!!) {
                            rowsList.add(item)
                        }
                        Log.i("iterationCheck", rowsList.size.toString())
//                    (this@HawShowRecords as GlobalClass).hideLoader()
                        globalClass.hideLoader()
                        if (response.data?.get(0)?.rows!!.size > 0) {
                            if (Integer.parseInt(skip) < 10) {
                                showRecordsAdapter = ShowHawRecordsAdapter(
                                    this@HawShowRecords,
                                    intent.getIntExtra(Constants.ACTIVITY_KEY, 0),
                                    rowsList
                                )

                                rvRecords?.apply {
                                    layoutManager = LinearLayoutManager(context!!)
                                    adapter = showRecordsAdapter


                                }
                            } else {
                                showRecordsAdapter.notifyDataSetChanged()
                            }
                        } else {
                            ToastUtils.normalShowToast(this@HawShowRecords, "No record found", 1)
                        }

                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (this@HawShowRecords),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            this@HawShowRecords,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((this@HawShowRecords), (e as Exception))
                    }
                }


                override fun showRecordsFailure(response: BaseResponse) {
//                    (this@HawShowRecords as GlobalClass).hideLoader()
                    globalClass.hideLoader()

                }

            })
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.backBtn -> {
                onBackPressed()
            }
            R.id.ivRefresh -> {
                load()
            }
        }
    }
}
