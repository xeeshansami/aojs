package com.hbl.bot.ui.fragments.cif

import com.hbl.bot.ui.activities.HawActivity
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.graphics.Color
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import com.google.gson.Gson
import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.*
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.LovRequest
import com.hbl.bot.network.models.request.base.ManualVerysisRequest
import com.hbl.bot.network.models.request.baseRM.CUSTDEMOGRAPHICSX
import com.hbl.bot.network.models.request.baseRM.CUSTINFO
import com.hbl.bot.network.models.response.base.*
import com.hbl.bot.network.models.response.baseRM.City
import com.hbl.bot.network.models.response.baseRM.Country
import com.hbl.bot.network.models.response.baseRM.NadraVerify
import com.hbl.bot.network.models.response.baseRM.PurposeCIF
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep1_3.*
import kotlinx.android.synthetic.main.fragment_cifstep1_3.btNext
import kotlinx.android.synthetic.main.fragment_cifstep1_3.formSectionHeader
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException

/**
 * A simple [Fragment] subclass.
 */
class HawCifFragment2 : Fragment(), View.OnClickListener {
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myView: View? = null
    var cityOfBirthSP: Spinner? = null
    var countryOfBirthSP: Spinner? = null
    var countryOfResidenceSP: Spinner? = null
    var POCIFSpinner: Spinner? = null
    var customerInfo = CUSTINFO()
    var customerDemographics = CUSTDEMOGRAPHICSX()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            // Inflate the layout for this fragment
            if (myView == null) {
                myView = inflater.inflate(R.layout.fragment_cifstep1_3, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {
            return myView
        }
    }

    @SuppressLint("SetTextI18n")
    fun header() {
        viewModel.totalSteps.value = 2
        viewModel.currentFragmentIndex.value = 1
        var txt = resources!!.getString(R.string.customer_id_info) + " (2/2 Page)"
        formSectionHeader.header_tv_title.setText(
            GlobalClass.textColor(
                txt,
                24,
                txt.length,
                Color.RED
            )
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            btNext.setOnClickListener(this)
            btBack.setOnClickListener(this)
            (activity as HawActivity).ivRefresh!!.setOnClickListener(this)
            header()
            init()
            load()
            onBackPress(view)
            setCondition()
            disabilityFields()
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong), 3)
            SendEmail.sendEmail((activity as HawActivity), (e as Exception))
        } finally {

        }
    }

    fun init() {
        customerInfo = (activity as HawActivity).sharedPreferenceManager.customerInfo
        customerDemographics =
            (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics
        countryOfBirthSP = fsvCountryOfBirth.getSpinner(R.id.countryOfBirthID)
        cityOfBirthSP = fsvCityOfBirth.getSpinner(R.id.cityOfBirthID)
        countryOfResidenceSP = fivCOR.getSpinner(R.id.countryOfResidenceID)
        POCIFSpinner = fsvPOCIF.getSpinner(R.id.POPCIFID)
    }

    private fun disabilityFields() {
        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled == true) {
            (activity as HawActivity).globalClass?.setDisbaled(countryOfBirthSP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(cityOfBirthSP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(countryOfResidenceSP!!, false)
            (activity as HawActivity).globalClass?.setDisbaled(POCIFSpinner!!, false)
        }
    }

    private fun setCondition() {
        countryOfResidenceSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "B9"
                        && countryOfResidenceSP?.selectedItem.toString() != "Pakistan"
                    ) {
                        countryOfResidenceSP?.setSelection(0)
                        ToastUtils.normalShowToast(
                            activity,
                            "Country of residence for freelancer should be PAKISTANI, please try different",
                            1
                        )
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
        countryOfBirthSP?.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            @SuppressLint("Range")
            override fun onItemSelected(
                adapterView: AdapterView<*>?,
                view: View?,
                pos: Int,
                l: Long,
            ) {
                try {
                    (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCode(
                        (activity as HawActivity).sharedPreferenceManager.lovCountries,
                        countryOfBirthSP?.selectedItem.toString()
                    ).let {
                        if (!it!!.equals("PK",true)) {
                            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeIndex((activity as HawActivity).sharedPreferenceManager.lovCountries,
                                "PK").let {
                                countryOfBirthSP?.setSelection(it!!)
                            }
                            (activity as HawActivity).visitBranchPopup("Please visit the nearest branch to open account for other than country of PAKISTAN")
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }

            override fun onNothingSelected(adapterView: AdapterView<*>?) {
            }
        }
    }


    fun load() {
        (activity as HawActivity).globalClass?.showDialog(activity)
        if ((activity as HawActivity).sharedPreferenceManager.lovCountries.isNullOrEmpty()) {
            callAllCountries()
        } else {
            setCountries((activity as HawActivity).sharedPreferenceManager.lovCountries)
        }
    }

    fun callAllCountries() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.COUNTRIES_IDENTIFIER
        val gson = Gson()
        val json = gson.toJson(lovRequest)
        HBLHRStore.instance?.getCountries(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CountryCallBack {
                override fun CountrySuccess(response: CountryResponse) {
                    try {
                        response.data?.let {
                            setCountries(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun CountryFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    fun setCountries(it: ArrayList<Country>) {
        (activity as HawActivity).countryList = it
        (activity as HawActivity).sharedPreferenceManager.lovCountries = it
        fsvCountryOfBirth.setItemForfsvCountryOfBirth(it)
        fivCOR.setItemForfivCOR(it)
        if (!(activity as HawActivity).customerInfo.COUNTRYOFBIRTHDESC.isNullOrEmpty()) {
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                (activity as HawActivity).customerInfo.COUNTRYOFBIRTHDESC
            ).let {
//            (activity as HawActivity).globalClass?.setDisbaled(countryOfBirthSP!!,false)
                countryOfBirthSP?.setSelection(it!!)
            }
        } else {
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
                it,
                "Pakistan"
            ).let {
//            (activity as HawActivity).globalClass?.setDisbaled(countryOfBirthSP!!,false)
                countryOfBirthSP?.setSelection(it!!)
            }
        }
//        fsvCountryOfBirth.remainSelection(it.size)
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCodeGetIndex(
            it,
            "Pakistan"
        ).let {
            (activity as HawActivity).globalClass?.setDisbaled(countryOfResidenceSP!!, false)
            countryOfResidenceSP?.setSelection(it!!)
        }
        fivCOR.remainSelection(it.size)
        if ((activity as HawActivity).sharedPreferenceManager.lovCities.isNullOrEmpty()) {
            callCities()
        } else {
            setCities((activity as HawActivity).sharedPreferenceManager.lovCities)
        }
    }

    fun setCities(it: ArrayList<City>) {
        (activity as HawActivity).cityList = it
        (activity as HawActivity).sharedPreferenceManager.lovCities = it
        fsvCityOfBirth.setItemForCities(it)
        customerInfo.CITYOFBIRTHDESC.let { city ->
            (activity as HawActivity).globalClass?.findSpinnerPositionFromCityIndex(
                it,
                city
            ).let { cityOfBirthSP?.setSelection(it!!) }
        }
        fsvCityOfBirth.remainSelection(it.size)
        callPurpOfCif()
    }

    fun callCities() {
        var lovRequest = LovRequest()
        lovRequest.identifier = Constants.CITIES_IDENTIFIER
        HBLHRStore.instance?.getCities(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : CityCallBack {
                override fun CitySuccess(response: CityResponse) {
                    try {
                        response.data?.let {
                            setCities(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun CityFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }
            })
    }

    fun callPurpOfCif() {
        var lovRequest = LovRequest()
        lovRequest.condition = true
        (activity as HawActivity).customerDemoGraphicx.CUST_TYPE.let {
            lovRequest.conditionvalue = it
        }
        lovRequest.conditionname = Constants.CUST_SEGMENT
        lovRequest.identifier = Constants.PURPOSE_CIF_IDENTIFIER
        HBLHRStore.instance?.getPurposeCIF(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : PurposeCIFCallBack {
                override fun PurposeCIFSuccess(response: PurposeCIFResponse) {
                    try {
                        response.data?.let {
                            setPurposeOfCif(it)
                        };
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    } finally {

                    }
                }

                override fun PurposeCIFFailure(response: BaseResponse) {
                    //Utils.failedAwokeCalls((activity as HawActivity)) { load() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun setPurposeOfCif(it: java.util.ArrayList<PurposeCIF>) {
        fsvPOCIF.setItemForPurpOfCif(it)
        (activity as HawActivity).pepList = it
        (activity as HawActivity).sharedPreferenceManager.lovPurposeCIF = it
        customerDemographics.PURP_OF_CIF_DESC.let { city ->
            (activity as HawActivity).globalClass?.findSpinnerPositionFromPURPOFIndex(
                it,
                city
            ).let {
                if (it != 0) {
                    POCIFSpinner?.setSelection(it!!)
                } else {
                    var list = (activity as HawActivity).sharedPreferenceManager.hawKeys
                    for (x in 0 until list.size) {
                        if (list[x].IDENTIFIER == Constants.PURPOSE_CIF_IDENTIFIER) {
                            (activity as HawActivity).globalClass?.findSpinnerPositionFromPURPOFIndex(
                                (activity as HawActivity).sharedPreferenceManager.lovPurposeCIF,
                                list[x].LOV_DESC
                            ).let {
                                POCIFSpinner?.setSelection(it!!)
                            }
                        }
                    }
                }
            }
        }
        fsvPOCIF.remainSelection(it.size)
        (activity as HawActivity).globalClass?.setDisbaled(POCIFSpinner!!, false)
        (activity as HawActivity).globalClass?.hideLoader()
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.btNext -> {
                try {
                    if (validation()) {
                        (activity as HawActivity).globalClass?.showDialog(activity)
                        if ((activity as HawActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7") {
                            callManualVerysis()
                        } else {
                            (activity as HawActivity).globalClass?.setDisbaled(btNext!!, false)
                            java.util.concurrent.Executors.newSingleThreadExecutor()
                                .execute(Runnable {
                                    saveAndNext()
                                    activity?.runOnUiThread {
                                        (activity as HawActivity).recyclerViewSetup()
                                        if ((activity as HawActivity).globalClass?.isAllFieldsDisbaled!!) {
                                            if (findNavController().currentDestination?.id == R.id.CIFStep1_3) {
                                                findNavController().navigate(R.id.action_CIFStep1_3_to_CIFStep1_6)
                                            }
                                        } else
                                            if (findNavController().currentDestination?.id == R.id.CIFStep1_3) {
                                                findNavController().navigate(R.id.action_CIFStep1_3_to_CIFStep1_4)
                                            }
                                        (activity as HawActivity).globalClass?.setEnabled(
                                            btNext!!,
                                            true
                                        )
                                        (activity as HawActivity).globalClass?.hideLoader()
                                    }
                                })
                        }
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail(
                        (activity as HawActivity),
                        (e as ArrayIndexOutOfBoundsException)
                    )
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(
                        activity,
                        getString(R.string.something_went_wrong),
                        3
                    )
                    SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                } finally {

                }
            }
            R.id.btBack -> {
                var bundle = Bundle()
                bundle.putString("back", "0")
                if (findNavController().currentDestination?.id == R.id.CIFStep1_3) {
                    findNavController().navigate(R.id.action_CIFStep1_3_to_CIFStep1, bundle)
                }
            }
        }
    }

    fun callManualVerysis() {
        var lovRequest = ManualVerysisRequest()
        lovRequest.identifier = Constants.MANUAL_VERYSIS_IDENTIFIER
        lovRequest.doc_no =
            (activity as HawActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_NO
        HBLHRStore.instance?.manualVerysisRequest(
            RetrofitEnums.URL_HBL,
            lovRequest,
            object : APKCheckCallBack {
                override fun APKCheckSuccess(response: APKCheckResponse) {
                    try {
                        ToastUtils.normalShowToast(activity, response.message, 2)
                        java.util.concurrent.Executors.newSingleThreadExecutor()
                            .execute(Runnable {
                                saveAndNext()
                                clearBiometricCust()
                                activity?.runOnUiThread {
                                    (activity as HawActivity).recyclerViewSetup()
                                    if (findNavController().currentDestination?.id == R.id.CIFStep1_3) {
                                        findNavController().navigate(R.id.action_CIFStep1_3_to_CIFStep1_6)
                                    }
                                    (activity as HawActivity).globalClass?.hideLoader()
                                }
                            })
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IllegalArgumentException)
                        )
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ClassNotFoundException)
                        )
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ActivityNotFoundException)
                        )
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as IndexOutOfBoundsException)
                        )
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail(
                            (activity as HawActivity),
                            (e as ArrayIndexOutOfBoundsException)
                        )
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(
                            activity,
                            getString(R.string.something_went_wrong),
                            3
                        )
                        SendEmail.sendEmail((activity as HawActivity), (e as Exception))
                    }
                }

                override fun APKCheckFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as HawActivity)) { callManualVerysis() }
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as HawActivity).globalClass?.hideLoader()
                }

            })
    }

    private fun clearBiometricCust() {
        var obj = NadraVerify()
        (activity as HawActivity).customerBiometric = obj
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric = obj
        (activity as HawActivity).globalClass?.clearModel(SharedPreferenceManager.CUSTOMER_BIOMATRIC_KEY)
        (activity as HawActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as HawActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as HawActivity).sharedPreferenceManager.aofAccountInfo =
            (activity as HawActivity).aofAccountInfoRequest
    }


    fun saveAndNext() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in HawActivity*/
        /*set the values from widgets and another models*/
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCode(
            (activity as HawActivity).sharedPreferenceManager.lovCountries,
            countryOfBirthSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerInfo.COUNTRYOFBIRTHDESC =
                    countryOfBirthSP?.selectedItem.toString()
                (activity as HawActivity).customerInfo.COUNTRY_OF_BIRTH = it.toString()
            }
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCityDESC(
            (activity as HawActivity).sharedPreferenceManager.lovCities,
            cityOfBirthSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerInfo.CITYOFBIRTHDESC =
                    cityOfBirthSP?.selectedItem.toString()
                (activity as HawActivity).customerInfo.CITY_OF_BIRTH = it.toString()
            }
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromCountryCode(
            (activity as HawActivity).sharedPreferenceManager.lovCountries,
            countryOfResidenceSP?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                (activity as HawActivity).customerInfo.COUNTRYOFRESDESC =
                    countryOfResidenceSP?.selectedItem.toString()
                (activity as HawActivity).customerInfo.COUNTRY_OF_RES = it.toString()
            }
        }
        (activity as HawActivity).globalClass?.findSpinnerPositionFromPepCode(
            (activity as HawActivity).pepList,
            POCIFSpinner?.selectedItem.toString()
        ).let {
            if (!it.isNullOrEmpty() && it != resources!!.getString(R.string.select) && it != "0") {// when select there is no item selected "Choose an item"  <--- hint
                ((activity as HawActivity).customerDemoGraphicx).PURP_OF_CIF_CODE =
                    it.toString()
                ((activity as HawActivity).customerDemoGraphicx).PURP_OF_CIF_DESC =
                    POCIFSpinner?.selectedItem.toString()
            }
        }
        /*Save current aofAccountInfoRequest data in current sharedpreferences who initiate in HawActivity*/
        (activity as HawActivity).sharedPreferenceManager.customerInfo =
            ((activity as HawActivity).customerInfo)
        (activity as HawActivity).sharedPreferenceManager.customerDemoGraphics =
            ((activity as HawActivity).customerDemoGraphicx)
    }

    fun validation(): Boolean {
        if (countryOfBirthSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_select_birth_country), 1
            )
            return false
        } else if (cityOfBirthSP?.isEnabled == true && cityOfBirthSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.please_select_birth_city), 1
            )
            return false
        } else if (countryOfResidenceSP?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity, resources!!.getString(R.string.please_select_country_of_residence), 1
            )
            return false
        } else if (POCIFSpinner?.selectedItemPosition == 0) {
            ToastUtils.normalShowToast(
                activity, resources!!.getString(R.string.please_select_POCIF), 1
            )
            return false
        } else {
            return true
        }
    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                if (findNavController().currentDestination?.id == R.id.CIFStep1_3) {
                    findNavController().navigate(R.id.action_CIFStep1_3_to_CIFStep1)
                }
                true
            }
            false
        }
    }
}
