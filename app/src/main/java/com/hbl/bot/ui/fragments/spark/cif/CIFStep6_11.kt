package com.hbl.bot.ui.fragments.spark.cif


import com.hbl.bot.ui.activities.spark.CIFRootActivity
import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.fragment.app.activityViewModels
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.LocationListener
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.gson.Gson

import com.hbl.bot.R
import com.hbl.bot.network.ResponseHandlers.callbacks.CustomerAofAccountInfoCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.GenerateCIFCallBack
import com.hbl.bot.network.ResponseHandlers.callbacks.VerifyAccountCallBack
import com.hbl.bot.network.enums.RetrofitEnums
import com.hbl.bot.network.models.request.base.AofAccountInfoRequest
import com.hbl.bot.network.models.request.base.GenerateCIFRequest
import com.hbl.bot.network.models.request.base.VerifyAccountRequest
import com.hbl.bot.network.models.request.baseRM.CUSTNEXTOFKIN
import com.hbl.bot.network.models.request.baseRM.CUST_ACCOUNTS
import com.hbl.bot.network.models.request.baseRM.Data
import com.hbl.bot.network.models.response.base.AllSubmitResponse
import com.hbl.bot.network.models.response.base.BaseResponse
import com.hbl.bot.network.models.response.base.GenerateCIFResponse
import com.hbl.bot.network.models.response.base.VerifyAccountResponse
import com.hbl.bot.network.store.HBLHRStore
import com.hbl.bot.utils.*
import com.hbl.bot.utils.managers.SharedPreferenceManager
import com.hbl.bot.viewModels.SharedCIFViewModel
import kotlinx.android.synthetic.main.fragment_cifstep6_11.*
import kotlinx.android.synthetic.main.fragment_cifstep6_11.btBack
import kotlinx.android.synthetic.main.fragment_cifstep6_11.openaccount_btn
import kotlinx.android.synthetic.main.fragment_cifstep6_11.formSectionHeader
import kotlinx.android.synthetic.main.fragment_cifstep6_11.opencif_btn
import kotlinx.android.synthetic.main.view_form_section_header.view.*
import java.io.FileNotFoundException
import java.io.IOException
import java.lang.ClassCastException
import java.text.SimpleDateFormat
import java.util.*

/**
 * A simple [Fragment] subclass.
 */
class CIFStep6_11 : Fragment(), View.OnClickListener  , GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {
    var currentLatitude = 0.0
    var currentLongitude = 0.0
    var currentAddress = ""
    var TAG="GeoLocationGenerateACC_CIF"
    private var mGoogleApiClient: GoogleApiClient? = null
    private var mLocation: Location? = null
    private var locationManager: LocationManager? = null
    private var mLocationRequest: LocationRequest? = null
    val viewModel: SharedCIFViewModel by activityViewModels()
    var myDateTextView: TextView? = null
    val myCalendar = Calendar.getInstance()
    var myView: View? = null
    var fivAccountOfficerNameET: EditText? = null
    var fivBranchmanagerNameET: EditText? = null
    var fivComplanceAuthorityNameET: EditText? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        try {
            if (myView == null) {
                // Inflate the layout for this fragment
                myView = inflater.inflate(R.layout.fragment_cifstep6_11, container, false)
            }
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        } finally {
            return myView
        }


    }

    fun setAppStatusValue() {
        (activity as CIFRootActivity).sharedPreferenceManager.appStatus.CIF.let {
            (activity as CIFRootActivity).globalClass?.appStatus = it.toString()
        }
    }

    fun header() {
        var custSegType =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        val txt = resources.getString(R.string.edd_verification)
        if (custSegType == "A0" || /*STUDENT*/ custSegType == "A2" || /*HOUSE WIFE*/ custSegType == "A4" /*UNEMPLOYED*/) {
            viewModel.totalSteps.setValue(7)
            viewModel.currentFragmentIndex.setValue(6)
            val txt1 = " (7/7 Page)"
            val txt2 = txt + txt1
            formSectionHeader.header_tv_title.text =
                GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
        } else {
            viewModel.totalSteps.setValue(5)
            viewModel.currentFragmentIndex.setValue(4)
            val txt1 = " (5/5 Page)"
            val txt2 = txt + txt1
            formSectionHeader.header_tv_title.text =
                GlobalClass.textColor(txt2, txt.length, txt2.length, Color.RED)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        try {
            super.onViewCreated(view, savedInstanceState)
            LocalBroadcastManager.getInstance(activity as CIFRootActivity)
                .registerReceiver(mStatusCodeResponse, IntentFilter(
                    Constants.STATUS_BROADCAST));
            opencif_btn.setOnClickListener(this)
            openaccount_btn.setOnClickListener(this)
            btBack.setOnClickListener(this)
            header()
            locationGet()
            init()
            load()
            openDateDialog()
            setLengthAndType()
            /*   if (GlobalClass.isDummyDataTrue) {
                   loadDummyData()
               }*/
            onBackPress(view)
        } catch (e: UnsatisfiedLinkError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
        } catch (e: NullPointerException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
        } catch (e: IllegalArgumentException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
        } catch (e: NumberFormatException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
        } catch (e: InterruptedException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: IOException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
        } catch (e: FileNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
        } catch (e: ClassNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
        } catch (e: ActivityNotFoundException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
        } catch (e: IndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
        } catch (e: ArrayIndexOutOfBoundsException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
        } catch (e: ClassCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
        } catch (e: TypeCastException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
        } catch (e: SecurityException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
        } catch (e: IllegalStateException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
        } catch (e: OutOfMemoryError) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
        } catch (e: RuntimeException) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
        } catch (e: Exception) {
            ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
            SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
        }
    }

    private fun load() {
        (activity as CIFRootActivity).customerEdd.eDD_CONDUCT_BY_NAME
            .let { fivAccountOfficerNameET?.setText(it) }
        (activity as CIFRootActivity).customerEdd.aPPROVED_BY_NAME
            .let { fivBranchmanagerNameET?.setText(it) }
        (activity as CIFRootActivity).customerEdd.aPPROVED_COMPL_AUTH_NAME
            .let { fivComplanceAuthorityNameET?.setText(it) }
        (activity as CIFRootActivity).customerEdd.eDD_CONDUCT_BY_DATE
            .let {
                if (!it.isNullOrEmpty())
                    myDateTextView?.setText(it)
            }
    }

    fun init() {
        fivAccountOfficerNameET = fivAccountOfficerName.getTextFromEditText(R.id.acountofficeName1)
        fivBranchmanagerNameET = fivBranchmanagerName.getTextFromEditText(R.id.branchMangerName)
        fivComplanceAuthorityNameET =
            fivComplanceAuthorityName.getTextFromEditText(R.id.fivComplanceAuthorityName)
        fivComplanceAuthorityNameET =
            fivComplanceAuthorityName.getTextFromEditText(R.id.fivComplanceAuthorityName)
        myDateTextView = fivDate.getTextFromTextView(R.id.fivDate)
        if (!(activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().equals(
                "HIGH",
                true
            )
        ) {
            /*Risk Status is high*/
            openaccount_btn?.text = activity?.resources?.getString(R.string.next)
            opencif_btn?.visibility = View.GONE
        }
        //TODO: IF CRC and CUSTOMER SEGMENT MINOR/GUARDIAN then show only open account button will be shown
        if ((activity as CIFRootActivity).sharedPreferenceManager.customerInfo.iddocumenttype == "07" &&
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE == "A7"
        ) {
            opencif_btn?.visibility = View.GONE
            openaccount_btn?.visibility = View.VISIBLE
        }
    }
    private fun locationGet() {
        mGoogleApiClient = GoogleApiClient.Builder(activity as CIFRootActivity)
            .addConnectionCallbacks(this@CIFStep6_11)
            .addOnConnectionFailedListener(this@CIFStep6_11)
            .addApi(LocationServices.API)
            .build()
        locationManager = (activity as CIFRootActivity).getSystemService(Context.LOCATION_SERVICE) as LocationManager?
    }
    fun getAddress(lat: Double, lng: Double) {
        val gCoder = Geocoder(activity as CIFRootActivity)
        var addresses: List<Address>? = null
        try {
            addresses = gCoder.getFromLocation(lat, lng, 1)
        } catch (e: IOException) {
            e.printStackTrace()
        }
        if (addresses != null && addresses.isNotEmpty()) {
            currentAddress= addresses[0].getAddressLine(0)
            Log.i(TAG, addresses[0].getAddressLine(0))
        }
    }
    override fun onConnected(bundle: Bundle?) {
        if (ActivityCompat.checkSelfPermission(
                activity as CIFRootActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity as CIFRootActivity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return
        }
        startLocationUpdates()
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (mLocation == null) {
            startLocationUpdates()
        }
        if (mLocation != null) {
            val latitude = mLocation!!.getLatitude()
            val longitude = mLocation!!.getLongitude()
        } else {
            // Toast.makeText(this, "Location not Detected", Toast.LENGTH_SHORT).show();
        }
    }
    private fun startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
            .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
            .setInterval(10 * 1000)        // 10 seconds, in milliseconds
            .setFastestInterval(1 * 1000); // 1 second, in milliseconds
        // Request location updates
        if (ActivityCompat.checkSelfPermission(
                activity as CIFRootActivity,
                android.Manifest.permission.ACCESS_FINE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                activity as CIFRootActivity,
                android.Manifest.permission.ACCESS_COARSE_LOCATION
            ) !== PackageManager.PERMISSION_GRANTED
        ) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Log.i(
                TAG,
                "Location Allowed"
            )
            return
        }else{
            Log.i(
                TAG,
                "Location Allowed"
            )
        }
        val location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient)
        if (location == null) {
            LocationServices.FusedLocationApi.requestLocationUpdates(
                mGoogleApiClient!!,
                mLocationRequest!!,
                object :com.google.android.gms.location.LocationListener{
                    override fun onLocationChanged(p0: Location) {
                        Log.i(
                            TAG,
                            "${p0!!.latitude},${p0!!.longitude}"
                        )
                        currentLatitude = p0!!.latitude
                        currentLongitude = p0!!.longitude
                    }

                })
            Log.i(
                TAG,
                "$currentLatitude WORKS $currentLongitude"
            )
        } else {
            //If everything went fine lets get latitude and longitude
            currentLatitude = location.latitude
            currentLongitude = location.longitude
            //If everything went fine lets get latitude and longitude
            getAddress(currentLatitude,currentLongitude)
            Log.i(
                TAG,
                "$currentLatitude WORKS $currentLongitude"
            )
        }
        Log.d("reque", "--->>>>")
    }
    override fun onConnectionSuspended(i: Int) {
        Log.i(TAG, "Connection Suspended")
        mGoogleApiClient!!.connect()
    }
    override fun onConnectionFailed(connectionResult: ConnectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.errorCode)
    }
    override fun onStart() {
        super.onStart()
        mGoogleApiClient!!.connect()
    }
    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient!!.isConnected) {
            mGoogleApiClient!!.disconnect()
        }
    }
    fun setLengthAndType() {
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivAccountOfficerNameET!!,
            25,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivBranchmanagerNameET!!,
            25,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
        (activity as CIFRootActivity).globalClass?.edittextTypeCount(
            fivComplanceAuthorityNameET!!,
            25,
            Constants.INPUT_TYPE_CUSTOM_ALPHANUMERIC
        )
    }

    fun loadDummyData() {
        fivAccountOfficerNameET?.setText("Mr Mohammad Ahmed Shah")
        fivBranchmanagerNameET?.setText("Mohammad Mushtaq Iliyaas")
        fivComplanceAuthorityNameET?.setText("Bank Authority Name")
    }


    fun openDateDialog() {
        myDateTextView!!.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                try {
                    var datePicker =
                        activity?.let {
                            DatePickerDialog(
                                it,
                                R.style.DialogTheme,
                                date,
                                myCalendar[Calendar.YEAR],
                                myCalendar[Calendar.MONTH],
                                myCalendar[Calendar.DAY_OF_MONTH]
                            )
                        }
                    datePicker?.getDatePicker()?.setMaxDate(System.currentTimeMillis() - 1000)
                    datePicker?.show()
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
        })
    }

    var date = DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
        // TODO Auto-generated method stub
        myCalendar.set(Calendar.YEAR, year)
        myCalendar.set(Calendar.MONTH, monthOfYear)
        myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
        myCalendar.add(Calendar.DATE, 0);
        // Set the Calendar new date as minimum date of date picker
        updateLabel()
    }

    private fun updateLabel() {
//        val myFormat = "dd-MMM-yyyy" //In which you need put here
        val myFormat = "dd-MM-yyyy"
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        myDateTextView?.setText(sdf.format(myCalendar.time))
    }

    override fun onClick(v: View?) {
        when (v!!.id) {
            R.id.ivRefresh -> {
                load()
            }
            R.id.opencif_btn -> {
                try {
                    if (validation()) {
                        GlobalClass.FilledEDD = true
                        (activity as CIFRootActivity).sharedPreferenceManager.appStatus.CIF.let {
                            (activity as CIFRootActivity).globalClass?.appStatus = it.toString()
                        }
                        (activity as CIFRootActivity).globalClass?.setDisbaled(opencif_btn!!, false)
                        (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                        java.util.concurrent.Executors.newSingleThreadExecutor().execute(Runnable {
                            saveAndNext()
                            cif()
                            activity?.runOnUiThread {
                                (activity as CIFRootActivity).globalClass?.setEnabled(opencif_btn!!,
                                    true)
                            }
                        })
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.openaccount_btn -> {
                try {
                    if (validation()) {
                        (activity as CIFRootActivity).globalClass?.setDisbaled(openaccount_btn!!,
                            false)
                        (this.context as CIFRootActivity).globalClass?.showDialog(this.context)
                        verifyAccountApi()
                    }
                } catch (e: UnsatisfiedLinkError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                } catch (e: NullPointerException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                } catch (e: IllegalArgumentException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                } catch (e: NumberFormatException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                } catch (e: InterruptedException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: IOException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                } catch (e: FileNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                } catch (e: ClassNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                } catch (e: ActivityNotFoundException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                } catch (e: IndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                } catch (e: ArrayIndexOutOfBoundsException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                } catch (e: ClassCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                } catch (e: TypeCastException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                } catch (e: SecurityException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                } catch (e: IllegalStateException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                } catch (e: OutOfMemoryError) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                } catch (e: RuntimeException) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                } catch (e: Exception) {
                    ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                    SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                }
            }
            R.id.btBack -> isFinancialSupportEDDPage()
        }

    }

    fun onBackPress(view: View) {
        view.isFocusableInTouchMode = true
        view.requestFocus()
        view.setOnKeyListener { v, keyCode, event ->
            if (keyCode == KeyEvent.KEYCODE_BACK && keyCode == KeyEvent.ACTION_UP) {
                // handle back button's click listener
                isFinancialSupportEDDPage()
                true
            }
            false
        }
    }

    private fun verifyAccountApi() {
        var verifyAccount = VerifyAccountRequest()
        verifyAccount.cnic =
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.ID_DOCUMENT_NO
        verifyAccount.ACCT_TYPE = ""
        verifyAccount.identifier = "cddacctvalidation"
        HBLHRStore.instance?.VerifyAccount(
            RetrofitEnums.URL_HBL,
            verifyAccount,
            object : VerifyAccountCallBack {
                override fun VerifyAccountSuccess(response: VerifyAccountResponse) {
                    try {
                        if (response.status == "00") {
                            if (validation()) {
                                (activity as CIFRootActivity).sharedPreferenceManager.appStatus.CIFAcct.let {
                                    (activity as CIFRootActivity).globalClass?.appStatus =
                                        it.toString()
                                }
                                java.util.concurrent.Executors.newSingleThreadExecutor()
                                    .execute(Runnable {
                                        saveAndNext()
                                        openAccount()
                                        activity?.runOnUiThread {
                                            (activity as CIFRootActivity).globalClass?.setEnabled(
                                                openaccount_btn!!,
                                                true)
                                        }
                                    })
                                GlobalClass.FilledEDD = true
                            }
                        } else {
                            ToastUtils.normalShowToast(context, response.message.toString(),1)
                        }
                        (activity as CIFRootActivity).globalClass?.setEnabled(openaccount_btn!!,
                            true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun VerifyAccountFailure(response: BaseResponse) {
//                    Utils.failedAwokeCalls((activity as CIFRootActivity)) { verifyAccountApi() }
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                    ToastUtils.normalShowToast(context, response.message.toString(),1)

                }

            })
    }

    fun isFinancialSupportEDDPage() {
        var custSegType =
            (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.CUST_TYPE
        if (custSegType == "A0" || /*STUDENT*/ custSegType == "A2"  /*HOUSE WIFE*/ || custSegType == "A7" || /*MINOR*/ custSegType == "A4" /*UNEMPLOYED*/
        ) {
            if (findNavController().currentDestination?.id == R.id.CIFStep6_11)
                findNavController().navigate(R.id.action_CIFStep6_11_to_CIFStep6_9)
        } else {
            if (findNavController().currentDestination?.id == R.id.CIFStep6_11)
                findNavController().navigate(R.id.action_CIFStep6_11_to_CIFStep6_5)
        }
        (activity as CIFRootActivity).recyclerViewSetup()
    }

    private fun validation(): Boolean {
        if (fivAccountOfficerNameET?.text?.isEmpty() == true) {
            fivAccountOfficerNameET?.error = resources!!.getString(R.string.pleaseEnterAONErr)
            fivAccountOfficerNameET?.requestFocus()
            return false
        } else if (fivBranchmanagerNameET?.text?.isEmpty() == true) {
            fivBranchmanagerNameET?.error = resources!!.getString(R.string.pleaseEnterAONErr)
            fivBranchmanagerNameET?.requestFocus()
            return false
        } else if (fivComplanceAuthorityNameET?.text?.isEmpty() == true) {
            fivComplanceAuthorityNameET?.error = resources!!.getString(R.string.pleaseEnterAONErr)
            fivComplanceAuthorityNameET?.requestFocus()
            return false
        } else if (myDateTextView?.text?.toString()
                .equals(resources!!.getString(R.string.date_picker)) || myDateTextView?.text.toString()
                .isNullOrEmpty()
        ) {
            ToastUtils.normalShowToast(
                activity,
                resources!!.getString(R.string.pleaseSelectDateErr),
                1
            )
            return false
        } else {
            return true
        }
    }

    fun openAccount() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        saveAndOpenAccount()
    }

    fun saveAndOpenAccount() {
        (activity as CIFRootActivity).aofAccountInfoRequest.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let {
                    (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as CIFRootActivity).etbntbFLAG
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as CIFRootActivity).REMEDIATE
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        (activity as CIFRootActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME =
                (activity as CIFRootActivity).fullName
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as CIFRootActivity).riskRating.toString()
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }
        //TODO: set WORK_FLOW_CIF_AND_ACCOUNT
        (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "3"
        (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC = "CIF AND ACCOUNT"

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.isForcelyChangedCustomerSegment = false
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postGenerateCIF((activity as CIFRootActivity).aofAccountInfoRequest)
    }

    fun postGenerateCIF(aofAccountInfo: Data) {
        var request = GenerateCIFRequest()
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        request.payload = Constants.PAYLOAD
        request.identifier = Constants.UPDATE_DRAFT_IDENTIFIER
        request.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(request)
        Log.i("RequestJson",json)
        HBLHRStore.instance?.postGenerateCIF(
            RetrofitEnums.URL_HBL,
            request, object : GenerateCIFCallBack {
                @SuppressLint("WrongConstant")
                override fun GenerateCIFSuccess(response: GenerateCIFResponse) {
                    try {
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                        if (findNavController().currentDestination?.id == R.id.CIFStep6_11)
                            findNavController().navigate(R.id.action_CIFStep6_11_to_CIFStep7)
                        (activity as CIFRootActivity).recyclerViewSetup()
                        (activity as CIFRootActivity).globalClass?.setEnabled(openaccount_btn!!,
                            true)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun GenerateCIFFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    fun cif() {
        /*Save some fields of aofAccountInfoRequest in its object, who initiate in CIFRootActivity*/
        /*set the values from widgets and another models*/
        saveModelAndCIFGenerate()
    }

    fun saveAndNext() {
        fivAccountOfficerNameET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.eDD_CONDUCT_BY_NAME = it }
        fivBranchmanagerNameET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.aPPROVED_BY_NAME = it }
        fivComplanceAuthorityNameET?.text.toString()
            .let { (activity as CIFRootActivity).customerEdd.aPPROVED_COMPL_AUTH_NAME = it }
        myDateTextView?.text.toString()
            .let {
                if (it != resources!!.getString(R.string.date_picker)) {
                    (activity as CIFRootActivity).customerEdd.eDD_CONDUCT_BY_DATE = it
                }
            }
        (activity as CIFRootActivity).sharedPreferenceManager.setCustomerEDD((activity as CIFRootActivity).customerEdd)
    }

    fun saveModelAndCIFGenerate() {
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    it
                )
            }
        }

        (activity as CIFRootActivity).sharedPreferenceManager.customerCDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CDD?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerEDD.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_EDD?.add(
                    it
                )
            }
        }

        (activity as CIFRootActivity).sharedPreferenceManager.customerContacts.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_CONTACTS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerDemoGraphics.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_DEMOGRAPHICS?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerFIN.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_FIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerPep.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_PEP?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerAddress.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ADDR = it
        }

        (activity as CIFRootActivity).sharedPreferenceManager.customerBiomatric.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_BIOMETRIC?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerUserInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerStatus.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_STATUS?.add(
                    it
                )
            }
        }
        cifGenerate()
    }

    fun cifGenerate() {
        (activity as CIFRootActivity).aofAccountInfoRequest.CHANNEL = "2"
        //TODO: set trackingID
        if ((activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()
                .isNullOrEmpty()
        ) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.trackingID.getBatch()?.get(0)
                ?.itemDescription.let {
                    (activity as CIFRootActivity).aofAccountInfoRequest.TRACKING_ID = it.toString()
                }
        }
        //TODO: set ETBNTBFLAG
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as CIFRootActivity).etbntbFLAG
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.ETBNTBFLAG =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.ETBNTBFLAG
        }
        //TODO: set REMEDIATE
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as CIFRootActivity).REMEDIATE
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.REMEDIATE =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.REMEDIATE
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.INITIATED_USER_ID =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getUSERID().toString()
        (activity as CIFRootActivity).aofAccountInfoRequest.PICKEDBY_ROLE =
            (activity as CIFRootActivity).sharedPreferenceManager.loginData.getROLE().toString()
        //TODO: set NAME
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME =
                (activity as CIFRootActivity).fullName
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.NAME =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.NAME
        }
        //TODO: set RISK_RATING
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING.toString().isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as CIFRootActivity).riskRating.toString()
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING
        }
        //TODO: set RISK_RATING_TOTAL
        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL.isNullOrEmpty()) {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).riskRatingTotal!!
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.RISK_RATING_TOTAL =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.RISK_RATING_TOTAL
        }
        //TODO: set BRANCH_NAME
        (activity as CIFRootActivity).sharedPreferenceManager.branchCode.BR_NAME.let {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH = it.toString()
        }
        //TODO: set MYSIS_REF
        if ((activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.MYSIS_REF.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        } else {
            (activity as CIFRootActivity).sharedPreferenceManager.mySisRef.data.get(0).MYSIS_REF.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.MYSIS_REF = it.toString()
            }
        }

        //TODO: set WORK_FLOW_CIF
        (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE = "1"
        (activity as CIFRootActivity).aofAccountInfoRequest.WORK_FLOW_CODE_DESC = "CIF"

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH = it.BR_CODE
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_BRANCH =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_BRANCH
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.USER_REGION = it.REG_CODE
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.USER_REGION =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.USER_REGION
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.AREA
        }

        if ((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA.isNullOrEmpty()) {
            (activity as CIFRootActivity).sharedPreferenceManager.businessArea.let {
                (activity as CIFRootActivity).aofAccountInfoRequest.BUSINESS_AREA = it.BUSINESS_AREA
            }
        } else {
            (activity as CIFRootActivity).aofAccountInfoRequest.BUSINESS_AREA =
                (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.BUSINESS_AREA
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.isForcelyChangedCustomerSegment = false
        removeObjects()
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
        postFinalGenerateCIF((activity as CIFRootActivity).aofAccountInfoRequest)
    }

    fun removeObjects() {
        (activity as CIFRootActivity).globalClass?.clearModel(SharedPreferenceManager.CUSTOMER_ACCOUNT)
        (activity as CIFRootActivity).globalClass?.clearModel(SharedPreferenceManager.CUSTOMER_NEXTOFKIN)
        var customerNextOfKin: CUSTNEXTOFKIN = CUSTNEXTOFKIN()
        var customerAccounts: CUST_ACCOUNTS = CUST_ACCOUNTS()

        (activity as CIFRootActivity).customerAccounts = customerAccounts
        (activity as CIFRootActivity).customerNextOfKin = customerNextOfKin


        (activity as CIFRootActivity).sharedPreferenceManager.customerAccount.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.set(
                    0,
                    customerAccounts
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_ACCOUNTS?.add(
                    customerAccounts
                )
            }
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerNextOfKin.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.set(
                    0,
                    customerNextOfKin
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_NEXTOFKIN?.add(
                    customerNextOfKin
                )
            }
        }
        activity?.runOnUiThread {
            (activity as CIFRootActivity).recyclerViewSetup()
        }
    }

    fun postFinalGenerateCIF(aofAccountInfo: Data) {
        aofAccountInfo.GEOLOCATION.LATITUDE=currentLatitude.toString()
        aofAccountInfo.GEOLOCATION.LONGITUDE=currentLongitude.toString()
        aofAccountInfo.GEOLOCATION.GEO_LOCATION_ADDRESS=currentAddress.toString()
        (activity as CIFRootActivity).globalClass?.showDialog(activity)
        var aofAccountInfoRequest = AofAccountInfoRequest()
        aofAccountInfoRequest.identifier = Constants.FINAL_DRAFT_IDENTIFIER
        aofAccountInfoRequest.data = aofAccountInfo
        val gson = Gson()
        val json = gson.toJson(aofAccountInfoRequest)
        Log.i("RequestJson",json)
        HBLHRStore.instance?.postAofaccountInfo(
            RetrofitEnums.URL_HBL,
            aofAccountInfoRequest, object : CustomerAofAccountInfoCallBack {
                @SuppressLint("WrongConstant")
                override fun CustomerAofAccountInfoSuccess(response: AllSubmitResponse) {
                    try {
                        (activity as CIFRootActivity).globalClass?.hideLoader()
                        response.data?.get(0)?.CIF_NO.let { saveCIFNoInSP(it!!) }
                        Handler().postDelayed({
                            response.message?.let {
//                            (activity as CIFRootActivity).globalClass?.clearSubmitAOF()
                                confirmCompletedUpload(it)
                            }
                        }, 500)
                    } catch (e: UnsatisfiedLinkError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as UnsatisfiedLinkError))
                    } catch (e: NullPointerException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NullPointerException))
                    } catch (e: IllegalArgumentException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalArgumentException))
                    } catch (e: NumberFormatException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as NumberFormatException))
                    } catch (e: InterruptedException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as InterruptedException))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: IOException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IOException))
                    } catch (e: FileNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as FileNotFoundException))
                    } catch (e: ClassNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassNotFoundException))
                    } catch (e: ActivityNotFoundException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ActivityNotFoundException))
                    } catch (e: IndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IndexOutOfBoundsException))
                    } catch (e: ArrayIndexOutOfBoundsException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ArrayIndexOutOfBoundsException))
                    } catch (e: ClassCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as ClassCastException))
                    } catch (e: TypeCastException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as TypeCastException))
                    } catch (e: SecurityException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as SecurityException))
                    } catch (e: IllegalStateException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as IllegalStateException))
                    } catch (e: OutOfMemoryError) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as OutOfMemoryError))
                    } catch (e: RuntimeException) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as RuntimeException))
                    } catch (e: Exception) {
                        ToastUtils.normalShowToast(activity, getString(R.string.something_went_wrong),3)
                        SendEmail.sendEmail((activity as CIFRootActivity), (e as Exception))
                    }
                }

                override fun CustomerAofAccountInfoFailure(response: BaseResponse) {
                    ToastUtils.normalShowToast(activity, response.message, 1)
                    (activity as CIFRootActivity).globalClass?.hideLoader()
                }
            })
    }

    private fun saveCIFNoInSP(cifNo: String) {
        (activity as CIFRootActivity).customerInfo.CIF_NO = cifNo
        (activity as CIFRootActivity).customerInfo.let {
            (activity as CIFRootActivity).sharedPreferenceManager.customerInfo = it
        }
        (activity as CIFRootActivity).sharedPreferenceManager.customerInfo.let {
            if ((activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.size != 0) {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.set(
                    0,
                    it
                )
            } else {
                (activity as CIFRootActivity).aofAccountInfoRequest.CUST_INFO?.add(
                    it
                )
            }
        }
        (activity as CIFRootActivity).aofAccountInfoRequest.let {
            (activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo = it
        }
    }

    fun confirmCompletedUpload(msg: String) {
        val builder = AlertDialog.Builder((activity as CIFRootActivity))
        builder.setTitle(msg)
        builder.setCancelable(false)
        builder.setMessage((activity as CIFRootActivity).sharedPreferenceManager.aofAccountInfo.TRACKING_ID.let { "Your tracking number is $it" })
        builder.setPositiveButton(android.R.string.yes) { dialog, which ->
            if (findNavController().currentDestination?.id == R.id.CIFStep6_11)
                findNavController().navigate(R.id.route_to_document_page)
        }
        builder.show()
    }

    private val mStatusCodeResponse: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {
            // Get extra data included in the Intent
            Log.i("Clicks", "Enabled")
            try {
                (activity as CIFRootActivity).globalClass?.setEnabled(openaccount_btn!!, true)
                (activity as CIFRootActivity).globalClass?.setEnabled(opencif_btn!!, true)
            } catch (ex: Exception) {
            }
        }
    }

    override fun onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(activity as CIFRootActivity)
            .unregisterReceiver(mStatusCodeResponse)
        super.onDestroy()
    }
}
