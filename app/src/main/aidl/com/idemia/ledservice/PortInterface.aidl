package com.idemia.ledservice;

interface PortInterface {
	void setTorchLed(int upLedValue, int downLedValue);
    void setFlashLed(int upLedValue, int downLedValue);
    void setIRLed(int leftLedValue, int rightLedValue);
}
