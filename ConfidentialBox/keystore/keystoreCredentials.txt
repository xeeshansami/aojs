Alias: Hbl@alias1234
All Password: Hbl@1234
_________________________________

Remaining: Keystore Data is in Images
_________________________________
Keystore File Name: hbl_bot_keystore.jks
First And Last Name: Mohammad Zeeshan
Organizational Unit: It And Solutions
Organizational: Habib Bank Limited
City Or Locality: Karachi
State Or Province: Sindh
Country Code (XX): 923412030258
Validity: 50 Years
Location of JKS File: D:\HBL\Android\Project_Gitlab\bot-tabapp\keystore\hbl_bot_keystore.jks
Output APK Destination: D:\HBL\Android\Project_Gitlab\bot-tabapp\app\live\release\apk
Remote Directory: https://gitlab.com/mobileapphblteam2022/bot-tabapp.git


Regards,
Mohammad Zeeshan
Software Developer – IT SOLUTIONS

Phone#:	+92 21 36215591
Ext#	15591
Address:	HBL Innovation Center, 2nd Floor, Mai Kolachi Bypass, Intelligence Colony, Karachi, Pakistan.
 
